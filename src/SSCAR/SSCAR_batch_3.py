#! /usr/bin/env python3
"""
Function to batch process Cases using SSCAR

Step 3 in an overall process

Last modified: 2018/7/26
Authors: Ingo Jahn, Conor Lane
"""

import numpy as np
import sys as sys 
import os as os 
import shutil as shutil
from getopt import getopt
import matplotlib.pyplot as plt 
import SSCAR as SSCAR


def main(uODict):
    # function to create data file

    resultsFileName  = uoDict.get("--resultsfile", "test_r")

    ## add .py extension if forgotten
    #if not ".py" in resultsFileName:
    # 	resultsFileName = ''.join([resultsFileName, ".py"])

    if "--outputExtension" not in uoDict:
        print("    outputExtension not defined. Setting outputExtension = '_out.txt'")
        outputExtension = "_out.txt"
    else:
        outputExtension     = uoDict.get("--outputExtension" )


    while True:

        lines = []
        with open(resultsFileName,'r') as f:

            check = 0
            for line in f:
                if line.rstrip().split(" ")[0] == '#': # Skips Header and commented lines
                    pass
                elif line.rstrip().split(" ")[1] == '0': # Only execute lines wit Status == 0
                    if check == 0:
                        check = 1

                        # create input and output file
                        jobfile = line.split(" ")[0]
                        if not ".py" in jobfile:
        	                jobfile = ''.join([jobfile, ".py"])
                        outfile = jobfile.split(".")[0]+outputExtension

                        print("JOBFILE",jobfile)

                        # run SCCAR
                        Input_String = "SSCAR.py --job={0} --out-file={1} --noplot".format(jobfile,outfile)
                        

                        try:
                            print("    Running Command: {0}".format(Input_String))
                            #os.system(Input_String)
                            Dict = {'--job':jobfile, '--out-file':outfile, '--noplot':''}
                            SSCAR.main(Dict)
                            # change status in results file to 1   
                            line = line.split(" ")[0]+" 1\n"                
                        except:
                            print(" Evaluation failed")
                            # change status in results file to "e"
                            line = line.split(" ")[0]+" e\n"
                    else:
                        pass
                else:
                    #print("skipping Line", line.rstrip().split(" ") )
                    pass
                lines.append(line)
                

        # if no unprocessed line has been found exit while loop
        if check == 0:
            break

        # writing updated results file
        with open(resultsFileName,'w') as f:
            for line in lines:
                f.write(line)

        plt.pause(1)

    return 0
###
###
shortOptions = ""
longOptions = ["help", "verbosity=", "resultsfile=", "outputBase="]
###
###
def printUsage():
    print("")
    print("Usage: SSCAR_batch_3.py [--help] [--resultsfile=<FileName>]")
    print("              [--outputBase=<FielName>] [--verbosity=0 (1 or 2)]")
    return
###
###
class MyError(Exception):
    def __init__(self, value):
        self.value = value
    def __str__(self):
        return repr(self.value)
###
###
if __name__ == "__main__":
    userOptions = getopt(sys.argv[1:], shortOptions, longOptions)
    uoDict = dict(userOptions[0])
    
    #if len(userOptions[0]) == 0 or "--help" in uoDict:
    #    printUsage()
    #    sys.exit(1)

    # execute the code
    try:
        print("START: SSCAR_batch_3.py")
        main(uoDict)
        print("  SUCESS.")
        print("COMPLETE: SSCAR_batch_3.py")
        print("\n \n")

    except MyError as e:
        print("This run has gone bad.")
        print(e.value)
        sys.exit(1)
