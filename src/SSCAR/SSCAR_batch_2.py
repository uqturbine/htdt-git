#! /usr/bin/env python3
"""
Function to batch process Cases using SSCAR

Step 2 in an overall process

Last modified: 2018/7/26
Authors: Ingo Jahn
"""

import numpy as np
import sys as sys 
import os as os 
import shutil as shutil
from getopt import getopt



def main(uODict):
    # function to create data file

    dataFileName     = uoDict.get("--datafile"   , "test_d")
    MasterFileName   = uoDict.get("--MasterCase" , "test_M")

    # add .py extension if forgotten
    if not ".py" in MasterFileName:
    	MasterFileName = ''.join([MasterFileName, ".py"])

    if "--resultsfile" not in uoDict:
        print("    resultsfile not defined. Setting resultsfile = '{0}_results'".format(dataFileName))
        resultsFileName = dataFileName + "_results"
    else:
        resultsFileName  = uoDict.get("--resultsfile")

    if "--BaseCase" not in uoDict:
        print("    BaseCase not defined. Setting BaseCase = 'case'")
        baseFileName = "case"
    else:
        baseFileName     = uoDict.get("--BaseCase" )

    # strip .py extension from BaseCase
    if ".py" in baseFileName:
        baseFileName = baseFileName.split(".")[0]



    data = []
    # extrac variable names from datafile
    with open(dataFileName,'r') as f:
        for line in f:
            item = line.split(" ")
            if item[0] == '#':
                # extract varNames
                varNames = []
                for name in item[1:]:
                    varNames.append( name.split(":")[1])

                print("    The following variables have been identified: ", varNames)
            else:
                data.append(item)


    # check that MastCase is correctly formated
    with open(MasterFileName,'r') as f_M:
        # check that all variable names are found in MasterCase
        for var in varNames:
            check = 0
            for line in f_M:
                if (var+" =") in line:
                    check = check+1
            if check != 1:
                raise MyError("Variable '{0}' not correctly defined in Case {1}. There should be excatly one line that starts with '{0} ='. Currently there are {2}." .format(var,MasterFileName,check)  )
            f_M.seek(0) # move read cursor back to beginning of file
  

    # create new output files 
    List_of_Filenames = []
    with open(MasterFileName,'r') as f_M: 
        count = 0
        for row in data:
            # define filename where current case will be saved
            Current_file = baseFileName+"_"+str(count)+".py"

            lines = []
            # lines 
            for line in f_M:
                for i in range(len(varNames)):
                    var = varNames[i]
                    if (var+" =") in line:
                        line = var+" = {0:.4f} \n".format(float(row[i]))
                # add lines to new output file
                lines.append(line)
            f_M.seek(0) # move read cursor back to beginning of file

            # write to output file
            with open(Current_file,'w') as f:
                for line in lines:
                    f.write(line)

            # keep a record of all the files we have generated.
            List_of_Filenames.append(Current_file)
            count= count+1


    # start the resultsFile
    with open(resultsFileName,'w') as f:
        f.write("# 0:InputFilename 1:Status\n")
        for Case in List_of_Filenames:
            f.write("{0} {1}\n".format(Case,0))

    return 0
###
###
shortOptions = ""
longOptions = ["help", "verbosity=", "datafile=", "MasterCase=", "BaseCase=", "resultsfile="]
###
###
def printUsage():
    print("")
    print("Usage: SSCAR_batch_2.py [--help] [--datafile=<FileName>] [--MasterCase=<FileName>]")
    print("              [--resultsfile=<FileName>] [--BaseCase=<FileName>]")
    print("              [--verbosity=0 (1 or 2)]")
    return
###
###
class MyError(Exception):
    def __init__(self, value):
        self.value = value
    def __str__(self):
        return repr(self.value)
###
###
if __name__ == "__main__":
    userOptions = getopt(sys.argv[1:], shortOptions, longOptions)
    uoDict = dict(userOptions[0])
    
    #if len(userOptions[0]) == 0 or "--help" in uoDict:
    #    printUsage()
    #    sys.exit(1)

    # execute the code
    try:
        print("START: SSCAR_batch_2.py")
        main(uoDict)
        print("  SUCESS.")
        print("COMPLETE: SSCAR_batch_2.py")
        print("\n \n")

    except MyError as e:
        print("This run has gone bad.")
        print(e.value)
        sys.exit(1)
