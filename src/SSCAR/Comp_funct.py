#! /usr/bin/env python3
"""
Python Code to scale a given compressor map, and evaulate performance

Function has two operating modes:
(1) Stand alone
This evaluates the compressor map and plots the maps. 
Optionally the design point and multiple operating poitns can be plotted on the 
map.

(2) imported
The function can be called by SSCAR.py to allow the quasi-steady evaluation 
of compressor performance as part of Cycle off-design modelling.

Author: Joshua Keep & Ingo Jahn
Last Modified: 20/02/2017
"""

import csv
import numpy as np
import scipy.interpolate 
import matplotlib.pyplot as plt
import sys as sys 
import os as os 
from getopt import getopt
import CoolProp.CoolProp as CP
import itertools


################################################################################
################################################################################
class Compressor:
    """
    Class that defines the compressor and that contains the compressor related functions
    """
    def __init__(self):
        self.type =         None 
        self.mdot_eta =     None
        self.MFP_deta =     None
        self.DH_eta =       None
        self.PR_eta =       None
        self.eta =          []
        self.mdot_speed =   None
        self.MFP_speed =    None
        self.DH_speed =     None
        self.PR_speed =     None
        self.speed =        []
        self.mdot_design =  None
        self.MFP_design =   None
        self.speed_design = []
        self.DH_design =    None
        self.PR_design =    None
        self.eta_design =   []
        self.Volume =       []
        self.Tcorr =        []
        self.Pcorr =        []
        self.method = 'linear'

    def checkdata(self):
        # check that input data is correct
        if self.type == 'MFP_PR':
            # TODO: implement error chcking for MFP and PR input
            pass
        if self.type == 'MDOT_PR':
            # TODO: implement error chcking for MFP and PR input
            pass
        else:
            # TODO
            if (len(self.mdot_eta) != len(self.DH_eta)) or (len(self.mdot_eta) != len(self.eta)) or  (self.mdot_eta.any() == None) or (self.DH_eta.any() == None) or (self.eta.any() == None):
                print('mdot_eta : ', self.mdot_eta)
                print('PR_eta   : ', self.DH_eta)
                print('eta      : ', self.eta)
                print('mdot_eta (length): ', len(self.mdot_eta))
                print('PR_eta   (length): ', len(self.DH_eta))
                print('eta      (length): ', len(self.eta))
                raise MyError("Error in lists defining mdot_eta and PR_eta and eta")
            if (len(self.mdot_speed) != len(self.DH_speed)) or (len(self.mdot_speed) != len(self.speed)) or  (self.mdot_speed.any() == None) or (self.DH_speed.any() == None) or (self.speed.any() == None):
                print('mdot_eta : ', self.mdot_speed)
                print('PR_eta   : ', self.DH_speed)
                print('eta      : ', self.speed)
                print('mdot_eta (length): ', len(self.mdot_speed))
                print('PR_eta   (length): ', len(self.DH_speed))
                print('eta      (length): ', len(self.speed))
                raise MyError("Error in lists defining mdot_speed and PR_speed and speed")
        if not isinstance(self.mdot_design,float):
            print('mdot_design :', self.mdot_design)
            raise MyError("Error in definition of mdot_design")
        if not isinstance(self.speed_design,float):
            print('speed_design :', self.speed_design)
            raise MyError("Error in definition of speed_design")
        if self.type == 'MFP_PR':
            # TODO: implement error chcking for MFP and PR input
            pass
        elif self.type == 'MDOT_PR':
            # TODO: implement error chcking for MFP and PR input
            pass
        else:
            if not isinstance(self.DH_design,float):
                raise MyError("DH_design :{0} \n    Error in definition of DH_design".format(self.DH_design))
        if not isinstance(self.eta_design,float):
            print('eta_design :', self.eta_design)
            raise MyError("Error in definition of eta_design")
        if not isinstance(self.Volume,float):
            print('Volume :', self.Volume)
            print('Volume not defined. Volume set to 0. m3')
            self.Volume = 0.
        if not isinstance(self.Tcorr,float):
            print('Tcorr :', self.Tcorr)
            raise MyError("Error in definition of Tcorr")
        if not isinstance(self.Pcorr,float):
            print('Pcorr :', self.Pcorr)
            raise MyError("Error in definition of Pcorr")

    def create_griddata(self,res=20j):
        """
        Function to create maps of ETA SPEED and DH at equivalent conditions. 
        Maps haev been dimensionalised based on design points. 
        """
        method = 'linear'
        #method = 'cubic'

        if self.type == 'MFP_PR':
            # establish limits of available input data
            min_MFP   = min( [min(self.MFP_eta), min(self.MFP_speed)] )
            max_MFP   = max( [max(self.MFP_eta), max(self.MFP_speed)] )
            min_PR    = min( [min(self.PR_eta),  min(self.PR_speed)] )
            max_PR    = max( [max(self.PR_eta),  max(self.PR_speed)] )
            min_speed = min( self.speed )
            max_speed = max( self.speed )

            # create grids for interpolation
            self.MFP_grid     = np.mgrid[ min_MFP   :max_MFP   :res]*self.MFP_design
            self.PR_grid      = np.mgrid[ min_PR    :max_PR    :res]*self.PR_design
            self.speed_grid   = np.mgrid[ min_speed :max_speed :res]*self.speed_design
            grid_MFP, grid_PR = np.meshgrid(self.MFP_grid, self.PR_grid)
            grid_MFP, grid_speed = np.meshgrid(self.MFP_grid, self.speed_grid)

            # need to normalise grids to ensure intepolation works properly
            MFP_scale   = max_MFP   * self.MFP_design
            PR_scale    = max_PR    * self.PR_design
            speed_scale = max_speed * self.speed_design

            # create interpolants
            self.ETA   = scipy.interpolate.griddata((self.MFP_eta*self.MFP_design / MFP_scale , self.PR_eta*self.PR_design / PR_scale), 
                              self.eta*self.eta_design, (grid_MFP/MFP_scale, grid_PR/PR_scale), method=method, fill_value=-1)
            self.SPEED = scipy.interpolate.griddata((self.MFP_speed*self.MFP_design / MFP_scale, self.PR_speed*self.PR_design / PR_scale), 
                              self.speed*self.speed_design, (grid_MFP/MFP_scale, grid_PR/PR_scale), method=method, fill_value=-1)
            self.PR    = scipy.interpolate.griddata((self.MFP_speed*self.MFP_design / MFP_scale, self.speed*self.speed_design /max_speed), 
                              self.PR_speed*self.PR_design, (grid_MFP/MFP_scale, grid_speed/max_speed), method=method, fill_value=-1)

            # create limits for interpolation
            self.I_MFP_min = min_MFP*self.MFP_design; self.I_MFP_max = max_MFP*self.MFP_design
            self.I_speed_min = min_speed*self.speed_design; self.I_speed_max = max_speed*self.speed_design
            self.I_PR_min = min_PR*self.PR_design; self.I_PR_max = max_PR*self.PR_design; 
            #print 'Limits: ', self.I_mass_min, self.I_mass_max, self.I_speed_min, self.I_speed_max, self.I_dh_min, self.I_dh_max

            # create line that follows surge margin
            # TODO: add surge line
            #self.SURGE   = scipy.interpolate.interp1d(self.speed_surge,self.mdot_surge,kind=method, fill_value=-1)

            # create MDOT interpolant on the speed vs PR map.
            self.MFP    = scipy.interpolate.griddata( (self.speed*self.speed_design / max_speed, self.PR_speed*self.PR_design / PR_scale), 
                              self.MFP_speed*self.MFP_design, (grid_speed/max_speed, grid_PR/PR_scale), method=method, fill_value=-1)
            # create DH_ISEN interpolant on mdot vs dh map
            #self.DH_ISEN = scipy.interpolate.griddata((self.mdot_eta*self.mdot_design /mass_scale , self.DH_eta*self.DH_design /DH_scale), 
            #                  self.DH_isen_eta*self.DH_design, (grid_mass/mass_scale,grid_DH/DH_scale), method=method, fill_value=-1)

        elif self.type == 'MDOT_PR':
            # establish limits of available input data
            min_mdot  = min( [min(self.mdot_eta), min(self.mdot_speed)] )
            max_mdot  = max( [max(self.mdot_eta), max(self.mdot_speed)] )
            min_PR    = min( [min(self.PR_eta),  min(self.PR_speed)] )
            max_PR    = max( [max(self.PR_eta),  max(self.PR_speed)] )
            min_speed = min( self.speed )
            max_speed = max( self.speed )

            # create grids for interpolation
            self.mdot_grid    = np.mgrid[ min_mdot    :max_mdot    :res]*self.mdot_design
            self.PR_grid      = np.mgrid[ min_PR    :max_PR    :res]*self.PR_design
            self.speed_grid   = np.mgrid[ min_speed :max_speed :res]*self.speed_design
            grid_mdot, grid_PR = np.meshgrid(self.mdot_grid, self.PR_grid)
            grid_mdot, grid_speed = np.meshgrid(self.mdot_grid, self.speed_grid)

            # need to normalise grids to ensure intepolation works properly
            mdot_scale = max_mdot*self.mdot_design
            PR_scale    = max_PR    * self.PR_design
            speed_scale = max_speed * self.speed_design

            # create interpolants
            self.ETA   = scipy.interpolate.griddata((self.mdot_eta*self.mdot_design / mdot_scale , self.PR_eta*self.PR_design / PR_scale), 
                              self.eta*self.eta_design, (grid_mdot/mdot_scale, grid_PR/PR_scale), method=method, fill_value=-1)
            self.SPEED = scipy.interpolate.griddata((self.mdot_speed*self.mdot_design / mdot_scale, self.PR_speed*self.PR_design / PR_scale), 
                              self.speed*self.speed_design, (grid_mdot/mdot_scale, grid_PR/PR_scale), method=method, fill_value=-1)
            self.PR    = scipy.interpolate.griddata((self.mdot_speed*self.mdot_design / mdot_scale, self.speed*self.speed_design /max_speed), 
                              self.PR_speed*self.PR_design, (grid_mdot/mdot_scale, grid_speed/max_speed), method=method, fill_value=-1)

            # create limits for interpolation
            self.I_mdot_min = min_mdot*self.mdot_design; self.I_mdot_max = max_mdot*self.mdot_design
            self.I_speed_min = min_speed*self.speed_design; self.I_speed_max = max_speed*self.speed_design
            self.I_PR_min = min_PR*self.PR_design; self.I_PR_max = max_PR*self.PR_design; 
            #print 'Limits: ', self.I_mass_min, self.I_mass_max, self.I_speed_min, self.I_speed_max, self.I_dh_min, self.I_dh_max

            # create line that follows surge margin
            # TODO: add surge line
            #self.SURGE   = scipy.interpolate.interp1d(self.speed_surge,self.mdot_surge,kind=method, fill_value=-1)

            # create MDOT interpolant on the speed vs PR map.
            self.MDOT    = scipy.interpolate.griddata( (self.speed*self.speed_design / max_speed, self.PR_speed*self.PR_design / PR_scale), 
                              self.mdot_speed*self.mdot_design, (grid_speed/max_speed, grid_PR/PR_scale), method=method, fill_value=-1)
            # create DH_ISEN interpolant on mdot vs dh map
            #self.DH_ISEN = scipy.interpolate.griddata((self.mdot_eta*self.mdot_design /mass_scale , self.DH_eta*self.DH_design /DH_scale), 
            #                  self.DH_isen_eta*self.DH_design, (grid_mass/mass_scale,grid_DH/DH_scale), method=method, fill_value=-1)

        else:
            # establish limits of available input data
            min_mdot = min( [min(self.mdot_eta), min(self.mdot_speed)] )
            max_mdot = max( [max(self.mdot_eta), max(self.mdot_speed)] )
            min_DH   = min( [min(self.DH_eta), min(self.DH_speed)] )
            max_DH   = max( [max(self.DH_eta), max(self.DH_speed)] )
            min_DH_isen = min(self.DH_isen_eta)
            max_DH_isen = max(self.DH_isen_eta)
            min_speed = min( self.speed)
            max_speed = max( self.speed)

            # create grids for interpolation
            self.mass_grid    = np.mgrid[ min_mdot    : max_mdot    : res] * self.mdot_design
            self.DH_grid      = np.mgrid[ min_DH      : max_DH      : res] * self.DH_design
            self.DH_isen_grid = np.mgrid[ min_DH_isen : max_DH_isen : res] * self.DH_design
            self.speed_grid   = np.mgrid[ min_speed   : max_speed   : res] * self.speed_design
            grid_mass, grid_DH = np.meshgrid(self.mass_grid, self.DH_grid)
            grid_mass, grid_speed = np.meshgrid(self.mass_grid, self.speed_grid)

            # need to normalise grids to ensure intepolation works properly
            mass_scale = max_mdot*self.mdot_design
            DH_scale = max_DH*self.DH_design
            speed_scale = max_speed*self.speed_design

            # create interpolants
            self.ETA   = scipy.interpolate.griddata((self.mdot_eta*self.mdot_design /mass_scale , self.DH_eta*self.DH_design /DH_scale), 
                              self.eta*self.eta_design, (grid_mass/mass_scale,grid_DH/DH_scale), method=method, fill_value=-1)
            self.SPEED = scipy.interpolate.griddata((self.mdot_speed*self.mdot_design/mass_scale, self.DH_speed*self.DH_design /DH_scale), 
                              self.speed*self.speed_design, (grid_mass/mass_scale,grid_DH/DH_scale), method=method, fill_value=-1)
            self.DH    = scipy.interpolate.griddata((self.mdot_speed*self.mdot_design/mass_scale, self.speed*self.speed_design /max_speed), 
                              self.DH_speed*self.DH_design, (grid_mass/mass_scale,grid_speed/max_speed), method=method, fill_value=-1)

            # create limits for interpolation
            self.I_mass_min = min_mdot*self.mdot_design; self.I_mass_max = max_mdot*self.mdot_design
            self.I_speed_min = min_speed*self.speed_design; self.I_speed_max = max_speed*self.speed_design
            self.I_dh_min = min_DH*self.DH_design; self.I_dh_max = max_DH*self.DH_design; 
            #print 'Limits: ', self.I_mass_min, self.I_mass_max, self.I_speed_min, self.I_speed_max, self.I_dh_min, self.I_dh_max

            # create line that follows surge margin
            self.SURGE   = scipy.interpolate.interp1d(self.speed_surge,self.mdot_surge,kind=method, fill_value=-1)

            # create MDOT interpolant on the speed vs dh map.
            self.MDOT    = scipy.interpolate.griddata( (self.speed*self.speed_design/max_speed, self.DH_speed*self.DH_design /DH_scale), 
                              self.mdot_speed*self.mdot_design, (grid_speed/max_speed, grid_DH/DH_scale), method=method, fill_value=-1)
            # create DH_ISEN interpolant on mdot vs dh map
            self.DH_ISEN = scipy.interpolate.griddata((self.mdot_eta*self.mdot_design /mass_scale , self.DH_eta*self.DH_design /DH_scale), 
                              self.DH_isen_eta*self.DH_design, (grid_mass/mass_scale,grid_DH/DH_scale), method=method, fill_value=-1)

    def calc_downstream(self, Massflow, Speed, P_in, T_in, fluid):
        """
        function to calculate compressore downstream conditions based on operating condition at the inlet, mass flow and speed
        Inputs:
        Massflow - mass flow rate through compressor (kg/s)
        Speed - rotational speed (RPM)
        P_in - inlet pressure (Pa)
        T_in - inlet temperature (K)
        fluid - fluid type for use with CoolProp

        Outputs:
        P_out - outlet pressure (Pa)
        T_out - outlet temperature (K)
        delta_H - enthalpy rise (kJ)
        eta - compressor efficiency
        """   
        # find enthalpy rise for given speed and massflow combination
        #print 'Calculating compressor outlet conditions'
        #print 'massflow range :', self.mass_grid
        #print 'speed range    :', self.speed_grid
        #print 'DH range       :', self.DH_grid
        #print self.DH
        #print self.ETA
        #print self.SPEED

        if self.type == 'MFP_PR':
            raise MyError('Compressor type {} currently not implemented in calc_downstream()')
        elif self.type == 'MDOT_PR':
            # convert mass flow and speed to equivalent conditions      
            N_eq = get_N_eq(Speed,self.Tcorr,self.Pcorr,T_in,P_in)
            mdot_eq = get_mdot_eq(Massflow,self.Tcorr,self.Pcorr,T_in,P_in)

            #N_eq = Speed
            #mdot_eq = Massflow

            print("    Just before Compressor map interpolation")
            print("    Tin,Tcorr",T_in,self.Tcorr)
            print("    Pin,Pcorr",P_in,self.Pcorr)
            print("    Speed", Speed, N_eq)
            print("    mdot", Massflow, mdot_eq)

            # limit bounds of interpolation
            if N_eq < self.I_speed_min:
                N_eq = self.I_speed_min
                print('Warning: Compressor interpolation trying to exceed min speed. Equivalent speed (N_eq) limited to ', N_eq)
            elif N_eq > self.I_speed_max:
                N_eq = self.I_speed_max
                print('Warning: Compressor interpolation trying to exceed max speed. Equivalent speed (N_eq) limited to ', N_eq)
            if mdot_eq < self.I_mdot_min:
                mdot_eq = self.I_mdot_min
                print('Warning: Compressor interpolation trying to exceed min mass flow. Equivalent mass flow rate (mdot_eq) limited to ', mdot_eq)
            elif mdot_eq > self.I_mdot_max:
                mdot_eq = self.I_mdot_max
                print('Warning: Compressor interpolation trying to exceed max mass flow. Equivalent mass flow rate (mdot_eq) limited to ', mdot_eq)

#            # check if within map
#           # TODO: add surge function
#            mdot_min = self.SURGE(N_eq)
#            if mdot_eq < mdot_min:
#                mdot_eq = mdot_min
#                print('Warning: Compressor trying to operate outside of surge line. Equivalent mass flow rate (mdot_eq) limited to ', mdot_eq)

            # intrpolation mass flow rate vs Speed map to obtain pressure ratio. 
            PR = scipy.interpolate.interpn( (self.mdot_grid, self.speed_grid), self.PR.T , (mdot_eq,N_eq)  , method='linear', bounds_error=False, fill_value=-1)[0]
            #print(self.PR.T)
            #print(self.mdot_grid)
            #print(self.speed_grid)

            #print("    Pressure Ratio",PR)

            # limit bounds of interpolation
            if PR < self.I_PR_min:
                PR = self.I_PR_min
                print('Warning: Compressor interpolation trying to exceed min pressure ratio. Pressure Ratio (PR) limited to ', PR)
            elif PR > self.I_PR_max:
                PR = self.I_PR_max
                print('Warning: Compressor interpolation trying to exceed max pressure ratio. Pressure Ratio (PR) limited to ', PR)

            # do intrpolation mass flow rate vs pressure ratio map to find efficiency
            eta = scipy.interpolate.interpn( (self.mdot_grid, self.PR_grid)   , self.ETA.T, (mdot_eq, PR), method='linear', bounds_error=False, fill_value=-1)[0]

            # pass compressor efficiency back to SSCAR
            return eta, PR

        else:
            # convert mass flow and speed to equivalent conditions      
            N_eq = get_N_eq(Speed,self.Tcorr,self.Pcorr,T_in,P_in)
            mdot_eq = get_mdot_eq(Massflow,self.Tcorr,self.Pcorr,T_in,P_in)

            # limit bounds of interpolation
            if N_eq < self.I_speed_min:
                N_eq = self.I_speed_min
                print('Warning: Compressor interpolation trying to exceed min speed. Equivalent speed (N_eq) limited to ', N_eq)
            elif N_eq > self.I_speed_max:
                N_eq = self.I_speed_max
                print('Warning: Compressor interpolation trying to exceed max speed. Equivalent speed (N_eq) limited to ', N_eq)
            if mdot_eq < self.I_mass_min:
                mdot_eq = self.I_mass_min
                print('Warning: Compressor interpolation trying to exceed min mass flow. Equivalent mass flow rate (mdot_eq) limited to ', mdot_eq)
            elif mdot_eq > self.I_mass_max:
                mdot_eq = self.I_mass_max
                print('Warning: Compressor interpolation trying to exceed max mass flow. Equivalent mass flow rate (mdot_eq) limited to ', mdot_eq)

            # check if within map
            mdot_min = self.mdot_design * self.SURGE(N_eq)
            if mdot_eq < mdot_min:
                mdot_eq = mdot_min
                print('Warning: Compressor trying to oeprate outside of surge line. Equivalent mass flow rate (mdot_eq) limited to ', mdot_eq)

            # do intrpolation of maps to find correct conditions. 
            delta_h_eq = scipy.interpolate.interpn( (self.mass_grid, self.speed_grid), self.DH.T , (mdot_eq,N_eq)  , method='linear', bounds_error=False, fill_value=-1)[0]

            # limit bounds of interpolation
            if delta_h_eq < self.I_dh_min:
                delta_h_eq = self.I_dh_min
                print('Warning: Compressor interpolation trying to exceed min enthalpy change. Equivalent enthalpy change (delta_h_eq) limited to ', delta_h_eq)
            elif delta_h_eq > self.I_dh_max:
                delta_h_eq = self.I_dh_max
                print('Warning: Compressor interpolation trying to exceed max enthalpy change. Equivalent enthalpy change (delta_h_eq) limited to ', delta_h_eq)

            # do intrpolation of maps to find correct conditions. Get eta and delta_h
            eta = scipy.interpolate.interpn( (self.mass_grid, self.DH_grid)   , self.ETA.T, (mdot_eq,delta_h_eq), method='linear', bounds_error=False, fill_value=-1)[0]
            delta_h = get_h(delta_h_eq,self.Tcorr,self.Pcorr,T_in,P_in)

            # calculate conditions at other side of compressor using isentropic relations and eta
            h_in = CP.PropsSI('HMASS','P',P_in,'T', T_in ,fluid)    # enthalpy (J/kg)
            s_in = CP.PropsSI('SMASS','P',P_in,'T', T_in ,fluid)    # entropy (j/kg/K)

            #print h_in, s_in
            # perform thermodynamic energy balance calculation
            delta_h_isen = eta/100. * delta_h
            h_out_isen = h_in + delta_h_isen
            h_out = h_in + delta_h

            # use EoS to get output conditions
            P_out = CP.PropsSI('P','HMASS',h_out_isen,'SMASS',s_in,fluid)
            T_out = CP.PropsSI('T','P',P_out,'HMASS',h_out,fluid)
            delta_H = delta_h * Massflow  

            print('\n')
            print('Compressor Outlet Conditions:')
            print('P_out, T_out, delta_H, eta :', P_out, T_out, delta_H, eta)
            print(Massflow)

            return P_out, T_out, delta_h, eta

    def plot_map_eq(self, MFP=[], MDOT=[], SPEED=[], DH=[], PR=[], ETA=[],
                    Tin=[], Pin=[], STRING=[], AXIS=[], label='',
                    marker_string=('*', 'v', '^', 's', '.', 'o')):
        """
        Plot compressor map and to show operating points if supplied.

        Map and oeprating poitns are at equivalent conditions
        Inputs:
        MFP   - list of mass flow parameters
        MDOT  - list of mass flows (actual)
        SPEED - list of speeds (actual)
        DH    - list of specific enthalpy differnce (actual)
        PR    - list of pressure ratios
        ETA   - list of efficiencies 
        Tin   - list of inlet temperature at actual conditions 
                (if empty defaults to Tcrr, if single value same will be used for all)
        Tin   - list of inlet pressure at actual conditions 
                (if empty defaults to Pcrr, if single value same will be used for all)
        STRING- list of labels corresponding to conditions
        label - string containing label for current compressor
        AXIS  - list of axis used for plotting. Needs to be two axis opjects.
        marker_sring - recursively used list for plot labels
        """
        # check that correct combination of MFP + PR or MDOT + DH is defined
        if self.type == 'MFP_PR':
            if MFP == None or PR == None:
                raise MyError('In plot_map_eq; C.type={0}; list for MFP or PR defined incorrectly MFP={1}, PR={2}'.format(self.type,MFP,PR))
            # copy MFP into MDOT to allow pre-processing
            # MDOT=MFP
        elif self.type == 'MDOT_PR':
            if MDOT == None or PR == None:
                raise MyError('In plot_map_eq; C.type={0}; list for MDOT or PR defined incorrectly MDOT={1}, PR={2}'.format(self.type,MFP,PR))
            # copy MFP into MDOT to allow pre-processing
            # MDOT=MFP
        else:
            if MDOT == None or DH == None:
                raise MyError('In plot_map_eq; C.type={0}; list for MDOT or DH defined incorrectly MDOT={1}, DH={2}'.format('n/a',MDOT,DH))

        # creaet Tin and Pin lists
        if len(Tin) is 0:
            Tin = np.ones(len(MDOT))*self.Tcorr
        elif len(Tin) is 1:
            Tin = np.ones(len(MDOT))*Tin
        if len(Pin) is 0:
            Pin = np.ones(len(MDOT))*self.Pcorr
        elif len(Tin) is 1:
            Pin = np.ones(len(MDOT))*Pin

        # check if rwo axis are suppplied. If not create new figures.
        if not ( isinstance(AXIS,(list, tuple)) and len(AXIS) == 2):
            f1, ax1 = plt.subplots( 1, figsize=(11,8))
            f2, ax2 = plt.subplots( 1, figsize=(11,8))
        else:
            ax1 = AXIS[0]
            ax2 = AXIS[1]

        if self.type == 'MFP_PR':
            # plot MFP, PR, ETA map 
            vmax = max(self.eta*self.eta_design)
            vmin = min(self.eta*self.eta_design)
            plt.sca(ax1)  
            plt.xticks(fontsize=16) 
            plt.yticks(fontsize=16)      
            plt.pcolormesh(self.MFP_grid,self.PR_grid,self.ETA, cmap = plt.get_cmap('rainbow'), vmin=vmin, vmax=vmax)
            plt.scatter(self.MFP_eta*self.MFP_design, self.PR_eta*self.PR_design, 
                          c =self.eta*self.eta_design, cmap = plt.get_cmap('rainbow'), 
                          marker='o', edgecolors='black', vmin=vmin, vmax=vmax)
            plt.colorbar()
            #CS=plt.contour(self.mass_grid,self.DH_grid,self.SPEED, 10, colors='k')
            CS=plt.contour(self.MFP_grid,self.PR_grid,self.ETA, 10, colors='k')
            #plt.plot(self.mdot_surge*self.mdot_design,self.dh_surge*self.DH_design, 'g-',label='Surge Line') 

            plt.clabel(CS, inline=1, fmt='%1.0f', fontsize=10) #fmt adjusts rounding
            plt.xlabel('Mass Flow Parameter [???]',fontsize=16)
            plt.ylabel('Pressure Ratio [-]',fontsize=16)
            plt.title('{0} map:  Contours of efficiency [%]'.format(label),fontsize=18)

            marker = itertools.cycle(marker_string) # reset iterator for point markers
            if len(MFP) > 0:
                # add extra points
                for i in range(len(MFP)):
                    # convert metric data for points to equivalent conditions
                    MFP_eq = MFP[i]
                    PR_eq  = PR[i]
                    plt.plot(MFP_eq,PR_eq,marker=next(marker), markersize=10)
                if len(STRING) > 0:
                    plt.legend(STRING,loc=4)

            # plot MFP, PR, SPEED map
            vmax = np.amax(self.PR)
            vmin = max(np.amin(self.PR),1.0)
            plt.sca(ax2) 
            plt.xticks(fontsize=16) 
            plt.yticks(fontsize=16)
            plt.pcolormesh(self.MFP_grid,self.speed_grid,self.PR, cmap = plt.get_cmap('rainbow'), vmin=vmin, vmax=vmax)
            plt.scatter(self.MFP_speed*self.MFP_design, self.speed*self.speed_design, 
                          c=self.PR_speed*self.PR_design, cmap = plt.get_cmap('rainbow'), 
                          marker='o', edgecolors='black', vmin=vmin, vmax=vmax)
            plt.colorbar()
            CS=plt.contour(self.MFP_grid,self.speed_grid, self.PR, 10,colors='k')
            plt.clabel(CS, inline=1, fmt='%1.1f', fontsize=10) #fmt adjusts rounding

            # TODO: add surge line
            # plt.plot(self.mdot_surge*self.mdot_design,self.speed_surge*self.speed_design, 'g-',label='Surge Line')
            
            plt.xlabel('Mass flow parameter [???]',fontsize=16)
            plt.ylabel('Speed (normalised) [-]',fontsize=16)
            plt.title('{0} map:  Contours of pressure ratio [-]'.format(label),fontsize=18)

            marker = itertools.cycle(marker_string) # reset iterator for point markers
            if len(MFP) > 0:
                # add extra points
                for i in range(len(MFP)):
                    MFP_eq = MFP[i]
                    speed_eq   = get_N_eq(SPEED[i],self.Tcorr,self.Pcorr,Tin[i],Pin[i])
                    #plt.plot(MDOT[i],SPEED[i],marker=marker.next(), markersize=10)
                    plt.plot(MFP_eq,speed_eq,marker=next(marker), markersize=10)
            plt.legend(['Surge Line']+STRING,loc=4)

        elif self.type == 'MDOT_PR':
            # plot MFP, PR, ETA map
            vmax = max(self.eta*self.eta_design)
            vmin = min(self.eta*self.eta_design)
            plt.sca(ax1)  
            plt.xticks(fontsize=16) 
            plt.yticks(fontsize=16)      
            plt.pcolormesh(self.mdot_grid,self.PR_grid,self.ETA, cmap = plt.get_cmap('rainbow'), vmin=vmin, vmax=vmax)
            plt.scatter(self.mdot_eta*self.mdot_design, self.PR_eta*self.PR_design, 
                          c =self.eta*self.eta_design, cmap = plt.get_cmap('rainbow'), 
                          marker='o', edgecolors='black', vmin=vmin, vmax=vmax)
            plt.colorbar()
            #CS=plt.contour(self.mass_grid,self.DH_grid,self.SPEED, 10, colors='k')
            CS=plt.contour(self.mdot_grid,self.PR_grid,self.ETA, 20, colors='k')
            #plt.plot(self.mdot_surge*self.mdot_design,self.dh_surge*self.DH_design, 'g-',label='Surge Line') 

            plt.clabel(CS, inline=1, fmt='%0.2f', fontsize=10) #fmt adjusts rounding
            plt.xlabel('Mass Flow Rate [kg/s]',fontsize=16)
            plt.ylabel('Pressure Ratio [-]',fontsize=16)
            plt.title('{0} map:  Contours of efficiency [%]'.format(label),fontsize=18)

            marker = itertools.cycle(marker_string) # reset iterator for point markers
            if len(MDOT) > 0:
                # add extra points
                for i in range(len(MDOT)):
                    # convert metric data for points to equivalent conditions
                    mdot_eq = get_mdot_eq(MDOT[i],self.Tcorr,self.Pcorr,Tin[i],Pin[i])
                    PR_eq  = PR[i]
                    plt.plot(mdot_eq, PR_eq, marker=next(marker), markersize=10, markeredgecolor='k')
                if len(STRING) > 0:
                    plt.legend(STRING,loc=4)

            # plot MFP, PR, SPEED map
            vmax = np.amax(self.PR)
            vmin = max(np.amin(self.PR),1.0)
            plt.sca(ax2) 
            plt.xticks(fontsize=16) 
            plt.yticks(fontsize=16)
            plt.pcolormesh(self.mdot_grid,self.speed_grid,self.PR, cmap = plt.get_cmap('rainbow'), vmin=vmin, vmax=vmax)
            plt.scatter(self.mdot_speed*self.mdot_design, self.speed*self.speed_design, 
                          c=self.PR_speed*self.PR_design, cmap = plt.get_cmap('rainbow'), 
                          marker='o', edgecolors='black', vmin=vmin, vmax=vmax)
            plt.colorbar()
            CS=plt.contour(self.mdot_grid,self.speed_grid, self.PR, 10,colors='k')
            plt.clabel(CS, inline=1, fmt='%1.1f', fontsize=10) #fmt adjusts rounding

            # TODO: add surge line
            # plt.plot(self.mdot_surge*self.mdot_design,self.speed_surge*self.speed_design, 'g-',label='Surge Line')
            
            plt.xlabel('Mass Flow Rate [kg/s]',fontsize=16)
            plt.ylabel('Speed [rad/s]',fontsize=16)
            plt.title('{0} map:  Contours of pressure ratio [-]'.format(label),fontsize=18)

            marker = itertools.cycle(marker_string) # reset iterator for point markers
            if len(MDOT) > 0:
                # add extra points
                for i in range(len(MDOT)):
                    mdot_eq = get_mdot_eq(MDOT[i],self.Tcorr,self.Pcorr,Tin[i],Pin[i])
                    speed_eq   = get_N_eq(SPEED[i],self.Tcorr,self.Pcorr,Tin[i],Pin[i])
                    #plt.plot(MDOT[i],SPEED[i],marker=marker.next(), markersize=10)
                    plt.plot(mdot_eq,speed_eq,marker=next(marker), markersize=10, markeredgecolor='k')
            # plt.legend(['Surge Line']+STRING, loc=4)

        else:
            # plot MDOT, DH, ETA map 
            vmax = max(self.eta*self.eta_design)
            vmin = min(self.eta*self.eta_design)
            plt.sca(ax1)  
            plt.xticks(fontsize=16) 
            plt.yticks(fontsize=16)      
            plt.pcolormesh(self.mass_grid,self.DH_grid,self.ETA, cmap = plt.get_cmap('rainbow'), vmin=vmin, vmax=vmax)
            plt.scatter(self.mdot_eta*self.mdot_design, self.DH_eta*self.DH_design, 
                          c =self.eta*self.eta_design, cmap = plt.get_cmap('rainbow'), 
                          marker='o', edgecolors='black', vmin=vmin, vmax=vmax)
            plt.colorbar()
            #CS=plt.contour(self.mass_grid,self.DH_grid,self.SPEED, 10, colors='k')
            CS=plt.contour(self.mass_grid,self.DH_grid,self.ETA, 10, colors='k')
            #plt.plot(self.mdot_surge*self.mdot_design,self.dh_surge*self.DH_design, 'g-',label='Surge Line') 

            plt.clabel(CS, inline=1, fmt='%1.0f', fontsize=10) #fmt adjusts rounding
            plt.xlabel('Mass flow rate [kg/s]',fontsize=16)
            plt.ylabel('Specific enthalpy rise [J/kg]',fontsize=16)
            plt.title(label+' map:  Contours of efficiency [%]',fontsize=18)

            marker = itertools.cycle(marker_string) # reset iterator for point markers
            if len(MDOT) > 0:
                # add extra points
                for i in range(len(MDOT)):
                    # convert metric data for points to equivalent conditions
                    mdot_eq = get_mdot_eq(MDOT[i],self.Tcorr,self.Pcorr,Tin[i],Pin[i])
                    dh_eq   = get_h_eq(DH[i],self.Tcorr,self.Pcorr,Tin[i],Pin[i])
                    #plt.plot(MDOT[i],DH[i],marker=marker.next(), markersize=10)
                    plt.plot(mdot_eq,dh_eq,marker=next(marker), markersize=10)
                if len(STRING) > 0:
                    plt.legend(STRING,loc=4)

            # plot MDOT, DH, SPEED map
            vmax = np.amax(self.DH)
            vmin = np.amin(self.DH)
            plt.sca(ax2) 
            plt.xticks(fontsize=16) 
            plt.yticks(fontsize=16)
            plt.pcolormesh(self.mass_grid,self.speed_grid,self.DH, cmap = plt.get_cmap('rainbow'), vmin=vmin, vmax=vmax)
            plt.scatter(self.mdot_speed*self.mdot_design, self.speed*self.speed_design, 
                          c=self.DH_speed*self.DH_design, cmap = plt.get_cmap('rainbow'), 
                          marker='o', edgecolors='black', vmin=vmin, vmax=vmax)
            plt.colorbar()
            CS=plt.contour(self.mass_grid,self.speed_grid,self.DH, 10,colors='k')
            plt.clabel(CS, inline=1, fmt='%1.0f', fontsize=10) #fmt adjusts rounding
            plt.plot(self.mdot_surge*self.mdot_design,self.speed_surge*self.speed_design, 'g-',label='Surge Line')
            plt.xlabel('Mass flow rate [kg/s]',fontsize=16)
            plt.ylabel('Speed (normalised) [-]',fontsize=16)
            plt.title(label+' map:  Contours of specific enthalpy rise [J/kg]',fontsize=18)

            marker = itertools.cycle(marker_string) # reset iterator for point markers
            if len(MDOT) > 0:
                # add extra points
                for i in range(len(MDOT)):
                    mdot_eq = get_mdot_eq(MDOT[i],self.Tcorr,self.Pcorr,Tin[i],Pin[i])
                    speed_eq   = get_N_eq(SPEED[i],self.Tcorr,self.Pcorr,Tin[i],Pin[i])
                    #plt.plot(MDOT[i],SPEED[i],marker=marker.next(), markersize=10)
                    plt.plot(mdot_eq,speed_eq,marker=next(marker), markersize=10)
            plt.legend(['Surge Line']+STRING,loc=4)

        return 0

################################################################################
### Helper Functions
################################################################################

"""functions to correct map per Glassman 1972. Assume gamma referes to inlet"""
def eval_heat_ratio(T0_std,P0_std,Tin,Pin):
    "fucntion to evaluate specific heat ratio for map correction for non-const inlet conds"
    C_P_std= CP.PropsSI('CP0MASS','P',P0_std,'T', T0_std ,'CO2')
    C_V_std= CP.PropsSI('CVMASS','P',P0_std,'T', T0_std ,'CO2')
    gm_std=C_P_std/C_V_std
    C_P= CP.PropsSI('CP0MASS','P',Pin,'T', Tin ,'CO2')
    C_V= CP.PropsSI('CVMASS','P',Pin,'T', Tin ,'CO2')
    gm=C_P/C_V
    V_cr_ratio=(gm_std*(gm+1.)*T0_std)/(gm*(gm_std+1.)*Tin)
    return V_cr_ratio

def eval_epsilon(T0_std,P0_std,Tin,Pin):
    "fucntion to evaluate epsilon for map correction for non-const inlet conds"
    C_P_std= CP.PropsSI('CP0MASS','P',P0_std,'T', T0_std ,'CO2')
    C_V_std= CP.PropsSI('CVMASS','P',P0_std,'T', T0_std ,'CO2')
    gm_std=C_P_std/C_V_std
    C_P= CP.PropsSI('CP0MASS','P',Pin,'T', Tin ,'CO2')
    C_V= CP.PropsSI('CVMASS','P',Pin,'T', Tin ,'CO2')
    gm=C_P/C_V
    epsilon=(gm_std*(2./(gm_std+1.))**(gm_std/(gm_std-1.)))/(gm*(2./(gm+1.))**(gm/(gm-1.)))
    return epsilon

def get_h(h_eq,T0_std,P0_std,Tin,Pin):
    "function to evaluate specific enthalpy given equivalent value at point"
    V_cr=eval_heat_ratio(T0_std,P0_std,Tin,Pin)
    h=h_eq/(V_cr)
    return h

def get_h_eq(h,T0_std,P0_std,Tin,Pin):
    "function to evaluate equivalent enthalpy given value at point"
    V_cr=eval_heat_ratio(T0_std,P0_std,Tin,Pin)
    h_eq=h*V_cr
    return h_eq
   
def get_mdot_eq(mdot,T0_std,P0_std,Tin,Pin):
    "function to evaluate equivalent mass flow rate given value at point"
    V_cr=eval_heat_ratio(T0_std,P0_std,Tin,Pin)
    epsilon=eval_epsilon(T0_std,P0_std,Tin,Pin)
    mdot_eq=mdot*np.sqrt(1./V_cr)*(P0_std/Pin)*epsilon
    return mdot_eq

def get_mdot(mdot_eq,T0_std,P0_std,Tin,Pin):
    "function to evaluate mass flow rate given equivalent value at point"
    V_cr=eval_heat_ratio(T0_std,P0_std,Tin,Pin)
    epsilon=eval_epsilon(T0_std,P0_std,Tin,Pin)
    mdot=mdot_eq*np.sqrt(V_cr)*(Pin/P0_std)/epsilon
    return mdot

def get_N_eq(N,T0_std,P0_std,Tin,Pin):
    "function to evaluate equivalent speed given value at point"
    V_cr=eval_heat_ratio(T0_std,P0_std,Tin,Pin)
    N_eq=N*V_cr
    return N_eq

def get_N(N_eq,T0_std,P0_std,Tin,Pin):
    "function to evaluate equivalent enthalpy given value at point"
    V_cr=eval_heat_ratio(T0_std,P0_std,Tin,Pin)
    N=N_eq/V_cr
    return N

################################################################################
def main(uoDict): # main function

    # main file to be executed 
    #  dataFileName = uoDict.get("--datafile", "test")
    dataFileName = uoDict.get("--datafile",
            "/home/maxgains/htdt/examples/SSCAR/map_components/comp_data.py")

    # strip .py extension form jobName
    dataName = dataFileName.split('.')
    dataName = dataName[0]

    print_flag = 1
    # set print_flag (can be overwritten from jobfile)
    if "--noprint" in uoDict:
        print_flag = 0

    C = Compressor()

    # Execute jobFile, this creates all the variables
    exec(open(dataFileName).read(),globals(),locals())
    #execfile(dataFileName,globals(),locals())

    # check that loaded compressor data is correct.
    C.checkdata()

    # create interpolants
    C.create_griddata(res=100j)

    Test = 0
    if Test is 1:
        # Test case to show how map can be evaluated
        # Works for default case with map defined by mdot, deltaH, speed
        Massflow = 10.       # (kg/s)
        Speed = 1.0       # (RPM)
        P_in = 7.687*1.e6         # (Pa)
        T_in = 305.3         # (K)
        fluid = 'CO2'       # string to suit CoolProp

        P_out, T_out, delta_h, eta = C.calc_downstream(Massflow, Speed, P_in, T_in, fluid)
        delta_H = delta_h * Massflow

        print('Operating Conditions:')
        print('Massflow (kg/s) :', Massflow)
        print('Speed     (RPM) :', Speed)
        print('P-in       (Pa) :', P_in)
        print('T-in        (K) :', T_in)

        print('Compressor Outlet Conditions:')
        print('delta_h (J/kg):', delta_h)
        print('delta_H   (J) :', delta_H)
        print('eta       (%) :', eta)
        print('P-out    (Pa) :', P_out)
        print('T-out     (K) :', T_out)
        print('Pressure Ratio:', P_out/P_in)
    elif Test is 2:
        # Test case to show how map can be evaluated
        # Works for MDOT_PR cases
        print(C.type)
        Massflow = 0.15       # (kg/s)
        Speed = 8000.0       # (RPM)
        P_in = 1.3*1.e5         # (Pa)
        T_in = 305.3         # (K)
        fluid = 'CO2'       # string to suit CoolProp

        eta, PR = C.calc_downstream(Massflow, Speed, P_in, T_in, fluid)

        print('Operating Conditions:')
        print('Massflow (kg/s) :', Massflow)
        print('Speed     (RPM) :', Speed)
        print('P-in       (Pa) :', P_in)
        print('T-in        (K) :', T_in)

        print('Compressor Outlet Conditions:')
        print('eta       (%) :', eta)
        print('P-out    (Pa) :', P_in/PR)
        print('Pressure Ratio:', PR)

    if print_flag is 1:
        # Design Point
        print("Design Point")

        if C.type == 'MFP_PR':
            print('MFP   [kg/s K^0.5 bar^-1]: {0:.4f}'.format(C.MFP_design))
            print('Speed             [rad/s]: {0:.2f}'.format(C.speed_design))
            print('PR                    [-]: {0:.4f}'.format(C.PR_design))
        if C.type == 'MDOT_PR':
            print('mdot   [kg/s]: {0:.4f}'.format(C.mdot_design))
            print('Speed [rad/s]: {0:.2f}'.format(C.speed_design))
            print('PR        [-]: {0:.4f}'.format(C.PR_design))
        else:
            print('mdot   [kg/s]: {0:.4f}'.format(C.mdot_design))
            print('Speed [rad/s]: {0:.2f}'.format(C.speed_design))
            print('DH   [J/kg.K]: {0:.4f}'.format(C.DH_design))

    # Plot Function
    if ("--plotmap" in uoDict) and Test is 0:
        if C.type == 'MFP_PR':
            C.plot_map_eq(MFP=[C.MFP_design],SPEED=[C.speed_design],PR=[C.PR_design],ETA = [100.],STRING=['Design Point'])
        elif C.type == 'MDOT_PR':
            C.plot_map_eq(MDOT=[C.mdot_design],SPEED=[C.speed_design],PR=[C.PR_design],ETA = [100.],STRING=['Design Point'])
        else:
            C.plot_map_eq(MDOT=[C.mdot_design],SPEED=[C.speed_design],DH=[C.DH_design],ETA = [100.],STRING=['Design Point'])
        plt.draw()
    elif ("--plotmap" in uoDict) and Test is not 0:
        if C.type == 'MDOT_PR':
            C.plot_map_eq(MDOT=[C.mdot_design, Massflow], SPEED=[C.speed_design, Speed], PR=[C.PR_design, PR], ETA=[100.], STRING=['Design Point'])
        else:
            C.plot_map_eq(MDOT=[C.mdot_design,Massflow],SPEED=[C.speed_design,Speed],DH=[C.DH_design,delta_h],ETA=[100.,eta],STRING=['Design Point','Test Point'])
        plt.draw()

    # Close all figures
    if "--plotmap" in uoDict:
        plt.pause(1) # <-------
        print('\n \n')

        input('<Hit Enter To Close Figures>')

        plt.close()

    return C


shortOptions = ""
longOptions = ["help", "datafile=", "noprint", "plotmap"]


def printUsage():
    print("")
    print("Usage: Comp_funct.py [--help] [--datafile=<dataFileName>] [--noprint] [--plotmap]")
    return


class MyError(Exception):
    def __init__(self, value):
        self.value = value
    def __str__(self):
        return repr(self.value)


if __name__ == "__main__":
    userOptions = getopt(sys.argv[1:], shortOptions, longOptions)
    uoDict = dict(userOptions[0])
    
    if len(userOptions[0]) == 0 or "--help" in uoDict:
        printUsage()
        sys.exit(1)

    # execute the code
    try:
        main(uoDict)
        print("\n \n")
        print("SUCCESS.")
        print("\n \n")

    except MyError as e:
        print("This run has gone bad.")
        print(e.value)
        sys.exit(1)
