#! /usr/bin/env python
"""
Collection of postprocessign functions to plo the outputs from Cycle.py. The 
following functions are provided:

plot_points(points) - Function to plot the properties at all the 'points' used 
            in the simulation. 

            INPUTS: 
            points - Class POINT containing all the point instances

plot_components(points,Components) -  Function to plot each component used in
            the simulation. This provdies a quick way to give an overview of the 
            simulation and whether matching conditions have been reached at the 
            component interfaces.

            INPUTS:
            Components - List containing all the Class instances of the differnt 
                        components use in the smulation. 
            points - Class POINT containing all the point instances

plot_compressor_map(Compressor) - Function to plot the compressor maps for the 
            differnt compressors used in the cycle simulation. For each 
            compressor a Delta_H vs mdot and N vs mdot map at equivalent 
            conditions is plotted.

            INPUTS:
            Compressor - Class containing instances of the different compressors 
                        used in the cycle simualtion

plot_turbine_map(Turbine) - Function to plot the turbine operating line for the 
            differnt turbines used in the cycle simulation. For each 
            compressor a eta vs UUC/U0 and ER vs mdot map at equivalen 
            conditions is plotted.

            INPUTS:
            Turbine - Class containing instances of the different turbines used 
                        in the cycle simualtion

plot_TS(points,components,fluid) - Function to plot the cycle of a TS-diagram. 
            The cycel data will be overlayed on a TS diagram of the correspnding 
            fluid showing contours of rho and h. 

            INPUTS:
            points - Class POINT containing all the point instances
            components - List containing all the Class instances of the differnt 
                        components use in the smulation.
            fluid - string, as used by CoolProp to identify the working fluid to 
                        be plotted. Only components with this fluid will be 
                        included in TS diagram

labelLine(line,x,label=None,align=True,**kwargs) - Function to attach labels to 
            graph lines in existing plots

            INPUTS:
            line - identifier of the line to which the label will be attached
            x - x position of the label.
            label - string of the label
            align - option to align the label with the line gradient. 

Author: Ingo Jahn
Last Modified: 30/03/2017
"""

import numpy as np
import CoolProp
import CoolProp.CoolProp as CP
from CoolProp.Plots.Plots import PropertyPlot
import matplotlib.pyplot as plt 
from math import atan2,degrees


###
###
def plot_points(points):
    """
    function to plot conditions (Pressure and Temperature) at the points between the components
    """
    # create plot of pressure along different locations
    #plt.figure(figsize=(11,8))
    f, (ax1, ax2) = plt.subplots( 2, sharex=True,figsize=(11,8))

    point_string = []
    PL_list = []
    PR_list = []
    TL_list = []
    TR_list = []
    for point in points.instances:
        point_string.append(point.label)
        PL_list.append(point.PL)
        PR_list.append(point.PR)
        TL_list.append(point.TL)
        TR_list.append(point.TR)
    # add repeat of first point
    point = points.instances[0]
    point_string.append(point.label)
    PL_list.append(point.PL)
    PR_list.append(point.PR)
    TL_list.append(point.TL)
    TR_list.append(point.TR)

    # create np arrays to allow manipulation
    PL_list = np.array(PL_list)
    PR_list = np.array(PR_list)
    TL_list = np.array(TL_list)
    TR_list = np.array(TR_list)

    # plot pressure data    
    ax1.plot(range(len(PL_list)), PL_list/1.e6,'k*:',markersize=10,markerfacecolor='k',fillstyle='full',label='left')
    ax1.plot(range(len(PR_list)), PR_list/1.e6,'ko:',markersize=10,markerfacecolor='k',fillstyle='full',label='right')
    ax1.legend()

    # plot temperature data
    ax2.plot(range(len(TL_list)), TL_list,'k*:',markersize=10,markerfacecolor='k',fillstyle='full',label='left')
    ax2.plot(range(len(TR_list)), TR_list,'ko:',markersize=10,markerfacecolor='k',fillstyle='full',label='right')
    ax2.legend()

    # set labels
    ax1.set_ylabel('Pressure (MPa)',fontsize=20)
    ax1.set_title('Pressure at Points',fontsize=20)
    ax2.set_ylabel('Temperature (K)',fontsize=20)
    ax2.set_title('Temperature at Points',fontsize=20)
    for tick in ax1.get_yticklabels():
        tick.set_fontsize(16)
    for tick in ax2.get_yticklabels():
        tick.set_fontsize(16)
    plt.xlabel('Point',fontsize=20)
    plt.xticks(range(len(PL_list)), point_string, fontsize=20) 
###
###
def plot_components(points,Components):
    """
    Function to plot different components and how pressure and temperature varies across the component
    """
    # create plot of pressure along different locations
    #plt.figure(figsize=(11,8))
    f, (ax1, ax2) = plt.subplots( 2, sharex=True,figsize=(11,8))

    # define scaling factor for pressure
    p_scale = 1e3

    # find co-located points (e.g. at Recuperator)
    for comp in Components:
        if comp.type is 'RECUP':
            pointL1 = comp.pL_H
            pointR1 = comp.pR_H
            pointL2 = comp.pL_C
            pointR2 = comp.pR_C
            #pointL1.label = pointL1.label + '/' + pointR2.label
            #pointR2.label = pointL1.label
            #pointR1.label = pointR1.label + '/' + pointL2.label
            #pointL2.label = pointR1.label 
            pointL1.label = pointL1.label
            pointL2.label = pointL2.label 
            pointR1.label = pointR1.label
            pointR2.label = pointR2.label


    # create points string
    point_string = []
    for point in points.instances:
        point_string.append(point.label)

    # remove duplciates
    point_string = np.unique(np.array(point_string)).tolist()

    for comp in Components:

        if comp.type is 'RECUP':
            pointL1 = comp.pL_H; pointR1 = comp.pR_H
            pointL2 = comp.pL_C; pointR2 = comp.pR_C
            indexL1 = point_string.index(pointL1.label)
            indexR1 = point_string.index(pointR1.label)
            indexL2 = point_string.index(pointL2.label)
            indexR2 = point_string.index(pointR2.label)

            # plot Pressure line
            ax1.plot([indexL1, indexR1], [pointL1.PR/p_scale, pointR1.PL/p_scale], 'k:',markersize=10,markerfacecolor='k',fillstyle='full')   
            labelLine(ax1,plt.getp(ax1,'lines')[-1], 0.5*(indexL1 + indexR1) ,label=[comp.label+'(1)'],align=True,fontsize=8.)
            ax1.plot([indexL1], [pointL1.PR/p_scale], 'ko',markersize=10,markerfacecolor='k',fillstyle='full')   
            ax1.plot([indexR1], [pointR1.PL/p_scale], 'go',markersize=10,markerfacecolor='g',fillstyle='full')   

            ax1.plot([indexL2, indexR2], [pointL2.PR/p_scale, pointR2.PL/p_scale], 'ko:',markersize=10,markerfacecolor='k',fillstyle='full')    
            labelLine(ax1,plt.getp(ax1,'lines')[-1], 0.5*(indexL2 + indexR2) ,label=[comp.label+'(2)'],align=True,fontsize=8.)
            ax1.plot([indexL2], [pointL2.PR/p_scale], 'ko',markersize=10,markerfacecolor='k',fillstyle='full')   
            ax1.plot([indexR2], [pointR2.PL/p_scale], 'go',markersize=10,markerfacecolor='g',fillstyle='full')   

            # plot Temperature line
            ax2.plot([indexL1, indexR1], [pointL1.TR, pointR1.TL], 'ko:',markersize=10,markerfacecolor='k',fillstyle='full')    
            labelLine(ax2,plt.getp(ax2,'lines')[-1], 0.5*(indexL1+indexR1) ,label=[comp.label+'(1)'],align=True,fontsize=8.)
            ax2.plot([indexL1], [pointL1.TR], 'ko',markersize=10,markerfacecolor='k',fillstyle='full')   
            ax2.plot([indexR1], [pointR1.TL], 'go',markersize=10,markerfacecolor='g',fillstyle='full')   
            ax2.plot([indexL2, indexR2], [pointL2.TR, pointR2.TL], 'ko:',markersize=10,markerfacecolor='k',fillstyle='full')    
            labelLine(ax2,plt.getp(ax2,'lines')[-1], 0.5*(indexL2+indexR2) ,label=[comp.label+'(2)'],align=True,fontsize=8.)
            ax2.plot([indexL2], [pointL2.TR], 'ko',markersize=10,markerfacecolor='k',fillstyle='full')   
            ax2.plot([indexR2], [pointR2.TL], 'go',markersize=10,markerfacecolor='g',fillstyle='full')

        elif (comp.type=='HX'   or  
              comp.type=='TURB' or comp.type=='TURB_MAP_SP' or comp.type=='TURB_AX_SP' or comp.type=='TURB_ER' or comp.type=="TURB_POWER" or
              comp.type=='COMP' or comp.type=='COMP_MAP_SP' or comp.type=='COMP_MASSF' or comp.type=='COMP_POWER' or 
              comp.type=='PIPE' or comp.type=='EX_Orifice' or comp.type=='RAM_AIR_INTAKE'):
            pointL = comp.pL; pointR = comp.pR
            indexL = point_string.index(pointL.label)
            indexR = point_string.index(pointR.label)
            # plot Pressure line
            ax1.plot([indexL, indexR], [pointL.PR/p_scale, pointR.PL/p_scale], 'ko:',markersize=10,markerfacecolor='k',fillstyle='full') 
            labelLine(ax1,plt.getp(ax1,'lines')[-1], 0.5*(indexL + indexR) ,label=comp.label,align=True,fontsize=8.)
            ax1.plot([indexL], [pointL.PR/p_scale], 'ko',markersize=10,markerfacecolor='k',fillstyle='full')   
            ax1.plot([indexR], [pointR.PL/p_scale], 'go',markersize=10,markerfacecolor='g',fillstyle='full') 
            # plot Temperature line
            ax2.plot([indexL, indexR], [pointL.TR, pointR.TL], 'ko:',markersize=10,markerfacecolor='k',fillstyle='full')    
            labelLine(ax2,plt.getp(ax2,'lines')[-1], 0.5*(indexL+indexR) ,label=comp.label,align=True,fontsize=8.)
            ax2.plot([indexL], [pointL.TR], 'ko',markersize=10,markerfacecolor='k',fillstyle='full')   
            ax2.plot([indexR], [pointR.TL], 'go',markersize=10,markerfacecolor='g',fillstyle='full') 

        elif comp.type=='SPLIT':
            # 1st line
            pointL = comp.pL;  pointR = comp.pR0
            indexL = point_string.index(pointL.label)
            indexR = point_string.index(pointR.label)
            # plot Pressure line
            ax1.plot([indexL, indexR], [pointL.PR/p_scale, pointR.PL/p_scale], 'ko:',markersize=10,markerfacecolor='k',fillstyle='full') 
            labelLine(ax1,plt.getp(ax1,'lines')[-1], 0.5*(indexL + indexR) ,label=comp.label,align=True,fontsize=8.)
            ax1.plot([indexL], [pointL.PR/p_scale], 'ko',markersize=10,markerfacecolor='k',fillstyle='full')   
            ax1.plot([indexR], [pointR.PL/p_scale], 'go',markersize=10,markerfacecolor='g',fillstyle='full') 
            # plot Temperature line
            ax2.plot([indexL, indexR], [pointL.TR, pointR.TL], 'ko:',markersize=10,markerfacecolor='k',fillstyle='full')    
            labelLine(ax2,plt.getp(ax2,'lines')[-1], 0.5*(indexL+indexR) ,label=comp.label,align=True,fontsize=8.)
            ax2.plot([indexL], [pointL.TR], 'ko',markersize=10,markerfacecolor='k',fillstyle='full')   
            ax2.plot([indexR], [pointR.TL], 'go',markersize=10,markerfacecolor='g',fillstyle='full') 

            # 2nd line
            pointL = comp.pL;  pointR = comp.pR1
            indexL = point_string.index(pointL.label)
            indexR = point_string.index(pointR.label)
            # plot Pressure line
            ax1.plot([indexL, indexR], [pointL.PR/p_scale, pointR.PL/p_scale], 'ko:',markersize=10,markerfacecolor='k',fillstyle='full') 
            labelLine(ax1,plt.getp(ax1,'lines')[-1], 0.5*(indexL + indexR) ,label=comp.label,align=True,fontsize=8.)
            ax1.plot([indexL], [pointL.PR/p_scale], 'ko',markersize=10,markerfacecolor='k',fillstyle='full')   
            ax1.plot([indexR], [pointR.PL/p_scale], 'go',markersize=10,markerfacecolor='g',fillstyle='full') 
            # plot Temperature line
            ax2.plot([indexL, indexR], [pointL.TR, pointR.TL], 'ko:',markersize=10,markerfacecolor='k',fillstyle='full')    
            labelLine(ax2,plt.getp(ax2,'lines')[-1], 0.5*(indexL+indexR) ,label=comp.label,align=True,fontsize=8.)
            ax2.plot([indexL], [pointL.TR], 'ko',markersize=10,markerfacecolor='k',fillstyle='full')   
            ax2.plot([indexR], [pointR.TL], 'go',markersize=10,markerfacecolor='g',fillstyle='full') 

        elif comp.type=='MERGE':
            # 1st line
            pointL = comp.pL0;  pointR = comp.pR
            indexL = point_string.index(pointL.label)
            indexR = point_string.index(pointR.label)
            # plot Pressure line
            ax1.plot([indexL, indexR], [pointL.PR/p_scale, pointR.PL/p_scale], 'ko:',markersize=10,markerfacecolor='k',fillstyle='full') 
            labelLine(ax1,plt.getp(ax1,'lines')[-1], 0.5*(indexL + indexR) ,label=comp.label,align=True,fontsize=8.)
            ax1.plot([indexL], [pointL.PR/p_scale], 'ko',markersize=10,markerfacecolor='k',fillstyle='full')   
            ax1.plot([indexR], [pointR.PL/p_scale], 'go',markersize=10,markerfacecolor='g',fillstyle='full') 
            # plot Temperature line
            ax2.plot([indexL, indexR], [pointL.TR, pointR.TL], 'ko:',markersize=10,markerfacecolor='k',fillstyle='full')    
            labelLine(ax2,plt.getp(ax2,'lines')[-1], 0.5*(indexL+indexR) ,label=comp.label,align=True,fontsize=8.)
            ax2.plot([indexL], [pointL.TR], 'ko',markersize=10,markerfacecolor='k',fillstyle='full')   
            ax2.plot([indexR], [pointR.TL], 'go',markersize=10,markerfacecolor='g',fillstyle='full') 

            # 2nd line
            pointL = comp.pL1;  pointR = comp.pR
            indexL = point_string.index(pointL.label)
            indexR = point_string.index(pointR.label)
            # plot Pressure line
            ax1.plot([indexL, indexR], [pointL.PR/p_scale, pointR.PL/p_scale], 'ko:',markersize=10,markerfacecolor='k',fillstyle='full') 
            labelLine(ax1,plt.getp(ax1,'lines')[-1], 0.5*(indexL + indexR) ,label=comp.label,align=True,fontsize=8.)
            ax1.plot([indexL], [pointL.PR/p_scale], 'ko',markersize=10,markerfacecolor='k',fillstyle='full')   
            ax1.plot([indexR], [pointR.PL/p_scale], 'go',markersize=10,markerfacecolor='g',fillstyle='full') 
            # plot Temperature line
            ax2.plot([indexL, indexR], [pointL.TR, pointR.TL], 'ko:',markersize=10,markerfacecolor='k',fillstyle='full')    
            labelLine(ax2,plt.getp(ax2,'lines')[-1], 0.5*(indexL+indexR) ,label=comp.label,align=True,fontsize=8.)
            ax2.plot([indexL], [pointL.TR], 'ko',markersize=10,markerfacecolor='k',fillstyle='full')   
            ax2.plot([indexR], [pointR.TL], 'go',markersize=10,markerfacecolor='g',fillstyle='full') 

        elif comp.type=='ICE_SIMPLE':
            pointL = comp.pL; pointR = comp.pR
            indexL = point_string.index(pointL.label)
            indexR = point_string.index(pointR.label)
            # plot Pressure line
            ax1.plot([indexL, indexR], [pointL.PR/p_scale, pointR.PL/p_scale], 'ko:',markersize=10,markerfacecolor='k',fillstyle='full') 
            labelLine(ax1,plt.getp(ax1,'lines')[-1], 0.5*(indexL + indexR) ,label=comp.label,align=True,fontsize=8.)
            ax1.plot([indexL], [pointL.PR/p_scale], 'ko',markersize=10,markerfacecolor='k',fillstyle='full')   
            ax1.plot([indexR], [pointR.PL/p_scale], 'go',markersize=10,markerfacecolor='g',fillstyle='full') 
            # plot Temperature line
            ax2.plot([indexL, indexR], [pointL.TR, pointR.TL], 'ko:',markersize=10,markerfacecolor='k',fillstyle='full')    
            labelLine(ax2,plt.getp(ax2,'lines')[-1], 0.5*(indexL+indexR) ,label=comp.label,align=True,fontsize=8.)
            ax2.plot([indexL], [pointL.TR], 'ko',markersize=10,markerfacecolor='k',fillstyle='full')   
            ax2.plot([indexR], [pointR.TL], 'go',markersize=10,markerfacecolor='g',fillstyle='full') 


    # set labels
    if p_scale == 1.e3 or p_scale == 1e3 :
        ax1.set_ylabel('Pressure (kPa)',fontsize=20)
    elif p_scale == 1.e6 or p_scale == 1e6 :
        ax1.set_ylabel('Pressure (MPa)',fontsize=20)
    elif p_scale == 1. or p_scale == 1 : 
        ax1.set_ylabel('Pressure (Pa)',fontsize=20)
    else:
        ax1.set_ylabel('Pressure (Pa) / {0:.1f}'.format(p_scale),fontsize=20)
        
    ax1.set_title('Pressure at Points',fontsize=20)
    ax2.set_ylabel('Temperature (K)',fontsize=20)
    ax2.set_title('Temperature at Points',fontsize=20)
    for tick in ax1.get_yticklabels():
        tick.set_fontsize(16)
    for tick in ax2.get_yticklabels():
        tick.set_fontsize(16)
    plt.xticks(range(len(point_string)), point_string, fontsize=20)        
###
### 
def plot_compressor_map(Compressor):
    """
    function to plot maps for compressors in system and corresponding operating points
    """
    N = len(Compressor)
    if N > 0:
        f, AX = plt.subplots( N, 2, figsize=( 14,(6*N) ))
        #f.tight_layout()
        f.subplots_adjust(hspace=.2)
        
        for i in range(N):
            if N == 1:
                ax0 = AX[0]; ax1 = AX[1]
            else:
                ax0 = AX[i][0]; ax1 = AX[i][1]

            # AX[i].set_title('Compressor Map ' + Compressor[i].label)
            massflow, OP_speed, Pin, Tin, Pout, Tout, delta_h, eta=Compressor[i].OP_Point
            PR = Pout/Pin
            Compressor[i].COMP.plot_map_eq(MDOT=[massflow], SPEED=[OP_speed],
                                           DH=[delta_h], ETA=[eta], Tin=[Tin],
                                           Pin=[Pin], PR=[PR], STRING=[Compressor[i].label+' OP'],
                                           label=Compressor[i].label,
                                           AXIS=[ax0, ax1])
        plt.suptitle('COMPRESSOR MAPS (at equivalent conditions)', fontsize=20)

###
###
def plot_turbine_map(Turbine):
    """
    function to plot maps for turbines in system and corresponding operating points
    """
    N = len(Turbine)
    if N > 0:
        f, AX = plt.subplots( N, 2, figsize=(12,(6*N)))
        f.subplots_adjust(hspace=.4)
        f.subplots_adjust(wspace=.4)

        for i in range(N):
            if N == 1:
                ax0 = AX[0]; ax1 = AX[1]
            else:
                ax0 = AX[i][0]; ax1 = AX[i][1]

            #AX[i].set_title('Turbine Map ' + Turbine[i].label)            
            massflow, OP_speed, Pin, Tin, Pout, Tout, delta_h, eta, mfp, u_c, er = Turbine[i].OP_Point
            Turbine[i].TURB.plot_map_eq(MFP=[mfp], U_C=[u_c], ER=[er], ETA=[eta], 
                                            Tin=[Tin], Pin=[Pin], STRING=[Turbine[i].label+' OP'], 
                                            label=Turbine[i].label, AXIS =[ax0, ax1], N_turb=[OP_speed])
        plt.suptitle('TURBINE MAPS (at equivalent conditions)',fontsize=20)
###
###
def plot_TS(points,components,fluid):
    """
    function to plot cycle on T-s diagram
    """
    # create plot of pressure along different locations
    f, ax1 = plt.subplots( 1, figsize=(11,8))
    
    # create points string and find min/max temp and min/max entropy
    point_string = []
    TT = []; PP = []; SS = []; DD = []; HH = []
    for point in points.instances:
        point_string.append(point.label)
        T = 0.5* (point.TL + point.TR)
        P = 0.5* (point.PL + point.PR)
        TT.append(T)
        PP.append(P)
        if point.fluid == fluid:
            SS.append( CP.PropsSI('SMASS','P',P,'T', T ,point.fluid) ) 
            DD.append( CP.PropsSI('D','P',P,'T', T ,point.fluid) )
            HH.append( CP.PropsSI('HMASS','P',P,'T', T ,point.fluid) )
    Tmin = min(TT); Tmax = max(TT); deltaT = Tmax-Tmin
    Pmin = min(PP); Pmax = max(PP); #deltaT = Tmax-Tmin
    Smin = min(SS); Smax = max(SS); deltaS = Smax-Smin
    Dmin = min(DD); Dmax = max(DD); #deltaD = Dmax-Dmin
    Hmin = min(SS); Hmax = max(HH); #deltaH = Hmax-Hmin
    
    ts_plot = PropertyPlot(fluid, 'TS', axis=ax1, unit_system='SI') # tp_limits='ORC')
    #ts_plot.set_axis_limits([1000., 4000., 273., 900.])
    ts_plot.set_axis_limits([Smin-0.1*deltaS, Smax+0.1*deltaS, Tmin-0.1*deltaT, Tmax+0.1*deltaT])

    ts_plot.calc_isolines(CoolProp.iQ, num=2)
    ts_plot.calc_isolines(CoolProp.iP, iso_range=[8.e6, 20.e6],num=13)
    ts_plot.calc_isolines(CoolProp.iHmass, iso_range=[250.e3, 2000.e3],num=13)
    ts_plot.calc_isolines(CoolProp.iDmass, iso_range=[1., 1000.],num=25)

    ts_plot.draw()   

    x_label = 'SMASS'
    y_label = 'T'

    for comp in components:
        if comp.type is 'RECUP':

            fluid_H = comp.fluid_H
            if fluid_H == fluid:
                P = comp.PH_trace
                T = comp.TH_trace
                x = []; y= []
                for i in range(len(P)):
                    x.append(CP.PropsSI(x_label,'P',P[i],'T', T[i] ,fluid_H))
                    y.append(CP.PropsSI(y_label,'P',P[i],'T', T[i] ,fluid_H))

                ax1.plot(x, y, ':',markersize=10,markerfacecolor='k',fillstyle='full')  
                labelLine(ax1,plt.getp(ax1,'lines')[-1], 0.5*(x[0] + x[-1]) ,label=comp.label+'(1)',align=True)  
                # also plot end point
                ax1.plot([x[0], x[-1]], [y[0], y[-1]], 'ko',markersize=10,markerfacecolor='k',fillstyle='full')  

            fluid_C = comp.fluid_C
            if fluid_C == fluid:
                P = comp.PC_trace
                T = comp.TC_trace

                x = []; y= []
                for i in range(len(P)):
                    x.append(CP.PropsSI(x_label,'P',P[i],'T', T[i] ,fluid_C))
                    y.append(CP.PropsSI(y_label,'P',P[i],'T', T[i] ,fluid_C))

                ax1.plot(x, y, ':',markersize=10,markerfacecolor='k',fillstyle='full')  
                labelLine(ax1,plt.getp(ax1,'lines')[-1], 0.5*(x[0] + x[-1]) ,label=comp.label+'(2)',align=True) 
                # also plot end point
                ax1.plot([x[0], x[-1]], [y[0], y[-1]], 'ko',markersize=10,markerfacecolor='k',fillstyle='full')  
        ###
        elif comp.type is "RECUP_HALF_SAM":
            
            P = comp.PH_trace
            T = comp.TH_trace

            x = []; y= []
            for i in range(len(P)):
                x.append(CP.PropsSI(x_label,'P',P[i],'T', T[i] ,fluid))
                y.append(CP.PropsSI(y_label,'P',P[i],'T', T[i] ,fluid))
            ax1.plot(x, y, ':',markersize=10,markerfacecolor='k',fillstyle='full')  
            labelLine(ax1,plt.getp(ax1,'lines')[-1], 0.5*(x[0] + x[-1]) ,label=comp.label+'(1)',align=True)  
            # also plot end point
            ax1.plot([x[0], x[-1]], [y[0], y[-1]], 'ko',markersize=10,markerfacecolor='k',fillstyle='full')  

            P = comp.PC_trace
            T = comp.TC_trace

            x = []; y= []
            for i in range(len(P)):
                x.append(CP.PropsSI(x_label,'P',P[i],'T', T[i] ,fluid))
                y.append(CP.PropsSI(y_label,'P',P[i],'T', T[i] ,fluid))
            ax1.plot(x, y, ':',markersize=10,markerfacecolor='k',fillstyle='full')  
            labelLine(ax1,plt.getp(ax1,'lines')[-1], 0.5*(x[0] + x[-1]) ,label=comp.label+'(2)',align=True) 
            # also plot end point
            ax1.plot([x[0], x[-1]], [y[0], y[-1]], 'ko',markersize=10,markerfacecolor='k',fillstyle='full')

        elif comp.type is 'MERGE':
            pointL = comp.pL0;  pointR = comp.pR
            indexL = point_string.index(pointL.label)
            indexR = point_string.index(pointR.label)
            
            x = [comp.pL0.PR, comp.pR.PR, comp.pL1.PR]
            y = [comp.pL0.TR, comp.pR.TR, comp.pL1.TR]

                        
            ax1.plot(x, y, ':',markersize=10,markerfacecolor='k',fillstyle='full')  
            # labelLine(ax1,plt.getp(ax1,'lines')[-1], 0.5*(0.5*(x[0]+x[2]) + x[1]) ,label=comp.label,align=True) 
            # also plot end point
            ax1.plot(x, y, 'ko',markersize=10,markerfacecolor='k',fillstyle='full')  
        ###
        elif comp.type is 'HX':
            P = comp.P_trace
            T = comp.T_trace

            x = []; y= []
            for i in range(len(P)):
                x.append(CP.PropsSI(x_label,'P',P[i],'T', T[i] ,fluid))
                y.append(CP.PropsSI(y_label,'P',P[i],'T', T[i] ,fluid))
            ax1.plot(x, y, ':',markersize=10,markerfacecolor='k',fillstyle='full')  
            labelLine(ax1,plt.getp(ax1,'lines')[-1], 0.5*(x[0] + x[-1]) ,label=comp.label,align=True) 
            # also plot end point
            ax1.plot([x[0], x[-1]], [y[0], y[-1]], 'ko',markersize=10,markerfacecolor='k',fillstyle='full')            
        ###      
        else:
            pointL1 = comp.pL; pointR1 = comp.pR

            # plot Line 1
            if fluid == fluid:
                x1 = CP.PropsSI(x_label,'P',pointL1.PR,'T', pointL1.TR ,fluid)
                y1 = CP.PropsSI(y_label,'P',pointL1.PR,'T', pointL1.TR ,fluid)
                x2 = CP.PropsSI(x_label,'P',pointR1.PL,'T', pointR1.TL ,fluid)
                y2 = CP.PropsSI(y_label,'P',pointR1.PL,'T', pointR1.TL ,fluid)

            ax1.plot([x1, x2], [y1, y2], 'ko:',markersize=10,markerfacecolor='k',fillstyle='full')  
            labelLine(ax1,plt.getp(ax1,'lines')[-1], 0.5*(x1 + x2) ,label=comp.label,align=True) 


    # set labels
    ax1.set_ylabel('Temperature (K)',fontsize=20)
    #ax1.set_title('Pressure at Points')
    ax1.set_xlabel('Specific Entropy (kJ/kg/K',fontsize=20)
    for tick in ax1.get_yticklabels():
        tick.set_fontsize(16)
    for tick in ax1.get_xticklabels():
        tick.set_fontsize(16)
    ax1.set_title('TS-diagram, Working Fluid: '+fluid,fontsize=25) 


###
###
def labelLine(ax,line,x,label=None,align=True,**kwargs):
    # ax = plt.getp(line,'axes')
    xdata = plt.getp(line,'xdata')
    ydata = plt.getp(line,'ydata')

    # xdata = sorted(xdata) # reorder to allow lines that go in negative x direction
    if (x < xdata[0] and x < xdata[-1]) or (x > xdata[0] and x > xdata[-1]):
        print('x label location is outside data range!')
        return

    #Find corresponding y co-ordinate and angle of the
    ip = 1
    if xdata[-1] > xdata[0]:
        direction = 'forward'
    else:
        direction = 'backward'

    for i in range(len(xdata)):
        if direction == 'forward':
            if x < xdata[i]:
                ip = i
                break
        elif direction == 'backward':
            if x > xdata[i]:
                ip = i
                break
    #print ip
    #print x, xdata
    #print ydata

    y = ydata[ip-1] + (ydata[ip]-ydata[ip-1])*(x-xdata[ip-1])/(xdata[ip]-xdata[ip-1])

    #print y

    if not label:
        label = plt.getp(line,'label')

    if align:
        #Compute the slope
        dx = xdata[ip] - xdata[ip-1]
        dy = ydata[ip] - ydata[ip-1]
        ang = degrees(atan2(dy,dx))
        # rotate so that always text up
        if ang < -90:
            ang = ang+180.
        elif ang > 90:
            ang = ang-180.
        #Transform to screen co-ordinates
        pt = np.array([x,y]).reshape((1,2))
        trans_angle = ax.transData.transform_angles(np.array((ang,)),pt)[0]
    else:
        trans_angle = 0

    #Set a bunch of keyword arguments
    if 'color' not in kwargs:
        kwargs['color'] = plt.getp(line,'color')
    if ('horizontalalignment' not in kwargs) and ('ha' not in kwargs):
        kwargs['ha'] = 'center'
    if ('verticalalignment' not in kwargs) and ('va' not in kwargs):
        kwargs['va'] = 'center'
    #if 'backgroundcolor' not in kwargs:
    #    kwargs['backgroundcolor'] = plt.getp(ax,'facecolor')
    if 'clip_on' not in kwargs:
        kwargs['clip_on'] = True
    if 'zorder' not in kwargs:
        kwargs['zorder'] = 2.5

    ax.text(x,y,label,rotation=trans_angle,**kwargs) 

###
###
    
