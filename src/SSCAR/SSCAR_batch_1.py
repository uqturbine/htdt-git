#! /usr/bin/env python3
"""
Function to batch process Cases using SSCAR

Step 1 in an overall process

Last modified: 2018/7/26
Authors: Ingo Jahn
"""

import numpy as np
import sys as sys 
import os as os 
import shutil as shutil
from getopt import getopt


class input_data:
    def __init__(self):
        self.varNames = []
        self.varUnits = []
        self.varValues = []
    ###
    def check(self):
        # check that all variables have been supplied
        if not isinstance(self.varNames,list):
            raise MyError("Required input variable 'varNames' must be a list")
        if not isinstance(self.varUnits,list):
            raise MyError("Required input variable 'varUnits' must be a list")
        if not isinstance(self.varValues,list):
            raise MyError("Required input variable 'varValues' must be a list")

        # check that have at least one entry
        if len(self.varNames) == 0:
            raise MyError("Required input list 'varNames' must have at last one entry")
        if len(self.varUnits) == 0:
            raise MyError("Required input list 'varUnits' must have at last one entry")
        if len(self.varValues) == 0:
            raise MyError("Required input list 'varValues' must have at last one entry")

        # check that inputs have correct length.
        if len(self.varNames) != len(self.varUnits) or len(self.varNames) != len(self.varValues):
            raise MyError('The dimensions of varNames [{0}], varUnits [{1}], and varValues [{2}] are not identical'.format(len(self.varNames), len(self.varUnits), len(self.varValues) ))



def main(uODict):
    # function to create data file
   
    # Use the following lists to define the variables that you plan to alter
    varNames = []
    varUnits = []
    varValues = []

    # load jobfile 
    jobFileName = uoDict.get("--jobfile", "test-j")

    # load output file  
    dataFileName = uoDict.get("--datafile", "test-o")

    # add .py extension if forgotten
    if not ".py" in jobFileName:
    	jobFileName = ''.join([jobFileName, ".py"])

    # Trying to execute from a relative directory, not supported
    if os.sep in jobFileName:
    	relative_dir = jobFileName.split(os.sep)[:-1]
    	abs_dir = os.path.abspath(os.path.join(*relative_dir))
    	jobName = jobFileName.split(os.sep)[-1]
    	raise MyError(("SSCAR does not currently support relative imports. "
    		"Please change directory to {0} and retry with command: \n\n "
    		"$ SSCAR_batch1.py --job={1} \n"
    		.format(abs_dir, jobName)))

    # Make sure that the desired job file actually exists
    if not jobFileName in os.listdir('.'):
    	local_directory = os.path.dirname(os.path.realpath(__file__))
    	raise MyError("No job file {0} found in directory: {1}"
    		.format(jobFileName, local_directory))

    # create input data class
    II=input_data()

    # Execute jobFile, this creates all the variables
    exec(open(jobFileName).read(),globals(),locals())
  



    if 0: # option to hard-code variables
        ##########################################        
        # Add your code to define Variables here #
        ##########################################
        # Note Currently only lists with 1,2 or 3 variables are supported


        II.varNames.append('MassFlowRate')  # Variable Names cannot have spaces
        II.varUnits.append('kg/s')
        #i.varValues.append(np.mgrid(0.1,2.5,7j))
        II.varValues.append(np.arange(8.,12.,2.))


        II.varNames.append('PR')   # Variable Names cannot have spaces
        II.varUnits.append('-')
        II.varValues.append(np.arange(1.8,2.8,0.4)) 

        II.varNames.append('T_source')   # Variable Names cannot have spaces
        II.varUnits.append('K')
        II.varValues.append(np.arange(300.,400.,100.)) 

        ##########################################

    # check input data
    II.check()

    
    # create data file
    with open(dataFileName,'w') as fp:
        # write Header
        line = "#"
        for i in range(len(II.varNames)):
            line = line + " {0}:{1}:[{2}]".format(i,II.varNames[i],II.varUnits[i])
        line = line + "\n"            
        fp.write(line)

        if len(II.varNames) == 1:
            for var0 in II.varValues[0]:
                fp.write("{0:.6f} \n".format(var0))

        elif len(II.varNames) == 2:
            for var0 in II.varValues[0]:
                for var1 in II.varValues[1]:
                    fp.write("{0:.6f} {1:.6f} \n".format(var0, var1))
 
        elif len(II.varNames) == 3:
            for var0 in II.varValues[0]:
                for var1 in II.varValues[1]:
                    for var2 in II.varValues[2]:
                        fp.write("{0:.6f} {1:.6f} {2:.6f} \n".format(var0, var1, var2))
                        
    print("    Data written to file: {0}".format(dataFileName))
    
    return 0



shortOptions = ""
longOptions = ["help", "verbosity=", "jobfile=", "datafile=", "test"]
###
###
def printUsage():
    print("")
    print("Usage: SSCAR_batch_1.py [--jobfile=<FileName>] [--datafile=<FileName>]")
    print("                [--help] [--verbosity=0 (1 or 2)] ")
    return
###
###
class MyError(Exception):
    def __init__(self, value):
        self.value = value
    def __str__(self):
        return repr(self.value)
###
###
if __name__ == "__main__":
    userOptions = getopt(sys.argv[1:], shortOptions, longOptions)
    uoDict = dict(userOptions[0])
    
    #if len(userOptions[0]) == 0 or "--help" in uoDict:
    #    printUsage()
    #    sys.exit(1)

    # execute the code
    try:
        print("START: SSCAR_batch_1.py")
        main(uoDict)
        print("  SUCESS.")
        print("COMPLETE: SSCAR_batch_1.py")
        print("\n \n")

    except MyError as e:
        print("This run has gone bad.")
        print(e.value)
        sys.exit(1)
