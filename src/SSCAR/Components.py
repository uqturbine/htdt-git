#! /usr/bin/env python3
"""
Module that stores all the Component classes used by Cycle.py.

Follwoing Components are available:
HX - Heat exchanger that can be used as constant temperature source/sink

RECUP - Recuperator that models co- and counter-flow heat exchange

TURB - Turbine - based on turbine map

COMP - Compressor - based on compressor map

SPLIT - Split flow into two branches

MERGE - bring together flow from two branches


Author: Ingo Jahn
Last Modified: 19/03/2017
"""

from HX_fluids import *
import numpy as np
import CoolProp
import CoolProp.CoolProp as CP
import scipy as sci
from scipy.integrate import quad
import matplotlib.pyplot as plt
import os as os
import HX_solver as hx
import Comp_funct as compf
import Turbine_funct as turbf


###
class HX:
    instances = []
    def __init__(self,hxtype,T,pL,pR,M,Volume=0., label='HX',A=[], L=[], D=[], N_channel=1 , epsilon=0. ):
        self.__class__.instances.append(self)
        self.type = 'HX'
        self.hxtype = hxtype
        self.hxtypelist = ['source','sink']
        self.T = T
        self.pL = pL
        self.pR = pR
        self.label = label
        self.Volume = Volume
        self.A = A
        self.L = L
        self.D = D
        self.N_channel = N_channel
        self.epsilon = epsilon
        self.M = M
        self.DH = []
        self.set_MASS()
        self.check_inputs()
    ###
    def check_inputs(self):
        if not (self.hxtype is 'source' or self.hxtype is 'sink'):
            raise MyError("Only types 'source' and 'sink' are supported. Define differnt HX")
        if (isinstance(self.A,float) and not isinstance(self.L,float)) or (not isinstance(self.A,float) and isinstance(self.L,float)):
            # insufficient parameters for friction loss calculation
            print('HX: self.A =', self.A)
            print('HX: self.L =', self.L)
            raise MyError("For friction loss case both A and L need to be defined as floats. ")
    ###
    def set_MASS(self):
        self.MASS = 0.
        for m in self.M:
            self.MASS = self.MASS + m.mass
    ###
    def set_fluid(self):
        M=self.M[0]
        self.fluid = M.fluid
        self.pL.fluid = M.fluid
        self.pR.fluid = M.fluid
    ###
    def evalR(self):
        self.set_MASS()
        # adjust mass flow based on massfraction massf
        massflow = self.MASS
        #print('HX', massflow)
        # evaluate conditions at other side
        #PL,TL = self.pL.getR()
        PL,HL = self.pL.getR()
        # print('Here ', self.pL.label, PL,TL)
        if isinstance(self.A,float) == 0:
            # lossless HX without pressure decreas
            PR = PL
            #TR = self.T
            HR = CP.PropsSI('HMASS','P',PR,'T',self.T,self.fluid)
        else:
            # calculate pressure loss
            #PL,TL  = self.pL.getR()
            #PR,TR  = self.pR.getR()
            #rhoL = CP.PropsSI('D','P',PL,'T', TL ,self.fluid)
            #rhoR = CP.PropsSI('D','P',PR,'T', TR ,self.fluid)
            #muL = CP.PropsSI('VISCOSITY','P',PL,'T', TL ,self.fluid)
            #muR = CP.PropsSI('VISCOSITY','P',PR,'T', TR ,self.fluid)
            PL,HL  = self.pL.getR()
            PR,HR  = self.pR.getR()
            rhoL = CP.PropsSI('D','P',PL,'HMASS', HL ,self.fluid)
            rhoR = CP.PropsSI('D','P',PR,'HMASS', HR ,self.fluid)
            muL = CP.PropsSI('VISCOSITY','P',PL,'T', TL ,self.fluid)
            muR = CP.PropsSI('VISCOSITY','P',PR,'T', TR ,self.fluid)

            rho = 0.5 * (rhoL+rhoR)
            mu = 0.5 * (muL+muR)
            V = (massflow / self.N_channel) / (self.A * rho)
            if isinstance(self.D,float) == 0: # if D no defined, calculate from circle.
                self.D = 2* (self.A**0.5)/np.pi
            Re = rho*V*self.D/mu
            if Re < 2300.:
                f = 64./Re
            else:
                temp = 1.8* np.log10((self.epsilon/self.D / 3.7)**1.11 + 6.9/Re)
                f = temp**-2
            Delta_P = f * self.L/self.D * V**2 / (2.*9.81) * rho
            # set downstream conditions
            PR = PL - Delta_P
            #TR = self.T
            HR = CP.PropsSI('HMASS','P',PR,'T',self.T,self.fluid)

        # set condition at downstream point
        #self.pR.setL(PR,TR)
        self.pR.setL(PR,HR)

        # calculate enthalpy change
        #print("HX-in", PL, TL)
        #print("HX-out", PR,TR)
        #hin = CP.PropsSI('HMASS','P',PL,'T', TL ,self.fluid)
        #hout = CP.PropsSI('HMASS','P',PR,'T', TR ,self.fluid)
        #self.OP_Point = [massflow, 'nan', PL, TL, PR, TR, hout-hin]
        #self.DH = massflow * (hout-hin)
        self.OP_Point = [massflow, 'nan', PL, 'nan', PR, 'nan', HR-HL]
        self.DH = massflow * (HR-HL)

        # define Temperature and Pressure Traces for plotting
        #self.T_trace = np.linspace(TL,self.T,30)
        self.P_trace = np.linspace(PL,PR,30)
        self.T_trace = []
        self.H_trace = np.linspace(HL,HR,30)
        for i in range(len(self.H_trace)):
            self.T_trace.append(CP.PropsSI('T','P',self.P_trace[i],'HMASS',self.H_trace[i],self.fluid))
    ###
    def calc_mass(self):
        # function to return mass of working fluid stored in components
        # get inlet conditions
        P,H  = self.pL.getR()
        rhoL = CP.PropsSI('D','P',P,'HMASS', H ,self.fluid)
        P,H  = self.pR.getR()
        rhoR = CP.PropsSI('D','P',P,'HMASS', H ,self.fluid)
        self.mass = 0.5*(rhoL + rhoR)*self.Volume
    ###
    def calc_cost(self):
        # Cost function retrieved from https://www.sciencedirect.com/science/article/pii/S0360544218302305?via%3Dihub
        LMTD = 10
        # Default LMTD - Cannot be calculated due to no temperature change in hot / cold stream

        if ('sink' and 'air') in self.label:
            # Gas cooler (air and sCO2) plate heat exchanger
            factor = 36
        else:
            # PCHX heat exchanger cost factor
            factor = 2500

        self.cost = (factor*(abs(self.OP_Point[6]*self.MASS)/1000))/LMTD
    ###
    def write(self,fp):
        '''
        function to write data to output file
        '''
        fp.write("TYPE = HX \n")
        fp.write("Label = "+self.label+"\n")
        fp.write("Fluid Type = "+self.fluid+"\n")
        fp.write("Point pL = "+self.pL.label+"\n")
        fp.write("Point pR = "+self.pR.label+" \n")
        fp.write("q (J/kg) = {0:0.8f} \n".format(self.OP_Point[6]))
        fp.write("Mass Flow Rate (kg/s) = {0:0.8f} \n".format(self.MASS))
        fp.write("Fluid mass (kg) = {0:0.8f} \n".format(self.mass))
        fp.write("Cost ($) = {0:0.2f} \n".format(self.cost))
        fp.write("--- \n")
###
###
class RECUP:
    instances = []
    def __init__(self,fname,pL_C,pR_C,pL_H,pR_H,M_C, M_H, label='RECUP',use_T0=0,Volume_H=0.,Volume_C=0.):
        self.__class__.instances.append(self)
        self.type = 'RECUP'
        self.fname  = fname
        self.pL_H = pL_H
        self.pR_H = pR_H
        self.pL_C = pL_C
        self.pR_C = pR_C
        self.label = label
        self.M_H = M_H
        self.M_C = M_C
        self.T0 = []
        self.OP_Point = []
        self.use_T0 = use_T0
        self.Volume_H = Volume_H
        self.Volume_C = Volume_C
        self.DH = []
        self.set_MASS()
    ###
    def set_MASS(self):
        self.MASS_H = 0.
        self.MASS_C = 0.
        for m in self.M_H:
            self.MASS_H = self.MASS_H + m.mass
        for m in self.M_C:
            self.MASS_C = self.MASS_C + m.mass
    ###
    def set_fluid(self):
        self.fluid_H = self.M_H[0].fluid
        self.fluid_C = self.M_C[0].fluid
        self.pL_H.fluid = self.M_H[0].fluid
        self.pR_H.fluid = self.M_H[0].fluid
        self.pL_C.fluid = self.M_C[0].fluid
        self.pR_C.fluid = self.M_C[0].fluid
    ###
    def evalR(self):
        self.set_MASS()
        # adjust mass flow based on massfraction massf
        massflow_H = self.MASS_H
        massflow_C = self.MASS_C
        # evaluate conditions at other side and set into node on right
        # adjust HX job file

        #PC_in,TC_in  = self.pL_C.getR()
        #PH_in,TH_in  = self.pL_H.getR()
        PC_in,hC_in  = self.pL_C.getR()
        PH_in,hH_in  = self.pL_H.getR()

        new_run = 1
        if len(self.OP_Point) > 0:
            # compare to old conditions
            massflow_old_C, a, PC_in_old, TC_in_old, PC_out_out, TC_out_old, dh_C, PH_in_old, TH_in_old, PH_out_old, TH_out_old, dh_H, massflow_old_H,hC_in_old,hH_in_old = self.OP_Point

            #if (massflow_old_H == massflow_H and massflow_old_C == massflow_C and
            #    PC_in_old == PC_in and TC_in_old == TC_in and
            #    PH_in_old == PH_in and TH_in_old == TH_in):
            if (massflow_old_H == massflow_H and massflow_old_C == massflow_C and
                PC_in_old == PC_in and hC_in_old == hC_in and
                PH_in_old == PH_in and hH_in_old == hH_in):

                # same conditions as in previous evaluation.
                new_run = 0

        if new_run == 1:
            # run HX_solver
            # extrat data from current file and replace lines as necessary
            lines = []; label_flag=0
            with open(self.fname,'r') as infile:
                for line in infile:
                    if "F.fluidH" in line:
                        line = "F.fluidH = '"+self.fluid_H+"' \n"
                    if 'F.fluidC' in line:
                        line = "F.fluidC = '"+self.fluid_C+"'\n"
                    if 'F.hC_in' in line:
                        line = 'F.hC_in = '+str(hC_in)+'\n'
                    if 'F.hH_in' in line:
                        line = 'F.hH_in = '+str(hH_in)+'\n'
                    #if 'F.TC_in' in line:
                    #    line = 'F.TC_in = '+str(TC_in)+'\n'
                    #if 'F.TH_in' in line:
                    #    line = 'F.TH_in = '+str(TH_in)+'\n'
                    if 'F.PC_in' in line:
                        line = 'F.PC_in = '+str(PC_in)+'\n'
                    if 'F.PC_out' in line:
                        line = 'F.PC_out = '+str(PC_in)+'\n'
                    if 'F.PH_in' in line:
                        line = 'F.PH_in = '+str(PH_in)+'\n'
                    if 'F.PH_out' in line:
                        line = 'F.PH_out = '+str(PH_in)+'\n'
                    if 'F.mdotC' in line:
                        line = 'F.mdotC = '+str(massflow_C)+'\n'
                    if 'F.mdotH' in line:
                        line = 'F.mdotH = '+str(massflow_H)+'\n'
                    if 'F.T0' in line:
                        if len(self.T0) > 0:
                            string = 'F.T0 = ['
                            for item in self.T0:
                                string = string + (' %s,' % item)
                            string = string[:-1]; string = string + '] \n'
                            if self.use_T0 == 1: # check if old data is to be used
                                line = string
                            else:
                                line = 'F.T0 = [ ] \n'
                    if 'M.label' in line:
                        line = "M.line = '"+self.label+"'\n"
                        label_flag = 1
                    lines.append(line)

                #print(label_flag)
                #if label_flag == 0: # add label if not already exists
                #    lines.append("M.line = '"+self.label+"'\n")

            # write lines to new output file.
            with open(self.fname,'w') as outfile:
                for line in lines:
                    outfile.write(line)

            # call main function directly
            uoDict = {'--verbosity': '0', '--noplot': '', '--job': self.fname}
            PH_out, TH_out, PC_out, TC_out, PH, TH, PC, TC, T, hH_in, hH_out, hC_in, hC_out = hx.main(uoDict)
            #print('Recup', TH_out, PH_out, TC_out, PC_out)

            #hin = CP.PropsSI('HMASS','P',PC_in,'T', TC_in ,self.fluid_C)
            #hout = CP.PropsSI('HMASS','P',PC_out,'T', TC_out ,self.fluid_C)
            dh_C = hC_out-hC_in
            #hin = CP.PropsSI('HMASS','P',PH_in,'T', TH_in ,self.fluid_H)
            #hout = CP.PropsSI('HMASS','P',PH_out,'T', TH_out ,self.fluid_H)
            dh_H = hH_out-hH_in

            self.OP_Point = [massflow_C, 'nan', PC_in, 'nan', PC_out, TC_out, dh_C, PH_in, 'nan', PH_out, TH_out, dh_H, massflow_H,hC_in,hH_in]
            self.DH = dh_C*massflow_C
            self.TH_trace = TH
            self.TC_trace = TC
            self.T_pinch = min(abs(TH-TC))
            print("HX", self.label,"Delat T, pinch:",self.T_pinch,"[K]")
            self.PH_trace = PH
            self.PC_trace = PC
            self.T0 = T
            # adjust points on right
            #print(self.pR_H.label)
            #print(self.pR_C.label)

            #self.pR_H.setL(PH_out,TH_out)
            #self.pR_C.setL(PC_out,TC_out)
            self.pR_H.setL(PH_out,hH_out)
            self.pR_C.setL(PC_out,hC_out)
    ###
    def calc_mass(self):
        # function to return mass of working fluid stored in components
        # get inlet conditions
        # H-side
        if self.fluid_H.split(':')[0].upper() != 'CUSTOM':
            P,H  = self.pL_H.getR()
            rhoL = CP.PropsSI('D','P',P,'HMASS', H ,self.fluid_H)
            P,H  = self.pR_H.getR()
            rhoR = CP.PropsSI('D','P',P,'HMASS', H ,self.fluid_H)
            self.mass_H = 0.5*(rhoL + rhoR)*self.Volume_H
        else:
            self.mass_H = 0.
        # C-side
        if self.fluid_C.split(':')[0].upper() != 'CUSTOM':
            P,H  = self.pL_C.getR()
            rhoL = CP.PropsSI('D','P',P,'HMASS', H ,self.fluid_C)
            P,H  = self.pR_C.getR()
            rhoR = CP.PropsSI('D','P',P,'HMASS', H ,self.fluid_C)
            self.mass_C = 0.5*(rhoL + rhoR)*self.Volume_C
        else:
            self.mass_C = 0.
        self.mass = self.mass_C + self.mass_H
    ###
    def calc_cost(self):
        # Cost function retrieved from https://www.sciencedirect.com/science/article/pii/S0360544218302305?via%3Dihub
        # N and S used to denote north and south
        # It is assumed that the hot stream flows south to north
        # and the cool stream flows north to south.
        # Thus, for the LMTD, use delta_T between fluids on sides
        # N and S respectively.
        
        TH_N = (self.pR_H.TL + self.pR_H.TR)/2
        TC_S = (self.pR_C.TL + self.pR_C.TR)/2
        
        props = [CP.iT]
        
        H = ((self.pR_H.HL + self.pR_H.HR)/2) - self.OP_Point[11]
        h1 = H
        if not self.pR_H.Eos:
            if self.pR_H.fluid.split(':')[0].upper() == 'CUSTOM':
                self.pR_H.Eos = CustomFluid(self.pR_H.fluid.split(':')[1])
            else:
                self.pR_H.Eos = CP.AbstractState('HEOS',self.pR_H.fluid)

        state = [CP.HmassP_INPUTS,H,self.pR_H.PR]
        
        TH_S = GetFluidPropLow(self.pR_H.Eos,state,props)

        
        H = ((self.pR_C.HL + self.pR_C.HR)/2) - self.OP_Point[6]
        h2 = H
        if not self.pR_C.Eos:
            if self.pR_C.fluid.split(':')[0].upper() == 'CUSTOM':
                self.pR_C.Eos = CustomFluid(self.pR_C.fluid.split(':')[1])
            else:
                self.pR_C.Eos = CP.AbstractState('HEOS',self.pR_C.fluid)

        state = [CP.HmassP_INPUTS,H,self.pR_C.PR]
        
        TC_N = GetFluidPropLow(self.pR_C.Eos,state,props)

        try:
            LMTD = (abs(TH_N - TC_N) - abs(TH_S - TC_S))/\
                   (np.log(abs(TH_N - TC_N)/abs(TH_S - TC_S)))
        except:
            LMTD = 40
            print('LMTD set to 40 due to value error.')
        
        if ('sink' and 'air') in self.label:
            # Gas cooler (air and sCO2) plate heat exchanger
            factor = 36
        else:
            # PCHX heat exchanger cost factor
            factor = 2500
        
        self.cost = (factor*(abs(self.DH)/1000))/LMTD
    ###
    def write(self,fp):
        '''
        function to write data to output file
        '''
        fp.write("TYPE = RECUP \n")
        fp.write("Label = "+self.label+"\n")
        fp.write("Point pL_H = "+self.pL_H.label+"\n")
        fp.write("Point pR_H = "+self.pR_H.label+" \n")
        fp.write("q_H (J/kg) = {0:0.8f} \n".format(self.OP_Point[11]))
        fp.write("Mass Flow Rate_H (kg/s) = {0:0.8f} \n".format(self.MASS_H))
        fp.write("Fluid mass_H (kg) = {0:0.8f} \n".format(self.mass_H))
        fp.write("Point pL_C = "+self.pL_C.label+"\n")
        fp.write("Point pR_C = "+self.pR_C.label+" \n")
        fp.write("q_C (J/kg) = {0:0.8f} \n".format(self.OP_Point[6]))
        fp.write("Mass Flow Rate_C (kg/s) = {0:0.8f} \n".format(self.MASS_C))
        fp.write("Fluid mass_C (kg) = {0:0.8f} \n".format(self.mass_C))
        fp.write("Cost ($) = {0:0.2f} \n".format(self.cost))
        fp.write("--- \n")
###
class RECUP_HALF:
    instances = []
    def __init__(self,fname,pL_C,pR_C,M_C, label='RECUP_HALF',use_T0=0):
        self.__class__.instances.append(self)
        self.type = 'RECUP_HALF'
        self.fname  = fname
        self.pL_C = pL_C
        self.pR_C = pR_C
        self.label = label
        self.M_C = M_C
        self.T0 = []
        self.mass = []
        self.OP_Point = []
        self.use_T0 = use_T0
        self.DH = []
        self.set_MASS()
    ###
    def set_MASS(self):
        self.MASS_C = 0.
        for m in self.M_C:
            self.MASS_C = self.MASS_C + m.mass
    ###
    def set_fluid(self):
        self.fluid_C = self.M_C[0].fluid
        self.pL_C.fluid = self.M_C[0].fluid
        self.pR_C.fluid = self.M_C[0].fluid
    ###
    def evalR(self):
        self.set_MASS()
        # adjust mass flow based on massfraction massf
        massflow_C = self.MASS_C
        # evaluate conditions at other side and set into node on right
        # adjust HX job file

        PC_in,TC_in  = self.pL_C.getR()

        new_run = 1
        if len(self.OP_Point) > 0:
            # compare to old conditions
            massflow_old_C, a, PC_in_old, TC_in_old, PC_out_out, TC_out_old, dh_C = self.OP_Point

            if (massflow_old_C == massflow_C and
                PC_in_old == PC_in and
                PH_in_old == PH_in):
                # same conditions as in previous evaluation.
                new_run = 0

        if new_run == 1:
            # run HX_solver
            # extrat data from current file and replace lines as necessary
            lines = []
            with open(self.fname,'r') as infile:
                for line in infile:
                    if 'F.fluidC' in line:
                        fout.write('F.fluidC = '+self.fluid_C+'\n')
                    if 'F.TC_in' in line:
                        line = 'F.TC_in = '+str(TC_in)+'\n'
                    elif 'F.PC_in' in line:
                        line = 'F.PC_in = '+str(PC_in)+'\n'
                    elif 'F.PC_out' in line:
                        line = 'F.PC_out = '+str(PC_in)+'\n'
                    elif 'F.mdotC' in line:
                        line = 'F.mdotC = '+str(massflow_C)+'\n'
                    elif 'F.T0' in line:
                        if len(self.T0) > 0:
                            string = 'F.T0 = ['
                            for item in self.T0:
                                string = string + (' %s,' % item)
                            string = string[:-1]; string = string + '] \n'
                            if self.use_T0 == 1: # check if old data is to be used
                                line = string
                            else:
                                line = 'F.T0 = [ ] \n'
                    lines.append(line)
            # write lines to new output file.
            with open(self.fname,'w') as outfile:
                for line in lines:
                    outfile.write(line)

            # call main function directly
            uoDict = {'--noprint': '', '--noplot': '', '--job': self.fname}
            PH_out, TH_out, PC_out, TC_out, PH, TH, PC, TC, T = hx.main(uoDict)
            #print('Recup', TH_out, PH_out, TC_out, PC_out)

            hin = CP.PropsSI('HMASS','P',PC_in,'T', TC_in ,self.fluid_C)
            hout = CP.PropsSI('HMASS','P',PC_out,'T', TC_out ,self.fluid_C)
            dh_C = hout-hin

            self.OP_Point = [massflow_C, 'nan', PC_in, TC_in, PC_out, TC_out, dh_C]
            self.DH = dh_C*massflow_C
            #self.TH_trace = TH
            self.TC_trace = TC
            #self.PH_trace = PH
            self.PC_trace = PC
            self.T0 = T
            # adjust points on right
            #print(self.pR_H.label)
            #print(self.pR_C.label)

            self.pR_C.setL(PC_out,TC_out)
    ###
    def calc_mass(self):
        # function to return mass of working fluid stored in components
        # get inlet conditions
        # C-side
        P,H  = self.pL.getR()
        rhoL = CP.PropsSI('D','P',P,'HMASS', H ,self.fluid)
        P,H  = self.pR.getR()
        rhoR = CP.PropsSI('D','P',P,'HMASS', H ,self.fluid)
        self.mass_C = 0.5*(rhoL + rhoR)*self.Volume_H
        self.mass = self.mass_C

        ###
    def calc_cost(self):
        # Cost function retrieved from https://www.sciencedirect.com/science/article/pii/S0360544218302305?via%3Dihub
        LMTD = 10
        # Default LMTD - Do not have temperature data for second stream, half recuperator

        if ('sink' and 'air') in self.label:
            # Gas cooler (air and sCO2) plate heat exchanger
            factor = 36
        else:
            # PCHX heat exchanger cost factor
            factor = 2500

        self.cost = (factor*(abs(self.OP_Point[6]*self.MASS_C)/1000))/LMTD
    ###
    def write(self,fp):
        '''
        function to write data to output file
        '''
        fp.write("TYPE = RECUP_HALF \n")
        fp.write("Label = "+self.label+"\n")
        fp.write("Fluid Type = "+self.fluid+"\n")
        fp.write("Point pL_C = "+self.pL_C.label+"\n")
        fp.write("Point pR_C = "+self.pR_C.label+" \n")
        fp.write("q_C (J/kg) = {0:0.8f} \n".format(self.OP_Point[6]))
        fp.write("Mass Flow Rate (kg/s) = {0:0.8f} \n".format(self.MASS_C))
        fp.write("Fluid mass (kg) = {0:0.8f} \n".format(self.mass))
        fp.write("Cost ($) = {0:0.2f} \n".format(self.cost))
        fp.write("--- \n")
###
###
class TURB:
    instances = []
    def __init__(self,fname,pL,pR,M,OP_speed,label='TURB'):
        self.__class__.instances.append(self)
        self.type = 'TURB'
        self.fname  = fname
        self.pL = pL
        self.pR = pR
        self.OP_speed = OP_speed
        self.label = label
        self.M = M
        self.mass = []
        self.check()
        self.create()
        self.set_MASS()
    ###
    def set_MASS(self):
        self.MASS = 0.
        for m in self.M:
            self.MASS = self.MASS+ m.mass
    ###
    def set_fluid(self):
        self.fluid = self.M[0].fluid
        self.pL.fluid = self.M[0].fluid
        self.pR.fluid = self.M[0].fluid
    ###
    def check(self):
        if not os.path.isfile(self.fname):
            raise MyError("fname used for TURB doesn't exist")
    ###
    def create(self):
        uoDict = {'--noprint': '', '--datafile': self.fname}
        self.TURB = turbf.main(uoDict)
    ###
    def evalR(self):
        self.set_MASS()
        # adjust mass flow based on massfraction massf
        massflow = self.MASS
        # get inlet conditions
        #Pin,Tin  = self.pL.getR()
        Pin,hin  = self.pL.getR()
        Tin = CP.PropsSI('T','P',Pin,'HMASS', hin ,self.fluid)

        # calculate downstream conditions
        Pout , Tout, eta, u_c, er, mfp, delta_h = self.TURB.calc_downstream(massflow, self.OP_speed, Pin, Tin, self.fluid)

        self.OP_Point = [massflow, self.OP_speed, Pin, Tin, Pout, Tout, delta_h, eta, mfp, u_c, er]

        hout = CP.PropsSI('HMASS','P',Pout,'T', Tout ,self.fluid)
        # set points on right
        #self.pR.setL(Pout,Tout)
        self.pR.setL(Pout,hout)
    ###
    def calc_mass(self):
        if len(self.mass) == 0:
            # function to return mass of working fluid stored in components
            # get inlet conditions
            P,H  = self.pL.getR()
            rhoL = CP.PropsSI('D','P',P,'HMASS', H ,self.fluid)
            P,H  = self.pR.getR()
            rhoR = CP.PropsSI('D','P',P,'HMASS', H ,self.fluid)
            self.mass = 0.5*(rhoL + rhoR)*self.TURB.Volume

    ###
    def calc_cost(self):
        # Cost function retrieved from https://www.sciencedirect.com/science/article/pii/S0360544218302305?via%3Dihub
        # Cost for this component does not accurately work for mapped components.
        self.cost = 479.34 * (self.OP_Point[0]) * (1 / (0.93 - self.OP_Point[7])) * np.log(self.OP_Point[10]) * \
                    (1 + np.exp(0.036 * (self.OP_Point[3] - 273.15) - 54.4))
    ###
    def write(self,fp):
        '''
        function to write data to output file
        '''
        fp.write("TYPE = TURB \n")
        fp.write("Filename = "+self.fname+"\n")
        fp.write("Label = "+self.label+"\n")
        fp.write("Fluid Type = "+self.fluid+"\n")
        fp.write("Point pL = "+self.pL.label+"\n")
        fp.write("Point pR = "+self.pR.label+" \n")
        fp.write("Mass Flow Rate (kg/s) = {0:0.8f} \n".format(self.OP_Point[0]))
        fp.write("Speed (-) = {0:0.8f} \n".format(self.OP_Point[1]))
        fp.write("Delta h (J/kg) = {0:0.8f} \n".format(self.OP_Point[6]))
        fp.write("Efficiency (%%) = {0:0.8f} \n".format(float(self.OP_Point[7])))
        fp.write("MFP (kg/s T**0.5 / bar) = {0:0.8f} \n".format(float(self.OP_Point[8])))
        fp.write("U_C (-) = {0:0.8f} \n".format(float(self.OP_Point[9])))
        fp.write("Expansion Ratio (-) = {0:0.8f} \n".format(float(self.OP_Point[10])))
        fp.write("Fluid mass (kg) = {0:0.8f} \n".format(self.mass))
        fp.write("Cost ($) = {0:0.2f} \n".format(self.cost))
        fp.write("--- \n")
###
###
class TURB_AX_SP:
    """
    TURB_AX_SP is for an axial turbine who's operation is defined by both rotational speed and input power.

    Implementation of an axial turbine with off-design loss model using the
    Global Turbine Characteristics Method as described in Schobeiri 2012
    This is based on the method introdiced by Stodola and Traupel.
    """
    instances = []
    def __init__(self,pL,pR,M,Radius, kappa=None, S_target=None, SS=[None], P_target=None, SP=[None], mdot_star=1., Pin_star=1.e5, Tin_star=300., PR_star=2.0, N_star=1., eta_star=0.9, eta_poly=[0.,2.,-1.], Volume=0., label='TURB_AX_SP'):
        self.__class__.instances.append(self)
        self.type = 'TURB_AX_SP'
        self.pL = pL                # POINT conneted to left of unit
        self.pR = pR                # POINT conneted to right of unit
        self.label = label
        self.M = M                  # list containing MASS elements
        self.Radius = Radius        # [m] mean blade radius used to calculate U = omega * Radius
        self.S_target = S_target    # [rad/s] fixed operating speed, when using un-coupled mode
        self.SS = SS                # [rad/s] list containing single Speed-Shaft element
        self.P_target = P_target    # [W] fixed power, when using un-coupled mode
        self.SP = SP                # list containing single or multiple Power-Shaft element(s)
        self.mdot_star = mdot_star  # [kg/s] Mass flow rate at design point
        self.Pin_star = Pin_star    # [Pa] Inlet Pressure at design point
        self.Tin_star = Tin_star    # [K] Inlet temperature at design point
        self.PR_star = PR_star      # [-] Pressure Ratio at design point
        self.N_star = N_star        # [rad/s] Rotational speed at design point
        self.eta_star = eta_star    # [-] Efficiency at design point
        self.eta_poly = eta_poly    # [-] coefficient for polynominal to prescribe efficiency as a function of velocity dimensionless velocity.
        self.Volume = Volume        # [m3] internal volume, used for inventory calculation
        self.kappa = kappa          # [-] kappa = Cp / Cv
        self.mass = []
        self.eta_old = 0.8          # [-] efficiency required for calculation of n parameter. Will be updated during iteration.

        self.set_MASS()
        self.set_SPEED()
        self.set_POWER()
        if self.S_target == None and self.SS == None:
            raise MyError("In TURB_AX_SP-{}, neither a connecting speed-shaft or a speed target has been specified".format(self.label))
        if self.P_target == None and self.SP == None:
            raise MyError("In TURB_AX_SP-{}, neither a connecting power-shaft or a speed target has been specified".format(self.label))
        if len(self.SS) != 1:
            raise MyError("In TURB_AX_SP-{}, more or less than a single shaft have been connected to this unit.".format(self.label))

        # need to get fluid type so that we can do equation of state calls
        self.set_fluid()

        # pre-calculate some veriables.
        # dimensionless velocity parameter v_star at design point
        U_star = self.N_star * self.Radius
        hin_star = CP.PropsSI('HMASS','P',self.Pin_star,'T', self.Tin_star ,self.fluid)
        sin_star = CP.PropsSI('SMASS','P',self.Pin_star,'T', self.Tin_star ,self.fluid)
        hout_star = CP.PropsSI('HMASS','P',self.Pin_star*self.PR_star,'SMASS', sin_star ,self.fluid)
        self.v_star = U_star / np.sqrt( 2. * (hin_star - hout_star) )
    ###
    def set_MASS(self):
        self.MASS = 0.
        for m in self.M:
            self.MASS = self.MASS+ m.mass
    ###
    def set_SPEED(self):
        if self.SS[0] is None:
            self.SPEED = self.S_target
        else:
            self.SPEED = self.SS[0].speed
    ###
    def set_POWER(self):
        self.POWER = 0.
        if self.SP[0] is None:
            self.POWER = self.P_target
        else:
            for p in self.SP:
                self.POWER += p.power
    ###
    def set_fluid(self):
        self.fluid = self.M[0].fluid
        self.pL.fluid = self.M[0].fluid
        self.pR.fluid = self.M[0].fluid
    ###
    def evalR(self):
        # update massflow rate, speed, and power
        self.set_MASS()
        self.set_SPEED()
        self.set_POWER()
        massflow = self.MASS
        # get conditions on either side of the turbine
        Pin,hin  = self.pL.getR()
        Tin = CP.PropsSI('T','P',Pin,'HMASS', hin ,self.fluid)
        sin = CP.PropsSI('SMASS','P',Pin,'HMASS', hin ,self.fluid)


        print("\n+++++++++++++++")
        print("Inside TURB_AX_SP")

        # calcualte n parameter. eta is taken from previous iteration
        n = self.kappa / (self.kappa - self.eta_old * (self.kappa-1.))
        print("n",n)


        # calculate off-design pressure ratio based on inlet conditions and mass flow rate
        temp0 = ( massflow / self.mdot_star * self.Pin_star / Pin * ( Tin / self.Tin_star)**0.5 )**2
        temp1 = temp0 * (1. - self.PR_star**((n+1.)/n) )
        temp2 = 1. - temp1
        if temp2 < 0:
            print("temp2 < 0, hence PR limited to 0.1 .")
            PR = 0.1
        else:
            PR = temp2**( n/(n+1.) )
        self.ER = 1./PR
        #Pout = Pin / self.ER
        Pout = Pin * PR
        print("temp1",temp1, "temp2",temp2)

        print("massflow,masslfow*",massflow,self.mdot_star)
        print("Pin,Pin*",Pin,self.Pin_star)
        print("Tin,Tin*",Tin,self.Tin_star)
        print("PR,PR*",PR,self.PR_star)
        print("Pout",Pout)

        # calculate outlet conditions
        hout_isen = CP.PropsSI('HMASS','P',Pout,'SMASS', sin ,self.fluid)
        delta_h_isen = hin - hout_isen

        U = self.Radius * self.SPEED
        V = np.sqrt(2 * delta_h_isen)
        v = U / V

        # calculate efficiency
        eta_ratio = 0.
        for i in range(len(self.eta_poly)):
            eta_ratio += self.eta_poly[i] * (v/self.v_star)**(i)
        if eta_ratio < 0:   # avoid case of negative efficiency.
            eta_ratio = 0.
        self.eta = eta_ratio * self.eta_star

        if self.eta < 0.5:
            print("TURB_AX_SP, efficiency less than 50%, adjusted to eat = 0.5")
            self.eta = 0.5

        # calculate outlet conditions
        delta_h = self.eta * delta_h_isen
        hout = hin - delta_h
        Tout = CP.PropsSI('T','P',Pout,'HMASS', hout ,self.fluid)

        # assign variables to operating point list
        self.OP_Point = [massflow, self.SPEED, Pin, Tin, Pout, Tout, delta_h, self.eta]

        self.POWER_thermo = massflow*delta_h

        # Set eta for next iteration
        self.eta_old = self.eta

        # add thermodynamic power to POWER shaft
        self.SP[0].add_Power(self.POWER_thermo)

        Tout = CP.PropsSI('T','P',Pout,'HMASS', hout ,self.fluid)

        print("U,V",U,V)
        print("v,v*",v,self.v_star)
        print("eta:",self.eta)
        print("Pout,Tout,delta_h,eta",Pout,Tout,delta_h,self.eta)
        print('Power - thermodynamic:', self.POWER_thermo)
        print('Power - shaft:',self.POWER)




        print("\n")

        # set points on right
        self.pR.setL(Pout,hout)
    ###
    def calc_mass(self):
        # function to return mass of working fluid stored in components
        if len(self.mass) == 0:
            # get inlet conditions
            P,H  = self.pL.getR()
            rhoL = CP.PropsSI('D','P',P,'HMASS', H ,self.fluid)
            P,H  = self.pR.getR()
            rhoR = CP.PropsSI('D','P',P,'HMASS', H ,self.fluid)
            self.mass = 0.5*(rhoL + rhoR)*self.Volume

    ###
    def calc_cost(self):
        # Cost function retrieved from https://www.sciencedirect.com/science/article/pii/S0360544218302305?via%3Dihub
        self.cost = 479.34 * (self.OP_Point[0]) * (1 / (0.93 - self.eta)) * np.log(self.ER) * \
                    (1 + np.exp(0.036 * (self.OP_Point[3] - 273.15) - 54.4))
    ###
    def write(self,fp):
        '''
        function to write data to output file
        '''
        fp.write("TYPE = TURB_AX_SP \n")
        fp.write("Label = "+self.label+"\n")
        fp.write("Fluid Type = "+self.fluid+"\n")
        fp.write("Point pL = "+self.pL.label+"\n")
        fp.write("Point pR = "+self.pR.label+" \n")
        fp.write("Mass Flow Rate (kg/s) = {0:0.8f} \n".format(self.OP_Point[0]))
        fp.write("Speed (-) = {0:0.8f} \n".format(self.OP_Point[1]))
        fp.write("Delta h (J/kg) = {0:0.8f} \n".format(self.OP_Point[6]))
        fp.write("Efficiency (-) = {0:0.8f} \n".format(self.eta))
        fp.write("Expansion Ratio (-) = {0:0.8f} \n".format(self.ER))
        fp.write("Fluid mass (kg) = {0:0.8f} \n".format(self.mass))
        fp.write("Radius     (m) = {0:0.8f} \n".format(self.Radius))
        fp.write("Design point conditions")
        fp.write("mdot_star (kg/s) = {0:0.8f} \n".format(self.mdot_star))
        fp.write("Pin_star    (Pa) = {0:0.8f} \n".format(self.Pin_star))
        fp.write("Tin_star     (K) = {0:0.8f} \n".format(self.Tin_star))
        fp.write("PR_star Pout/Pin (-) = {0:0.8f} \n".format(self.PR_star))
        fp.write("N_star     (rad/s) = {0:0.8f} \n".format(self.N_star))
        fp.write("kappa     (-) = {0:0.8f} \n".format(self.kappa))
        fp.write("eta_star     (-) = {0:0.8f} \n".format(self.eta_star))
        fp.write("v_star     (-) = {0:0.8f} \n".format(self.v_star))
        fp.write("eta_poly"+str(self.eta_poly))
        fp.write("Cost ($) = {0:0.2f} \n".format(self.cost))
        fp.write("--- \n")
###
###
class TURB_ER:
    """
    Turbine operating to maintain a target pressure ratio
    """
    instances = []
    def __init__(self,pL,pR,M,target_ER,eta,label='TURB',Volume=0.):
        self.__class__.instances.append(self)
        self.type = 'TURB_ER'
        self.pL = pL
        self.pR = pR
        self.target_ER = target_ER
        self.eta = eta
        self.label = label
        self.M = M
        self.Volume = Volume
        self.mass = []
        self.set_MASS()
    ###
    def set_MASS(self):
        self.MASS = 0.
        for m in self.M:
            self.MASS = self.MASS+ m.mass
    ###
    def set_fluid(self):

        self.fluid = self.M[0].fluid
        self.pL.fluid = self.M[0].fluid
        self.pR.fluid = self.M[0].fluid
    ###
    def evalR(self):
        self.set_MASS()
        # adjust mass flow based on massfraction massf
        massflow = self.MASS
        # get conditions on either side of compressor
        #Pin,Tin  = self.pL.getR()
        Pin,hin  = self.pL.getR()
        #hin = CP.PropsSI('HMASS','P',Pin,'T', Tin ,self.fluid)
        self.Tin = CP.PropsSI('T','P',Pin,'HMASS',hin,self.fluid)
        sin = CP.PropsSI('SMASS','P',Pin,'HMASS', hin ,self.fluid)

        ER = self.target_ER

        Pout = Pin/ER

        hout_isen = CP.PropsSI('HMASS','P',Pout,'SMASS', sin ,self.fluid)
        delta_h = (hout_isen-hin)*self.eta
        hout = hin + delta_h
        Tout = CP.PropsSI('T','P',Pout,'HMASS', hout ,self.fluid)

        self.OP_Point = [massflow, 'nan', Pin, 'nan', Pout, Tout, delta_h, self.eta]

        # set points on right
        #self.pR.setL(Pout,Tout)
        self.pR.setL(Pout,hout)
    ###
    def calc_mass(self):
        # function to return mass of working fluid stored in components
        if len(self.mass) == 0:
            # get inlet conditions
            P,H  = self.pL.getR()
            rhoL = CP.PropsSI('D','P',P,'HMASS', H ,self.fluid)
            P,H  = self.pR.getR()
            rhoR = CP.PropsSI('D','P',P,'HMASS', H ,self.fluid)
            self.mass = 0.5*(rhoL + rhoR)*self.Volume
    ###
    def calc_cost(self):
        # Cost function retrieved from https://www.sciencedirect.com/science/article/pii/S0360544218302305?via%3Dihub
        self.cost = 479.34*(self.OP_Point[0])*(1/(0.93 - self.eta))*np.log(self.target_ER)*\
                    (1 + np.exp(0.036*(self.Tin - 273.15) - 54.4))
    ###
    def write(self,fp):
        '''
        function to write data to output file
        '''
        fp.write("TYPE = TURB_ER \n")
        fp.write("Label = "+self.label+"\n")
        fp.write("Fluid Type = "+self.fluid+"\n")
        fp.write("Point pL = "+self.pL.label+"\n")
        fp.write("Point pR = "+self.pR.label+" \n")
        fp.write("Mass Flow Rate (kg/s) = {0:0.8f} \n".format(self.OP_Point[0]))
        #fp.write("Speed (-) = {0:0.8f} \n".format(self.OP_Point[1]))
        fp.write("Delta h (J/kg) = {0:0.8f} \n".format(self.OP_Point[6]))
        fp.write("Efficiency (-) = {0:0.8f} \n".format(self.eta))
        #fp.write("MFP (-) = {0:0.8f} \n".format(self.OP_Point[8]))
        #fp.write("U_C (-) = {0:0.8f} \n".format(self.OP_Point[9]))
        fp.write("Expansion Ratio (-) = {0:0.8f} \n".format(self.target_ER))
        fp.write("Fluid mass (kg) = {0:0.8f} \n".format(self.mass))
        fp.write("Cost ($) = {0:0.2f} \n".format(self.cost))
        fp.write("--- \n")
###
###
class TURB_POWER:
    """
    Turbine operating to maintain a target pressure ratio
    """
    instances = []
    def __init__(self,pL,pR,M,Power_target,eta,S=None,P=None,label='TURB',Volume=0.):
        self.__class__.instances.append(self)
        self.type = 'TURB_POWER'
        self.pL = pL
        self.pR = pR
        self.Power_target = Power_target
        self.eta = eta
        self.label = label
        self.M = M
        self.S = S
        self.P = P
        self.Volume = Volume
        self.mass = []
        self.set_MASS()
        self.set_SPEED()
        self.set_POWER()
        if self.P is not None:
            print("In TURB_POWER-{}, is connected to shaft, Power_target has been nulled".format(self.label))
            self.Power_target = None
    ###
    def set_MASS(self):
        self.MASS = 0.
        for m in self.M:
            self.MASS = self.MASS+ m.mass
    ###
    def set_SPEED(self):
        if self.S is None:
            self.SPEED = 1.
        else:
            self.SPEED = self.S.speed
    ###
    def set_POWER(self):
        self.POWER = 0.
        if self.P is None:
            self.POWER = self.Power_target
        else:
            for p in self.P:
                self.POWER += p.power
    ###
    def set_fluid(self):

        self.fluid = self.M[0].fluid
        self.pL.fluid = self.M[0].fluid
        self.pR.fluid = self.M[0].fluid
    ###
    def evalR(self):
        self.set_MASS()
        self.set_SPEED()
        self.set_POWER()
        # adjust mass flow based on massfraction massf
        massflow = self.MASS
        # get conditions on either side of compressor
        #Pin,Tin  = self.pL.getR()
        Pin,hin  = self.pL.getR()
        #hin = CP.PropsSI('HMASS','P',Pin,'T', Tin ,self.fluid)
        self.Tin = CP.PropsSI('T','P',Pin,'HMASS',hin,self.fluid)
        sin = CP.PropsSI('SMASS','P',Pin,'HMASS', hin ,self.fluid)

        # calculate outlet onditions based on prescribed output power
        delta_h = -self.POWER / massflow
        hout = hin + delta_h
        hout_isen = hin + delta_h / self.eta
        Pout = CP.PropsSI('P','HMASS',hout_isen,'SMASS', sin ,self.fluid)

        Tout = CP.PropsSI('T','P',Pout,'HMASS', hout ,self.fluid)

        self.OP_Point = [massflow, 'nan', Pin, 'nan', Pout, Tout, delta_h, self.eta]

        self.ER = Pin/Pout

        # set points on right
        #self.pR.setL(Pout,Tout)
        self.pR.setL(Pout,hout)
    ###
    def calc_mass(self):
        # function to return mass of working fluid stored in components
        if len(self.mass) == 0:
            # get inlet conditions
            P,H  = self.pL.getR()
            rhoL = CP.PropsSI('D','P',P,'HMASS', H ,self.fluid)
            P,H  = self.pR.getR()
            rhoR = CP.PropsSI('D','P',P,'HMASS', H ,self.fluid)
            self.mass = 0.5*(rhoL + rhoR)*self.Volume

    ###
    def calc_cost(self):
        # Cost function retrieved from https://www.sciencedirect.com/science/article/pii/S0360544218302305?via%3Dihub
        self.cost = 479.34 * (self.OP_Point[0]) * (1 / (0.93 - self.eta)) * \
                    np.log(self.OP_Point[2]/self.OP_Point[4]) * (1 + np.exp(0.036 * (self.Tin - 273.15) - 54.4))
    ###
    def write(self,fp):
        '''
        function to write data to output file
        '''
        fp.write("TYPE = TURB_POWER \n")
        fp.write("Label = "+self.label+"\n")
        fp.write("Fluid Type = "+self.fluid+"\n")
        fp.write("Point pL = "+self.pL.label+"\n")
        fp.write("Point pR = "+self.pR.label+" \n")
        fp.write("Power (W) = {0:0.8f} \n".format(self.POWER))
        fp.write("Mass Flow Rate (kg/s) = {0:0.8f} \n".format(self.OP_Point[0]))
        #fp.write("Speed (-) = {0:0.8f} \n".format(self.OP_Point[1]))
        fp.write("Delta h (J/kg) = {0:0.8f} \n".format(self.OP_Point[6]))
        fp.write("Efficiency (-) = {0:0.8f} \n".format(self.eta))
        #fp.write("MFP (-) = {0:0.8f} \n".format(self.OP_Point[8]))
        #fp.write("U_C (-) = {0:0.8f} \n".format(self.OP_Point[9]))
        fp.write("Expansion Ratio (-) = {0:0.8f} \n".format(self.OP_Point[2]/self.OP_Point[4]))
        fp.write("Fluid mass (kg) = {0:0.8f} \n".format(self.mass))
        fp.write("Cost ($) = {0:0.2f} \n".format(self.cost))
        fp.write("--- \n")
###
###
"""
TURB_MAP_SP is a call for turbine described by a map.

"""
class TURB_MAP_SP:
    instances = []
    def __init__(self,pL,pR,M,fname,S_target=None, SS=None, P_target=None, SP=None, Volume=0., label='COMP_MAP_SP'):
        self.__class__.instances.append(self)
        self.type = 'TURB_MAP_SP'
        self.pL = pL                # POINT conneted to left of unit
        self.pR = pR                # POINT conneted to right of unit
        self.M = M                  # list containing MASS elements
        self.fname  = fname         # filename pointing to turbine map
        self.S_target = S_target    # [rad/s] fixed operating speed, when using un-coupled mode
        self.SS = SS                # list containing single Speed-Shaft element
        self.P_target = P_target    # [W] fixed power, when using un-coupled mode
        self.SP = SP                # list containing single or multiple Power-Shaft element(s)
        self.Volume = Volume        # [m3] internal volume, used for inventory calculation
        self.label = label          # user define label for compressor
        self.mass = []

        self.check()
        self.create()
        self.set_MASS()
        self.set_SPEED()
        self.set_POWER()
        if self.S_target == None and self.SS == None:
            raise MyError("In TURB_MAP_SP-{}, neither a connecting speed-shaft or a speed target has been specified".format(self.label))
        if self.P_target == None and self.SP == None:
            raise MyError("In TURB_MAP_SP-{}, neither a connecting power-shaft or a speed target has been specified".format(self.label))
        if len(self.SS) != 1:
            raise MyError("In TURB_MAP_SP-{}, more or less than a single shaft have been connected to this unit.".format(self.label))
    ###
    def set_MASS(self):
        self.MASS = 0.
        for m in self.M:
            self.MASS = self.MASS+ m.mass
    ###
    def set_SPEED(self):
        if self.SS[0] is None:
            self.SPEED = self.S_target
        else:
            self.SPEED = self.SS[0].speed
    ###
    def set_POWER(self):
        self.POWER = 0.
        if self.SP[0] is None:
            self.POWER = self.P_target
        else:
            for p in self.SP:
                self.POWER += p.power
    ###
    def set_fluid(self):
        self.fluid = self.M[0].fluid
        self.pL.fluid = self.M[0].fluid
        self.pR.fluid = self.M[0].fluid
    ###
    def check(self):
        if not os.path.isfile(self.fname):
            raise MyError("fname used for TURB_MAP_SP doesn't exist")
    ###
    def create(self):
        uoDict = {'--noprint': '', '--datafile': self.fname}
        self.TURB = turbf.main(uoDict)
    ###
    def evalR(self):
        # update conditions to match current case
        self.set_MASS()
        self.set_SPEED()
        self.set_POWER()
        # adjust mass flow based on massfraction massf
        massflow = self.MASS
        # get inlet conditions
        # Pin,Tin  = self.pL.getR()
        Pin,hin  = self.pL.getR()
        self.Tin = CP.PropsSI('T','P',Pin,'HMASS', hin ,self.fluid)

        print("\n+++++++++++++++")
        print("Inside TURB_MAP_SP")

        # calculate downstream conditions
        Pout , Tout, eta, u_c, er, mfp, delta_h = self.TURB.calc_downstream(massflow, self.SPEED, Pin, self.Tin, self.fluid)

        eta = eta/100.
        delta_h= - delta_h

        print("massflow: {0:.4f} (kg/s)".format(massflow))
        print("Pin, Tin {0:.1f} {1:.2f}".format(Pin, self.Tin))
        print("Pout, Tout {0:.1f} {1:.2f}".format(Pout, Tout))
        print("delta_h {0:.1f}".format(delta_h))
        print("Speed: {0} (RPM)".format(self.SPEED))
        print("ER: {0:.4f} (-)".format(float(er)))
        print("eta: {0:.4f} (-)".format(float(eta)))


        #if eta == -1. or eta < 0.5:
        #    print("Outside turbine map. Efficiency set to 50%")
        #    eta = 0.5
        ## calculate conditions at other side of turbine using isentropic relations and eta
        #sin = CP.PropsSI('SMASS', 'P', Pin, 'HMASS', hin, self.fluid)    # entropy (j/kg/K)
        #
        ## perform thermodynamic energy balance calculation
        #Pout = Pin / er
        #hout_isen = CP.PropsSI('HMASS', 'P', Pout, 'SMASS', sin , self.fluid)
        #delta_h_isen = hout_isen - hin
        #delta_h = delta_h_isen * eta
        #hout = hin - delta_h

        #print(hin, hout, delta_h_isen, delta_h)

        #Tout = CP.PropsSI('T', 'P', Pout, 'HMASS', hout , self.fluid)

        self.POWER_thermo = delta_h*massflow

        # add thermodynamic power to POWER shaft
        self.SP[0].add_Power(self.POWER_thermo)

        print('Turbine Outlet Conditions:')
        print('Pout, Tout, delta_h, eta :', Pout, Tout, delta_h, eta)
        print('Power - thermodynamic:', self.POWER_thermo)
        print('Power - shaft:',self.POWER)
        print("\n")

        self.OP_Point = [massflow, self.SPEED, Pin, self.Tin, Pout, Tout, delta_h, eta, mfp, u_c, er]

        hout = CP.PropsSI('HMASS','P',Pout,'T', Tout ,self.fluid)
        # set points on right
        #self.pR.setL(Pout,Tout)
        self.pR.setL(Pout,hout)
    ###
    def calc_mass(self):
        # function to return mass of working fluid stored in components
        if len(self.mass) == 0:
            # get inlet conditions
            P,H  = self.pL.getR()
            rhoL = CP.PropsSI('D','P',P,'HMASS', H ,self.fluid)
            P,H  = self.pR.getR()
            rhoR = CP.PropsSI('D','P',P,'HMASS', H ,self.fluid)
            self.mass = 0.5*(rhoL + rhoR)*self.Volume
    ###
    def calc_cost(self):
        # Cost function retrieved from https://www.sciencedirect.com/science/article/pii/S0360544218302305?via%3Dihub
        self.cost = 479.34 * (self.OP_Point[0]) * (1 / (0.93 - self.OP_Point[7])) * \
                    np.log(self.OP_Point[2]/self.OP_Point[4]) * (1 + np.exp(0.036 * (self.Tin - 273.15) - 54.4))
    ###
    def write(self,fp):
        '''
        function to write data to output file
        '''
        fp.write("TYPE = TURB_MAP_SP \n")
        fp.write("Filename = "+self.fname+"\n")
        fp.write("Label = "+self.label+"\n")
        fp.write("Fluid Type = "+self.fluid+"\n")
        fp.write("Point pL = "+self.pL.label+"\n")
        fp.write("Point pR = "+self.pR.label+" \n")
        fp.write("Mass Flow Rate (kg/s) = {0:0.8f} \n".format(self.OP_Point[0]))
        fp.write("Speed (-) = {0:0.8f} \n".format(self.OP_Point[1]))
        fp.write("Delta h (J/kg) = {0:0.8f} \n".format(self.OP_Point[6]))
        fp.write("Efficiency (%%) = {0:0.8f} \n".format(self.OP_Point[7]))
        fp.write("Expansion Ratio (-) = {0:0.8f} \n".format(self.OP_Point[4]/self.OP_Point[2]))
        fp.write("Fluid mass (kg) = {0:0.8f} \n".format(self.mass))
        fp.write("Cost ($) = {0:0.2f} \n".format(self.cost))
        fp.write("--- \n")
###
###
class COMP:
    instances = []
    def __init__(self,fname,pL,pR,M,OP_speed,label='COMP'):
        self.__class__.instances.append(self)
        self.type = 'COMP'
        self.fname  = fname
        self.pL = pL
        self.pR = pR
        self.M = M
        self.OP_speed = OP_speed
        self.label = label
        self.mass = []
        self.check()
        self.create()
        self.set_MASS()
    ###
    def set_MASS(self):
        self.MASS = 0.
        for m in self.M:
            self.MASS = self.MASS+ m.mass
    ###
    def set_fluid(self):
        self.fluid = self.M[0].fluid
        self.pL.fluid = self.M[0].fluid
        self.pR.fluid = self.M[0].fluid
    ###
    def check(self):
        if not os.path.isfile(self.fname):
            raise MyError("fname used for COMP doesn't exist")
    ###
    def create(self):
        uoDict = {'--noprint': '', '--datafile': self.fname}
        self.COMP = compf.main(uoDict)
    ###
    def evalR(self):
        self.set_MASS()
        # adjust mass flow based on massfraction massf
        massflow = self.MASS
        # get inlet conditions
        # Pin,Tin  = self.pL.getR()
        Pin,hin  = self.pL.getR()
        Tin = CP.PropsSI('T','P',Pin,'HMASS', hin ,self.fluid)

        # calculate downstream conditions
        Pout, Tout, delta_h, eta = self.COMP.calc_downstream(massflow, self.OP_speed, Pin, Tin, self.fluid)

        self.OP_Point = [massflow, self.OP_speed, Pin, Tin, Pout, Tout, delta_h, eta]

        hout = CP.PropsSI('HMASS','P',Pout,'T', Tout ,self.fluid)
        # set points on right
        #self.pR.setL(Pout,Tout)
        self.pR.setL(Pout,hout)
    ###
    def calc_mass(self):
        # function to return mass of working fluid stored in components
        if len(self.mass) == 0:
            # get inlet conditions
            P,H  = self.pL.getR()
            rhoL = CP.PropsSI('D','P',P,'HMASS', H ,self.fluid)
            P,H  = self.pR.getR()
            rhoR = CP.PropsSI('D','P',P,'HMASS', H ,self.fluid)
            self.mass = 0.5*(rhoL + rhoR)*self.COMP.Volume

    ###
    def calc_cost(self):
        # Cost function retrieved from https://www.sciencedirect.com/science/article/pii/S0360544218302305?via%3Dihub
        # Cost for this component does not accurately work for mapped components.
        self.cost = 71.10 * (self.OP_Point[0]) * (1 / (0.92 - self.OP_Point[7])) * \
                    (self.OP_Point[4] / self.OP_Point[2]) * np.log(self.OP_Point[4] / self.OP_Point[2])
    ###
    def write(self,fp):
        '''
        function to write data to output file
        '''
        fp.write("TYPE = COMP \n")
        fp.write("Filename = "+self.fname+"\n")
        fp.write("Label = "+self.label+"\n")
        fp.write("Fluid Type = "+self.fluid+"\n")
        fp.write("Point pL = "+self.pL.label+"\n")
        fp.write("Point pR = "+self.pR.label+" \n")
        fp.write("Mass Flow Rate (kg/s) = {0:0.8f} \n".format(self.OP_Point[0]))
        fp.write("Speed (-) = {0:0.8f} \n".format(self.OP_Point[1]))
        fp.write("Delta h (J/kg) = {0:0.8f} \n".format(self.OP_Point[6]))
        fp.write("Efficiency (%%) = {0:0.8f} \n".format(self.OP_Point[7]))
        fp.write("Compression Ratio (-) = {0:0.8f} \n".format(self.OP_Point[4]/self.OP_Point[2]))
        fp.write("Fluid mass (kg) = {0:0.8f} \n".format(self.mass))
        fp.write("Cost ($) = {0:0.2f} \n".format(self.cost))
        fp.write("--- \n")
###
###
"""
COMP_MAP_SP is a call for radial compressors described by a map.

"""
class COMP_MAP_SP:
    instances = []
    def __init__(self,pL,pR,M,fname,S_target=None, SS=None, P_target=None, SP=None, Volume=0., label='COMP_MAP_SP'):
        self.__class__.instances.append(self)
        self.type = 'COMP_MAP_SP'
        self.pL = pL                # POINT conneted to left of unit
        self.pR = pR                # POINT conneted to right of unit
        self.M = M                  # list containing MASS elements
        self.fname  = fname         # filename pointing to compressor map
        self.S_target = S_target    # [rad/s] fixed operating speed, when using un-coupled mode
        self.SS = SS                # list containing single Speed-Shaft element
        self.P_target = P_target    # [W] fixed power, when using un-coupled mode
        self.SP = SP                # list containing single or multiple Power-Shaft element(s)
        self.Volume = Volume        # [m3] internal volume, used for inventory calculation
        self.label = label          # user define label for compressor
        self.mass = []

        self.check()
        self.create()
        self.set_MASS()
        self.set_SPEED()
        self.set_POWER()
        if self.S_target == None and self.SS == None:
            raise MyError("In COMP_MAP_SP-{}, neither a connecting speed-shaft or a speed target has been specified".format(self.label))
        if self.P_target == None and self.SP == None:
            raise MyError("In COMP_MAP_SP-{}, neither a connecting power-shaft or a speed target has been specified".format(self.label))
        if len(self.SS) != 1:
            raise MyError("In COMP_MAP_SP-{}, more or less than a single shaft have been connected to this unit.".format(self.label))
    ###
    def set_MASS(self):
        self.MASS = 0.
        for m in self.M:
            self.MASS = self.MASS+ m.mass
    ###
    def set_SPEED(self):
        if self.SS[0] is None:
            self.SPEED = self.S_target
        else:
            self.SPEED = self.SS[0].speed
    ###
    def set_POWER(self):
        self.POWER = 0.
        if self.SP[0] is None:
            self.POWER = self.P_target
        else:
            for p in self.SP:
                self.POWER += p.power
    ###
    def set_fluid(self):
        self.fluid = self.M[0].fluid
        self.pL.fluid = self.M[0].fluid
        self.pR.fluid = self.M[0].fluid
    ###
    def check(self):
        if not os.path.isfile(self.fname):
            raise MyError("fname used for COMP doesn't exist")
    ###
    def create(self):
        uoDict = {'--noprint': '', '--datafile': self.fname}
        self.COMP = compf.main(uoDict)
    ###
    def evalR(self):
        # update conditions to match current case
        self.set_MASS()
        self.set_SPEED()
        self.set_POWER()
        # adjust mass flow based on massfraction massf
        massflow = self.MASS
        # get inlet conditions
        # Pin,Tin  = self.pL.getR()
        Pin,hin  = self.pL.getR()
        Tin = CP.PropsSI('T','P',Pin,'HMASS', hin ,self.fluid)

        print("\n+++++++++++++++")
        print("Inside COMP_MAP_SP")

        # calculate downstream conditions
        eta,PR = self.COMP.calc_downstream(massflow, self.SPEED, Pin, Tin, self.fluid)

        print("massflow: {0} (kg/s)".format(massflow))
        print("Pin, Tin",Pin, Tin)
        print("Speed: {0} (RPM)".format(self.SPEED))
        print("PR: {0} (-)".format(PR))
        print("eta: {0} (-)".format(eta))

        if eta == -1. or eta < 0.5:
            print("Outside compressor map. Efficiency set to 50%")
            eta = 0.5
        # calculate conditions at other side of compressor using isentropic relations and eta
        sin = CP.PropsSI('SMASS', 'P', Pin, 'HMASS', hin, self.fluid)    # entropy (j/kg/K)

        # perform thermodynamic energy balance calculation
        Pout = Pin * PR
        hout_isen = CP.PropsSI('HMASS', 'P', Pout, 'SMASS', sin , self.fluid)
        delta_h_isen = hout_isen - hin
        delta_h = delta_h_isen / eta
        hout = hin + delta_h
        delta_h = hin-hout
        Tout = CP.PropsSI('T', 'P', Pout, 'HMASS', hout , self.fluid)

        self.POWER_thermo = delta_h*massflow

        # add thermodynamic power to POWER shaft
        self.SP[0].add_Power(self.POWER_thermo)

        print('Compressor Outlet Conditions:')
        print('Pout, Tout, delta_h, eta :', Pout, Tout, delta_h, eta)
        print('Power - thermodynamic:', self.POWER_thermo)
        print('Power - shaft:',self.POWER)
        print("\n")


        # set operating point
        self.OP_Point = [massflow, self.SPEED, Pin, Tin, Pout, Tout, delta_h, eta]

        # set points on right
        #self.pR.setL(Pout,Tout)
        self.pR.setL(Pout,hout)
    ###
    def calc_mass(self):
        # function to return mass of working fluid stored in components
        if len(self.mass) == 0:
            # get inlet conditions
            P,H  = self.pL.getR()
            rhoL = CP.PropsSI('D','P',P,'HMASS', H ,self.fluid)
            P,H  = self.pR.getR()
            rhoR = CP.PropsSI('D','P',P,'HMASS', H ,self.fluid)
            self.mass = 0.5*(rhoL + rhoR)*self.COMP.Volume

    ###
    def calc_cost(self):
        # Cost function retrieved from https://www.sciencedirect.com/science/article/pii/S0360544218302305?via%3Dihub
        self.cost = 71.10 * (self.OP_Point[0]) * (1 / (0.92 - self.OP_Point[7])) * \
                    (self.OP_Point[4] / self.OP_Point[2]) * np.log(self.OP_Point[4] / self.OP_Point[2])
    ###
    def write(self,fp):
        '''
        function to write data to output file
        '''
        fp.write("TYPE = COMP_MAP_SP \n")
        fp.write("Filename = "+self.fname+"\n")
        fp.write("Label = "+self.label+"\n")
        fp.write("Fluid Type = "+self.fluid+"\n")
        fp.write("Point pL = "+self.pL.label+"\n")
        fp.write("Point pR = "+self.pR.label+" \n")
        fp.write("Mass Flow Rate (kg/s) = {0:0.8f} \n".format(self.OP_Point[0]))
        fp.write("Speed (-) = {0:0.8f} \n".format(self.OP_Point[1]))
        fp.write("Delta h (J/kg) = {0:0.8f} \n".format(self.OP_Point[6]))
        fp.write("Efficiency (%%) = {0:0.8f} \n".format(self.OP_Point[7]))
        fp.write("Compression Ratio (-) = {0:0.8f} \n".format(self.OP_Point[4]/self.OP_Point[2]))
        fp.write("Fluid mass (kg) = {0:0.8f} \n".format(self.mass))
        fp.write("Cost ($) = {0:0.2f} \n".format(self.cost))
        fp.write("--- \n")
###
###
class COMP_MASSF:
    """
    Compressor that adjusts pressure ratio until target mass flow rate is achieved
    """
    instances = []
    def __init__(self,pL,pR,M,M_target,eta,label='COMP',Volume=0.):
        self.__class__.instances.append(self)
        self.type = 'COMP_MASSF'
        self.pL = pL
        self.pR = pR
        self.M_target = M_target
        self.eta = eta
        self.M = M
        self.label = label
        self.Volume = Volume
        self.mass = []
        self.set_MASS()
    ###
    def set_fluid(self):
        self.fluid = self.M[0].fluid
        self.pL.fluid = self.M[0].fluid
        self.pR.fluid = self.M[0].fluid
    ###
    def set_MASS(self):
        self.MASS = 0.
        for m in self.M:
            self.MASS = self.MASS+ m.mass
    ###
    def evalR(self):
        self.set_MASS()
        # adjust mass flow based on massfraction massf
        massflow = self.MASS
        # get conditions on either side of compressor
        #Pin,Tin  = self.pL.getR()
        Pin,hin  = self.pL.getR()
        #hin = CP.PropsSI('HMASS','P',Pin,'T', Tin ,self.fluid)
        sin = CP.PropsSI('SMASS','P',Pin,'HMASS', hin ,self.fluid)
        Pout,Tout = self.pR.getR()

        hout_isen = CP.PropsSI('HMASS','P',Pout,'SMASS', sin ,self.fluid)
        delta_h = (hout_isen-hin)/self.eta
        hout = hin + delta_h
        Tout = CP.PropsSI('T','P',Pout,'HMASS', hout ,self.fluid)

        # add error in P_out based on mass flow rate error
        Pout2 = Pout + (massflow-self.M_target)*100.

        self.OP_Point = [massflow, 'nan', Pin, 'nan', Pout, Tout, delta_h, self.eta]

        # set points on right
        #self.pR.setL(Pout2,Tout)
        self.pR.setL(Pout2,hout)
    ###
    def calc_mass(self):
        # function to return mass of working fluid stored in components
        if len(self.mass) == 0:
            # get inlet conditions
            P,H  = self.pL.getR()
            rhoL = CP.PropsSI('D','P',P,'HMASS', H ,self.fluid)
            P,H  = self.pR.getR()
            rhoR = CP.PropsSI('D','P',P,'HMASS', H ,self.fluid)
            self.mass = 0.5*(rhoL + rhoR)*self.Volume

    ###
    def calc_cost(self):
        # Cost function retrieved from https://www.sciencedirect.com/science/article/pii/S0360544218302305?via%3Dihub
        self.cost = 71.10 * (self.OP_Point[0]) * (1 / (0.92 - self.eta)) * \
                    (self.OP_Point[4] / self.OP_Point[2]) * np.log(self.OP_Point[4] / self.OP_Point[2])
    ###
    def write(self,fp):
        '''
        function to write data to output file
        '''
        fp.write("TYPE = COMP_MASSF \n")
        fp.write("Label = "+self.label+"\n")
        fp.write("Fluid Type = "+self.fluid+"\n")
        fp.write("Point pL = "+self.pL.label+"\n")
        fp.write("Point pR = "+self.pR.label+" \n")
        fp.write("Mass Flow Rate (kg/s) = {0:0.8f} \n".format(self.OP_Point[0]))
        #fp.write("Speed (-) = {0:0.8f} \n".format(self.OP_Point[1]))
        fp.write("Delta h (J/kg) = {0:0.8f} \n".format(self.OP_Point[6]))
        fp.write("Efficiency (-) = {0:0.8f} \n".format(self.eta))
        #fp.write("MFP (-) = {0:0.8f} \n".format(self.OP_Point[8]))
        #fp.write("U_C (-) = {0:0.8f} \n".format(self.OP_Point[9]))
        fp.write("Compression Ratio (-) = {0:0.8f} \n".format(self.OP_Point[4]/self.OP_Point[2]))
        fp.write("Fluid mass (kg) = {0:0.8f} \n".format(self.mass))
        fp.write("Cost ($) = {0:0.2f} \n".format(self.cost))
        fp.write("--- \n")
###
###
class COMP_PR:
    """
    Compressor that maintains a fixed pressure ratio
    """
    instances = []
    def __init__(self,pL,pR,M,PR_target,eta,label='COMP',Volume=0.):
        self.__class__.instances.append(self)
        self.type = 'COMP_MASSF'
        self.pL = pL
        self.pR = pR
        self.PR_target = PR_target
        self.eta = eta
        self.M = M
        self.label = label
        self.Volume = Volume
        self.mass = []
        self.set_MASS()
    ###
    def set_fluid(self):
        self.fluid = self.M[0].fluid
        self.pL.fluid = self.M[0].fluid
        self.pR.fluid = self.M[0].fluid
    ###
    def set_MASS(self):
        self.MASS = 0.
        for m in self.M:
            self.MASS = self.MASS+ m.mass
    ###
    def evalR(self):
        self.set_MASS()
        # adjust mass flow based on massfraction massf
        massflow = self.MASS
        # get conditions on either side of compressor
        #Pin,Tin  = self.pL.getR()
        Pin,hin  = self.pL.getR()
        #hin = CP.PropsSI('HMASS','P',Pin,'T', Tin ,self.fluid)
        sin = CP.PropsSI('SMASS','P',Pin,'HMASS', hin ,self.fluid)

        # calculate outlet onditions based on prescribed pressure ratio
        Pout = Pin * self.PR_target

        hout_isen = CP.PropsSI('HMASS','P',Pout,'SMASS', sin ,self.fluid)
        delta_h = (hout_isen-hin)/self.eta
        hout = hin + delta_h
        Tout = CP.PropsSI('T','P',Pout,'HMASS', hout ,self.fluid)

        self.OP_Point = [massflow, 'nan', Pin, 'nan', Pout, Tout, delta_h, self.eta]

        # set points on right
        self.pR.setL(Pout,hout)
    ###
    def calc_mass(self):
        # function to return mass of working fluid stored in components
        if len(self.mass) == 0:
            # get inlet conditions
            P,H  = self.pL.getR()
            rhoL = CP.PropsSI('D','P',P,'HMASS', H ,self.fluid)
            P,H  = self.pR.getR()
            rhoR = CP.PropsSI('D','P',P,'HMASS', H ,self.fluid)
            self.mass = 0.5*(rhoL + rhoR)*self.Volume
    ###
    def calc_cost(self):
        # Cost function retrieved from https://www.sciencedirect.com/science/article/pii/S0360544218302305?via%3Dihub
        self.cost = 71.10 * (self.OP_Point[0]) * (1 / (0.92 - self.eta)) * \
                    (self.OP_Point[4] / self.OP_Point[2]) * np.log(self.OP_Point[4] / self.OP_Point[2])
    ###
    def write(self,fp):
        '''
        function to write data to output file
        '''
        fp.write("TYPE = COMP_PR \n")
        fp.write("Label = "+self.label+"\n")
        fp.write("Fluid Type = "+self.fluid+"\n")
        fp.write("Point pL = "+self.pL.label+"\n")
        fp.write("Point pR = "+self.pR.label+" \n")
        fp.write("Mass Flow Rate (kg/s) = {0:0.8f} \n".format(self.OP_Point[0]))
        #fp.write("Speed (-) = {0:0.8f} \n".format(self.OP_Point[1]))
        fp.write("Delta h (J/kg) = {0:0.8f} \n".format(self.OP_Point[6]))
        fp.write("Efficiency (-) = {0:0.8f} \n".format(self.eta))
        #fp.write("MFP (-) = {0:0.8f} \n".format(self.OP_Point[8]))
        #fp.write("U_C (-) = {0:0.8f} \n".format(self.OP_Point[9]))
        fp.write("Compression Ratio (-) = {0:0.8f} \n".format(self.OP_Point[4]/self.OP_Point[2]))
        fp.write("Fluid mass (kg) = {0:0.8f} \n".format(self.mass))
        fp.write("Cost ($) = {0:0.2f} \n".format(self.cost))
        fp.write("--- \n")
###
###
class COMP_POWER:
    """
    Compressor that consumes a fixed amount of power
    """
    instances = []
    def __init__(self,pL,pR,M,Power_target,eta,S=None,P=None,label='COMP',Volume=0.):
        self.__class__.instances.append(self)
        self.type = 'COMP_POWER'
        self.pL = pL
        self.pR = pR
        self.Power_target = Power_target
        self.eta = eta
        self.M = M
        self.S = S
        self.P = P
        self.label = label
        self.Volume = Volume
        self.mass = []
        self.set_MASS()
        if self.P is not None:
            print("In COMP_POWER-{}, is connected to shaft, Power_target has been nulled".format(self.label))
            self.Power_target = None
    ###
    def set_fluid(self):
        self.fluid = self.M[0].fluid
        self.pL.fluid = self.M[0].fluid
        self.pR.fluid = self.M[0].fluid
    ###
    def set_MASS(self):
        self.MASS = 0.
        for m in self.M:
            self.MASS = self.MASS+ m.mass
    ###
    def set_SPEED(self):
        if self.S is None:
            self.SPEED = 1.
        else:
            self.SPEED = self.S.speed
    ###
    def set_POWER(self):
        self.POWER = 0.
        if self.P is None:
            self.POWER = self.Power_target
        else:
            for p in self.P:
                self.POWER += p.power
    ###
    def evalR(self):
        self.set_MASS()
        self.set_SPEED()
        self.set_POWER()
        # adjust mass flow based on massfraction massf
        massflow = self.MASS
        # get conditions on either side of compressor
        #Pin,Tin  = self.pL.getR()
        Pin,hin  = self.pL.getR()
        #hin = CP.PropsSI('HMASS','P',Pin,'T', Tin ,self.fluid)
        sin = CP.PropsSI('SMASS','P',Pin,'HMASS', hin ,self.fluid)

        # calculate outlet onditions based on prescribed input power
        delta_h = self.POWER / massflow
        hout = hin + delta_h
        hout_isen = hin + delta_h * self.eta
        Pout = CP.PropsSI('P','HMASS',hout_isen,'SMASS', sin ,self.fluid)
        Tout = CP.PropsSI('T','P',Pout,'HMASS', hout ,self.fluid)

        self.OP_Point = [massflow, 'nan', Pin, 'nan', Pout, Tout, delta_h, self.eta]

        # set points on right
        self.pR.setL(Pout,hout)
    ###
    def calc_mass(self):
        # function to return mass of working fluid stored in components
        if len(self.mass) == 0:
            # get inlet conditions
            P,H  = self.pL.getR()
            rhoL = CP.PropsSI('D','P',P,'HMASS', H ,self.fluid)
            P,H  = self.pR.getR()
            rhoR = CP.PropsSI('D','P',P,'HMASS', H ,self.fluid)
            self.mass = 0.5*(rhoL + rhoR)*self.Volume
    ###
    def calc_cost(self):
        # Cost function retrieved from https://www.sciencedirect.com/science/article/pii/S0360544218302305?via%3Dihub
        self.cost = 71.10 * (self.OP_Point[0]) * (1 / (0.92 - self.eta)) * \
                    (self.OP_Point[4] / self.OP_Point[2]) * np.log(self.OP_Point[4] / self.OP_Point[2])
    ###
    def write(self,fp):
        '''
        function to write data to output file
        '''
        fp.write("TYPE = COMP_POWER \n")
        fp.write("Label = "+self.label+"\n")
        fp.write("Fluid Type = "+self.fluid+"\n")
        fp.write("Point pL = "+self.pL.label+"\n")
        fp.write("Point pR = "+self.pR.label+" \n")
        fp.write("Power (W)= {0:0.8f} \n".format(self.POWER))
        fp.write("Mass Flow Rate (kg/s) = {0:0.8f} \n".format(self.OP_Point[0]))
        #fp.write("Speed (-) = {0:0.8f} \n".format(self.OP_Point[1]))
        fp.write("Delta h (J/kg) = {0:0.8f} \n".format(self.OP_Point[6]))
        fp.write("Efficiency (-) = {0:0.8f} \n".format(self.eta))
        #fp.write("MFP (-) = {0:0.8f} \n".format(self.OP_Point[8]))
        #fp.write("U_C (-) = {0:0.8f} \n".format(self.OP_Point[9]))
        fp.write("Compression Ratio (-) = {0:0.8f} \n".format(self.OP_Point[4]/self.OP_Point[2]))
        fp.write("Fluid mass (kg) = {0:0.8f} \n".format(self.mass))
        fp.write("Cost ($) = {0:0.2f} \n".format(self.cost))
        fp.write("--- \n")
###
###
class MERGE:
    instances = []
    def __init__(self,pL0,pL1,pR,ML0,ML1,MR,label='MERGE',mtype='free'):
        self.__class__.instances.append(self)
        self.mtype = mtype
        self.type = 'MERGE'
        self.pL0 = pL0
        self.pL1 = pL1
        self.pR = pR
        self.label = label
        self.ML0 = ML0
        self.ML1 = ML1
        self.MR =MR
        self.mass = []
        self.set_MASS()
    ###
    def set_MASS(self):
        self.MASS_L0 = 0.
        self.MASS_L1 = 0.
        for m in self.ML0:
            self.MASS_L0 = self.MASS_L0 + m.mass
        for m in self.ML1:
            self.MASS_L1 = self.MASS_L1 + m.mass
        self.MASS_R = 0.
        for m in self.MR:
            self.MASS_R = self.MASS_R + m.mass
    ###
    def set_fluid(self):
        if self.ML0[0].fluid == self.ML1[0].fluid:
            self.fluid = self.ML0[0].fluid
        else:
            raise MyError("Fluid types of stream merging at MERGE don't agree.")
        self.pL0.fluid = self.fluid
        self.pL1.fluid = self.fluid
        self.pR.fluid = self.fluid
    ###
    def evalR(self):
        self.set_MASS()
        # get incoming conditions
        #PL0,TL0 = self.pL0.getR()
        #PL1,TL1 = self.pL1.getR()
        #PR,TR = self.pR.getL()
        PL0,HL0 = self.pL0.getR()
        PL1,HL1 = self.pL1.getR()
        PR,HR = self.pR.getR()

        # calculate outlet conditions.

        # enthalpy is conserved
        h0 = HL0 # CP.PropsSI('HMASS','P',PL0,'T', TL0 ,self.fluid)
        h1 = HL1 # CP.PropsSI('HMASS','P',PL1,'T', TL1 ,self.fluid)
        h_out = (self.MASS_L0 * h0 + self.MASS_L1 * h1) / (self.MASS_L0+self.MASS_L1)

        # need to penalise outlet pressure if both inlet pressures are not equally
        P_out = 0.5*(PL0 + PL1) + abs(PL0-PR) + abs(PL1-PR)

        #T_out = CP.PropsSI('T','P',P_out,'HMASS', h_out ,self.fluid)
        #print("MERGE0")
        #print("inlet", PL0, TL0, PL1, TL1)
        #print("outlet", P_out, T_out)
        """
        if T_out < TL0 and T_out < TL1:
            T_out = min([TL0, TL1])
            print('T limited to min')
        if T_out > TL0 and T_out > TL1:
            T_out = max([TL0, TL1])
            print('T limited to min')
        """

        # doing a forwatd mapping, averages pressure
        #self.pR.setL(P_out,T_out)
        self.pR.setL(P_out,h_out)
    ###
    def get_diff(self):
        #print('get_diff for merge :', self.label)
        #print('PL0 and PL1 :', self.PL0, self.PL1)
        #PL0,TL0 = self.pL0.getR()
        #PL1,TL1 = self.pL1.getR()
        PL0,HL0 = self.pL0.getR()
        PL1,HL1 = self.pL1.getR()

        out = PL0 - PL1
        return out
    ###
    def calc_mass(self):
        # function to return mass of working fluid stored in components
        if len(self.mass) == 0:
            self.mass = 0.
    ###
    def calc_cost(self):
        self.cost = 0
    ###
    def write(self,fp):
        '''
        function to write data to output file
        '''
        fp.write("TYPE = MERGE \n")
        fp.write("Label = "+self.label+"\n")
        fp.write("Fluid Type = "+self.fluid+"\n")
        fp.write("Point pL0 = "+self.pL0.label+"\n")
        fp.write("Point pL1 = "+self.pL1.label+"\n")
        fp.write("Point pR = "+self.pR.label+" \n")
        fp.write("Mass Flow Rate L0 (kg/s) = {0:0.8f} \n".format(self.MASS_L0))
        fp.write("Mass Flow Rate L1 (kg/s) = {0:0.8f} \n".format(self.MASS_L1))
        fp.write("Fluid mass (kg) = {0:0.8f} \n".format(self.mass))
        fp.write("--- \n")
###
###
class PIPE:
    instances = []
    def __init__(self,pL,pR,A,L,M,label='PIPE',D='n/a',epsilon=0.,loss=1):
        self.__class__.instances.append(self)
        self.type = 'PIPE'
        self.pL = pL
        self.pR = pR
        self.label = label
        self.A = A
        self.L = L
        self.M = M
        self.D = D
        self.epsilon = epsilon
        self.loss = loss
        self.set_MASS()
    ###
    def set_MASS(self):
        self.MASS = 0.
        for m in self.M:
            self.MASS = self.MASS+ m.mass
    ###
    def set_fluid(self):
        self.fluid = self.M[0].fluid
        self.pL.fluid = self.M[0].fluid
    ###
    def evalR(self):
        self.set_MASS()
        # adjust mass flow based on massfraction massf
        massflow = self.MASS

        # calculate pressure loss
        PL,TL  = self.pL.getR()
        PR,TR  = self.pR.getR()

        if self.loss == 0:
            print("WARNING: setting pipe loss = 0 leads to unstable results.")
            # use loss-less pipe
            Pout = PL
            Tout = PR
            rhoR = CP.PropsSI('D','P',PR,'T', TR ,self.fluid)
            delta_h = 0.
        elif self.loss == 1:
            # use circular pipe correlations based on
            rhoL = CP.PropsSI('D','P',PL,'T', TL ,self.fluid)
            rhoR = CP.PropsSI('D','P',PR,'T', TR ,self.fluid)
            muL = CP.PropsSI('VISCOSITY','P',PL,'T', TL ,self.fluid)
            muR = CP.PropsSI('VISCOSITY','P',PR,'T', TR ,self.fluid)
            rho = 0.5 * (rhoL+rhoR)
            mu = 0.5 * (muL+muR)
            V = massflow / (self.A * rho)
            if isinstance(self.D,float) == 0: # if D no defined, calculate from circle.
                self.D = 2* (self.A**0.5)/np.pi
            Re = rho*V*self.D/mu
            if Re < 2300.:
                f = 64./Re
            else:
                temp = 1.8* np.log10((self.epsilon/self.D / 3.7)**1.11 + 6.9/Re)
                f = temp**-2
            Delta_P = f * self.L/self.D * V**2 / (2.*9.81) * rho

            # set downstream conditions
            Pout = PL - Delta_P
            hin = CP.PropsSI('HMASS','P',PL,'T', TL ,self.fluid)
            hout = hin
            Tout = CP.PropsSI('T','P',PR,'HMASS', hout ,self.fluid)
            delta_h = hout-hin
        else:
            raise MyError("self.loss is not properly defined for pipe")

        # calculate inlet and exit Reynolds Number
        Re_L = rhoL*V*self.D/muL
        Re_R = rhoL*V*self.D/muL
        if Re_L / Re_R > 1.2 or Re_R / Re_L > 1.2:
            print('WARNING: Ratio of Reynolds number between inlet and outlet of pipe: ', self.label, ' is greater than 1.2.')

        # set condition at downstream point
        self.pR.setL(Pout,Tout)
        self.OP_Point = [massflow, 'nan', PL, TL, Pout, Tout, delta_h]

        # define Temperature and Pressure Traces for plotting
        self.T_trace = np.linspace(TL,Tout,30)
        self.P_trace = np.linspace(PL,Pout,30)

        # check exit Mach number
        V_exit = massflow / (self.A * rhoR)
        a = CP.PropsSI('SPEED_OF_SOUND','P',Pout,'T', Tout ,self.fluid)
        Mach = abs(V_exit) / a
        if Mach > 0.5:
            print('Mach number at exit of PIPE ',self.label,' is: ', Mach)
            print('Consider increasing pipe area to avoid sonic effects')
    ###
    def calc_mass(self):
        # function to return mass of working fluid stored in components
        # get inlet conditions
        P,H  = self.pL.getR()
        rhoL = CP.PropsSI('D','P',P,'HMASS', H ,self.fluid)
        P,H  = self.pR.getR()
        rhoR = CP.PropsSI('D','P',P,'HMASS', H ,self.fluid)
        self.mass = 0.5*(rhoL + rhoR)*self.A*self.L
    ###
    def calc_cost(self):
        self.cost = 0
    ###
    def write(self,fp):
        '''
        function to write data to output file
        '''
        fp.write("TYPE = PIPE \n")
        fp.write("Label = "+self.label+"\n")
        fp.write("Fluid Type = "+self.fluid+"\n")
        fp.write("Point pL = "+self.pL.label+"\n")
        fp.write("Point pR = "+self.pR.label+" \n")
        fp.write("Area (m**2) = {0:0.8f} \n".format(self.A))
        fp.write("Length (m) = {0:0.8f} \n".format(self.L))
        fp.write("M = {0:0.8f} \n".format(self.M))
        fp.write("Label = "+self.label+ " \n")
        fp.write("D (m) = {0:0.8f} \n".format(self.D))
        fp.write("Epsilon (m) = {0:0.8f} \n".format(self.epsilon))
        fp.write("loss = {0:0.8f} \n".format(self.loss))
        fp.write("Mass Flow Rate (kg/s) = {0:0.8f} \n".format(self.MASS))
        fp.write("Fluid mass (kg) = {0:0.8f} \n".format(self.mass))
        fp.write("--- \n")
###
class EX_OR: # orifice with isentropic expansion.
    instances = []
    def __init__(self,pL,pR,M,A,Cd=1.0,label='EX_Orifice',otype='isentropic',Volume=0.):
        self.__class__.instances.append(self)
        self.type = 'EX_Orifice'
        self.pL = pL
        self.pR = pR
        self.label = label
        self.M = M
        self.A = A
        self.Cd = Cd
        self.otype = otype
        self.Volume = Volume
        self.set_MASS()
        self.flow_state='n/a'
    ###
    def set_MASS(self):
        self.MASS = 0.
        for m in self.M:
            self.MASS = self.MASS+ m.mass
    ###
    def set_fluid(self):
        self.fluid = self.M[0].fluid
        self.pL.fluid = self.fluid
        self.pR.fluid = self.fluid
    ###
    def evalR(self):
        print("Inside ExOR")
        self.set_MASS()
        # adjust mass flow based on massfraction massf
        massflow = self.MASS
        # calculate effective flow area:
        A_eff = self.A * self.Cd

        # calculate pressures
        PL,hL  = self.pL.getR()
        Pin=PL
        Pout, _ = self.pR.getR() # this data is used to set exit pressure
        sL = CP.PropsSI('SMASS','P',PL,'HMASS', hL ,self.fluid)

        # calculate isentropic pressure drop to reach Mach 1
        # mach_one = lambda P_star: self.calcMach(P_star,PL,hL) - 1. # move zero for Mach=1
        #Pstar = sci.optimize.newton(mach_one, 0.95*PL)
        P_star = float(sci.optimize.fsolve(self.calcPressure, 0.95*PL, args=(PL,hL,1.),xtol=1e-3))
        P_star = abs(P_star)
        print("Pressures [MPa]:", P_star/1e6, Pout/1e6)

        if P_star < Pout: # subsonic flow
            # mass flow rate is set by up and dosntream conditions
            # calculate mass flow rate through orifice
            h_throat = CP.PropsSI('HMASS','P',Pout,'SMASS', sL ,self.fluid)
            if hL < h_throat:
                u_throat = 0.  # prevents reverse flow
            else:
                u_throat = np.sqrt( (hL - h_throat)/0.5 )
            rho_throat = CP.PropsSI('D','P',Pout,'SMASS', sL ,self.fluid)
            massflow_maximum = A_eff * rho_throat * u_throat
            print("Massflows (subsonic) max/actual:",massflow_maximum,massflow)
            self.flow_state = 'subsonic, PR={:f}, PR_crit={:f}'.format(PL/Pout,PL/P_star)

        else: # chocked flow
            # mass flow rate is only governed by upstream conditions.
            # calculate mass flow rate through orifice
            rho_star = CP.PropsSI('D','P',P_star,'SMASS', sL ,self.fluid)
            h_star = CP.PropsSI('HMASS','P',P_star,'SMASS', sL ,self.fluid)
            if hL < h_star:
                u_star = 0. # prevents reverse flow
            else:
                u_star = np.sqrt( (hL - h_star)/0.5 )
            massflow_maximum = A_eff * rho_star * u_star
            print("Masslfows (choked):",massflow_maximum,massflow)
            self.flow_state = 'chocked max/actual, PR={:f}, PR_crit={:f}'.format(PL/Pout,PL/P_star)


        alpha = 0.01
        Pout2 = Pout * (1. + alpha * ( (massflow-massflow_maximum)/max(massflow,massflow_maximum) )**2  )
        # introduce error to Pout based on mass flow rate mis-match. This guides optimiser.
        #print(Pout, Pout2)

        if self.otype is 'isentropic':
            # fluid expands through orifice isentropically
            #Pout = Pout
            hout = CP.PropsSI('HMASS','P',Pout,'SMASS', sL ,self.fluid)
        elif self.otype is 'isenthalpic':
            # fluid expands through orifice isenthalpicly
            #Pout = Pout
            hout = hL
        else:
            raise MyError("Type of orifices for ", self.label," defined incorrectly.")

        # set point on right
        self.pR.setL(Pout2,hout)
     ###
    def calcPressure(self,P,P0,h0,M_target):
        # function to calculate mach number for isentropic expansion from P0 to P
        # h0 = h_star + 0.5 * u_star**2
        P = abs(P)  # limit P to be positive and also limit maximum value to P0. This spans solution space betwen Mach = 0 and 1.
        if P > P0:
            U = 0 # avoid CooProp call as tolerancing can lead to h_star>h0
            Mach = 0
            #print(P,P0,h0,U,Mach)
        else:
            s0 = CP.PropsSI('SMASS','P',P0,'HMASS', h0 ,self.fluid)
            h_star = CP.PropsSI('HMASS','P',P,'SMASS', s0 ,self.fluid)
            a_star = CP.PropsSI('A','P',P,'SMASS', s0 ,self.fluid)

            U = np.sqrt( (h0 - h_star)/0.5 )
            Mach = U/a_star
            #print(P,P0,h0,s0,h_star,a_star,U,Mach)

        Error = (Mach-M_target)**2
        return Error
    ###
    def calc_mass(self):
        # function to return mass of working fluid stored in components
        # get inlet conditions
        P,H  = self.pL.getR()
        rhoL = CP.PropsSI('D','P',P,'HMASS', H ,self.fluid)
        P,H  = self.pR.getR()
        rhoR = CP.PropsSI('D','P',P,'HMASS', H ,self.fluid)
        self.mass = 0.5*(rhoL + rhoR)*self.Volume
    ###
    def calc_cost(self):
        self.cost = 0
    ###
    def write(self,fp):
        '''
        function to write data to output file
        '''
        fp.write("TYPE = EX_OR \n")
        fp.write("Fluid Type = "+self.fluid+"\n")
        fp.write("Label = "+self.label+"\n")
        fp.write("Point pL = "+self.pL.label+"\n")
        fp.write("Point pR = "+self.pR.label+" \n")
        fp.write("Mass Flow Rate (kg/s) = {0:0.8f} \n".format(self.MASS))
        fp.write("--- \n")
###
###
class SHAFT: # shaft to connect two S_POINTS
    instances = []
    def __init__(self,sL,sR,loss=0.,loss_type='absolute',label='Shaft'):
        self.__class__.instances.append(self)
        self.type = 'SHAFT'
        self.sL = sL
        self.sR = sR
        self.label = label
        self.loss = loss
        self.loss_type = loss_type
        self.MASS = None # required for compatibility
    ###
    def set_fluid(self):
        # will be called during initialise
        NL,PL  = self.sL.getR()
        NR,PR  = self.sR.getL()
        self.N = 0.5*(NL+NR)
        self.PL = PL
        self.PR = PR
    ###
    def evalR(self):
        print("Inside SHAFT")

        # get properties on left
        NL,PL  = self.sL.getR()

        if self.loss_type == 'absolute':
            # fixed amount of loss is removed from shaft
            self.Loss = self.loss
            PR = np.sign(PL) * (abs(PL)-self.Loss)
        elif self.loss_type == "rel_power":
            # loss is applied proportional to transmitted power
            self.Loss = self.loss * PL
            PR = PL-self.Loss
        elif self.loss_type == "rel_speed":
            # loss is applied proportional to shaft speed
            self.Loss = self.loss * NL
            PR = np.sign(PL) * (abs(PL)-self.Loss)
        else:
            raise MyError("loss_type = {} not supported for SHAFT. Bailing out!".format(self.loss_type))

        self.N = NL
        self.PL = PL
        self.PR = PR

        # set point on right
        self.sR.setL(NL,PR)
    ###
    def calc_mass(self):
        # function is required for compatibility
        self.mass = 0.
    ###
    def calc_cost(self):
        self.cost = 0
    ###
    def write(self,fp):
        '''
        function to write data to output file
        '''
        fp.write("TYPE = SHAFT \n")
        fp.write("Label = {0} \n".format(self.label))
        fp.write("Point sL = {0} \n".format(self.sL.label))
        fp.write("Point sR = {0} \n".format(self.sR.label))
        fp.write("loss_type = {0} \n".format(self.loss_type))
        fp.write("loss (input) = {0} \n".format(self.loss_type))
        fp.write("Speed (rad/s) = {0} \n".format(self.N))
        fp.write("Power-left (W) = {0} \n".format(self.PL))
        fp.write("Power-right (W) = {0} \n".format(self.PR))
        fp.write("Loss (W) = {0} \n".format(self.Loss))
        fp.write("--- \n")
###
###
class ICE_SIMPLE:
    """
    Simple implementation of an Internal Combustion Engine (ICE)
    For this simple implementation it is assumed that the fuel has no mass and
    that the thermodynamic properties of the fluid running through the engine
    remain the same.

    The performance of the engine is calculated based on the following approach.
    Energy released during combustion = Qfuel
    Shaft work out = eff * Qfuel
    Energy to coolant (water + oil) = (1. - eff) * Qfuel * coolant split
    Energy to heat exhaust gases = (1. - eff) * Qfuel * (1. - coolant split)

    """
    instances = []
    def __init__(self,pL,pR,M,Speed,Vswept,Qfuel,eff,coolant_split,MOUT=[],label='ICE_simple'):
        self.__class__.instances.append(self)
        self.type = 'ICE_SIMPLE'
        self.pL = pL
        self.pR = pR
        self.Speed = Speed # Rev per second
        self.label = label
        self.MIN = M
        if not MOUT:  # check if MOUT has been defined. If not copy across M.
            self.MOUT = M
        else:
            self.MOUT = MOUT
        self.Vswept = Vswept
        self.Qfuel = Qfuel
        self.eff = eff
        self.coolant_split = coolant_split
        self.Q_coolant = 0. # amount of energy being conducted to coolant [W]
        self.Volume = 0. # internal volume of component
        self.mass = [] # mass of fluid contained in component
        self.mdot_in = 0.
        self.ShaftPower = 0.
        self.set_MASS()
    ###
    def set_MASS(self):
        self.MASS = 0.
        for m in self.MIN:
            self.MASS = self.MASS+ m.mass
    ###
    def set_fluid(self):  # TODO this needs to be altered to allow different fluid types before and after
        self.fluid_in = self.MIN[0].fluid  # get fluid for inlet
        self.pL.fluid = self.MIN[0].fluid
        self.fluid_out = self.MOUT[0].fluid  # get fluid for inlet
        self.pR.fluid = self.MOUT[0].fluid
    ###
    def evalR(self):
        # Notes, the internal combustion engine behaves as follows:
        # - mass flow rate is governed by amount of fluid drawn into system
        # - can achieve any outlet pressure
        # - outlet tempeature is governed by heat addition (combustion)

        # calculate current massflow as per applied streams
        self.set_MASS()
        # adjust mass flow based on massfraction massf
        massflow = self.MASS

        # calculate volume flow rate into ICE
        Q_in = self.Speed * self.Vswept  # volume flow rate in m3/s
        # calculate mass flow rate into ICE
        # Pin,Tin  = self.pL.getR()
        Pin,hin  = self.pL.getR()
        rhoin = CP.PropsSI('D','P',Pin,'HMASS',hin,self.pL.fluid)
        self.mdot_in = Q_in * rhoin  # mass flow rate in kg/s

        # get outlet conditions before update Get from righ side of downstream point
        Pout, _ = self.pR.getR() # this data is used to set exit pressure

        # calculate power split
        self.ShaftPower = self.Qfuel*self.eff
        self.Q_coolant = self.Qfuel*(1.-self.eff) * self.coolant_split
        Qfluid = self.Qfuel* (1.-self.eff) * (1.-self.coolant_split)

        # calculate enthalpy rise due to combustion
        hout = hin + Qfluid / self.mdot_in

        #self.pR.setL(Pout,hout)

        # add error in P_out based on mass flow rate error
        Pout2 = Pout + ((self.mdot_in-massflow)/self.mdot_in)**2 * 0.1*Pout

        # set point on right
        self.pR.setL(Pout2,hout)
    ###
    def get_diff(self):
        #print('get_diff for ICE_simple :', self.label)
        #print('MASS and mdot_in :', self.MASS, self.mdot_in)

        out = self.MASS - self.mdot_in
        return out
    ###
    def calc_mass(self):
        if len(self.mass) == 0:
            # function to return mass of working fluid stored in components
            # get inlet conditions
            P,H  = self.pL.getR()
            rhoL = CP.PropsSI('D','P',P,'HMASS', H ,self.fluid_in)
            P,H  = self.pR.getR()
            rhoR = CP.PropsSI('D','P',P,'HMASS', H ,self.fluid_out)
            self.mass = 0.5*(rhoL + rhoR)*self.Volume
    ###
    def calc_cost(self):
        self.cost = 0
    ###
    def write(self,fp):
        '''
        function to write data to output file
        '''
        fp.write("TYPE = ICE_SIMPLE \n")
        fp.write("Label = "+self.label+"\n")
        fp.write("Fluid Type (in) = "+self.fluid_in+"\n")
        fp.write("Fluid Type (out) = "+self.fluid_out+"\n")
        fp.write("Point pL = "+self.pL.label+"\n")
        fp.write("Point pR = "+self.pR.label+" \n")
        fp.write("Mass Flow Rate (kg/s) = {0:0.8f} \n".format(self.MASS))
        fp.write("Speed (-) = {0:0.8f} \n".format(self.Speed))
        fp.write("Vswept (m3) = {0:0.8f} \n".format(self.Vswept))
        fp.write("Qfuel (W) = {0:0.8f} \n".format(self.Qfuel))
        fp.write("Efficiency (-) = {0:0.8f} \n".format(self.eff))
        fp.write("Coolant_split (-) = {0:0.8f} \n".format(self.coolant_split))
        fp.write("Q_coolant (W) = {0:0.8f} \n".format(self.Q_coolant))
        fp.write("Fluid mass (kg) = {0:0.8f} \n".format(self.mass))
        fp.write("--- \n")
###
###
class ICE_ADVANCED_0:
    """
    Advanced implementation of an Internal Combustion Engine (ICE)


    For this simple implementation it is assumed that the fuel has no mass and
    that the thermodynamic properties of the fluid running through the engine
    remain the same.

    The performance of the engine is calculated based on the following approach.
    Energy released during combustion = mdot_fuel * q_fuel
    Shaft work out = eff * Qfuel
    Energy to coolant (water + oil) = (1. - eff) * Qfuel * coolant split
    Energy to heat exhaust gases = (1. - eff) * Qfuel * (1. - coolant split)

    """
    instances = []
    def __init__(self,pL,pR,Min,Mout,Speed,Vswept,mdot_fuel,q_fuel,c_fuel,T_fuel,eff,coolant_split,label='ICE_ADVANCED_0'):
        self.__class__.instances.append(self)
        self.type = 'ICE_ADVANCED_0'
        self.pL = pL
        self.pR = pR
        self.Speed = Speed      # [Rev per second] Shaft speed for engine
        self.label = label
        self.Min = Min          # list of MASS elements connected to input
        self.Mout = Mout        # list of MASS elements connected to output
        self.Vswept = Vswept    # [m3] swept volume of engines
        self.mdot_fuel = mdot_fuel  # [kg/s] mass flow rate of fuel supplied to engine
        self.cal_fuel = cal_fuel    # [J/kg] calorific value of fuel (based on 25degC)
        self.c_fuel = c_fuel        # [J/K] heat capacity of fuel
        self.T_fuel = T_fuel        # [K] Temperature of fuel
        self.eff = eff                      # [-] efficiency of power extraction to shaft
        self.coolant_split = coolant_split  # [-] fraction of residual energy going to coolant
        self.Volume = 0. # internal volume of component
        self.mass = [] # mass of fluid contained in component
        self.mdot_in = 0.
        self.ShaftPower = 0.
        if len(self.Min) != 1:
            raise MyError("In ICE_ADVANCED_0-{}, only a single inlet MASS flow Min can be specified".format(self.label))
        if len(self.Mou) != 1:
            raise MyError("In ICE_ADVANCED_0-{}, only a single outlet MASS flow Mout can be specified".format(self.label))
        #self.set_MASS()
    ###
    def set_MASS(self):
        self.MASSin = 0.
        for m in self.Min:
            self.MASSin = self.MASSin+ m.mass
    ###
    def update_MASS(self):
        #self.Min.mass = self.mdot_in
        self.Mout.mass = seld.mdot_out
    ###
    def set_fluid(self):
        self.fluidin = self.Min[0].fluid
        self.fluidout = self.Min[0].fluid
        self.pL.fluid = self.Min[0].fluid
        self.pR.fluid = self.Mout[0].fluid
    ###
    def evalR(self):
        # Notes, the internal combustion engine behaves as follows:
        # - mass flow rate is governed by amount of fluid drawn into system
        # - can achieve any outlet pressure
        # - outlet tempeature is governed by heat addition (combustion)

        # calculate current massflow as per applied streams
        self.set_MASS()
        # adjust mass flow based on massfraction massf
        massflow = self.MASSin

        # calculate volume flow rate into ICE
        Q_in = self.Speed * self.Vswept  # volume flow rate in m3/s
        # calculate mass flow rate into ICE
        Pin,hin  = self.pL.getR()
        rhoin = CP.PropsSI('D','P',Pin,'HMASS',hin,self.fluidin)
        Tin = CP.PropsSI('T','P',Pin,'HMASS',hin,self.fluidin)
        sin = CP.PropsSI('SMASS','P',Pin,'HMASS',hin,self.fluidin)
        self.mdot_in = Q_in * rhoin  # mass flow rate in kg/s

        # calculate mass flow rate out of ICE
        self.mdot_out = self.mdot_in + self.mdot_fuel

        # update mass flows outside
        update_MASS()

        # calculate heat released
        Q0 = (T_fuel - (273.15 + 25.) ) * self.c_fuel * self.mdot_fuel  # heat released/required to bring fuel to 25C
        Q1 = (Tin - (273.15 + 25.) ) * self.c_fuel * self.mdot_fuel     # heat required to warm fuel to inlet conditions
        Qfuel = self.mdot_fuel * self.cal_fuel + Q0 - Q1                # total energy released

        # calculate power split
        self.ShaftPower = self.Qfuel*self.eff
        Qfluid = self.Qfuel* (1.-self.eff) * (1.-self.coolant_split)

        # calculate warming up of fluid
        hin_dash = CP.PropsSI('HMASS','P',Pin,'Tin',Tin,self.fluidout)
        # calculate enthalpy rise due to combustion
        hout = hin + Qfluid / self.mdot_out

        # get outlet conditions before update Get from righ side of downstream point
        Pout, _ = self.pR.getR() # this data is used to set exit pressure

        # add error in P_out based on mass flow rate error
        Pout2 = Pout + ((self.mdot_in-massflow)/self.mdot_in)**2 * 0.1*Pout

        # set point on right
        self.pR.setL(Pout2,hout)
    ###
    def get_diff(self):
        #print('get_diff for ICE_simple :', self.label)
        #print('MASS and mdot_in :', self.MASS, self.mdot_in)

        out = self.MASS - self.mdot_in
        return out
    ###
    def calc_mass(self):
        if len(self.mass) == 0:
            # function to return mass of working fluid stored in components
            # get inlet conditions
            P,H  = self.pL.getR()
            rhoL = CP.PropsSI('D','P',P,'HMASS', H ,self.fluid)
            P,H  = self.pR.getR()
            rhoR = CP.PropsSI('D','P',P,'HMASS', H ,self.fluid)
            self.mass = 0.5*(rhoL + rhoR)*self.Volume
    ###
    def calc_cost(self):
        self.cost = 0
    ###
    def write(self,fp):
        '''
        function to write data to output file
        '''
        fp.write("TYPE = ICE_ADVANCED_0 \n")
        fp.write("Label = "+self.label+"\n")
        fp.write("Fluid Type = "+self.fluid+"\n")
        fp.write("Point pL = "+self.pL.label+"\n")
        fp.write("Point pR = "+self.pR.label+" \n")
        fp.write("Mass Flow Rate in  (kg/s) = {0:0.8f} \n".format(self.MASSin))
        fp.write("Mass Flow Rate out (kg/s) = {0:0.8f} \n".format(self.MASSout))
        fp.write("Speed (-) = {0:0.8f} \n".format(self.Speed))
        fp.write("Vswept (m3) = {0:0.8f} \n".format(self.Vswept))
        fp.write("mdot_fuel   (W) = {0:0.8f} \n".format(self.mdot_fuel))
        fp.write("cal_fuel (J/kg) = {0:0.8f} \n".format(self.cal_fuel))
        fp.write("c_fuel    (J/K) = {0:0.8f} \n".format(self.c_fuel))
        fp.write("T_fuel      (K) = {0:0.8f} \n".format(self.T_fuel))
        fp.write("Efficiency (-) = {0:0.8f} \n".format(self.eff))
        fp.write("Coolant_split (-) = {0:0.8f} \n".format(self.coolant_split))
        fp.write("Fluid mass (kg) = {0:0.8f} \n".format(self.mass))
        fp.write("--- \n")
###
###
class RAM_AIR_INTAKE:
    instances = []
    def __init__(self,pL,pR,M,Mach,eta_isen=1.0,label='RAM_air_intake'):
        self.__class__.instances.append(self)
        self.type = 'RAM_AIR_INTAKE'
        self.pL = pL
        self.pR = pR
        self.M = M
        self.Mach = Mach
        self.eta_isen = eta_isen
        self.label = label
        self.Volume = 0. # internal volume of component
        self.mass = [] # mass of fluid contained in component
        self.set_MASS()
    ###
    def set_MASS(self):
        self.MASS = 0.
        for m in self.M:
            self.MASS = self.MASS+ m.mass
    ###
    def set_fluid(self):
        self.fluid = self.M[0].fluid
        self.pL.fluid = self.M[0].fluid
        self.pR.fluid = self.M[0].fluid
    ###
    def evalR(self):
        # Calculate Pressure increase throug ram compression based on flight Mach number
        # efficiency of compression process is defined by eta_isen.
        self.set_MASS()
        # adjust mass flow based on massfraction massf
        massflow = self.MASS
        # get conditions on either side of compressor
        #Pin,Tin  = self.pL.getR()
        Pin,hin  = self.pL.getR()
        sin = CP.PropsSI('SMASS','P',Pin,'HMASS', hin ,self.fluid)
        rhoin = CP.PropsSI('D','P',Pin,'HMASS', hin ,self.fluid)
        ain = CP.PropsSI('A','P',Pin,'HMASS', hin ,self.fluid)
        Vin = ain * self.Mach


        # calculate outlet onditions based on availabel 'ram energy' and eta_isen
        delta_h = 0.5 * rhoin * Vin * Vin
        hout = hin + delta_h
        hout_isen = hin + delta_h * self.eta_isen
        Pout = CP.PropsSI('P','HMASS',hout_isen,'SMASS', sin ,self.fluid)
        Tout = CP.PropsSI('T','P',Pout,'HMASS', hout ,self.fluid)

        self.OP_Point = [massflow, 'nan', Pin, 'nan', Pout, Tout, delta_h, self.eta_isen]

        # set points on right
        self.pR.setL(Pout,hout)
    ###
    def get_diff(self):
        #print('get_diff for ICE_simple :', self.label)
        #print('MASS and mdot_in :', self.MASS, self.mdot_in)

        out = self.MASS - self.mdot_in
        return out
    ###
    def calc_mass(self):
        if len(self.mass) == 0:
            # function to return mass of working fluid stored in components
            # get inlet conditions
            P,H  = self.pL.getR()
            rhoL = CP.PropsSI('D','P',P,'HMASS', H ,self.fluid)
            P,H  = self.pR.getR()
            rhoR = CP.PropsSI('D','P',P,'HMASS', H ,self.fluid)
            self.mass = 0.5*(rhoL + rhoR)*self.Volume
    ###
    def calc_cost(self):
        self.cost = 0
    ###
    def write(self,fp):
        '''
        function to write data to output file
        '''
        fp.write("TYPE = RAM_AIR_INTAKE \n")
        fp.write("Label = "+self.label+"\n")
        fp.write("Fluid Type = "+self.fluid+"\n")
        fp.write("Point pL = "+self.pL.label+"\n")
        fp.write("Point pR = "+self.pR.label+" \n")
        fp.write("Mass Flow Rate (kg/s) = {0:0.8f} \n".format(self.MASS))
        fp.write("Mach (-) = {0:0.8f} \n".format(self.Mach))
        fp.write("Eta_isen (-) = {0:0.8f} \n".format(self.eta_isen))
        fp.write("Fluid mass (kg) = {0:0.8f} \n".format(self.mass))
        fp.write("--- \n")
###
###
class RAD:
    instances = []
    def __init__(self,T,pL,pR,eff,M,Volume=0., label='RAD',A=[], L=[], D=[], N_channel=1 , epsilon=0. ):
        self.__class__.instances.append(self)
        self.type = 'HX'
        self.T = T
        self.pL = pL
        self.pR = pR
        self.eff = eff
        self.label = label
        self.Volume = Volume
        self.A = A
        self.L = L
        self.D = D
        self.N_channel = N_channel
        self.epsilon = epsilon
        self.M = M
        self.DH = []
        # self.SA = 0
        # print(self.SA)
        self.set_MASS()

        # self.check_inputs()
    ###
    def set_MASS(self):
        self.MASS = 0.
        for m in self.M:
            self.MASS = self.MASS + m.mass
    ###
    def set_fluid(self):
        M=self.M[0]
        self.fluid = M.fluid
        self.pL.fluid = M.fluid
        self.pR.fluid = M.fluid
    ###
    def evalR(self):
        self.set_MASS()
        # adjust mass flow based on massfraction massf
        massflow = self.MASS
        #print('HX', massflow)
        # evaluate conditions at other side
        #PL,TL = self.pL.getR()
        PL,HL = self.pL.getR()
        self.TL = CP.PropsSI('T','P',PL,'HMASS', HL ,self.fluid)
        # print('Here ', self.pL.label, PL,TL)
        if isinstance(self.A,float) == 0:
            # lossless HX without pressure decreas
            PR = PL
            #TR = self.T
            HR = CP.PropsSI('HMASS','P',PR,'T',self.T,self.fluid)
        else:
            # calculate pressure loss
            #PL,TL  = self.pL.getR()
            #PR,TR  = self.pR.getR()
            #rhoL = CP.PropsSI('D','P',PL,'T', TL ,self.fluid)
            #rhoR = CP.PropsSI('D','P',PR,'T', TR ,self.fluid)
            #muL = CP.PropsSI('VISCOSITY','P',PL,'T', TL ,self.fluid)
            #muR = CP.PropsSI('VISCOSITY','P',PR,'T', TR ,self.fluid)
            PL,HL  = self.pL.getR()
            PR,HR  = self.pR.getR()
            rhoL = CP.PropsSI('D','P',PL,'HMASS', HL ,self.fluid)
            rhoR = CP.PropsSI('D','P',PR,'HMASS', HR ,self.fluid)
            muL = CP.PropsSI('VISCOSITY','P',PL,'T', TL ,self.fluid)
            muR = CP.PropsSI('VISCOSITY','P',PR,'T', TR ,self.fluid)

            rho = 0.5 * (rhoL+rhoR)
            mu = 0.5 * (muL+muR)
            V = (massflow / self.N_channel) / (self.A * rho)
            if isinstance(self.D,float) == 0: # if D no defined, calculate from circle.
                self.D = 2* (self.A**0.5)/np.pi
            Re = rho*V*self.D/mu
            if Re < 2300.:
                f = 64./Re
            else:
                temp = 1.8* np.log10((self.epsilon/self.D / 3.7)**1.11 + 6.9/Re)
                f = temp**-2
            Delta_P = f * self.L/self.D * V**2 / (2.*9.81) * rho
            # set downstream conditions
            PR = PL - Delta_P
            #TR = self.T
            HR = CP.PropsSI('HMASS','P',PR,'T',self.T,self.fluid)

        # set condition at downstream point
        #self.pR.setL(PR,TR)
        self.pR.setL(PR,HR)

        # calculate enthalpy change
        #print("HX-in", PL, TL)
        #print("HX-out", PR,TR)
        #hin = CP.PropsSI('HMASS','P',PL,'T', TL ,self.fluid)
        #hout = CP.PropsSI('HMASS','P',PR,'T', TR ,self.fluid)
        #self.OP_Point = [massflow, 'nan', PL, TL, PR, TR, hout-hin]
        #self.DH = massflow * (hout-hin)
        self.OP_Point = [massflow, 'nan', PL, 'nan', PR, 'nan', HR-HL]
        self.DH = massflow * (HR-HL)

        # define Temperature and Pressure Traces for plotting
        #self.T_trace = np.linspace(TL,self.T,30)
        self.P_trace = np.linspace(PL,PR,30)
        self.T_trace = []
        self.H_trace = np.linspace(HL,HR,30)
        for i in range(len(self.H_trace)):
            self.T_trace.append(CP.PropsSI('T','P',self.P_trace[i],'HMASS',self.H_trace[i],self.fluid))

        self.calc_SA()
    ###
    def calc_mass(self):
        # function to return mass of working fluid stored in components
        # get inlet conditions
        P,H  = self.pL.getR()
        rhoL = CP.PropsSI('D','P',P,'HMASS', H ,self.fluid)
        P,H  = self.pR.getR()
        rhoR = CP.PropsSI('D','P',P,'HMASS', H ,self.fluid)
        self.mass = 0.5*(rhoL + rhoR)*self.Volume
    ###
    def calc_cost(self):
        # Cost function retrieved from https://www.sciencedirect.com/science/article/pii/S0360544218302305?via%3Dihub
        LMTD = 10
        # Default LMTD - Cannot be calculated due to no temperature change in hot / cold stream
        if ('sink' and 'air') in self.label:
            # Gas cooler (air and sCO2) plate heat exchanger
            factor = 36
        else:
            # PCHX heat exchanger cost factor
            factor = 2500

        self.cost = (factor*(abs(self.OP_Point[6]*self.MASS)/1000))/LMTD
    ###
    def calc_SA(self):
        # Calculates the surface area of the radiator
        BC = 5.669e-8 #Boltzmann constant
        self.SA = abs(self.OP_Point[6]/(BC*self.eff*(self.TL**4-self.T**4)))
    ###
    def write(self,fp):
        '''
        function to write data to output file
        '''
        fp.write("TYPE = RAD \n")
        fp.write("Label = "+self.label+"\n")
        fp.write("Fluid Type = "+self.fluid+"\n")
        fp.write("Point pL = "+self.pL.label+"\n")
        fp.write("Point pR = "+self.pR.label+" \n")
        fp.write("q (J/kg) = {0:0.8f} \n".format(self.OP_Point[6]))
        fp.write("Surface area (m^2) =  "+str(self.SA)+" \n")
        fp.write("Mass Flow Rate (kg/s) = {0:0.8f} \n".format(self.MASS))
        fp.write("Fluid mass (kg) = {0:0.8f} \n".format(self.mass))
        fp.write("Cost ($) = {0:0.2f} \n".format(self.cost))
        fp.write("--- \n")
###
###
class GCHP:
    instances = []
    def __init__(self,fname,pL_C,pR_C,pL_H,pR_H,M_C, M_H, alpha, k,  Radius,label='GCHP',use_T0=0,Volume_H=0.,Volume_C=0.):
        self.__class__.instances.append(self)
        self.type = 'GCHP'
        self.fname  = fname
        self.pL_H = pL_H
        self.pR_H = pR_H
        self.pL_C = pL_C
        self.pR_C = pR_C
        self.label = label
        self.M_H = M_H
        self.M_C = M_C
        self.T0 = []
        self.OP_Point = []
        self.use_T0 = use_T0
        self.Volume_H = Volume_H
        self.Volume_C = Volume_C
        self.DH = []
        self.alpha = alpha
        self.k = k
        self.Radius = Radius
        self.set_MASS()

    ###
    def set_MASS(self):
        self.MASS_H = 0.
        self.MASS_C = 0.
        for m in self.M_H:
            self.MASS_H = self.MASS_H + m.mass
        for m in self.M_C:
            self.MASS_C = self.MASS_C + m.mass
    ###
    def set_fluid(self):
        self.fluid_H = self.M_H[0].fluid
        self.fluid_C = self.M_C[0].fluid
        self.pL_H.fluid = self.M_H[0].fluid
        self.pR_H.fluid = self.M_H[0].fluid
        self.pL_C.fluid = self.M_C[0].fluid
        self.pR_C.fluid = self.M_C[0].fluid
    ###
    def evalR(self):
        self.set_MASS()
        # adjust mass flow based on massfraction massf
        massflow_H = self.MASS_H
        massflow_C = self.MASS_C
        # evaluate conditions at other side and set into node on right
        # adjust HX job file

        #PC_in,TC_in  = self.pL_C.getR()
        #PH_in,TH_in  = self.pL_H.getR()
        PC_in,hC_in  = self.pL_C.getR()
        PH_in,hH_in  = self.pL_H.getR()

        new_run = 1
        if len(self.OP_Point) > 0:
            # compare to old conditions
            massflow_old_C, a, PC_in_old, TC_in_old, PC_out_out, TC_out_old, dh_C, PH_in_old, TH_in_old, PH_out_old, TH_out_old, dh_H, massflow_old_H,hC_in_old,hH_in_old = self.OP_Point

            #if (massflow_old_H == massflow_H and massflow_old_C == massflow_C and
            #    PC_in_old == PC_in and TC_in_old == TC_in and
            #    PH_in_old == PH_in and TH_in_old == TH_in):
            if (massflow_old_H == massflow_H and massflow_old_C == massflow_C and
                PC_in_old == PC_in and hC_in_old == hC_in and
                PH_in_old == PH_in and hH_in_old == hH_in):

                # same conditions as in previous evaluation.
                new_run = 0

        if new_run == 1:
            # run HX_solver
            # extrat data from current file and replace lines as necessary
            lines = []; label_flag=0
            with open(self.fname,'r') as infile:
                for line in infile:
                    if "F.fluidH" in line:
                        line = "F.fluidH = '"+self.fluid_H+"' \n"
                    if 'F.fluidC' in line:
                        line = "F.fluidC = '"+self.fluid_C+"'\n"
                    if 'F.hC_in' in line:
                        line = 'F.hC_in = '+str(hC_in)+'\n'
                    if 'F.hH_in' in line:
                        line = 'F.hH_in = '+str(hH_in)+'\n'
                    #if 'F.TC_in' in line:
                    #    line = 'F.TC_in = '+str(TC_in)+'\n'
                    #if 'F.TH_in' in line:
                    #    line = 'F.TH_in = '+str(TH_in)+'\n'
                    if 'F.PC_in' in line:
                        line = 'F.PC_in = '+str(PC_in)+'\n'
                    if 'F.PC_out' in line:
                        line = 'F.PC_out = '+str(PC_in)+'\n'
                    if 'F.PH_in' in line:
                        line = 'F.PH_in = '+str(PH_in)+'\n'
                    if 'F.PH_out' in line:
                        line = 'F.PH_out = '+str(PH_in)+'\n'
                    if 'F.mdotC' in line:
                        line = 'F.mdotC = '+str(massflow_C)+'\n'
                    if 'F.mdotH' in line:
                        line = 'F.mdotH = '+str(massflow_H)+'\n'
                    if 'F.T0' in line:
                        if len(self.T0) > 0:
                            string = 'F.T0 = ['
                            for item in self.T0:
                                string = string + (' %s,' % item)
                            string = string[:-1]; string = string + '] \n'
                            if self.use_T0 == 1: # check if old data is to be used
                                line = string
                            else:
                                line = 'F.T0 = [ ] \n'
                    if 'M.label' in line:
                        line = "M.line = '"+self.label+"'\n"
                        label_flag = 1
                    lines.append(line)

                #print(label_flag)
                #if label_flag == 0: # add label if not already exists
                #    lines.append("M.line = '"+self.label+"'\n")

            # write lines to new output file.
            with open(self.fname,'w') as outfile:
                for line in lines:
                    outfile.write(line)

            # call main function directly
            uoDict = {'--verbosity': '0', '--noplot': '', '--job': self.fname}
            PH_out, TH_out, PC_out, TC_out, PH, TH, PC, TC, T, hH_in, hH_out, hC_in, hC_out = hx.main(uoDict)
            #print('Recup', TH_out, PH_out, TC_out, PC_out)

            #hin = CP.PropsSI('HMASS','P',PC_in,'T', TC_in ,self.fluid_C)
            #hout = CP.PropsSI('HMASS','P',PC_out,'T', TC_out ,self.fluid_C)
            dh_C = hC_out-hC_in
            #hin = CP.PropsSI('HMASS','P',PH_in,'T', TH_in ,self.fluid_H)
            #hout = CP.PropsSI('HMASS','P',PH_out,'T', TH_out ,self.fluid_H)
            dh_H = hH_out-hH_in

            self.OP_Point = [massflow_C, 'nan', PC_in, 'nan', PC_out, TC_out, dh_C, PH_in, 'nan', PH_out, TH_out, dh_H, massflow_H,hC_in,hH_in]
            self.DH = dh_C*massflow_C
            self.TH_trace = TH
            self.TC_trace = TC
            self.T_pinch = min(abs(TH-TC))
            print("HX", self.label,"Delat T, pinch:",self.T_pinch,"[K]")
            self.PH_trace = PH
            self.PC_trace = PC
            self.T0 = T
            # adjust points on right
            #print(self.pR_H.label)
            #print(self.pR_C.label)

            #self.pR_H.setL(PH_out,TH_out)
            #self.pR_C.setL(PC_out,TC_out)
            self.pR_H.setL(PH_out,hH_out)
            self.pR_C.setL(PC_out,hC_out)
            self.temp_dist()

    ###
    def calc_mass(self):
        # function to return mass of working fluid stored in components
        # get inlet conditions
        # H-side
        if self.fluid_H.split(':')[0].upper() != 'CUSTOM':
            P,H  = self.pL_H.getR()
            rhoL = CP.PropsSI('D','P',P,'HMASS', H ,self.fluid_H)
            P,H  = self.pR_H.getR()
            rhoR = CP.PropsSI('D','P',P,'HMASS', H ,self.fluid_H)
            self.mass_H = 0.5*(rhoL + rhoR)*self.Volume_H
        else:
            self.mass_H = 0.
        # C-side
        if self.fluid_C.split(':')[0].upper() != 'CUSTOM':
            P,H  = self.pL_C.getR()
            rhoL = CP.PropsSI('D','P',P,'HMASS', H ,self.fluid_C)
            P,H  = self.pR_C.getR()
            rhoR = CP.PropsSI('D','P',P,'HMASS', H ,self.fluid_C)
            self.mass_C = 0.5*(rhoL + rhoR)*self.Volume_C
        else:
            self.mass_C = 0.
        self.mass = self.mass_C + self.mass_H
    ###
    def calc_cost(self):
        # Cost function retrieved from https://www.sciencedirect.com/science/article/pii/S0360544218302305?via%3Dihub
        LMTD = ((max(self.TH_trace) - min(self.TH_trace)) - (max(self.TC_trace) - min(self.TC_trace)))/\
               (np.log((max(self.TH_trace) - min(self.TH_trace))/(max(self.TC_trace) - min(self.TC_trace))))

        if ('sink' and 'air') in self.label:
            # Gas cooler (air and sCO2) plate heat exchanger
            factor = 36
        else:
            # PCHX heat exchanger cost factor
            factor = 2500

        self.cost = (factor*(abs(self.OP_Point[6]*self.MASS_C)/1000))/LMTD
    ###
    def temp_dist(self):

        def integrand(x):
            return np.exp(-x**2)/x

        time = 1e20
        radius = np.arange(self.Radius,10000,1)
        T0 = 250 #ground temperature

        self.temperature = []
        il = 0
        j = 0

        for j in range(len(radius)):
            il = radius[j]/(2*np.sqrt(self.alpha*time))
            I = quad(integrand, il, np.inf)
            self.temperature.append(self.OP_Point[6]*I[0]/(2*sci.pi*self.k)+T0)

        plt.plot(radius,self.temperature)
        plt.ylabel("Temperature (K)")
        plt.xlabel("Distance (m)")
        plt.show()

    ###
    def write(self,fp):
        '''
        function to write data to output file
        '''
        fp.write("TYPE = RECUP \n")
        fp.write("Label = "+self.label+"\n")
        fp.write("Point pL_H = "+self.pL_H.label+"\n")
        fp.write("Point pR_H = "+self.pR_H.label+" \n")
        fp.write("q_H (J/kg) = {0:0.8f} \n".format(self.OP_Point[11]))
        fp.write("Mass Flow Rate_H (kg/s) = {0:0.8f} \n".format(self.MASS_H))
        fp.write("Fluid mass_H (kg) = {0:0.8f} \n".format(self.mass_H))
        fp.write("Point pL_C = "+self.pL_C.label+"\n")
        fp.write("Point pR_C = "+self.pR_C.label+" \n")
        fp.write("q_C (J/kg) = {0:0.8f} \n".format(self.OP_Point[6]))
        fp.write("Mass Flow Rate_C (kg/s) = {0:0.8f} \n".format(self.MASS_C))
        fp.write("Fluid mass_C (kg) = {0:0.8f} \n".format(self.mass_C))
        fp.write("Cost ($) = {0:0.2f} \n".format(self.cost))
        fp.write("--- \n")
###
###
class MyError(Exception):
    def __init__(self, value):
        self.value = value
    def __str__(self):
        return repr(self.value)

# def calc_surf_area(self):
#     # Calculates the surface area of the radiator
#     BC = 5.669e-8 #Boltzmann constant
#     self.SA = self.OP_Point[6]/(BC*self.EFF*(self.TL^4-self.T^4))

# TL = CP.PropsSI('T','P',PL,'HMASS', HL ,self.fluid)
