#! /usr/bin/env python3
"""
Python Code to evaluate on and off-design performance of Cycles.

The cycles are parametrically defined in a job file, which then allows the
performance to be evaluated.

Author: Ingo Jahn
Last Modified: 20/02/2017
"""

import numpy as np
import datetime as datetime
import CoolProp as CP
import CoolProp.CoolProp as CPCP
import scipy as sci
import matplotlib.pyplot as plt
import sys as sys
import os as os
import shutil as shutil
from getopt import getopt
from Components import *
from SSCAR_plots import *
import pdb
from HX_fluids import *


class ETA:
    """Class to calculate efficiency of cycle base on power input and output."""

    def __init__(self):
        """Initialise class."""
        self.sources = []
        self.sinks = []

    def Qin(self, sources):
        """Set list of components that act as sources."""
        self.sources = sources

    def Qout(self, sinks):
        """Set list of components that act as sinks."""
        self.sinks = sinks

    def calc(self):
        """Function to calculate efficiency."""
        self.Qin = 0.
        for s in self.sources:
            if s.__class__.__name__ is 'ICE_SIMPLE':
                self.Qin = self.Qin + abs(s.Qfuel)
            else:
                self.Qin = self.Qin + abs(s.DH)
        self.Qout = 0.
        for s in self.sinks:
            if s.__class__.__name__ is 'ICE_SIMPLE':
                self.Qout = self.Qout - abs(s.Q_coolant)
            else:
                self.Qout = self.Qout - abs(s.DH)

        Qnet = self.Qin+self.Qout
        return self.Qin, self.Qout, Qnet


class GDATA:
    """Class containing global simulation parameters."""

    def __init__(self):
        """Initialise class."""
        self.iter = 0  # integer to set number of iterations for fsolve
        # self.m0 = 10. # set initial guess of mass flow [kg/s]
        self.name = ''  # set name of simualtion.
        self.print_flag = 0  # adjust how much data will be printed to display (for debugging)
        self.optim = 'root:hybr'  # select optimiser that will be used.
        self.relax = 0  # define relaxation parameter to slow down iteraction.


class MASS:
    """Class to define mass flow stream running through the cycle."""

    instances = []

    def __init__(self, m0, fluid='air', label='empty', mtype='free', ratio=1., mass_r=[]):
        """Initialise class."""
        # if mtype == 'free':
        self.__class__.instances.append(self)
        self.fluid = fluid
        self.label = label
        if not (mtype == 'free' or mtype == 'fixed' or mtype == 'ratio'):
            raise MyError('Error when defining instance of MASS' + self.label + ' \n Error: mtype set incorrectly')
        self.mtype = mtype
        if self.mtype == 'ratio':
            self.mass_r = mass_r
            self.ratio = ratio
            self.update()
        else:
            self.mass = m0
        if label is 'empty':
            self.label = "mass_"+len(self.instances)

    def update(self):
        """Update mass flow if mtype = ratio."""
        if self.mtype == 'ratio':
            m0 = 0.
            for a in self.mass_r:
                m0 = m0 + self.ratio * a.mass
            self.mass = m0

    def write(self, fp):
        """Write class data to output file."""
        fp.write("Label = "+self.label+" \n")
        fp.write("Mass Flow Rate (kg/s) = {0:0.8f} \n".format(self.mass))
        fp.write("Fluid Type = "+self.fluid+" \n")
        fp.write("mtype = "+self.mtype+" \n")
        if self.mtype == 'ratio':
            fp.write("ratio = "+str(self.ratio)+" \n")
            string = ""
            for m in self.mass_r:
                string = string+m.label+", "
            fp.write("[ " + string + " ] \n")
        fp.write("--- \n")


class SPEED:
    """Class to define a shaft that controls speed of two components."""

    instances = []

    def __init__(self, N0, label='empty', stype='free'):
        """Initialise class."""
        self.__class__.instances.append(self)
        self.label = label
        self.stype = stype
        self.N0 = N0
        self.speed = N0
        if label is 'empty':
            self.label = "speed_"+len(self.instances)

    def write(self, fp):
        """Write class data to output file."""
        fp.write("Label = {0} \n".format(self.label))
        fp.write("Speed (rad/s) = {0:0.8f} \n".format(self.speed))
        fp.write("stype = {0} \n".format(self.stype))
        fp.write("--- \n")


class POWER:
    """Class to define a shaft that controls power of two components."""

    instances = []

    def __init__(self, P0, label='empty', ptype='free'):
        """Initialise class."""
        self.__class__.instances.append(self)
        self.label = label
        self.ptype = ptype
        self.P0 = P0
        self.power = P0
        if label is 'empty':
            self.label = "power_"+len(self.instances)

    def clear_power_balance(self):
        """Set power balance to zero."""
        self.power_balance = 0.

    def add_Power(self, addPower):
        """Combine power contributions."""
        self.power_balance = self.power_balance + addPower

    def write(self, fp):
        """Write class data to output file."""
        fp.write("Label = {0} \n".format(self.label))
        fp.write("Power (W) = {0:0.8f} \n".format(self.power))
        fp.write("ptype = {0} \n".format(self.ptype))
        fp.write("--- \n")


class POINT:
    """Class definint a link point between component."""

    instances = []

    def __init__(self, P0, T0, Q0=-1., label='empty', ptype='free'):
        """Initialise class."""
        self.__class__.instances.append(self)
        self.ptype = ptype
        self.P0 = P0
        self.T0 = T0
        self.Q0 = Q0
        self.ptype = ptype
        self.label = label
        self.set_label(label)
        self.PL = []
        self.PR = []
        self.TL = []
        self.TR = []
        self.HL = []
        self.HR = []
        self.ML = []
        self.MR = []
        self.CL = []    # list of components connected to left
        self.CR = []    # list of components connected to right
        self.Eos = []
        self.fluid = []
        self.check()
        self.initialise()
        # self.update_MASS()
        if label is 'empty':
            self.label = "pt_{}".format(len(self.instances))

    def check(self):
        """Check that inputs are correct."""
        ptypeList = ['free', 'P_fixed', 'T_fixed', 'PT_fixed']
        if self.ptype not in ptypeList:
            raise MyError("Point: {0}; setting for ptype={1} is not supported. \
                          Available ptype options are: {2}".format(self.label, self.ptype, ptypeList))

    def norm(self, M, P, H):
        """Set normalisation parameters."""
        self.normM = M
        self.normP = P
        # self.normT = T
        self.normH = H

    def initialise(self):
        """Initilaise P, T and Q on left & right of each point."""
        self.PL = self.P0
        self.PR = self.P0
        self.TL = self.T0
        self.TR = self.T0
        self.QL = self.Q0
        self.QR = self.Q0

    def set_limits(self):
        """Set limits P_min, P_max, T_min, T_max for current fluid Equation of State."""
        props = [CP.iP_min, CP.iP_max, CP.iT_min, CP.iT_max]
        state = [CP.HmassP_INPUTS, self.HL, self.PL]
        # print("Output-L", GetFluidPropLow(self.Eos, state, props))
        if GetFluidPropLow(self.Eos, state, props) is None:
            raise MyError("Initial conditions set at Point {} exceed limits of fluid model. Bailing Out.".format(self.label))
        else:
            [self.P_min, self.P_max, self.T_min, self.T_max] = GetFluidPropLow(self.Eos, state, props)

    def set_enthalpy(self):
        """Based on properties (P, T, Q) set the enthalpy of the point."""
        if not self.Eos:
            if self.fluid.split(':')[0].upper() == 'CUSTOM':
                self.Eos = CustomFluid(self.fluid.split(':')[1])
            else:
                self.Eos = CP.AbstractState('HEOS', self.fluid)

        # once fluid type is available, evaluate enthalpy at point.
        if self.QL == -1:
            # self.H0 = CPCP.PropsSI('HMASS','P',self.PL,'T',self.TL,self.fluid)
            self.H0 = GetFluidPropLow(self.Eos, [CP.PT_INPUTS, self.PL, self.TL], [CP.iHmass])
        else:
            # self.H0 = CPCP.PropsSI('HMASS','P',self.PL,'Q',self.QL,self.fluid)
            self.H0 = GetFluidPropLow(self.Eos, [CP.PQ_INPUTS, self.PL, self.QL], [CP.iHmass])
        self.HL = self.H0
        self.HR = self.H0

    def limit_to_range(self):
        """Apply limit so that properties remain within Equation of State limitations."""
        # function that limits temperature and increases temperature to avoid crashes range to match EOS
        if not self.Eos:
            if self.fluid.split(':')[0].upper() == 'CUSTOM':
                self.Eos = CustomFluid(self.fluid.split(':')[1])
            else:
                self.Eos = CP.AbstractState('HEOS', self.fluid)

        # check if there is a negative pressure exceedance
        if self.PL < self.P_min:
            self.PL = self.P_min
            print("Point {0}, PL={1:.1f} kPa, changed to PL={2:.1f} kPa".format(self.label, self.PL, self.P_min))
        elif self.PL > self.P_max:
            self.PL = self.P_max
            print("Point {0}, PL={1:.1f} kPa, changed to PL={2:.1f} kPa".format(self.label, self.PL, self.P_max))
        if self.PR < self.P_min:
            self.PR = self.P_min
            print("Point {0}, PR={1:.1f} kPa, changed to PR={2:.1f} kPa".format(self.label, self.PR, self.P_min))
        elif self.PR > self.P_max:
            self.PR = self.P_max
            print("Point {0}, PR={1:.1f} kPa, changed to PR={2:.1f} kPa".format(self.label, self.PR, self.P_max))

        # check if enthalpy exceeds the limit.
        props = [CP.iT]
        state = [CP.HmassP_INPUTS, self.HL, self.PL]
        if GetFluidPropLow(self.Eos, state, props) is None:  # no solution found, hence T outside range
            props = [CP.iHmass]
            # try T_min
            state = [CP.PT_INPUTS, self.PL, self.T_min+1.]  # increase T_min by +1 to increase stability of CoolProp solver
            HL_temp = float(GetFluidPropLow(self.Eos, state, props))
            if HL_temp > self.HL:  # if H is increased, this indicates exceedance at T_min side
                self.HL = HL_temp
            else:
                state = [CP.PT_INPUTS, self.PL, self.T_max]
                self.HL = float(GetFluidPropLow(self.Eos, state, props))

        props = [CP.iT]
        state = [CP.HmassP_INPUTS, self.HR, self.PR]
        if GetFluidPropLow(self.Eos, state, props) is None:  # no solution found, hence T outside range
            props = [CP.iHmass]
            # try T_min
            state = [CP.PT_INPUTS, self.PR, self.T_min+1.]  # increase T_min by +1 to increase stability of CoolProp solver
            HL_temp = float(GetFluidPropLow(self.Eos, state, props))
            if HL_temp > self.HR:  # if H is increased, this indicates exceedance at T_min side
                self.HR = HL_temp
            else:
                state = [CP.PT_INPUTS, self.PR, self.T_max]
                self.HR = float(GetFluidPropLow(self.Eos, state, props))
        return 0

    def get_TQ(self):
        """Extract temperature and quality from enthalpy."""
        if not self.Eos:
            if self.fluid.split(':')[0].upper() == 'CUSTOM':
                self.Eos = CustomFluid(self.fluid.split(':')[1])
            else:
                self.Eos = CP.AbstractState('HEOS', self.fluid)

        props = [CP.iT, CP.iQ]
        state = [CP.HmassP_INPUTS, self.HL, self.PL]
        [self.TL, self.QL] = GetFluidPropLow(self.Eos, state, props)
        state = [CP.HmassP_INPUTS, self.HR, self.PR]
        [self.TR, self.QR] = GetFluidPropLow(self.Eos, state, props)
        self.TL = np.float(self.TL)
        self.TR = np.float(self.TR)  # ensure that subsequent formating is possible
        self.QL = np.float(self.QL)
        self.QR = np.float(self.QR)

    def set_label(self, label):
        """Set chronological point labels if not defined."""
        L = []
        for l in self.__class__.instances:
            L.append(l.label)
        L = L[:-1]  # remove last label that has been added as it refers to self.
        if any(label in s for s in L):
            print('WARNING: point label defined multiple times')
            label_new = label+str(len(self.__class__.instances)-1)
            print('Point ', self.label, ' renamed to: ', label_new)
            self.label = label_new
        else:
            self.label = label

    def getL(self):
        """Return self.PL, self.TL."""
        return self.PL, self.HL

    def getR(self):
        """Return self.PR, self.TR."""
        return self.PR, self.HR

    def setL(self, A, B):
        """Set properties on left."""
        self.PL = A
        self.HL = B

    def setR(self, A, B):
        """Set properties on right."""
        if self.ptype == 'free':
            self.PR = A
            # self.TR = B
            self.HR = B
        elif self.ptype == 'P_fixed':
            self.PR = self.P0  # reset PR to P0
            # self.TR = B
            self.HR = B
        elif self.ptype == 'T_fixed':
            self.PR = A
            # self.TR = self.T0 # reset TR to T0
            # self.HR = CPCP.PropsSI('HMASS','P',self.PR,'T',self.T0,self.fluid)
            self.HR = GetFluidPropLow(self.Eos, [CP.PT_INPUTS, self.PR, self.T0], [CP.iHmass])
        elif self.ptype == 'PT_fixed':
            self.PR = self.P0  # reset PR to P0
            # self.TR = self.T0  # reset TR to T0
            # self.HR = CPCP.PropsSI('HMASS','P',self.PR,'T',self.T0,self.fluid)
            self.HR = GetFluidPropLow(self.Eos, [CP.PT_INPUTS, self.PR, self.T0], [CP.iHmass])
        else:
            raise MyError('Setting of Point.ptype wrong for POINT ' + self.label)

    def setRnorm(self, A, B):
        """Set normalised properties on right."""
        if self.ptype == 'free':
            self.PR = A*self.normP
            # self.TR = B*self.normT
            self.HR = B*self.normH
        elif self.ptype == 'P_fixed':
            self.PR = self.P0  # reset PR to P0
            # self.TR = B*self.normT
            self.HR = B*self.normH
        elif self.ptype == 'T_fixed':
            self.PR = A*self.normP
            # self.TR = self.T0  # reset TR to T0
            # self.HR = CPCP.PropsSI('HMASS','P',self.PR,'T',self.T0,self.fluid)
            self.HR = GetFluidPropLow(self.Eos, [CP.PT_INPUTS, self.PR, self.T0], [CP.iHmass])
        elif self.ptype == 'PT_fixed':
            self.PR = self.P0  # reset PR to P0
            # self.TR = self.T0  # reset TR to T0
            # self.HR = CPCP.PropsSI('HMASS','P',self.PR,'T',self.T0,self.fluid)
            self.HR = GetFluidPropLow(self.Eos, [CP.PT_INPUTS, self.PR, self.T0], [CP.iHmass])
        else:
            raise MyError('Setting of Point.ptype wrong for POINT ' + self.label)

    def get_diff(self):
        """Get difference in properties between left & right."""
        # print('get_diff for point :', self.label)
        # print('PL and PR :', self.PL, self.PR)
        # print('TL and TR :', self.TL, self.TR)
        out = self.PR-self.PL, self.HR-self.HL
        return out

    def get_diffnorm(self):
        """Get normalised difference in properties between left & right."""
        # print('get_diff for point :', self.label)
        # print('PL and PR :', self.PL, self.PR)
        # print('TL and TR :', self.TL, self.TR)
        out = (self.PR-self.PL)/self.normP, (self.HR-self.HL)/self.normH
        return out

    def write(self, fp):
        """Write class data to output file."""
        fp.write("Label = "+self.label+" \n")
        fp.write("P0 (Pa) = {0:0.8f} \n".format(self.P0))
        fp.write("T0 (K) = {0:0.8f} \n".format(self.T0))
        fp.write("Q0 (K) = {0:0.8f} \n".format(self.Q0))
        fp.write("ptype = "+self.ptype+" \n")
        fp.write("Fluid = "+self.fluid+" \n")
        fp.write("PL (Pa) = {0:0.8f} \n".format(self.PL))
        fp.write("TL (K) = {0:0.8f} \n".format(self.TL))
        if self.fluid.split(':')[0].upper() == 'CUSTOM':
            fp.write("hL (J/(kg)) = n/a \n")
            fp.write("QL (-) = n/a \n")
        else:
            fp.write("hL (J/(kg)) = {0:0.8f} \n".format(self.HL))
            fp.write("QL (-) = {0:0.8f} \n".format(self.QL))
        fp.write("PR (Pa) = {0:0.8f} \n".format(self.PR))
        fp.write("TR (K) = {0:0.8f} \n".format(self.TR))
        if self.fluid.split(':')[0].upper() == 'CUSTOM':
            fp.write("hR (J/(kg)) = n/a \n")
            fp.write("QR (-) = n/a \n")
        else:
            fp.write("hR (J/(kg)) = {0:0.8f} \n".format(self.HR))
            fp.write("QR (-) = {0:0.8f} \n".format(self.QR))
        fp.write("Error P (Pa) = {0:0.8f} \n".format(self.PR-self.PL))
        fp.write("Error T (K) = {0:0.8f} \n".format(self.TR-self.TL))
        fp.write("--- \n")


def equations(A0, B, Components, POINT, MASS, SPEED, POWER, gdata, flag=0):
    """
    function that evaluates the cycle operating point
    Inputs:
    A0 - List containing solution variables
        [m, P2, P3, ... PN, T1, T2, ... TN ]
    B - List containing normalisation parmeters for A
        [m0, P0, T0 ] (in kg/s, Pa, K)
    p1 - reference pressure used to anchor the simualtion (Pa)
         refers to first point that has been defined.
    Components - List containing componets
    POINT -  List containing points
    MASS - List containing mass
    S_POINT -   List containing shaft points
    gdata - class containing gdata setting parameters
    flag - set display of results on screen

    Outputs:
    error - vector containing miss-balance in mass flow, pressure and temperature.
    """

    if flag > 1:
        print("\n")
        print("++++++++++++++++++++++++++++++++++")
        print("+++ New Evaluation Loop ++++++++++")
        print("A0", A0)

    # re-set power balance
    for p in POWER.instances:
        p.clear_power_balance()
    # set relaxation parameter:
    # relax = 0 --> no relaxation.
    # relax = 1 --> old solution is re-used.
    relax = gdata.relax

    # create data vector from A0
    i = 0
    # extract pressure and temperature data and set accorndingly
    for p in POINT.instances:
        # p.update_MASS()
        if p.ptype is 'P_fixed' or p.ptype == 'P_source':
            # p.setRnorm('nan',A0[i])
            p.setRnorm('nan', relax * p.HR/p.normH + (1.-relax) * A0[i])
            i = i+1
        elif p.ptype is 'T_fixed' or p.ptype == 'T_source':
            # p.setRnorm(A0[i],'nan')
            p.setRnorm(relax * p.PR/p.normP + (1.-relax) * A0[i], 'nan')
            i = i+1
        elif p.ptype is 'PT_fixed' or p.ptype == 'PT_source':
            p.setRnorm('nan', 'nan')
        else:
            # p.setRnorm(A0[i],A0[i+1])
            p.setRnorm(relax * p.PR/p.normP + (1.-relax) * A0[i], relax * p.HR/p.normH + (1.-relax) * A0[i+1])
            i = i+2
    # extract mass data and set accordingly
    for m in MASS.instances:
        if m.mtype == 'free':
            # m.mass = abs(A0[i]*B[0])
            m.mass = relax * m.mass + (1.-relax) * abs(A0[i]*B[0])
            i = i+1
    # extract speed data and set accordingly
    for s in SPEED.instances:
        if s.stype == 'free':
            s.speed = abs(A0[i]*B[3])
            i = i+1
    # extract power and set accordingly
    for p in POWER.instances:
        if p.ptype == 'free':
            p.power = abs(A0[i]*B[4])
            i = i+1

    # update the mass flows defined as a ratio of others.
    for m in MASS.instances:
        m.update()

    # limit to validity range of EOS
    for point in POINT.instances:
        point.limit_to_range()

    # check that correct parameters have been set to right side of each point
    if flag > 1:
        # reconstruct temperatures and quality at points
        for point in POINT.instances:
            point.get_TQ()

        print('massflow :', ["{0:0.2f}".format(m.mass) for m in MASS.instances], '(kg/s)')
        print('speed    :', ["{0:0.2f}".format(s.speed) for s in SPEED.instances], '(rad/s)')
        print('power    :', ["{0:0.2f}".format(p.power) for p in POWER.instances], '(W)')
        print('P :', ["{0:0.2f}".format(p.PR/1.e3) for p in POINT.instances], '(3Pa)')
        print('T :', ["{0:0.2f}".format(p.TR) for p in POINT.instances], '(K)')
        print('H :', ["{0:0.2f}".format(p.HR/1.e3) for p in POINT.instances], '(kJ/kg.K)')

    # evaluate conditions on other side of each component (typically right)
    for c in Components:
        c.evalR()

    error = []
    # get difference in conditions at each point
    for p in POINT.instances:
        if p.ptype is 'PT_fixed':
            pass
        else:
            # dP, dT = p.get_diffnorm()
            dP, dH = p.get_diffnorm()
            error.append(dP)
            # error.append(dT)
            error.append(dH)

    # add extra constraint that exists at MERGEs
    for c in Components:
        if c.__class__.__name__ is 'MERGE':
            if c.mtype != 'fixed':
                error.append(c.get_diff()/B[0])
        if c.__class__.__name__ is 'COMP_MAP_SP':
            error.append((abs(c.POWER) - abs(c.POWER_thermo))/B[4])
        if c.__class__.__name__ is 'TURB_MAP_SP':
            error.append((abs(c.POWER) - abs(c.POWER_thermo))/B[4])
        if c.__class__.__name__ is 'TURB_AX_SP':
            error.append((abs(c.POWER) - abs(c.POWER_thermo))/B[4])
        # elif c.__class__.__name__ is 'ICE_SIMPLE':
        #    error.append(c.get_diff()/B[0] )

    # look at power balance for shafts
    # or p in POWER.instances:
    #    #if p.type != 'fixed':
    #    error.append(p.power_balance/B[4] )

    # print debugging data
    if flag > 1:
        # reconstruct temperatures and quality at points
        for point in POINT.instances:
            point.get_TQ()

        print('Mass flow rate :', ["{0:0.2f}".format(m.mass) for m in MASS.instances], '(kg/s)')
        print('Speed          :', ["{0:0.2f}".format(s.speed) for s in SPEED.instances], '(rad/s)')
        print('Power          :', ["{0:0.2f}".format(p.power) for p in POWER.instances], '(W)')
        print('Pressures      :', ["{0:0.2f}".format(p.PR/1.e3) for p in POINT.instances], '(kPa)')
        print('Temperature    :', ["{0:0.2f}".format(p.TR) for p in POINT.instances], '(K)')
        print('Enthalpy       :', ["{0:0.2f}".format(p.HR/1.e3) for p in POINT.instances], '(kJ/kg.K)')
        print('Shaft Power    :', ["{0:0.2f}".format(p.power/1e3) for p in POWER.instances], '(kW)')
        print('error          :', ["{0:0.2e}".format(e) for e in error])

        print('End of Evaluation Loop')

        for point in POINT.instances:
            # print(point.label, ' P (MPa) T(K) left:', point.PL/1.e6, point.TL, 'right :' , point.PR/1.e6, point.TR)
            print(point.label, ' P (kPa) H(kJ/kg.K) left:', point.PL/1.e3, point.HL/1.e3, 'right :', point.PR/1.e3, point.HR/1.e3)
            # print(point.label, ' Error P (MPa): ', point.PL/1.e6-point.PR/1.e6, ' T (K):', point.TL-point.TR)
            print(point.label, ' Error P (kPa): ', point.PL/1.e3-point.PR/1.e3, ' H (kJ/kg.K):', point.HL/1.e3-point.HR/1.e3)

        for c in Components:
            if c.__class__.__name__ is 'MERGE':
                print(c.type, c.label, 'Mass flow (kg/s) - L):', c.MASS_L0, ' L1:', c.MASS_L1, ' R:', c.MASS_R)
            elif c.__class__.__name__ is 'RECUP':
                print(c.type, c.label, 'Mass flow (kg/s) - H):', c.MASS_H, ' C:', c.MASS_C)
            elif c.__class__.__name__ is 'GCHP':
                print(c.type, c.label, 'Mass flow (kg/s) - H):', c.MASS_H, ' C:', c.MASS_C)
            elif c.__class__.__name__ is 'ICE_SIMPLE':
                print(c.type, c.label, 'Mass flow (kg/s) - H): tbc')
            else:
                print(c.type, c.label, 'Mass flow (kg/s) - L):', c.MASS)

    return error


def main(uoDict):
    """Main function call."""

    # create string to collect warning messages
    warn_str = "\n"

    # main file to be executed
    jobFileName = uoDict.get("--job", "test")

    # Make SSCAR run even if .py extension is forgotten
    if ".py" not in jobFileName:
        jobName = jobFileName
        jobFileName = ''.join([jobFileName, ".py"])
    else:
        # strip .py extension from jobName
        jobName = jobFileName.replace('.py', '')

    # Trying to execute from a relative directory, not supported
    if os.sep in jobFileName:
        relative_dir = jobFileName.split(os.sep)[:-1]
        abs_dir = os.path.abspath(os.path.join(*relative_dir))
        jobName = jobFileName.split(os.sep)[-1]
        raise MyError("SSCAR does not currently support relative imports. \
                      Please change directory to {0} and retry with command: \n\n \
                      $ SSCAR.py --job={1} \n".format(abs_dir, jobName))

    # Make sure that the desired job file actually exists
    if jobFileName not in os.listdir('.'):
        local_directory = os.path.dirname(os.path.realpath(__file__))
        raise MyError("No job file {0} found in directory: {1}".format(jobFileName, local_directory))

    # get in-file and out-file name
    out_file = uoDict.get("--out-file", "none")
    in_file = uoDict.get("--in-file", "none")

    print_flag = 0
    # set print_flag (can be overwritten from jobfile)
    print_flag = float(uoDict.get("--verbosity", 0))

    # if uoDict.has_key("--print"):
    #    print_flag = 2

    gdata = GDATA()
    eta = ETA()

    # clear component instances. This is required to allow batch processing. Otherwise repeated calls of the cycle. E.g. by calling Cycle.main(uodict) will see the previously defined instances.
    POINT.instances = []  # Points
    MASS.instances = []    # Mass flows
    SPEED.instances = []  # Speeds
    POWER.instances = []  # Power
    HX.instances = []     # Components
    RECUP.instances = []
    RECUP_HALF.instances = []
    TURB.instances = []
    TURB_MAP_SP.instances = []
    TURB_AX_SP.instances = []
    TURB_ER.instances = []
    TURB_POWER.instancs = []
    COMP.instances = []
    COMP_MAP_SP.instances = []
    COMP_MASSF.instances = []
    COMP_PR.instances = []
    COMP_POWER.instances = []
    MERGE.instances = []
    PIPE.instances = []
    EX_OR.instances = []
    SHAFT.instances = []
    ICE_SIMPLE.instances = []
    ICE_ADVANCED_0.instances = []
    RAM_AIR_INTAKE.instances = []
    RAD.instances = []
    GCHP.instances = []

    # Execute jobFile, this creates all the variables
    exec(open(jobFileName).read(), globals(), locals())
    # execfile(jobFileName,globals(),locals())

    # update print flag
    if gdata.print_flag > print_flag:
        print_flag = gdata.print_flag

    if print_flag >= 1:
        print("Cycle Input data file read")

    # set point properties based on in_file
    if in_file is not "none":
        print("Loading mass flow and point data from: "+in_file)
        print("Values set in "+jobFileName+" will be overwritten.")
        print("\n")

        # open file
        fp = open(in_file, 'r')
        lines = fp.readlines()

        for m in MASS.instances:  # set mass instances
            phrase = "Label = "+m.label
            datalabel = "Mass Flow Rate (kg/s)"
            pointer = 1
            for i, line in enumerate(lines):  # find line number
                if phrase in line:
                    break
            line = lines[i+pointer].split('=')
            if line[0].rstrip() == datalabel:
                m.mass = float(line[1].rstrip())
            else:
                raise MyError("Error when reading input file. Pointer for Mass \
                              Flow Rate for {0} points to incorrect line.".format(phrase))
            if print_flag >= 2:
                print("MASS: {0}, has been set to {1} = {2}".format(phrase, datalabel, m.mass))
        ####
        for p in POINT.instances:  # set point instances
            phrase = "Label = "+p.label
            datalabel = "PL (Pa)"
            pointer = 6
            for i, line in enumerate(lines):  # find line number
                if phrase in line:
                    break
            line = lines[i+pointer].split('=')
            if line[0].rstrip() == datalabel:
                p.P0 = float(line[1].rstrip())
            else:
                raise MyError("Error when reading input file. Pointer for PL \
                              for {0} points to incorrect line.".format(phrase))
            if print_flag >= 2:
                print("POINT: {0}, has been set to {1} = {2}".format(phrase, datalabel, p.P0))

            datalabel = "TL (K)"
            pointer = 7
            for i, line in enumerate(lines):  # find line number
                if phrase in line:
                    break
            line = lines[i+pointer].split('=')
            if line[0].rstrip() == datalabel:
                p.T0 = float(line[1].rstrip())
            else:
                raise MyError("Error when reading input file. Pointer for TL \
                              for {0} points to incorrect line.".format(phrase))
            if print_flag >= 2:
                print("POINT: {0}, has been set to {1} = {2}".format(phrase, datalabel, p.T0))

            datalabel = "QL (-)"
            pointer = 9
            for i, line in enumerate(lines):  # find line number
                if phrase in line:
                    break
            line = lines[i+pointer].split('=')
            if line[0].rstrip() == datalabel:
                if not line[1].rstrip() == ' n/a':
                    p.Q0 = float(line[1].rstrip())
            else:
                raise MyError("Error when reading input file. Pointer for QL \
                              for {0} points to incorrect line.".format(phrase))
            if print_flag >= 2:
                print("POINT: {0}, has been set to {1} = {2}".format(phrase, datalabel, p.Q0))

            p.initialise()  # re-run the initialise routine to set properties based on P0, T0, Q0

        for s in SPEED.instances:  # set point instances
            raise MyError("Loading function currently doesnt support SPEED. \
                          Extra code needs to be added.")

        for p in POWER.instances:  # set point instances
            raise MyError("Loading function currently doesnt support POWER. \
                          Extra code needs to be added.")

        fp.close()
        if print_flag >= 1:
            print("Input file has been reads and POINT and MASS data is updated.")

    # create list of Components # NEED TO new components to this list so that
    # they can be picked up by solver.
    Components = HX.instances + RECUP.instances + RECUP_HALF.instances \
        + TURB.instances + TURB_MAP_SP.instances + TURB_AX_SP.instances \
        + TURB_ER.instances + TURB_POWER.instances \
        + COMP.instances + COMP_MAP_SP.instances + COMP_MASSF.instances \
        + COMP_PR.instances + COMP_POWER.instances \
        + MERGE.instances + PIPE.instances + EX_OR.instances \
        + ICE_SIMPLE.instances + ICE_ADVANCED_0.instances \
        + RAM_AIR_INTAKE.instances + RAD.instances + GCHP.instances
    # + SPLIT.instances + COMP_SLAVE.instances + FIXED_P.instances
    # print Components

    # Set fluid type for components
    for c in Components:
        c.set_fluid()
    # Initialise enthalpy for points
    for p in POINT.instances:
        p.set_enthalpy()
        p.set_limits()

    #################
    # main code

    # create parameters to normalise the data
    B = []
    # mass flow
    M = []
    for m in MASS.instances:
        M.append(m.mass)
    if len(M) == 0:
        B.append(1.)  # placeholder
    else:
        B.append(np.average(np.array(M)))  # Set B[0] to be average mass flow of points
    # for pressure and temperature
    P = []
    H = []
    T = []
    for p in POINT.instances:
        P.append(p.P0)
        T.append(p.T0)
        H.append(p.H0)
    if len(P) == 0:
        B.append(1.)  # set B[1] to average pressure of points
        B.append(1.)  # set B[2] to average enthalpy of points
    else:
        B.append(np.average(np.array(P)))  # set B[1] to average pressure of points
        # B.append(np.average(np.array(T)))  # set B[2] to average temperature of points
        B.append(np.average(np.array(H)))  # set B[2] to average enthalpy of points
    S = []
    for s in SPEED.instances:
        S.append(s.speed)
    if len(S) == 0:
        B.append(1.)  # placeholder
    else:
        B.append(np.average(np.array(S)))  # Set B[3] to be average speed
    Po = []
    for p in POWER.instances:
        Po.append(p.power)
    if len(Po) == 0:
        B.append(1.)  # placeholder
    else:
        B.append(np.average(np.array(Po)))  # Set B[4] to be average power

    # set normalisation parameters in points
    for p in POINT.instances:
        p.norm(B[0], B[1], B[2])

    print(B)

    # create data vector
    A0 = []
    for p in POINT.instances:
        if p.ptype is 'P_fixed':
            # A0.append(p.T0/B[2])
            A0.append(p.H0/B[2])
        elif p.ptype is 'T_fixed':
            A0.append(p.P0/B[1])
        elif p.ptype is 'free':
            A0.append(p.P0/B[1])
            # A0.append(p.T0/B[2])
            A0.append(p.H0/B[2])
        elif p.ptype is 'PT_fixed':
            pass  # do nothing add no equation.
        else:
            raise MyError('ptype incorrectly defined for POINT: '+p.label)
    for m in MASS.instances:
        if m.mtype == 'free':
            A0.append(m.mass/B[0])
    for s in SPEED.instances:
        if s.stype == 'free':
            A0.append(s.speed/B[3])
    for p in POWER.instances:
        if p.ptype == 'free':
            A0.append(p.power/B[4])

    # Display Initial Conditions.
    if print_flag > 1:
        print('INITIAL CONDITIONS:')
        print('P, T, H, M, S, Po:', P, T, H, M, S, Po)
        print(A0, B)
        print(Components)
        print('------------------------------')

    # Checking that correct number of unknowns and equations exists
    # number of unknowns
    N_variables = len(A0)
    # number of equations
    N_equations = 0
    for c in Components:
        if c.__class__.__name__ is 'MERGE':
            if c.mtype != 'fixed':
                N_equations = N_equations + 1
        """
        if c.__class__.__name__ is 'COMP_POWER':
            if c.sL != None or c.sR != None:
                N_equations = N_equations + 1
        if c.__class__.__name__ is 'TURB_POWER':
            if c.sL != None or c.sR != None:
                N_equations = N_equations + 1
        """
        if c.__class__.__name__ is 'COMP_MAP_SP':
            N_equations = N_equations + 1
        if c.__class__.__name__ is 'TURB_MAP_SP':
            N_equations = N_equations + 1
        if c.__class__.__name__ is 'TURB_AX_SP':
            N_equations = N_equations + 1

        # if c.__class__.__name__ is 'ICE_SIMPLE':
        #    N_equations = N_equations + 1

    for p in POINT.instances:
        if p.ptype == 'PT_fixed':
            pass
        # elif p.ptype == 'P_fixed':
        #     N_equations = N_equations + 1
        # elif p.ptype == 'T_fixed':
        #     N_equations = N_equations + 1
        else:
            N_equations = N_equations + 2

    if N_equations > N_variables:
        print("N_equations = ", N_equations)
        print("N_variables = ", N_variables)
        print('POINTS:', POINT.instances)
        print('MASS:', MASS.instances)
        raise MyError("More equations than variables. The problem is over-constraint and cannot be solved")
    elif N_equations < N_variables:
        print("N_equations = ", N_equations)
        print("N_variables = ", N_variables)
        print('POINTS:', POINT.instances)
        print('MASS:', MASS.instances)
        raise MyError("Less equations than variables. The problem is under-constraint and cannot be solved")
    else:
        if print_flag > 0:
            print("\n")
            print("Equation Check Passed")
            print("N_equations = ", N_equations)
            print("N_variables = ", N_variables)

    max_fev = gdata.iter*(N_variables+1)  # caculate maximum number of function evaluations. 0 --> till convergence

    args = (B, Components, POINT, MASS, SPEED, POWER, gdata, print_flag)  # set list of optional inputs used by optimiser.

    # employ non linear equations solver.
    if gdata.optim == 'fsolve':
        # use fsolve to solve equations
        A, infodict, status, mesg = sci.optimize.fsolve(
            equations, A0, args=args, full_output=1, factor=100.,
            maxfev=max_fev, epsfcn=0.0001
            )
    elif gdata.optim == 'root:lm':
        sol = sci.optimize.root(equations, A0, args=args, method='lm',
                                options={'eps': 1.e-3, 'xtol': 1.e-12,
                                         'ftol': 1e-12, 'maxfev': max_fev})
        status = sol.status
        A = sol.x
        mesg = sol.message
    elif gdata.optim == 'root:Newton-CG':
        sol = sci.optimize.root(equations, A0, args=args, method='lm',
                                options={'eps': 1.e-3, 'xtol': 1.e-12,
                                         'maxfev': max_fev})
        status = sol.status
        A = sol.x
        mesg = sol.message
    elif gdata.optim == 'root:df-sane':
        sol = sci.optimize.root(equations, A0, args=args, method='df-sane',
                                options={'ftol': 1.e-12,  'maxfev': max_fev})
        status = sol.status
        A = sol.x
        mesg = sol.message
    elif gdata.optim == 'root:hybr':
        sol = sci.optimize.root(equations, A0, args=args, method='hybr',
                                options={'xtol': 1.e-8, 'maxfev': max_fev})
        status = sol.status
        A = sol.x
        mesg = sol.message
    else:
        raise MyError("gdata.optim = '' not set properly.")

    # calculate mass in cycle
    Mass = 0.
    for a in Components:
        a.calc_mass()   # caclulate mass
        Mass = Mass + a.mass

    # calculate cost of components
    Cost = 0.
    # Assumes LMTD for HX and half_RECUP components is 10 by default (in Components.py)
    # To treat a RECUP or half_RECUP component as a gas cooler for costing, include 'sink' and 'air' in the label
    for a in Components:
        a.calc_cost()   # calculate cost
        Cost = Cost + a.cost

    Cost = Cost * 1.3   # Apply installation factor of 30%

    # reconstruct temperatures and quality at points
    for point in POINT.instances:
        point.get_TQ()

    PP = []
    TT = []
    QQ = []
    HH = []
    # get min and max pressure and temperature:
    for point in POINT.instances:
        PP.append(point.PL)
        PP.append(point.PR)
        TT.append(point.TL)
        TT.append(point.TR)
        QQ.append(point.QL)
        QQ.append(point.QR)
        HH.append(point.HL)
        HH.append(point.HR)

    T_min = min(TT)
    T_max = max(TT)
    P_min = min(PP)
    P_max = max(PP)

    # calculate carnot efficiency
    eta_carnot = (T_max-T_min) / T_max

    # create list of fluids
    Fluids = []
    for m in MASS.instances:
        if m.fluid not in Fluids:
            Fluids.append(m.fluid)

    if print_flag > 0 or (out_file is not "none"):
        print("\n")
        print("Preparing data for output")
        # extract cycle data:
        # trubine
        T_label = []
        T_eta = []
        T_deltaH = []
        for a in TURB.instances+TURB_MAP_SP.instances+TURB_AX_SP.instances+TURB_ER.instances+TURB_POWER.instances:
            T_label.append(a.label)
            T_eta.append(a.OP_Point[7])
            T_deltaH.append(a.OP_Point[0]*a.OP_Point[6])
        # compressor
        C_label = []
        C_eta = []
        C_deltaH = []
        for a in COMP.instances+COMP_MAP_SP.instances+COMP_MASSF.instances+COMP_PR.instances+COMP_POWER.instances:
            C_label.append(a.label)
            C_eta.append(a.OP_Point[7])
            C_deltaH.append(a.OP_Point[0]*a.OP_Point[6])
        # internal combustion engine
        ICE_label = []
        ICE_eta = []
        ICE_Wshaft = []
        for a in ICE_SIMPLE.instances:
            ICE_label.append(a.label)
            ICE_eta.append(a.eff)
            ICE_Wshaft.append(a.ShaftPower)
        # heat exchanger
        HX_label = []
        HX_deltaHin = [0]
        HX_deltaHout = [0]
        for a in HX.instances:
            HX_label.append(a.label)
            # print a.label, a.OP_Point[0]*a.OP_Point[6]
            if a.OP_Point[0]*a.OP_Point[6] > 0:
                HX_deltaHin.append(a.OP_Point[0]*a.OP_Point[6])
            if a.OP_Point[0]*a.OP_Point[6] < 0:
                HX_deltaHout.append(a.OP_Point[0]*a.OP_Point[6])

        W_turb = sum(T_deltaH)
        W_comp = sum(C_deltaH)
        W_ice = sum(ICE_Wshaft)
        W_net = -W_turb - W_comp + W_ice

        Q_in, Q_out, Q_net = eta.calc()

        Shaft_label = []
        Shaft_speed = []
        Shaft_power = []

    if print_flag > 0:
        print('++++++++++++++++++++++++++++++++++++++++')
        print('RESULTS:')
        print('Mass flow rate :', ["{0:0.3f}".format(m.mass) for m in MASS.instances], '(kg/s)')
        print('Pressures      :', ["{0:0.2f}".format(p.PR/1.e3) for p in POINT.instances], '(kPa)')
        print('Enthalpy       :', ["{0:0.2f}".format(p.HR/1.e3) for p in POINT.instances], '(kJ/kg.K)')
        print('Temperature    :', ["{0:0.2f}".format(p.TR) for p in POINT.instances], '(K)')
        print('Quality        :', ["{0:0.2f}".format(p.QR) for p in POINT.instances], '(-)')
        print('Speed          :', ["{0:0.2f}".format(s.speed) for s in SPEED.instances], '(rad/s)')
        print('Power          :', ["{0:0.3f}".format(p.power/1.e3) for p in POWER.instances], '(kW)')
        print('Pressure Ratio (Pmax/Pmin) :', P_max/P_min)
        print('Working Fluid(s)  :', Fluids)
        print('Inventory      :', Mass, ' (kg)')
        print('message        :', mesg)
        print('++++++++++++++++++++++++++++++++++++++++')
        print('\n')
        print('++++++++++++++++++++++++++++++++++++++++')
        print('CYCLE DATA')
        print('Mass flow rate :', ["{0:0.3f}".format(m.mass) for m in MASS.instances], '(kg/s)')
        print('Turbine Efficiencies :')
        for i in range(len(T_label)):
            print(T_label[i], ' eta : %.2f (%%)' % (T_eta[i]))
        print('Compressor Efficiencies :')
        for i in range(len(C_label)):
            print(C_label[i], ' eta : %.2f (%%)' % (C_eta[i]))
        print('Internal Combustion Engine Efficiencies :')
        for i in range(len(ICE_label)):
            print(ICE_label[i], ' eta : {0:.2f} (%%)'.format(ICE_eta[i]*100))
        # Orifice Plate Data
        if EX_OR.instances:
            print('ORIFICE PLATES:')
            for i in EX_OR.instances:
                print(i.label, ' type  : {} '.format(i.otype))
                print(i.label, ' flow_state : {} '.format(i.flow_state))
        # Shaft data
        # if SHAFT.instances:
        #    print("Shafts:")
        #    for s in SHAFT.instances:
        #        print("{0} - Speed (rad/s): {1:.2f}".format(s.label,s.N))
        #        print("{0} - Speed   (RPM): {1:.2f}".format(s.label,s.N/(2*np.pi)*60))
        #        print("{0} - PL       (kW): {1:.2f}".format(s.label,s.PL/1.e3))
        #        print("{0} - PR       (kW): {1:.2f}".format(s.label,s.PR/1.e3))

        print('Q_in   (in) (kW): {0:.3f} '.format(Q_in/1.e3))
        print('Q_out  (out)(kW): {0:.3f} '.format(-Q_out/1.e3))
        print('Q_net  (in) (kW): {0:.3f} '.format(Q_net/1.e3))
        print('W_turb (out)(kW): {0:.3f} '.format(-W_turb/1.e3))
        print('W_ice  (out)(kW): {0:.3f} '.format(W_ice/1e3))
        print('W_comp (in) (kW): {0:.3f} '.format(W_comp/1.e3))
        print('W_net  (out)(kW): {0:.3f} '.format(W_net/1.e3))
        print('Cycle eta W_net/Q_in : %.2f (%%)' % (W_net/Q_in * 100.))
        print('Carnot eta (1- T_c/T_h)  : %.2f (%%)' % (eta_carnot * 100.))
        if W_net == 0:
            print('Cycle specific cost ($/kW) : nan ')
        else:
            print('Cycle specific cost ($/kW) : {0:.2f} '.format(Cost/(W_net/1.e3)))
        print('++++++++++++++++++++++++++++++++++++++++')
        print('\n')
        ###
        print('++++++++++++++++++++++++++++++++++++++++')
        print('POINT DATA')
        for p in POINT.instances:
            if p.fluid.split(':')[0].upper() == 'CUSTOM':
                hL = p.HL
                sL = 0.
                hR = p.HR
                sR = 0.
            else:
                hL = p.HL  # CPCP.PropsSI('HMASS','P',p.PL ,'T',  p.TL, p.fluid)
                # sL = CPCP.PropsSI('SMASS','P',p.PL ,'T',  p.TL ,p.fluid)
                sL = GetFluidPropLow(p.Eos, [CP.HmassP_INPUTS, p.HL, p.PL], [CP.iSmass])
                hR = p.HR  # CPCP.PropsSI('HMASS','P',p.PR ,'T',  p.TR, p.fluid)
                # sR = CPCP.PropsSI('SMASS','P',p.PR ,'T',  p.TR ,p.fluid)
                sR = GetFluidPropLow(p.Eos, [CP.HmassP_INPUTS, p.HR, p.PR], [CP.iSmass])
            print('%s    %s    P(avg) (kPa): %6.4f      T(avg) (K): %.2f     h (kJ/kg): %.2f    s (J/kg/K): %.2f  ' % (p.label, p.fluid, 0.5*(p.PL+p.PR)/1.e3, 0.5*(p.TL+p.TR), 0.5*(hL+hR)/1.e3, 0.5*(sL+sR)))
            print('  Errors                     %6.4e    %.2e    %.2e   %.2e  ' % ((p.PL-p.PR)/1.e3, (p.TL-p.TR), (hL-hR)/1.e3, (sL-sR)/1.e3))

        print('++++++++++++++++++++++++++++++++++++++++')
        print('\n')
        print('++++++++++++++++++++++++++++++++++++++++')
        print('SIMULATION ASSESSMENT')
        print('Energy Miss-balance (Q_net - W_net) (kW): {0:.3f} '.format((Q_net-W_net)/1.e3))
        print('Energy Miss-balance (fraction of Q_in): %.2f (%%)' % (abs((Q_net-W_net)/Q_in * 100.)))
        if abs((Q_net-W_net)/Q_in) > 0.01:
            print('WARNING: Error > 1 %%. Consider increasing number of iterations to improve results')
        print('++++++++++++++++++++++++++++++++++++++++')

        # if out_file is not "none" :
    if out_file is not "none":
        print("\n")
        print("CREATING OUTPUT FILE")

        if os.path.isfile(out_file):
            print("Output file already exists.")
            fntemp = out_file+".old"
            shutil.copyfile(out_file, fntemp)
            print("Existing file has been copied to:", fntemp)

        fp = open(out_file, 'w')
        fp.write("++++++++++++++++++++++++++++++\n")
        fp.write("Output File from SCARR.\n")
        fp.write("++++++++++++++++++++++++++++++\n")
        fp.write("Date / Time = "+str(datetime.datetime.now())+"\n")
        fp.write("Jobfile = "+jobFileName+"\n")
        fp.write("Iterative Solver Message ="+mesg+"\n")
        fp.write("Working Fluid(s) = "+str(Fluids)+"\n")
        fp.write("T_min = "+str(T_min)+"\n")
        fp.write("T_max = "+str(T_max)+"\n")
        fp.write("Mass in Loop = "+str(Mass)+"\n")
        fp.write("Carnot Cycle Efficiency (1- T_c/T_h) = "+str(eta_carnot)+"\n")
        if W_net == 0:
            fp.write('Cycle specific cost ($/kW) = nan \n')
        else:
            fp.write('Cycle specific cost ($/kW) = '+str(round((Cost/(W_net/1.e3)), 2))+"\n")

        fp.write("++++++++++++++++++++++++++++++\n")
        fp.write("MASS \n")
        for m in MASS.instances:
            m.write(fp)
        fp.write("++++++++++++++++++++++++++++++\n")
        fp.write("SPEED \n")
        for s in SPEED.instances:
            s.write(fp)
        fp.write("++++++++++++++++++++++++++++++\n")
        fp.write("POWER \n")
        for p in POWER.instances:
            p.write(fp)
        fp.write("++++++++++++++++++++++++++++++\n")
        fp.write("POINT \n")
        for p in POINT.instances:
            p.write(fp)
        fp.write("++++++++++++++++++++++++++++++\n")
        fp.write("COMPONENT \n")
        for c in Components:
            c.write(fp)
        fp.write("End of File")
        fp.close()
        print("File: ", out_file, " has been written. \n")

    if '--noplot' not in uoDict:
        # plot pressures and temperatures at different points
        # plot_points(POINT)
        # plot pressure and temperatures across different components
        plot_components(POINT, Components)

        # plot compressor maps with operating point
        plot_compressor_map(COMP.instances + COMP_MAP_SP.instances)

        # plot turbine maps with operating point
        plot_turbine_map(TURB.instances + TURB_MAP_SP.instances)

        # plot T-s diagram
        if "--ts-diagram" in uoDict:
            plot_TS(POINT, Components, Fluids[-1])

        plt.draw()

        plt.pause(1)  # <-------
        print('\n \n')

        input('<Hit Enter To Close Figures>')

        plt.close()

    return 0


shortOptions = ""
longOptions = ["help", "job=", "verbosity=", "ts-diagram", "out-file=", "in-file=", "noplot"]


def printUsage():
    """Print Usage instructions."""
    print("")
    print("Usage: SSCAR.py [--help] [--job=<jobFileName>] [--verbosity=0 (1 or 2)]")
    print("                [--ts-diagram] [--out-file=<FileName>]")
    print("                [--in-file=<FileName>] [--noplot]")
    return


class MyError(Exception):
    """Error catching exception."""

    def __init__(self, value):
        """Initilaise class."""
        self.value = value

    def __str__(self):
        """Create string."""
        return repr(self.value)


if __name__ == "__main__":
    userOptions = getopt(sys.argv[1:], shortOptions, longOptions)
    uoDict = dict(userOptions[0])

    if len(userOptions[0]) == 0 or "--help" in uoDict:
        printUsage()
        sys.exit(1)

    # execute the code
    try:
        main(uoDict)
        print("\n \n")
        print("SUCCESS.")
        print("\n \n")

    except MyError as e:
        print("This run has gone bad.")
        print(e.value)
        sys.exit(1)
