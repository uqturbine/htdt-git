#! /usr/bin/env python3
"""
Python Code to scale a given turbine map, and evaulate performance

Function has two oeprating modes:
(1) Stand alone
This evaluates the turbine map and plots the maps.
Optionally the design point and multiple operating points can be plotted on the
map.

(2) imported
The function is can be called by SSCAR.py to allow the quasi-steady evaluation
of turbine performance as part of Cycle off-design modelling.

Author: Ingo Jahn & Joshua Keep
Last Modified: 2018/10/21
"""


import sys as sys
from getopt import getopt
import numpy as np
import matplotlib.pyplot as plt
import scipy.interpolate
import CoolProp.CoolProp as CP
import itertools

shortOptions = ""
longOptions = ["help", "datafile=", "noprint", "plotmap"]

###############################################################################
#   Helper Functions
###############################################################################
# Functions adapted from Glassman


def C_0(T_in, P_in, P_out, fluid='CO2'):
    """
    Calculate turbine spouting velocity.

    Evaluation of Spouting velocity based on inlet conditions
    """
    C_P = CP.PropsSI('CP0MASS', 'P', P_in, 'T', T_in, fluid)
    C_V = CP.PropsSI('CVMASS', 'P', P_in, 'T', T_in, fluid)
    gm = C_P/C_V
    a = 2.*C_P*T_in*(1.-(P_out/P_in)**((gm-1.)/gm))
    C0 = np.sqrt(a)
    return C0


def get_MFP(mdot, Tin, Pin):
    """
    Calculate MFP from mdot.

    function to calucalte mass flow rate parameter -
    UK standard units - pressure converted to Bar"
    """
    MFP = mdot*np.sqrt(Tin)/(Pin/10**5)
    return MFP


def get_mdot(MFP, Tin, Pin):
    """
    Calculate mdot from MFP.

    function to calucalte mass flow rate parameter -
    UK standard units - pressure converted to Bar
    """
    mdot = MFP*(Pin*10**5)/(np.sqrt(Tin))
    return mdot


def get_N(N_corr, T0_in, T0_d):
    """
    Convert corrected speed --> speed.

    function.
    """
    N = N_corr*np.sqrt(T0_in)/np.sqrt(T0_d)
    return N


def get_N_corr(N, T0_in, T0_d):
    """
    Convert speed --> corrected speed.

    function.
    """
    N_corr = N*np.sqrt(T0_d)/np.sqrt(T0_in)
    return N_corr


###############################################################################
class Turbine:
    """Class that defines a Turbine."""

    def __init__(self):
        """Initialise class."""
        self.uc = []
        self.eta = []
        self.er = []
        self.mfp = []
        self.speed = []
        self.er_design = []
        self.U_C_design = []
        self.mfp_design = []
        self.speed_design = []
        self.T0_design = []
        self.Volume = []

    def checkdata(self):
        """Check that correct data has been inputted."""
        print('Turbine input data check not yet implemented')
        if (len(self.uc) != len(self.eta)) \
                or (len(self.uc) <= 1) \
                or (len(self.eta) <= 1):
            print('uc  : ', self.uc)
            print('eta : ', self.eta)
            raise MyError("Error lists defining UC and eta")
        if (len(self.er) != len(self.mfp)) \
                or (len(self.er) <= 1) \
                or (len(self.mfp) <= 1):
            print('er  : ', self.er)
            print('mfp : ', self.mfp)
            raise MyError("Error lists defining UC and eta")
        if not isinstance(self.Volume, float):
            print('Volume :', self.Volume)
            print('Volume not defined. Volume set to 0. m3')
            self.Volume = 0.

    def create_griddata(self, res=20j):
        """
        Create maps from inout data.

        Function to create maps of ETA SPEED and DH at equivalent conditions.
        Maps haev been dimensionalised based on design points.
        """
        method = 'linear'

        # set limits to match available data
        # min_mfp = min(self.mfp)
        # max_mfp = max(self.mfp)
        min_er = min(self.er)
        max_er = max(self.er)

        min_uc = min(self.uc)
        max_uc = max(self.uc)
        self.uc_grid = np.mgrid[min_uc:max_uc:res]  # Uc is nondimenaional

        self.ER_mfp_min = min(self.mfp*self.mfp_design)
        self.ER_mfp_max = max(self.mfp*self.mfp_design)

        if isinstance(self.speed, float):
            # if fixed speed only have single mfp vs er line
            self.ER = scipy.interpolate.interp1d(
                self.mfp*self.mfp_design,
                self.er*(self.er_design-1.)+1.,
                kind=method, fill_value=-1
                )

            self.ER_speed_min = 1.
            self.ER_speed_max = 1.
        else:
            # if variabel speed create a single mfp vs er map
            min_speed = min(self.speed)
            max_speed = max(self.speed)

            self.ER_speed_min = min(self.speed*self.speed_design)
            self.ER_speed_max = max(self.speed*self.speed_design)

            speed_grid = np.mgrid[min_speed:max_speed:res] * self.speed_design
            er_grid = np.mgrid[min_er:max_er:res] * (self.er_design-1.)+1
            grid_x, grid_y = np.meshgrid(speed_grid, er_grid)

            self.speed_grid = speed_grid
            self.er_grid = er_grid
            self.MFP = scipy.interpolate.griddata(
                (self.speed*self.speed_design, self.er*(self.er_design-1.)+1),
                self.mfp*self.mfp_design, (grid_x, grid_y),
                method=method, fill_value=-1
                )

        # create interpolant for efficiency
        self.ETA = scipy.interpolate.interp1d(
                    self.uc, self.eta*self.eta_design,
                    kind=method, fill_value=-1
                    )

    def calc_actual_ER(self, SPEED):
        """
        Create working line at current speed.

        Create actual working line if turbine operates at differnt speeds.
        we follow a two-stage process where we first create an equivalent
        working line and then use this worling line for further analysis.
        """
        # check that withi speed range
        if SPEED < self.ER_speed_min:
            SPEED = self.ER_speed_min
            print('Warning: Turbine interpolation trying to exceed min speed. \
                  Corrected speed (N_corr) limited to {0}'
                  .format(self.ER_speed_max_min))
        elif SPEED > self.ER_speed_max:
            SPEED = self.ER_speed_max
            print('Warning: Turbine interpolation trying to exceed max speed. \
            Corrected speed (N_corr) limited to {0}'.format(self.ER_speed_max))

        # create actual mfp line
        self.mfp_actual = scipy.interpolate.interpn(
                            (self.speed_grid, self.er_grid), self.MFP.T,
                            (np.ones(len(self.er_grid))*SPEED, self.er_grid),
                            method='linear', bounds_error=False, fill_value=-1
                            )

        self.ER = scipy.interpolate.interp1d(self.mfp_actual, self.er_grid,
                                             kind='linear', fill_value=-1)

    def calc_downstream(self, Massflow, Speed, P_in, T_in, fluid):
        """
        Calculate condiotions downstream of turbine.

        Function to calculate Turbine downstream conditions based on operating
        condition at the inlet, mass flow and speed
        Inputs:
        Massflow - mass flow rate through turbine (kg/s)
        Speed - rotational speed (RPM)
        P_in - inlet pressure (Pa)
        T_in - inlet temperature (K)
        fluid - string for fluid type to suit CoolPRop

        Outputs:
        P_out - outlet pressure (Pa)
        T_out - outlet temperature (K)
        eta - turbine efficiency
        """
        print(Massflow, T_in, P_in)

        # calculate mfp given mass flow rate and inlet conditions
        mfp = get_MFP(Massflow, T_in, P_in)

        # calculate corrected speed based on deviation of inlet conditions
        # from design
        N_corr = get_N_corr(Speed, T_in, self.T0_design)

        # interpolate Expansion ratio from map
        print('Operating Point', (mfp, N_corr))

        # limit bounds for interpolation
        if mfp < self.ER_mfp_min:
            mfp = self.ER_mfp_min
            print('Warning: Turbine interpolation trying to exceed min mass \
                  flow. Normalised mass flow parameter (mfp) limited to ', mfp)
        elif mfp > self.ER_mfp_max:
            mfp = self.ER_mfp_max
            print('Warning: Turbine interpolation trying to exceed max mass \
                  flow. Normalised mass flow parameter (mfp) limited to ', mfp)

        # print 'mfp',mfp
        if not isinstance(self.speed, float):
            # if multiple working lines, create appropriate working line
            self.calc_actual_ER(N_corr)

        print(mfp)

        # interpolate
        er = self.ER(mfp)
        P_out = P_in/er

        # calculate spouting velocity
        C0 = C_0(T_in, P_in, P_out, fluid)
        if np.isnan(C0):
            print('C0 is :', C0)
            print('T_in, P_in, P_out, er:', T_in, P_in, P_out, er)

        # calculate spouting velocity
        C0_d = C_0(self.T0_design, P_in, P_in/self.er_design)

        # calculate U_C based on C0 and fraction of speed from nominal
        u_c = self.U_C_design * (Speed/self.speed_design) / (C0/C0_d)
        # avoids need to calc tip speed / geom.
        # Assumes turbine design point U_C=0.7
        # print self.uc_grid, self.ETA

        # interpolate eta from U_C map
        eta = self.ETA(u_c)

        # calculate remaining outlet conditions using efficinecy
        h_in = CP.PropsSI('HMASS', 'P', P_in, 'T', T_in, fluid)  # enthalpy (J/kg)
        s_in = CP.PropsSI('SMASS', 'P', P_in, 'T', T_in, fluid)  # entropy (j/kg/K)

        # print P_out, s_in
        h_out_isen = CP.PropsSI('HMASS', 'P', P_out, 'S', s_in, fluid)

        delta_h_isen = h_out_isen - h_in
        delta_h = delta_h_isen * eta/100.
        h_out = h_in + delta_h

        T_out = CP.PropsSI('T', 'P', P_out, 'HMASS', h_out, fluid)

        return P_out, T_out, eta, u_c, er, mfp, delta_h
    ###

    def plot_map_eq(self, MFP=[], U_C=[], ER=[], ETA=[], Tin=[], Pin=[],
                    STRING=[], label='', AXIS=[], N_turb=[],
                    marker_string=('*', 'v', '^', 's', '.', 'o')):
        """
        Plot compressor map and to show operating pointsself.

        if supplied
        Map and oeprating poitns are at equivalent conditions
        Inputs:
        MFP   - list of mass flow parameters (actual)
        U_C   - list of spouting velocities
        ER    - list of expansion ratios
        ETA   - list of efficiencies
        Tin   - list of inlet temperature at actual conditions
                (if empty defaults to Tcrr, if single value same will be used for all)
        Tin   - list of inlet pressure at actual conditions
                (if empty defaults to Pcrr, if single value same will be used for all)
        STRING- list of labels corresponding to conditions
        label - string containing label for current compressor
        AXIS  - list of axis used for plotting. Needs to be three axis opjects.
        N_turb- N-turb, if create
        marker_sring - recursively used list for plot labels
        """
        # check if rwo axis are suppplied. If not create new figures.
        if not (isinstance(AXIS, (list, tuple)) and len(AXIS) == 2):
            f1, ax1 = plt.subplots(1, figsize=(11, 8))
            f2, ax2 = plt.subplots(1, figsize=(11, 8))
        else:
            ax1 = AXIS[0]
            ax2 = AXIS[1]

        plt.sca(ax1)
        plt.xticks(fontsize=16)
        plt.yticks(fontsize=16)

        # plot eta vs U_C
        plt.plot(self.uc, self.eta*self.eta_design, 'k-o', markersize=5,
                 markerfacecolor='k', linewidth=2, fillstyle='full',
                 label='Workingline')
        marker = itertools.cycle(marker_string)  # reset iterator for point markers
        # plot design point(s)
        if len(U_C) > 0 and len(ETA) > 0:
            for i in range(len(U_C)):
                plt.plot(U_C[i], ETA[i]*100, next(marker), markersize=10,
                         markeredgecolor='k', linewidth=2, fillstyle='full',
                         label=STRING[i])
            if len(STRING) > 0:
                plt.legend()
        plt.xlabel('U_C [-]', fontsize=16)
        plt.ylabel('eta_t_s [%]', fontsize=16)
        plt.title(label+' efficiency vs velocity ratio', fontsize=18)

        # plot MFP vs ER
        plt.sca(ax2)
        plt.xticks(fontsize=16)
        plt.yticks(fontsize=16)
        if isinstance(self.speed, float):
            plt.plot(self.er*(self.er_design-1.)+1, self.mfp*self.mfp_design,
                     'ko-', markersize=10, markerfacecolor='k',
                     markeredgewidth=2, linewidth=2, fillstyle='full')
        else:
            # print('Speed', 'MFP', 'ER')
            # for temp0, temp1, temp2 in zip(self.speed,self.mfp,self.er):
            #    print(temp0, temp1, temp2, temp0*self.speed_design, temp1*self.mfp_design, temp2*(self.er_design-1.)+1)
            sc = plt.scatter(self.er*(self.er_design-1.)+1, self.mfp*self.mfp_design, c=self.speed*self.speed_design, s=100., marker='o')
            cbar = plt.colorbar(sc)
            cbar.set_label('Normalised Speed [-]', rotation=270)
            # add working line if oeprating at a given speed
            if len(N_turb) > 0:
                for speed in N_turb:
                    self.calc_actual_ER(N_turb)
                    mfp = np.arange(min(self.mfp_actual),
                                    max(self.mfp_actual), 0.05)
                    er = self.ER(mfp)
                    plt.plot(er, mfp, 'b-',
                             label='Operating Line N={0:.2}'.format(speed))
        marker = itertools.cycle(marker_string)  # reset iterator for markers
        # plot design point
        if len(ER) > 0 and len(MFP) > 0:
            for i in range(len(ER)):
                plt.plot(ER[i], MFP[i], next(marker), markersize=10,
                         markeredgewidth=2, markeredgecolor='k', linewidth=2,
                         fillstyle='full', label=STRING[i])
        plt.legend()
        plt.xlabel('Total to total expansion ratio  [-],', fontsize=16)
        plt.ylabel('Mass flow parameter [kg/s(sqrt(K)/bar]', fontsize=16)
        plt.title(label+' mass flow parameter vs expansion ratio', fontsize=18)


def main(uoDict):  # main function
    """Run main script."""
    # main file to be executed
    dataFileName = uoDict.get("--datafile", "test")

    # strip .py extension form jobName
    dataName = dataFileName.split('.')
    dataName = dataName[0]

    # set print_flag (can be overwritten from jobfile)
    print_flag = 1
    if "--noprint" in uoDict:
        print_flag = 0

    T = Turbine()

    # Execute jobFile, this creates all the variables
    exec(open(dataFileName).read(), globals(), locals())

    # check that loaded turbine data is correct.
    T.checkdata()

    # create data on equally spaced grid
    T.create_griddata(res=50j)

    if print_flag is 1:
        # Design Point
        print("Design Point")
        print('mfp   [kg/s k^0.5 bar^-1]: {0:.4f}'.format(T.mfp_design))
        print('Speed [rad/s]: {0:.2f}'.format(T.speed_design))
        print('ER    [-]: {0:.4f}'.format(T.er_design))

    Test = 0
    if Test:
        # evaluate operating point
        Massflow = 10.  # (kg/s)
        Speed = 1.    # (RPM)
        P_in = 13.4e6    # (Pa)
        T_in = 800.     # (K)
        fluid = 'CO2'

        print('Operating Conditions:')
        print('Mass flow (kg/s): {0:.4f}'.format(Massflow))
        print('Speed     (RPM) : {0:.4f}'.format(Speed))
        print('P-in       (Pa) : {0:.4f}'.format(P_in))
        print('T-in        (K) : {0:.4f}'.format(T_in))
        MFP = get_MFP(Massflow, T_in, P_in)

        print('Turbine Outlet Conditions:')
        print('mfp           :  {0:.4f}'.format(MFP))
        P_out, T_out, eta, U_C, ER, MFP, delta_h = T.calc_downstream(
                                        Massflow, Speed, P_in, T_in, fluid
                                        )
        print('delta_h (J/kg): {0:.4f}'.format(delta_h))
        print('delta_H   (J) : {0:.4f}'.format(delta_h*Massflow))
        print('eta       (%) : {0:.4f}'.format(eta))
        print('P-out    (Pa) : {0:.4f}'.format(P_out))
        print('T-out     (K) : {0:.4f}'.format(T_out))
        print('Pressure Ratio: {0:.4f}'.format(ER))

    if ("--plotmap" in uoDict) and Test is 0:
        eta_design = T.ETA(T.U_C_design)
        T.plot_map_eq(MFP=[T.mfp_design], U_C=[T.U_C_design], ER=[T.er_design],
                      ETA=[eta_design], Tin=[T.T0_design], Pin=[T.P0_design],
                      STRING=['Design Point'],
                      marker_string=('.', 'o', '*', 'v', '^', 's'))
        plt.draw()
    elif ("--plotmap" in uoDict) and Test is 1:
        eta_design = T.ETA(T.U_C_design)
        T.plot_map_eq(MFP=[T.mfp_design, MFP], U_C=[T.U_C_design, U_C],
                      ER=[T.er_design, ER], ETA=[eta_design, eta],
                      Tin=[T.T0_design, T_in], Pin=[T.P0_design, P_in],
                      STRING=['Design Point', 'Operating 1'],
                      marker_string=('.', 'o', '*', 'v', '^', 's'))
        plt.draw()

    # Close all figures
    if "--plotmap" in uoDict:
        plt.pause(1)  # <-------
        print('\n \n')
        input("<Hit Enter To Close Figures>")
        plt.close()

    return T


def printUsage():
    """Display function usage instructions."""
    print("")
    print("Usage: Turbine_funct.py [--help] [--datafile=<dataFileName>] [--noprint] [--plotmap]")
    return


class MyError(Exception):
    """Exception manager."""

    def __init__(self, value):
        """Initialise."""
        self.value = value

    def __str__(self):
        """Return str."""
        return repr(self.value)


if __name__ == "__main__":
    userOptions = getopt(sys.argv[1:], shortOptions, longOptions)
    uoDict = dict(userOptions[0])

    if len(userOptions[0]) == 0 or "--help" in uoDict:
        printUsage()
        sys.exit(1)

    # execute the code
    try:
        main(uoDict)
        print("\n \n")
        print("SUCESS.")
        print("\n \n")

    except MyError as e:
        print("This run has gone bad.")
        print(e.value)
        sys.exit(1)
