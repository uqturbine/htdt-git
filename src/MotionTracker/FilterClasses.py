#! /usr/bin/env python3
"""
Classes that encapsulate different Karman Filters to be used for motion tracking.

To analyse new systems copy and rename the template class and use this to develop
a problem specific Kalman Filter.

Author: Ingo Jahn
Last Modified: 28/05/2019
"""

from filterpy.kalman import ExtendedKalmanFilter
from filterpy.common import Q_discrete_white_noise
import numpy as np
from MotionTracker import CustomError, GConf

'''
Each heat new Kalman  Filter should be defined as an outright class. The class
should then be initialised and added to the respective list.
'''

# define available filters
KalmanFilterList = []


class KFilter:
    # Below here are helper functions and these shouldn't be modified
    def setVerbosity(self, verbosity):
        """Update verbosity."""
        self.verbosity = verbosity

    def setIndex(self, index):
        """Update index used to extract data."""
        self.index = index

    def set_zOld(self, zOld):
        """Update zOld."""
        self.zOld = zOld

    def set_zNewIndex(self, zNewIndex):
        """Specify indices that should be used for attribution."""
        self.zNewIndex = zNewIndex

    def getCovariance(self):
        """Return Covariance."""
        return self.rk.P

    def set_zOldList(self, zOldList):
        """Update zOld."""
        self.zOldList = zOldList

    def check_validity(self):
        """Check validaity of using ."""
        # add code to calculate if function is within validaity range
        self.validity = 'not checked'
        print('Validity check not yet implemented, skipping this step.')

    def write_filter_details(self, long=False):
        """Write correlation details into output file."""
        string = 'Filter Name={0}; Validity check: {2}'.format(self.name,  self.validity)
        if long is False:
            return string
        else:
            [string + '\nDescription: {0} \nReference: {1}'.format(self.description, self.reference)]
            return string

    # Prototypes, these should be replaced.  
    def __init__(self):
        raise CustomError("Must override method '__init__'")
    def filterSetup(self, filter_settings):
        """Define Filter Settings."""
        raise CustomError("Must override method 'filterSetup'")
    def getCurrentMeasurement(self, i, NTargets, type=None):
        raise CustomError("Must override method 'getCurrentMeasurement'")
    def computeMeasurement(self, X):
        """Compute Measurements at zNewIndex based on current state X."""
        raise CustomError("Must override method 'computeMeasurement'")
    def computeJacobian(self, X):
        """Compute Jacobian for H-matrix at X."""
        raise CustomError("Must override method 'computeJacobian'")
    def loadregionData(self, regionData):
        """Extract data from regionData and make usable for filter analysis."""
        raise CustomError("Must override method 'loadRegionData'")
    def update(self, z_flat):
        """Forward step the Kalman Filter."""
        raise CustomError("Must override method 'update'")


class ROTATING_WHEEL_1ST(KFilter):
    """1st order Kalman Filter for rotating wheel."""

    def __init__(self):
        """Initialise class."""
        self.name = 'ROTATING_WHEEL_1ST'  # name by which correlation will be called
        self.description = 'First order Kalman Filter applied to track rotation of wheel. Filter uses measured centre location.'  # provide a description of the correlation
        self.reference = ''  # provide a reference for the correlation

        self.stateVector = ['rotationAngle[rad]', 'rotationVelocity[rad/s]']
        self.dataVector = ['x-Coordinate[Px]', 'y-Coordinate[Px]', 'radiusFromCentre[Px]', 'angle[rad]']

        # initialise other parameters
        self.index = None
        self.verbosity = 0  # set verbosity flag
        self.h = []  # empty list for h
        self.H = []  # empty list for  H
        self.regionData = []  # empty list to be populated by regionData

    def getCurrentMeasurement(self, i, NTargets, type=None):
        """Calculate current Measurement, including association."""
        # print(NTargets)
        NTargets = NTargets[0]  # we are only doing tracking on blobData
        z = self.blobData[i][0:NTargets, 0:2]  # get x, y data from each data set, but only include limited number.
        return z  # output needs to be single list.

    def computeMeasurement(self, X):
        """Compute Measurements at zNewIndex based on current state X."""
        if self.verbosity > 0:
            print('Evaluating hx at {}'.format(self.index))
        # h_x = x_origin + Radius * cos( Angle_0 + x[0])
        # h_y = y_origin + Radius * sin( Angle_0 + x[0])

        blobCoords = self.blobData  # list containing coordinates for all blobs
        centreCoords = self.centreData  # list containing coordinates extracted for centre

        h = []
        for j in range(len(self.zNewIndex)):
            ind = self.zNewIndex[j]
            # print(j,ind)
            if ind == -1:
                h.append(self.zOld[j, 0])
                h.append(self.zOld[j, 1])
            else:
                h.append(centreCoords[self.index][0]
                         + blobCoords[self.index][j, 2] * np.cos(blobCoords[0][j, 3] + X[0]))
                h.append(centreCoords[self.index][1]
                         + blobCoords[self.index][j, 2] * np.sin(blobCoords[0][j, 3] + X[0]))
        self.h = np.array(h)
        # print('Centre:', centreCoords[self.index][0], centreCoords[self.index][1])
        # print('Blobs: ', blobCoords[self.index][self.zNewIndex, 0], '\n', blobCoords[self.index][self.zNewIndex, 1], '\n',
        #                 blobCoords[self.index][self.zNewIndex, 2], '\n', blobCoords[self.index][self.zNewIndex, 3])
        return self.h

    def computeJacobian(self, X):
        """Compute Jacobian for H-matrix at X."""
        if self.verbosity > 0:
            print('Evaluating HJacobian at {}'.format(self.index))

        blobCoords = self.blobData  # list containing coordinates for all blobs
        # centreCoords = self.centreData  # list containing coordinates extracted for centre

        H = np.zeros((2*len(self.zNewIndex), 2))
        for j in range(len(self.zNewIndex)):
            ind = self.zNewIndex[j]
            if ind == -1:
                H[2*j, 0] = 0
                H[2*j, 1] = 0
                H[2*j+1, 0] = 0
                H[2*j+1, 1] = 0
            else:
                H[2*j, 0] = - blobCoords[self.index][j, 2] * np.sin(blobCoords[0][j, 3] + X[0])
                H[2*j, 1] = 0
                H[2*j+1, 0] = blobCoords[self.index][j, 2] * np.cos(blobCoords[0][j, 3] + X[0])
                H[2*j+1, 1] = 0

        self.H = np.array(H)
        # print('HJac:', np.shape(H))
        return self.H

    def loadregionData(self, regionData):
        """Extract data from regionData and make usable for filter analysis."""
        # for current case regionData will have two entries.
        # [0] corresponding to all the blobs
        # [1] corresponding to data of single central blob
        self.blobData = []
        self.centreData = []
        # process centre data
        for frame in regionData[1]:
            temp = np.ones(2)*np.nan
            temp[0] = np.average(frame[:, 0])  # averaged x-coordinate
            temp[1] = np.average(frame[:, 1])  # averaged y-coordinate
            self.centreData.append(temp)
        # print('centreData', self.centreData)

        # process point data
        for index, frame in enumerate(regionData[0]):
            temp = np.ones([np.shape(frame)[0], 4])*np.nan
            temp[:, 0] = frame[:, 0]  # x-coordinate
            temp[:, 1] = frame[:, 1]  # y-coordinate
            temp[:, 2] = np.sqrt(pow(frame[:, 0] - self.centreData[index][0], 2)
                                 + pow(frame[:, 1] - self.centreData[index][1], 2))  # radius to centre
            temp[:, 3] = np.arctan2(frame[:, 1] - self.centreData[index][1],
                                    frame[:, 0] - self.centreData[index][0])  # angle
            self.blobData.append(temp)
            # print('index', index)
            # print('centre', self.centreData[index][0], self.centreData[index][1])
            # print('data (first 10)', temp[1:10, :])

    def filterSetup(self, filter_settings):
        """Define Filter Settings."""
        # define set number of targets that will be tracked by filter
        NTarget = np.sum(filter_settings["NTargets"])

        # create extended Kalman filter
        self.rk = ExtendedKalmanFilter(dim_x=2, dim_z=2*NTarget)
        # set starting guess
        self.rk.x = filter_settings["x_start"]
        # covariance
        # rk.P *= 1
        self.rk.P = filter_settings["covariance_P"]
        # set transfer matrix to capture dynamics
        self.rk.F = np.array([[1., filter_settings["DT"]],
                              [0., 1.]])
        # set process noise
        self.rk.Q[0:2, 0:2] = filter_settings["processNoise_Q"]
        # rk.Q[1, 1] = 0.1
        # set measuremnt noise.
        self.rk.R = np.diag(np.ones(NTarget*2)) * filter_settings["measurementNoise_R_scale"]  # assume measurememnt noise is 2 pixels

    def update(self, z_flat):
        """Forward step the Kalman Filter."""
        self.rk.update(z_flat, self.computeJacobian, self.computeMeasurement)  # set current Jacobian and function to compute Measurement
        x_filter = self.rk.x  # save current state as predicted by Kalman Filter
        x_raw = self.blobData[self.index][self.zNewIndex, :]  # raw measurements of state variables
        z_filter = z_flat  # save curent measurement as predicted by Kalman Filter
        self.rk.predict()  # step forward KalmanFilter
        return x_raw, x_filter, z_filter

KalmanFilterList.append(ROTATING_WHEEL_1ST())


class ROTATING_WHEEL_2ND(KFilter):
    """2nd order Kalman Filter for rotating wheel."""

    def __init__(self):
        """Initialise class."""
        self.name = 'ROTATING_WHEEL_2ND'  # name by which correlation will be called
        self.description = 'Second order Kalman Filter applied to track rotation of wheel. Filter uses measured centre location.'  # provide a description of the correlation
        self.reference = ''  # provide a reference for the correlation

        self.stateVector = ['rotationAngle[rad]', 'rotationVelocity[rad/s]', 'rotationAcceleration[rad/s^2]']
        self.dataVector = ['x-Coordinate[Px]', 'y-Coordinate[Px]', 'radiusFromCentre[Px]', 'angle[rad]']

        # initialise other parameters
        self.index = None
        self.verbosity = 0  # set verbosity flag
        self.h = []  # empty list for h
        self.H = []  # empty list for  H
        self.regionData = []  # empty list to be populated by regionData

    def getCurrentMeasurement(self, i, NTargets, type=None):
        """Calculate current Measurement, including association."""
        # print(NTargets)
        NTargets = NTargets[0]  # we are only doing tracking on blobData
        z = self.blobData[i][0:NTargets, 0:2]  # get x, y data from each data set, but only include limited number.
        return z  # output needs to be single list.

    def computeMeasurement(self, X):
        """Compute Measurements at zNewIndex based on current state X."""
        if self.verbosity > 0:
            print('Evaluating hx at {}'.format(self.index))
        # h_x = x_origin + Radius * cos( Angle_0 + x[0])
        # h_y = y_origin + Radius * sin( Angle_0 + x[0])

        blobCoords = self.blobData  # list containing coordinates for all blobs
        centreCoords = self.centreData  # list containing coordinates extracted for centre

        self.h = []
        for j in range(len(self.zNewIndex)):
            ind = self.zNewIndex[j]
            # print(j,ind)
            if ind == -1:
                self.h.append(self.zOld[j, 0])
                self.h.append(self.zOld[j, 1])
            else:
                self.h.append(centreCoords[self.index][0]
                              + blobCoords[self.index][j, 2] * np.cos(blobCoords[0][j, 3] + X[0]))
                self.h.append(centreCoords[self.index][1]
                              + blobCoords[self.index][j, 2] * np.sin(blobCoords[0][j, 3] + X[0]))
        self.h = np.array(self.h)
        # print('h: \n', h)
        return self.h

    def computeJacobian(self, X):
        """Compute Jacobian for H-matrix at X."""
        if self.verbosity > 0:
            print('Evaluating HJacobian at {}'.format(self.index))

        blobCoords = self.blobData  # list containing coordinates for all blobs
        # centreCoords = self.centreData  # list containing coordinates extracted for centre

        self.H = np.zeros((2*len(self.zNewIndex), 3))
        for j in range(len(self.zNewIndex)):
            ind = self.zNewIndex[j]
            if ind == -1:
                self.H[2*j, 0] = 0
                self.H[2*j, 1] = 0
                self.H[2*j, 2] = 0
                self.H[2*j+1, 0] = 0
                self.H[2*j+1, 1] = 0
                self.H[2*j+1, 2] = 0
            else:
                self.H[2*j, 0] = - blobCoords[self.index][j, 2] * np.sin(blobCoords[0][j, 3] + X[0])
                self.H[2*j, 1] = 0
                self.H[2*j, 2] = 0
                self.H[2*j+1, 0] = blobCoords[self.index][j, 2] * np.cos(blobCoords[0][j, 3] + X[0])
                self.H[2*j+1, 1] = 0
                self.H[2*j+1, 2] = 0

        self.H = np.array(self.H)
        # print('HJac:', np.shape(H))
        return self.H

    def loadregionData(self, regionData):
        """Extract data from regionData and make usable for filter analysis."""
        # for current case regionData will have two entries.
        # [0] corresponding to all the blobs
        # [1] corresponding to data of single central blob
        self.blobData = []
        self.centreData = []
        # process centre data
        for frame in regionData[1]:
            temp = np.ones(2)*np.nan
            temp[0] = np.average(frame[:, 0])  # averaged x-coordinate
            temp[1] = np.average(frame[:, 1])  # averaged y-coordinate
            self.centreData.append(temp)

        # process point data
        for index, frame in enumerate(regionData[0]):
            temp = np.ones([np.shape(frame)[0], 4])*np.nan
            temp[:, 0] = frame[:, 0]  # x-coordinate
            temp[:, 1] = frame[:, 1]  # y-coordinate
            temp[:, 2] = np.sqrt(pow(frame[:, 0] - self.centreData[index][0], 2)
                                 + pow(frame[:, 1] - self.centreData[index][1], 2))  # radius to centre
            temp[:, 3] = np.arctan2(frame[:, 1] - self.centreData[index][1],
                                    frame[:, 0] - self.centreData[index][0])  # angle
            self.blobData.append(temp)

    def filterSetup(self, filter_settings):
        """Define Filter Settings."""
        # define set number of targets that will be tracked by filter
        NTarget = np.sum(filter_settings["NTargets"])

        # create extended Kalman filter
        self.rk = ExtendedKalmanFilter(dim_x=3, dim_z=2*NTarget)
        # set starting guess
        self.rk.x = filter_settings["x_start"]
        # covariance
        # rk.P *= 1
        self.rk.P = filter_settings["covariance_P"]
        # set transfer matrix to capture dynamics
        self.rk.F = np.array([[1., filter_settings["DT"], 0.5*filter_settings["DT"]**2],
                             [0., 1., filter_settings["DT"]],
                             [0., 0., 1.]])
        # set process noise
        self.rk.Q[0:3, 0:3] = filter_settings["processNoise_Q"]
        # rk.Q[1, 1] = 0.1
        # set measuremnt noise.
        self.rk.R = np.diag(np.ones(NTarget*2)) * filter_settings["measurementNoise_R_scale"]  # assume measurememnt noise is 2 pixels

    def update(self, z_flat):
        """Forward step the Kalman Filter."""
        self.rk.update(z_flat, self.computeJacobian, self.computeMeasurement)  # set current Jacobian and function to compute Measurement
        x_filter = self.rk.x  # save current state as predicted by Kalman Filter
        x_raw = self.blobData[self.index][self.zNewIndex, :]  # raw measurements of state variables
        z_filter = z_flat  # save curent measurement as predicted by Kalman Filter
        self.rk.predict()  # step forward KalmanFilter
        return x_raw, x_filter, z_filter


KalmanFilterList.append(ROTATING_WHEEL_2ND())


class ROTATING_WHEEL_3RD(KFilter):
    """2nd order Kalman Filter for rotating wheel."""

    def __init__(self):
        """Initialise class."""
        self.name = 'ROTATING_WHEEL_3RD'  # name by which correlation will be called
        self.description = 'Second order Kalman Filter applied to track rotation of wheel in combination with first order filters to track x and y motion of centre.'  # provide a description of the correlation
        self.reference = ''  # provide a reference for the correlation

        self.stateVector = ['rotationAngle[rad]', 'rotationVelocity[rad/s]', 'rotationAcceleration[rad/s^2]',
                            'centreX[Px]', 'centreXvel[Px/s]',
                            'centreY[Px]', 'centreYvel[Px/s]']
        self.dataVector = ['x-Coordinate[Px]', 'y-Coordinate[Px]', 'radiusFromCentre[Px]', 'angle[rad]']

        # initialise other parameters
        self.index = None
        self.verbosity = 0  # set verbosity flag
        self.h = []  # empty list for h
        self.H = []  # empty list for  H
        self.regionData = []  # empty list to be populated by regionData

    def getCurrentMeasurement(self, i, NTargets, type=None):
        """Calculate current Measurement, including association."""
        # print(NTargets)
        NTargets = NTargets[0]  # we are only doing tracking on blobData
        z = self.blobData[i][0:NTargets, 0:2]  # get x, y data from each data set, but only include limited number.
        return z  # output needs to be single list.

    def computeMeasurement(self, X):
        """Compute Measurements at zNewIndex based on current state X."""
        if self.verbosity > 0:
            print('Evaluating hx at {}'.format(self.index))
        # h_x = x_origin + Radius * cos( Angle_0 + x[0])
        # h_y = y_origin + Radius * sin( Angle_0 + x[0])

        blobCoords = self.blobData  # list containing coordinates for all blobs
        # centreCoords = self.centreData  # list containing coordinates extracted for centre

        self.h = []
        for j in range(len(self.zNewIndex)):
            ind = self.zNewIndex[j]
            # print(j,ind)
            if ind == -1:
                self.h.append(self.zOld[j, 0])
                self.h.append(self.zOld[j, 1])
            else:
                radius = np.sqrt(pow(X[3] - blobCoords[self.index][j, 0], 2) + pow(X[5] - blobCoords[self.index][j, 1], 2))
                # print('radius', radius, 'data', blobCoords[self.index][j, 2], 'delta', radius-blobCoords[self.index][j, 2])
                self.h.append(X[3] + radius * np.cos(blobCoords[0][j, 3] + X[0]))
                self.h.append(X[5] + radius * np.sin(blobCoords[0][j, 3] + X[0]))
        self.h = np.array(self.h)
        # print('h: \n', h)
        return self.h

    def computeJacobian(self, X):
        """Compute Jacobian for H-matrix at X."""
        if self.verbosity > 0:
            print('Evaluating HJacobian at {}'.format(self.index))

        blobCoords = self.blobData  # list containing coordinates for all blobs
        # centreCoords = self.centreData  # list containing coordinates extracted for centre

        self.H = np.zeros((2*len(self.zNewIndex), 7))
        for j in range(len(self.zNewIndex)):
            ind = self.zNewIndex[j]
            if ind == -1:
                self.H[2*j, 0] = 0
                self.H[2*j, 1] = 0
                self.H[2*j, 2] = 0
                self.H[2*j, 3] = 0
                self.H[2*j, 4] = 0
                self.H[2*j, 5] = 0
                self.H[2*j, 6] = 0
                self.H[2*j+1, 0] = 0
                self.H[2*j+1, 1] = 0
                self.H[2*j+1, 2] = 0
                self.H[2*j+1, 3] = 0
                self.H[2*j+1, 4] = 0
                self.H[2*j+1, 5] = 0
                self.H[2*j+1, 6] = 0
            else:
                radius = np.sqrt(pow(X[3] - blobCoords[self.index][j, 0], 2)
                                 + pow(X[5] - blobCoords[self.index][j, 1], 2))
                dr_dX3 = (X[3] - blobCoords[self.index][j, 0]) \
                    / np.sqrt(pow(X[3] - blobCoords[self.index][j, 0], 2)
                              + pow(X[5] - blobCoords[self.index][j, 1], 2))
                dr_dX5 = (X[5] - blobCoords[self.index][j, 1]) \
                    / np.sqrt(pow(X[3] - blobCoords[self.index][j, 0], 2)
                              + pow(X[5] - blobCoords[self.index][j, 1], 2))
                # Z[0] differentiated with respect to state variables.
                self.H[2*j, 0] = - radius * np.sin(blobCoords[0][j, 3] + X[0])
                self.H[2*j, 1] = 0
                self.H[2*j, 2] = 0
                self.H[2*j, 3] = 1. + dr_dX3 * np.cos(blobCoords[0][j, 3] + X[0])
                self.H[2*j, 4] = 0
                self.H[2*j, 5] = + dr_dX5 * np.cos(blobCoords[0][j, 3] + X[0])
                self.H[2*j, 6] = 0
                # Z[1] differentiated with respect to state variables.
                self.H[2*j+1, 0] = radius * np.cos(blobCoords[0][j, 3] + X[0])
                self.H[2*j+1, 1] = 0
                self.H[2*j+1, 2] = 0
                self.H[2*j+1, 3] = + dr_dX3 * np.sin(blobCoords[0][j, 3] + X[0])
                self.H[2*j+1, 4] = 0
                self.H[2*j+1, 5] = 1. + dr_dX5 * np.sin(blobCoords[0][j, 3] + X[0])
                self.H[2*j+1, 6] = 0

        self.H = np.array(self.H)
        # print('HJac:', np.shape(H))
        return self.H

    def loadregionData(self, regionData):
        """Extract data from regionData and make usable for filter analysis."""
        # for current case regionData will have two entries.
        # [0] corresponding to all the blobs
        # [1] corresponding to data of single central blob
        self.blobData = []
        self.centreData = []
        # process centre data
        for frame in regionData[1]:
            temp = np.ones(2)*np.nan
            temp[0] = np.average(frame[:, 0])  # averaged x-coordinate
            temp[1] = np.average(frame[:, 1])  # averaged y-coordinate
            self.centreData.append(temp)

        # process point data
        for index, frame in enumerate(regionData[0]):
            temp = np.ones([np.shape(frame)[0], 4])*np.nan
            temp[:, 0] = frame[:, 0]  # x-coordinate
            temp[:, 1] = frame[:, 1]  # y-coordinate
            temp[:, 2] = np.sqrt(pow(frame[:, 0] - self.centreData[index][0], 2)
                                 + pow(frame[:, 1] - self.centreData[index][1], 2))  # radius to centre
            temp[:, 3] = np.arctan2(frame[:, 1] - self.centreData[index][1],
                                    frame[:, 0] - self.centreData[index][0])  # angle
            self.blobData.append(temp)

    def filterSetup(self, filter_settings):
        """Define Filter Settings."""
        # define set number of targets that will be tracked by filter
        NTarget = np.sum(filter_settings["NTargets"])

        # create extended Kalman filter
        self.rk = ExtendedKalmanFilter(dim_x=7, dim_z=2*NTarget)
        # set starting guess
        self.rk.x = filter_settings["x_start"]
        # covariance
        # rk.P *= 1
        self.rk.P = filter_settings["covariance_P"]
        # set transfer matrix to capture dynamics
        self.rk.F = np.array([[1., filter_settings["DT"], 0.5*filter_settings["DT"]*filter_settings["DT"], 0., 0., 0., 0.],
                             [0., 1., filter_settings["DT"],  0., 0., 0., 0.],
                             [0., 0., 1., 0., 0., 0., 0.],
                             [0., 0., 0., 1., filter_settings["DT"], 0., 0.],
                             [0., 0., 0., 0., 1., 0., 0.],
                             [0., 0., 0., 0., 0., 1., filter_settings["DT"]],
                             [0., 0., 0., 0., 0., 0., 1.]])
        # set process noise
        self.rk.Q[0:7, 0:7] = filter_settings["processNoise_Q"]
        # rk.Q[1, 1] = 0.1
        # set measuremnt noise.
        self.rk.R = np.diag(np.ones(NTarget*2)) * filter_settings["measurementNoise_R_scale"]  # assume measurememnt noise is 2 pixels

    def update(self, z_flat):
        """Forward step the Kalman Filter."""
        self.rk.update(z_flat, self.computeJacobian, self.computeMeasurement)  # set current Jacobian and function to compute Measurement
        x_filter = self.rk.x  # save current state as predicted by Kalman Filter
        x_raw = self.blobData[self.index][self.zNewIndex, :]  # raw measurements of state variables
        z_filter = z_flat  # save curent measurement as predicted by Kalman Filter
        self.rk.predict()  # step forward KalmanFilter
        return x_raw, x_filter, z_filter


KalmanFilterList.append(ROTATING_WHEEL_3RD())


class HYACE_0(KFilter):
    """2nd order Kalman Filter for wing."""

    def __init__(self):
        """Initialise class."""
        self.name = 'HYACE_0'  # name by which correlation will be called
        self.description = 'First order Kalman Filter applied to track rotation of entire wing and flap. Flap assumed as rigid.'  # provide a description of the correlation
        self.reference = ''  # provide a reference for the correlation

        self.stateVector = ['wingRotationAngle[rad]', 'wingRotationVelocity[rad/s]', 'wingRotationAcceleration[rad/s]']
        self.dataVector = ['x-Coordinate[Px]', 'y-Coordinate[Px]', 'radiusFromCentre[Px]', 'angle[rad]']

        self.centreX = 1430
        self.centreY = 650
        self.radiusHinge = 850  # based on flap depth
        self.radiusHunge = 663  # based on motor rear.

        # initialise other parameters
        self.index = None
        self.verbosity = 0  # set verbosity flag
        self.h = []  # empty list for h
        self.H = []  # empty list for  H
        self.regionData = []  # empty list to be populated by regionData

    def getCurrentMeasurement(self, i, NTargets, type=None):
        """Calculate current Measurement, including association."""
        # print(NTargets)
        # combine data from both regions
        zWing = self.wingData[i][0:NTargets[0], 0:2]
        zFlap = self.flapData[i][0:NTargets[1], 0:2]
        # print('getCurrentMeasurement - zWing', zWing)
        return np.concatenate((zWing, zFlap))  # output needs to be single list.

    def computeMeasurement(self, X):
        """Compute Measurements at zNewIndex based on current state X."""
        if self.verbosity > 0:
            print('Evaluating hx at {}'.format(self.index))
        # h_x = x_origin + Radius * cos( Angle_0 + x[0])
        # h_y = y_origin + Radius * sin( Angle_0 + x[0])

        wingCoords = self.wingData[self.index]  # list containing coordinates for blobs on wing
        flapCoords = self.flapData[self.index]  # list containing coordinates for blobs on flap
        coords = np.concatenate((wingCoords, flapCoords))

        wingCoordsStart = self.wingData[0]  # list containing coordinates for blobs on wing
        flapCoordsStart = self.flapData[0]  # list containing coordinates for blobs on flap
        coordsStart = np.concatenate((wingCoordsStart, flapCoordsStart))

        h = []
        for j in range(len(self.zNewIndex)):
            ind = self.zNewIndex[j]
            # print(j,ind)
            if ind == -1:
                h.append(self.zOld[j, 0])
                h.append(self.zOld[j, 1])
            else:
                h.append(self.centreX
                         + coords[j, 2] * np.cos(coordsStart[j, 3] + X[0]))
                h.append(self.centreY
                         + coords[j, 2] * np.sin(coordsStart[j, 3] + X[0]))
        self.h = np.array(h)
        # print('computeMeasurement - wingCoords', wingCoords)
        # print('computeMeasurement - wingCoordsStart', wingCoordsStart)
        # print('self.centreX', self.centreX, 'self.centreY', self.centreY)
        # print('X', X)
        # print('computeMeasurement - h', h)
        # print('Centre:', centreCoords[self.index][0], centreCoords[self.index][1])
        # print('Blobs: ', blobCoords[self.index][self.zNewIndex, 0], '\n', blobCoords[self.index][self.zNewIndex, 1], '\n',
        #                 blobCoords[self.index][self.zNewIndex, 2], '\n', blobCoords[self.index][self.zNewIndex, 3])
        return self.h

    def computeJacobian(self, X):
        """Compute Jacobian for H-matrix at X."""
        if self.verbosity > 0:
            print('Evaluating HJacobian at {}'.format(self.index))

        wingCoords = self.wingData[self.index]  # list containing coordinates for blobs on wing
        flapCoords = self.flapData[self.index]  # list containing coordinates for blobs on flap
        coords = np.concatenate((wingCoords, flapCoords))

        wingCoordsStart = self.wingData[0]  # list containing coordinates for blobs on wing
        flapCoordsStart = self.flapData[0]  # list containing coordinates for blobs on flap
        coordsStart = np.concatenate((wingCoordsStart, flapCoordsStart))

        H = np.zeros((2*len(self.zNewIndex), 3))
        for j in range(len(self.zNewIndex)):
            ind = self.zNewIndex[j]
            if ind == -1:
                H[2*j, 0] = 0
                H[2*j, 1] = 0
                H[2*j, 2] = 0
                H[2*j+1, 0] = 0
                H[2*j+1, 1] = 0
                H[2*j+1, 2] = 0
            else:
                H[2*j, 0] = - coords[j, 2] * np.sin(coordsStart[j, 3] + X[0])
                H[2*j, 1] = 0
                H[2*j, 2] = 0
                H[2*j+1, 0] = coords[j, 2] * np.cos(coordsStart[j, 3] + X[0])
                H[2*j+1, 1] = 0
                H[2*j+1, 2] = 0
        self.H = np.array(H)
        # print('HJac:', np.shape(H))
        return self.H

    def loadregionData(self, regionData):
        """Extract data from regionData and make usable for filter analysis."""
        # for current case regionData will have two entries.
        # [0] corresponding to wing data
        # [1] corresponding to flap data
        self.wingData = []
        self.flapData = []

        # process point data
        for index, frame in enumerate(regionData[0]):
            temp = np.ones([np.shape(frame)[0], 4])*np.nan
            temp[:, 0] = frame[:, 0]  # x-coordinate
            temp[:, 1] = frame[:, 1]  # y-coordinate
            temp[:, 2] = np.sqrt(pow(frame[:, 0] - self.centreX, 2)
                                 + pow(frame[:, 1] - self.centreY, 2))  # radius to centre
            temp[:, 3] = np.arctan2(frame[:, 1] - self.centreY,
                                    frame[:, 0] - self.centreX)  # angle
            self.wingData.append(temp)
        # process point data
        for index, frame in enumerate(regionData[1]):
            temp = np.ones([np.shape(frame)[0], 4])*np.nan
            temp[:, 0] = frame[:, 0]  # x-coordinate
            temp[:, 1] = frame[:, 1]  # y-coordinate
            temp[:, 2] = np.sqrt(pow(frame[:, 0] - self.centreX, 2)
                                 + pow(frame[:, 1] - self.centreY, 2))  # radius to centre
            temp[:, 3] = np.arctan2(frame[:, 1] - self.centreY,
                                    frame[:, 0] - self.centreX)  # angle
            self.flapData.append(temp)

    def filterSetup(self, filter_settings):
        """Define Filter Settings."""
        # define set number of targets that will be tracked by filter
        NTarget = np.sum(filter_settings["NTargets"])

        # create extended Kalman filter
        self.rk = ExtendedKalmanFilter(dim_x=3, dim_z=2*NTarget)
        # set starting guess
        self.rk.x = filter_settings["x_start"]
        # covariance
        # rk.P *= 1
        self.rk.P = filter_settings["covariance_P"]
        # set transfer matrix to capture dynamics
        self.rk.F = np.array([[1., filter_settings["DT"], 0.5*filter_settings["DT"]*filter_settings["DT"]],
                              [0., 1., filter_settings["DT"]],
                              [0., 0., 1.]])
        # set process noise
        self.rk.Q[0:3, 0:3] = filter_settings["processNoise_Q"]
        # rk.Q[1, 1] = 0.1
        # set measuremnt noise.
        self.rk.R = np.diag(np.ones(NTarget*2)) * filter_settings["measurementNoise_R_scale"]  # assume measurememnt noise is 2 pixels

    def update(self, z_flat):
        """Forward step the Kalman Filter."""
        self.rk.update(z_flat, self.computeJacobian, self.computeMeasurement)  # set current Jacobian and function to compute Measurement
        x_filter = self.rk.x  # save current state as predicted by Kalman Filter

        wingCoords = self.wingData  # list containing coordinates for blobs on wing
        flapCoords = self.flapData  # list containing coordinates for blobs on flap
        coords = np.concatenate((wingCoords, flapCoords))
        x_raw = coords[self.index][self.zNewIndex, :]  # raw measurements of state variables
        z_filter = z_flat  # save curent measurement as predicted by Kalman Filter
        self.rk.predict()  # step forward KalmanFilter
        return x_raw, x_filter, z_filter


KalmanFilterList.append(HYACE_0())


class HYACE_1(KFilter):
    """2nd order Kalman Filter for wing + 2nd order KF for flap."""

    def __init__(self):
        """Initialise class."""
        self.name = 'HYACE_1'  # name by which correlation will be called
        self.description = 'First order Kalman Filter applied to track rotation of entire wing and flap. Flap assumed as rigid.'  # provide a description of the correlation
        self.reference = ''  # provide a reference for the correlation

        self.stateVector = ['wingRotationAngle[rad]', 'wingRotationVelocity[rad/s]', 'wingRotationAcceleration[rad/s]',
                            'flapRotationAngle[rad]', 'flapRotationVelocity[rad/s]', 'flapAcceleration[rad/s]']
        self.dataVector = ['x-Coordinate[Px]', 'y-Coordinate[Px]', 'radiusFromCentre[Px]', 'angle[rad]']

        self.centreX = 1430
        self.centreY = 650
        self.radiusHinge = -850  # based on flap depth
        # self.radiusHunge = -663  # based on motor rear.

        # initialise other parameters
        self.index = None
        self.verbosity = 0  # set verbosity flag
        self.h = []  # empty list for h
        self.H = []  # empty list for  H
        self.regionData = []  # empty list to be populated by regionData
        self.NTargets = [40, 30]

    def getCurrentMeasurement(self, i, NTargets, type=None):
        """Calculate current Measurement, including association."""
        # print(NTargets)
        # combine data from both regions
        if type == 'new':
            zWing = self.wingData[i][0:NTargets[0], 0:2]
            zFlap = self.flapData[i][0:NTargets[1], 0:2]
            z = np.concatenate((zWing, zFlap))  # output needs to be single list.
            z_new_indexWing = np.arange(0, NTargets[0])
            z_new_indexFlap = np.arange(0, NTargets[1]) # + np.shape(self.wingData[i])[0]
            z_new_index = np.concatenate((z_new_indexWing, z_new_indexFlap))

            print('Get First Measurement')
            print('Wing: z, index', zWing, '\n', z_new_indexWing)
            print('Flap: z, index', zFlap, '\n', z_new_indexFlap)
            # return z, z_new_index

            position = [zWing, zFlap]  # create list containing positions for each region
            newIndex = [z_new_indexWing, z_new_indexFlap]
            return position, newIndex

        else:
            zWing = self.wingData[i][:, 0:2]
            zFlap = self.flapData[i][:, 0:2]
            # print('getCurrentMeasurement - zWing', zWing)
            # return np.concatenate((zWing, zFlap))  # output needs to be single list.
            position = [zWing, zFlap]  # create list containing positions for each region
            return position

    def computeMeasurement(self, X):
        """Compute Measurements at zNewIndex based on current state X."""
        if self.verbosity > 0:
            print('Evaluating hx at {}'.format(self.index))

        # do calculation for wing
        # h_x = x_origin + Radius * cos( Angle_0 + X[0])
        # h_y = y_origin + Radius * sin( Angle_0 + X[0])

        wingCoords = self.wingData[self.index]  # list containing coordinates for blobs on wing
        wingCoordsStart = self.wingData[0]  # list containing coordinates for blobs on wing

        # print('wingCoords', wingCoords)

        if self.index > 0:
            zOld = self.zOldList[0]
        zNewIndexWing = self.zNewIndex[0]

        hWing = []
        for j in range(len(zNewIndexWing)):
            ind = zNewIndexWing[j]
            # print('WING: j, ind', j, ind)

            if ind == -1:
                hWing.append(zOld[j, 0])
                hWing.append(zOld[j, 1])
            else:
                hWing.append(self.centreX
                         + wingCoordsStart[j, 2] * np.cos(wingCoordsStart[j, 3] + X[0]))
                hWing.append(self.centreY
                         + wingCoordsStart[j, 2] * np.sin(wingCoordsStart[j, 3] + X[0]))

        # do calculation for flap
        # h_x = x_origin + Radius0 * cos(X[0]) + Radius * cos( Angle_0 + X[3])
        # h_y = y_origin + Radius0 * sin(X[0]) + Radius * sin( Angle_0 + X[3])

        flapCoords = self.flapData[self.index]  # list containing coordinates for blobs on flap
        flapCoordsStart = self.flapData[0]  # list containing coordinates for blobs on flap

        # print('flapCoords', flapCoords)

        if self.index > 0:
            zOld = self.zOldList[1]
        zNewIndexFlap = self.zNewIndex[1]

        hFlap = []
        for j in range(len(zNewIndexFlap)):
            ind = zNewIndexFlap[j]
            # print('FALP: j, ind', j, ind)

            if ind == -1:
                hFlap.append(zOld[j, 0])
                hFlap.append(zOld[j, 1])
            else:
                hFlap.append(self.centreX
                         + self.radiusHinge * np.cos(X[0])
                         + flapCoordsStart[j, 2] * np.cos(flapCoordsStart[j, 3] + X[3]))
                hFlap.append(self.centreY
                         + self.radiusHinge * np.sin(X[0])
                         + flapCoordsStart[j, 2] * np.sin(flapCoordsStart[j, 3] + X[3]))

                # print(np.cos(X[0]), np.sin(X[0]), X[3])
                # print('LoopData', j, ind, self.centreX, self.radiusHinge, '|',
                #         flapCoords[j, 2], flapCoordsStart[j, 3], '|',
                #         flapCoords[ind, 0], flapCoords[ind, 1])

        #if self.index == 1:
        #    a=a+5

        h = hWing + hFlap
        self.h = np.array(h)

        """
        print('self.index', self.index)
        print('computeMeasurement - wingCoords', np.shape(wingCoords), wingCoords)
        print('computeMeasurement - flapCoords', np.shape(flapCoords), flapCoords)
        print('computeMeasurement - wingCoordsStart', wingCoordsStart)
        print('computeMeasurement - flapCoordsStart', flapCoordsStart)
        print('self.centreX', self.centreX, 'self.centreY', self.centreY)
        print('self.radiusHinge', self.radiusHinge)
        print('X', X)
        print('computeMeasurementWing - h', np.shape(h), h[0:80])
        print('computeMeasurementFlap - h', np.shape(h), h[80:])
        if self.index == 1:
            a=a+5
        """
        # print('Centre:', centreCoords[self.index][0], centreCoords[self.index][1])
        # print('Blobs: ', blobCoords[self.index][self.zNewIndex, 0], '\n', blobCoords[self.index][self.zNewIndex, 1], '\n',
        #                 blobCoords[self.index][self.zNewIndex, 2], '\n', blobCoords[self.index][self.zNewIndex, 3])
        return self.h

    def computeJacobian(self, X):
        """Compute Jacobian for H-matrix at X."""
        if self.verbosity > 0:
            print('Evaluating HJacobian at {}'.format(self.index))


        # do calculation for wing
        # h_x = x_origin + Radius * cos( Angle_0 + X[0])
        # h_y = y_origin + Radius * sin( Angle_0 + X[0])

        wingCoords = self.wingData[self.index]  # list containing coordinates for blobs on wing
        wingCoordsStart = self.wingData[0]  # list containing coordinates for blobs on wing

        zNewIndexWing = self.zNewIndex[0]

        HWing = np.zeros((2*len(zNewIndexWing), 6))
        for j in range(len(zNewIndexWing)):
            ind = zNewIndexWing[j]
            # print('j, ind', j, ind)
            if ind == -1:
                HWing[2*j, 0] = 0
                HWing[2*j, 1] = 0
                HWing[2*j, 2] = 0
                HWing[2*j, 3] = 0
                HWing[2*j, 4] = 0
                HWing[2*j, 5] = 0
                HWing[2*j+1, 0] = 0
                HWing[2*j+1, 1] = 0
                HWing[2*j+1, 2] = 0
                HWing[2*j+1, 3] = 0
                HWing[2*j+1, 4] = 0
                HWing[2*j+1, 5] = 0
            else:
                HWing[2*j, 0] = - wingCoordsStart[j, 2] * np.sin(wingCoordsStart[j, 3] + X[0])
                HWing[2*j, 1] = 0
                HWing[2*j, 2] = 0
                HWing[2*j, 3] = 0
                HWing[2*j, 4] = 0
                HWing[2*j, 5] = 0
                HWing[2*j+1, 0] = wingCoordsStart[j, 2] * np.cos(wingCoordsStart[j, 3] + X[0])
                HWing[2*j+1, 1] = 0
                HWing[2*j+1, 2] = 0
                HWing[2*j+1, 3] = 0
                HWing[2*j+1, 4] = 0
                HWing[2*j+1, 5] = 0

        # do calculation for flap
        # h_x = x_origin + Radius0 * cos(X[0]) + Radius * cos( Angle_0 + X[3])
        # h_y = y_origin + Radius0 * sin(X[0]) + Radius * sin( Angle_0 + X[3])

        flapCoords = self.flapData[self.index]  # list containing coordinates for blobs on flap
        flapCoordsStart = self.flapData[0]  # list containing coordinates for blobs on flap
        zNewIndexFlap = self.zNewIndex[1]

        HFlap = np.zeros((2*len(zNewIndexFlap), 6))
        for j in range(len(zNewIndexFlap)):
            ind = zNewIndexFlap[j]
            # print('j, ind', j, ind)

            if ind == -1:
                HFlap[2*j, 0] = 0
                HFlap[2*j, 1] = 0
                HFlap[2*j, 2] = 0
                HFlap[2*j, 3] = 0
                HFlap[2*j, 4] = 0
                HFlap[2*j, 5] = 0
                HFlap[2*j+1, 0] = 0
                HFlap[2*j+1, 1] = 0
                HFlap[2*j+1, 2] = 0
                HFlap[2*j+1, 3] = 0
                HFlap[2*j+1, 4] = 0
                HFlap[2*j+1, 5] = 0
            else:
                HFlap[2*j, 0] = - self.radiusHinge * np.sin(X[0])
                HFlap[2*j, 1] = 0
                HFlap[2*j, 2] = 0
                HFlap[2*j, 3] = - flapCoordsStart[j, 2] * np.sin(flapCoordsStart[j, 3] + X[3])
                HFlap[2*j, 4] = 0
                HFlap[2*j, 5] = 0
                HFlap[2*j+1, 0] = self.radiusHinge * np.cos(X[0])
                HFlap[2*j+1, 1] = 0
                HFlap[2*j+1, 2] = 0
                HFlap[2*j+1, 3] = flapCoordsStart[j, 2] * np.cos(flapCoordsStart[j, 3] + X[3])
                HFlap[2*j+1, 4] = 0
                HFlap[2*j+1, 5] = 0

        H = np.vstack((HWing, HFlap))
        self.H = np.array(H)
        # print('HJac:', np.shape(H))
        return self.H

    def loadregionData(self, regionData):
        """Extract data from regionData and make usable for filter analysis."""
        # for current case regionData will have two entries.
        # [0] corresponding to wing data
        # [1] corresponding to flap data
        self.wingData = []
        self.flapData = []

        # process point data
        for index, frame in enumerate(regionData[0]):
            temp = np.ones([np.shape(frame)[0], 4])*np.nan
            temp[:, 0] = frame[:, 0]  # x-coordinate
            temp[:, 1] = frame[:, 1]  # y-coordinate
            temp[:, 2] = np.sqrt(pow(frame[:, 0] - self.centreX, 2)
                                 + pow(frame[:, 1] - self.centreY, 2))  # radius to centre
            temp[:, 3] = np.arctan2(frame[:, 1] - self.centreY,
                                    frame[:, 0] - self.centreX)  # angle to centre
            self.wingData.append(temp)
        # process point data
        for index, frame in enumerate(regionData[1]):
            temp = np.ones([np.shape(frame)[0], 4])*np.nan
            temp[:, 0] = frame[:, 0]  # x-coordinate
            temp[:, 1] = frame[:, 1]  # y-coordinate
            temp[:, 2] = np.sqrt(pow(frame[:, 0] - (self.centreX + self.radiusHinge), 2)
                                 + pow(frame[:, 1] - self.centreY, 2))  # radius to hinge
            temp[:, 3] = np.arctan2(frame[:, 1] - self.centreY,
                                    frame[:, 0] - (self.centreX + self.radiusHinge))  # angle to hinge
            self.flapData.append(temp)

    def filterSetup(self, filter_settings):
        """Define Filter Settings."""
        # define set number of targets that will be tracked by filter
        NTarget = np.sum(filter_settings["NTargets"])

        # create extended Kalman filter
        self.rk = ExtendedKalmanFilter(dim_x=6, dim_z=2*NTarget)
        # set starting guess
        self.rk.x = filter_settings["x_start"]
        # covariance
        # rk.P *= 1
        print("existing-rk.P", self.rk.P)
        self.rk.P = filter_settings["covariance_P"]
        # set transfer matrix to capture dynamics
        self.rk.F = np.array([[1., filter_settings["DT"], 0.5*filter_settings["DT"]*filter_settings["DT"], 0., 0., 0.],
                              [0., 1., filter_settings["DT"], 0., 0., 0.],
                              [0., 0., 1., 0., 0., 0.],
                              [0., 0., 0., 1., filter_settings["DT"], 0.5*filter_settings["DT"]*filter_settings["DT"]],
                              [0., 0., 0., 0., 1., filter_settings["DT"]],
                              [0., 0., 0., 0., 0., 1.]
                              ])
        # set process noise
        print("existing-rk.Q", self.rk.Q)
        self.rk.Q[0:6, 0:6] = filter_settings["processNoise_Q"]
        # rk.Q[1, 1] = 0.1
        # set measuremnt noise.
        print("existing-rk.R", self.rk.R)
        self.rk.R = np.diag(np.ones(NTarget*2)) * filter_settings["measurementNoise_R_scale"] # assume measurememnt noise is 2 pixels
        # print("existing-rk.H", self.rk.H)


    def update(self, z_flat):
        """Forward step the Kalman Filter."""
        self.rk.update(z_flat, self.computeJacobian, self.computeMeasurement)  # set current Jacobian and function to compute Measurement
        x_filter = self.rk.x  # save current state as predicted by Kalman Filter



        wingCoords = self.wingData[self.index]  # list containing coordinates for blobs on wing
        flapCoords = self.flapData[self.index]  # list containing coordinates for blobs on flap
        x_raw = []
        x_raw.append(wingCoords[self.zNewIndex[0], :])
        x_raw.append(flapCoords[self.zNewIndex[1], :])

        # print('shape(coords)', np.shape(coords[self.index]))
        # print('shape(wingCoords)', np.shape(wingCoords[self.index]))
        # print('shape(flapCoords)', np.shape(flapCoords[self.index]))
        # print('self.zNewIndex', self.zNewIndex)

        z_filter = z_flat  # save curent measurement as predicted by Kalman Filter
        self.rk.predict()  # step forward KalmanFilter
        return x_raw, x_filter, z_filter


KalmanFilterList.append(HYACE_1())


class HYACE_2(KFilter):
    """2nd order Kalman Filter for wing + 2nd order KF for flap."""

    def __init__(self):
        """Initialise class."""
        self.name = 'HYACE_2'  # name by which correlation will be called
        self.description = 'First order Kalman Filter applied to track rotation of entire wing and flap. Flap assumed as rigid.'  # provide a description of the correlation
        self.reference = ''  # provide a reference for the correlation

        self.stateVector = ['wingRotationAngle[rad]', 'wingRotationVelocity[rad/s]', 'wingRotationAcceleration[rad/s2]',
                            'flapRotationAngle[rad]', 'flapRotationVelocity[rad/s]', 'flapRotationAcceleration[rad/s2]',
                            'frameXPos[m]', 'frameXVel[m/s]', 'frameYPos[m]', 'frameYVel[m/s]']
        self.dataVector = ['x-Coordinate[Px]', 'y-Coordinate[Px]', 'radiusFromCentre[Px]', 'angle[rad]']

        self.centreX = 1430
        self.centreY = 650
        self.radiusHinge = -850  # based on flap depth
        # self.radiusHunge = -663  # based on motor rear.

        # initialise other parameters
        self.index = None
        self.verbosity = 0  # set verbosity flag
        self.h = []  # empty list for h
        self.H = []  # empty list for  H
        self.regionData = []  # empty list to be populated by regionData
        self.NTargets = [40, 30]

    def getCurrentMeasurement(self, i, NTargets, type=None):
        """Calculate current Measurement, including association."""
        # print(NTargets)
        # combine data from both regions
        if type == 'new':
            zFrame = self.frameData[i][0:NTargets[0], 0:2]
            zFlap = self.flapData[i][0:NTargets[1], 0:2]
            # z = np.concatenate((zFrame, zFlap))  # output needs to be single list.
            z_new_indexFrame = np.arange(0, NTargets[0])
            z_new_indexFlap = np.arange(0, NTargets[1])  # + np.shape(self.wingData[i])[0]
            # z_new_index = np.concatenate((z_new_indexFrame, z_new_indexFlap))

            print('Get First Measurement')
            print('Frame: z, index', zFrame, '\n', z_new_indexFrame)
            print('Flap: z, index', zFlap, '\n', z_new_indexFlap)
            # return z, z_new_index

            position = [zFrame, zFlap]  # create list containing positions for each region
            newIndex = [z_new_indexFrame, z_new_indexFlap]
            return position, newIndex

        else:
            zFrame = self.frameData[i][:, 0:2]
            zFlap = self.flapData[i][:, 0:2]
            # print('getCurrentMeasurement - zWing', zWing)
            # return np.concatenate((zWing, zFlap))  # output needs to be single list.
            position = [zFrame, zFlap]  # create list containing positions for each region
            return position

    def computeMeasurement(self, X):
        """Compute Measurements at zNewIndex based on current state X."""
        if self.verbosity > 0:
            print('Evaluating hx at {}'.format(self.index))

        # do calculation for wing
        # h_x = x_origin + Radius * cos( Angle_0 + X[0])
        # h_y = y_origin + Radius * sin( Angle_0 + X[0])

        frameCoords = self.frameData[self.index]  # list containing coordinates for blobs on frame
        frameCoordsStart = self.frameData[0]  # list containing coordinates for blobs on frame

        # print('wingCoords', wingCoords)

        if self.index > 0:
            zOld = self.zOldList[0]
        zNewIndexFrame = self.zNewIndex[0]

        hFrame = []
        for j in range(len(zNewIndexFrame)):
            ind = zNewIndexFrame[j]
            # print('WING: j, ind', j, ind)

            if ind == -1:
                hFrame.append(zOld[j, 0])
                hFrame.append(zOld[j, 1])
            else:
                hFrame.append(frameCoordsStart[j, 0] + X[6])
                hFrame.append(frameCoordsStart[j, 1] + X[8])

        # do calculation for flap
        # h_x = x_origin + Radius0 * cos(X[0]) + Radius * cos( Angle_0 + X[3])
        # h_y = y_origin + Radius0 * sin(X[0]) + Radius * sin( Angle_0 + X[3])

        flapCoords = self.flapData[self.index]  # list containing coordinates for blobs on flap
        flapCoordsStart = self.flapData[0]  # list containing coordinates for blobs on flap

        # print('flapCoords', flapCoords)

        if self.index > 0:
            zOld = self.zOldList[1]
        zNewIndexFlap = self.zNewIndex[1]

        hFlap = []
        for j in range(len(zNewIndexFlap)):
            ind = zNewIndexFlap[j]
            # print('FALP: j, ind', j, ind)

            if ind == -1:
                hFlap.append(zOld[j, 0])
                hFlap.append(zOld[j, 1])
            else:
                hFlap.append(self.radiusHinge * np.cos(X[0])
                             + flapCoordsStart[j, 2] * np.cos(flapCoordsStart[j, 3] + X[3])
                             + self.centreX + X[6])
                hFlap.append(self.radiusHinge * np.sin(X[0])
                             + flapCoordsStart[j, 2] * np.sin(flapCoordsStart[j, 3] + X[3])
                             + self.centreY + X[8])

                # print(np.cos(X[0]), np.sin(X[0]), X[3])
                # print('LoopData', j, ind, self.centreX, self.radiusHinge, '|',
                #         flapCoords[j, 2], flapCoordsStart[j, 3], '|',
                #         flapCoords[ind, 0], flapCoords[ind, 1])

        # if self.index == 1:
        #    a=a+5

        h = hFrame + hFlap
        self.h = np.array(h)

        """
        print('self.index', self.index)
        print('computeMeasurement - wingCoords', np.shape(wingCoords), wingCoords)
        print('computeMeasurement - flapCoords', np.shape(flapCoords), flapCoords)
        print('computeMeasurement - wingCoordsStart', wingCoordsStart)
        print('computeMeasurement - flapCoordsStart', flapCoordsStart)
        print('self.centreX', self.centreX, 'self.centreY', self.centreY)
        print('self.radiusHinge', self.radiusHinge)
        print('X', X)
        print('computeMeasurementWing - h', np.shape(h), h[0:80])
        print('computeMeasurementFlap - h', np.shape(h), h[80:])
        if self.index == 1:
            a=a+5
        """
        # print('Centre:', centreCoords[self.index][0], centreCoords[self.index][1])
        # print('Blobs: ', blobCoords[self.index][self.zNewIndex, 0], '\n', blobCoords[self.index][self.zNewIndex, 1], '\n',
        #                 blobCoords[self.index][self.zNewIndex, 2], '\n', blobCoords[self.index][self.zNewIndex, 3])
        return self.h

    def computeJacobian(self, X):
        """Compute Jacobian for H-matrix at X."""
        if self.verbosity > 0:
            print('Evaluating HJacobian at {}'.format(self.index))

        # do calculation for frame
        # h_x = x_origin + Radius * cos( Angle_0 + X[0])
        # h_y = y_origin + Radius * sin( Angle_0 + X[0])

        frameCoords = self.frameData[self.index]  # list containing coordinates for blobs on wing
        frameCoordsStart = self.frameData[0]  # list containing coordinates for blobs on wing

        zNewIndexframe = self.zNewIndex[0]

        HFrame = np.zeros((2*len(zNewIndexframe), 10))
        for j in range(len(zNewIndexframe)):
            ind = zNewIndexframe[j]
            # print('j, ind', j, ind)
            if ind == -1:
                HFrame[2*j, 0] = 0
                HFrame[2*j, 1] = 0
                HFrame[2*j, 2] = 0
                HFrame[2*j, 3] = 0
                HFrame[2*j, 4] = 0
                HFrame[2*j, 5] = 0
                HFrame[2*j, 6] = 0
                HFrame[2*j, 7] = 0
                HFrame[2*j, 8] = 0
                HFrame[2*j, 9] = 0
                HFrame[2*j+1, 0] = 0
                HFrame[2*j+1, 1] = 0
                HFrame[2*j+1, 2] = 0
                HFrame[2*j+1, 3] = 0
                HFrame[2*j+1, 4] = 0
                HFrame[2*j+1, 5] = 0
                HFrame[2*j+1, 6] = 0
                HFrame[2*j+1, 7] = 0
                HFrame[2*j+1, 8] = 0
                HFrame[2*j+1, 9] = 0
            else:
                HFrame[2*j, 0] = 0
                HFrame[2*j, 1] = 0
                HFrame[2*j, 2] = 0
                HFrame[2*j, 3] = 0
                HFrame[2*j, 4] = 0
                HFrame[2*j, 5] = 0
                HFrame[2*j, 6] = 1
                HFrame[2*j, 7] = 0
                HFrame[2*j, 8] = 0
                HFrame[2*j, 9] = 0
                HFrame[2*j+1, 0] = 0
                HFrame[2*j+1, 1] = 0
                HFrame[2*j+1, 2] = 0
                HFrame[2*j+1, 3] = 0
                HFrame[2*j+1, 4] = 0
                HFrame[2*j+1, 5] = 0
                HFrame[2*j+1, 6] = 0
                HFrame[2*j+1, 7] = 0
                HFrame[2*j+1, 8] = 1
                HFrame[2*j+1, 9] = 0

        # do calculation for flap
        # h_x = x_origin + Radius0 * cos(X[0]) + Radius * cos( Angle_0 + X[3])
        # h_y = y_origin + Radius0 * sin(X[0]) + Radius * sin( Angle_0 + X[3])

        flapCoords = self.flapData[self.index]  # list containing coordinates for blobs on flap
        flapCoordsStart = self.flapData[0]  # list containing coordinates for blobs on flap
        zNewIndexFlap = self.zNewIndex[1]

        HFlap = np.zeros((2*len(zNewIndexFlap), 10))
        for j in range(len(zNewIndexFlap)):
            ind = zNewIndexFlap[j]
            # print('j, ind', j, ind)

            if ind == -1:
                HFlap[2*j, 0] = 0
                HFlap[2*j, 1] = 0
                HFlap[2*j, 2] = 0
                HFlap[2*j, 3] = 0
                HFlap[2*j, 4] = 0
                HFlap[2*j, 5] = 0
                HFlap[2*j, 6] = 0
                HFlap[2*j, 7] = 0
                HFlap[2*j, 8] = 0
                HFlap[2*j, 9] = 0
                HFlap[2*j+1, 0] = 0
                HFlap[2*j+1, 1] = 0
                HFlap[2*j+1, 2] = 0
                HFlap[2*j+1, 3] = 0
                HFlap[2*j+1, 4] = 0
                HFlap[2*j+1, 5] = 0
                HFlap[2*j+1, 6] = 0
                HFlap[2*j+1, 7] = 0
                HFlap[2*j+1, 8] = 0
                HFlap[2*j+1, 9] = 0
            else:
                HFlap[2*j, 0] = - self.radiusHinge * np.sin(X[0])
                HFlap[2*j, 1] = 0
                HFlap[2*j, 2] = 0
                HFlap[2*j, 3] = - flapCoordsStart[j, 2] * np.sin(flapCoordsStart[j, 3] + X[3])
                HFlap[2*j, 4] = 0
                HFlap[2*j, 5] = 0
                HFlap[2*j, 6] = 1
                HFlap[2*j, 7] = 0
                HFlap[2*j, 8] = 0
                HFlap[2*j, 9] = 0
                HFlap[2*j+1, 0] = self.radiusHinge * np.cos(X[0])
                HFlap[2*j+1, 1] = 0
                HFlap[2*j+1, 2] = 0
                HFlap[2*j+1, 3] = flapCoordsStart[j, 2] * np.cos(flapCoordsStart[j, 3] + X[3])
                HFlap[2*j+1, 4] = 0
                HFlap[2*j+1, 5] = 0
                HFlap[2*j+1, 6] = 0
                HFlap[2*j+1, 7] = 0
                HFlap[2*j+1, 8] = 1
                HFlap[2*j+1, 9] = 0

        H = np.vstack((HFrame, HFlap))
        self.H = np.array(H)
        # print('HJac:', np.shape(H))
        return self.H

    def loadregionData(self, regionData):
        """Extract data from regionData and make usable for filter analysis."""
        # for current case regionData will have two entries.
        # [0] corresponding to wing data
        # [1] corresponding to flap data
        self.frameData = []
        self.flapData = []

        # process point data
        for index, frame in enumerate(regionData[0]):
            temp = np.ones([np.shape(frame)[0], 4])*np.nan
            temp[:, 0] = frame[:, 0]  # x-coordinate
            temp[:, 1] = frame[:, 1]  # y-coordinate
            temp[:, 2] = np.sqrt(pow(frame[:, 0] - self.centreX, 2)
                                 + pow(frame[:, 1] - self.centreY, 2))  # radius to centre
            temp[:, 3] = np.arctan2(frame[:, 1] - self.centreY,
                                    frame[:, 0] - self.centreX)  # angle to centre
            self.frameData.append(temp)
        # process point data
        for index, frame in enumerate(regionData[1]):
            temp = np.ones([np.shape(frame)[0], 4])*np.nan
            temp[:, 0] = frame[:, 0]  # x-coordinate
            temp[:, 1] = frame[:, 1]  # y-coordinate
            temp[:, 2] = np.sqrt(pow(frame[:, 0] - (self.centreX + self.radiusHinge), 2)
                                 + pow(frame[:, 1] - self.centreY, 2))  # radius to hinge
            temp[:, 3] = np.arctan2(frame[:, 1] - self.centreY,
                                    frame[:, 0] - (self.centreX + self.radiusHinge))  # angle to hinge
            self.flapData.append(temp)

    def filterSetup(self, filter_settings):
        """Define Filter Settings."""
        # define set number of targets that will be tracked by filter
        NTarget = np.sum(filter_settings["NTargets"])

        # create extended Kalman filter
        self.rk = ExtendedKalmanFilter(dim_x=10, dim_z=2*NTarget)
        # set starting guess
        self.rk.x = filter_settings["x_start"]
        # covariance
        # rk.P *= 1
        self.rk.P = filter_settings["covariance_P"]
        # set transfer matrix to capture dynamics
        self.rk.F = np.array([[1., filter_settings["DT"], 0.5*filter_settings["DT"]*filter_settings["DT"], 0., 0., 0., 0., 0., 0., 0.],
                              [0., 1., filter_settings["DT"], 0., 0., 0., 0., 0., 0., 0.],
                              [0., 0., 1., 0., 0., 0., 0., 0., 0., 0.],
                              [0., 0., 0., 1., filter_settings["DT"], 0.5*filter_settings["DT"]*filter_settings["DT"], 0., 0., 0., 0.],
                              [0., 0., 0., 0., 1., filter_settings["DT"], 0., 0., 0., 0.],
                              [0., 0., 0., 0., 0., 1., 0., 0., 0., 0.],
                              [0., 0., 0., 0., 0., 0., 1., filter_settings["DT"], 0., 0.],
                              [0., 0., 0., 0., 0., 0., 0., 1., 0., 0.],
                              [0., 0., 0., 0., 0., 0., 0., 0., 1., filter_settings["DT"]],
                              [0., 0., 0., 0., 0., 0., 0., 0., 0., 1.]])
        # set process noise
        self.rk.Q[0:10, 0:10] = filter_settings["processNoise_Q"]
        # rk.Q[1, 1] = 0.1
        # set measuremnt noise.
        self.rk.R = np.diag(np.ones(NTarget*2)) * filter_settings["measurementNoise_R_scale"]  # assume measurememnt noise is 2 pixels

    def update(self, z_flat):
        """Forward step the Kalman Filter."""
        self.rk.update(z_flat, self.computeJacobian, self.computeMeasurement)  # set current Jacobian and function to compute Measurement
        x_filter = self.rk.x  # save current state as predicted by Kalman Filter

        frameCoords = self.frameData[self.index]  # list containing coordinates for blobs on wing
        flapCoords = self.flapData[self.index]  # list containing coordinates for blobs on flap
        x_raw = []
        x_raw.append(frameCoords[self.zNewIndex[0], :])
        x_raw.append(flapCoords[self.zNewIndex[1], :])

        # print('shape(coords)', np.shape(coords[self.index]))
        # print('shape(wingCoords)', np.shape(wingCoords[self.index]))
        # print('shape(flapCoords)', np.shape(flapCoords[self.index]))
        # print('self.zNewIndex', self.zNewIndex)

        z_filter = z_flat  # save curent measurement as predicted by Kalman Filter
        self.rk.predict()  # step forward KalmanFilter
        return x_raw, x_filter, z_filter


KalmanFilterList.append(HYACE_2())


# function to get list of all
def get_FilterList():
    """Return list of available names and cooresponding functions."""
    NameList = []
    for n in KalmanFilterList:
        NameList.append(n.name)
    return NameList, KalmanFilterList
