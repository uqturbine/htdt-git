#! /usr/bin/env python3
"""
Python code to blob detect point features in a series of images.

This code takes a series of images and performs simple image processing to
enable blob detection. The data from the blobs (position and size) in each
frame is recorded in tabular form.
The results can be displayed as a sequence of images.

Author: Ingo Jahn
Last Modified: 21/03/2019
"""

import cv2 as cv2
from getopt import getopt
import numpy as np
import matplotlib.pyplot as plt
import os as os
import pdb
import shutil as shutil
import sys as sys

# Need these two lines to stop OpenCV hanging on some machines
import os
os.environ['DISPLAY'] = ':0'

class GDATA:
    """Class containing global simulation parameters."""

    def __init__(self):
        """Initialise class."""
        self.verbosity = 0
        self.wait = 0.1  # time to wait between image updates when plotting results
        self.showRegions = False  # highlight regions that have been identified on image
        self.showRegionsFrameIndx = 0  # initialise which frame is used for showing regions

    def showRegions(self, frameIndx):
        """Set frame that will be used for showing regions."""
        self.showRegions = True
        self.showRegionsFrameIndx = int(frameIndx)


class SOURCE:
    """Class containing information about data source."""

    def __init__(self):
        """Initialise class."""
        self.path = None  # relative path to where images are stored
        self.filenameBase = None  # based component of filename
        self.filenameBaseCol = None  # as above for source colour imgs
        self.firstFrame = None  # string containing frame number of first frame
        self.lastFrame = None  # string containing frame number of last frame
        self.suffix = None  # suffix used in image filename


class SEQUENCE:
    """Class containing information about data source."""

    def __init__(self):
        """Initialise class."""
        self.sequence_numbers = None
        self.imagesRaw = []
        self.imagesRawCol = []
        self.images = []
        self.blobData = []
        self.blobDataNative = [] # Store keypoints using OCV format
        # self.blobData_centre = []


class REGION:
    """Class that can be used to identify regions for blob detection."""

    instances = []

    def __init__(self, type=None, detector=None, name=None, threshold=50, x0=None, y0=None, width=None, height=None, radius=None, xList=None, yList=None):
        """Initialise class."""
        self.__class__.instances.append(self)  # add self to instance list
        self.type = type.upper()  # set type of region selector used
        self.detector = detector  # instance of DETECTOR class passed into region to determine how region is analysed.
        self.name = name  # give name of region
        self.threshold = threshold  # value used for thresholding when turning image into binary
        self.x0 = x0  # x-location of centre in pixels
        self.y0 = y0  # y-location of centre in pixels
        self.width = width  # width in pixles when type = rectangle
        self.height = height  # height in pixles when type = rectangle
        self.radius = radius  # radius in picles when type = circle
        self.xList = xList  # list of x-coordinates when using polygon
        self.yList = yList  # list of y-coordinates when using polygon
        self.check()  # check
        self.detector.check()  # run check that detector has been corectly set.
        self.blob_detector = cv2.SimpleBlobDetector_create(detector.params)

    def check(self):
        """Check that options have been set correctly."""
        # check that type is in list of supported options
        self.typeList = ['ALL', 'RECTANGLE', 'CIRCLE']
        if self.type not in self.typeList:
            raise MyError('Setting for region.type={0} not supported. Possible options are: {1}'.format(self.type, self.typeList))

        # check settings for rectangle
        if self.type == 'RECTANGLE':
            if self.x0 is None or self.y0 is None or self.width is None or self.height is None:
                raise MyError('When selecting region.type={0} for region with name={1}, \
                               not all required settings are specified \n \
                               region.x0={2}  region.y0={3} \n \
                               region.width={4}  region.height={5}'.format(self.type, self.name, self.x0, self.y0, self.width, self.height))
        # checks ettings for circle
        if self.type == 'CIRCLE':
            if self.x0 is None or self.y0 is None or self.radius is None:
                raise MyError('When selecting region.type={0} for region with name={1}, \
                               not all required settings are specified \n \
                               region.x0={2}  region.y0={3} \n \
                               region.radius={4}'.format(self.type, self.name, self.x0, self.y0, self.radius))

    def setOutsideValue(self, imageRaw, value=0, scale=1):
        """Set region of image_raw outside of region to  value."""
        self.imageRaw = imageRaw.copy()  # create copy of input image
        imageOut = self.imageRaw

        # set pixels outside region area to value.
        if self.type == 'ALL':
            pass
        elif self.type == 'RECTANGLE':
            imageOut[0:int(self.y0*scale), :] = value
            imageOut[int(self.y0*scale+self.height*scale):-1, :] = value
            imageOut[:, 0:int(self.x0*scale)] = value
            imageOut[:, int(self.x0*scale+self.width*scale):-1] = value
        elif self.type == 'CIRCLE':
            [rows, columns, depth] = np.shape(imageOut)
            for r in range(rows):
                if r < self.y0*scale-self.radius*scale or r > self.y0*scale+self.radius*scale:
                    imageOut[r, :] = value
                else:
                    for c in range(columns):
                        if c < self.x0*scale-self.radius*scale or c > self.x0*scale+self.radius*scale:
                            imageOut[:, c] = value
                        else:
                            if np.ceil(np.sqrt(pow(c-self.x0*scale, 2) + pow(r-self.y0*scale, 2))) > self.radius*scale:
                                imageOut[r, c] = value
        else:
            raise MyError('Setting for region.type={} not supported.'.format(self.type))

        return imageOut


class DETECTOR:
    """Class that defines blob detector."""

    def __init__(self):
        """Initialise class."""
        self.params = cv2.SimpleBlobDetector_Params()  # create params template for detector.

        # Define default settings for blob detector
        self.minDistBetweenBlobs = 0.1
        # Change thresholds
        self.params.minThreshold = 10
        self.params.maxThreshold = 200
        # Filter by Area.
        self.params.filterByArea = False
        self.params.minArea = None
        self.params.maxArea = 200
        # Filter by Circularity
        self.params.filterByCircularity = False
        self.params.minCircularity = None
        self.params.maxCircularity = None
        # Filter by Convexity
        self.params.filterByConvexity = False
        self.params.minConvexity = None
        self.params.maxConvexity = None
        # Filter by Inertia
        self.params.filterByInertia = False
        self.params.minInertiaRatio = None
        self.params.maxInertiaRatio = None

    def check(self):
        """Check that settings are valid."""
        pass


class PROCESS:
    """Class that stores all the information used for image processing ."""

    def __init__(self):
        """Initialise class."""
        self.scale = 1  # factor used to rescale the image
        self.thresholdvalue = 50  # threshold used to turn into bimary


def main(uoDict):
    """Main function call."""

    # create string to collect warning messages
    warn_str = "\n"

    # main file to be executed
    jobFileName = uoDict.get("--job", "test")

    # Make SSCAR run even if .py extension is forgotten
    if ".py" not in jobFileName:
        jobName = jobFileName
        jobFileName = ''.join([jobFileName, ".py"])
    else:
        # strip .py extension from jobName
        jobName = jobFileName.replace('.py', '')

    # Make sure that the desired job file actually exists
    if jobFileName not in os.listdir('.'):
        local_directory = os.path.dirname(os.path.realpath(__file__))
        raise MyError("No job file {0} found in directory: {1}".format(jobFileName, local_directory))

    # get in-file and out-file name
    out_file = uoDict.get("--out-file", "none")

    gdata = GDATA()
    source = SOURCE()
    sequence = SEQUENCE()
    # detector = DETECTOR()
    # detector_centre = DETECTOR()
    process = PROCESS()

    # Execute jobFile, this creates all the variables
    exec(open(jobFileName).read(), globals(), locals())

    # overwrite verbosity using setting in uoDict
    if "--verbosity" in uoDict:
        gdata.verbosity = float(uoDict.get("--verbosity", 0))

    if gdata.verbosity >= 1:
        print("Input data file: {} read.".format(jobFileName))

    # create blobdetector
    # blob_detector = cv2.SimpleBlobDetector_create(detector.params)
    # blob_detector_centre = cv2.SimpleBlobDetector_create(detector_centre.params)

    # create list of filenames for loading individuak frames.
    source.frameList = np.arange(int(source.firstFrame), int(source.lastFrame)+1)
    cwd = os.getcwd()
    temp = os.path.join(cwd, source.path)
    source.fileNameList = []
    source.fileNameListCol = []
    for i in source.frameList:
        source.fileNameList.append(os.path.join(temp,
            '{0}{1:0{width}d}.{2}'.format(source.filenameBase, i, source.suffix,
                width=len(str(source.lastFrame)))))
        source.fileNameListCol.append(os.path.join(temp,
            '{0}{1:0{width}d}.{2}'.format(source.filenameBaseCol, i, source.suffix,
                width=len(str(source.lastFrame)))))

    if gdata.verbosity > 1:
        print("List of Filenames created: \n {}".format(source.frameList))

    # check that at least one regin has been defined.
    if len(REGION.instances) == 0:
        raise MyError('At least one region for blob detection has to be defined.')

    # display regions that that been selected
    if gdata.showRegions:
        # load image
        img = cv2.imread(source.fileNameList[gdata.showRegionsFrameIndx])
        # rescale image to allow for better resolution
        img = cv2.resize(img, (int(process.scale*img.shape[1]), int(process.scale*img.shape[0])))

        if True:  # plot all regions on single figure
            f, axis = plt.subplots(1, 1, sharey=True)
            axis = [axis]*len(REGION.instances)
        else:
            f, axis = plt.subplots(len(REGION.instances), 1, sharey=True)
        # display each region in a picture
        for i, region in enumerate(REGION.instances):
            axis[i].imshow(img)
            if region.type == 'ALL':
                pass
            elif region.type == 'RECTANGLE':
                axis[i].plot([region.x0*process.scale, region.x0*process.scale+region.width*process.scale,
                              region.x0*process.scale+region.width*process.scale, region.x0*process.scale,
                              region.x0*process.scale],
                             [region.y0*process.scale, region.y0*process.scale,
                              region.y0*process.scale+region.height*process.scale,
                              region.y0*process.scale+region.height*process.scale, region.y0*process.scale],
                             label=region.name)
            elif region.type == 'CIRCLE':
                thetaList = np.arange(start=0, stop=2*np.pi, step=0.01)
                x = region.x0*process.scale + region.radius*process.scale*np.cos(thetaList)
                y = region.y0*process.scale + region.radius*process.scale*np.sin(thetaList)
                axis[i].plot(x, y, label=region.name)
            else:
                raise MyError('region.type={} not supported for showRegions')
            axis[i].legend()

        axis[0].set_title("Regions used for data blob identification")

        # display the plots
        plt.draw()
        plt.pause(1)  # <-------
        print('\n \n')
        print('Review and accept region placement.')
        print('<Hit (A) key to abort>')
        print('\n')
        keyStroke = input('<Hit any other key to continue>')
        if keyStroke.upper() == 'A':
            raise MyError('Run stopped as regions not accepted.')
        plt.close()

    # create figure and axis to show masking progress
    if gdata.verbosity > 1 and gdata.showRegions:
        f, ax = plt.subplots(1, 1, sharey=True)

    # analyse each frame
    source.sequence_numbers = source.frameList
    for i in range(len(source.fileNameList)):
        if gdata.verbosity > 1:
            print("Analysing frame: {}".format(source.fileNameList[i]))

        # load image
        img = cv2.imread(source.fileNameList[i])
        img_col = cv2.imread(source.fileNameListCol[i])
        # rescale image to allow for better resolution
        img = cv2.resize(img, (int(process.scale*img.shape[1]), int(process.scale*img.shape[0])))
        img_col = cv2.resize(img_col, (int(process.scale*img_col.shape[1]), int(process.scale*img_col.shape[0])))
        # store raw image
        sequence.imagesRaw.append(img)
        sequence.imagesRawCol.append(img_col)

        # Once we have the keypoints for each image, we can plot them as follows:
        # im_with_keypoints = cv2.drawKeypoints(col_img, keypoints, np.array([]), (0,0,255),
                # cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)
        #
        # How do we want to store the keypoints though?

        regionDataNative = []
        regionData = []
        imageList = []
        imageNo = []
        for rIndx, region in enumerate(REGION.instances):  # extract different regions
            # print('rIndx', rIndx)
            region = REGION.instances[rIndx]
            # apply region masking
            imageMasked = region.setOutsideValue(img, value=0., scale=process.scale)

            if gdata.verbosity > 1 and gdata.showRegions:
                ax.imshow(imageMasked)
                ax.set_title('Masked image for region={0} and frame={1}'.format(region.name, i))
                plt.draw()
                plt.pause(gdata.wait)

            # apply thresholding
            imageMasked = cv2.threshold(imageMasked, region.threshold, 255, cv2.THRESH_BINARY)[1]
            imageList.append(imageMasked)  # store image in list
            # do blob detection for markers
            keypoints = region.blob_detector.detect(imageMasked)
            regionDataNative.append(keypoints)
            # option exists to do this more precisely by using findContour function
            # consider as a future improvement

            # Initialise data array for each image
            data = np.zeros((len(keypoints), 5))
            for j in range(len(keypoints)):
                data[j][0] = keypoints[j].pt[0]  # get keypoint x-coordinate
                data[j][1] = keypoints[j].pt[1]  # get keypoint y-coordinate
                data[j][2] = keypoints[j].size  # get keypoint size
                data[j][3] = keypoints[j].angle  # get keypoint angle
                data[j][4] = rIndx  # set indx of region to which key point belongs
            regionData.append(data)
            imageNo.append(len(keypoints))
        sequence.images.append(imageList)
        sequence.blobData.append(regionData)
        sequence.blobDataNative.append(regionDataNative)
        if gdata.verbosity > 1:
            print("    Number of points identified per region: {}".format(imageNo))

    # if out_file is not "none" :
    if out_file is not "none":

        # print("\n")
        # print("START: Creating Output File")

        if os.path.isfile(out_file):
            print("    Output file already exists.")
            fntemp = out_file+".old"
            shutil.copyfile(out_file, fntemp)
            print("    Existing file has been copied to:", fntemp)

        fp = open(out_file, 'w')  # open output file
        fp.write("++++++++++++++++++++++++++++++\n")
        fp.write("SETTINGS\n")
        fp.write("Needs to be added.\n")

        fp.write("++++++++++++++++++++++++++++++\n")
        fp.write("HEADER\n")
        fp.write("col0:ImageIndex[-] col1:regionIndx[-] col2:xCord[px] col3:yCord[px] col4:size[px2] col5:angle[rad]\n")
        fp.write("++++++++++++++++++++++++++++++\n")
        fp.write("DATA\n")
        for i in range(len(source.sequence_numbers)):
            imgIndx = source.sequence_numbers[i]
            regionData = sequence.blobData[i]
            for data in regionData:  # go through the entries corresponding to each region
                for j in range(np.shape(data)[0]):  # extract each row corresponding to a point
                    fp.write("{0:d} {1:d} {2:.10f} {3:.10f} {4:.10f} {5:.10f} \n".format(int(imgIndx), int(data[j][4]), data[j][0], data[j][1], data[j][2], data[j][3]))
        fp.write("++++++++++++++++++++++++++++++\n")

        fp.write("END")
        fp.close()  # close putput file
        # print("  COMPLETE: Writing Output File. Data written to: {}".format(out_file))

    if '--plot' in uoDict:
        # plotting code goes here.

        # f, (ax1, ax2) = plt.subplots(1, 2, sharey=True)
        # # display each frame
        # for i in range(len(source.fileNameList)):
            # if gdata.verbosity > 1:
                # print("Displaying frame: {}".format(source.fileNameList[i]))

            # regionData = sequence.blobData[i]  # get list containing blobs from each region
            # # display image
            # ax1.cla()
            # ax1.imshow(sequence.imagesRaw[i])
            # markers = ['o', 'o', 'o', 'o', 'o', 'o']  # define list for iteration
            # colours = ['r', 'g', 'b', 'c', 'm', 'k']  # define list for iteration
            # for rIndx in range(len(REGION.instances)):
                # # print(rIndx, regionData[rIndx])
                # data = regionData[rIndx]
                # ax1.plot(data[:, 0], data[:, 1], color=colours[rIndx], marker=markers[rIndx], markersize=1, linestyle='None', label=REGION.instances[rIndx].name)
            # ax1.legend()
            # ax1.set_title("Raw-image: {}".format(source.sequence_numbers[i]))

            # ax2.cla()
            # imageList = sequence.images[i]  # get list containing images from each region
            # ax2.imshow(imageList[0])
            # for rIndx in range(len(REGION.instances)):
                # data = regionData[rIndx]
                # ax2.plot(data[:, 0], data[:, 1], color=colours[rIndx], marker=markers[rIndx], markersize=1, linestyle='None', label=REGION.instances[rIndx].name)
            # ax2.legend()
            # ax2.set_title("Thresholded-image: {}".format(source.sequence_numbers[i]))

            # plt.pause(gdata.wait)

        #-------------------------------------------------------------------------------
        # New plotting (using OpenCV)

        # Get video frame size
        frame_0 = sequence.imagesRawCol[0]
        frame_width = int(frame_0.shape[1])
        frame_height = int(frame_0.shape[0])
        # Initialize the video writer
        out = cv2.VideoWriter('particle_tracker.avi',
                cv2.VideoWriter_fourcc('M','J','P','G'),
                5, (frame_width,frame_height))

        for i in range(len(source.fileNameList)):
            if gdata.verbosity > 1:
                print("Displaying frame: {}".format(source.fileNameList[i]))

            # How do we merge keypoint data for different regions?
            # Currently just enforce one region only.
            assert(len(REGION.instances) == 1)
            regionDataNative = sequence.blobDataNative[i]
            keypoints = regionDataNative[0]

            im_with_keypoints = cv2.drawKeypoints(sequence.imagesRawCol[i],
                    keypoints, np.array([]), (0,0,255),
                    cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)
            out.write(im_with_keypoints)
            cv2.imshow("img_keypoints", im_with_keypoints)
            k = cv2.waitKey(60) & 0xff
            if k == 27: break

        out.release()

        cv2.destroyAllWindows()

        # # Using matplotlib
        # s = 2
        # f, (ax1) = plt.subplots(1, 1, sharey=True, figsize=(s*40, s*1))
        # # display each frame
        # for i in range(len(source.fileNameList)):
            # if gdata.verbosity > 1:
                # print("Displaying frame: {}".format(source.fileNameList[i]))

            # regionData = sequence.blobData[i]  # get list containing blobs from each region
            # # display image
            # ax1.cla()
            # # ax1.imshow(sequence.imagesRaw[i], aspect='auto')
            # ax1.imshow(sequence.imagesRawCol[i], aspect='auto')
            # markers = ['o', 'o', 'o', 'o', 'o', 'o']  # define list for iteration
            # colours = ['r', 'g', 'b', 'c', 'm', 'k']  # define list for iteration
            # for rIndx in range(len(REGION.instances)):
                # # print(rIndx, regionData[rIndx])
                # data = regionData[rIndx]
                # ax1.plot(data[:, 0], data[:, 1], color=colours[rIndx],
                        # marker=markers[rIndx], markersize=3, linestyle='None',
                        # label=REGION.instances[rIndx].name)
            # ax1.legend()
            # ax1.set_title("Raw-image: {}".format(source.sequence_numbers[i]))

            # f.tight_layout(rect=[0, 0.03, 1, 0.95])
            # plt.pause(gdata.wait)

        #-------------------------------------------------------------------------------

        # TODO: Removed for scipting

        # plt.pause(1)  # <-------
        # print('\n \n')

        # input('<Hit Enter To Close Figures>')

        plt.close()

    return source, sequence


shortOptions = ""
longOptions = ["help", "job=", "verbosity=", "out-file=", "plot"]


def printUsage():
    """Print Usage instructions."""
    print("")
    print("Usage: imageAnalyse.py [--help] [--job=<jobFileName>] [--verbosity=0 (1 or 2)]")
    print("                [--out-file=<FileName>] [--plot]")
    return


class MyError(Exception):
    """Error catching exception."""

    def __init__(self, value):
        """Initilaise class."""
        self.value = value

    def __str__(self):
        """Create string."""
        return repr(self.value)


if __name__ == "__main__":
    userOptions = getopt(sys.argv[1:], shortOptions, longOptions)
    uoDict = dict(userOptions[0])

    if len(userOptions[0]) == 0 or "--help" in uoDict:
        printUsage()
        sys.exit(1)

    # execute the code
    try:
        main(uoDict)
        # print("\n \n")
        # print("SUCCESS.")
        # print("\n \n")

    except MyError as e:
        print("This run has gone bad.")
        print(e.value)
        sys.exit(1)
