#! /usr/bin/env python3
"""
Python Code to track blobs on series of images.

Analyses motion of objects in the image by applying an averaging routine as well
as Kalman Filters to track system dynamics. 

Author: Ingo Jahn
Last Modified: 22/03/2019
"""

import datetime as datetime
from filterpy.kalman import ExtendedKalmanFilter
from filterpy.common import Q_discrete_white_noise
from getopt import getopt
import imageAnalyse as iA
import matplotlib.pyplot as plt
import numpy as np
import os as os
import pdb
import shutil as shutil
import sys as sys
import FilterClasses as KFilters


class CustomError(Exception):
    def __init__(self, value):
        self.value = value
    def __str__(self):
        return repr(self.value)


class GConf:
    # Define all global-level config vars (with default value if applicable)
    _config = {
        "VERBOSITY"         : 0,
        "PX2M"              : 1.,  # conversion factor from pixels (unit in input file) to m
        "IMAGE_DIR"         : "./",  # path to image directory
        "RUN_NAME"          : "run",  #

        "IMAGE_DATA_FILE"   : "run-img_data.txt",
        "FILTER_DATA_FILE"  : "run-filter-data.txt",
        "FILTER_DATA_FILE_LIST" : None,

        "ASSOCIATION_TYPE"  : "nearest",  # how predictions and measurements are associated
        "FILTER_TYPE"       : None,
        "DT"                : 1.,
        }

    class classproperty(property):
        def __get__(self, cls, owner):
            return classmethod(self.fget).__get__(None, owner)()

    for var in _config.keys():
        exec("@classproperty\ndef {0}(cls): return cls._config['{0}']".format(var))

    @staticmethod
    def read_inputs(settings):
        # Check if the input variables are valid then overwrite
        for option, value in settings.items():
            if option not in GConf._config:
                raise CustomError(
                        "Invalid configuration variable '" + option +
                        "' supplied. Valid configuration variables are [" +
                        ", ".join([GConf._config]) + "]"
                        )
            GConf._config[option] = value

    @staticmethod
    def check_inputs():
        # Valid value ranges for each config option
        # Config variables not in this dict will NOT be checked
        valid_input_dict = {
            "VERBOSITY"         : [0, 1, 2, 3],
            }
        for option, valid_inputs in valid_input_dict.items():
            if GConf._config[option] not in valid_inputs:
                raise CustomError(
                        "Configuration variable " + option + " = " +
                        str(GConf._config[option]) + " is not valid." +
                        "Valid values are " + str([valid_inputs]))


def prep_main(GConf):
    raise CustomError("--prep Option not yet ported across")
    return None


def run_main(GConf, uoDict, associate_config, filter_settings):

    # Following code has not been updated yet.
    if GConf.VERBOSITY >= 1:
        print("START: Loading data from data-file.")
        # load outputfile from image processing routine
    '''
    # read file even if .py extension is forgotten
    if ".py" not in dataFileName:
        dataName = dataFileName
        dataFileName = ''.join([dataFileName, ".py"])
    else:
        # strip .py extension from jobName
        dataName = dataFileName.replace('.py', '')
    '''
    # Make sure that the desired data-file actually exists
    if GConf.IMAGE_DATA_FILE not in os.listdir('.'):
        local_directory = os.path.dirname(os.path.realpath(__file__))
        raise CustomError("No data-file {0} found in directory: {1}".format(GConf.IMAGE_DATA_FILE, local_directory))

    # Load data and group it by regions.
    regionData = []
    with open(GConf.IMAGE_DATA_FILE, 'r') as file:
        # get frameList and regionList
        frameList = []
        regionList = []
        for line in file:
            if 'col' in line:
                HeaderList = line[1:].split(" ")
            if line[0] == '#':  # skip lines that are commented
                continue
            lineData = line.split(' ')  # split data by spaces
            frameList.append(int(lineData[0]))
            regionList.append(int(lineData[1]))
        frameList = list(set(frameList))  # convert to set and back to remove duplicates
        regionList = list(set(regionList))  # convert to set and back to remove duplicates
        frameList.sort()  # sort the list
        regionList.sort()  # sort the list

        for i, rIndx in enumerate(regionList):
            # initialise regionData
            data = [[]] * len(frameList)
            file.seek(0)  # move pointer back to start of file for repeat reads.
            for line in file:
                if line[0] == "#":  # skip lines that are commented
                    continue
                # now start reading data
                lineData = line.split(' ')  # split data by spaces
                if int(lineData[1]) == rIndx:
                    # print(lineData)
                    # print('rIndx', rIndx)
                    fIndx = frameList.index(int(lineData[0]))
                    # print('fIndx', fIndx)
                    current = data[fIndx].copy()
                    temp_raw = [float(x) for x in lineData[2:6]]
                    temp_raw[0] = temp_raw[0]*GConf.PX2M  # convert from px to m
                    temp_raw[1] = temp_raw[1]*GConf.PX2M  # convert from px to m
                    temp_raw[2] = temp_raw[2]*GConf.PX2M*GConf.PX2M  # convert from px2 to m2
                    current.append(temp_raw)
                    # print('current', current)
                    data[fIndx] = current.copy()
            # convert data into set of arrays
            dataListOfArrays = []
            for d in data:
                dataListOfArrays.append(np.array(d))
            regionData.append(dataListOfArrays)  # add data into regionData

    # final data structure
    # regionData = [regionList0, regionList1, regionList2, ...]
    # regionList0 = [frame0Array, frame1Array, frame2Array, ...]

    # get number of steps
    filter_settings["NSteps"] = len(frameList)
    # filter_settings.NSteps = len(frameList)

    if GConf.VERBOSITY >= 1:
        print("    COMPLETE: Loading data from date-file={0} for {1} steps.".format(GConf.IMAGE_DATA_FILE, filter_settings["NSteps"]))

    if GConf.VERBOSITY >= 1:
        print("START: Executing jobFile.")

    # find number of targets to track for each region
    NRegions = len(regionData)
    if GConf.VERBOSITY > 1:
        print('Data has {} regions.'.format(NRegions))

    filter_settings["NTargets"] = []
    for rIndx in range(len(regionList)):
        NminTargets = 1e14  # set  to a high number that will be reduced
        NmaxTargets = 0
        for data in regionData[rIndx]:
            if NmaxTargets < len(data):
                NmaxTargets = len(data)
            if NminTargets > len(data):
                NminTargets = len(data)
        if NmaxTargets > filter_settings["N_max"][rIndx]:
            N = filter_settings["N_max"][rIndx]
        else:
            N = NmaxTargets
        filter_settings["NTargets"].append(N)
        if GConf.VERBOSITY > 1:
            print('    Region {0} has between {1} and {2} targets available. Tracking {3}.'.format(rIndx, NminTargets, NmaxTargets, N))

    if GConf.VERBOSITY >= 1:
        print("START: Setting up Kalman Filter.")

    nameList, KFilterList = KFilters.get_FilterList()

    # select appropriate Kalman Filter
    if not GConf.FILTER_TYPE.upper() in nameList:  # check that selected filter type is available
        raise CustomError("No filter of track.filterType={0} available. Possible options are: {1}".format(GConf.FILTER_TYPE, nameList))

    KFilterClass = KFilterList[nameList.index(GConf.FILTER_TYPE.upper())]

    KFilterClass.verbosity = GConf.VERBOSITY
    KFilterClass.loadregionData(regionData)  # pass data to filter
    KFilterClass.filterSetup(filter_settings)  # create filter based  on inputs
    KFilterClass.check_validity()  # check that filter has been set correctly

    if GConf.VERBOSITY >= 1:
        print("    COMPLETE: Setting up Kalman Filter.")

    ####################################
    # start Kalman filtering routine. ##
    ####################################
    if GConf.VERBOSITY >= 1:
        print("START: Kalman Filtering loop.")

    # create empty arrays
    x_filterList = []
    z_filterList = []
    x_rawList = []
    z_rawList = []
    covarianceList = []
    for i in range(filter_settings["NSteps"]):
        if GConf.VERBOSITY > 1:
            print('    ###############################')
            print("    Updating Kalman filter for step {0} / {1}.".format(i, filter_settings["NSteps"]))
            if i > 0:
                print('    State Vector:')
                print('    {}'.format(x_filter))
        KFilterClass.setIndex(i)  # keep filter updated to reference correct index

        # getting current measurement
        if i == 0:
            # z = sequence.blobData[i][0:N_target, 0:2]  # extract x, y location of each blob based initial table
            zList, z_new_indexList = KFilterClass.getCurrentMeasurement(i, NTargets=filter_settings["NTargets"], type='new')
            # print(zList)
            # z_new_index = np.arange(0, np.sum(track.NTargets))
        else:
            # once tracking has commenced, we need to do associations
            if GConf.ASSOCIATION_TYPE == 'nearest':
                z_oldList = zList
                KFilterClass.set_zOldList(z_oldList)
                # print("z_old", z_old)
                # z_new = sequence.blobData[i][:, 0:2]
                zNewList = KFilterClass.getCurrentMeasurement(i, NTargets=filter_settings["NTargets"]).copy()
                # print("z_new", z_new)
                z_new_indexList = []
                zList = []
                for rIndx, z_new in enumerate(zNewList):
                    if GConf.VERBOSITY > 1:
                        print("    Doing association for region {}.".format(rIndx))
                    tempZnew = z_new.copy()

                    z_old = z_oldList[rIndx]

                    z = np.zeros(np.shape(z_oldList[rIndx]))
                    # print('HERE', z, z_old, tempZnew)
                    z_new_index = []
                    for j in range(z.shape[0]):
                        dist = []
                        for n in range(tempZnew.shape[0]):
                            dist.append(np.sqrt((z_old[j, 0]-tempZnew[n, 0])**2
                                                + (z_old[j, 1]-tempZnew[n, 1])**2))
                        # print("dist:", dist)

                        if min(dist) > associate_config["distNearest"]:
                            # print('min(dist) > {0} for j={1}'.format(track.distNearest, j))
                            # print('z_old', z_old)
                            # print('tempZnew', tempZnew)
                            # print('index_min', index_min)
                            z[j, 0] = z_old[j, 0]
                            z[j, 1] = z_old[j, 1]
                            z_new_index.append(-1)
                        else:
                            index_min = np.argmin(dist)
                            z[j, 0] = tempZnew[index_min, 0]
                            z[j, 1] = tempZnew[index_min, 1]
                            # tempZnew = np.delete(tempZnew, (index_min), axis=0)
                            tempZnew[index_min, 0] = 2e6  # set large value in tempZnew, so that this is not called again
                            tempZnew[index_min, 1] = 3e6  # set large value in tempZnew, so that this is not called again
                            # print("index_min", index_min)
                            # print("tempZnew_short", tempZnew)
                            z_new_index.append(index_min)
                            # print("z", z)
                    if GConf.VERBOSITY > 1:
                        print("    {0} items were associated for region {1}.".format(len(z_new_index) - z_new_index.count(-1), rIndx))
                    zList.append(z)
                    z_new_indexList.append(z_new_index)
            else:
                raise CustomError("track.AssociationType ='{0}' is not implemented.".format(GConf.ASSOCIATION_TYPE))

        # flatten z to suit filter
        zTemp = np.concatenate(zList)
        z_flat = zTemp.flatten()

        KFilterClass.set_zNewIndex(z_new_indexList)  # set indexes obtained from Association
        # print('z_flat: \n', z_flat)
        # print('rk.x', rk.x)
        # print("z:", np.shape(z_flat))
        [x_raw, x_filter, z_filter] = KFilterClass.update(z_flat)
        # rk.update(z_flat, HJacobian_at, hx)
        x_rawList.append(x_raw)
        x_filterList.append(x_filter)
        # z_rawList.append(z_raw)
        z_filterList.append(z_filter)
        # rk.predict()
        covarianceList.append(KFilterClass.getCovariance())

    if GConf.VERBOSITY > 0:
        print("    COMPLETE: Kalman Filtering Loop.")

    x_filterList = np.array(x_filterList)

    """
    # computing average of raw state vector values
    x_rawAverage = []
    for i, frame in enumerate(x_rawList):
        # print(frame)
        x_rawAverage.append(np.average(frame, 0))
    x_rawAverage = np.array(x_rawAverage)
    x_rawList = np.array(x_rawList)
    """
    covariance = np.zeros((filter_settings["NSteps"], len(KFilterClass.stateVector)))
    for i in range(covariance.shape[0]):
        for k in range(len(KFilterClass.stateVector)):
            covariance[i, k] = covarianceList[i][k, k]

    # write data to out-file
    if GConf.FILTER_DATA_FILE is not None:
        print("\n")
        print("START: CREATING OUTPUT FILE")

        if os.path.isfile(GConf.FILTER_DATA_FILE):
            print("    Output file already exists.")
            fntemp = GConf.FILTER_DATA_FILE+".old"
            shutil.copyfile(GConf.FILTER_DATA_FILE, fntemp)
            print("    Existing file has been copied to:", fntemp)

        fp = open(GConf.FILTER_DATA_FILE, 'w')
        fp.write("#++++++++++++++++++++++++++++++\n")
        fp.write("#Ouput file from Motiontracker.py. \n")
        fp.write("#Date / Time: {} \n".format(datetime.datetime.now()))
        fp.write("#++++++++++++++++++++++++++++++\n")
        fp.write("#SOURCE: \n")
        fp.write("#DataFile: {} \n".format(GConf.IMAGE_DATA_FILE))
        # fp.write("#FilenameBase: {} \n".format(source.filenameBase))
        # fp.write("#firstFrame: {} \n".format(source.firstFrame))
        # fp.write("#LastFrame: {} \n".format(source.lastFrame))
        # fp.write("#FilenameSuffix: {} \n".format(source.suffix))
        fp.write("#deltaT: {} \n".format(GConf.DT))
        fp.write("#++++++++++++++++++++++++++++++\n")
        fp.write("#FILTER: \n")
        fp.write("#++++++++++++++++++++++++++++++\n")
        fp.write("#DATA: \n")
        # fp.write("col0:Time[s] col1:{} col2:AverageVelocity[rad/s] col3:FilteredAngle[rad] col4:FilteredVelocity[rad/s] col5:FilteredAcceleration[rad/s**2] \n")
        # write Header
        dataString = "# col0:Time[s]"
        for i, state in enumerate(KFilterClass.stateVector):
            dataString = dataString + " col{0}:{1} col{2}:{1}.P".format(2*i+1, state, 2*i+2)
        dataString = dataString.replace('\n', '')
        fp.write(dataString + '\n')
        # write data table
        for t in range(filter_settings["NSteps"]):
            # fp.write("{0:.12e} {1:.12e} {2:.12e} {3:.12e} {4:.12e} {5:.12e} \n".format(t*track.deltaT, Angle_average[t], Speed_average[t], output[t, 0], output[t, 1], output[t, 2]))
            # fp.write("{0:.12e} {1:.12e} {2:.12e} {3:.12e} {4:.12e} {5:.12e} \n".format(t*track.deltaT, -1, -1, output[t, 0], output[t, 1], output[t, 2]))
            dataString = "{0:.12e}".format(t*GConf.DT)
            for i in range(len(KFilterClass.stateVector)):
                dataString = dataString + " {0:.12e} {1:.12e}".format(x_filterList[t, i], covariance[t, i])
            fp.write(dataString + '\n')

        if GConf.VERBOSITY > 0:
            print('COMPLETE: Creating Output File.')

    return None


def post_main(GConf, uoDict, plot_config):
    """
    Plot the output data from filtering.

    plot_config is a dictionary containing settigns that define how data is plotted.
    """

    def load_plot_data(fileName, GConf):
        if GConf.VERBOSITY > 0:
            print("")
            print("Starting to read data from {}.".format(fileName))
        time = []
        data = []
        data_P = []
        # load data from "FILTER_DATA_FILE"
        with open(GConf.FILTER_DATA_FILE, 'r') as f:
            for line in f:
                if 'col0:' in line:  # Grab Headers
                    Header = line[1:].split(" ")
                    Header = [i for i in Header if i]  # removed empties
                if line[0] == '#':  # skip commented line
                    continue
                d = []
                d_p = []
                for i, word in enumerate(line.split(" ")):
                    if i == 0:
                        time.append(float(word))
                    if i % 2 == 0:  # even indices
                        d_p.append(float(word))
                    else:
                        d.append(float(word))
                data.append(d)
                data_P.append(d_p)
        time = np.array(time)
        data = np.array(data)
        data_P = np.array(data_P)
        Header_data = []
        Header_data_P = []

        for i, word_long in enumerate(Header):
            word = word_long.split(':')[-1]
            if i == 0:
                Header_time = word
                continue
            elif i % 2 == 0:  # even indices
                Header_data_P.append(word)
            else:
                Header_data.append(word)
        return time, data, data_P, Header_time, Header_data, Header_data_P

    if GConf.FILTER_DATA_FILE_LIST is None:
        time, data, data_P, Header_time, Header_data, Header_data_P = load_plot_data(GConf.FILTER_DATA_FILE, GConf)
    else:
        time, data, data_P, Header_time, Header_data, Header_data_P = load_plot_data(GConf.FILTER_DATA_FILE_LIST[0], GConf)

    # create plots of data
    if '--plot' in uoDict:
        # plotting code goes here.

        # create functions required for secondary axis
        def rad2deg(x):
            return np.degrees(x)

        def deg2rad(x):
            return np.radians(x)

        def m2mm(x):
            return x*1000.

        def mm2m(x):
            return x/1000.

        if plot_config["figure_list"] is None:
            f, axList = plt.subplots(len(Header_data), 1, sharex=True)
            f.suptitle('Kalman Filter State Variables', fontsize=16)
        else:  # get uniquq entries in figure_list
            axList = []
            if len(plot_config["figure_list"]) > len(Header_data):
                plot_config["figure_list"] = plot_config["figure_list"][0:len(Header_data)]
            for f in set(plot_config["figure_list"]):
                f, axL = plt.subplots(plot_config["figure_list"].count(f), 1, sharex=True)
                axL[-1].set_xlabel(Header_time)
                axList.extend(axL)
                f.suptitle('Kalman Filter State Variables', fontsize=16)

        secax = []
        for i, name in enumerate(Header_data):
            print(i, name, axList[i])
            axList[i].plot(time, data[:, i])
            if '[rad]' in name:  # if state variable in radians, add secondary axis in deg
                secax.append(axList[i].secondary_yaxis('right', functions=(rad2deg, deg2rad)))
                secax[-1].set_ylabel('[deg]')
            if '[rad/s]' in name:  # if state variable in radians, add secondary axis in deg
                secax.append(axList[i].secondary_yaxis('right', functions=(rad2deg, deg2rad)))
                secax[-1].set_ylabel('[deg/s]')
            if '[rad/s2]' in name:  # if state variable in radians, add secondary axis in deg
                secax.append(axList[i].secondary_yaxis('right', functions=(rad2deg, deg2rad)))
                secax[-1].set_ylabel('[deg/s2]')
            if '[m]' in name:
                secax.append(axList[i].secondary_yaxis('right', functions=(m2mm, mm2m)))
                secax[-1].set_ylabel(name.replace('[m]', '[mm]'))
            if '[m/s]' in name:
                secax.append(axList[i].secondary_yaxis('right', functions=(m2mm, mm2m)))
                secax[-1].set_ylabel(name.replace('[m/s]', '[mm/s]'))
            if plot_config["plot_covariance"] is True:
                axList[i].plot(time, np.array(data[:, i]) + np.array(data_P[:, i]), 'k:')
                axList[i].plot(time, np.array(data[:, i]) - np.array(data_P[:, i]), 'k:')
            axList[i].set_ylabel(name)

        if GConf.FILTER_DATA_FILE_LIST is not None:
            for file in GConf.FILTER_DATA_FILE_LIST[1:]:
                time, data, data_P, Header_time, Header_data, Header_data_P = load_plot_data(file, GConf)
                for i in range(len(Header_data)):
                    axList[i].plot(time, data[:, i])
                    if plot_config["plot_covariance"] is True:
                        axList[i].plot(time, np.array(data[:, i]) + np.array(data_P[:, i]), 'k:')
                        axList[i].plot(time, np.array(data[:, i]) - np.array(data_P[:, i]), 'k:')
        """
        # figure showing how raw data (with initial offset removed
        f, axList = plt.subplots(NDatas, 1, sharex=True)
        axList[0].set_title('Raw Data')
        for i, name in enumerate(KFilterClass.dataVector):
            for k in range(np.shape(x_rawList)[1]):
                axList[i].plot(timeVector, x_rawList[:, k, i] - x_rawList[0, k, i])
            axList[i].set_ylabel(name)
        axList[NDatas-1].set_xlabel('Time [s]')
        """

        """
        # figure showing how avergae of measured data changes
        f, axList = plt.subplots(NDatas, 1, sharex=True)
        axList[0].set_title('Average of Measurements')
        for i, name in enumerate(KFilterClass.dataVector):
            axList[i].plot(timeVector, x_rawAverage[:, i])
            axList[i].set_ylabel(name)
        axList[NDatas-1].set_xlabel('Time [s]')
        """

        """
        f, (ax1, ax2, ax3, ax4, ax5) = plt.subplots(5, 1, sharex=True)
        ax1.plot(np.arange(0, len(output[:, 0]))*track.deltaT, blob1[:, 0], 'r-')
        ax1.plot(np.arange(0, len(output[:, 0]))*track.deltaT, blob2[:, 0], 'g-')
        ax1.set_ylabel('X-position [px]')
        ax2.plot(np.arange(0, len(output[:, 0]))*track.deltaT, blob1[:, 1], 'r-')
        ax2.plot(np.arange(0, len(output[:, 0]))*track.deltaT, blob2[:, 1], 'g-')
        ax2.set_ylabel('Y-position [px]')
        ax3.plot(np.arange(0, len(output[:, 0]))*track.deltaT, blob1[:, 2], 'r-')
        ax3.plot(np.arange(0, len(output[:, 0]))*track.deltaT, blob2[:, 2], 'g-')
        ax3.set_ylabel('Distance from origin [px]')
        ax4.plot(np.arange(0, len(output[:, 0]))*track.deltaT, blob1[:, 3], 'r-')
        ax4.plot(np.arange(0, len(output[:, 0]))*track.deltaT, blob2[:, 3], 'g-')
        ax4.set_ylabel('Angle [rad]')
        ax5.plot(np.arange(0, len(output[:, 0]))*track.deltaT, blob1[:, 3]-blob1[0, 3], 'r-')
        ax5.plot(np.arange(0, len(output[:, 0]))*track.deltaT, blob2[:, 3]-blob2[0, 3], 'g-')
        ax5.set_ylabel('Angle [rad]')
        ax5.set_xlabel('Time [s]')

        f, (ax1) = plt.subplots(1, 1)
        for k in range(N_target):
            ax1.plot(np.arange(0, len(output[:, 0]))*track.deltaT, blobbb[:, 3, k]-blobbb[0, 3, k])
        ax1.plot(np.arange(0, len(output[:, 0]))*track.deltaT, Angle_average-Angle_average[0], 'k--', Linewidth=2, label='Average Measurement')
        ax1.plot(np.arange(0, len(output[:, 0]))*track.deltaT, output[:, 0], 'b--', Linewidth=2, label='Kalman filter')
        ax1.legend(loc=0)
        ax1.set_ylabel('Angle [rad]')
        ax1.set_xlabel('Time [s]')
        ax1.set_title('Track of {} blobs being tracked'.format(N_target))
        """

        plt.draw()

        plt.pause(1)  # <-------
        print('\n \n')

        input('<Hit Enter To Close Figures>')

        plt.close()

    return 0

def main(uoDict):
    # Read the jobfile then do some input checking
    exec(open(uoDict["--job"]).read(), globals())
    GConf.read_inputs(global_config)
    GConf.check_inputs()

    if "--prep" in uoDict:
        print("Running '--prep'.")
        # run image processing routine
        prep_main(GConf)

    if "--run" in uoDict:
        print("Running '--run'.")
        # run Kalman Filter Routine
        run_main(GConf, uoDict, associate_config, filter_settings)
        pass

    if "--post" in uoDict:
        print("Running '--post'")
        # plot results.
        post_main(GConf, uoDict, plot_config)

    return None


shortOptions = ""
longOptions = ["help", "job=", "prep", "run", "post", "data-file=", "out-file=", "plot", "verbosity="]


def printUsage():
    """Print Usage instructions."""
    print("")
    print("Usage: MotionTracker.py [--help] [--job=<jobFileName>] [--data-file=<FileName>]")
    print("                [--out-file=<FileName>] [--plot] [--verbosity=0 (1 or 2)]")
    return


if __name__ == "__main__":
    userOptions = getopt(sys.argv[1:], shortOptions, longOptions)
    uoDict = dict(userOptions[0])

    if len(userOptions[0]) == 0 or "--help" in uoDict:
        printUsage()
        sys.exit(1)

    # execute the code
    try:
        main(uoDict)
        print("\n \n")
        print("SUCCESS.")
        print("\n \n")

    except CustomError as e:
        print("This run has gone bad.")
        print(e.value)
        sys.exit(1)
