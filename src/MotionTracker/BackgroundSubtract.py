#! /usr/bin/env python3
"""
Preprocess raw video to enable accurate blob detection.

Steps are as follows:
  1. Read raw video file.
  2. Perform 'background subtraction' using MOG2 algorithm.
     This step produces a grayscale images of the moving parts of the image.
  3. Resize images by a scaling factor (scale length and width by 3 by default).
  4. Apply Gaussian blur (5 px stencil by default).
  5. Apply thresholding to obtain pure black and white image.
  6. Invert colours and add 1 px border (req'd for blob detection algorithm,
     this step is not shown in the video output).
  7. Export.

Author:      Viv Bone
First code:  24-01-20
"""

import numpy as np
import cv2
import sys
import os
import shutil
from getopt import getopt
from skimage.filters import threshold_yen
from skimage.exposure import rescale_intensity
from skimage.io import imread, imsave

# Need these two lines to stop OpenCV hanging on some machines
import os
os.environ['DISPLAY'] = ':0'

# Global settings
SETTINGS = {
    "LEARN_RATE"   : -1, # auto
    "FRAME_PAUSE"  : 30,
    # "FRAME_PAUSE"  : 1000,
    "BLUR_SIZE"    : 5,
    "THRESH_ALPHA" : 30,
    "RESCALE"      : 3
    }

class CustomError(Exception):
    def __init__(self, value):
        self.value = value
    def __str__(self):
        return repr(self.value)

def check_inputs(uo_dict):
    # Check all mandatory options have been provided
    reqd_options = ["--source", "--out_dir"]
    for op in reqd_options:
        if op not in uo_dict:
            raise CustomError("".join(("BackgroundSubtract.py requires argument '",
                op, "', but this argument was not provided.\n")))

    # Check that source file exists
    if not os.path.isfile(uo_dict["--source"]):
        raise CustomError("".join(("Source file '", uo_dict["--source"], "' not found,",
            " check that file path is correct.\n")))

    # Check that output directory string has actually been specified
    if len(uo_dict["--out_dir"]) == 0:
        raise CustomError("".join(("Output image destination directory not "
            "specifed, check argument to '--out_dir='.")))

    # Check that output directory doesn't exist or overwrite flag has been specified
    if os.path.isdir(uo_dict["--out_dir"]) and "--overwrite" not in uo_dict:
        raise CustomError("".join(("Output image destination directory '", uo_dict["--source"],
            "' already exists, please specify a new directory or supply '--overwrite' flag.\n")))

def overwrite_settings(uo_dict):
    if "--blur_size" in uo_dict:
        SETTINGS["BLUR_SIZE"] = int(uo_dict["--blur_size"])
    if "--threshold_alpha" in uo_dict:
        SETTINGS["THRESH_ALPHA"] = int(uo_dict["--threshold_alpha"])
    if "--img_scale" in uo_dict:
        SETTINGS["RESCALE"] = int(uo_dict["--img_scale"])

def setup_output_directory(uo_dict):
    uo_dict["--out_dir"] = "/".join((os.getcwd(), uo_dict["--out_dir"])) # absolute path

    # Clean existing directory if overwriting (confirm <out_dir> is specified so
    # we don't accidentally delete the parent directory)
    if "--overwrite" in uo_dict and len(uo_dict["--out_dir"]) > 0:
        # Check outdir exists, otherwise don't need to do anything
        if os.path.isdir(uo_dict["--out_dir"]):
            shutil.rmtree(uo_dict["--out_dir"])

    os.mkdir(uo_dict["--out_dir"])

def process_video(uo_dict):
    cap = cv2.VideoCapture(uo_dict["--source"])
    fgbg = cv2.createBackgroundSubtractorMOG2()
    n_frames = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))

    i = 1 # for naming output files
    while(1):
        im_number = '{:0{width}d}'.format(i, width=len(str(n_frames)))
        im_name = "".join((uo_dict["--source"].split(".")[0], "_", im_number, ".tiff"))
        col_im_name = "".join((uo_dict["--source"].split(".")[0], "_col_",
            im_number, ".tiff"))

        ret, frame = cap.read()
        img = cv2.resize(frame, None, fx=SETTINGS["RESCALE"],
                fy=SETTINGS["RESCALE"], interpolation=cv2.INTER_CUBIC)

        # Trying to do 'levels' adjustment, this implementation is not effective
        # yen_threshold = threshold_yen(img)
        # img = rescale_intensity(img, (0, yen_threshold), (0, 255))

        img = fgbg.apply(img, learningRate=SETTINGS["LEARN_RATE"])
        img = cv2.GaussianBlur(img, (SETTINGS["BLUR_SIZE"], SETTINGS["BLUR_SIZE"]), 0)
        ret_val, img = cv2.threshold(img, SETTINGS["THRESH_ALPHA"], 255, cv2.THRESH_BINARY)

        # Prepare the colour image
        col_img_scaled = cv2.resize(frame, None, fx=SETTINGS["RESCALE"],
                fy=SETTINGS["RESCALE"], interpolation=cv2.INTER_CUBIC)

        if "--no_write" not in uo_dict:
            save_img = cv2.bitwise_not(img)
            # Add borders (required to detect blobs on edges)
            bw = 1
            save_img = cv2.copyMakeBorder(
                            save_img, 
                            bw, bw, bw, bw,
                            cv2.BORDER_CONSTANT, 
                            value=[255,255,255])
            cv2.imwrite("/".join((uo_dict["--out_dir"], im_name)), save_img)
            save_col_img = cv2.copyMakeBorder(
                            col_img_scaled, 
                            bw, bw, bw, bw,
                            cv2.BORDER_CONSTANT, 
                            value=[255,255,255])
            cv2.imwrite("/".join((uo_dict["--out_dir"], col_im_name)), save_col_img)

        if "--no_display" not in uo_dict:
            img = cv2.cvtColor(img, cv2.COLOR_GRAY2BGR)
            cat_img = np.concatenate((img, col_img_scaled), axis=0)
            cv2.imshow('frame', cat_img)
            k = cv2.waitKey(SETTINGS["FRAME_PAUSE"]) & 0xff
            if k == 27: break

        else:
            k = cv2.waitKey(0) & 0xff
            if k == 27: break

        if i == n_frames: break
        i = i + 1

    cap.release()
    cv2.destroyAllWindows()

def main(uo_dict):
    check_inputs(uo_dict)
    overwrite_settings(uo_dict)
    setup_output_directory(uo_dict)
    process_video(uo_dict)

def print_usage():
    print("\n")
    print("Separate moving components of video from static background to ")
    print("enable accurate particle detection.")
    print("Outputs greyscale frames that show moving areas of image.")
    print("")
    print("Usage: BackgroundSubtract.py [--help] [--source=<VideoFileName>]")
    print("             [--out_dir=<OutputImageDirectory>] [--overwrite]")
    print("             [--no_display] [--no_write] [--blur_size=<BlurStencil>]")
    print("             [--threshold_alpha=<ThresholdAlpha>]")
    print("             [--img_scale=<ImgScale>]")
    print("")
    print("<OutputImageDirectory> and <VideoFileName> are mandatory and must ")
    print("be specified relative to the current working directory.")
    print("")
    print("Supply '--overwrite' flag to overwrite contents of already existing ")
    print("<OutputImageDirectory>.")
    print("")
    print("Supply '--no_display' flag to to suppress video or '--no_write' to ")
    print("disable saving images.")
    print("")
    print("Blur size is adjustable with '--blur_size=' argument (default = 5px).")
    print("Greyscale conversion is adjustable with '--threshold_alpha=' argument ")
    print("(default = 30, values from 0 to 255 permitted).")
    print("Image rescaling using intercubic interpolation (to improve background ")
    print("separation performance) is adjustable with '--img_scale=' argument ")
    print("(integer scaling factor, default = 3).")
    print("\n")

short_options = ""
long_options = ["help", "source=", "out_dir=", "overwrite", "no_display",
        "no_write", "blur_size=", "threshold_alpha=", "img_scale="]

if __name__ == "__main__":
    user_options = getopt(sys.argv[1:], short_options, long_options)
    uo_dict = dict(user_options[0])

    if len(user_options[0]) == 0 or "--help" in uo_dict:
        print_usage()
        sys.exit(1)

    else:
        try:
            main(uo_dict)
            # print("\n")
            # print("SUCCESS.")
            # print("\n")

        except CustomError as e:
            print("\nERROR: " + e.value)
            sys.exit(1)


