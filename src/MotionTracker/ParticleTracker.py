#! /usr/bin/env python3
"""
Code to track particles using output data from imageAnalyse.py
"""

import copy
import os
import sys

import matplotlib.pyplot as plt
import numpy as np
from scipy import stats
import statistics as st

from getopt import getopt

# Need these two lines to stop code hanging on some machines
import os
os.environ['DISPLAY'] = ':0'


# Default global settings
G = {
    # Tracking parameters
    "J_thresh"          : 2000, # Works for 160K
    "max_missed_frames" : 2,
    "dy_max"            : np.inf,
    # Initial velocity estimate
    "n_pts_v0" : 60,
    "v0_min"   : None,
    "v0_max"   : None,
    # Flow parameters
    "flow_dir"    : None,
    "exit_x_dist" : None,
    # "entry_x_dist"  : None, # DEPRECATED
    # Velocity estimation
    "int_x_dist" : None,
    "img_scale"  : None,
    "frame_rate" : None,
    "px_per_cm"  : None
}


class BlobData:
    I = { 'frame' : 0, 'region' : 1, 'data_start' : 2 } # For indexing lines

    def __init__(self, data_file_name):
        self.data_file_name = data_file_name
        self.frame_start = 999999999
        self.frame_end   = 0
        self.region_list = []
        self._data = {} # contains FrameData objects with frame numbers as keys
        self.variance = {}

        # Loop through lines and add info
        with open(data_file_name, 'r') as data_file:
            data_flag = 0
            for raw_line in data_file:
                # Do nothing before data section is reached
                if data_flag == 0:
                    if 'DATA' in raw_line: data_flag = 1
                else:
                    line = raw_line.split(' ') # split data by spaces
                    # Skip the final two rows
                    if line[0] == 'END' or line[0][0] == '+': pass
                    # otherwise read data row
                    else: self.add_line(line)

        # Loop through regions and get combined data arrays, so that we can
        # calculate variances for initial data
        x_region_data = dict.fromkeys(self.region_list, np.zeros(0))
        y_region_data = dict.fromkeys(self.region_list, np.zeros(0))
        for f in range(self.frame_start, self.frame_end + 1):
            for r in self.region_list:
                x_region_data[r] = np.hstack(
                        (x_region_data[r], self.get_data(f, r).x_data()))
                y_region_data[r] = np.hstack(
                        (y_region_data[r], self.get_data(f, r).y_data()))
        # Calculate variance for each region
        for r in self.region_list:
            self.variance[r] = {}
            self.variance[r]['x'] = st.pvariance(x_region_data[r])
            self.variance[r]['y'] = st.pvariance(y_region_data[r])

    def get_data(self, frame, region=None):
        # If the frame doesn't exist, make a blank; TODO
        if frame not in self._data: self._data[frame] = FrameData(frame)
        if region is not None:
            return self._data[frame].get_data(region)
        else:
            return self._data[frame]

    def add_line(self, line):
        frame_id = int(line[self.I['frame']])
        region_id = int(line[self.I['region']])
        line_data = line[self.I['data_start']:-1] # -1 to remove newline char

        if frame_id < self.frame_start: self.frame_start = frame_id
        if frame_id > self.frame_end:   self.frame_end = frame_id
        if region_id not in self.region_list:
            self.region_list.append(region_id)

        if frame_id not in self._data:
            # New frame, add new entry
            self._data[frame_id] = FrameData(frame_id)
        # Add the line data
        self._data[frame_id].add_line(frame_id, region_id, line_data)

class FrameData:
    def __init__(self, frame_id):
        self.region_list = []
        self._data = {}
        self.frame_id = frame_id

    def get_data(self, region):
        # If the region doesn't exist, make a blank; TODO
        if region not in self._data:
            return RegionData(self.frame_id, region)
        else:
            return self._data[region]

    def add_line(self, frame_id, region_id, line_data):
        assert(self.frame_id == frame_id)
        if region_id not in self.region_list:
            # New region, add new entry
            self._data[region_id] = RegionData(frame_id, region_id)
            self.region_list.append(region_id)
        self._data[region_id].add_line(frame_id, line_data)

class RegionData:
    I = { 'x' : 0, 'y' : 1, 'size' : 2, 'angle' : 3 }

    def __init__(self, frame_id, region_id):
        self.n_particles = 0
        self.frame_id = frame_id
        self.region_id = region_id
        self._data = np.zeros((0,4))

    def make_new_line(self, line_data):
        return np.array(line_data).astype(np.double)

    def add_line(self, frame_id, line_data):
        self.n_particles += 1
        assert(self.frame_id == frame_id)
        self._data = np.vstack((self._data, self.make_new_line(line_data)))

    def x_data(self, i=None):
        if i is not None: return self._data[:, self.I['x']][i]
        else:             return self._data[:, self.I['x']]

    def y_data(self, i=None):
        if i is not None: return self._data[:, self.I['y']][i]
        else:             return self._data[:, self.I['y']]

    def size(self, i=None):
        if i is not None: return self._data[:, self.I['size']][i]
        else:             return self._data[:, self.I['size']]

    def angle(self, i=None):
        if i is not None: return self._data[:, self.I['angle']][i]
        else:             return self._data[:, self.I['angle']]

    def get_particle_data(self, i):
        out_data = {
                'x'    : self._data[i][self.I['x']],
                'y'    : self._data[i][self.I['y']],
                'size' : self._data[i][self.I['size']]
                }
        return out_data


class ParticleData:
    def __init__(self, region_id, init_frame, point_data):
        self.missed_frames = 0
        self.data = {}
        self.estimate = {}
        self.region_id = region_id
        self.frame_start = init_frame # first appearance
        self.frame_end = None       # last appearance
        self.terminated_flag = False
        self.vx = np.nan
        self.vy = np.nan

        self.data[str(init_frame)] = point_data
        # Set J = 0 for first frame
        self.data[str(init_frame)]['J'] = 0.0

        # Next-timestep predicted and actual positions
        self.x = point_data['x']
        self.y = point_data['y']
        self.x_hat = None
        self.y_hat = None

        # To hold x-t and y-t data once tracking is complete
        self.x_arr = None
        self.y_arr = None
        self.J_arr = None

    def add_point(self, frame_id, point_data, J=0.0):
        self.data[str(frame_id)] = point_data
        # J is association residual
        self.data[str(frame_id)]['J'] = J
        # self.x = point_data['x'] # TODO
        # self.y = point_data['y']

    def get_frame_data(self, frame_id):
        return self.data[str(frame_id)]

    def terminate(self, frame_id):
        self.frame_end = frame_id
        self.terminated_flag = True

    def update_velocity_estimate(self, vx, vy):
        self.vx = vx
        self.vy = vy

    def propagate(self, frame_id):
        """
        Propagate active particle forward using mean velocity from last 'n'
        frames.
        If a particle has only existed for one frame, then propagate using
        initial velocity v0 = (vx_0, vy_0)
        """
        # Don't need to propagate terminated particles
        if not self.terminated_flag:
            assert(not np.isnan(self.vx))
            assert(not np.isnan(self.vy))
            self.x_hat = self.data[str(frame_id - 1)]['x'] + self.vx
            self.y_hat = self.data[str(frame_id - 1)]['y'] + self.vy

    def propagate_true_pos(self, frame_id): # TODO: naming?
        if not self.terminated_flag:
            self.x += self.vx
            self.y += self.vy
            self.estimate[str(frame_id)] = {'x' : self.x, 'y' : self.y}

    def estimate_velocity_old(self, frame_id):
        """
        Compute mean velocity using average over last 'n_frames_max' frames.
        """
        if not self.terminated_flag:
            vx_sum = 0.0
            vy_sum = 0.0
            n_frames_max = 5
            n_frames = 0
            for i in range(n_frames_max):
                if str(frame_id - i - 1) in self.data:
                    vx_sum += (self.data[str(frame_id - i)]['x']
                            -  self.data[str(frame_id - i - 1)]['x'])
                    vy_sum += (self.data[str(frame_id - i)]['y']
                            -  self.data[str(frame_id - i - 1)]['y'])
                    n_frames += 1
                else: break
            self.vx = vx_sum / n_frames
            self.vy = vy_sum / n_frames

    def estimate_velocity(self, frame_id):
        K = 0.3 # AR filter gain; TODO: main input
        if not self.terminated_flag:
            vx_bar = (self.data[str(frame_id)]['x'] - self.data[str(frame_id - 1)]['x'])
            vy_bar = (self.data[str(frame_id)]['y'] - self.data[str(frame_id - 1)]['y'])
            self.vx = self.vx + K * (vx_bar - self.vx)
            self.vy = self.vy + K * (vy_bar - self.vy)

    def form_results_arrays(self):
        # # TODO: switch, should predict pos on final step
        # n_frames = len(self.data)
        # self.x_arr = np.zeros(n_frames)
        # self.y_arr = np.zeros(n_frames)
        # self.J_arr = np.zeros(n_frames)
        # # Convert spatial coordinates to 'actual' values
        # s = 1 / (G["px_per_cm"] * 100 * G["img_scale"])
        # for i in range(n_frames):
            # self.J_arr[i] = self.data[str(i + self.frame_start)]['J']
            # self.x_arr[i] = s * self.data[str(i + self.frame_start)]['x']
            # self.y_arr[i] = s * self.data[str(i + self.frame_start)]['y']

        n_frames = len(self.estimate)
        self.x_arr = np.zeros(n_frames)
        self.y_arr = np.zeros(n_frames)
        self.J_arr = np.zeros(n_frames)
        # Convert spatial coordinates to 'actual' values
        s = 1 / (G["px_per_cm"] * 100 * G["img_scale"])
        for i in range(n_frames):
            self.J_arr[i] = self.data[str(i + self.frame_start)]['J']
            self.x_arr[i] = s * self.estimate[str(i + self.frame_start)]['x']
            self.y_arr[i] = s * self.estimate[str(i + self.frame_start)]['y']


class CustomError(Exception):
    def __init__(self, value):
        self.value = value
    def __str__(self):
        return repr(self.value)


def load_data(uo_dict):
    """ Load data file and store information in 'BlobData' object. """
    data_file_name = uo_dict.get("--data-file")

    # Make sure the specified data file actually exists
    if data_file_name not in os.listdir('.'):
        local_directory = os.path.dirname(os.path.realpath(__file__))
        raise CustomError("No data-file {0} found in directory: {1}"
                .format(data_file_name, local_directory))

    return BlobData(data_file_name)

def initialize_particle_list(blob_data, frame=0):
    """ Initialize 'ParticleData' objects for all particles in given frame. """
    particle_list = []
    frame_0 = blob_data.frame_start + frame
    # Loop through the regions and make a new object for each particle
    for region_id in blob_data.region_list:
        region_data = blob_data.get_data(frame_0, region_id)
        for i in range(region_data.n_particles):
            particle_list.append(ParticleData(region_id, frame_0,
                region_data.get_particle_data(i)))

    return particle_list

def compute_initial_velocity_estimate(blob_data):
    # TODO: N_FRAMES should be input (and fix docs)
    N_FRAMES = 50 # Compute initial velocity estimate using this many frames
    if G["flow_dir"] == "l2r":
        v_x = np.linspace(G["v0_min"] / G["img_scale"], G["v0_max"] / G["img_scale"], G["n_pts_v0"])
    elif G["flow_dir"] == "r2l":
        v_x = np.linspace(-G["v0_min"] / G["img_scale"], -G["v0_max"] / G["img_scale"], G["n_pts_v0"])
    J_v = np.zeros(G["n_pts_v0"])

    # Loop through the candidate velocities and compute association residuals
    # over N_FRAMES frames.
    for i_vel in range(len(v_x)):
        for i_frame in range(N_FRAMES):
            p_list_tmp = initialize_particle_list(blob_data, i_frame)
            frame_i_data = blob_data.get_data(blob_data.frame_start + i_frame + 1)
            # Shift particles in particle_list_0 forward by v_x
            for p in p_list_tmp:
                p.x_hat = p.get_frame_data(p.frame_start)['x'] + v_x[i_vel]
                p.y_hat = p.get_frame_data(p.frame_start)['y']
            J_tmp = compute_association_residual(p_list_tmp, frame_i_data, blob_data.variance)
            J_v[i_vel] += np.sum(J_tmp)
    vx_0 = v_x[J_v.argmin()] # Best estimate of initial velocity

    # # Plot to verify v_0 estimation; TODO: Show to user somehow?
    # plt.plot(v_x, J_v)
    # plt.xlabel('v0_x')
    # plt.ylabel('J_total')
    # plt.show()

    # As v_y is small compared to v_x, we can separately find a guess for v_y
    v_y = np.linspace(-30, 30, 21)
    J_vy = np.zeros(21)
    for i_vel in range(len(v_y)):
        for i_frame in range(N_FRAMES):
            p_list_tmp = initialize_particle_list(blob_data, i_frame)
            frame_i_data = blob_data.get_data(blob_data.frame_start + i_frame + 1)
            # Shift particles in particle_list_0 forward by v_x
            for p in p_list_tmp:
                p.x_hat = p.get_frame_data(p.frame_start)['x']
                p.y_hat = p.get_frame_data(p.frame_start)['y'] + v_y[i_vel]
            J_tmp = compute_association_residual(p_list_tmp, frame_i_data,
                    blob_data.variance)
            J_vy[i_vel] += np.sum(J_tmp)
    vy_0 = v_y[J_vy.argmin()]

    # # Plot to verify v_0 estimation
    # plt.plot(v_y, J_vy)
    # plt.xlabel('v0_y')
    # plt.ylabel('J_total')
    # plt.show()

    return (vx_0, vy_0)

# TODO: Delete, move docstring.
def compute_initial_velocity_estimate_old(blob_data):
    """
    Compute estimate of initial velocity by finding which velocity gives
    minimum value of association residuals.
    Uses Mahalanobis distance to compute the association residuals.
    """
    p_list_tmp = initialize_particle_list(blob_data, 0)
    frame_1_data = blob_data.get_data(blob_data.frame_start + 1)

    if G["flow_dir"] == "l2r":
        v_x = np.linspace(G["v0_min"], G["v0_max"], G["n_pts_v0"])
    elif G["flow_dir"] == "r2l":
        v_x = np.linspace(-G["v0_min"], -G["v0_max"], G["n_pts_v0"])
    J_v = np.zeros(G["n_pts_v0"])

    # Compare the number of points in each region for each frame, take
    # n_good_points as the sum of the lowest number of points in each region
    n_good_pts = 0
    frame_0_data = blob_data.get_data(blob_data.frame_start)
    for r in frame_0_data.region_list:
        assert(r in frame_1_data.region_list)
        n_good_pts += min(frame_0_data.get_data(r).n_particles,
                frame_1_data.get_data(r).n_particles)

    for i in range(len(v_x)):
        # Shift particles in particle_list_0 forward by v_x
        for p in p_list_tmp:
            p.x_hat = p.get_frame_data(p.frame_start)['x'] + v_x[i]
            p.y_hat = p.get_frame_data(p.frame_start)['y']

        # Compute array of association residuals
        J_tmp = compute_association_residual(p_list_tmp, frame_1_data,
                blob_data.variance)
        # Take the total association residual as the sum of the best
        # 'n_good_pts' to prevent missed particles from corrupting the estimate
        J_v[i] = np.sum(np.sort(J_tmp)[0:n_good_pts])

    vx_0 = v_x[J_v.argmin()] # Best estimate of initial velocity

    # # Plot to verify v_0 estimation
    # plt.plot(v_x, J_v)
    # plt.xlabel('v0_x')
    # plt.ylabel('J_total')
    # plt.show()

    # Since v_y is small compared to v_x, we can separately find a guess for v_y
    v_y = np.linspace(-30, 30, 21)
    J_vy = np.zeros(21)
    p_list_tmp = initialize_particle_list(blob_data, 0)
    for i in range(len(v_y)):
        for p in p_list_tmp:
            p.x_hat = p.get_frame_data(p.frame_start)['x']
            p.y_hat = p.get_frame_data(p.frame_start)['y'] + v_y[i]
        J_tmp = compute_association_residual(p_list_tmp, frame_1_data,
                blob_data.variance)
        J_vy[i] = np.sum(np.sort(J_tmp)[0:n_good_pts])
    vy_0 = v_y[J_vy.argmin()]

    # # Plot to verify v_0 estimation
    # plt.plot(v_y, J_vy)
    # plt.xlabel('v0_y')
    # plt.ylabel('J_total')
    # plt.show()

    return (vx_0, vy_0)

def compute_association_residual(particle_list, frame_data, variance):
    """
    For particles in 'particle_list' with estimated current frame positions
    (p.x_hat, p.y_hat), computes the value of association residual J using the
    Mahalanobis distance.

    x and y position data have different variances, so we set the association
    residual J as
        J = sqrt(M(x_hat - x)^2 + M(y_hat - y)^2)
    where M() denotes the Mahalanobis distance between two points.

    'variance' is a dictionary containing x and y variance for each region
    (req'd for Mahalanobis distance computation).
    """
    J = np.zeros(len(particle_list))
    # Compute association errors for all points
    for i, p in enumerate(particle_list):
        # Compute J using Mahalanobis distance for all points and take best result
        r_id = p.region_id
        r_data = frame_data.get_data(r_id) # Get data for particle region
        J_arr = np.zeros(r_data.n_particles)
        for j in range(r_data.n_particles):
            M_x = np.abs(p.x_hat - r_data.x_data(j)) / np.sqrt(variance[r_id]['x'])
            M_y = np.abs(p.y_hat - r_data.y_data(j)) / np.sqrt(variance[r_id]['y'])
            J_arr[j] = np.sqrt(M_x**2 + M_y**2)

        # Take the J value from the 'closest' point
        if r_data.n_particles == 0: J[i] = 0
        else:                       J[i] = J_arr.min()

    return J

def compute_particle_associations(particle_list, frame_data, variance):
    """
    For particles in 'particle_list' with estimated current frame positions
    (p.x_hat, p.y_hat), compute nearest recorded particles in 'frame_data'.
    """
    # Store what particles have been associated; TODO: Store this in frame_data?
    assoc_data_pts = dict.fromkeys(frame_data.region_list, [])

    # Compute association errors for all points
    for ip, p in enumerate(particle_list):
        if p.terminated_flag: continue # Exclude terminated particles

        r_id = p.region_id
        # r_id + 10 # This one is a string
        r_data = frame_data.get_data(r_id) # Get data for particle region
        # No particles, failed assocation by default
        if r_data.n_particles == 0:
            p_data = { 'x' : p.x_hat, 'y' : p.y_hat, 'J' : np.nan }
            p.add_point(frame_data.frame_id, p_data, np.nan)
            p.missed_frames += 1
            # If no successful matches for too long, terminate particle
            if p.missed_frames > G["max_missed_frames"]:
                p.terminate(frame_data.frame_id)
            continue
            # TODO: This is copy from down below

        # Compute J using Mahalanobis distance for all points then take best result
        J_arr = np.zeros(r_data.n_particles)
        for j in range(r_data.n_particles):
            M_x = np.abs(p.x_hat - r_data.x_data(j)) * np.sqrt(variance[r_id]['x'])
            M_y = np.abs(p.y_hat - r_data.y_data(j)) * np.sqrt(variance[r_id]['y'])
            J_arr[j] = np.sqrt(M_x**2 + M_y**2)

        # If there are no observed particles particles,
        # Take minimum J value that satisfies J_thresh and dy_max
        J_sort = copy.deepcopy(J_arr)
        J_sort = np.sort(J_sort)
        for J_i in J_sort:
            i = int(np.where(J_arr == J_i)[0]) # index of current value
            y_i = r_data.y_data(i)
            dy = abs(p.y_hat - y_i)

            # dy constraint satisfied, take this point; TODO: Hacky, remove
            if J_i <= G["J_thresh"] and dy <= G["dy_max"]:
                p.add_point(frame_data.frame_id,
                        r_data.get_particle_data(i), J_i)
                p.missed_frames = 0
                # Record index of associated particles
                assoc_data_pts[p.region_id].append(i)
                break

            # If J > J_thresh or dy >= dy_max, association failed;
            # take the propagated particle position as the true position and try
            # again next frame
            else:
                p_data = { 'x' : p.x_hat, 'y' : p.y_hat, 'J' : np.nan }
                p.add_point(frame_data.frame_id, p_data, np.nan)
                p.missed_frames += 1
                # If no successful matches for too long, terminate particle
                if p.missed_frames > G["max_missed_frames"]:
                    p.terminate(frame_data.frame_id)
                break

    return particle_list, assoc_data_pts

def mark_exiting_particles(particle_list, frame_data):
    """ Find what particles exited the test domain and terminate them. """
    for i, p in enumerate(particle_list):
        if not p.terminated_flag:
            if G["flow_dir"] == 'r2l':
                if p.x_hat < G["exit_x_dist"]:
                    # TODO: Verbosity flag
                    # print("Terminating particle {:d} at position ({:.2f}, {:.2f})"
                            # .format(i, p.x_hat, p.y_hat))
                    p.terminate(frame_data.frame_id - 1)
            elif G["flow_dir"] == 'l2r':
                if p.x_hat > G["exit_x_dist"]:
                    # print("Terminating particle {:d} at position ({:.2f}, {:.2f})"
                            # .format(i, p.x_hat, p.y_hat))
                    p.terminate(frame_data.frame_id - 1)

    return particle_list

def find_new_particles(frame_data, assoc_data_pts):
    """
    Add new data points from frame_data into new_particle_list.
    assoc_data_pts contains IDs of particles successfully associated.
    """
    new_particle_list = []
    for r_id in frame_data.region_list:
        r_data = frame_data.get_data(r_id)
        r_assoc_pts = assoc_data_pts[r_id] # associated pts this region

        for i in range(r_data.n_particles):
            if i not in r_assoc_pts:
                p_data = r_data.get_particle_data(i)
                new_particle_list.append(ParticleData(r_id, frame_data.frame_id, p_data))

    return new_particle_list

def estimate_new_particle_velocities(
        new_particle_list, next_frame_data, v_0, variance, frame_id=None):
    """
    Initial velocity estimation approach:
    For each new particle, sample possible space of initial velocities and find
    what v0 values give low association error.

    Currently not sure what the best treatment of vy_0 is.
    We can optimize for this too but it makes finding a good guess harder.
    Let's just try with vx initially and see how much success we have.
    Eventually, if we want to do this more thoroughly, we should probably use
    Haar cascades with EKF for velocity tracking anyway.
    """
    vx_0 = v_0[0]
    vy_0 = v_0[1]

    # Given that the std-devs for the axial velocities are between 5 and 12
    # [px/frame?], let's try a rough sweep from 60 to 140% of vx_0
    dv_max = vx_0 * 0.4
    n_pts = 30
    v_x = np.linspace(vx_0 - dv_max, vx_0 + dv_max, n_pts)
    J_arr = np.zeros(n_pts) # association residuals

    for p in new_particle_list:
        r_var = variance[p.region_id]
        # Assuming uniform random dist in x and y position, so
        # - covariance matrix is diagonal
        # - thus, Md = sqrt(Mx^2 + My^2)
        inv_cov = np.array([[1/r_var['x'], 0.0], [0.0, 1/r_var['y']]])
        next_r_data = next_frame_data.get_data(p.region_id)
        # If no new particles seen in this frame, terminate new particles
        if next_r_data.x_data().shape[0] == 0:
            p.terminate(frame_id)
            continue
        x_hat_arr = p.x + v_x
        y_hat_arr = p.y * np.ones(n_pts)
        Md = np.zeros(n_pts)

        # For each value in x_hat_arr, find the minimum Mahalanobis distance Md
        # (i.e. 'closest' point) -> take Md from this point
        for i, (x_hat_i, y_hat_i) in enumerate(zip(x_hat_arr, y_hat_arr)):
            Mx = np.abs(x_hat_i - next_r_data.x_data()) * np.sqrt(1/r_var['x'])
            My = np.abs(y_hat_i - next_r_data.y_data()) * np.sqrt(1/r_var['y'])
            Md[i] = np.min(np.sqrt(Mx**2 + My**2))

        # # Have a look to see if one velocity is a clear winner
        # plt.plot(v_x, Md)
        # plt.xlabel('v0_x')
        # plt.ylabel('Md')
        # plt.show()

        # Take best velocity value
        p.update_velocity_estimate(v_x[Md.argmin()], vy_0)

def plot_J(particle_list):
    # Each particle might have a different number of frames so we can't pre-size
    # the array
    J_all = np.array([])
    for p in particle_list: J_all = np.hstack((J_all, p.J_arr))
    J_all = np.sort(J_all)
    plt.plot(J_all)
    plt.ylabel("J (association residual)")
    plt.show()

def plot_spatial_trajectories(particle_list):
    """ Plot spatial trajectories for all particles. """
    plt.figure()
    for p in particle_list: plt.plot(p.x_arr)
    plt.xlabel("frame")
    plt.ylabel("x position (px)")

    plt.figure()
    for p in particle_list: plt.plot(p.y_arr)
    plt.xlabel("frame")
    plt.ylabel("y position (px)")

    # plt.legend()
    plt.show()

def plot_velocity_distribution(particle_list):
    """
    Compute x and y velocities of particles across the interrogation plane from
    particle positions either side of this plane.

    Potentially better velocity estimates could be obtained via a least-squares
    fit of a straight line through the ~6 data points nearest to the
    interrogation plane.
    """
    # Convert velocities to 'actual' values; TODO: repeated-ish
    s = 1 / (G["px_per_cm"] * 100 * G["img_scale"] * G["frame_rate"])

    # for i, p in enumerate(particle_list):
        # if len(p.data) < 3: continue
        # if p.get_frame_data(p.frame_end)['x'] - p.get_frame_data(p.frame_start)['x'] < 1000:
            # continue
        # print('start: ', p.get_frame_data(p.frame_start)['x'],
              # ' end: ',  p.get_frame_data(p.frame_end)['x'])

    vx_arr = np.zeros(len(particle_list))
    vy_arr = np.zeros(len(particle_list))
    for i, p in enumerate(particle_list):
        # Check that the particle has more than 3 succesful assocations
        if len(p.data) < G['max_missed_frames'] + 2: continue

        # Check that particle has crossed interrogation plane
        # Particles flow right to left
        if G["flow_dir"] == 'r2l':
            if (p.get_frame_data(p.frame_start)['x'] > G['int_x_dist'] and
                p.get_frame_data(p.frame_end)['x'] < G['int_x_dist']):
                # Find where particle crosses interrogation plane and compute
                # x-velocity
                for f_id in range(p.frame_end, p.frame_start, -1):
                        if p.get_frame_data(f_id)['x'] < G['int_x_dist']:
                            vx_arr[i] = s * abs(p.get_frame_data(f_id)['x']
                                    - p.get_frame_data(f_id - 1)['x'])
                            vy_arr[i] = s * abs(p.get_frame_data(f_id)['y']
                                    - p.get_frame_data(f_id - 1)['y'])
                            break
        # Particles flow left to right
        elif G["flow_dir"] == 'l2r':
            if (p.get_frame_data(p.frame_start)['x'] < G['int_x_dist'] and
                p.get_frame_data(p.frame_end)['x'] > G['int_x_dist']):
                # Find where particle crosses interrogation plane and compute
                # x-velocity
                for f_id in range(p.frame_end, p.frame_start, -1):
                    if p.get_frame_data(f_id)['x'] > G['int_x_dist']:
                        vx_arr[i] = s * abs(p.get_frame_data(f_id)['x']
                                - p.get_frame_data(f_id - 1)['x'])
                        vy_arr[i] = s * abs(p.get_frame_data(f_id)['y']
                                - p.get_frame_data(f_id - 1)['y'])
                        break

    # Colors
    lt_green_255 = (116, 196, 118)
    dk_green_255 = (0, 109, 44)
    highlight  = (198, 7, 115)
    lt_green   = tuple(c/255 for c in lt_green_255)
    dk_green   = tuple(c/255 for c in dk_green_255)
    highlight  = tuple(c/255 for c in highlight)

    # Make axes
    PHI = 1.61803398875
    scale = 4.0
    plt.figure(figsize=(PHI*scale, scale))

    # Filter out particles that didn't cross
    vy_arr = vy_arr[vx_arr != 0]
    vx_arr = vx_arr[vx_arr != 0]

    # Remove outliers (outside +- 2 std devs)
    mu, stdev = stats.norm.fit(vx_arr)
    vy_arr = vy_arr[vx_arr < mu + 2.0*stdev]
    vx_arr = vx_arr[vx_arr < mu + 2.0*stdev]
    vy_arr = vy_arr[vx_arr > mu - 2.0*stdev]
    vx_arr = vx_arr[vx_arr > mu - 2.0*stdev]
    n_particles = len(vx_arr)
    if n_particles > 99: n_bins = 9
    else:                n_bins = 7

    # # Plot histogram so bars sum to 1
    # y_lab = 'Probability'
    # weights = np.ones_like(vx_arr)/float(len(vx_arr)) 
    # plt.hist(vx_arr, bins=n_bins, color=lt_green, edgecolor='black', alpha=0.8,
            # density=False, stacked=True, weights=weights)

    # Plot probability density histogram
    y_lab = 'Probability density'
    plt.hist(vx_arr, bins=n_bins, color=lt_green, edgecolor='black', alpha=0.8,
            density=True, stacked=True)

    # Normal dist
    mu, stdev = stats.norm.fit(vx_arr) # update from trimmed data
    xmin, xmax = plt.xlim()
    interval = np.linspace(0.8 * xmin, 1.2 * xmax, 100)
    pdf = stats.norm.pdf(interval, mu, stdev)
    plt.plot(interval, pdf, color=highlight, linewidth=2.5)

    # # Chi-squared test
    # # Histograms for observed and estimated data
    # hist_obs = np.histogram(vx_arr, bins=n_bins)
    # f_obs = hist_obs[0]
    # nd = stats.norm(loc=mu, scale=stdev)
    # vx_arr_est = nd.rvs(size=sum(hist_obs[0])*1000)
    # hist_est = np.histogram(vx_arr_est, bins=n_bins)
    # f_exp = (hist_est[0]/1000).astype(int)
    # # Dodgy way to trim the outliers (won't always work)
    # while f_obs[1] == 0.0:
        # f_obs = np.hstack((f_obs[0] + f_obs[1], f_obs[2:]))
        # f_exp = np.hstack((f_exp[0] + f_exp[1], f_exp[2:]))
    # while f_obs[-2] == 0.0:
        # f_obs = np.hstack((f_obs[:-2], f_obs[-2] + f_obs[-1]))
        # f_exp = np.hstack((f_exp[:-2], f_exp[-2] + f_exp[-1]))
    # chisq, pval = stats.chisquare(f_obs, f_exp)

    # Shapiro-Wilk normality test
    hist_obs = np.histogram(vx_arr, bins=n_bins)
    ts, pval = stats.shapiro(hist_obs[0])
    moe = 2 * stdev / np.sqrt(n_particles)

    # Format and save/display
    plt.xlim((vx_arr.min() * 0.83, vx_arr.max() * 1.17))
    plt.ylabel(y_lab)
    plt.xlabel('Axial velocity [m/s]')
    plt.title(G["name"])
    ax = plt.gca()
    ax.yaxis.grid()
    plt.tight_layout()

    # Annotate
    stat_info = \
            'N = {}\n$\mu$ = {:.2f} m/s\n$\sigma$ = {:.3f} m/s\npval = {:.3f}\nMoE = {:.3f} %'.format(
            n_particles, mu, stdev, pval, moe)
    plt.text(0.05, 0.92, stat_info,
            horizontalalignment='left',
            verticalalignment='top',
            transform = ax.transAxes,
            bbox=dict(facecolor='white', edgecolor='black', pad=5.0))

    plt.savefig(G["name"] + ".pdf", format='pdf')
    # plt.show()

    muy, stdevy = stats.norm.fit(vy_arr)
    print("{} vx (mean, std-dev) = {:.2f}, {:.3f}  [m/s], vy (m, sd) = {:.2f}, {:.3f}  [m/s],  N = {}, pval = {:.3f}, MoE = {:.3f}%"
            .format(G["name"], mu, stdev, muy, stdevy, n_particles, pval, moe))

    return

def check_and_read_inputs(uo_dict):
    # Check that the job file actually exists then execute
    job_file_name = uo_dict.get("--job")
    if job_file_name not in os.listdir('.'):
        local_directory = os.path.dirname(os.path.realpath(__file__))
        raise CustomError("No job file {0} found in directory {1}"
                .format(job_file_name, local_directory))
    exec(open(job_file_name).read(), globals(), locals())

    # Check that the mandatory inputs have been provided
    if G["flow_dir"] not in ["l2r", "r2l"]:
        raise CustomError("Flow direction '{0}' not valid, code curently"
            + " supports flow directions 'l2r' and 'r2l'".format(G["flow_dir"]))

    reqd_inputs = ["exit_x_dist", "int_x_dist", "img_scale", "frame_rate",
            "px_per_cm", "v0_min", "v0_max"]
    for inp in reqd_inputs:
        if G[inp] is None:
            raise CustomError("Required input '{0}' not specified."
                    .format(inp))

    # Absolute velocities are used for v0
    if G["v0_min"] < 0.0 or G["v0_max"] < 0.0:
        raise CustomError("Absolute velocities are used for v0_min, v0_max"
            + " input (direction inferred from 'flow_dir'), but negative v0_min"
            + " or v0_max has been specified.")

def main(uo_dict):
    blob_data = load_data(uo_dict)
    particle_list = initialize_particle_list(blob_data)

    # TODO: put in function above
    for p in particle_list: p.estimate[str(blob_data.frame_start)] = {'x' : p.x, 'y' : p.y}

    # First step, estimate velocities then propagate all particles by v_mean
    frame_1_data = blob_data.get_data(blob_data.frame_start + 1)
    vx_0, vy_0 = compute_initial_velocity_estimate(blob_data)

    # Estimate first timestep velocities for all particles
    estimate_new_particle_velocities(
            particle_list, frame_1_data, (vx_0, vy_0), blob_data.variance)

    # Predict positions, associate, terminate exiting particles, init new particles
    for frame_id in range(blob_data.frame_start + 1, blob_data.frame_end + 1):
        for p in particle_list: p.propagate(frame_id)
        frame_i_data = blob_data.get_data(frame_id)
        particle_list = mark_exiting_particles(particle_list, frame_i_data)
        particle_list, assoc_data_pts = compute_particle_associations(
                particle_list, frame_i_data, blob_data.variance)
        for p in particle_list:
            p.estimate_velocity(frame_id)
            p.propagate_true_pos(frame_id)
        # TODO: wrap above functions into a routine

        # Find new (i.e. unassociated) particles and estimate their initial
        # velocities - assoc_data_pts contains IDs of associated particles
        # We don't need to do this part for the last frame
        if frame_id < blob_data.frame_end:
            new_particle_list = find_new_particles(frame_i_data, assoc_data_pts)
            for p in new_particle_list:
                p.estimate[str(frame_id)] = {'x' : p.x, 'y' : p.y}
            estimate_new_particle_velocities(new_particle_list,
                    blob_data.get_data(frame_id+1), (vx_0, vy_0),
                    blob_data.variance, frame_id)
            particle_list.extend(new_particle_list)

    # Set 'frame_end' for particles that have not crossed the exit line
    for p in particle_list:
        if not p.terminated_flag: p.terminate(blob_data.frame_end)

    # Stores positional data for plotting and convert to actual coordinates from
    # pixel coordinates
    for p in particle_list: p.form_results_arrays()

    plot_velocity_distribution(particle_list)
    # plot_spatial_trajectories(particle_list)
    # plot_J(particle_list)
    #---------------------------------------------------------------------------

short_options = ""
long_options = ["help", "job=", "data-file=", "out-file=", "plot", "verbosity="]

if __name__ == "__main__":
    user_options = getopt(sys.argv[1:], short_options, long_options)
    uo_dict = dict(user_options[0])

    if len(user_options[0]) == 0 or "--help" in uo_dict:
        # print_usage()
        sys.exit(1)

    else:
        try:
            check_and_read_inputs(uo_dict)
            main(uo_dict)
            # print("\n")
            # print("SUCCESS.")
            # print("\n")

        except CustomError as e:
            print("\nERROR: " + e.value)
            sys.exit(1)

