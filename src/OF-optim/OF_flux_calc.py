#! /usr/bin/env python3

"""
Boundary flux calculator for OpenFOAM.

This program can be called to evaluate the area intergarted properties or the
area integrated flux of properties across boundaries in OpenFOAM.

This Python file uses the following encoding: utf-8
----------------------------------------------------
Author: Jianhui Qi
        Ingo Jahn       i.jahn@uq.edu.au
Last Modified: 28/3/2019
"""

import os         as         os
import numpy      as         np
from   getopt     import getopt
import sys        as         sys 


def POSTPROCESSOR(Case_name,ROOT_DIR,verbosity):
    """Postprocessing functions."""
    # check the openfoam version
    # TODO: write the command to check the OpenFOAM version

    if verbosity > 1 :
        print("    Now strating the post processors")
  
    #Doing postprocess with processors of OpenFOAM
    CASE_DIR = ROOT_DIR+'/'+Case_name
    os.chdir(CASE_DIR)
    
    os.system("sonicFoam -postProcess -func MachNo -latestTime > MachNoLatestTime")
    #os.system("wallCellVelocity -latestTime")

    # reading the latest time value from the file
    f_latest = open('MachNoLatestTime', 'r')
    for line in f_latest:
        if 'Create mesh for time =' in line:
            timeString = line

    TimeList = timeString.replace('\n','').split(' ')
    latestTime = TimeList[5]
    return latestTime


def createScalarList(caseDirectory, timeDirectory, boundaryName, parameter, verbosity):
    """Create a list containing scalar values for every cell on face."""

    if parameter == 'P':  # change P to lower case to match filename
        parameter = 'p'

    if parameter == 'MA':
        parameter = 'Ma'

    os.chdir(os.path.join(caseDirectory, timeDirectory))
    if verbosity>1 :
        print("    The scalar extractor is working on directory {}".format(caseDirectory))

    scalarList = []
    addFlag = 0
    with open(parameter) as bcFile:
        for Line in bcFile:
            if ')' in Line:  # stop adding if ')' is encountered
                addFlag = 0

            if addFlag == 2:
                scalarList.append( float(Line.replace('\r','').replace('\n','')))

            if boundaryName in Line:  # get ready for adding
                addFlag = 1
            if addFlag > 0 and '(' in Line:  # start adding from '(' onwards
                addFlag = 2

    if verbosity > 2: 
        print("    The scalarList{}".format(scalarList))

    return scalarList


def createVectorList(caseDirectory, timeDirectory, boundaryName, parameter, verbosity):
    """Create a list containing scalar values for every cell on face."""


    os.chdir(os.path.join(caseDirectory, timeDirectory))
    if verbosity>1 :
        print("    The scalar extractor is working on directory {}".format(caseDirectory))

    vectorList = []
    addFlag = 0
    with open(parameter) as bcFile:
        for Line in bcFile:
            if ')' in Line:  # stop adding if ')' is encountered
                addFlag = 0

            if addFlag == 2:
                temp = Line.replace('(','').replace(')','').replace('\r','').replace('\n','').split(' ')
                vectorList.append([float(t) for t in temp])

            if boundaryName in Line:  # get ready for adding
                addFlag = 1
            if addFlag > 0 and '(' in Line:  # start adding from '(' onwards
                addFlag = 2

    if verbosity > 2: 
        print("    The vectorList{}".format(vectorList))

    return vectorList


def createAreaList(caseDirectory, boundaryName, verbosity):
    """Create a list containing area for every cell on face."""

    verbosity=3

    os.chdir(caseDirectory)
    if verbosity>1 :
        print("    The area caculator is working on directory {}".format(caseDirectory))

    boundary_file      = os.path.join(caseDirectory, 'constant', 'polyMesh', 'boundary')
    faces_file         = os.path.join(caseDirectory, 'constant', 'polyMesh', 'faces')
    points_file        = os.path.join(caseDirectory, 'constant', 'polyMesh', 'points')

    # open the boundary file and read the line number of nFaces and startFace
    addFlag = 0
    with open(boundary_file,'r') as f_boundary:
        line_count = 0
        for line in f_boundary:
            line_count +=1
            if boundaryName in line:
                addFlag = 2
            if addFlag > 0 and 'nFaces' in line:
                nFaces = int(line.replace('\r','').replace('\n','').replace(';','').split(' ')[-1])
                addFlag -= 1
            if addFlag > 0 and 'startFace' in line:
                startFace = int(line.replace('\r','').replace('\n','').replace(';','').split(' ')[-1])
                addFlag -= 1

    if verbosity >2 :
        print("    The number of faces is:{}".format(nFaces))
        print("    The index of start face is:{}".format(startFace))

    # open the faces file:
    faces_list = []
    with open(faces_file,'r') as f_faces:
        line_count = 0
        for line in f_faces:
            line_count += 1
            if line_count >= (startFace + 21) and line_count < (startFace + 21 + nFaces):
                temp = line.replace('\r','').replace('\n','').replace('(',' ').replace(')',' ').split(' ') 
                temp.remove('')
                del temp[0]
                faces_list.append(  temp )

    # Reading the point coordinates into a list
    pointsList = []
    with open(points_file,'r') as f_points:
        line_count = 0
        for line in f_points:
            line_count += 1
            if line_count == 19 :
                total_line = float(line.replace('\r','').replace('\n',''))
            if line_count >= 21 and line_count < (21 + total_line):
                temp = line.replace('\r','').replace('\n','').replace('(',' ').replace(')',' ').split(' ') 
                temp.remove('')
                temp.remove('')
                pointsList.append([float(t) for t in temp])

    # Use facelist to extract corresponding points coordinates
    face_coordinates_list = []
    for i in range(len(faces_list)):
        facet_coordinates = []
        for j in range(len(faces_list[i])):
            index_temp = int(faces_list[i][j])
            coordinates_temp = pointsList[index_temp]
            facet_coordinates.append(coordinates_temp)
        face_coordinates_list.append(facet_coordinates)

    # calculate area for each face.
    areaList = []
    for CornerList in face_coordinates_list:
        areaList.append(calcCellfaceArea(CornerList))

    if verbosity > 1: 
        print("    The areaList{}".format(areaList))

    return areaList


def calcCellfaceArea(CornerList):
    """
    Calculate the area of a cellface.

    faceCoordinateList is a list containing the corder point coordinates of a 
    polygon cell face. The coordinates should be provided in chronological order 
    going clockwise around the face, e.g.

        B--------C
        |        |
        A--------D

    Returns the area of the face.
    """
    N_corners = len(CornerList)


    if N_corners < 3:
        raise MyError("Less than 3 corners provided, cannot calculate face area.")
    elif N_corners == 3:
        #         B         
        #        /  \
        #       /     \
        #      A-------C
        a = np.array(CornerList[0])
        b = np.array(CornerList[1])
        c = np.array(CornerList[2])

        Area = 0.5* np.linalg.norm(np.cross((b-a), (c-a)))

        return Area
    else:
        # Subdivide polygon into triangles, then calcuate area 
        Centre_x = sum( [ c[0] for c in CornerList]) / N_corners
        Centre_y = sum( [ c[1] for c in CornerList]) / N_corners
        Centre_z = sum( [ c[2] for c in CornerList]) / N_corners
        Centre = np.array([Centre_x, Centre_y, Centre_z])

        Area = 0
        for n in range(N_corners-1):
            a = np.array(CornerList[n])
            b = np.array(CornerList[n+1])
            c = Centre
            Area += 0.5* np.linalg.norm(np.cross((b-a), (c-a)))

        a = np.array(CornerList[N_corners-1])  # -1 due tp python indexing
        b = np.array(CornerList[0])
        c = Centre
        Area += 0.5* np.linalg.norm(np.cross((b-a), (c-a)))

        return Area


def Area_Weighted_Average_Scalar(scalar_list,area_list):
    """Calculate area-weighted average of scalar field."""
    if len(scalar_list) != len(area_list):
        raise MyError("The length of scalar_list is not equal to the area_list")
    
    numerator = []
    for i in range(len(scalar_list)):
        temp = scalar_list[i]*area_list[i]
        numerator.append(temp )
    average = sum(numerator)/sum(area_list)

    return average


def main(uoDict):
    """Main function to be executed."""
    # create data class
    gdata = GDATA()

    # load extract data from uoDict
    gdata.verbosity = uoDict.get("--verbosity", 0)
    gdata.rootDir = uoDict.get("--rootDir", os.getcwd())  # set supplied root directory or get cwd
    gdata.caseName = uoDict.get("--caseName", "test")

    gdata.boundary = uoDict.get("--boundary", None)
    gdata.property = uoDict.get("--property", None)
    gdata.sumMode = uoDict.get("--sumMode", None)

    gdata.customSolverCommand = uoDict.get("--customCommand", None)

    if gdata.verbosity > 1:
        print("START: Running OF_flux_calc")
        print("       CaseName={0}; Boundary={1}; Property={2}; sumMode={3}".format(gdata.caseName, gdata.boundary, gdata.property, gdata.sumMode))

    # check that settings are supported.
    gdata.check()

    caseDirectory = os.path.join(gdata.rootDir, gdata.caseName)

    # run custom postprocessing command
    if not gdata.customSolverCommand is None:
        if gdata.verbosity > 1:
            print("       Running OF-postprocessor with command: {}".format(gdata.customSolverCommand))
        #Doing postprocess with processors of OpenFOAM
        os.chdir(caseDirectory)
        os.system(gdata.customSolverCommand)

    # calculate averaged flux
    if gdata.property == 'MDOT':
        """
        To compute mdot we use the functionality in controlDict to calculate mass flow rate on the fly.
        This allows mass flow rate to be averaged over the last N iterative steps.
        """
        averagingSteps = 100  # set number of iterative steps used for averaging.

        directory = os.path.join(gdata.rootDir, gdata.caseName, 'postProcessing',
                            'flowRatePatch(name={0})'.format(gdata.boundary))

        # latestRsd = findlateststep(directory)
        latestRsd = str(0)

        fileName = os.path.join(directory, latestRsd, 'surfaceFieldValue.dat')
        with open(fileName) as file:
            Lines = file.readlines()
            rows = len(Lines)
            lineID = 0
            sumflux = 0
            counter = 0
            for line in Lines:
                if lineID > rows - averagingSteps:
                    # extract relevant text from each line
                    text = str(line).split("  \t")
                    printtext = text[1].strip()

                    sumflux += float(printtext)
                    counter += 1
                lineID += 1

        mdotAverage = sumflux / counter  # calculare average mass flow rate
        Flux = mdotAverage

        if gdata.verbosity > 1:
            print("    Boundary: {0} --> mdot={1:.6f} kg/s".format(gdata.boundary, Flux))

    elif gdata.property == 'K' or gdata.property == 'T' or gdata.property == 'P' or gdata.property == 'MA':

        # get directory name for latest directory
        latestTimeDirectory = findLatestTimeDirectory(caseDirectory, gdata.verbosity)
        directory = os.path.join(caseDirectory, latestTimeDirectory)

        # calculate scalars
        scalarList = createScalarList(caseDirectory, latestTimeDirectory, gdata.boundary, gdata.property, gdata.verbosity)

        if gdata.sumMode == 'A':  # Compute flux using Averaging

            Flux = sum(scalarList) / len(scalarList)

        elif gdata.sumMode == 'AA':  # Compute flux using Area Averaging

            # calculate face areas
            areaList = createAreaList(caseDirectory, gdata.boundary, gdata.verbosity)

            totalArea = 0.
            FluxArea = 0.
            for scalar, area in zip(scalarList, areaList):
                FluxArea += scalar * area
                totalArea += area
            Flux = FluxArea / totalArea

        elif gdata.sumMode == 'FA':  # Compute flux using Area Averaging

            # calculate face fluxes
            fluxList = createScalarList(caseDirectory, latestTimeDirectory, gdata.boundary, 'phi', gdata.verbosity)

            fluxScalar = 0.
            totalFlux = 0.
            for flux, scalar in zip(fluxList, scalarList):
                fluxScalar += flux * scalar
                totalFlux += flux 
            Flux = fluxScalar / totalFlux

        else:
            raise MyError('Setting for {} not supported.'.format(gdata.sumMode))            

        if gdata.verbosity > 1:
            print("    Boundary: {0} --> {1}={2:.6f} [?]".format(gdata.boundary, gdata.property, Flux))

        return Flux

    else:
        raise MyError('gdata.property={} currently not supported for postprocessing.'.format(gdata.property))

    if gdata.verbosity > 2:
        print("    COMPLETE: OF_flux_calc")

    return Flux


def findLatestTimeDirectory(caseDirectory, verbosity=0):
    """Find last timestep at which a results file was written."""

    Time_string_list = []
    Time_float_list = []
    dir_names = os.listdir(caseDirectory)
    for i in range(len(dir_names)):
        try:
            temp =float(dir_names[i])
            Time_string_list.append(dir_names[i])
            Time_float_list.append(temp)
        except:
            pass

    sorted_time_index = np.argsort(Time_float_list, kind='quicksort')
    latestTime = Time_string_list[sorted_time_index[-1]]

    if verbosity > 0:
        print("    Latest timedirectory is {}.".format(latestTime))

    return latestTime


class GDATA():
    """class containing settings used for analysis."""

    def init(self):
        """Initialise class."""
        self.boundary = None
        self.property = None
        self.sumMode = None
        self.caseName = None
        self.rootDir = None
        self.verbosity = None

    def check(self):
        """Check that settings are correct."""
        self.property = self.property.upper()  # make all inputs upper case
        propertyList = ['P', 'T', 'MDOT', 'MA']
        if self.property not in propertyList:
            print(self.property)
            raise MyError("Setting for 'property'={0} is not supported. Use one of the following: {1}".format(self.property, propertyList))
        sumModeList = ['A', 'AA', 'FA']
        if self.sumMode not in sumModeList:
            raise MyError("Setting for 'sumMode'={0} is not supported. Use one of the following: {1}".format(self.sumMode, sumModeList))
        # TODO: Add check function that check boundaries and directories.


class MyError(Exception):
    """Class to capture exceptions."""

    def __init__(self, value):
        """Initialise class."""
        self.value = value

    def __str__(self):
        """String container."""
        return repr(self.value)


shortOptions = ""
longOptions = ["help", "case_name=", "verbosity=", 'root_dir=']


def printUsage():
    """Display usage of the code."""
    print("")
    print("Usage: Evaluate_means.py [--help] [--boundary=<boundaryName>] [--property=<propertyName>]")
    print("                         [--sumMode=<sumMode>] [--caseName=<CaseFileName>] [--rootDir=<path>")
    print("                         [--verbosity=<0,1,2>]")
    print("\n")
    print(" --help         Display help.")
    print(" --boundary=    Provide the label of the boundary that will be analysed. Needs to agree with")
    print("                label used in OpenFOAM mesh.")
    print(" --propery=     Provide quantitiy that will be analysed. Correspods to filennames as found in")
    print("                time directories of the OpenFOAM case. Currently the following are supported:")
    print("                p - pressure (Pa); T - temperature (K); mdot - mass flow rate (kg/s)")
    print(" --sumMode=     Define how the properties are summed.")
    print("                A - average of face values.")
    print("                AA - area weighted average of cell values.")
    print("                FA - flux weighted average of cell values.")
    print(" --caseName=    Directory for Case folder (absolute path)")
    print(" --rootDir=     Set root directory from which to work. If blank use cwd.")
    print(" --customCommand =  Custom command to perform postprocessing using the OF solver. For example:")
    print("                'sonicFoam -postProcess -func MachNo -latestTime > MachNoLatestTime'")
    print("                to calculate Mach No")
    print(" --verbosity=   Set level of screen output 0-none; 1-some; 2-all. (default = 0)")
    return


if __name__ == "__main__":

    userOptions = getopt(sys.argv[1:], shortOptions, longOptions)
    uoDict = dict(userOptions[0])

    if len(userOptions[0]) == 0 or "--help" in uoDict:
        printUsage()
        sys.exit(1)

    # execute the code
    try:
        main(uoDict)
        print("\n \n")
        print("SUCCESS.")
        print("\n \n")

    except MyError as e:
        print("This run has gone bad.")
        print(e.value)
        sys.exit(1)
