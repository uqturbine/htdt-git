"""
Optimiser setup file for running blast nozzle

The corresponding default OF case is provided in directory Case_Master

Author: Joshua Keep, Yuanshen Lu & Ingo Jahn & Jianhui Qi
Last Modified: 04/08/2018
"""


# Optimiser settings
M.method = 'Nelder-Mead'
M.maxiter = 100
M.Nvar = 4   # number of optimisation variables
M.NPost = 1
M.NCost = 1
M.cutoff_0 = 1e-3 #thresholds for interpolation (lower)
M.cutoff_1 = 10 #thresholds for interpolation (upper)
M.allow_interpolation = False
M.plot_results = False
M.Post_properties = ['S']   # mass flows [ mdot_o-00, mdot_i-01 ]
M.Post_boundaries = ['o-00']
M.Post_sumMode = ['FA']
M.Post_customCostFlag = False
M.Post_CustomCostFunction = '-(X[0]/X[1])**2.0'  # define custom equation to calculate cost. x=[Flux[0], Flux[1], ...], where flux is returned from OF_flux_calc.py
M.Post_targets = ['min']
M.Cost_weights = [1.]

M.Plot_progress = True
M.Save_solver_log = True
M.V_Eilmer = 4
M.interpolationType = 'interpolate'  # options are krigin and linear interpolation
#M.interpolationType = 'kriging'  # options are krigin and linear interpolation

# CFD case (or other piece of software) that is used for function evaluation. 
C.MasterCase_folder = 'Case_Master'
C.ChildCase_folder_base = 'Case_'
C.Input_filename = 'BN_3D.lua'
C.Keep_ChildCase = True
C.setup_command = 'run-mesh.sh'
C.simulate_command = 'run-solver_parallel.sh'
C.postprocess_command =  'run-post.sh'


# Data that used by optimiser and results
D.Use_initial_simplex = True   # if false, the simulation will restart based on data from D.filename
D.initial_simplex_filename = 'Inputs.txt'
D.write_data = True
D.filename = 'Results.txt'
 


