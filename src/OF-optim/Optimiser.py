#! /usr/bin/env python3
"""
Optimisation wrapper for OpenFOAM.

This code is an optimiser for use with OpenFOAM. It performs automated
optimisation of parametrically defined geometries using the Nelder-Mead simplex
approach. The optimisation process has the option to be surrogate assisted.

This Python file uses the following encoding: utf-8
----------------------------------------------------
Author: Jianhui Qi     j.qi@uq.edu.au
        Ingo Jahn      i.jahn@uq.edu.au
Time:     02-May-2018
The University of Queensland
"""

import os         as         os
import numpy      as         np
import shutil     as         sh
from   getopt            import getopt
import sys        as         sys 
from   scipy.optimize    import minimize
from   scipy.spatial     import distance
from   numpy.random      import random
from   scipy.interpolate import griddata
import scipy      as sci
from   scipy             import integrate
import matplotlib.pyplot as plt 
import OF_flux_calc as OF_flux_calc
from   numpy.linalg import inv
import time
from   scipy.spatial import Delaunay
from   scipy.spatial import ConvexHull


# add less common modules after checking availabilityself.
from importlib import util
pyKriging_spec = util.find_spec("pyKriging")
if pyKriging_spec is not None:
    import pyKriging as pyK
else:
    print("Module pyKriging not installed. Porceeding without support for Kriging.")


def load_data(M, C, D):
    """Fucntion to load all the data."""
    # Generate data base list:
    DATA_LIST = [] # List to store the whole information
    Index     = [] # List to store the index
    Status    = [] # List to store the Status
    Geometry  = [] # List to store the Geometry
    Post      = [] # List to store the results of post processing
    Cost      = [] # List to store the cost value
    Directory = [] # List to store the case directory
    Method    = [] # List to store the method for getting the data, interpolation or evaluation

    # print the location of the result file
    if M.verbosity > 1:
        print("    The Result file is located at :{}".format(M.ROOT_DIR+ '/' +D.filename))

    # check that file exists
    if os.path.isfile(M.ROOT_DIR+ '/' +D.filename):
        if M.verbosity > 1:
            print('    Loading Inital data from {}'.format(D.filename)) 


        # remove brackets and other signs, and append to DATA_LIST
        # That is becasuem in the Result.txt, the data is store as:
        # [1, 0, 0.5, 0.6 ..... ,"e"]\r\n
        # Thus we need to remove the brackets and the \r \n signs to obtain a data list.
        f_data = open(M.ROOT_DIR+ '/' +D.filename, 'r')
        for line in f_data:
            if line.startswith("["):
                temp = line.replace(" ", '').replace('[','').replace(']','').replace('\r','').replace('\n','').split(",")
                DATA_LIST.append(temp)     
        f_data.close()

        # Convert string data to floats   
        for i in range(len(DATA_LIST)):
            for j in range(2+M.Nvar+M.NPost+M.NCost):
                DATA_LIST[i][j] = float(DATA_LIST[i][j])
     
    # Add extra line corresponding to first line of simplex
    if D.Use_initial_simplex and D.Counter == 0:
        temp = np.append([0,0], D.initial_simplex[0])
        temp = np.append(temp, np.zeros(M.NPost+M.NCost+2))
        DATA_LIST.append(temp)

    #if M.verbosity > 1:
    #    print('DATA_LIST:', DATA_LIST) 

    for i in range(len(DATA_LIST)):
       
        Index.append(DATA_LIST[i][0])  # Reading the Index information
         
        Status.append(DATA_LIST[i][1])  #Reading the Status information
        
        Geometry.append(DATA_LIST[i][2:2+M.Nvar]) #Reading the Geometry information to the list
        
        Post.append(DATA_LIST[i][2+M.Nvar:2+M.Nvar+M.NPost]) # Reading the post processing information
        
        Cost.append(DATA_LIST[i][2+M.Nvar+M.NPost:2+M.Nvar+M.NPost+M.NCost]) # reading the Cost infromation
        
        Directory.append(DATA_LIST[i][2+M.Nvar+M.NPost+M.NCost+1-1])  # Reading the Deirectory information
        
        Method.append(DATA_LIST[i][2+M.Nvar+M.NPost+M.NCost+2-1]) # Reading the Method information

    if M.verbosity > 1:
        print('    Data Lists have been created.')

    if M.verbosity > 2:
        print('    Index    :', Index)  
        print('    Status   :', Status) 
        print('    Geometry :', Geometry) 
        print('    Post     :', Post) 
        print('    Cost     :', Cost) 
        print('    Directory:', Directory) 
        print('    Method   :', Method) 

    return DATA_LIST, Index, Status, Geometry, Post, Cost, Directory, Method


def EXECUTE(Geometry_list, Case_directory, neareast_DIR, M, C, D):
    """Execute the meshing routine and solver for current state vector."""
    # Initialiing the case directory
    if M.verbosity >1:
        print("    Creating new simulation case:")
    MASTER_DIR = M.ROOT_DIR + '/' + C.MasterCase_folder + '/'
    CASE_DIR = Case_directory
    os.chdir(M.ROOT_DIR)


    # output information: -------------------------
    if M.verbosity >1:
        print("    The Root directory is:{}.".format(M.ROOT_DIR))
        print("    The case folder is:{}.".format(CASE_DIR))
        print("    The basic setting is stroed in:{}.".format(MASTER_DIR))


    # Here we use a line of script to find if the calculation is done
    # If the calculation is done, the EXCUTE function do nothing.
    # Otherwise, the EXCUTE function starting to create the mesh, run the simulation
    if os.path.exists(Case_directory):
         print("    Case_directory {} already exist".format(Case_directory) )
    else:
        os.mkdir(Case_directory)
        os.system("cp -r "+ MASTER_DIR + ". " + Case_directory )
        if M.verbosity > 1:
            print('    Master_Case has been duplciated to {}.'.format(Case_directory))


        print("    Creating job file, the name of the job file is:{}".format(C.Input_filename))
        print("    The version of Eilmer is:{}".format(M.V_Eilmer))

    # [1]--------------------------------------------------
    #    The job file is been updated by the optimiser
    # -----------------------------------------------------
        # For Eilmer 3:
        # The job_file need to put under the /e3prep directory.
        if 3 == int(M.V_Eilmer):

            GeometryString = str(Geometry_list)
            job = open(Case_directory + '/e3prep/' + C.Input_filename ,'r')
            newjob = open(Case_directory + "/e3prep/newjob.py",'w')
            for line in job:
                if "OP = [" in line:
                    line = 'OP = ' + GeometryString + '\r\n'
                newjob.write("%s" % line)
            job.close()
            newjob.close()

            os.chdir( M.ROOT_DIR + '/' + CASE_DIR)
            print('    ' + M.ROOT_DIR + '/' + CASE_DIR)
            os.system('rm e3prep/'+ C.Input_filename)
            os.system('mv e3prep/newjob.py e3prep/'+ C.Input_filename)

        # For Eilmer 4:
        elif 4 == int(M.V_Eilmer):
            GeometryString = str(Geometry_list).replace('[','').replace(']','')
            print("    Geometry String current case: {}".format(GeometryString))
            job = open(Case_directory + '/' + C.Input_filename ,'r')
            newjob = open(Case_directory + "/newjob.lua",'w')
            for line in job:
                if "OP = {" in line:
                    line = 'OP = {' + GeometryString + '}\r\n'
                newjob.write("%s" % line)
            job.close()
            newjob.close()

            os.chdir(CASE_DIR)
            os.system('rm '+ C.Input_filename)
            os.system('mv newjob.lua '+ C.Input_filename)

        # Otherwise we raise up an error to ask the user check the setting of M.V_Eilmer
        else:
            raise MyError("!!!Please check if you set the version of Eilmer correctly")

    # [2]--------------------------------------------------
    #    Then operate the setup command and create the mesh
    # -----------------------------------------------------
        if M.verbosity >1:
            print('    START: create Mesh')
        os.system('./' + C.setup_command)
        if M.verbosity >1:
            print('      COMPLETE: Mesh creation')


    # [3]--------------------------------------------------
    #    Find the neareast directory and copy the file
    # -----------------------------------------------------
        if M.verbosity >1:
            print('    START: load initial solution')

        #TODO: automatic reading latest time
        #Need to put this section out the optimise
        if not(None == neareast_DIR):
            print("        Loading data from: {}".format(neareast_DIR))

            Time_string_list = []
            Time_float_list = []
            dir_names = os.listdir(neareast_DIR)
            for i in range(len(dir_names)):
                try:
                    temp =float(dir_names[i])
                    Time_string_list.append(dir_names[i])
                    Time_float_list.append(temp)
                except:
                    pass

            sorted_time_index = np.argsort(Time_float_list,kind='quicksort')
            latestTime = Time_string_list[ sorted_time_index[-1]  ]

            os.system("cp -r " + neareast_DIR +"/" + latestTime + "/. ./0/.")
            os.system("rm -r ./0/uniform")
            os.system("rm -r ./postProcessing")

        if M.verbosity >1:
            print('      COMPLETE: Load initial solution')

    # [4]--------------------------------------------------
    #    Run the simulation  
    # -----------------------------------------------------

        if M.verbosity >1:
            print('    START: run OpenFOAM Simulation')
        os.system('./' + C.simulate_command)
        if M.verbosity >1:
            print('      COMPLETE: OpenFOAM Simualtion')

    # [5]--------------------------------------------------
    #    Try to remove the saved log to save the memory 
    # -----------------------------------------------------
        if False == M.Save_solver_log:
            os.system('rm  log-solver')
            if M.verbosity >1:
                print('    Remove the solver log to reduce occupation of disk space')

        os.chdir(M.ROOT_DIR)
    return


def Cost_Eval(Post_list, M,C,D):
    """Evaluate the cost for the different outputs, based on targets."""
    # function to evaluate weighted costs base on optimisation target
    Cost_list = []
    for i in range(len(Post_list)):
        if M.Post_targets[i] == 'max':
            cost = - Post_list[i]**2
        elif M.Post_targets[i] == 'min':
            cost = Post_list[i]**2
        else: 
            cost = ((abs(M.Post_targets[i]) - abs(Post_list[i]))/M.Post_targets[i])**2
        Cost_list.append(cost)

    return Cost_list


def COMPARE_VECTOR(List0,List1,VT):
    """Calculate Mahalanobis distance between two state vectors."""
    # Use Mahala-Nobis method to evaluate the distance
    # To be remind that the two list shoud have the same length
    return  distance.mahalanobis(List0,List1,VT)


def Intergrand(t, mu, sigma_sqaure):
    """Calcualte cumulative distribution fucntion for Normal distribution."""
    # Cumulative distribution function for the normal distribution
    return np.exp(- (t - mu)**2. / (2.*sigma_sqaure) )/ np.sqrt(2.*np.pi*sigma_sqaure)


def in_hull(p, hull):
    """
    Test if points in `p` are inside the convex hull defined by `hull`.

    `p` should be a `NxK` coordinates of `N` points in `K` dimensions
    `hull` is either a scipy.spatial.Delaunay object or the `MxK` array of the
    coordinates of `M` points in `K`dimensions for which Delaunay triangulation
    will be computed
    """
    if not isinstance(hull,Delaunay):
        hull = Delaunay(hull)

    return hull.find_simplex(p)>=0


def Limitation(x,D):
    """Check if the current state vecotor exceeds the limits."""
    # This function we used to set up the limitation evaluation.
    lower   = D.limit[0]
    upper   = D.limit[1]
    outrange = 0

    for i in range(len(x)):
        if x[i] < lower[i]:
            print("    The {}th value is below the lower limit".format(i))
            print("    The value of the geometry is: {}".format(x[i]))
            print("    And the lower limit is: {}".format(lower[i]))
            outrange = 1
        if x[i] > upper[i]:
            print("    The {}th value is over the upper limit".format(i))
            print("    The value of the geometry is: {}".format(x[i]))
            print("    And the upper limit is: {}".format(upper[i]))
            outrange = 1

    return outrange


def Calculation(x,M,C,D):
    """
    Calculate the cost for the current state.

    This calculation function is called by the Optimiser to evaluate the cost
    associated with the state vector x.
    The calculation follows the following approachself.
    1. Check if the current state has been visited previously.
       If YES, reuse previous cost and proceed to end
    2. check if the current state exceeds the limits. If YES, go to step 9.
    3. Check if use of surrogate model is permitted. If NO, go to step 7.
    4. Evaluate distance to neighbouring states.
    5. Based on distance to nearest neighbout decide between surrogate model and new evaluation.
        if surrogate, go to step 6; if new evaluation, go to step 7.
    6. Use the surrogate model to evaluate the cost of the current stateself.
    7. Call the meshing and solver routine to do a new evaluation.
    8. Postprocess the solver solution to obtain a cost. Go to step 10.
    9. Apply penalty for out-of range case. Go to step 10.
    10. DONE
    """

    # set the iteration counter
    D.Counter = D.Counter+1

    if M.verbosity > 1:
        print("------------------------------------------------")
        print('NEW ITERATION LOOP')  
        print('  Iteration: {}'.format(D.Counter))
        print('  Current Geometry: '.format(x) )

    #Initialize the control flag to 'None'
    flag = 'None'
    

    if M.verbosity > 1:
        print("START: Loading existing data")
    # Load the data into memory
    DATA_LIST, Index, Status, Geometry, Post, Cost, Directory, Method  = load_data(M,C,D)
    if M.verbosity > 1:
        print("  COMPLETE: Loading existing data")

    #TODO: 在这个地方加上一个判断句，如果D.Counter的数值不等于Geometry的长度的话，就要考虑是否是从之前的datalist读取进来继续开始的运算

    # ###########################################################
    # 1. check if the current state has been visited previously.#
    #############################################################
    if M.verbosity > 1:
        print("START: Checking if point has been visited previously.")

    for i in range(np.shape(np.array(Geometry))[0]): 
        #print(np.array(Geometry)[i,:].tolist())
        #print(x.tolist())
        if np.array(Geometry)[i,:].tolist()==x.tolist() :
            print("    Current point is idemtical to {}.".format(Index[i])) 
            print("    --> Reuse")
            print("    Copying COST from {}".format(Index[i]))

            # copy cost data
            COST = 0
            # get line in Results file corresponding to this index
            Line = int(Index[i]) - 1  # -1 to account for python indexing
            for j in range(len(Cost[Line])):
                COST = COST + float( Cost[Line][j] ) * M.Cost_weights[j]
            flag = 'completed'


    if M.verbosity > 1:
        print("  COMPLETE: Checking if point has been visited previously.")


    #if M.verbosity > 1:
        #print("The Post is: {}".format(Post))
        #print("The Geometry is:{}".format(Geometry))
        #print("------------------------------------------------")

    # Mahalanobis norm Assessment
    # only need to do Mahalanobis Norm Assessment if Solutions exist that have been evaluated by CFD 

    # ###################################################
    # 2. check if the current state exceeds the limits. #
    # ###################################################
    if M.verbosity > 1:
        print("START: Check if the value is out of range")
    # Test if the geometry vector is within the limits:
    outrange = Limitation(x, D)
    if M.verbosity > 1:
        print("    The outrange is:{}".format(outrange))

    if 1 == outrange:
        # If the component of the vector is out-of-limits, then give a large cost value to the optimiser.
        # Then the optimiser know it is a bad evaluation to be skipped. 
        # Give the value of cost equal to one*weight. 
        flag = 'outrange'
        if M.verbosity > 1:
            print("    The value is out of range.")
    if M.verbosity > 1:
        print("  COMPLETE: Check if the value is out of range")


    # #########################################
    # 3. check if we can use surrogate model. #
    # #########################################

    if flag != 'completed' and flag != 'outrange': # Proceed to Mahalnobis norm evaluation 
        if M.verbosity > 1:
            print("START: Check if interpolation is permitted")


        if (M.allow_interpolation == False) or ( "'e'" in Method ) == False :  
            if M.verbosity >1:
                print("    Allowing interpolation is {}".format(M.allow_interpolation))
                print("    Evaluation cases in data base is {}".format('e' in Method))
                print("    --> Evaluation")

            # No previous data is available.
            flag = 'evaluation_new'
        else:
            if M.verbosity > 1:
                print("    Interpolation is permitted")
                print("    Starting to evaluate distance")

            #这一步将现在的Geometry与之前所有的进行比较，把得到的马式距离放到一个list里
            # ##############################################
            # 4. Evaluate distance to neighbouring states. #
            # ##############################################

            # Here we try to create the coviarant matrix.
            # Due to that the matrix should have a length large than the initial simplex, 
            # If we have enough geometry list, we use the whole geometry list as the initial arrary
            # Otherwise we use  the initial simplex as the initial array
            if len(Geometry) > len(Geometry[0]):
                initial_array = np.array(Geometry)
            else:
                initial_array = np.array(D.initial_simplex)


            # calculate the inverse coviariance matrix here
            inv_covariance = inv(np.cov(initial_array.T))#,bias = True ))


            # Here some lists are initilized.        
            dist_list_all  = [] # List to store the Mahalanobis distance for all cases
            dist_list_eval = [] # List to store the Mahalanobis distance for only evaluation cases
            index_all      = [] # List to store the index for all the geometry points, like [0, 1, 2, 3 ..]
            index_eval     = [] # List to store the index only for the evaluated geometry points, like [0, 1, 3, 5...]

            for i in range(len(Geometry)):
                # calculate the Mahalanobis distance between x and each geometry point
                dist_temp = COMPARE_VECTOR(x.tolist(),Geometry[i],inv_covariance)

                # appending the calculated distance
                dist_list_all.append( dist_temp )

                # appending the index
                index_all.append(i)

                # appending the evaluated geometry distance and index
                if Method[i] == "'e'":
                    dist_list_eval.append(dist_temp) 
                    index_eval.append(i)


            # Find the minimum distance for both list.
            min_dist_all = min(dist_list_all)  # minimum distance for all cases
            min_dist_eval= min(dist_list_eval) # minimum distance for only evaluated cases


            # Find the index of the geometry whose distance to x is minimum.
            # The geometries include interpolating ones.  
            min_index_all = dist_list_all.index(min(dist_list_all))
            min_method_all = Method[min_index_all]

            # Find the index of the geometry whose distance to x is minimum.
            # The geometries are only evaluated.
            temp_eval = dist_list_eval.index(min(dist_list_eval))
            min_index_eval = index_eval[temp_eval]
            #min_method_eval= Method[min_index_eval]


            if M.verbosity > 1:
                print('    The closest Geometry is: {}, with Mahalanobis Norm: {}'.format(min_index_all,min_dist_all))
                print('    The method for minimum value is:{}'.format(min_method_all))
                #print('')
                print('    Distance evaluation complete')

            # ##############################################################################################
            # 5. Based on distance to nearest neighbout decide between surrogate model and new evaluation. #
            # ##############################################################################################



            #DETERMINATION OF INTERPOLATION OR EVALUATION        
            #================================================================#
            #
            #    Use the same value                 cutoff_1
            #            v                             v
            #            |-|---------|-----------------|------------       
            #              *         ^                 
            #                     cutoff_0
            #   *: should add cutoff_s, denote the 'cutoff same', to use  the same cost
            #

            if min_dist_all == 0.0 :
                # point has been visited previously.
                # Calculate Cost for Optimiser
                COST = 0
                for i in range(len(Cost[min_index_all])):
                    COST = COST + float(Cost[min_index_all][i]) * M.Cost_weights[i]
                flag = 'completed'

                if M.verbosity > 1:
                    print('    Point visited previously')
                    print('    --> Reuse data.')
                raise MyError("Got to same point after mahalanobis norm. Shouldn't happen as check is earlier")       

            elif min_dist_eval < M.cutoff_0  :
                # point is very close to previously visited. Always interpolate. 
                flag = 'interpolation' 

                if M.verbosity > 1:
                    print('    Mahalanobis norm ={0:.4f} is less than M.cutoff ={1:.4f}'.format(min_dist_eval,M.cutoff_0))
                    print('     --> Interpolation')  

            elif min_dist_eval < M.cutoff_1: 
                # point is in proximite of previous evaluations. Toss coin.  
                if M.verbosity > 1:
                    print('    Mahalanobis norm ={0:.4f} is in proprotional range (M.cutoff_0 ={1:.4f}, M.cutoff_1 ={2:.4f})'.format(min_dist_eval,M.cutoff_0,M.cutoff_1))
                #print("minimum distance for evaluation is:", min_dist_eval)
                # TODO: Fix the CDF function.
                #这一步是让距离从0～0.01 整合为 0 ～0.1,以适应0～0.1的概率累计方程
                #a = min_dist_eval  * 10. 

                # cumulative distrubution function
                # we use the cumulative distrubution fuction for the normal distrubution.
                # possibility_criteria =  (integrate.quad(Intergrand, -np.inf, a , args = (0.085 , 0.00005) ) )[0]
                probability = 0.1 # currently 90% cases are doing interpolation.

                # THROW the COIN! -- to get a random number
                b = random()
                print("    The random number is {0:.4f}, the probability criteria is {1:.4f}".format(b,probability))

                if b >= probability:
                    if M.verbosity > 1:
                        print("    --> Interpolation")
                    flag = 'interpolation'
                else:
                    if M.verbosity > 1:
                        print("    --> Evaluation")
                    flag = 'evaluation'

                # If the length of geometry is less than the length of variable list, do the evaluation
                # That's avoid the error that the interpolation progress stopped due to not enough geometry points
                if len(Geometry) <= len(Geometry[0]):
                    if M.verbosity > 1:    
                        print("    Insufficient evaluate geometries to create an interpolation simplex.")
                        print("    --> Evaluation")
                    flag = 'evaluation'

            else:
                # point is far away. Always evaluate. 
                if M.verbosity > 1:
                    print('    Mahalanobis norm ={0:.4f} is greater than M.cutoff_1 ={1:.4f}'.format(min_dist_eval,M.cutoff_1))
                    print("    --> Evaluation")
                flag = 'evaluation'

            if M.verbosity > 1:
                print("  COMPLETE: Check if interpolation is permitted")


        #if M.allow_interpolation == False and 'interpolation' == flag:
        #    # overwrite interpolation by evaluation to ensure all functions are evaluated.  
        #    flag = 'evaluation'
        # this option should not be feasible. 

    ############################################################################
    # 6. Use the surrogate model to evaluate the cost of the current stateself.#
    # ##########################################################################
    # INTERPOLATION PROGRESS      
    #=======================================================================#   
    if 'interpolation' == flag:
        if M.verbosity > 1:
            print("START: Interpolation routine.")
            print("    Evaluation by Interpolation - Using: {0}".format(M.interpolationType))

        if M.interpolationType == 'interpolate':

            # create convex hull based on all geometry points. 
            if in_hull(x,Geometry) == False:
                print("    Point x={0} is outside convex hull".format(x))
                print("    --> Evaluation")
                flag ='evaluation'
            else:
                print("    Point x={0} is inside convex hull -> proceed".format(x))

                # Initializing some list.
                Geometry_New = [] # The new geometry list, which will contain the sorted geometry points.
             
                temp_var     = [] # The variable list.
            
                fluxList    = [] # The post processing results list.

                hull_index   = [] # The list to hold the index of the hull vertices


                # [1]--------------------------------------------------
                #    Sort the geometry points with Mahalanobis distance
                # -----------------------------------------------------

                # Sort the distance list which only by evaluation
                # Thus the sorted_index_eval is the index of dist_list_eval, not the whole Geometry list

                # sorted_index_eval 
                sorted_index_eval = np.argsort(dist_list_eval,kind='quicksort')
                  
                # initial the list to store the index of the evaluated geometry sorted by distance
                Eval_Geom_index_sorted_by_distance = []
                for i in range(len(dist_list_eval)):
                    Geometry_index =  index_eval[sorted_index_eval[i]]
                    Eval_Geom_index_sorted_by_distance.append(Geometry_index)

                #   Example for understanding the methdology:
                # 
                #   [0.2, 0.1, 0.7, 0.8, 0.4]  <-- dist_list_eval
                #   
                #   [ 0,   1,   2,   3,   4 ]  <-- index of dist_list_eval
                #
                #   [ 0,   1,   2,   5,   7 ]  <-- index_eval: list of the index of evaluated geometry
                #                                  (To be noted that 3,4,6 are obtained by interpolation)
                #
                #   [ 1,   0,   4,   2,   3 ]  <-- sorted_index_eval
                #
                #   [ 1,   0,   7,   2,   5 ]  <-- Eval_Geom_index_sorted_by_distance
                #    
                #   Thus we can use Eval_Geom_index_sorted_by_distance to find the corresponding geometry point.

                # [2]--------------------------------------------------
                #    Find the final minimum convex hull
                # -----------------------------------------------------
                
                # Initialize lists to store the final geometry and corresponding index.
                Final_geometry = []
                Final_index =[]

                """
                TODO: code to find optimum convex hull size by bi-section. 
                # do convex hull search by bisection
                # find level of bisection required
                i = 1
                N = 4
                while True:
                    if N**i > len(Eval_Geom_index_sorted_by_distance):
                        break
                    else:
                        i += 1
                N = i
                print("N",N)                

                i_search_start = len(Eval_Geom_index_sorted_by_distance)
                i_temp = np.ceil(i_search_start/2.)
                for i in range(N):
                    print("i",i)
                    print("i_temp",i_temp)
                    print(i_search_start)
                    Geo_temp = []
                    for j in range(int(i_temp)):
                        Geo_temp.append( Geometry[Eval_Geom_index_sorted_by_distance[j]])
                    # check if in hull
                    if in_hull(x,Geo_temp) == True:
                        if M.verbosity >1:
                            print("    {} points create a convex hull.".format(i_temp))
                        i_temp = i_temp - np.floor(i_search_start/(2.**(i+2)) ) 
                        if i_temp < np.shape(Geo_temp)[1]+1:
                            i_temp = i_temp + np.floor(i_search_start/(2.**(i+2)) )  
                            break                        
                    else:                        
                        if M.verbosity >1:
                            print("    {} points fail to create convex hull.".format(i_temp))
                        i_temp = i_temp + np.ceil(i_search_start/(2.**(i+2)) ) 

                i_temp = i_temp + 1
                print("Going actual work:",i_temp)
                for i in range(int(i_temp)):
                """

                for i in range(len(Eval_Geom_index_sorted_by_distance)):
                    Real_geo_index_temp = []
                    Geo_temp = []

                    # Create a new geometry list
                    for j in range(len(Eval_Geom_index_sorted_by_distance) - i):
                        Geo_temp.append( Geometry[Eval_Geom_index_sorted_by_distance[j]]   )
                        Real_geo_index_temp.append( Eval_Geom_index_sorted_by_distance[j])
                    Geo_temp = np.array(Geo_temp)
                        
                    # Use the try and excepted syntax to avoid the potential error of the Q_hull package. 
                    try:
                        hull_vertices = ConvexHull(Geo_temp).vertices
                        Real_geometry_index = []
                        for a in range(len(hull_vertices)):
                            Real_geometry_index.append(  Real_geo_index_temp[hull_vertices[a]] )
                           
                        # Appending 
                        hull_geometry = []            
                        for k in range(len(hull_vertices)):
                            hull_geometry.append( Geo_temp[hull_vertices[k]]  )
                        hull_geometry = np.array(hull_geometry)

                    except:
                        break

                    #print in_hull(x,hull_geometry  )          
                    if  in_hull(x,hull_geometry) == True:
                        if M.verbosity >1:
                            print("    Minimum convex hull with {} points identified".format(np.shape(hull_geometry) ) )
                        Final_geometry = hull_geometry
                        Final_index    = Real_geometry_index
                    else:
                        break

                Final_geometry = np.array(Final_geometry)


                # [3]--------------------------------------------------
                #    Carrying out the interpolation
                # -----------------------------------------------------

                if not  Final_geometry.any():
                    print("    Failed to find a suitable convex hull to cover the given geometry point in N-dimensional space")
                    print("    --> Evaluation")
                    flag = 'evaluation'
                else:
                    for i in range(len(M.Post_properties)):
                        temp = []
                        for j in range(len(Final_index)):
                            temp.append(Post[Final_index[j]][i])
                        temp_var.append(temp)
                            
                    temp_var     = np.array(temp_var)

                    for k in range(len(temp_var)):

                        Inter_class = sci.interpolate.LinearNDInterpolator(Final_geometry, temp_var[k],rescale=True)
                        Inter_value = (Inter_class(x)).tolist()[0]
                        fluxList.append( float(Inter_value) )
                          
                    # use custom function or default options to calculate total cost
                    if M.Post_customCostFlag is True:
                        # use user-defined equation 
                        X = fluxList
                        COST = eval(M.Post_CustomCostFunction)
                        Cost_list = fluxList

                    elif M.Post_customCostFlag is False:
                        # use standard defined functions on 'min', 'max' or target and combine this with specified weights. 
                        Cost_list = []
                        Cost_list = Cost_Eval(fluxList, M,C,D)

                        # apply weigths to respective costs and sum to get total cost
                        COST = 0
                        for i in range(len(Cost_list)):
                            COST = COST + Cost_list[i] * M.Cost_weights[i]

                    else:
                        raise MyError("Value specified for 'M.Post_customCostFlag'= {0} is not supported.")

                    if M.verbosity > 1:
                        print('    OF_flux_calc.py has been exexcuted on directory {}.'.format(C.ChildCase_folder_base+str(D.Counter)))
                    #    print('PfluxList: {}'.format(fluxList))

                    # If the interpolator try to do the extrapolation, which means the new geometry point lies outside of the convex hull
                    # Then switch the flag to 'evaluation'
                    if np.isnan( COST ):
                        print("TRY to extrapolation!!!!!! rerun the case.")
                        flag = 'evaluation'#'interpolation'

                    else:
                        if M.verbosity >1 :
                            print("The total cost is:{} and cost is, Ma: {}, p: {}".format(COST, Cost_list[0], Cost_list[1]))

                        write = [D.Counter] + [0] + x.tolist() + fluxList + Cost_list + [C.ChildCase_folder_base+str(D.Counter)] + ['i']
                        method = 'i'

                        f= open(M.ROOT_DIR+'/'+D.filename,'a+')
                        f.write("%s" % write + '\r\n')
                        f.close()

            if M.verbosity >1 : 
                print("    Complete interpolation - Using: {0}".format(M.interpolationType))

        elif M.interpolationType == 'kriging':
            # evaluate new case by kriging. 
            # compelted in 2 steps:
            # 1) check if inside convex hull
            # 2) perform kriging

            # 1) Check if in convex hull
            # Initialize lists to store the final geometry and corresponding index.

            # create convex hull based on all geometry points. 
            if in_hull(x,Geometry) == False:
                print("    Point x={0} is outside convex hull".format(x))
                print("    --> Evaluation")
                flag ='evaluation'
            else:
                print("    Point x={0} is inside convex hull -> proceed".format(x))

                # 2) Set up Krigin 

                # TODO: Adjust the code so that I keep track of what data has already been used to train the model.
                # i.e. only add new data points for re-training. 

                N_kriging = 0
                if not ('KrigingModel' in locals()): # if model doesn't exist, create individual models
                    KrigingModel = []

                    for i in range(len(M.Post_properties)):
                        print("HEREHERE")
                        print(np.shape(np.array(Geometry)) )
                        print(np.shape(np.array(Post)) )
                        
                        print("Creating Krigin Model for i={}.".format(i))
                        KrigingModel.append( pyK.kriging( np.array(Geometry), np.array(Post)[:,i] ) )

                        print("Training Krigin Model for i={}.".format(i))
                        KrigingModel[i].train()

                    N_kriging = D.Counter                        
                else:
                    for i in range(len(M.Post_properties)):
                        print("Adding data point to Krigin Model for i={}.".format(i))
                        KrigingModel[i].addPoint( np.array(Geometry)[D.counter-1,:], np.array(Post)[D.counter-1,i] ) 

                        print("Training Krigin Model for i={}.".format(i))
                        KrigingModel[i].train()

                    N_kriging = D.Counter  

                # now evaluat the kriging Model
                for i in range(len(M.Post_properties)):
                    print("Evaluating Krigin Model for i={}.".format(i))
                    Z_krigin = KrigingModel[i].predict(x)  
                    print("Value = {}".format(Z_krigin)) 
                    fluxList.append( Z_krigin )


                Cost_list = []
                Cost_list = Cost_Eval(fluxList, M, C, D  )

                # Calculate Cost for Optimiser
                COST = 0
                for i in range(len(Cost_list)):
                    COST = COST + Cost_list[i] * M.Cost_weights[i]

                if M.verbosity > 1:
                    print('    OF_flux_calc.py has been exexcuted on directory {}.'.format(C.ChildCase_folder_base+str(D.Counter)))
                #    print('Post_list: {}'.format(Post_list))

                # If the interpolator try to do the extrapolation, which means the new geometry point lies outside of the convex hull
                # Then switch the flag to 'evaluation'
                if np.isnan( COST ):
                    print("    TRY to extrapolation!!!!!! rerun the case.")
                    flag = 'evaluation'#'interpolation'

                else:
                    if M.verbosity >1 :
                        print("    The total cost is:{} and cost is, Ma: {}, p: {}".format(COST, Cost_list[0], Cost_list[1]))

                    write = [D.Counter] + [0] + x.tolist() + fluxList + Cost_list + [C.ChildCase_folder_base+str(D.Counter)] + ['i']
                    method = 'i'

                    f= open(M.ROOT_DIR+'/'+D.filename,'a+')
                    f.write("%s" % write + '\r\n')
                    f.close()

            if M.verbosity >1 : 
                print("    Complete interpolation - Using: {0}".format(M.interpolationType))


        else:
            if M.verbosity >1 : 
                print("    Selected interpolation Type {0} not supported.".format(M.interpolationType))
                print("    --> Evaluate.")
            flag = 'evaluation'

        if M.verbosity >1 : 
            print("  COMPLETE: Interpolation")

    # ################################################################
    # 7. Call the meshing and solver routine to do a new evaluation. #
    # ################################################################
    
    # =======================================================================
    # EVALUATION PROCESS
    # =======================================================================
    if 'evaluation' == flag or 'evaluation_new' == flag: 
        if M.verbosity > 1:
            print("START: Evaluation by Evaluation")

        if flag == 'evaluation' and C.Keep_ChildCase == True: # initiaise simualtion from nearest case

            neareast_DIR = M.ROOT_DIR+ '/' + str(Directory[min_index_eval]).replace('\'','').replace('\'','')
            #print neareast_DIR, min_index_eval
            EXECUTE(x.tolist(), C.ChildCase_folder_base+str(D.Counter) , neareast_DIR, M,C,D)

            

        elif flag == 'evaluation_new' or C.Keep_ChildCase == False: 
            # initialise from MasterCase
            #raise MyError('Need to Continue from here') 
            if M.verbosity >1:
                print("    Start a new evaluation...")

            EXECUTE(x.tolist(), C.ChildCase_folder_base+str(D.Counter) ,None,M,C,D)
        
            if M.verbosity >1:
                print("    Case: {} has been evaluated".format(C.ChildCase_folder_base+str(D.Counter)))

        else:
            raise MyError('setting for flag not supported')

        if M.verbosity > 1:
            print("  COMPLETE: Evaluation by Evaluation")

        # ######################################################
        # 8. Postprocess the solver solution to obtain a cost. #
        # ######################################################
        if M.verbosity > 1:
            print("START: Post-processing")

        # Execute postprocessing routine to evaluate conditions of current simualtion.
        fluxList = []
        for boundary, property, sumMode in zip(M.Post_boundaries, M.Post_properties, M.Post_sumMode):
            # define uoDict and then call main(uoDict) from OF_flux_calc.py to get average Flux
            uoDict = {'--boundary': boundary, '--property': property, '--sumMode': sumMode,
                      '--rootDir': M.ROOT_DIR, '--caseName': C.ChildCase_folder_base+str(D.Counter),
                      '--customCommand': M.Post_customSolverCommand, '--verbosity': M.verbosity}
            fluxList.append(OF_flux_calc.main(uoDict))

        if M.verbosity > 1:
            print('    OF_flux_calc.py has been exexcuted on directory {}.'.format(C.ChildCase_folder_base+str(D.Counter)))
            print('    fluxList: {}'.format(fluxList))

        # use custom function or default options to calculate total cost
        if M.Post_customCostFlag is True:
            # use user-defined equation 
            X = fluxList
            COST = eval(M.Post_CustomCostFunction)
            Cost_list = fluxList

        elif M.Post_customCostFlag is False:
            # use standard defined functions on 'min', 'max' or target and combine this with specified weights. 
            Cost_list = []
            Cost_list = Cost_Eval(fluxList, M,C,D)

            # apply weigths to respective costs and sum to get total cost
            COST = 0
            for i in range(len(Cost_list)):
                COST = COST + Cost_list[i] * M.Cost_weights[i]

        else:
            raise MyError("Value specified for 'M.Post_customCostFlag'= {0} is not supported.")

        # Updating the cost function
        if M.verbosity >1:
            print('    Cost for Case {} = {}'.format(C.ChildCase_folder_base+str(D.Counter),Cost_list))


        # writing Current Case to Results file         
        write = [D.Counter] + [0] + x.tolist() + fluxList + Cost_list + [C.ChildCase_folder_base+str(D.Counter)] + ['e']
        method = 'e'

        f= open(M.ROOT_DIR+'/'+D.filename,'a+')
        f.write("%s" % write + '\r\n')
        f.close()

        if M.verbosity >1:
            print('    New line is being added to file {}.'.format(D.filename))
            print('    ',write)

        if M.verbosity > 1:
            print("  COMPLETE: Post-processing")

        ###########################################
        # 9. Apply penalty for out-of range case. #
        ###########################################
        # =======================================================================#  
    elif 'outrange' == flag:

        # Calculate Cost for Optimiser
        fluxList = []
        Cost_list = []
    
        outRangeValue = 999.
        for i in range(len(M.Cost_weights)):
            Cost_list.append(outRangeValue)
            fluxList.append(outRangeValue)
        COST = outRangeValue * M.Cost_weights[i]
       

        # writing Current Case to Results file         
        write = [D.Counter] + [0] + x.tolist() + fluxList + Cost_list + [C.ChildCase_folder_base+str(D.Counter)] + ['o']
        method = 'o'

        f= open(M.ROOT_DIR+'/'+D.filename,'a+')
        f.write("%s" % write + '\r\n')
        f.close()

        # =======================================================================#
    elif 'completed' == flag:
        pass
    elif 'interpolation' == flag:
        pass
    else:
        raise MyError("you use wrong entry for the flag")



    D.Residual.append(COST)

    ###################################################################
    # plot optimisation progress
    if M.Plot_progress:
        if M.verbosity >1:
            print("START: Plotting output data")


        if flag == 'completed':
            print("    Plot the interactive figure to show the changing")
            plot_outcomes(M,C,D,Geometry, Post, Method, Cost)
            #time.sleep(0.01)
        else:
            Geometry.append(x.tolist())
            Post.append(fluxList)
            Method.append(method)
            Cost.append(Cost_list)
            print("    Plot the interactive figure to show the changing")
            plot_outcomes(M,C,D,Geometry, Post, Method, Cost)

        if M.verbosity >1:
            print("  COMPLETE: Plotting output data")

    ###################################################################


    if M.verbosity >1:
        print('+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++')
        print('Iteration for Case: {} complete.'.format(D.Counter))
        print('The Total Cost is: {}'.format(COST))
        print('+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++')
        print('\n')

    return COST


class Model:
    """Class containing Model settings."""

    def __init__(self):
        """Initialise class."""
        self.test = 0
        self.Post_boundaries = []
        self.Post_sumMode = []
        self.Post_customCostFlag = None
        self.verbosity = None
        self.Post_customSolverCommand = None

    def check(self):
        """Check that class defined correctly."""
        if not self.test == 0:
            raise MyError('M.testincorrect not specified')
        # add additional checks to assess that basics have been set-up correct;


class Case:
    """Class containing properties defining the case."""

    def __init__(self):
        """Initialise class."""
        self.MasterCase_folder = []
        self.ChildCase_folder_base = []
        self.Input_filename = []
        self.Keep_ChildCase = []
        self.setup_command = []
        self.simulate_command = []
        self.postprocess_command = []

    def check(self):
        """Check that class defined correctly."""
        if not self.MasterCase_folder:
            raise MyError('C.MasterCase_folder not specified')
        if not self.ChildCase_folder_base:
            raise MyError('C.ChildCase_folder_base not specified')
        if not self.Input_filename:
            raise MyError('C.Input_filename not specified')
        if not (self.Keep_ChildCase == True or self.Keep_ChildCase == False):
            raise MyError('C.Keep_ChildCase has to be True or False')
        if not self.setup_command:
            raise MyError('C.setup_command not specified')
        if not self.simulate_command:
            raise MyError('C.simulate_command not specified')
        if not self.postprocess_command:
            raise MyError('C.postprocess_command not specified')
    # TODO: need to add routines tho check that folders and files exist.

class Data:
    """Class containing Optimisation data and settings."""
    def __init__(self):
        """Initialise class."""
        self.Counter = 0
        self.Use_initial_simplex = []
        self.initial_simplex_filename = []
        self.initial_simplex = []
        self.write_data = []
        self.filename = []
        self.Residual  = []

    def check(self):
        """Check that class defined correctly."""
        if not (self.Use_initial_simplex == True or self.Use_initial_simplex == False):
            raise MyError('D.Use_initial_simplex has to be True or False')
        if not self.initial_simplex_filename:
            raise MyError('D.initial_simplex_filename not specified')
        if not (self.write_data == True or self.Use_inputarray == False):
            raise MyError('D.write_data has to be True or False')
        if not self.filename:
            raise MyError('C.filename not specified')

    # TODO: need to add routines tho check that folders and files exist.


def plot_outcomes(M,C,D,Geometry,Post, Method, Cost):
    """Plot progress from optimisation."""
    # Iteration is the x-axis for 2D plot
    Iteration = np.linspace(1,D.Counter,D.Counter) 

    # Residual is the total value of the cost function
    Residual  = D.Residual
   
    if M.verbosity >1 :
        print("    Starting to updating the figure...")
    
    # Here initialize some color list.
    color = ['k', 'y', 'b', 'g', 'r', 'c', 'm', 'k', 'y', 'b', 'g', 'r', 'c', 'm']
    #color = ['#7b241c', '#633974', '#1a5276', '#117864', '#1d8348', '#9a7d0a', '#873600' , '#b03a2e', '#6c3483', '#2874a6', '#229954', '#d4ac0d', '#ba4a00', '#3498db', '#f1c40f']
    line  = ['-', '--']

    # Clear the canvas to avoid overlapping lines. 
    D.ax1.cla()
    D.ax2.cla()
    D.ax3.cla()
    D.ax4.cla()

    # Axis 1 is used to plot the total cost value.
    D.ax1.plot(Iteration,Residual,'k-',markersize=9,markerfacecolor='w',markeredgewidth=1.5,linewidth=1.5,label = 'The obejective function')
    
    # Adding dots data:
    Eval_Iter=[]
    Eval_Dots=[]
    Inter_Iter=[]
    Inter_Dots=[]
    for i in range(len(Iteration)):
        if Method[i] == "'e'":
            Eval_Iter.append(Iteration[i])
            Eval_Dots.append(Residual[i])
        else:
            Inter_Iter.append(Iteration[i])
            Inter_Dots.append(Residual[i])

    D.ax1.plot(Eval_Iter,Eval_Dots,'ko',label="Evaluation data")
    D.ax1.plot(Inter_Iter,Inter_Dots,'ko',markerfacecolor='w',label="Interpolation data")

    # Need to uniform the value to the initial value.
    Uni_Geometry  = []
    Uni_Post      = []    
    # Intialize the weighted cost list
    Weighted_Cost = []
    
    # Create data for the inputs geometries
    for i in range(len(Geometry)):
        Uni_temp = []
        for j in range(len(Geometry[0])):
            Uni_temp.append( (Geometry[i][j] - Geometry[0][j]) / (  abs(Geometry[0][j]) + 1.0e-8  ) )# the SMALL value is added to avoid devided by zero error
        Uni_Geometry.append(Uni_temp)

    # Create data for the post function
    for i in range(len(Post)):
        Uni_temp = []
        for j in range(len(Post[0])):
            Uni_temp.append( (Post[i][j] - Post[0][j])/(  abs(Post[0][j]) + 1.0e-8))# the SMALL value is added to avoid devided by zero error
        Uni_Post.append(Uni_temp)

    Geometry = (np.asarray(Uni_Geometry[0:D.Counter])).T
    Post     = (np.asarray(Uni_Post[0:D.Counter])).T

    # Create data for the weighted cost 
    for i in range(len(Cost)):
        Wei_temp = []
        for j in range(len(Cost[0])):
            Wei_temp.append(Cost[i][j] * M.Cost_weights[j])
        Weighted_Cost.append(Wei_temp)
    Cost = (np.asarray(Weighted_Cost[0:D.Counter])).T
    
    
    # Plot the rest subfigures
    for i in range(len(Geometry)):
        if i < len(color):
            D.ax2.plot(Iteration,Geometry[i],color[i]+'-',linewidth=1.5,label = str(i))
        else:
            D.ax2.plot(Iteration,Geometry[i],color[i-7]+'--',linewidth=1.5,label = str(i))


    for j in range(len(Cost)):
         D.ax3.plot(Iteration, Cost[j], color[j]+'-',lw=1.5, label = M.Post_properties[j]+' Cost'  )

    for k in range(len(Post)):
         D.ax4.plot(Iteration,Post[k],color[k]+'-',lw=1.5,label = M.Post_properties[k]+ ' Post')


    # Do some settings for the plotting function
    D.ax1.set_ylabel("The total cost value ")
    D.ax1.legend(loc = 'best',numpoints=1)

    D.ax2.set_ylabel("The changing of the input geometries")
    D.ax2.legend(loc = 'best')

    D.ax3.set_ylabel("The value of weighted cost function")
    D.ax3.set_xlabel("Number of iterations [-]")
    D.ax3.legend(loc = 'best')

    D.ax4.set_ylabel("The changing of the post processing values")
    D.ax4.set_xlabel("Number of iterations [-]")
    D.ax4.legend(loc='best')

    plt.tight_layout()
    D.fig.canvas.draw()
    return 0


def main(uoDict):
    """
    main function
    """
    # create string to collect warning messages
    warn_str = "\n"

    # main file to be executed
    jobFileName = uoDict.get("--job", "test")

    # strip .py extension from jobName
    jobName = jobFileName.split('.')[0]

    # create classes to store input data
    M = Model() # data definig optimisation run.
    C = Case()  # data that defines CFD Case that is to be executed
    D = Data()  # Results from Optimisation process

    # set verbosity (can be overwritten from jobfile)
    M.verbosity = 1
    if "--verbosity" in uoDict:
        M.verbosity = int(uoDict.get("--verbosity", 1))

    # set ROOT 
    M.ROOT_DIR = os.getcwd()
    if M.verbosity > 1:
        print('M.ROOT_DIR set to: {}'.format(M.ROOT_DIR))  

    # Execute jobFile, this creates all the variables
    exec(open(M.ROOT_DIR+'/'+jobFileName).read(),globals(),locals())  

    # check that input data has been set correctly.
    M.check()
    C.check()
    D.check()    

    # create initial simplex to start optimiser   
    if D.Use_initial_simplex:
        # Load initialarray from file
        with open(M.ROOT_DIR+'/'+D.initial_simplex_filename) as f:
            read_data = f.read()
        if M.verbosity > 1:
            print('initial array loaded from: {}'.format(D.initial_simplex_filename))
        exec(read_data) 

    else: 
  
        # load the last Nvar+1 lines from the existing geometry map
        D.initial_simplex = np.array(Geometry[:(M.Nvar+1)])
        if M.verbosity > 1:
            print('initial_simplex taken from existing Geometry')

    if M.verbosity > 1:
        print('initial_simplex loaded:')
        print('  D.initial_simplex =', D.initial_simplex)


    # load_data 
    DATA_LIST, Index, Status, Geometry, Post, Cost, Directory, Method  = load_data(M,C,D)


    # set inital vector
    x0 = Geometry[-1]   # Should this be replaced by last non evaluated function

    if M.verbosity >1:
        print('OPTIMISATION STARTING ...')
    
    # Initialize the dynamic figure:
    plt.ion()
    D.fig = plt.figure(figsize=(20,10))
    D.ax1  = D.fig.add_subplot(221) # Figure to show the Residual
    D.ax2  = D.fig.add_subplot(222) # Figure to show the change of cost
    D.ax3  = D.fig.add_subplot(223) # Figure to show the change of inputs
    D.ax4  = D.fig.add_subplot(224) # Figure to show the weighted separted cost


    ############################################################
    # THE OPTIMISER!
    # Here we use the minimize module from scipy.py
    # The method is Nelder-Mead.
    # For a given case, the initial simplex need to be adjust 
    # from the input.txt file, which located in the example folder
    res =  minimize(Calculation,x0,args=(M,C,D),method='Nelder-Mead',options={'initial_simplex': D.initial_simplex, 'maxiter':M.maxiter})
    #
    #
    ############################################################

    # Once the optimiser finished, stop the calculation and show the final result on the screen.
    plt.show(block = True)

    print('OPTIMISATION COMPLETE')
    print('    Final Residual: {}'.format(res))
    return 0


class MyError(Exception):
    """Class to capture exceptions."""

    def __init__(self, value):
        """Initialise class."""
        self.value = value

    def __str__(self):
        """String container."""
        return repr(self.value)


shortOptions = ""
longOptions = ["help", "job=", "verbosity="]


def printUsage():
    """Display usage of the code."""
    print("")
    print("Usage: Optimizer.py [--help] [--job=<jobFileName>] [--verbosity=<0,1,2>]")
    print("\n")
    print(" --help      Display help.")
    print("\n")
    print(" --job=      Use this to specify the job file.")
    print("\n")
    print(" --verbosity   Set level of screen output 0-none; 1-some; 2-all.")
    return


if __name__ == "__main__":

    userOptions = getopt(sys.argv[1:], shortOptions, longOptions)
    uoDict = dict(userOptions[0])

    if len(userOptions[0]) == 0 or "--help" in uoDict:
        printUsage()
        sys.exit(1)

    # execute the code
    try:
        main(uoDict)
        print("\n \n")
        print("SUCCESS.")
        print("\n \n")

    except MyError as e:
        print("This run has gone bad.")
        print(e.value)
        sys.exit(1)
