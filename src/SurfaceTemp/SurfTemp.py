#! /usr/bin/env python3
"""
Solve transient 1-D heat equation with time varying boundary conditions.

Code to analyse the transient evolution of the temperature in a material
subjected to different heat flux or heat transfer boundary conditions at
both ends.
The code is based on the assumption that the thermal system is linear and
thus can be solved by linear superposition of effects. With this approach,
once the impulse response of the system has been established the transient
response can be determined through convolution. In discrete form this
results in:
    T = T(t); Q = Q(t); and I(t) is the impulse response evaluated at t.

    T(0)      I(0)   0   0   ... 0     Q(0)
    T(1)      I(1) I(0)  0   ... 0     Q(1)
    ...    =  I(2) I(1) I(0) ... 0  *  Q(2)
    ...       ...  ...  ...  ... 0     ...
    T(N)      I(N) ...  ...  ... 0     Q(N)

To establish the response where Q(t) is dependent on T(t) a zero order hold
is implemented. I.e. Q(t) is calculated based on T(t-1).

For cases where heat flux are prescribed at the send end, this is addressed
linear superposition T(t) = I_1 * Q_1(t) + I_2 * Q_2(t), where I_1 and I_2
are the impulse repsponses applied to ends 1 and 2 respectively.

Author: Ingo Jahn
Last Modified: 02/04/2019
"""

import datetime as datetime
import numpy as np
from scipy import special, integrate
import matplotlib.pyplot as plt
import shutil as shutil
import sys as sys
import os as os
from getopt import getopt
import pdb


class GDATA:
    """Class containing global simulation parameters."""

    def __init__(self):
        """Initialise class."""
        self.qUpdatedMode = 'firstOrderHold'  # approach taken to update
        self.scheme = 'EXPLICIT'  # Set scheme used foor solutuion. Options: 'IMPLICIT', 'EXPLICIT'
        self.method = 'IMPULSE'  # set method for how response is calculated. Options 'IMPULSE', 'UNITSTEP'
        self.subcycle = None  # Set number of subcycles. Set to None or 1 to switch off.
        self.geometry = 'flat'  # set of equations used Options: 'flat', 'cylindrical', 'spherical'
        self.verbosity = 0  # set level of outputs provided.
        self.tStart = 0  # [s] set starting time
        self.tEnd = 1  # [s] set end time
        self.steps = 100  # [-] number of steps used for discretising
        self.dtplot = 1.  # [s] time interval between plots
        self.graph = 'SINGLE'  # Set if output plotted in single or multiple figures. Options: 'SINGLE', 'MULTIPLE'
        self.stefanBoltzmanConstant = 5.670373e-8  # [W⋅m−2⋅K−4] Stefan Boltzman Constant

    def check(self):
        """Check class."""
        # check settings for self.graph
        self.graph = self.graph.upper()  # make all inputs upper case
        graphList = ['SINGLE', 'MULTI']
        if self.graph not in graphList:
            raise MyError("Setting for gdata.graph={0} is not supported. Use one of the following: {1}".format(self.graph, graphList))

        # check settings for self.subcyle
        # if self.subcycle is not None:
        #    raise MyError('Only self.subcycle=None supported at the moment.')
        if self.subcycle is None or self.subcycle == 0:
            self.subcycle = 1  # set to single subcyle


class WALL:
    """Class containing properties of wall being analysed."""

    def __init__(self):
        """Initialise class."""
        self.x0 = None  # [m] thickness of wall to be analysed
        self.x1 = None
        self.rho = None  # [kg / m3] density of wall material
        self.thermalConductivity = None  # [W /(m K)] thermal conductivity
        self.end0_HTC = None  # instance of HTC class to specify how heat transfer is modelled
        self.end1_HTC = None  # instance of HTC class to specify how heat transfer is modelled
        self.end0_BC = None  # boundary condition for end0
        self.end1_BC = None  # boundary condition for end0

    def check(self):
        """Check class."""
        pass


class HTC:
    """Class containing models for calculation of heat transfer."""

    def __init__(self):
        """Initialise class."""
        self.type = None  # select type for heat transfer. Options 'adiabatic', 'set_Q', 'set_T'
        self.time = None  # list containing times for each data point.
        self.value = None  # list containing corresponding to each time point
        self.radius = None  # [m] radius of leading edge
        self.Cp = None  # [J/kg.K] heat capacity at constant pressure
        self.phi = None  # [rad] incident angle for flow on flat plate
        self.delta = None  # [rad] leading edge sweep andgle
        self.x = None  # [m] distance from L.E. for flat plate calculations
        self.Qref = None  # [W/m^2] reference list of heat flux for comparison
        self.radiationFlag = False  # True/False switch if radiation modelling is includes
        self.radiationEmissivity = None  # [-] emissivity

    def check(self):
        """Check class."""
        self.type = self.type.upper()  # make all inputs upper case
        typeList = ['ADIABATIC', 'SET_T', 'SET_Q', 'EQUATION_STAGNATION', 'EQUATION_LAMINAR_FP', 'EQUATION_TURBULENT_FP', 'EQUATION_LEADING_EDGE']
        if self.type not in typeList:
            raise MyError("Setting for 'type'={0} is not supported. Use one of the following: {1}".format(self.type, typeList))

        if self.type == 'SET_Q':
            if self.value is None:
                raise MyError("For HTC.type={} value for HTC.SET_Q needs to be specified.".format(self.type))
            if len(self.time) != len(self.value):
                raise MyError("Length of HTC.SET_Q and HTC.time are different.")
        if self.type == 'SET_T':
            if self.value is None:
                raise MyError("For HTC.type={} value for HTC.SET_T needs to be specified.".format(self.type))
            if len(self.time) != len(self.value):
                raise MyError("Length of HTC.SET_T and HTC.time are different.")

        if self.type == 'EQUATION_STAGNATION':
            if self.radius is None or self.Cp is None:
                raise MyError("For HTC.type={0}, HTC.radius={1}, HTC.Cp={2} corresponding to stagnation point radius needs to be specified.".format(self.type, self.radius, self.Cp))
            if self.rhoInf is None or self.VInf is None or self.TInf is None:
                raise MyError("For HTC.type={}, not all input parameters have been defined. \n HTC.rhoInf, HTC.VInf, HTC.TInf have been defiend.".format(self.type))
            if len(self.time) != len(self.rhoInf) or len(self.time) != len(self.VInf) or len(self.time) != len(self.TInf):
                raise MyError("For HTC.type={}, length of inpur parameters do not match time vecypr. \n Check length of: HTC.rhoInf, HTC.VInf, HTC.TInf.".format(self.type))

        if self.type == 'EQUATION_LAMINAR_FP' or self.type == 'EQUATION_TURBULENT_FP':
            if self.phi is None or self.x is None or self.Cp is None:
                raise MyError("For HTC.type={0}, one of the variables HTC.phi={1}, HTC.x={2}, HTC.Cp={3} has not been defined.".format(self.type, self.phi, self.x, self.Cp))
            if self.rhoInf is None or self.VInf is None or self.TInf is None:
                raise MyError("For HTC.type={}, not all input parameters have been defined. \n HTC.rhoInf, HTC.VInf, HTC.TInf have been defiend.".format(self.type))
            if len(self.time) != len(self.rhoInf) or len(self.time) != len(self.VInf) or len(self.time) != len(self.TInf):
                raise MyError("For HTC.type={}, length of inpur parameters do not match time vecypr. \n Check length of: HTC.rhoInf, HTC.VInf, HTC.TInf.".format(self.type))

        if self.type == 'EQUATION_LEADING_EDGE':
            if self.phi is None or self.delta is None or self.x is None or self.Cp is None:
                raise MyError("For HTC.type={0}, one of the variables HTC.phi={1}, HTC.delta={2}, HTC.x={3}, HTC.Cp={4} has not been defined.".format(self.type, self.phi, self.delta, self.x, self.Cp))
            if self.rhoInf is None or self.VInf is None or self.TInf is None:
                raise MyError("For HTC.type={}, not all input parameters have been defined. \n HTC.rhoInf, HTC.VInf, HTC.TInf have been defiend.".format(self.type))
            if len(self.time) != len(self.rhoInf) or len(self.time) != len(self.VInf) or len(self.time) != len(self.TInf):
                raise MyError("For HTC.type={}, length of inpur parameters do not match time vecypr. \n Check length of: HTC.rhoInf, HTC.VInf, HTC.TInf.".format(self.type))

        if self.radiationFlag is True:
            if self.radiationEmissivity is None:
                raise MyError("When settinf HTC.radiationFlag={} you must also specify a valuse for self.radiationEmissivity".format(self.radiationFlag))
        # if self.type == 'ADIABATIC':
        #    self.value = np.zeros(len(self.time))
        #    self.type = 'SET_Q'

    def getValueAtTime(self, time, Twall=np.nan):
        """Interpolate self.time to get cooresponding value at that time."""
        if self.type == 'ADIABATIC':
            return 0.

        elif self.type == 'SET_Q':
            return np.interp(time, self.time, self.value)

        elif self.type == 'EQUATION_STAGNATION':
            radius = self.radius  # need to specifiy leading edge radius
            rho_inf = np.interp(time, self.time, self.rhoInf)
            V_inf = np.interp(time, self.time, self.VInf)
            T_inf = np.interp(time, self.time, self.TInf)
            h0 = self.Cp * T_inf + 1./2. * V_inf**2
            hw = self.Cp * Twall

            qStagnation = self.calcStagnationFlux(rho_inf, V_inf, radius, hw, h0)
            return -qStagnation  # -ve sign as applied to right hand side

        elif self.type == 'EQUATION_LAMINAR_FP':
            x = self.x  # need to define distanc that has allowed leading edge boundary layer to grow.
            phi = self.phi  # need to define local body angle with respect to free-stream
            rho_inf = np.interp(time, self.time, self.rhoInf)
            V_inf = np.interp(time, self.time, self.VInf)
            T_inf = np.interp(time, self.time, self.TInf)
            h0 = self.Cp * T_inf + 1./2. * V_inf**2
            hw = self.Cp * Twall
            qFP = self.calcLaminarFluxFP(rho_inf, V_inf, x, phi, hw, h0)
            return -qFP

        elif self.type == 'EQUATION_TURBULENT_FP':
            x = self.x  # need to define distanc that has allowed leading edge boundary layer to grow.
            phi = self.phi  # need to define local body angle with respect to free-stream
            rho_inf = np.interp(time, self.time, self.rhoInf)
            V_inf = np.interp(time, self.time, self.VInf)
            T_inf = np.interp(time, self.time, self.TInf)
            h0 = self.Cp * T_inf + 1./2. * V_inf**2
            hw = self.Cp * Twall
            qFP = self.calculateTurbulentFluxFP(rho_inf, V_inf, Twall, x, phi, hw, h0)
            return -qFP

        elif self.type == "EQUATION_LEADING_EDGE":
            phi = self.phi  # need to define local body angle with respect to free-stream
            delta = self.delta  # need to define leading edge sweep
            x = self.x  # need to define distanc that has allowed leading edge boundary layer to grow.
            rho_inf = np.interp(time, self.time, self.rhoInf)
            V_inf = np.interp(time, self.time, self.VInf)
            T_inf = np.interp(time, self.time, self.TInf)
            h0 = self.Cp * T_inf + 1./2. * V_inf**2
            hw = self.Cp * Twall

            # do leading edge correction as introduced in:
            # Michael E. Tauber and Gene P. Menees (1987),
            # Aerothermodynamics of Transatmospheric Vehicles,
            # Journal of Aircraft
            qStagnation = self.calcStagnationFlux(rho_inf, V_inf, self.radius, hw, h0)
            qFP = self.calculateTurbulentFluxFP(rho_inf, V_inf, Twall, x, phi, hw, h0)

            q = (0.5 * qStagnation**2 * np.cos(delta)**2 + qFP**2 * np.sin(delta)**2)**(1./2.)
            return -q  # -ve sign as applied to right hand side

        else:
            raise MyError("Selection for self.type={} is not supported.".format(self.type))

    def calcStagnationFlux(self, rho_inf, V_inf, radius, hw, h0):
        """Calculate Stagnation Point Heat Flux."""
        # calculate heatFlux using correlcation from
        # Michael E. Tauber and Gene P. Menees (1987),
        # Aerothermodynamics of Transatmospheric Vehicles,
        # Journal of Aircraft
        M = 3.
        N = 0.5
        C = 1.83e-8 * self.radius**(-1./2.) * (1. - hw/h0) * 100*100  # *100*100 to convert from W/cm^2 to W/m^2
        qw = rho_inf**N * V_inf**M * C
        return qw

    '''
    TODO: write code that automatically calculates the transition point.
    def calcFPFlux(self, rho_inf, V_inf, Twall, x, phi, hw, h0):
        """Calculate FlatPlace HeatFlux under consideration for Transition."""
        q = calcTurbulentFluxFP(rho_inf, V_inf, Twall, x, phi, hw, h0)
        return q
    '''

    def calcLaminarFluxFP(self, rho_inf, V_inf, x, phi, hw, h0):
        """Calculate Laminar Heat Flux for Flat Plate."""
        # calculate heatFlux using correlcation from
        # Michael E. Tauber and Gene P. Menees (1987),
        # Aerothermodynamics of Transatmospheric Vehicles,
        # Journal of Aircraft
        M = 3.2
        N = 0.5
        C = 2.53e-9 * np.cos(phi)**0.5 * np.sin(phi) * x**(-1./2.) \
            * (1. - hw/h0) * 100*100  # *100*100 to convert from W/cm^2 to W/m^2
        return rho_inf**N * V_inf**M * C

    def calculateTurbulentFluxFP(self, rho_inf, V_inf, Twall, x, phi, hw, h0):
        """Calculate Turbulen Heat Flux for Flat Plate."""
        # calculate heatFlux using correlcation from
        # Michael E. Tauber and Gene P. Menees (1987),
        # Aerothermodynamics of Transatmospheric Vehicles,
        # Journal of Aircraft
        if V_inf < 3962:
            M = 3.37
            N = 0.8
            C = 3.89e-8 * np.cos(phi)**1.78 * np.sin(phi)**1.6 * x**(-1./5.) \
                * (Twall/556.)**(-1./4.) * (1. - 1.11 * hw/h0) * 100*100  # *100*100 to convert from W/cm^2 to W/m^2
        else:
            M = 3.7
            N = 0.8
            C = 2.2e-9 * np.cos(phi)**2.08 * np.sin(phi)**1.6 * x**(-1./5.) \
                * (1. - 1.11 * hw/h0)  # *100*100 to convert from W/cm^2 to W/m^2

        qw = rho_inf**N * V_inf**M * C
        return qw


class BOUNDARY_CONDITION:
    """Class containing models for calculation of heat transfer."""

    def __init__(self):
        """Initialise class."""
        self.time = None  # [s] list containing times for each data point.
        self.heatFlux = None  # [W / m2] list containing heat flux corresponding to each time point
        self.temp = None  # [K] list containing fluid temperature corresponding to each time point
        # self.rho = None  # [kg / m3] list containing fluid density corresponding to each time point

    def check(self):
        """Check class."""
        pass

    def setConditions(self, HTC, timeList):
        """Set BC type and populate value lists."""
        self.type = HTC.type
        self.time = timeList
        if self.type == "SET_Q":
            self.heatFlux = [np.interp(t, HTC.time, HTC.value) for t in timeList]
        elif self.type == "ADIABATIC":
            self.heatFlux = np.zeros(len(timeList))
        elif self.type == "SET_T":
            self.temp = [np.interp(t, HTC.time, HTC.value) for t in timeList]
        elif self.type == "EQUATION_STAGNATION":
            pass
        elif self.type == "EQUATION_LEADING_EDGE":
            pass
        elif self.type == "EQUATION_LAMINAR_FP":
            pass
        elif self.type == "EQUATION_TURBULENT_FP":
            pass
        else:
            raise MyError("Value for self.type={0} not supported in BOUNDARY_CONDITIONS".format(self.type))


class RESPONSE:
    """Class that contains impulse response."""

    def __init__(self, type, gdata, wall, side):
        """Initialise class."""
        self.type = type.upper()  # [FLAT / CYLINDRICAL / SPHERICAL] set type of problem being solved
        self.gdata = gdata
        self.wall = wall
        self.side = side  # [left / right] set if the heatflux is applied to left or right end

        self.side.upper()
        if self.side not in ['LEFT', 'RIGHT']:
            raise MyError('Value specified for side={} in RESPONSE not supported. Needs to be LEFT/RIGHT'.format(self.side))

        # do some preparatory calculations
        self.kappa = self.wall.thermalConductivity / (self.wall.specificHeat * self.wall.density)  # set thermal diffusivity

        self.prepResponses()

    def prepResponses(self):
        """
        Prepare variable used by responses.

        The solution process is based on assumption that we have a problem wiht
        Neuman boundary conditions. I.e. heat flux is prescribed at one end and
        heat flux is zero at the other.
        """
        if self.type == 'FLAT':
            # Need to find addition function that fulfills heat equation and
            # boundary conditions.
            # Equation: u_t = u_xx
            # BCs: u_x(0) = q; u_x(1) = 0
            # m(x, t) = c_1 * x - 0.5 * c_1 * x^2 - c_1 * t
            # m_x = c_1 - c_1 * x   --> c_1 = q
            # m_xx = -c_1
            # m_t = -c_1
            # --> m (x, t) = q*x - 0.5*q*x^2 - q*t
            #
            # Create new equation with homogenous boundary conditions
            # v(x, t) = u(x, t) - m(x,t) = u(x, t) - q*x - 0.5*q*x^2 - q*t
            #
            # Now we can solve v(x, t) with homogenuous boundary conditions.
            pass
        elif self.type == 'CYLINDRICAL':
            # Need find function that fulfils heat equation boundary conditions in ploar coordinates
            # Equation: u_t * 1/kappa = 1/r * d/dr ( r * du/dr)
            # BCs: u_r(r1) = q1/k; u_r(r2) = 0
            # m(r, t) = A*r^2/4 + B* ln(r) + kappa * A * t
            #
            # Create new euation with homogenous boundary conditions
            # v(r, t) = u(r, t) - m(r, t)
            denom = -self.wall.x0 / (2.*self.wall.x1) + self.wall.x1 / (2. * self.wall.x0)
            if self.side == 'LEFT':
                self.cylindrical_A_coeff = (-1. / (self.wall.x1 * self.wall.thermalConductivity)) / denom  # *q_1 but will be done later
                self.cylindrical_B_coeff = (-1. * self.wall.x1 / (2.*self.wall.thermalConductivity)) / denom  # *q_1 but will be done later
            elif self.side == 'RIGHT':
                self.cylindrical_A_coeff = (1. / (self.wall.x0 * self.wall.thermalConductivity)) / denom  # *q_1 but will be done later
                self.cylindrical_B_coeff = (1. * self.wall.x0 / (2.*self.wall.thermalConductivity)) / denom  # *q_1 but will be done later
            else:
                raise MyError("Error has occurred.")

            # calculate values of m that result in roots
            self.cylindrical_mnList = self.cylindrical_find_bessel_roots()
            self.cylindrical_BnList = self.cylindrical_calculate_Bn()
            self.cylindrical_AnList = self.cylindrical_calculate_An()

        elif self.type == 'SPHERICAL':
            # see The Analytical Theory of Heat by Fourier for solution.
            # https://play.google.com/books/reader?id=No8IAAAAMAAJ&hl=en&pg=GBS.PA272
            # Equation: u_t * 1/kappa = 1/r^2 * d/dr ( r^2 * du/dr)
            # BCs: u_r(r1) = q1/k; u_r(r2) = 0
            # m(r, t) = A/r + B/6*r^2 + kappa * B * t
            #
            # Create new euation with homogenous boundary conditions
            # v(r, t) = u(r, t) - m(r, t)
            if self.side == 'LEFT':
                self.spherical_A_coeff = 1. / (self.wall.thermalConductivity * self.wall.x0) / (-1./(self.wall.x0**3) + 1./(self.wall.x1**3))
                self.spherical_B_coeff = self.wall.x0**2 * 3. / (self.wall.thermalConductivity * (self.wall.x0**3 - self.wall.x1**3))
            elif self.side == 'RIGHT':
                self.spherical_B_coeff = -self.wall.x1**2 * 3. / (self.wall.thermalConductivity * (self.wall.x0**3 - self.wall.x1**3))
                self.spherical_A_coeff = -1. / (self.wall.thermalConductivity * self.wall.x1) / (-1./(self.wall.x0**3) + 1./(self.wall.x1**3))
            else:
                raise MyError("Error has occurred.")

            # calculate coefficients
            self.spherical_lambdaList = self.spherical_find_sin_roots()
            self.spherical_BnList = self.spherical_calculate_Bn()
            self.spherical_AnList = self.spherical_calculate_An()

    def evaluateUnitStep(self, heatflux, x, t):
        """Evaluate response to unitStep with heatflux at postion x and time t."""
        if self.type == 'FLAT':
            # take x and t to nondimensional form
            L_star = (self.wall.x1 - self.wall.x0)
            x_hat = (x - self.wall.x0) / L_star
            T_star = (self.wall.x1 - self.wall.x0)**2 / self.kappa
            t_hat = t / T_star
            # calculate non dimensional temperature gradient
            temperatureGradient = heatflux / self.wall.thermalConductivity * L_star
            # series solution to heat equation
            if self.side == "LEFT":
                temperature = - (temperatureGradient * x_hat - 0.5*temperatureGradient * x_hat**2 - temperatureGradient*t_hat)
                temperature += (temperatureGradient*0.5 - 0.5*temperatureGradient * 1./3.)  # A_0 term from cosine Fourier series
                for i in range(self.gdata.N_coeff):
                    n = i+1  # first addition is for 1
                    # temperature += (2*heatflux * (-1.)**n / (n*np.pi)
                    #                + heatflux * 1./(n**3 * np.pi**3) * (n**2 * np.pi**2 * (-1)**n + 2 * (-1)**n * (-1))) * np.cos(n * np.pi * x_hat) \
                    #    * np.exp(- n**2 * np.pi**2 * t_hat)
                    if n % 2 == 0:  # consider even cases
                        temperature += (0
                                        - 2*temperatureGradient * 1./(n**2 * np.pi**2) * (-1)**n) \
                                        * np.cos(n * np.pi * x_hat) \
                                        * np.exp(- n**2 * np.pi**2 * t_hat)
                    else:
                        temperature += (2*temperatureGradient * (-2.) / (n**2 * np.pi**2)
                                        - 2*temperatureGradient * 1./(n**2 * np.pi**2) * (-1)**n) \
                                        * np.cos(n * np.pi * x_hat) \
                                        * np.exp(- n**2 * np.pi**2 * t_hat)
                return temperature

            elif self.side == "RIGHT":
                temperatureGradient *= -1
                temperature = - (temperatureGradient * (1-x_hat) - 0.5*temperatureGradient * (1-x_hat)**2 - temperatureGradient*t_hat)
                temperature += (temperatureGradient*0.5 - 0.5*temperatureGradient * 1./3.)  # A_0 term from cosine Fourier series
                for i in range(self.gdata.N_coeff):
                    n = i+1  # first addition is for 1
                    if n % 2 == 0:  # consider even cases
                        temperature += (0
                                        - 2*temperatureGradient * 1./(n**2 * np.pi**2) * (-1)**n) \
                                        * np.cos(n * np.pi * (1.-x_hat)) \
                                        * np.exp(- n**2 * np.pi**2 * t_hat)
                    else:
                        temperature += (2*temperatureGradient * (-2.) / (n**2 * np.pi**2)
                                        - 2*temperatureGradient * 1./(n**2 * np.pi**2) * (-1)**n) \
                                        * np.cos(n * np.pi * (1.-x_hat)) \
                                        * np.exp(- n**2 * np.pi**2 * t_hat)
                return temperature

            else:
                raise MyError("Error has occurred.")

        elif self.type == 'CYLINDRICAL':
            # for cylindrical case LEFT/RIGHT is addressed in prep function.
            temperatureGradient = -heatflux
            temperature = \
                + 0.25 * x**2 * self.cylindrical_A_coeff * temperatureGradient \
                - np.log(x) * self.cylindrical_B_coeff * temperatureGradient \
                + self.kappa * t * self.cylindrical_A_coeff * temperatureGradient
            # add series term
            for n in np.arange(self.gdata.N_coeff):
                mn = self.cylindrical_mnList[n]
                bn = self.cylindrical_BnList[n]
                An = self.cylindrical_AnList[n]
                temperature += temperatureGradient * An * (special.j0(mn*x) + bn * special.y0(mn*x)) * np.exp(-mn**2 * self.kappa * t)
            return temperature

        elif self.type == 'SPHERICAL':
            # for spherical case LEFT/RIGHT is addressed in prep function.
            temperatureGradient = -heatflux
            temperature = \
                + 1./x * self.spherical_A_coeff * temperatureGradient \
                + 1./6. * x**2 * self.spherical_B_coeff * temperatureGradient \
                + self.kappa * t * self.spherical_B_coeff * temperatureGradient
            # add series term
            for n in np.arange(self.gdata.N_coeff):
                if n == 0:
                    An = self.spherical_AnList[n]
                    temperature += temperatureGradient * An
                else:
                    lambd = self.spherical_lambdaList[n]
                    bn = self.spherical_BnList[n]
                    An = self.spherical_AnList[n]
                    temperature += temperatureGradient * An * (np.sin(lambd*x)/x + bn * np.cos(lambd*x)/x) * np.exp(-lambd**2 * self.kappa * t)

            #print('x', x, 't', t)
            #print(self.spherical_A_coeff, self.spherical_B_coeff, self.kappa)
            #print('Temp:', temperature)

            return temperature

        else:
            raise MyError("Erro has occured.")

    def evaluateImpulse(self, heatImpulse, x, t):
        """Evaluate response to Impulse with heatflux at postion x and time t."""
        if self.type == 'FLAT':
            # take x and t to nondimensional form
            L_star = (self.wall.x1 - self.wall.x0)
            x_hat = (x - self.wall.x0) / L_star
            T_star = (self.wall.x1 - self.wall.x0)**2 / self.kappa
            t_hat = t / T_star
            # calculate nondimensional temperature gradient
            temperatureGradient = heatImpulse / self.wall.thermalConductivity * L_star / T_star  # as time of impulse needs to be non-dimensionalised.
            # series solution to heat equation
            if self.side == "LEFT":
                temperature = 0.
                temperature += (temperatureGradient)
                for i in range(self.gdata.N_coeff):
                    n = i+1  # first addition is for 1
                    # temperature += (2*heatflux * (-1.)**n / (n*np.pi)
                    #                + heatflux * 1./(n**3 * np.pi**3) * (n**2 * np.pi**2 * (-1)**n + 2 * (-1)**n * (-1))) * np.cos(n * np.pi * x_hat) \
                    #    * np.exp(- n**2 * np.pi**2 * t_hat)
                    if n % 2 == 0:  # consider even cases
                        temperature += (0
                                        - 2*temperatureGradient * 1./(n**2 * np.pi**2) * (-1)**n) \
                                        * np.cos(n * np.pi * x_hat) \
                                        * np.exp(- n**2 * np.pi**2 * t_hat) * (- n**2 * np.pi**2)
                    else:
                        temperature += (2*temperatureGradient * (-2.) / (n**2 * np.pi**2)
                                        - 2*temperatureGradient * 1./(n**2 * np.pi**2) * (-1)**n) \
                                        * np.cos(n * np.pi * x_hat) \
                                        * np.exp(- n**2 * np.pi**2 * t_hat) * (- n**2 * np.pi**2)
                return temperature
            elif self.side == "RIGHT":
                temperatureGradient *= -1
                temperature = 0.
                temperature += (temperatureGradient)
                for i in range(self.gdata.N_coeff):
                    n = i+1  # first addition is for 1
                    if n % 2 == 0:  # consider even cases
                        temperature += (0
                                        - 2*temperatureGradient * 1./(n**2 * np.pi**2) * (-1)**n) \
                                        * np.cos(n * np.pi * (1.-x_hat)) \
                                        * np.exp(- n**2 * np.pi**2 * t_hat) * (- n**2 * np.pi**2)
                    else:
                        temperature += (2*temperatureGradient * (-2.) / (n**2 * np.pi**2)
                                        - 2*temperatureGradient * 1./(n**2 * np.pi**2) * (-1)**n) \
                                        * np.cos(n * np.pi * (1.-x_hat)) \
                                        * np.exp(- n**2 * np.pi**2 * t_hat) * (- n**2 * np.pi**2)
                return temperature
            else:
                raise MyError("Error has occurred.")
        elif self.type == 'CYLINDRICAL':
            # for cylindrical case LEFT/RIGHT is addressed in prep function.
            temperatureGradient = -heatImpulse
            temperature = \
                + self.kappa * self.cylindrical_A_coeff * temperatureGradient
            # add series term
            for n in np.arange(self.gdata.N_coeff):
                mn = self.cylindrical_mnList[n]
                bn = self.cylindrical_BnList[n]
                An = self.cylindrical_AnList[n]
                temperature += temperatureGradient * An * (special.j0(mn*x) + bn * special.y0(mn*x)) * np.exp(-mn**2 * self.kappa * t) * (-mn**2 * self.kappa)
            return temperature

        elif self.type == 'SPHERICAL':
            # for spherical case LEFT/RIGHT is addressed in prep function.
            temperatureGradient = -heatImpulse
            temperature = \
                + self.kappa * self.spherical_B_coeff * temperatureGradient
            # add series term
            for n in np.arange(self.gdata.N_coeff):
                if n == 0:
                    pass
                else:
                    Cn = self.spherical_CnList[n]
                    lambda_n = self.spherical_lambdaList[n]
                    temperature += temperatureGradient * Cn * np.sin(lambda_n * x) / x * np.exp(-lambda_n**2 * self.kappa * t) * (-lambda_n**2 * self.kappa)
            print('Temp:', temperature)

            return temperature

        else:
            raise MyError("Error has occurred.")

    def cylindrical_besselfunc(self, m):
        """Evaluate bessel function relation to find roots."""
        return special.j1(self.wall.x0*m)*special.y1(self.wall.x1*m) - special.j1(self.wall.x1*m)*special.y1(self.wall.x0*m)

    def cylindrical_find_bessel_roots(self):
        """Find first N roots of Bessel function."""
        counter = 0
        roots = []
        if self.cylindrical_besselfunc(0.) == 0.:
            roots.append(0.)
            counter += 1
            sign_start = np.sign(self.cylindrical_besselfunc(1e-6))
            x_low = 1e-6
        else:
            sign_start = np.sign(self.cylindrical_besselfunc(0.))
            x_low = 0.

        while counter < self.gdata.N_coeff:
            # find x_high after change in sign
            x = x_low
            while True:
                x = x+1
                if np.sign(self.cylindrical_besselfunc(x)) != sign_start:
                    x_high = x
                    break
            x_root = bisection(self.cylindrical_besselfunc, x_low, x_high)
            roots.append(x_root)
            counter += 1
            x_low = x_root + 1e-6
            sign_start = np.sign(self.cylindrical_besselfunc(x_low))

        if self.gdata.verbosity > 1:
            print("Bessel Function Roots:", roots)

        return roots

    def cylindrical_calculate_Bn(self):
        """Calculate Bn coefficients."""
        BnList = []
        for n in range(self.gdata.N_coeff):
            mn = self.cylindrical_mnList[n]
            BnList.append(-special.j1(mn*self.wall.x0) / special.y1(mn*self.wall.x0))
        return BnList

    def cylindrical_calculate_Vhat(self, r):
        """Calculate Vhat."""
        t = 0

        return - 0.25 * r**2 * self.cylindrical_A_coeff \
            + np.log(r) * self.cylindrical_B_coeff \
            - self.kappa * t * self.cylindrical_A_coeff

    def cylindrical_calculate_An(self):
        """Calculate An coefficients."""
        self.cylindrical_LnList = []
        for n in range(self.gdata.N_coeff):
            mn = self.cylindrical_mnList[n]
            bn = self.cylindrical_BnList[n]
            self.cylindrical_LnList.append(integrate.quad(lambda r: r * (special.j0(mn*r) + bn * special.y0(mn*r))**2, self.wall.x0, self.wall.x1)[0])

        AnList = []
        for n in range(self.gdata.N_coeff):
            mn = self.cylindrical_mnList[n]
            bn = self.cylindrical_BnList[n]
            Ln = self.cylindrical_LnList[n]
            AnList.append(integrate.quad(lambda r: r * (special.j0(mn*r) + bn * special.y0(mn*r)) * self.cylindrical_calculate_Vhat(r), self.wall.x0, self.wall.x1)[0] / Ln)

        return AnList

    def spherical_sinfunc(self, m):
        """Evaluate bessel function relation to find roots."""
        def F1(lambd, r):
            return -1./r * np.sin(lambd * r) + lambd * np.cos(lambd * r)

        def F2(lambd, r):
            return -1./r * np.cos(lambd * r) - lambd * np.sin(lambd * r)

        r1 = self.wall.x0
        r2 = self.wall.x1
        return F1(m, r1) * F2(m, r2) - F1(m, r2) * F2(m, r1)

    def spherical_find_sin_roots(self):
        """Find first N roots of Bessel function."""
        counter = 0
        roots = []
        if self.spherical_sinfunc(0.) == 0.:
            roots.append(0.)
            counter += 1
            sign_start = np.sign(self.spherical_sinfunc(1e-6))
            x_low = 1e-6
        else:
            sign_start = np.sign(self.spherical_sinfunc(0.))
            x_low = 0.

        while counter < self.gdata.N_coeff:
            # find x_high after change in sign
            x = x_low
            while True:
                x = x+1
                if np.sign(self.spherical_sinfunc(x)) != sign_start:
                    x_high = x
                    break
            x_root = bisection(self.spherical_sinfunc, x_low, x_high)
            roots.append(x_root)
            counter += 1
            x_low = x_root + 1e-6
            sign_start = np.sign(self.spherical_sinfunc(x_low))

        if self.gdata.verbosity > 1:
            print("Sin Function Roots:", roots)

        return roots

    def spherical_calculate_Bn(self):
        """Calculate Bn coefficients."""
        BnList = []

        def F1(lambd, r):
            return -1./r * np.sin(lambd * r) + lambd * np.cos(lambd * r)

        def F2(lambd, r):
            return -1./r * np.cos(lambd * r) - lambd * np.sin(lambd * r)

        r1 = self.wall.x0
        for n in range(self.gdata.N_coeff):
            lambd = self.spherical_lambdaList[n]
            BnList.append(-F1(lambd, r1) / F2(lambd, r1))
        return BnList

    def spherical_calculate_Vhat(self, r):
        """Calculate Vhat."""
        t = 0
        return - 1./r * self.spherical_A_coeff \
            - 1./6. * r**2 * self.spherical_B_coeff \
            - self.kappa * t * self.spherical_B_coeff

    def spherical_calculate_An(self):
        """Calculate Cn coefficients."""
        AnList = []
        # Calculate C_0
        denominator = integrate.quad(lambda r: r**2, self.wall.x0, self.wall.x1)[0]
        numerator = integrate.quad(lambda r: r**2 * self.spherical_calculate_Vhat(r), self.wall.x0, self.wall.x1)[0]
        AnList.append(numerator/denominator)

        # Calculate C_ns
        for n in range(self.gdata.N_coeff-1):
            lambd = self.spherical_lambdaList[n+1]  # as labda_0 = 0
            bn = self.spherical_BnList[n+1]
            denominator = integrate.quad(lambda r: r**2 * (np.sin(lambd*r)/r + bn * np.cos(lambd*r)/r)**2, self.wall.x0, self.wall.x1)[0]
            numerator = integrate.quad(lambda r: r**2 * (np.sin(lambd*r)/r + bn * np.cos(lambd*r)/r) * self.spherical_calculate_Vhat(r), self.wall.x0, self.wall.x1)[0]
            AnList.append(numerator/denominator)
        return AnList


def bisection(f, x_low, x_high, ftol=1e-9, xtol=1e-6):
    """Find root of f using bisection between limits x_low and x_high."""
    f_low = f(x_low)
    f_high = f(x_high)

    if np.sign(f_low) == np.sign(f_high):
        raise MyError("Error in bisection calcualtion. Both f(x_low) and f(x_high) have same sign.")

    counter = 0
    N_max = 1000
    while counter < N_max:
        x_mid = (x_low + x_high)/2  # find new mid point
        if abs(f(x_mid)) < ftol or (x_high-x_low)/2 < xtol:
            # stop if close to root by less than ftol or if gap between x_high and x_low is less than xtol
            return x_mid

        if np.sign(f(x_mid)) == np.sign(f(x_low)):
            x_low = x_mid
        else:
            x_high = x_mid
        counter += 1
    raise MyError("Bisection method has not converged.")
    return -1


def calculateTemperatureProfile_subcycle(gdata, i, temperatureCrossSectionStart, spaceList, time, timeList, deltaT_list, response_0, response_1, end0_HTC, end1_HTC, heatFlux_0, heatFlux_1, subcycles=1):
    """
    Calculate temperature profile.

    Has additional function to conduct sub-cycling with frozen input heatflux.
    """
    #print('Start', i, subcycles)
    #print(timeList)
    #print(time)
    #print(heatFlux_0)
    if i == 0:
        subcycles = 1
    else:
        #print('setting parameters')
        delta_T_subcycle = (timeList[i] - timeList[i-1])/subcycles
        time = timeList[i-1] + delta_T_subcycle
        timeList = np.append(timeList[0:i], [timeList[i-1] + (s+1)*delta_T_subcycle for s in range(subcycles)])
        #print(timeList)
        #print(time)
        heatFlux_0 = heatFlux_0[0:-1] + [heatFlux_0[-1]]*subcycles
        heatFlux_1 = heatFlux_1[0:-1] + [heatFlux_1[-1]]*subcycles
        #print(heatFlux_0)

    for s in range(subcycles):

        # initialise the temperature for current evaluation
        temperatureCrossSection = temperatureCrossSectionStart
        for n in range(len(temperatureCrossSectionStart)):  # iterate through the space list
            x = spaceList[n]
            # print('s', s)
            for j in range(i+1+s):  # iterate through the history up to now to get the results.
                if gdata.method == 'IMPULSE':
                    if j == 0:
                        pass
                    else:
                        temperatureCrossSection[n] += response_0.evaluateImpulse(heatFlux_0[j]*deltaT_list[j], x, time-timeList[j]+0.5*deltaT_list[j])
                        temperatureCrossSection[n] += response_1.evaluateImpulse(heatFlux_1[j]*deltaT_list[j], x, time-timeList[j]+0.5*deltaT_list[j])
                elif gdata.method == 'UNITSTEP':
                    if j == 0:
                        temperatureCrossSection[n] += response_0.evaluateUnitStep(heatFlux_0[j], x, time-timeList[j])  # start new unit step
                        temperatureCrossSection[n] += response_1.evaluateUnitStep(heatFlux_1[j], x, time-timeList[j])
                    elif j == i:   # last case is special as no subsraction
                        temperatureCrossSection[n] += response_0.evaluateUnitStep(heatFlux_0[j-1], x, time-timeList[j])
                        temperatureCrossSection[n] += response_1.evaluateUnitStep(heatFlux_1[j-1], x, time-timeList[j])
                    else:
                        temperatureCrossSection[n] -= response_0.evaluateUnitStep(heatFlux_0[j-1], x, time-timeList[j])  # finish unit step from previous interval
                        temperatureCrossSection[n] += response_0.evaluateUnitStep(heatFlux_0[j], x, time-timeList[j])  # start new unit step
                        temperatureCrossSection[n] -= response_1.evaluateUnitStep(heatFlux_1[j-1], x, time-timeList[j])  # finish unit step from previous interval
                        temperatureCrossSection[n] += response_1.evaluateUnitStep(heatFlux_1[j], x, time-timeList[j])  # start new unit step
                else:
                    raise MyError("Setting for gdata.method={} is not upported".format(gdata.method))

        if i != 0:
            # grow parameter lists for subcycling
            time = time + delta_T_subcycle
            # print("end of loop")
            # print('time', time)
            # print('timeList', timeList)
            # print('heatFlux_1', heatFlux_1)

    return temperatureCrossSection


def calculateTemperatureProfile(gdata, i, temperatureCrossSectionStart, spaceList, time, timeList, deltaT_list, response_0, response_1, end0_HTC, end1_HTC, heatFlux_0, heatFlux_1):
    """Function to calculate temperature profile."""

    # initialise the temperature for current evaluation
    temperatureCrossSection = temperatureCrossSectionStart
    for n in range(len(temperatureCrossSectionStart)):  # iterate through the space list
        x = spaceList[n]

        for j in range(i+1):  # iterate through the history up to now to get the results.
            if gdata.method == 'IMPULSE':
                if j == 0:
                    pass
                else:
                    temperatureCrossSection[n] += response_0.evaluateImpulse(heatFlux_0[j]*deltaT_list[j], x, time-timeList[j]+0.5*deltaT_list[j])
                    temperatureCrossSection[n] += response_1.evaluateImpulse(heatFlux_1[j]*deltaT_list[j], x, time-timeList[j]+0.5*deltaT_list[j])
            elif gdata.method == 'UNITSTEP':
                if j == 0:
                    temperatureCrossSection[n] += response_0.evaluateUnitStep(heatFlux_0[j], x, time-timeList[j])  # start new unit step
                    temperatureCrossSection[n] += response_1.evaluateUnitStep(heatFlux_1[j], x, time-timeList[j])
                elif j == i:   # last case is special as no subsraction
                    temperatureCrossSection[n] += response_0.evaluateUnitStep(heatFlux_0[j-1], x, time-timeList[j])
                    temperatureCrossSection[n] += response_1.evaluateUnitStep(heatFlux_1[j-1], x, time-timeList[j])
                else:
                    temperatureCrossSection[n] -= response_0.evaluateUnitStep(heatFlux_0[j-1], x, time-timeList[j])  # finish unit step from previous interval
                    temperatureCrossSection[n] += response_0.evaluateUnitStep(heatFlux_0[j], x, time-timeList[j])  # start new unit step
                    temperatureCrossSection[n] -= response_1.evaluateUnitStep(heatFlux_1[j-1], x, time-timeList[j])  # finish unit step from previous interval
                    temperatureCrossSection[n] += response_1.evaluateUnitStep(heatFlux_1[j], x, time-timeList[j])  # start new unit step
            else:
                raise MyError("Setting for gdata.method={} is not upported".format(gdata.method))

    return temperatureCrossSection


def main(uoDict):
    """Main function call."""

    # main file to be executed
    jobFileName = uoDict.get("--job", "test")

    # Make SSCAR run even if .py extension is forgotten
    if ".py" not in jobFileName:
        jobName = jobFileName
        jobFileName = ''.join([jobFileName, ".py"])
    else:
        # strip .py extension from jobName
        jobName = jobFileName.replace('.py', '')

    # Make sure that the desired job file actually exists
    if jobFileName not in os.listdir('.'):
        local_directory = os.path.dirname(os.path.realpath(__file__))
        raise MyError("No job file {0} found in directory: {1}".format(jobFileName, local_directory))

    # get in-file and out-file name
    out_file = uoDict.get("--out-file", "none")

    # set verbosity (can be overwritten from jobfile)
    verbosity = float(uoDict.get("--verbosity", 0))

    # initialise classes
    gdata = GDATA()
    wall = WALL()
    end0_HTC = HTC()
    end1_HTC = HTC()
    end0_BC = BOUNDARY_CONDITION()
    end1_BC = BOUNDARY_CONDITION()

    # Execute jobFile, this creates all the variables
    exec(open(jobFileName).read(), globals(), locals())

    # update print flag
    if verbosity > gdata.verbosity:
        gdata.verbosity = verbosity

    if gdata.verbosity >= 1:
        print("Input data file read")

    # check classes are correctly initialised
    gdata.check()
    end0_HTC.check()
    end1_HTC.check()
    end0_BC.check()
    end1_BC.check()
    wall.end0_HTC = end0_HTC
    wall.end1_HTC = end1_HTC
    wall.end0_BC = end0_BC
    wall.end1_BC = end1_BC
    wall.check()

    if gdata.verbosity >= 1:
        print("Classes defined and checked.")

    # create time lists
    timeList = np.linspace(gdata.tStart, gdata.tEnd, gdata.timeSteps+1)
    deltaT_list = [timeList[i+1] - timeList[i] for i in range(len(timeList)-1)]
    deltaT_list.insert(0, 0.)
    spaceList = np.linspace(wall.x0, wall.x1, gdata.spaceSteps)

    # create boundary condition lists
    # Do interpolation of raw data.
    wall.end0_BC.setConditions(wall.end0_HTC, timeList)
    wall.end1_BC.setConditions(wall.end1_HTC, timeList)

    # set up the responses for the left and right end
    response_0 = RESPONSE(gdata.geometry, gdata, wall, 'LEFT')
    response_1 = RESPONSE(gdata.geometry, gdata, wall, 'RIGHT')

    # initialise heatFlux list that will be popolated as the simualtion progresses.
    heatFlux_0 = []
    heatFlux_1 = []
    radiationFlux_0 = []
    radiationFlux_1 = []
    temperatureResults = []
    timeResults = []
    time_write = gdata.dtplot

    temperatureResults.append(np.zeros((gdata.spaceSteps)) + wall.temperatureStart)
    timeResults.append(0.)
    if gdata.verbosity > 0:
        print('Writing Data')

    print("++++++++++++++++++++++")
    print("Starting time-marching")
    for i in range(gdata.timeSteps+1):
        time = timeList[i]
        if gdata.verbosity > 0:
            if np.remainder(i, 20) == 0:
                print("Step  Time  dt")
            print("{0:3}   {1:.6f}  {2:0.6f}".format(i, time, deltaT_list[i]))

        if gdata.scheme == 'IMPLICIT':
            # apply iterative process to find wall temperature at end of current step (more stable)
            if i == 0:
                Twall_end0_old = wall.temperatureStart
                Twall_end1_old = wall.temperatureStart
                Twall_end0 = wall.temperatureStart + 0.1
                Twall_end1 = wall.temperatureStart + 0.1
            else:
                Twall_end0_old = temperatureCrossSection_old[0]
                Twall_end1_old = temperatureCrossSection_old[-1]
                Twall_end0 = temperatureCrossSection_old[0] + 0.1
                Twall_end1 = temperatureCrossSection_old[-1] + 0.1

            # assemble the list of actual heat transfer and correct for radiation
            if end0_HTC.radiationFlag is True:
                heatFlux_0_temp = heatFlux_0 + [end0_HTC.getValueAtTime(timeList[i], Twall_end0_old) - gdata.stefanBoltzmanConstant * end0_HTC.radiationEmissivity * Twall_end0_old**4]
            else:
                heatFlux_0_temp = heatFlux_0 + [end0_HTC.getValueAtTime(timeList[i], Twall_end0_old)]
            if end1_HTC.radiationFlag is True:
                heatFlux_1_temp = heatFlux_1 + [end1_HTC.getValueAtTime(timeList[i], Twall_end0_old) + gdata.stefanBoltzmanConstant * end1_HTC.radiationEmissivity * Twall_end1_old**4]
            else:
                heatFlux_1_temp = heatFlux_1 + [end1_HTC.getValueAtTime(timeList[i], Twall_end0_old)]

            # calculate first estimate of Twall
            temperatureCrossSectionStart = np.array([0., 0.]) + wall.temperatureStart
            # solved the temperature Profile
            temperatureCrossSection = calculateTemperatureProfile(gdata, i, temperatureCrossSectionStart, [spaceList[0], spaceList[-1]], time,
                                                                  timeList, deltaT_list, response_0, response_1,
                                                                  end0_HTC, end1_HTC, heatFlux_0_temp, heatFlux_1_temp)
            F0_old = temperatureCrossSection[0] - Twall_end0_old
            F1_old = temperatureCrossSection[-1] - Twall_end1_old
            #print("HERE:", F0_old, F1_old)

            counter = 0
            while True:
                counter += 1
                # assemble the list of actual heat transfer and correct for radiation
                if end0_HTC.radiationFlag is True:
                    radiationFlux_0_temp = - gdata.stefanBoltzmanConstant * end0_HTC.radiationEmissivity * Twall_end0**4
                    heatFlux_0_temp = heatFlux_0 + [end0_HTC.getValueAtTime(timeList[i], Twall_end0) + radiationFlux_0_temp]
                else:
                    radiationFlux_0_temp = 0
                    heatFlux_0_temp = heatFlux_0 + [end0_HTC.getValueAtTime(timeList[i], Twall_end0)]
                if end1_HTC.radiationFlag is True:
                    radiationFlux_1_temp = gdata.stefanBoltzmanConstant * end1_HTC.radiationEmissivity * Twall_end1**4
                    heatFlux_1_temp = heatFlux_1 + [end1_HTC.getValueAtTime(timeList[i], Twall_end0) + radiationFlux_1_temp]
                else:
                    radiationFlux_1_temp = 0
                    heatFlux_1_temp = heatFlux_1 + [end1_HTC.getValueAtTime(timeList[i], Twall_end0)]

                #print("HERE2", radiationFlux_1_temp, heatFlux_1_temp)

                # calculate first estimate of Twall
                temperatureCrossSectionStart = np.array([0., 0.]) + wall.temperatureStart
                # solved the temperature Profile
                temperatureCrossSection = calculateTemperatureProfile(gdata, i, temperatureCrossSectionStart, [spaceList[0], spaceList[-1]], time,
                                                                      timeList, deltaT_list, response_0, response_1,
                                                                      end0_HTC, end1_HTC, heatFlux_0_temp, heatFlux_1_temp)
                F0 = temperatureCrossSection[0] - Twall_end0
                F1 = temperatureCrossSection[-1] - Twall_end1

                #print("HERE3", F0, F0_old)
                #print("HERE3", F1, F1_old)

                # perform root finding using the secant method
                Twall_end0_new = (Twall_end0_old * F0 - Twall_end0 * F0_old) \
                    / (F0 - F0_old)
                Twall_end1_new = (Twall_end1_old * F1 - Twall_end1 * F1_old) \
                    / (F1 - F1_old)
                # problem is that for certain starting points the secant method finds a locale minima that is not a root. 

                if gdata.verbosity > 1:
                    print("end0", Twall_end0, Twall_end0_new, F0)
                    print("end1", Twall_end1, Twall_end1_new, F1)

                # stop iterating if threshold has been reached.
                if max([abs(F0), abs(F1)]) < 5e-3:
                    if gdata.verbosity > 1:
                        print('Temperature iteration converged after {} iterations.'.format(counter))
                    break
                # stop iteration if max steps reached
                if counter > 20:
                    print("F0, F1:", F0, F1)
                    print('Temperature iteration not converged after {} iterations.'.format(counter))
                    break

                # update variables
                F0_old = F0
                F1_old = F1
                Twall_end0 = Twall_end0_new
                Twall_end1 = Twall_end1_new
                Twall_end0_old = Twall_end0
                Twall_end1_old = Twall_end1

            heatFlux_0 = heatFlux_0_temp
            heatFlux_1 = heatFlux_1_temp
            radiationFlux_0.append(radiationFlux_0_temp)
            radiationFlux_1.append(radiationFlux_1_temp)

            if gdata.verbosity > 1:
                print("Temperature Before:", Twall_end0_old, Twall_end1_old)
                print("Temperature After :", Twall_end0, Twall_end1)

        elif gdata.scheme == 'EXPLICIT':
            # calculate the surface  temperatures at both ends based on previous step
            if i == 0:
                Twall_end0 = wall.temperatureStart
                Twall_end1 = wall.temperatureStart
            else:
                Twall_end0 = temperatureCrossSection_old[0]
                Twall_end1 = temperatureCrossSection_old[-1]
            # assemble the list of actual heat transfer and correct for radiation
            if end0_HTC.radiationFlag is True:
                radiationFlux_0_temp = - gdata.stefanBoltzmanConstant * end0_HTC.radiationEmissivity * Twall_end0**4
                heatFlux_0.append(end0_HTC.getValueAtTime(timeList[i], Twall_end0) + radiationFlux_0_temp)
                radiationFlux_0.append(radiationFlux_0_temp)
            else:
                heatFlux_0.append(end0_HTC.getValueAtTime(timeList[i], Twall_end0))
            if end1_HTC.radiationFlag is True:
                radiationFlux_1_temp = gdata.stefanBoltzmanConstant * end1_HTC.radiationEmissivity * Twall_end1**4
                heatFlux_1.append(end1_HTC.getValueAtTime(timeList[i], Twall_end1) + radiationFlux_1_temp)
                radiationFlux_1.append(radiationFlux_1_temp)
            else:
                heatFlux_1.append(end1_HTC.getValueAtTime(timeList[i], Twall_end1))
        else:
            raise MyError('Setting for gdata.scheme={} not supported.'.format(gdata.scheme))

        # calculate the temperature profile for current step
        # initialise the temperature for current evaluation
        temperatureCrossSectionStart = np.zeros((gdata.spaceSteps)) + wall.temperatureStart
        # solved the temperature Profile
        temperatureCrossSection = calculateTemperatureProfile(gdata, i, temperatureCrossSectionStart, spaceList, time,
                                                              timeList, deltaT_list, response_0, response_1,
                                                              end0_HTC, end1_HTC, heatFlux_0, heatFlux_1)
        # create old temperature for next loop
        temperatureCrossSection_old = temperatureCrossSection

        # write data to results list
        if i == gdata.timeSteps or time > time_write:
            temperatureResults.append(temperatureCrossSection)
            timeResults.append(time)
            time_write = time + gdata.dtplot
            if gdata.verbosity > 0:
                print('Writing Data')

    if gdata.verbosity > 0:
        print("Completed Time Marching")

    if out_file is not "none":  # write data to file.
        if out_file is not "none":
            if gdata.verbosity > 0:
                print("\n")
                print("CREATING OUTPUT FILE")

            if os.path.isfile(out_file):
                print("Output file already exists.")
                fntemp = out_file+".old"
                shutil.copyfile(out_file, fntemp)
                print("Existing file has been copied to:", fntemp)

            with open(out_file, 'w') as fp:
                fp.write("++++++++++++++++++++++++++++++\n")
                fp.write("Output File from SurfTemp.py\n")
                fp.write("++++++++++++++++++++++++++++++\n")
                fp.write("Date / Time = {} \n".format(datetime.datetime.now()))
                fp.write("Jobfile = {} \n".format(jobFileName))
                fp.write("++++++++++++++++++++++++++++++\n")
                fp.write("pos0:Time(s) pos1:end0_qflux(W/m2) pos2:end1_qflux(W/m2) pos3:end0_T(K) pos4:end1_T(K) \n")
                for i in range(len(temperatureResults)):
                    fp.write("{0:.3f} {1:.8} {2:.8} {3:.8} {4:.8} \n".format(timeList[i],
                                                                             heatFlux_0[i],
                                                                             heatFlux_1[i],
                                                                             temperatureResults[i][0],
                                                                             temperatureResults[i][-1]))

    if '--plot' in uoDict:
        if gdata.graph == 'SINGLE':  # Set if output plotted in single or multiple figures. Options: 'SINGLE', 'MULTIPLE'
            plt.figure()
            ax0 = plt.axes()
            if end0_HTC.type == 'EQUATION_STAGNATION' or end1_HTC.type == 'EQUATION_STAGNATION' or \
               end0_HTC.type == 'EQUATION_LAMINAR_FP' or end1_HTC.type == 'EQUATION_LAMINAR_FP' or \
               end0_HTC.type == 'EQUATION_TURBULENT_FP' or end1_HTC.type == 'EQUATION_TURBULENT_FP' or \
               end0_HTC.type == 'EQUATION_LEADING_EDGE' or end1_HTC.type == 'EQUATION_LEADING_EDGE':
                f, (ax2, ax1, ax3) = plt.subplots(3, 1, sharex=True)
                ax1.set_xlabel('Time [s]')
            else:
                f, (ax2, ax1) = plt.subplots(2, 1, sharex=True)
                ax1.set_xlabel('Time [s]')
            # set labels
            ax0.set_title("Response to HeatFLux")
            ax0.set_xlabel('Spatial Position [m]')
        elif gdata.graph == 'MULTI':
            plt.figure()
            ax0 = plt.axes()
            plt.figure()
            ax1 = plt.axes()
            plt.figure()
            ax2 = plt.axes()
            # set labels
            ax0.set_title("Response to HeatFLux")
            ax0.set_xlabel('Spatial Position [m]')
            ax1.set_xlabel('Time [s]')
            ax1.set_title("Heat Flux at boundaries")
            ax2.set_xlabel('Time [s]')
            ax2.set_title("Surface Temperatures")

        else:
            raise MyError('Setting for gdata.graph={} not recognised.'.format(gdata.graph))

        # plot the results
        for time, temperature in zip(timeResults, temperatureResults):
            ax0.plot(spaceList, temperature, label="t={0:4.2f}s".format(time))
        # set labels
        ax0.set_ylabel('Temperature [K]')
        ax0.legend()

        # plot heat flux if prescribed
        end0_linestyle = '-'
        end1_linestyle = '--'
        actual_linewidth = 3
        # for end0
        ax1.plot(timeList, heatFlux_0, linestyle=end0_linestyle, linewidth=actual_linewidth, label='end0 (actual)')
        if end0_HTC.type == 'SET_Q':
            valueList = [end0_HTC.getValueAtTime(t) for t in timeList]
            ax1.plot(timeList, valueList, linestyle=end0_linestyle, label='end0 (set)')
        if end0_HTC.Qref is not None:
            ax1.plot(end0_HTC.time, end0_HTC.Qref, linestyle=end0_linestyle, label='end0 (reference)')
        if end0_HTC.radiationFlag is True:
            ax1.plot(timeList, [t-r for t, r in zip(heatFlux_0, radiationFlux_0)], linestyle=end0_linestyle, label='end0 (convection)')
            ax1.plot(timeList, radiationFlux_0, linestyle=end0_linestyle, label='end0 (radiation from surface)')
        # for end1
        ax1.plot(timeList, heatFlux_1, linestyle=end1_linestyle, linewidth=actual_linewidth, label='end1 (actual)')
        if end1_HTC.type == 'SET_Q':
            valueList = [end1_HTC.getValueAtTime(t) for t in timeList]
            ax1.plot(timeList, valueList, linestyle=end1_linestyle, label='end1 (set)')
        if end1_HTC.Qref is not None:
            ax1.plot(end1_HTC.time, end1_HTC.Qref, linestyle=end1_linestyle, label='end1 (reference)')
        if end1_HTC.radiationFlag is True:
            ax1.plot(timeList, [t-r for t, r in zip(heatFlux_1, radiationFlux_1)], linestyle=end1_linestyle, label='end1 (convection)')
            ax1.plot(timeList, radiationFlux_1, linestyle=end1_linestyle, label='end1 (radiation from surface)')
        # set labels
        ax1.set_ylabel('Heat Flux [W/m2]')
        ax1.legend()

        # plot temperature at both ends
        temperature = [temp[0] for temp in temperatureResults]
        ax2.plot(timeResults, temperature, linestyle=end0_linestyle, label='end0')
        temperature = [temp[-1] for temp in temperatureResults]
        ax2.plot(timeResults, temperature, linestyle=end1_linestyle, label='end1')
        # set labels
        ax2.set_ylabel('Temperature [K]')
        ax2.legend()

        # plot ambient conditions
        if end0_HTC.type == 'EQUATION_STAGNATION' or \
           end0_HTC.type == 'EQUATION_LAMINAR_FP' or \
           end0_HTC.type == 'EQUATION_TURBULENT_FP' or \
           end0_HTC.type == 'EQUATION_LEADING_EDGE':
            ax3.plot(end0_HTC.time, end0_HTC.rhoInf, linestyle=end0_linestyle, label='end0 (rhoInf)')
            ax3.plot(end0_HTC.time, [v/1000. for v in end0_HTC.VInf], linestyle=end0_linestyle, label='end0 (VInf)')
            ax3.plot(end0_HTC.time, [v/100. for v in end0_HTC.TInf], linestyle=end0_linestyle, label='end0 (TInf) /100')
            ax3.plot(end0_HTC.time, [(T + 0.5*V*V/end0_HTC.Cp)/1000. for T, V in zip(end0_HTC.TInf, end1_HTC.VInf)], linestyle=end1_linestyle, label='end0 (T0) /1000')
            ax3.set_ylabel('Density [kg/m3] \n Velocity [km/s] \n Temperature [K]')
            ax3.legend()
            ax2.plot(end0_HTC.time, [(T + 0.5*V*V/end0_HTC.Cp) for T, V in zip(end0_HTC.TInf, end1_HTC.VInf)], linestyle=end1_linestyle, label='end0 (T0)')
            ax2.legend()
        if end1_HTC.type == 'EQUATION_STAGNATION' or \
           end1_HTC.type == 'EQUATION_LAMINAR_FP' or \
           end1_HTC.type == 'EQUATION_TURBULENT_FP' or \
           end1_HTC.type == 'EQUATION_LEADING_EDGE':
            ax3.plot(end1_HTC.time, end1_HTC.rhoInf, linestyle=end1_linestyle, label='end1 (rhoInf)')
            ax3.plot(end1_HTC.time, [v/1000. for v in end1_HTC.VInf], linestyle=end1_linestyle, label='end1 (VInf)')
            ax3.plot(end1_HTC.time, [v/100. for v in end1_HTC.TInf], linestyle=end1_linestyle, label='end1 (TInf) /100')
            ax3.plot(end1_HTC.time, [(T + 0.5*V*V/end1_HTC.Cp)/1000. for T, V in zip(end1_HTC.TInf, end1_HTC.VInf)], linestyle=end1_linestyle, label='end1 (T0) /1000')
            ax3.set_ylabel('Density [kg/m3] \n Velocity [km/s] \n Temperature [K]')
            ax3.legend()
            ax2.plot(end1_HTC.time, [(T + 0.5*V*V/end1_HTC.Cp) for T, V in zip(end1_HTC.TInf, end1_HTC.VInf)], linestyle=end1_linestyle, label='end1 (T0)')
            ax2.legend()

        plt.draw()

        plt.pause(1)  # <-------
        print('\n \n')

        input('<Hit Enter To Close Figures>')

        plt.close()

    return 0


shortOptions = ""
longOptions = ["help", "job=", "verbosity=", "out-file=", "plot"]


def printUsage():
    """Print Usage instructions."""
    print("")
    print("Usage: SurfTemp.py [--help] [--job=<jobFileName>] [--verbosity=0 (1 or 2)]")
    print("                [--out-file=<FileName>] [--plot]")
    print("")
    print("     --job=          Name of python file, e.g. job.py that defines how the simulation parameters.")
    print("     --verbosity=    Sets the level of on-screen output being provided.")
    print("     --out-file=     Provide filename for writing output data.")
    print("     --plot          flag to plot output data")
    return


class MyError(Exception):
    """Error catching exception."""

    def __init__(self, value):
        """Initialise class."""
        self.value = value

    def __str__(self):
        """Create string."""
        return repr(self.value)


if __name__ == "__main__":
    userOptions = getopt(sys.argv[1:], shortOptions, longOptions)
    uoDict = dict(userOptions[0])

    if len(userOptions[0]) == 0 or "--help" in uoDict:
        printUsage()
        sys.exit(1)

    # execute the code
    try:
        main(uoDict)
        print("\n \n")
        print("SUCCESS.")
        print("\n \n")

    except MyError as e:
        print("This run has gone bad.")
        print(e.value)
        sys.exit(1)
