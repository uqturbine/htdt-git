#! /usr/bin/env python3
"""
Collection of friction factor correlations that are used by HX_solver.py

This module contains the following functions

LAMINAR
Friction factor calculated as 64/Re.
Valid for Re < 2300
Valid for ['GAS', 'LIQUID']

TURBULENT
Friction factor calculated using Haaland's formula
Valid for Re > 2300
Valid for ['GAS', 'LIQUID']

LAMINAR_TURBULENT
Friction factor calculation switches between laminar and turbulent cases, based
Reynolds number.
Valid for ['GAS', 'LIQUID']


Author: Ingo Jahn
Last Modified: 5/5/19
"""

import CoolProp as CP
import CoolProp.CoolProp as CPCP
from HX_fluids import *
import numpy as np

import numpy as np
import CoolProp as CP
import CoolProp.CoolProp as CPCP
from HX_fluids import *
#from HX_solver import MyError

'''
Each heat transfer / friction facto correlation should be defined as an outright 
class. The class should then be initialised and added to the respective list of
heat transfer and friction factor correlations.
'''

# define available correlation
frictionFactorCorrelationList = []


class F_CORRELATION_TEMPLATE:
    """Class template for heat transfer."""

    def __init__(self):
        """Initialise class."""
        self.type = []  # set what type of flow is supported. Options: GAS, LIQUID, 2PHASE_BOILING, 2PHASE_CONDENSING
        self.name = 'TEST'  # name by which correlation will be called
        self.description = ''  # provide a description of the correlation
        self.reference = ''  # provide a reference for the correlation

    def calculate(self, Enthalpy, Pressure, Quality, Twall, mdot, Area, EOS, D_hydraulic, epsilon=0):
        """Calculate friction factor transfer."""
        # add formula to calculate friction factor.
        f = None
        return f

    def check_validity(self, Enthalpy, Pressure, Quality, Twall, mdot, Area, EOS, D_hydraulic, epsilon=0):
        """Check validaity of using ."""
        # add code to calculate if function is within validaity range
        self.validity = 'not checked'

    def write_correlation_details(self):
        """Write correlation details into output file."""
        string = 'Correlation Name={0}; Types of flow supported: {1}; Validity check:'.format(self.name, self.type, self.validity)
        if long is False:
            return string
        else:
            [string + '\nDescription: {0} \nReference: {1}'.format(self.description, self.reference)]
            return string


frictionFactorCorrelationList.append(F_CORRELATION_TEMPLATE())


class LAMINAR:
    """Class template for heat transfer."""

    def __init__(self):
        """Initialise class."""
        self.type = ['GAS', 'LIQUID', 'SUPERCRITICAL']  # set what type of flow is supported. Options: GAS, LIQUID, 2PHASE_BOILING, 2PHASE_CONDENSING
        self.name = 'LAMINAR'  # name by which correlation will be called
        self.description = 'Laminar friction factor calculated as 64/Re'  # provide a description of the correlation
        self.reference = ''  # provide a reference for the correlation

    def calculate(self, Enthalpy, Pressure, Quality, Twall, mdot, Area, EOS, D_hydraulic, epsilon=0):
        """Calculate friction factor transfer."""
        # Compute Bulk properties
        enthalpyBulk = Enthalpy
        pressureBulk = Pressure
        stateBulk = [CP.HmassP_INPUTS, enthalpyBulk, pressureBulk]
        propsBulk = [CP.iDmass, CP.iviscosity]
        [rhoBulk, muBulk] = GetFluidPropLow(EOS, stateBulk, propsBulk)

        # Calculate Reynolds number
        U = abs(mdot/(rhoBulk*Area))
        Re = rhoBulk*U*D_hydraulic/muBulk

        f = 64./Re
        return f

    def check_validity(self, Enthalpy, Pressure, Quality, Twall, mdot, Area, EOS, D_hydraulic, epsilon=0):
        """Check validaity of using ."""
        # Compute Bulk properties
        enthalpyBulk = Enthalpy
        pressureBulk = Pressure
        stateBulk = [CP.HmassP_INPUTS, enthalpyBulk, pressureBulk]
        propsBulk = [CP.iDmass, CP.iviscosity]
        [rhoBulk, muBulk] = GetFluidPropLow(EOS, stateBulk, propsBulk)

        # Calculate Reynolds number
        U = abs(mdot/(rhoBulk*Area))
        Re = rhoBulk*U*D_hydraulic/muBulk
        if Re > 2300.:
            self.validity = 'Re={}. This exceeds validity limit of Re=2300.'
        else:
            self.validity = 'OK'

    def write_correlation_details(self):
        """Write correlation details into output file."""
        string = 'Correlation Name={0}; Types of flow supported: {1}; Validity check:'.format(self.name, self.type, self.validity)
        if long is False:
            return string
        else:
            [string + '\nDescription: {0} \nReference: {1}'.format(self.description, self.reference)]
            return string


frictionFactorCorrelationList.append(LAMINAR())


class TURBULENT:
    """Class template for heat transfer."""

    def __init__(self):
        """Initialise class."""
        self.type = ['GAS', 'LIQUID', 'SUPERCRITICAL']  # set what type of flow is supported. Options: GAS, LIQUID, 2PHASE_BOILING, 2PHASE_CONDENSING
        self.name = 'TURBULENT'  # name by which correlation will be called
        self.description = "Turbulent friction factor calculated using Haaland's formula"  # provide a description of the correlation
        self.reference = ''  # provide a reference for the correlation

    def calculate(self, Enthalpy, Pressure, Quality, Twall, mdot, Area, EOS, D_hydraulic, epsilon=0):
        """Calculate friction factor transfer."""
        # Compute Bulk properties
        enthalpyBulk = Enthalpy
        pressureBulk = Pressure
        stateBulk = [CP.HmassP_INPUTS, enthalpyBulk, pressureBulk]
        propsBulk = [CP.iDmass, CP.iviscosity]
        [rhoBulk, muBulk] = GetFluidPropLow(EOS, stateBulk, propsBulk)

        # Calculate Reynolds number
        U = abs(mdot/(rhoBulk*Area))
        Re = rhoBulk*U*D_hydraulic/muBulk

        temp = np.log10((epsilon/D_hydraulic / 3.7)*1.11 + (6.9/Re))
        f = (-1.8 * temp)**-2.
        return f

    def check_validity(self, Enthalpy, Pressure, Quality, Twall, mdot, Area, EOS, D_hydraulic, epsilon=0):
        """Check validaity of using ."""
        # Compute Bulk properties
        enthalpyBulk = Enthalpy
        pressureBulk = Pressure
        stateBulk = [CP.HmassP_INPUTS, enthalpyBulk, pressureBulk]
        propsBulk = [CP.iDmass, CP.iviscosity]
        [rhoBulk, muBulk] = GetFluidPropLow(EOS, stateBulk, propsBulk)

        # Calculate Reynolds number
        U = abs(mdot/(rhoBulk*Area))
        Re = rhoBulk*U*D_hydraulic/muBulk
        if Re < 2300.:
            self.validity = 'Re={}. This is below the validity limit of Re=2300.'
        else:
            self.validity = 'OK'

    def write_correlation_details(self):
        """Write correlation details into output file."""
        string = 'Correlation Name={0}; Types of flow supported: {1}; Validity check:'.format(self.name, self.type, self.validity)
        if long is False:
            return string
        else:
            [string + '\nDescription: {0} \nReference: {1}'.format(self.description, self.reference)]
            return string


frictionFactorCorrelationList.append(TURBULENT())


class LAMINAR_TURBULENT:
    """Class template for heat transfer."""

    def __init__(self):
        """Initialise class."""
        self.type = ['GAS', 'LIQUID', 'SUPERCRITICAL', '2PHASE_BOILING', '2PHASE_CONDENSING']  # set what type of flow is supported. Options: GAS, LIQUID, 2PHASE_BOILING, 2PHASE_CONDENSING
        self.name = 'LAMINAR_TURBULENT'  # name by which correlation will be called
        self.description = "Friction factor calculation swithces between 64/Re and Haaland's formula"  # provide a description of the correlation
        self.reference = ''  # provide a reference for the correlation

    def calculate(self, Enthalpy, Pressure, Quality, Twall, mdot, Area, EOS, D_hydraulic, epsilon=0):
        """Calculate friction factor transfer."""
        # Compute Bulk properties
        enthalpyBulk = Enthalpy
        pressureBulk = Pressure
        stateBulk = [CP.HmassP_INPUTS, enthalpyBulk, pressureBulk]
        propsBulk = [CP.iDmass, CP.iviscosity]
        [rhoBulk, muBulk] = GetFluidPropLow(EOS, stateBulk, propsBulk)

        # Calculate Reynolds number
        U = abs(mdot/(rhoBulk*Area))
        Re = rhoBulk*U*D_hydraulic/muBulk
        if Re < 2300.:
            f = 64./Re
        else:
            temp = np.log10((epsilon/D_hydraulic / 3.7)*1.11 + (6.9/Re))
            f = (-1.8 * temp)**-2.
        return f

    def check_validity(self, Enthalpy, Pressure, Quality, Twall, mdot, Area, EOS, D_hydraulic, epsilon=0):
        """Check validaity of using ."""
        # no Reynolds number restritions.
        self.validity = 'OK'

    def write_correlation_details(self):
        """Write correlation details into output file."""
        string = 'Correlation Name={0}; Types of flow supported: {1}; Validity check:'.format(self.name, self.type, self.validity)
        if long is False:
            return string
        else:
            [string + '\nDescription: {0} \nReference: {1}'.format(self.description, self.reference)]
            return string


frictionFactorCorrelationList.append(LAMINAR_TURBULENT())


def get_Correlation_List(type):
    """Return list of available fucntions."""
    NameList = []
    if type == 'ALL':
        for n in frictionFactorCorrelationList:
            NameList.append(n.name)
        return NameList, frictionFactorCorrelationList
    else:
        for n in frictionFactorCorrelationList:
            if type in n.type:
                NameList.append(n.name)
        return NameList


class MyError(Exception):
    """Error catching exception."""

    def __init__(self, value):
        """Initilaise class."""
        self.value = value

    def __str__(self):
        """Create string."""
        return repr(self.value)
