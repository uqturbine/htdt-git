#! /usr/bin/env python3
"""
Collection of heat transfer and friction factor correlations that are used 
by HX_solver.py

This module contains the following functions

## Nu_correlation_SinglePhase()
Collection of Nusselt Number correlations that apply to single phase flow in
heat exchangers.

## Nu_correlations_twoPhase()
Collection of Nusselt Number correlations that apply to two phase flow in heat
exchangers. 

## calc_friction_singlePhase
Collection of friction factor correlations that apply to single phase flow in
heat exchangers.

## calc_friction_twoPhase
Collection of friction factor correlations that apply to two phase flow in
heat exchangers.


Author: Ingo Jahn
Last Modified: 4/11/17
"""

import numpy as np
import CoolProp as CP
import CoolProp.CoolProp as CPCP
from HX_fluids import *
#from HX_solver import MyError



def calc_Nu_singlePhase(Pm,Pp,Tm,Tp,Tw,mdot,A_total,L_c,Eos,Correlation,N,M):
    """
    function to calculate local Nusselt number based on bulk flow properties
    set up to work with single phase flow
    Inputs:
    P - bulk pressure (Pa)
    Tm - bulk temperature to left (K)
    Tp - bulk temperature to right (K)
    Tw - temperature of wall (K)
    mdot - total mass flow rate (kg/s)
    A_total - total flow area (m**2)
    L_c - characteristicjlength (m)
    Eos - Equation of State type
    Correlation - select correlation to be used:
        1 - Yoon correlation for sCO2 in pipes for Tb > Tpc
        2 - S.M. Liao and T.S Zhaou correlation for microchannels
            http://www.me.ust.hk/~mezhao/pdf/33.PDF
            - horizontal pipes
        3 - S.M. Liao and T.S Zhaou correlation for microchannels
            http://www.me.ust.hk/~mezhao/pdf/33.PDF
            - vertical pipes, upwards flow
        4 - - S.M. Liao and T.S Zhaou correlation for microchannels
            http://www.me.ust.hk/~mezhao/pdf/33.PDF
            - vertical pipes, downwards flow
        5 - Dittus Boelter Equation
            HLT
        6 - as per paper by Xi
        7 - Natural Convection
            HLT
        8 - J.D. Jackson and W.B. Hall ideal gas correlation
            Forced convection heat transfer to fluids at supercritical pressure,
            in Turbulent forced convection in channels and bundles.
    Outputs:
    Nu - Nusselt Number
    """
    # Calculate bulk temperature
    Tb = 0.5*(Tm+Tp)
    Pb = 0.5*(Pm+Pp)

    # check if two-phase
    stateBulk = [CP.PQ_INPUTS,Pb,1.]
    propsBulk = [CP.iT]    
    T_sat = GetFluidPropLow(Eos,stateBulk,propsBulk)

    # Compute Bulk properties      
    stateBulk = [CP.PT_INPUTS,Pb,Tb]
    propsBulk = [CP.iPrandtl,CP.iDmass,CP.iviscosity,CP.iUmass,CP.iCpmass]
    [Pr,rho_b,mu_b,i_b,Cp_b] = GetFluidPropLow(Eos,stateBulk,propsBulk)

    # Compute properties at wall
    stateWall = [CP.PT_INPUTS,Pb,Tw]
    propsWall = [CP.iDmass,CP.iUmass]
    [rho_w,i_w] = GetFluidPropLow(Eos,stateWall,propsWall)

    # Compute properties at midpoint
    stateMid = [CP.PT_INPUTS,Pb,0.5*(Tw+Tb)]
    propsMid = [CP.iDmass]
    rho_mid = GetFluidPropLow(Eos,stateMid,propsMid)

    # Calculate Reynolds number
    U = abs(mdot/(rho_b*A_total))
    Re = rho_b*U*L_c/mu_b

    if M.verbosity > 1:
        print('In single phase')        
        print('Tm,Tp,Pm,Pp:',Tm,Tp,Pm,Pp)
        print('Tsat:',T_sat)
        print('stateBulk:',stateBulk)
        print('Pr,rho_b,mu_b,i_b,Cp_b:',GetFluidPropLow(Eos,stateBulk,propsBulk))
        print('stateWall:',stateWall)
        print('rho_w,i_w:',GetFluidPropLow(Eos,stateWall,propsWall))


    if Correlation is 1:
        # Yoon correlation for horizontal pipes
        # Compute pseudocritical temp and density according to Liao paper (in K)
        T_pc = -122.6 + 6.124*(Pb*1e-5)-0.1657*(Pb*1e-5)**2 + 0.01773*(Pb*1e-5)**2.5  \
                - 0.0005608*(Pb*1e-5)**3 + 273.15
        if Tb > T_pc:
            # Compute supercritical Nusselt number
            Nu = 0.14 * Re**0.69 * Pr**0.66
        else:
            # Compute subcritical Nusselt number
            state_pc = [CP.PT_INPUTS,Pb,Tpc]
            props_pc = [CP.iDmass]
            rho_pc = GetFluidPropLow(Eos,state_pc,props_pc)
            # Compute Nusselt number correlation
            Nu = 0.013 * Re**1.0 * Pr**-0.05 * (rho_pc/rho_b)**1.6

    elif Correlation is 2:
        # Liao correlation for horizontal pipes
        Gr = abs( 9.80665 * (rho_b-rho_w)*rho_b*L_c**3 / mu_b**2)
        if Tw == Tb:
            Nu = 0
        else:
            Cp_bar = (i_w-i_b) / (Tw-Tb)
            #print('Gr: ', Gr, 'Cp_bar: ', Cp_bar)
            Nu = 0.124 * Re**0.8 * Pr**0.4 * (Gr/Re**2)**0.203 * (rho_w/rho_b)**0.842 * (Cp_bar / Cp_b)**0.384
 
    elif Correlation is 3:
        # Liao correlation for vertical pipes - upwards flow
        rho_m = 1/(Tw-Tb) * (Tw-Tb)/6. * (rho_b + 4*rho_mid + rho_w)  # integration using Simpsons rule
        Gr_m = abs( 9.80665 * (rho_b-rho_m)*rho_b*L_c**3 / mu_b**2)
        if Tw == Tb:
            Nu = 0
        else:
            Cp_bar = (i_w-i_b) / (Tw-Tb)
            #print('Gr: ', Gr, 'Cp_bar: ', Cp_bar)
            Nu = 0.354 * Re**0.8 * Pr**0.4 * (Gr_m/Re**2.7)**0.157 * (rho_w/rho_b)**1.297 * (Cp_bar / Cp_b)**0.296 

    elif Correlation is 4:
        # Liao correlation for vertical pipes - downwards flow
        rho_m = 1/(Tw-Tb) * (Tw-Tb)/6. * (rho_b + 4*rho_mid + rho_w)  # integration using Simpson's rule
        Gr_m = abs( 9.80665 * (rho_b-rho_m)*rho_b*L_c**3 / mu_b**2)
        if Tw == Tb:
            Nu = 0
        else:
            Cp_bar = (i_w-i_b) / (Tw-Tb)
            # print('Gr: ', Gr, 'Cp_bar: ', Cp_bar)
            Nu = 0.643 * Re**0.8 * Pr**0.4 * (Gr_m/Re**2.7)**0.186 * (rho_w/rho_b)**2.154 * (Cp_bar / Cp_b)**0.751 

    elif Correlation is 5:
        # For flow in circular pipes (from HLT) use for incompressible fluids (water/oil)
        # Dittus Boelter Equation
        if Tw > Tb: # heating of fluid
            n = 0.3
        else:       # cooling of fluid
            n = 0.4
        Nu = 0.023 * Re**0.8 * Pr**n

    elif Correlation is 6:
        # Correlation for shell side as per paper by Xie et al.
        C = 0.16442; m=0.65582
        e = np.exp( C + m * np.log(Re) )
        Nu = e * Pr**(1./3.)

    elif Correlation is 7:
        if Re == 0. or Re < 0.0001:
            # use Natural Convection relationship
            rho_mid = GetFluidPropLow(Eos,[CP.PT_INPUTS,Pb,Tw],[CP.iDmass])
            beta = 1./Tb
            Gr = abs( 9.80665 * rho_w**2 * L_c**3 * (Tw - Tb) * beta / mu_b**2)
            GrPr = Gr*Pr
            if GrPr <= 1e9:
                C = 0.53; n=1/4
            else:
                C = 0.126; n=1/3
            Nu = C * GrPr**n
        else:
            # forced convection case
            if Re > 0.0001 and Re <= 0.004:
                C = 0.437; n = 0.0985
            elif Re >0.004 and Re <= 0.09:
                C = 0.565; n = 0.136
            elif Re > 0.09 and Re <= 1.:
                C = 0.8; n = 0.280
            elif Re > 1.0 and Re <= 35.:
                C = 0.795; n= 0.384
            elif Re > 35. and Re <= 5000.:
                C = 0.583; n = 0.471
            elif Re > 5000. and Re <= 50000.:
                C = 0.148; n = 0.633
            elif Re > 50000. and Re <= 200000:
                C = 0.0208; n = 0.814
            else:
                raise MyError('Correlation outside range of valid Nusselt numbers.')
            Nu = C* Re**n


    elif Correlation is 8:
        # Jackson (1979) - ideal gas region correlation
        Nu = 0.0183*Re**0.82*Pr**0.5

    elif Correlation is 25:
        # User input Nusselt number correlation
        # Nu = N.A*(Re**N.alpha)*(Pr**N.beta)
        # Follows same form as Liao correlation for horizontal pipes (bouyancy
        # effects are not important for this height scale)
        Gr = abs(9.80665*(rho_b-rho_w)*rho_b*L_c**3/mu_b**2)
        if Tw == Tb:
            Nu = 0
        else:
            Cp_bar = (i_w-i_b) / (Tw-Tb)
            Nu = N.A*(Re**N.B)*(Pr**N.C)*((Gr/Re**2)**N.D)*((rho_w/rho_b)**N.E)*((Cp_bar/Cp_b)**N.F)

    elif Correlation is 26:
        # User input Nusselt number correlations with switching between
        # supercritical and liquid conditions

        # Compute pseudocritical density
        state_pc = [CP.PT_INPUTS,Pb,T_pc]
        props_pc = [CP.iDmass]
        rho_pc = GetFluidPropLow(Eos,state_pc,props_pc)

        # Compute supercritical Nu parameters
        Gr = abs(9.80665*(rho_b-rho_w)*rho_b*L_c**3/mu_b**2)
        Cp_bar = (i_w-i_b) / (Tw-Tb)

        # Evaluate correlation
        if Tw == Tb:
            # No heat transfer condition
            return 0
        elif Tb <= T_pc-N.BETA:
            # Compute subcritical (liquid) Nusselt number
            Nu = N.LIQ_A*Re**N.LIQ_B*Pr**N.LIQ_C*(rho_pc/rho_b)**N.LIQ_D
        elif T_pc-N.BETA < Tb < T_pc+N.ALPHA:
            # Compute blended Nusselt number (should create lambda functions for NuLiq and NuSuper)
            psi = (Tb-(T_pc-N.BETA))/(N.ALPHA+N.BETA)
            NuLiq = N.LIQ_A*Re**N.LIQ_B*Pr**N.LIQ_C*(rho_pc/rho_b)**N.LIQ_D
            NuSuper = N.SC_A*(Re**N.SC_B)*(Pr**N.SC_C)*((Gr/Re**2)**N.SC_D)*((rho_w/rho_b)**N.SC_E)*((Cp_bar/Cp_b)**N.SC_F)
            # Compute blended Nusselt number
            Nu = (1-psi)*NuLiq + psi*NuSuper
        elif T_pc+N.ALPHA <= Tb:
            # Compute supercritical (liquid) Nusselt number
            Nu = N.SC_A*(Re**N.SC_B)*(Pr**N.SC_C)*((Gr/Re**2)**N.SC_D)*((rho_w/rho_b)**N.SC_E)*((Cp_bar/Cp_b)**N.SC_F)

    elif Correlation is 27:
        # Liao (2002, Horizontal Pipes) and Jackson (1979) correlations with switching at N.ALPHA (~110 C)

        # Liao correlation
        Gr = abs(9.80665*(rho_b-rho_w)*rho_b*L_c**3/mu_b**2)
        Cp_bar = (i_w-i_b)/(Tw-Tb)
        NuLiao = 0.124*Re**0.8*Pr**0.4*(Gr/Re**2)**0.203*(rho_w/rho_b)**0.842*(Cp_bar/Cp_b)**0.384

        # Jackson correlation
        NuJackson = 0.0183*Re**0.82*Pr**0.5

        # Linear blending parameters: N.ALPHA - transition temp, N.BETA - HALF blending region
        if Tw == Tb:
            Nu = 0
        elif Tb <= N.ALPHA-N.BETA:
            # Use Liao Nusselt number
            Nu = NuLiao
        elif N.ALPHA-N.BETA < Tb < N.ALPHA+N.BETA:
            # Compute blended Nusselt number
            psi = (Tb-(N.ALPHA-N.BETA))/(2*N.BETA)
            # Compute blended Nusselt number
            Nu = (1-psi)*NuLiao + psi*NuJackson
        elif N.ALPHA+N.BETA <= Tb:
            # Use Jackson Nusselt number
            Nu = NuJackson

    else:
        raise MyError('Correlation option for Nusselt number calculation not implemented.')

    return Nu

###
###
def calc_Nu_twoPhase(Pm,Pp,Q,Tm,Tp,Tw,mdot,A_total,L_c,Eos,Correlation,N,M):
    """
    function to calculate local Nusselt number based on bulk flow properties
    set up to work with two phase flows.
    Inputs:
    P - bulk pressure (Pa)
    Tm - bulk temperature to left (K)
    Tp - bulk temperature to right (K)
    Tw - temperature of wall (K)
    mdot - total mass flow rate (kg/s)
    A_total - total flow area (m**2)
    L_c - characteristicjlength (m)
    Eos - Equation of State type    
    N - ???
    M - Model class
    Outputs:
    Nu - Nusselt number
    """

    # Calculate bulk temperature
    Tb = 0.5*(Tm+Tp)
    Pb = 0.5*(Pm+Pp)

    # slip_factor = 1   # u_gas / u_liquid

    """
    stateBulk = [CP.PQ_INPUTS,Pb,0.]
    propsBulk = [CP.iPrandtl,CP.iDmass,CP.iviscosity]    
    Pr_L, rho_L, mu_L = GetFluidPropLow(Eos,stateBulk,propsBulk)

    stateBulk = [CP.PQ_INPUTS,Pb,1.]
    propsBulk = [CP.iPrandtl,CP.iDmass,CP.iviscosity]    
    Pr_G, rho_G, mu_G = GetFluidPropLow(Eos,stateBulk,propsBulk)

    rho_mix = (1.-Q)*rho_L + Q*rho_G
    mu_mix  = (1.-Q)*mu_L  + Q*mu_G
    Pr_mix  = (1.-Q)*Pr_L  + Q*Pr_G
    """

    # use CoolProp and call to quality to determien fluid properties. 
    stateBulk = [CP.PQ_INPUTS,Pb,Q]
    propsBulk = [CP.iPrandtl,CP.iDmass,CP.iviscosity]    
    Pr, rho, mu = GetFluidPropLow(Eos,stateBulk,propsBulk)

    U = abs(mdot/(rho*A_total))

    # Calculate Reynolds number
    Re = rho * U * L_c / mu

    if M.verbosity > 1:
        print("in two phase")
        print("Quality:", Q)
        print("Pr,rho,mu:",Pr,rho,mu)
        print("Re:",Re)


    if Correlation is 5:
        # For flow in circular pipes (from HLT) use for incompressible fluids (water/oil)
        # Dittus Boelter Equation
        if Tw > Tb: # heating of fluid
            n = 0.3
        else:       # cooling of fluid
            n = 0.4
        Nu = 0.023 * Re**0.8 * Pr**n

    elif Correlation is 7:
        if Re == 0. or Re < 0.0001:
            # use Natural Convection relationship
            rho_mid = GetFluidPropLow(Eos,[CP.PT_INPUTS,Pb,Tw],[CP.iDmass])
            beta = 1./Tb
            Gr = abs( 9.80665 * rho_w**2 * L_c**3 * (Tw - Tb) * beta / mu_b**2)
            GrPr = Gr*Pr
            if GrPr <= 1e9:
                C = 0.53; n=1/4
            else:
                C = 0.126; n=1/3
            Nu = C * GrPr **n
        else:
            if Re > 0.0001 and Re <= 0.004:
                 C = 0.437; n = 0.0985
            elif Re >0.004 and Re <= 0.09:
                 C = 0.565; n = 0.136
            elif Re > 0.09 and Re <= 1.:
                C = 0.8; n = 0.280
            elif Re > 1.0 and Re <= 35.:
                C = 0.795; n= 0.384
            elif Re > 35. and Re <= 5000.:
                C = 0.583; n = 0.471
            elif Re > 5000. and Re <= 50000.:
                C = 0.148; n = 0.633
            elif Re > 50000. and Re <= 200000:
                C = 0.0208; n = 0.814
            elif Re > 200000:
                C = 0.0208; n = 0.814
                print('Warning: Exceeding Reynolds number limit for Nusselt number correlation')
            else:
                raise MyError('Correlation outside range of valid Nusselt numbers.')
            Nu = C* Re**n

    else:
        raise MyError('Correlation option for Nusselt number calculation not implemented in twoPhase flow.')

    return Nu

###
###

def calc_friction_singlePhase(P, Tm, Tp, mdot, A_total, Dh, Eos, Correlation, epsilon = 0.):
    """
    function to calculate local friction factor based on local geometry and bulk flow properties
    implementation for single Phase flow
    Inputs:
    P - bulk pressure (Pa)
    Tm - bulk temperature to left (K)
    Tp - bulk temperature to right (K)
    mdot - total mass flow rate (kg/s)
    A_total - total flow area (m**2)
    Dh - Hydraulic Diameter (m)
    Eos - Equation of State
    Correlation - select correlation to be used:
        0 - no pressure drop calculation is performed. This option should not be called 
        1 - automatically switches between laminar and turbulent flow
        2 - laminar flow - circular pipe
        3 - turbulent flow - rough pipes (Haaland's formula)
    epsilon - roughness height (m)

    Outputs:
    f - friction factor
    """
    #print('In calc_frication:',P, Tm, Tp, mdot, A_total, Dh, Eos, Correlation)

    Tb = 0.5*(Tm+Tp) # bulk temperature

    # calculate Reynolds number
    state = [CP.PT_INPUTS,P,Tb]
    props = [CP.iDmass,CP.iviscosity]
    [rho_b,mu_b] = GetFluidPropLow(Eos,state,props)
    U = abs(mdot / (rho_b * A_total))
    Re = rho_b * U * Dh / mu_b
    Re = np.mean(Re) # FIX - needs to do cellwise friction factor for variable geometry HXs
    if Correlation is 0:
        raise MyError('Should not enter friction factor calculation if Correlation = 0 has been set.')        
    elif Correlation is 1:
        if Re < 2300:
            f = 64. / Re
        else:
            temp = np.log10( (epsilon/Dh / 3.7)*1.11 + (6.9/Re ))
            f = (-1.8 * temp )**-2.
    elif Correlation is 2:
        # laminar pipe flow
        f = 64. / Re
    elif Correlation is 3:
        # tubulent rough pipes
        temp = np.log10( (epsilon/Dh / 3.7)*1.11 + (6.9/Re ))
        f = (-1.8 * temp )**-2.

    else:
        raise MyError('Correlation option for friction factor calculation not implemented.')

    # print(Gr / Re**2)

    return f

###
###

def calc_friction_twoPhase(P, Q, Tm, Tp, mdot, A_total, Dh, Eos, Correlation, epsilon = 0.):
    """
    function to calculate local friction factor based on local geometry and bulk flow properties
    implementation for two Phase flow
    Inputs:
    P - bulk pressure (Pa)
    Tm - bulk temperature to left (K)
    Tp - bulk temperature to right (K)
    mdot - total mass flow rate (kg/s)
    A_total - total flow area (m**2)
    Dh - Hydraulic Diameter (m)
    Eos - Equation of State
    Correlation - select correlation to be used:
        1 - automatically switches between laminar and turbulent flow
        2 - laminar flow - circular pipe
        3 - turbulent flow - rough pipes (Haaland's formula)
    epsilon - roughness height (m)

    Outputs:
    f - friction factor
    """

    raise MyError('No friction correlations for two phase flow have been implemented so far')

class MyError(Exception):
    def __init__(self, value):
        self.value = value
    def __str__(self):
        return repr(self.value)

