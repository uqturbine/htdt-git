#! /usr/bin/env python3
"""
Collection of functions that allow HX_solver to work with custom fluids and to
directly access the CoolProp low level interface. 


Example for calling low level fluid interface:
props = [CP.iHmass, CP.iDmass]
state = [CP.PT_INPUTS,self.PH_out,self.TC_in]
h,d = GetFluidPropLow(EosH,state,props)


Author: Ingo Jahn
Last Modified: 4/11/17
"""

import numpy as np
from scipy import interpolate
import CoolProp as CP
import CoolProp.CoolProp as CPCP
#from HX_solver import MyError



def PropIds(index):
    """ Matches low level CoolProp property indicies to HTF oil props. """
    props = {
            CP.iDmass       : 'DMASS',
            CP.iDmass       : 'D',
            CP.iT           : 'T',
            CP.iCpmass      : 'CPMASS',
            CP.iconductivity: 'CONDUCTIVITY',
            CP.iPrandtl     : 'PRANDTL',
            CP.iHmass       : 'HMASS',
            CP.iUmass       : 'UMASS',
            CP.iviscosity   : 'VISCOSITY',
            CP.iQ           : 'QUALITY',
            CP.iSmass       : 'ENTROPY',
            CP.iT_max       : 'TMAX',
            CP.iT_min       : 'TMIN',
            CP.iP_max       : 'PMAX',
            CP.iP_min       : 'PMIN',
            CP.iP_critical  : 'PRCRIT'
            }
    return props[index]

###
###

def GetFluidPropLow(Eos,state,props):
    """
    Low level fluid property call.
    Uses CoolProp low level interface for all supported fluids.
    Computes Shell Thermia D properties using 'HTFOilProps'

    Inputs:
    Eos   - Equation of state object for desired fluid.
    state - 1x3 list that specifies fluid state according to CoolProp
            low level syntax. Takes the form
            [InputPairId,Prop1Value,Prop2Value], for example
            state = [CP.PT_INPUTS,20e6,273.15+150].
    props - List specifying required output properties in CoolProp low
            level syntax. For example, props = [CP.iPrandtl,CP.iDmass]
            will give Prandtl number and density.

    Outputs:
    outputProps - Array containing desired output properties.

    Notes:
    Currently only supports pressure & temperature input pairs.
    """
    if isinstance(Eos,CustomFluid):
        # Convert to custom fluid property format
        props = [PropIds(j) for j in props]
        if state[0] == 9:  # corresponds to CP.PT_INPUT
            T = state[2]
            # check if temperature range is being exceeded:
            if Eos.Tmax is not None and T > Eos.Tmax:
                print("WARNING: Custom Fluid being evaluated at {0}. This is above temperature range limit for CUSTOM fluid. Maximum supported temperature Tmax = {1}. Code is likely to crash soon!".format(T,Eos.Tmax))
            if Eos.Tmin is not None and T < Eos.Tmin:
                print("WARNING: Custom Fluid being evaluated at {0}. This is below temperature range limit for CUSTOM fluid. Maximum supported temperature Tmax = {1}. Code is likely to crash soon!".format(T,Eos.Tmax))

            #outputProps = [eval('Eos.'+k+'('+str(T)+')') for k in props]
            outputProps = [ getattr(Eos, k)(T) for k in props]
        elif state[0] == 20: # corresponds to CP.HmassP_INPUT
            H = state[1]
            # outputProps = [eval('Eos.'+k+'('+str(H)+')') for k in props]
            outputProps = [ getattr(Eos, k)(H) for k in props]
        elif state[0] == 2: # corresponds to CP.PQ_INPUT
            H = state[1]
            # outputProps = [eval('Eos.'+k+'('+str(H)+')') for k in props]
            outputProps = [ getattr(Eos, k)(H) for k in props]
        else:
            MyError('Custom fluid call only supports CP.PT_INPUT and \
                    CP.HmassP_INPUT and CP.PQ_INPUT')
        if len(outputProps) == 1:
            return outputProps[0]
        else:
            return outputProps
    else: # use CoolProp
        try:
            Eos.update(*state)
            outputProps = [Eos.keyed_output(k) for k in props]
            if len(outputProps) == 1:
                return outputProps[0]
            else:
                return outputProps
        except:
            MyError('Invalid fluid specified.')

###
###

class CustomFluid:
    def __init__(self,fileName):
        self.fileName             = fileName
        # self.temperature          =  []
        # self.density              =  []
        # self.specific_heat        =  []
        # self.enthalpy             =  []
        # self.conductivity         =  []
        # self.viscosity            =  []
        # self.prandtl_number       =  []
        self.Psat                 =  []
        self.Tmin                 =  None
        self.Tmax                 =  None
        self.TminPsat             =  []
        self.name                 =  'Custom Fluid'
        self.description          =  'Description'
        self.reference            =  'Reference'

        try:
            exec(open(self.fileName).read(),globals(),locals())
        except:
            MyError("Data file "+fileName+" containing custom fluid \
                    properties cannot be loaded. Check that this file exists \
                    and is named correctly.")

        # Create property interpolation objects that give property values
        # as a function of input state. Same names as CoolProp used for
        # properties.
        self.DMASS      = interpolate.interp1d(self.temperature,self.density, kind='cubic',
                            fill_value='extrapolate') # Density
        self.D          = self.DMASS
        self.T          = interpolate.interp1d(self.enthalpy,self.temperature,
                            kind='cubic',fill_value='extrapolate') # Temperature
        self.TMAX          = interpolate.interp1d(self.enthalpy,self.Tmax*np.zeros(np.size(self.enthalpy)),
                            kind='linear',fill_value='extrapolate') # maximum Temperature
        self.TMIN          = interpolate.interp1d(self.enthalpy,self.Tmin*np.zeros(np.size(self.enthalpy)),
                            kind='linear',fill_value='extrapolate') # minimum Temperature
        self.CPMASS     = interpolate.interp1d(self.temperature,self.specific_heat,
                            kind='linear',fill_value=(self.specific_heat[0],self.specific_heat[-1]),
                            bounds_error=False) # Specific heat
        self.CONDUCTIVITY = interpolate.interp1d(self.temperature,self.conductivity,
                            kind='cubic',fill_value='extrapolate') # Thermal conductivity
        self.PRANDTL    = interpolate.interp1d(self.temperature,self.prandtl_number,
                            kind='cubic',fill_value='extrapolate')# Prandtl number
        self.HMASS      = interpolate.interp1d(self.temperature,self.enthalpy,
                            kind='cubic',fill_value='extrapolate') # Enthalpy
        self.UMASS      = self.HMASS
        self.VISCOSITY  = interpolate.interp1d(self.temperature,self.viscosity,
                            kind='linear',fill_value='extrapolate') # DYNAMIC viscosity
        self.QUALITY    = interpolate.interp1d(self.enthalpy,self.quality,
                            kind='linear',fill_value='extrapolate') # quality

class MyError(Exception):
    def __init__(self, value):
        self.value = value
    def __str__(self):
        return repr(self.value)
