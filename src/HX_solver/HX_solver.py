#! /usr/bin/env python3
"""
Python Code to evaluate on- and off-design performance of heat exchangers.

Function has two operating modes:

(1) Stand alone
This evaluates the heat exchanger performance and can be used to plot
temperature traces inside the heat exchanger.

(2) Imported
The function can be called by Cycle.py to perform quasi-steady heat
exchanger performance evaluation as part of cycle off-design modelling.

Version: 1.0
Author: Ingo Jahn
Last Modified: 5/07/17
"""

import CoolProp as CP
import CoolProp.CoolProp as CPCP
from getopt import getopt
import HX_f_correlations as hx_f
import HX_Nu_correlations as hx_Nu
from HX_fluids import *
import matplotlib.pyplot as plt
import numpy as np
import os as os
from scipy import optimize
from scipy import interpolate
import sys as sys
import pdb


class Fluid:
    """Class defining fluid."""

    def __init__(self):
        """Initialise class."""
        self.fluidH = []  # set fluid for H channel
        self.fluidC = []  # set fluid for C channel
        self.TH_in = []  # (K)
        self.QH_in = []  # [-]
        self.hH_in = []  # J/kg/K
        self.mdotH = []  # (kg/s)
        self.PH_in = []  # pressure (Pa)
        self.PH_out = []  # pressure (Pa)
        self.TC_in = []  # (K)
        self.QC_in = []  # [-]
        self.hC_in = []  # J/kg/K
        self.mdotC = []  # (kg/s)
        self.PC_in = []  # pressure (Pa)
        self.PC_out = []  # pressure (Pa)
        self.T_ext = []  # ambient surrounding temperature
        self.T0 = []
        self.HT0 = []
        self.Q0 = []
        self.dPH_poly = None
        self.dPC_poly = None

    def check(self, M):
        """Fucntion to check that class has been defined correctly."""
        # fluid type
        if not self.fluidH and not self.fluidC:
            raise MyError('Neither F.fluidH or F.fluidC specified')
        if self.fluidH and not self.fluidC:
            self.fluidC = self.fluidH
            print('fluidC not specified and set to equal fluidH')
        if self.fluidC and not self.fluidH:
            self.fluidH = self.fluidC
            print('fluidH not specified and set to equal fluidC')
        # H channel
        if (not self.TH_in) and (not self.hH_in):
            raise MyError('F.TH_in or F.hH_in not specified correctly')
        if self.TH_in:
            if not self.QH_in:
                self.QH_in = -1.
                print('QH_in not specified and set to -1. \
                       (outside 2-phase region)')
        if not self.mdotH:
            raise MyError('F.mdot_H not specified')
        if not self.PH_in:
            raise MyError('F.PH_in not specified')
        if not self.PH_out:
            self.PH_out = self.PH_in
            print('PH_out not specified and set to equal PH_in')
        # C channel
        if (not self.TC_in) and (not self.hC_in):
            raise MyError('F.TC_in or F.hC_in not specified correctly')
        if self.TC_in:
            if not self.QC_in:
                self.QC_in = -1.
                print('QC_in not specified and set to -1. \
                       (outside 2-phase region)')
        if not self.mdotC:
            self.mdotC = self.mdotH
            print('mdotC not specified and set to equal mdotH')
        if self.mdotC < 0. or self.mdotH < 0.:
            raise MyError('Mass flow rates in both channels needs to be > 0.')
        if not self.PC_in:
            raise MyError('F.PC_in not specified')
        if not self.PC_out:
            self.PC_out = self.PC_in
            print('PC_out not specified and set to equal PC_in')

        if self.dPH_poly is not None and M.f_CorrelationH is not 0:
            raise MyError("F.dPH_poly= {0} has been specified. This is not supported in combination with M.f_CorrelationH = {1}. To use pressure drop prescribed by polynominal, set M.f_CorrelationH = 0 \n Bailing Out!".format(self.dPH_poly, M.f_CorrelationH))
        if self.dPC_poly is not None and M.f_CorrelationC is not 0:
            raise MyError("F.dPC_poly= {0} has been specified. This is not supported in combination with M.f_CorrelationC = {1}. To use pressure drop prescribed by polynominal, set M.f_CorrelationC = 0 \n Bailing Out!".format(self.dPC_poly, M.f_CorrelationC))

        # if M.external_loss is 1:
        #    if not self.T_ext:
        #        raise MyError('T_ext not specified')

        if M.verbosity > 0:
            print('Check of Fluid input parameters: Passed')

    def set_BC(self):
        """Set boundary conditions for HX."""
        # treatment of case when T and h are specified. Selected approach is:
        # (1) if only T (and Q) is specified, convert to h and go to h mode
        # (2) if only h is specified --> go to h maode
        # (3) if both specified, use h to overwrite T and Q --> go to h mode

        if self.TH_in and (not self.hH_in):  # case (1)
            if self.QH_in == -1.:  # fluid is outside of two-phase region
                state = [CP.PT_INPUTS, self.PH_in, self.TH_in]
                props = [CP.iHmass]
                self.hH_in = GetFluidPropLow(EosH, state, props)
            else:
                state = [CP.PQ_INPUTS, self.PH_in, self.QH_in]
                props = [CP.iHmass]
                self.hH_in = GetFluidPropLow(EosH, state, props)
        else:  # case (2) and (3)
            state = [CP.HmassP_INPUTS, self.hH_in, self.PH_in]
            props = [CP.iQ]
            self.QH_in = GetFluidPropLow(EosH, state, props)
            if self.QH_in == -1.:  # fluid is outside of two-phase region
                props = [CP.iT]
                self.TH_in = GetFluidPropLow(EosH, state, props)
            else:
                state = [CP.PQ_INPUTS, self.PH_in, 0.]
                props = [CP.iT]
                self.TH_in = GetFluidPropLow(EosH, state, props)

        if self.TC_in and (not self.hC_in):  # case (1)
            if self.QC_in == -1.:  # fluid is outside of two-phase region
                state = [CP.PT_INPUTS, self.PC_in, self.TC_in]
                props = [CP.iHmass]
                self.hC_in = GetFluidPropLow(EosC, state, props)
            else:
                state = [CP.PQ_INPUTS, self.PC_in, self.QC_in]
                props = [CP.iHmass]
                self.hC_in = GetFluidPropLow(EosC, state, props)
        else:  # case (2) and (3)
            state = [CP.HmassP_INPUTS, self.hC_in, self.PC_in]
            props = [CP.iQ]
            self.QC_in = GetFluidPropLow(EosC, state, props)
            if self.QC_in == -1:  # fluid is outside of two-phase region
                props = [CP.iT]
                self.TC_in = GetFluidPropLow(EosC, state, props)
            else:
                state = [CP.PQ_INPUTS, self.PC_in, 0.]
                props = [CP.iT]
                self.TC_in = GetFluidPropLow(EosC, state, props)
        return 0

    ###
    def get_T0_old(self, M):
        """
        Set initial temperature vector.

        function to set initial/old conditions for Temperature Vector
        Support vectors of 2*Ncell and 4*Ncell
        Inputs:
        M - instance of Model Class
        """
        if not (M.n == 2 or M.n == 4):
            raise MyError('get_T0 only supports n=2 or n=4')

        if len(self.T0) is not 0:
            if M.n == 2:
                if len(self.T0) is not 2*M.N_cell:
                    raise MyError("in get_T0: length of T0 supplied doesn't \
                                   match model. Len(T0) = 2 * N_cell expected")
            if M.n == 4:
                if len(self.T0) is not 4*M.N_cell:
                    raise MyError("in get_T0: length of T0 supplied doesn't \
                                   match model. Len(T0) = 4 * N_cell expected")

        if len(self.T0) is 0:
            T0 = np.zeros((M.n*M.N_cell))
            if M.co_flow:
                # set TH
                # T0[0:M.N_cell] = self.TH_in + np.arange(M.N_cell)/float(M.N_cell)*(self.TC_in-self.TH_in)*0.25
                # set TC
                # T0[1*M.N_cell:2*M.N_cell] = self.TC_in - np.arange(M.N_cell)/float(M.N_cell)*(self.TC_in-self.TH_in)*0.25
                # set TH
                T0[0:M.N_cell] = self.TH_in
                # set TC
                T0[1*M.N_cell:2*M.N_cell] = self.TC_in
            else:
                # set TH
                T0[0:M.N_cell] = self.TH_in + np.arange(M.N_cell)/float(M.N_cell)*(self.TC_in-self.TH_in)*0.5
                # set TC
                T0[1*M.N_cell:2*M.N_cell] = self.TC_in - (1.-np.arange(M.N_cell)/float(M.N_cell))*(self.TC_in-self.TH_in)*0.5

            if M.n == 4:
                # set TWH
                T0[2*M.N_cell:3*M.N_cell] = np.ones(M.N_cell) * 0.5*(self.TH_in+self.TC_in)
                # set TWC
                T0[3*M.N_cell:4*M.N_cell] = np.ones(M.N_cell) * 0.5*(self.TH_in+self.TC_in)

            return T0
        else:
            return self.T0

    def get_T0(self, M):
        """
        Set initial temperature vector.

        function to set initial/old conditions for Temperature Vector
        Support vectors of 2*Ncell and 4*Ncell
        Inputs:
        M   - instance of Model Class
        """
        if not (M.n == 2 or M.n == 4):
            raise MyError('get_T0 only supports n=2 or n=4')
        if len(self.T0) is not 0:
            if M.n == 2:
                if len(self.T0) is not 2*M.N_cell:
                    raise MyError("in get_T0: length of T0 supplied doesn't match model. Len(T0) = 2 * N_cell expected")
            if M.n == 4:
                if len(self.T0) is not 4*M.N_cell:
                    raise MyError("in get_T0: length of T0 supplied doesn't match model. Len(T0) = 4 * N_cell expected")

        if (len(self.T0) is 0) or (len(self.Q0) is 0):
            T0 = np.zeros((M.n*M.N_cell))
            Q0 = np.zeros((M.n*M.N_cell))
            if M.co_flow:  # for co-flow simple flat temperature profiles can be set.
                if M.n == 2:
                    # set TH
                    T0[0:M.N_cell] = self.TH_in
                    Q0[0:M.N_cell] = self.QH_in
                    # set delta_T
                    T0[1*M.N_cell:2*M.N_cell] = self.TH_in - self.TC_in
                    Q0[1*M.N_cell:2*M.N_cell] = self.QC_in
                elif M.n == 4:
                    # set TH
                    T0[0:M.N_cell] = self.TH_in
                    # set delta_T0
                    T0[2*M.N_cell:3*M.N_cell] = 0.45*(self.TH_in - self.TC_in)
                    # set delta_T1
                    T0[3*M.N_cell:4*M.N_cell] = 0.1*(self.TH_in - self.TC_in)
                    # set delta_T2
                    T0[1*M.N_cell:2*M.N_cell] = 0.45*(self.TH_in - self.TC_in)

            else:  # for counter-flow temperature profile can be set based on assumed change in enthalpy.
                # estimate dT for both sides:
                QH = self.mdotH * (
                    CPCP.PropsSI('HMASS', 'P', self.PH_in, 'T', self.TH_in, self.fluidH)
                    - CPCP.PropsSI('HMASS', 'P', self.PH_out, 'T', self.TC_in, self.fluidH))
                QC = self.mdotC * (
                    CPCP.PropsSI('HMASS', 'P', self.PC_in, 'T', self.TC_in, self.fluidC) 
                    - CPCP.PropsSI('HMASS', 'P', self.PC_out, 'T', self.TH_in, self.fluidC))
                # get a conservative estimate of Q (i.e. use 0.5 * Q)
                Q_actual = min(abs(QH), abs(QC)) * 0.5
                hH_in = CPCP.PropsSI('HMASS', 'P', self.PH_in, 'T', self.TH_in, self.fluidH)
                hH_out = hH_in - Q_actual * np.sign(QH) / self.mdotH
                hC_in = CPCP.PropsSI('HMASS', 'P', self.PC_in, 'T', self.TC_in, self.fluidC)
                hC_out = hC_in - Q_actual * np.sign(QC) / self.mdotC

                # These np.arange functions should be linspaces for correct endpoint handling ###
                # set hH
                hH = hH_in + np.arange(M.N_cell)/float(M.N_cell)*(hH_out-hH_in)
                # set hC
                hC = hC_in - (1. - np.arange(M.N_cell)/float(M.N_cell)) * (hC_in-hC_out)

                # set TH and QH
                TH = np.zeros(M.N_cell)
                QH = np.zeros(M.N_cell)
                for i in range(len(hH)):
                    TH[i] = CPCP.PropsSI('T', 'HMASS', hH[i], 'P', self.PH_in, self.fluidH)
                    QH[i] = CPCP.PropsSI('Q', 'HMASS', hH[i], 'P', self.PH_in, self.fluidH)
                # set TC and QC
                TC = np.zeros(M.N_cell)
                QC = np.zeros(M.N_cell)
                for i in range(len(hC)):
                    TC[i] = CPCP.PropsSI('T', 'HMASS', hC[i], 'P', self.PC_in, self.fluidC)
                    QC[i] = CPCP.PropsSI('Q', 'HMASS', hC[i], 'P', self.PC_in, self.fluidC)

                if M.n == 2:
                    # combine into temperature and Q vector
                    T0[0:M.N_cell] = TH
                    T0[1*M.N_cell:2*M.N_cell] = TH-TC  # store delta T to ensure problem is well conditioned
                    Q0[0:M.N_cell] = QH
                    Q0[1*M.N_cell:2*M.N_cell] = QC
                elif M.n == 4:
                    THw = 0.55 * TH + 0.45 * TC
                    TCw = 0.45 * TH + 0.55 * TC

                    QHw = np.zeros(M.N_cell)
                    for i in range(len(THw)):
                        QHw[i] = CPCP.PropsSI('Q', 'T', THw[i], 'P', self.PH_in, self.fluidH)
                    QCw = np.zeros(M.N_cell)
                    for i in range(len(TCw)):
                        QCw[i] = CPCP.PropsSI('Q', 'T', TCw[i], 'P', self.PC_in, self.fluidC)

                    # combine into temperature and Q vector
                    T0[0:M.N_cell] = TH
                    T0[2*M.N_cell:3*M.N_cell] = TH - THw
                    T0[3*M.N_cell:4*M.N_cell] = THw - TCw
                    T0[1*M.N_cell:2*M.N_cell] = TCw - TC

                    """
                    # set TH
                    TH = self.TH_in + np.arange(M.N_cell)/float(M.N_cell)*(self.TC_in-self.TH_in)*0.5
                    # set TC
                    TC = self.TC_in - (1.-np.arange(M.N_cell)/float(M.N_cell))*(self.TC_in-self.TH_in)*0.5
                    # set delta_T
                    delta_T = TH-TC
                    # set TH
                    T0[0:M.N_cell] = TH
                    # set delta_T0
                    T0[2*M.N_cell:3*M.N_cell] = 0.45*delta_T
                    # set delta_T1
                    T0[3*M.N_cell:4*M.N_cell] = 0.1*delta_T
                    # set delta_T2
                    T0[1*M.N_cell:2*M.N_cell] = 0.45*delta_T
                    """

                    # set quality vector
                    Q0[0:M.N_cell] = QH
                    Q0[2*M.N_cell:3*M.N_cell] = QHw
                    Q0[3*M.N_cell:4*M.N_cell] = QCw
                    Q0[1*M.N_cell:2*M.N_cell] = QC

            if M.verbosity > 1:
                print("Initialised vectors for starting conditions")
                print('T0:', T0)
                print('Q0:', Q0)
                print('TH:', TH)
                # print('THw:',THw)
                # print('TCw:',TCw)
                print('TC:', TC)

            return T0, Q0
        else:
            return self.T0, self.Q0

    def get_HT0(self, M):
        """
        Get thermodynamic state at start.

        Function to set initial conditions for thermodynamic state vector.
        Support vectors of 2*Ncell and 4*Ncell
        Inputs:
        M   - instance of model class
        Output:
        if M.n == 2
            HT = [hH, hC]
        if M.n == 4
            HT = [hH, hC, TW, TWd]
        """
        if not (M.n == 2 or M.n == 4):
            raise MyError('get_T0 only supports n=2 or n=4')
        if len(self.HT0) is not 0:
            if M.n == 2:
                if len(self.HT0) is not 2*M.N_cell:
                    raise MyError("in get_HT0: length of HT0 supplied doesn't match model."
                                  "Len(HT0) = 2 * N_cell expected")
            if M.n == 4:
                if len(self.T0) is not 4*M.N_cell:
                    raise MyError("in get_HT0: length of HT0 supplied doesn't match model."
                                  "Len(HT0) = 4 * N_cell expected")

        if (len(self.HT0) is 0):
            HT0 = np.zeros((M.n*M.N_cell))
            TH = np.zeros(M.N_cell)
            TC = np.zeros(M.N_cell)
            TW = np.zeros(M.N_cell)
            TWd = np.zeros(M.N_cell)
            hH = np.zeros(M.N_cell)
            hC = np.zeros(M.N_cell)

            if M.co_flow:
                # for co-flow, set simple flat temperature profiles
                if M.n == 2:
                    hH[:] = self.hH_in
                    hC[:] = self.hH_in
                elif M.n == 4:
                    hH[:] = self.hH_in
                    hC[:] = self.hC_in
                    # set TH
                    TH[:] = self.TH_in
                    TC[:] = self.TC_in
                    TW = 0.5 * TH + 0.5 * TC
                    TWd = 0.05 * (TH - TC)

                self.Q_start = 0.

            else:
                # for counter flow, set temperature profile based on assumed enthalpy change
                # estimate dT for both sides

                # check that fluid temperature range is not being exceeded
                if GetFluidPropLow(EosC, [CP.HmassP_INPUTS, self.hC_in, self.PC_in], [CP.iT_min]) is None:
                    print("Pressure (Pa):", self.PC_in, "Temperature (K):", self.TC_in)
                    raise MyError("C-stream exceeds the range of Equation of State. Bailing Out.")
                else:
                    TminC = GetFluidPropLow(EosC, [CP.HmassP_INPUTS, self.hC_in, self.PC_in], [CP.iT_min])
                
                if GetFluidPropLow(EosH, [CP.HmassP_INPUTS, self.hH_in, self.PH_in], [CP.iT_min]) is None:
                    print("Pressure (Pa):", self.PH_in, "Temperature (K):", self.TH_in)
                    raise MyError("H-stream exceeds the range of Equation of State. Bailing Out.")
                    
                else:
                    TminH = GetFluidPropLow(EosH, [CP.HmassP_INPUTS, self.hH_in, self.PH_in], [CP.iT_min])

                props = [CP.iHmass]
                state = [CP.PT_INPUTS, self.PH_out, max(self.TC_in, TminH)]
                qH = self.mdotH * (self.hH_in - GetFluidPropLow(EosH, state, props))
                state = [CP.PT_INPUTS, self.PC_out, max(self.TH_in, TminC)]
                qC = self.mdotC * (self.hC_in - GetFluidPropLow(EosC, state, props))

                # get a conservative estimate of Q (i.e. use 0.5 * Q)
                q_actual = min(abs(qH), abs(qC)) * 0.5
                self.Q_start = q_actual
                hH_out = self.hH_in - q_actual * np.sign(qH) / self.mdotH
                hC_out = self.hC_in - q_actual * np.sign(qC) / self.mdotC

                # set hH
                hH = self.hH_in + np.arange(M.N_cell)/float(M.N_cell) * (hH_out-self.hH_in)
                # set hC
                hC = self.hC_in - (1. - np.arange(M.N_cell)/float(M.N_cell)) * (self.hC_in-hC_out)

                # set TH and QH
                for i in range(len(hH)):
                    state = [CP.HmassP_INPUTS, hH[i], self.PH_in]
                    props = [CP.iT]
                    TH[i] = GetFluidPropLow(EosH, state, props)
                    state = [CP.HmassP_INPUTS, hC[i], self.PC_in]
                    props = [CP.iT]
                    TC[i] = GetFluidPropLow(EosC, state, props)
                if M.n == 4:
                    TW = 0.5 * TH + 0.5 * TC
                    TWd = 0.1 * (TH - TC)

            # combine to create HT vector
            HT0[0:M.N_cell] = hH/1e6
            HT0[1*M.N_cell:2*M.N_cell] = hC/1e6

            if M.n == 4:
                # HT0[2*M.N_cell:3*M.N_cell] = TW
                # HT0[3*M.N_cell:4*M.N_cell] = TWd
                HT0[2*M.N_cell:3*M.N_cell] = 0.45
                HT0[3*M.N_cell:4*M.N_cell] = 0.1

            if M.verbosity > 1:
                print("Initialised vectors for starting conditions")
                print('hH:', hH)
                print('hC:', hC)
                print('TH:', TH)
                print('TW:', TW)
                print('TWd:', TWd)
                print('TC:', TC)

            self.hH_start = hH
            self.hC_start = hC

            return HT0
        else:
            return self.HT0

    def get_HTL0(self, M):
        """
        Get HTL0.

        Function to set initial conditions for constant cell-wise enthalpy
        change/adaptive grid method.
        Sets values for enthalpy, temperature, and discretisation vectors.

        Supports vectors of 4*N_cell only.
        Inputs:
        M   - instance of model class
        Output:
        HTL = [hH, hC, TW, TWd, CellLengths]
        """
        if not (M.n == 4):
            raise MyError('Only supports n=4 currently.')
        if len(self.HT0) is not 0:
            if M.n == 4:
                if len(self.T0) is not 4*M.N_cell:
                    raise MyError("In GetHTX0: length of HT0 supplied doesn't match model."
                                  "len(HT0) = 4*N_cell expected")

        # Initialise vectors
        HTL0 = np.zeros((M.n*M.N_cell))
        TH = np.zeros(M.N_cell)
        TC = np.zeros(M.N_cell)
        TW = np.zeros(M.N_cell)
        TWd = np.zeros(M.N_cell)
        hH = np.zeros(M.N_cell)
        hC = np.zeros(M.N_cell)

        # For co-flow, set flat temperature profiles.
        if M.co_flow:
            if M.n == 2:
                hH[:] = self.hH_in
                hC[:] = self.hH_in
            elif M.n == 4:
                hH[:] = self.hH_in
                hC[:] = self.hC_in
                # Set TH
                TH[:] = self.TH_in
                TC[:] = self.TC_in
                TW = 0.5 * TH + 0.5 * TC
                TWd = 0.05 * (TH - TC)

        # For counter-flow, set temperature profile based on assumed change in enthalpy
        else:
            # Estimate dT for both sides
            props = [CP.iHmass]
            state = [CP.PT_INPUTS, self.PH_out, self.TC_in]
            qH = self.mdotH * (self.hH_in - GetFluidPropLow(EosH, state, props))
            state = [CP.PT_INPUTS, self.PC_out, self.TH_in]
            qC = self.mdotC * (self.hC_in - GetFluidPropLow(EosC, state, props))

            # Get a conservative estimate of Q (i.e. use 0.5 * Q)
            q_actual = min(abs(qH), abs(qC)) * 0.5
            hH_out = self.hH_in - q_actual * np.sign(qH) / self.mdotH
            hC_out = self.hC_in - q_actual * np.sign(qC) / self.mdotC

            # Set hH
            hH = self.hH_in - np.linspace(1/M.N_cell, 1.0, M.N_cell) * (self.hH_in-hH_out)
            # Set hC
            hC = self.hC_in + np.linspace(1.0, 1/M.N_cell, M.N_cell) * (hC_out-self.hC_in)

            if M.n == 4:
                # Set TH and QH
                for i in range(len(hH)):
                    state = [CP.HmassP_INPUTS, hH[i], self.PH_in]
                    props = [CP.iT]
                    TH[i] = GetFluidPropLow(EosH, state, props)
                    state = [CP.HmassP_INPUTS, hC[i], self.PC_in]
                    props = [CP.iT]
                    TC[i] = GetFluidPropLow(EosC, state, props)
                TW = 0.5 * TH + 0.5 * TC
                TWd = 0.1 * (TH - TC)

        # Discretisation vector
        CellLengths = np.ones(M.N_cell-1)

        # Delta Q vector - assume zero to begin with
        DeltaQ = np.zeros(M.N_cell)

        # Combine to create HT vector
        HTL0[0] = q_actual
        HTL0[1:M.N_cell+1] = DeltaQ
        HTL0[M.N_cell+1:2*M.N_cell] = CellLengths

        if M.n == 4:
            HTL0[2*M.N_cell:3*M.N_cell] = 0.45
            HTL0[3*M.N_cell:4*M.N_cell] = 0.1

        if M.verbosity > 1:
            print("Initialised vectors for starting conditions")
            print('hH:', hH)
            print('hC:', hC)
            print('TH:', TH)
            print('TC:', TC)

        return HTL0


class Correlation:
    """Define Correlations used for modelling heat and mass transfer in HX."""

    def __init__(self, verbosity=0):
        """Initialise class."""
        self.verbosity = verbosity
        self.frictionCorrelation_Gas = None  # set friction factor correlation of gas phase.
        self.frictionCorrelation_Liquid = None  # set friction factor correlation of liquid phase.
        self.frictionCorrelation_Supercritical = None  # set friction factor correlation of supercritical phase.
        self.frictionCorrelation_2phase_condensing = None  # set friction factor correlation for condensing.
        self.frictionCorrelation_2phase_boiling = None  # set friction factor correlation for boiling.
        self.frictionPolynominal = False  # set pressure drop using a polynominal
        self.heatTransferCorrelation_Liquid = None  # set heat transfer correlation of liquid phase.
        self.heatTransferCorrelation_Gas = None  # set heat transfer correlation of gas phase.
        self.heatTransferCorrelation_Supercritical = None  # set heat transfer correlation of supercritical phase.
        self.heatTransferCorrelation_2phase_condensing = None  # set heat transfer correlation for condensing.
        self.heatTransferCorrelation_2phase_boiling = None  # set heat transfer correlation for boiling.

        self.frictionPolynominal_Coeff = None  # provide List of coefficients for p

    def check(self, G):
        """Check that settings are appropriate."""
        if self.frictionPolynominal is True:
            print('Performing polynominal based pressure drop calculation frictionPolynominal=True. \n'
                  'Other friction correlations set to None.')
            self.frictionCorrelation_Liquid = None  # set friction factor correlation of liquid phase.
            self.frictionCorrelation_Gas = None  # set friction factor correlation of gas phase.
            self.frictionCorrelation_Supercritical = None  # set friction factor correlation of supercritical phase.
            self.frictionCorrelation_2phase_condensing = None  # set friction factor correlation for condensing.
            self.frictionCorrelation_2phase_boiling = None  # set friction factor correlation for boiling.
        if self.frictionPolynominal is True:
            if self.frictionPolynominal_Coeff is None:
                raise MyError("When using frictionPolynominal=True, also need to set frictionPolynominal_Coeff=[a0, a1, ...]")
        # check pressure drop correlations are defined
        if self.frictionPolynominal is False and \
           self.frictionCorrelation_Liquid is None and self.frictionCorrelation_Gas is None and \
           self.frictionCorrelation_2phase_condensing is None and self.frictionCorrelation_2phase_boiling is None:
            print('No friction correlation has been set. Modelling zero pressure drop.')
        # check that correlations are available
        if self.frictionCorrelation_Gas is not None:
            if self.frictionCorrelation_Gas.upper() not in hx_f.get_Correlation_List('GAS'):
                raise MyError('frictionCorrelation_Gas={0} is not supported. Options are: {1}'.format(self.frictionCorrelation_Gas, hx_f.get_Correlation_List('GAS')))
        if self.frictionCorrelation_Liquid is not None:
            if self.frictionCorrelation_Liquid.upper() not in hx_f.get_Correlation_List('LIQUID'):
                raise MyError('frictionCorrelation_Liquid={0} is not supported. Options are: {1}'.format(self.frictionCorrelation_Liquid, hx_f.get_Correlation_List('LIQUID')))
        if self.frictionCorrelation_Supercritical is not None:
            if self.frictionCorrelation_Supercritical.upper() not in hx_f.get_Correlation_List('SUPERCRITICAL'):
                raise MyError('frictionCorrelation_Supercritical={0} is not supported. Options are: {1}'.format(self.frictionCorrelation_Supercritical, hx_f.get_Correlation_List('SUPERCRITICAL')))
        if self.frictionCorrelation_2phase_condensing is not None:
            if self.frictionCorrelation_2phase_condensing.upper() not in hx_f.get_Correlation_List('2PHASE_CONDENSING'):
                raise MyError('frictionCorrelation_2phase_condensing={0} is not supported. Options are: {1}'.format(self.frictionCorrelation_2phase_condensing, hx_f.get_Correlation_List('2PHASE_CONDENSING')))
        if self.frictionCorrelation_2phase_boiling is not None:
            if self.frictionCorrelation_2phase_boiling.upper() not in hx_f.get_Correlation_List('2PHASE_BOILING'):
                raise MyError('frictionCorrelation_2phase_boiling={0} is not supported. Options are: {1}'.format(self.frictionCorrelation_2phase_boiling, hx_f.get_Correlation_List('2PHASE_BOILING')))

        # check heat transfer correlations are defined
        if (G.HXtype in ['UA', 'pinch_dT', 'dT_map']):
            return  # don't need check for these types of HX
        else:
            if self.heatTransferCorrelation_Liquid is None and \
               self.heatTransferCorrelation_Gas is None and \
               self.heatTransferCorrelation_Supercritical is None and \
               self.heatTransferCorrelation_2phase_condensing is None and \
               self.heatTransferCorrelation_2phase_boiling is None:
                raise MyError('No heatTransferCorrelation has been set. Bailing Out.')
        # check that correlations are available
        if self.heatTransferCorrelation_Gas is not None:
            if self.heatTransferCorrelation_Gas.upper() not in hx_Nu.get_Correlation_List('GAS'):
                raise MyError('heatTransferCorrelation_Gas={0} is not supported. Options are: {1}'.format(self.heatTransferCorrelation_Gas, hx_Nu.get_Correlation_List('GAS')))
        if self.heatTransferCorrelation_Liquid is not None:
            if self.heatTransferCorrelation_Liquid.upper() not in hx_Nu.get_Correlation_List('LIQUID'):
                raise MyError('heatTransferCorrelation_Liquid={0} is not supported. Options are: {1}'.format(self.heatTransferCorrelation_Liquid, hx_Nu.get_Correlation_List('LIQUID')))
        if self.heatTransferCorrelation_Supercritical is not None:
            if self.heatTransferCorrelation_Supercritical.upper() not in hx_Nu.get_Correlation_List('SUPERCRITICAL'):
                raise MyError('heatTransferCorrelation_Supercritical={0} is not supported. Options are: {1}'.format(self.heatTransferCorrelation_Supercritical, hx_Nu.get_Correlation_List('SUPERCRITICAL')))
        if self.heatTransferCorrelation_2phase_condensing is not None:
            if self.heatTransferCorrelation_2phase_condensing.upper() not in hx_Nu.get_Correlation_List('2PHASE_CONDENSING'):
                raise MyError('frictionCorrelation_2phase_condensing={0} is not supported. Options are: {1}'.format(self.heatTransferCorrelation_2phase_condensing, hx_Nu.get_Correlation_List('2PHASE_CONDENSING')))
        if self.heatTransferCorrelation_2phase_boiling is not None:
            if self.heatTransferCorrelation_2phase_boiling.upper() not in hx_Nu.get_Correlation_List('2PHASE_BOILING'):
                raise MyError('frictionCorrelation_2phase_boiling={0} is not supported. Options are: {1}'.format(self.heatTransferCorrelation_2phase_boiling, hx_Nu.get_Correlation_List('2PHASE_BOILING')))

        # check that modelling approach supports use of corelations.
        if (G.HXtype in ['UA', 'pinch_dT', 'dT_map']) and self.frictionPolynominal is False:
            print('Heat Exchanger model G.HXtype={} does not support use of correlations. All correlations set to None.'.format(G.HXtype))
            self.frictionCorrelation_Gas = None  # set friction factor correlation of gas phase.
            self.frictionCorrelation_Liquid = None  # set friction factor correlation of liquid phase.
            self.frictionCorrelation_2phase_condensing = None  # set friction factor correlation for condensing.
            self.frictionCorrelation_2phase_boiling = None  # set friction factor correlation for boiling.
            self.heatTransferCorrelation_Liquid = None  # set heat transfer correlation of liquid phase.
            self.heatTransferCorrelation_Gas = None  # set heat transfer correlation of gas phase.
            self.heatTransferCorrelation_2phase_condensing = None  # set heat transfer correlation for condensing.
            self.heatTransferCorrelation_2phase_boiling = None  # set heat transfer correlation for boiling.

        if self.verbosity > 0:
            print('Check of Correlation input parameters: Passed')
        return 0

    def calc_HTC(self, enthalpy, pressure, quality, Twall, mdot, area,
                           L_characteristic, EOS, cond_boil_flag, CUSTOM=[]):
        """Calculate local Nusselt number."""
        # get nameList and correspodning list of function pointers
        nameList, correlationList = hx_Nu.get_Correlation_List('ALL')
        # evaluate which state is present
        enthalpy_bar = 0.5*(enthalpy[0] + enthalpy[1])
        pressure_bar = 0.5*(pressure[0] + pressure[1])
        temperature = GetFluidPropLow(EOS, [CP.HmassP_INPUTS, enthalpy_bar, pressure_bar], [CP.iT])
        enthalpy_ref = GetFluidPropLow(EOS, [CP.PQ_INPUTS, pressure_bar, 1], [CP.iHmass])

        if enthalpy_ref is None:
            state = 'SUPERCRITICAL'
            if self.heatTransferCorrelation_Supercritical is None:
                raise MyError('SUPERCRITICAL heat transfer correlation required.')
            else:
                function = correlationList[nameList.index(self.heatTransferCorrelation_Supercritical)]
        else:
            if enthalpy_bar > enthalpy_ref and quality < 0:
                state = 'GAS'
                if self.heatTransferCorrelation_Gas is None:
                    raise MyError('GAS heat transfer correlation required.')
                else:
                    function = correlationList[nameList.index(self.heatTransferCorrelation_Gas)]
            elif enthalpy_bar <= enthalpy_ref and quality < 0:
                state = 'LIQUID'
                if self.heatTransferCorrelation_Liquid is None:
                    raise MyError('LIQUID heat transfer correlation required.')
                else:
                    function = correlationList[nameList.index(self.heatTransferCorrelation_Liquid)]
            else:
                if cond_boil_flag < 0:
                    state = '2PHASE_BOILING'
                    if self.heatTransferCorrelation_2phase_boiling is None:
                        if self.verbosity > 0:
                            print("Warning {}, heat transfer correlation not available, switching to LIQUID.".format(state))
                        function = correlationList[nameList.index(self.heatTransferCorrelation_Liquid)]
                    else:
                        function = correlationList[nameList.index(self.heatTransferCorrelation_2phase_boiling)]
                        # if quality > 0.5:
                        #    print('Quality > 50% --> treat as gas')
                        #    function = correlationList[nameList.index(self.heatTransferCorrelation_Gas)]
                        # elif quality < 0.1:
                        #    print('Quality < 10% --> treat as liquid')
                        #    function = correlationList[nameList.index(self.heatTransferCorrelation_Liquid)]
                        # else:
                        #    function = correlationList[nameList.index(self.heatTransferCorrelation_2phase_boiling)]
                else:
                    state = '2PHASE_CONDENSING'
                    if self.heatTransferCorrelation_2phase_condensing is None:
                        if self.verbosity > 0:
                            print("Warning {}, heat transfer correlation not available, switching to LIQUID.".format(state))
                        function = correlationList[nameList.index(self.heatTransferCorrelation_Liquid)]
                    else:
                        function = correlationList[nameList.index(self.heatTransferCorrelation_2phase_condensing)]

        # now pass the variables to function that will be executed to calculate
        HTC = function.calculate_HTC(enthalpy, pressure, quality, Twall, mdot, area, L_characteristic, EOS, CUSTOM)

        if self.verbosity > 1:
            print('State, Temp[K], Pressure[MPa], Quality[-]:', state, temperature, pressure_bar/1e6, quality)
            print('Solving Nussselt HTC with {0}, HTC={1}'.format(function.name, HTC))
        return HTC

    def calc_frictionFactor(self, enthalpy, pressure, quality, Twall, mdot, area,
                            EOS, D_hydraulic, cond_boil_flag, epsilon=0):
        """Calculate local friction factor."""
        # get nameList and correspodning list of function pointers
        nameList, correlationList = hx_f.get_Correlation_List('ALL')
        # evaluate which state is present
        temperature = GetFluidPropLow(EOS, [CP.HmassP_INPUTS, enthalpy, pressure], [CP.iT])
        enthalpy_ref = GetFluidPropLow(EOS, [CP.PQ_INPUTS, pressure, 1], [CP.iHmass])
        if enthalpy_ref is None:
            state = 'SUPERCRITICAL'
            if self.frictionCorrelation_Supercritical is None:
                return 0
            else:
                function = correlationList[nameList.index(self.frictionCorrelation_Supercritical)]
        else:
            if enthalpy > enthalpy_ref and quality < 0:
                state = 'GAS'
                if self.frictionCorrelation_Gas is None:
                    return 0
                else:
                    function = correlationList[nameList.index(self.frictionCorrelation_Gas)]
            elif enthalpy <= enthalpy_ref and quality < 0:
                state = 'LIQUID'
                if self.frictionCorrelation_Liquid is None:
                    return 0
                else:
                    function = correlationList[nameList.index(self.frictionCorrelation_Liquid)]
            else:
                if cond_boil_flag < 0:
                    state = '2PHASE_BOILING'
                    if self.frictionCorrelation_2phase_condensing is None:
                        return 0
                    else:
                        function = correlationList[nameList.index(self.frictionCorrelation_2phase_condensing)]
                else:
                    state = '2PHASE_CONDENSING'
                    if self.frictionCorrelation_2phase_boiling is None:
                        return 0
                    else:
                        function = correlationList[nameList.index(self.frictionCorrelation_2phase_boiling)]

        if self.verbosity > 1:
            print('State:', state, temperature, pressure/1e6)
            print('Solving friction with {}'.format(function.name))
        # now pass the variables to function that will be executed to calculate
        f = function.calculate(enthalpy, pressure, quality, Twall, mdot, area, EOS, D_hydraulic, epsilon)

        return f

    def check_correlation_validity(self):
        """Check if final solution meets correlation limits."""
        print('TODO: Correlation check not yet implemented.')
        return 0

    def includeCustom_check(self):
        """Check if custom correlation is used."""
        customList = ['CUSTOM_1', 'CUSTOM_2']
        if self.heatTransferCorrelation_Liquid in customList or self.heatTransferCorrelation_Gas in customList or \
           self.heatTransferCorrelation_2phase_condensing in customList or self.heatTransferCorrelation_2phase_boiling in customList:
           return True
        else:
            return False


class Model:
    """Defione Modelling parameters."""

    def __init__(self):
        """Initialise class."""
        self.optim = 'root:hybr'
        self.label = 'HX'
        self.N_cell = []  # number of cells
        self.co_flow = 0  # default is to analyse counterflow heat exchangers. Set to 1 for co flow
        self.flag_axial = []  # whether axial heat conduction is considered
        self.external_loss = []  # whether external heat loss is considered
        #self.Nu_CorrelationH = []  # set correlation for heat transfer in H channel
        #self.Nu_CorrelationC = []  # set correlation for heat transfer in C channel
        #self.f_CorrelationH = None  # set correlation for friction factor in H channel
        #self.f_CorrelationC = None  # set correlation for friction factor in C channel
        self.H_dP_poly = []  # polynominal coefficients for pressure drop in H channel
        self.C_dP_poly = []  # polynominal coefficients for pressure drop in H channel
        self.n = 4  # switch between numbers of temperatures used to model heat exchanger
        self.LM = 0  # allows switching between linear and log mean temperature difference for heat transfer calcs.
        self.set_pinch = 0  # allows the code to be operated to adjust HX parameters to achieve target pinch point temperature difference.
        self.flag_adaptive_grid = 0  # computes cell sizes according to local enthalpy change
        self.flag_constant_delta_h = 0  # solves using approx constant enthalpy change per cell and variable cell sizes

    def check(self):
        """Check class has been set correctly."""
        if not self.N_cell:
            raise MyError('M.N_cell not specified')
        if not self.flag_axial:
            self.flag_axial = 0
            print('M.flag_axial no specified defaulting to 0')
        '''
        if not self.Nu_CorrelationH:
            self.Nu_CorrelationH = 1
            print('Nu_CorrelationH not specified and set to default Nu_CorrelationH=1')
        if not self.Nu_CorrelationC:
            self.Nu_CorrelationC = self.Nu_CorrelationH
            print('Nu_CorrelationC not specified and set to equal Nu_CorrelationH')
        if self.f_CorrelationH is None:
            self.f_CorrelationH = 0
            print('f_CorrelationH not specified and set to default f_CorrelationH=0')
        if self.f_CorrelationC is None:
            self.f_CorrelationC = self.f_CorrelationH
            print('f_CorrelationC not specified and set to equal f_CorrelationH')
        '''
        if not (self.co_flow == 0 or self.co_flow == 1):
            raise MyError('M.co_flow not defined correctly. Set to 0 or 1')
        '''
        if len(self.H_dP_poly) > 0 and self.f_CorrelationH != 0:
            raise MyError('Pressure drop polynominal H_dP_poly can only be used in conjunction with f_CorrelationH = 0')
        if len(self.C_dP_poly) > 0 and self.f_CorrelationC != 0:
            raise MyError('Pressure drop polynominal C_dP_poly can only be used in conjunction with f_CorrelationC = 0')
        '''
        #
        if self.verbosity > 0:
            print('Check of Model input parameters: Passed')

    def set_poly(self, F):
        """Set up polynominal for outlet pressure."""
        # use polynominal to update outlet pressure
        if len(self.H_dP_poly) > 0:
            temp = []
            for a in range(len(self.H_dP_poly)):
                temp.append(self.H_dP_poly*F.mdotC**a)
            F.PC_out = sum(temp)

        if len(self.C_dP_poly) > 0:  # if polynominal is specified use this to calc PH_out
            temp = []
            for a in range(len(self.C_dP_poly)):
                temp.append(self.C_dP_poly*F.mdotC**a)
            F.PC_out = sum(temp)


class SolverManager:
    """
    Class to manage solver.
    Class containing root finder/optimiser solution data.
    Used to manage the execution flow of the code.
    """

    def __init__(self):
        """Initialise class."""
        # FIX: contain all solution information inside this class for tracking
        # self.X = hH,hC,TWH,etc
        self.CellLengths = []  # Vector containing normalised cell sizes. Must sum to 1.
        self.CellLengthsPrev = []  # Vector containing last step normalised cell sizes
        self.nIterations = 0  # Counts total iterations of root finder/optimiser
        self.SolverStage = "solve"  # Used to switch between preprocessing, solving, and postprocessing

    def CheckAndInitialise(self, M):
        """Check class has been set correctly."""
        self.CellLengths = np.ones(M.N_cell)/M.N_cell


class Geometry:
    """Class containing heat exchanger geometry."""

    def __init__(self):
        """Initialise class."""
        self.HXtype = []
        self.k_wall = []  # thermal conductivity (W / mk)
        self.HX_L = []
        self.dT = []
        self.mdotH = None
        self.mdotC = None
        self.Q_dT = None
        self.type_list = ['micro-channel', 'shell-tube', 'plate-shell', 'UA',
                          'UA_dT', 'pinch_dT', 'dT_map']

    def check_initialise(self, M):
        """Check class has been set correctly."""
        if not self.HXtype:
            raise MyError('G.HXtype not specified')
        if not any(self.HXtype in s for s in self.type_list):
            raise MyError('Specified type in G.HXtype not supported. Use:'+str(self.type_list))

        # look at microchannel case
        if self.HXtype == 'micro-channel':
            self.micro_check()
            self.micro_init(M)
        #
        if self.HXtype == 'shell-tube':
            self.shell_tube_check()
            self.shell_tube_init(M)
        #
        if self.HXtype == 'plate-shell':
            self.plate_shell_check()
            self.plate_shell_init(M)
        #
        if self.HXtype == 'UA':
            self.UA_based_check()
            self.UA_based_init(M)
        #
        if self.HXtype == 'UA_dT':
            self.UA_based_check()
            self.UA_based_init(M)
        #
        if self.HXtype == 'pinch_dT':
            self.pinch_dT_check()
            self.pinch_dT_init(M)
        #
        if self.HXtype == 'dT_map':
            self.dT_map_check()
            self.dT_map_init(M)
        #
        if M.verbosity > 0:
            print('Check of Geometry input parameters: Passed')

    def update(self, flag, value, fname, alpha=0.):
        """Update heat exchanger."""
        # adjust to UA
        if flag == "UA":
            value_old = self.UA
            value_new = alpha*value_old + (1.-alpha)*value
            flag = "U"
            value = value_new/self.Area
        # read current file and replace line
        lines = []
        label_flag = 0
        with open(fname, 'r') as infile:
            for line in infile:
                if ("G."+flag) in line:
                    line = "G."+flag+" = "+str(value)+" \n"
                lines.append(line)
        # write lines to new output file.
        with open(fname, 'w') as outfile:
            for line in lines:
                outfile.write(line)

    def micro_check(self):
        """Check that micro-channel HX has been defined correctly."""
        if not self.N_R:
            raise MyError('G.N_R not specified')
        if not self.N_C:
            raise MyError('G.N_C not specified')
        if not self.t_1:
            raise MyError('G.t_1 not specified')
        if not self.t_2:
            raise MyError('G.t_2 not specified')
        if not self.t_casing:
            raise MyError('G.t_casing not specified')
        if not self.HX_L:
            raise MyError('G.HX_L not specified')
        if not self.d_tube:
            raise MyError('G.d_tube not specified')
        if not self.k_wall:
            raise MyError('G.k_wall not specified')

    def micro_init(self, M):
        """Initialise micro-channel HX."""
        self.Area = self.N_C * self.N_R * np.pi * self.d_tube * self.HX_L
        self.AreaXWall = self.N_C * self.N_R * (self.d_tube + self.t_2) * self.HX_L

        self.AH = np.ones(M.N_cell)*self.N_R*self.N_C*self.d_tube**2/4*np.pi  # total flow area (m2)
        self.Lc_H = self.d_tube  # characteristic length (m)
        self.Dh_H = np.ones(M.N_cell)*self.d_tube
        self.AC = np.ones(M.N_cell)*self.N_R*self.N_C*self.d_tube**2/4*np.pi  # total flow area (m2)
        self.Lc_C = self.d_tube  # characteristic length (m)
        self.Dh_C = np.ones(M.N_cell)*self.d_tube

        self.t_wall = self.t_1  # (m)

        L1 = self.N_C*self.d_tube + (self.N_C-1)*self.t_2 + 2*self.t_casing
        L2 = 2*self.N_R*self.d_tube + (2*self.N_R-1)*self.t_1 + 2*self.t_casing
        self.A_wall = np.ones(M.N_cell) * (L1*L2 - self.AH - self.AC)

        self.L_wall = self.HX_L

    def shell_tube_check(self):
        """Check that shell-tube HX has been defined correctly."""
        if not self.N_T:
            raise MyError('G.N_T not specified')
        if not self.d:
            raise MyError('G.d not specified')
        if not self.D:
            raise MyError('G.D not specified')
        if not self.DD:
            raise MyError('G.DD not specified')
        if not self.t_casing:
            raise MyError('G.t_casing not specified')
        if not self.HX_L:
            raise MyError('G.HX_L not specified')
        if not self.k_wall:
            raise MyError('G.k_wall not specified')

    def shell_tube_init(self, M):
        """Initialise shell-tube HX."""
        self.Area = 0.5*(self.d + self.D) * np.pi * self.HX_L * self.N_T
        self.AreaXWall = self.Area

        self.AH = np.ones(M.N_cell)*self.N_T*self.d**2/4*np.pi  # total flow area (m2)
        self.Lc_H = self.d  # characteristic length (m)
        self.Dh_H = np.ones(M.N_cell)*self.d  # hydraulic diameter
        self.AC = np.ones(M.N_cell)*(self.DD**2/4*np.pi-self.N_T*self.D**2/4.*np.pi)  # total flow area (m2)
        self.Lc_C = self.D  # characteristic length (m)
        Perimeter = self.DD * np.pi + self.N_T * self.D*np.pi
        self.Dh_C = 4. * self.AC / Perimeter  # hydraulic diameter

        self.t_wall = (self.D-self.d)/2.  # (m)

        self.A_wall = np.ones(M.N_cell) \
            * (self.DD*np.pi*self.t_wall + self.N_T * (self.D**2 - self.d**2) / 4.*np.pi)

        self.L_wall = self.HX_L

    def plate_shell_check(self):
        """Check that plate-shell HX has been defined correctly."""
        if not self.n_plates:
            raise MyError('G.n_plates not specified')
        if not self.d_plate:
            raise MyError('d_plate not specified')
        if not self.t_plate:
            raise MyError('t_plate not specified')
        # if not self.Dh:
            # raise MyError('Dh not specified')
        if not self.plate_sep_shell:
            raise MyError('plate_sep_shell not specified')
        if not self.plate_sep_plate:
            raise MyError('plate_sep_plate not specified')
        if not self.t_casing:
            raise MyError('G.t_casing not specified')
        if not self.k_wall:
            raise MyError('G.k_wall not specified')
        if self.cold_stream not in ['plate', 'shell']:
            raise MyError("G.cold_stream not specified correctly \
                          (use 'plate' or 'shell')")
        if self.hot_stream not in ['plate', 'shell']:
            raise MyError("G.hot_stream not specified correctly \
                          (use 'plate' or 'shell' - case sensitive)")

    def plate_shell_init(self, M):
        """
        Initialise shell-plate HX.

        Calculates the following parameters.
        Area (effective heat transfer area)
        A_wall (heat transfer area)
        Dh_X (hydraulic diameter of both channels)
        Lc_X (characteristic length of both channels)
        t_wall (wall thickness)
        t_casing (casing  thickness)
        k_wall (wall thermal conductivity)
        AX (flow / cross-sectional area of both channels)
        """
        self.t_wall = self.t_plate
        self.r_plate = self.d_plate/2
        self.Area = self.n_plates*np.pi*self.r_plate**2
        self.AreaXWall = self.Area

        self.L_wall = self.d_plate
        self.HX_L = self.d_plate

        # Express hydraulic diameter in terms of plate diameter to fix
        # conditioning of optimisation problem
        self.Dh_H = np.ones(M.N_cell)*self.d_plate*0.028  # Constant from d_plate ~= 0., Dh ~= 0.008
        self.Dh_C = np.ones(M.N_cell)*self.d_plate*0.028

        self.Lc_H = self.d_plate
        self.Lc_C = self.d_plate

        # Compute flow areas
        self.chord_lengths = GetPsheChordLengths(self.r_plate, M.N_cell)
        self.flow_area_shell = (self.n_plates/2+1)*self.plate_sep_shell*self.chord_lengths
        self.flow_area_plate = (self.n_plates/2)*self.plate_sep_plate*self.chord_lengths
        self.A_wall = np.ones(M.N_cell)*(self.n_plates)*self.t_wall*self.chord_lengths

        # Assign to plate and shell flow areas to specified HX sides
        self.AC = eval('self.flow_area_'+self.cold_stream)
        self.AH = eval('self.flow_area_'+self.hot_stream)

    def UA_based_check(self):
        """Check that UA-based HX has been defined correctly."""
        if not self.Area:
            raise MyError('G.Area not specified')  # User specify effetive area
        if not self.U:
            raise MyError('G.U not specified')
        if self.HXtype == 'UA_dT':
            if not self.dT:
                raise MyError('G.dT not specified')
        if not self.AH:
            self.AH = 0.
            print("G.AH not not specified, set to default = 0")
        if not self.AC:
            self.AC = 0.
            print("G.AC not not specified, set to default = 0")
        # if not self.Dh_H:
        #    raise MyError('G.Dh_H not specified')
        # if not self.Dh_C:
        #    raise MyError('G.Dh_C not specified')
        if not self.HX_L:
            self.HX_L = 1.
            print('G.HX_L not specified, set to default = 1.')
        # if not self.k_wall:
        #    raise MyError('G.k_wall not specified')

    def UA_based_init(self, M):
        """Initialise UA-based HX."""
        M.n = 2  # set that only two temperatures are modelled
        f_CorrelationH = 0
        f_CorrelationC = 0
        self.UA = self.U * self.Area
        self.AH = np.ones(M.N_cell)*self.AH  # Used to claculate conduction in axial directon
        self.AC = np.ones(M.N_cell)*self.AC  # Used to claculate conduction in axial directon
        self.L_wall = self.HX_L  # Used to claculate conduction in axial directon

    def pinch_dT_check(self):
        """Check if pinch dT has been set."""
        if not self.dT:
            raise MyError('G.dT not specified')
        if not self.HX_L:
            self.HX_L = 1.
            print('G.HX_L not specified, set to default = 1.')

    def pinch_dT_init(self, M):
        """Initialise pinch_dT."""
        M.n = 2  # set that only two temperatures are modelled
        f_CorrelationH = 0
        f_CorrelationC = 0
        """
        # prepare settings for fixed pinch point modelling.
        if M.set_pinch == 0:
            0 # do nothing
            print("Performing HX calculation using fixed U and A")
        elif M.set_pinch == 1:
            print("Performing HX calculation and adjusting UA to achieve a target pinch point temperature difference")
            try:
                self.dT
            except:
                raise MyError('G.dT must be defined when using M.set_pinch = 1')
        elif M.set_pinch == 2:
            print('Performing HX calculation based purely on enthalpy balance to achieve target pinch point temperature differnece')
            try:
                self.dT
            except:
                raise MyError('G.dT must be defined when using M.set_pinch = 1')
        else:
            raise MyError('Setting for M.set_pinch not supported. Select one of the following 0 (default), 1, 2')
        """

    def dT_map_check(self):
        """Check dT_map HX has been defined correctly."""
        if self.mdotH is None:
            raise MyError('G.mdotH not specified')
        if self.mdotC is None:
            raise MyError('G.mdotC not specified')
        if self.Q_dT is None:
            raise MyError('G.Q_dT not specified')
        if not self.HX_L:
            self.HX_L = 1.
            print('G.HX_L not specified, set to default = 1.')

    def dT_map_init(self, M):
        """Initialise dt_map HX."""
        M.n = 2  # set that only two temperatures are modelled
        f_CorrelationH = 0
        f_CorrelationC = 0
        # create interpolant
        try:
            self.Q_dT_interp = interpolate.interp2d(self.mdotH, self.mdotC, self.Q_dT, kind='cubic')
        except:
            print("Failed to construct interpolant for Q_dT")
            print("Length of G.mdotH = {0}, should match Columns in G.Q_dT = {1}"
                  .format(len(self.mdotH), len(self.Q_dT[0])))
            print("Length of G.mdotC = {0}, should match Rows in    G.Q_dT = {1}"
                  .format(len(self.mdotC), len(self.Q_dT)))
            raise MyError("Unable to construct interpolant. Bailing out.")

def GetPsheChordLengths(R_PLATE, N_CELLS):
    """
    Computes chord lengths and hence cross-sectional flow areas of a plate and shell heat exchanger.

    VB 02-05-17
    """
    def Area(theta1, theta2, R_PLATE):
        return R_PLATE**2*((theta2+np.sin(theta2)*np.cos(theta2))
                           - (theta1+np.sin(theta1)*np.cos(theta1)))

    def FTheta2(theta2, *args):
        theta1, R_PLATE, N_CELLS = args
        return np.pi*R_PLATE**2/N_CELLS - Area(theta1, theta2, R_PLATE)

    def GetTheta2(theta1, R_PLATE, N_CELLS):
        PARAMETERS = (theta1, R_PLATE, N_CELLS)
        theta20 = theta1+2*R_PLATE/N_CELLS
        return optimize.fsolve(FTheta2, theta20, args=PARAMETERS, xtol=1e-8)

    def GetChordLength(theta, R_PLATE):
        return 2*R_PLATE*np.cos(theta)

    def GetMidChordLength(theta1, theta2, R_PLATE):
        midZ = 0.5*R_PLATE*(np.sin(theta1)+np.sin(theta2))
        midTheta = np.arcsin(midZ/R_PLATE)
        return 2*R_PLATE*np.cos(midTheta)

    def Check():
        """Compute area of circle with radius 1 to test integration function."""
        theta1 = -np.pi/2
        theta2 = np.pi/2
        testArea = Area(theta1, theta2, R_TEST)

    # Integrate over circle to compute angles and chord lengths
    CHORD_LENGTHS = np.zeros(N_CELLS)
    theta1 = -np.pi/2  # Starting angle
    for i in range(N_CELLS):
        theta2 = GetTheta2(theta1, R_PLATE, N_CELLS)
        CHORD_LENGTHS[i] = GetMidChordLength(theta1, theta2, R_PLATE)
        theta1 = theta2

    return CHORD_LENGTHS
###
###
class NuCorrelation:
    """
    Parameters for Nusselt number correlation. Allows users to specify their own
    correlation.
    Correlation takes the form A*Re**alpha*Pr*beta Class is only
    instantiated if M.Nu_CorrelationH and/or M.Nu_CorrelationC are set to 25 or
    greater.
    """
    def __init__(self):
        """Initialise class."""
        self.SC_A = []
        self.SC_B = []
        self.SC_C = []
        self.SC_D = []
        self.SC_E = []
        self.SC_F = []
        self.LIQ_A = []
        self.LIQ_B = []
        self.LIQ_C = []
        self.LIQ_D = []
        self.ALPHA = []
        self.BETA = []
        self.GAMMA = []

    def check(self):
        """Check that class has been defined correctly."""
        # FIX: need to revise this section
        # if not self.SC_A:
        #   raise MyError('N.SC_A not specified')
        # if not self.SC_B:
        #   raise MyError('N.SC_B not specified')
        # if not self.SC_C:
        #   raise MyError('N.SC_C not specified')
        # if not self.SC_D:
        #   raise MyError('N.SC_D not specified')
        # if not self.SC_E:
        #   raise MyError('N.SC_E not specified')
        # if not self.SC_F:
        #   raise MyError('N.SC_F not specified')
        # if not self.LIQ_A:
        #   raise MyError('N.LIQ_A not specified')
        # if not self.LIQ_B:
        #   raise MyError('N.LIQ_B not specified')
        # if not self.LIQ_C:
        #   raise MyError('N.LIQ_C not specified')
        # if not self.LIQ_D:
        #   raise MyError('N.LIQ_D not specified')
        if not self.ALPHA:
            raise MyError('N.ALPHA not specified')
        if not self.BETA:
            self.BETA = self.ALPHA
            print('Nu.BETA not specified. Set equal to Nu.ALPHA')
        # if not self.GAMMA:
        # if 'GAMMA' not in self.__dict__.keys(): # this check allows for parameters to have a 0.0 value
            # raise MyError('N.GAMMA not specified')
        if self.ALPHA < 0.0:
            raise MyError('N.ALPHA must be positive')
        if self.BETA < 0.0:
            raise MyError('N.BETA must be positive')


def Hequations(Q, M, G, F, N, S, C_hot, C_cold, flag=0):
    """
    Calculate temperature based on constant delta_H.
    
    function that temperature based on constant delta_H
    Inputs:
    Q - guess of exchanged energy
    M - class containing model parameters
    G - class containing geometry parameters
    F - class containing fluid boundary conditions
    flag - allows operation fo function to be altered
            0 - default for operation
            1 - output temperature and heat fluxes
            2 - returns pressure traces
    Outputs:
    error - error in pinch point
    """
    if M.verbosity > 1:
        print("\n")
        print("+++++++++++++++++++++++++")
        print("+++ New Solution Loop +++")
        print('Estimated Heat Flow Q', Q)

    TH = np.zeros(M.N_cell+1)
    TC = np.zeros(M.N_cell+1)
    hH = np.zeros(M.N_cell+1)
    hC = np.zeros(M.N_cell+1)
    TH[0] = F.TH_in
    PH = np.linspace(F.PH_in, F.PH_out, M.N_cell+1)
    # hH[0] = GetFluidPropLow(EosH,[CP.PT_INPUTS,PH[0],TH[0]],[CP.iHmass])
    hH[0] = F.hH_in

    if M.co_flow == 1:
        TC[0] = F.TC_in
        PC = np.linspace(F.PC_in, F.PC_out, M.N_cell+1)
        # hC[0] = GetFluidPropLow(EosC,[CP.PT_INPUTS,PC[0],TC[0]],[CP.iHmass])
        hC[0] = F.hC_in
    else:
        TC[M.N_cell] = F.TC_in
        PC = np.linspace(F.PC_out, F.PC_in, M.N_cell+1)
        # hC[M.N_cell] = GetFluidPropLow(EosC,[CP.PT_INPUTS,PC[M.N_cell],TC[M.N_cell]],[CP.iHmass])
        hC[M.N_cell] = F.hC_in

    if TH[0] > TC[0]:
        sign = 1
    else:
        sign = -1

    dH = Q[0] / M.N_cell

    for i in range(M.N_cell):
        hH[i+1] = (hH[i] - sign * dH / F.mdotH)
        TH[i+1] = GetFluidPropLow(EosH, [CP.HmassP_INPUTS, hH[i+1], PH[i+1]], [CP.iT])
        if M.co_flow == 1:
            hC[i+1] = hC[i] + sign * dH / (-1*F.mdotC)
            TC[i+1] = GetFluidPropLow(EosC, [CP.HmassP_INPUTS, hC[i+1], PC[i+1]], [CP.iT])
        else:
            hC[M.N_cell-1-i] = hC[M.N_cell-i] + sign * dH / F.mdotC
            TC[M.N_cell-1-i] = GetFluidPropLow(EosC, [CP.HmassP_INPUTS, hC[M.N_cell-1-i], PC[M.N_cell-1-i]], [CP.iT])

    if M.verbosity > 1:
        print('HH', hH)
        print('HC', hC)
        print('TH', TH)
        print('TC', TC)

    dT = []
    for i in range(len(TH)):
        dT.append(TH[i] - TC[i])

    if M.verbosity > 1:
        print('dT', dT)
        print('dT pinch:', min(dT))

    # TH,TC,TWH,TWC= open_T2(T,F.TH_in,F.TC_in,F.PH_in,F.PC_in,M,F)
    # TWH = 0.5 * (TH[1:] + TH[0:-1])
    # TWC = 0.5 * (TC[1:] + TC[0:-1])
    # print('TWH',TWH)
    # print('TWC',TWC)

    # print('Temperature',TH,TWH,TC)
    if flag is 1:
        Q1 = np.zeros(M.N_cell)
        Q2 = np.zeros(M.N_cell)
        Q5 = np.zeros(M.N_cell)
        Q6 = np.zeros(M.N_cell)
        Q7 = np.zeros(M.N_cell)
        Q8 = np.zeros(M.N_cell)
        Q3 = np.ones(M.N_cell)*dH
        Q4 = Q3

    error = (min(dT) - G.dT)**2

    """
    # assemble output T vector
    T = TH[1:]
    if M.co_flow == 1:
        T = np.append(T, dT[1:])
    else:
        T = np.append(T, dT)

    T=np.array(T)
    """
    # assemble HT vector
    HT = hH[1:]
    if M.co_flow == 1:
        HT = np.append(HT, hC[1:])
    else:
        HT = np.append(HT, hC[:-1])
    HT = np.array(HT)

    if M.verbosity > 1:
        print('HT:', HT)

    if flag is 0:
        return error
    elif flag is 1:
        return error, HT, Q1, Q2, Q3, Q4, Q5, Q6, Q7, Q8
    elif flag is 2:
        TWH = []
        TWC = []
        # calculate quality
        QH = np.zeros(M.N_cell)
        QC = np.zeros(M.N_cell)
        for i in range(len(hH[1:])):
            QH[i] = GetFluidPropLow(EosH, [CP.HmassP_INPUTS, hH[i+1], PH[i+1]], [CP.iQ])
            QC[i] = GetFluidPropLow(EosC, [CP.HmassP_INPUTS, hC[i], PC[i+1]], [CP.iQ])
        return TH, TC, PH, PC, QH, QC, hH, hC, TWH, TWC
    else:
        raise MyError('flag option not defined.')


def equations(HT, M, G, F, N, S, C_hot, C_cold, flag=0):
    """
    Solve steady state energy balance for each cell.

    FIX - I think HT should now be called 'X' because its contents now change
    depending on mode of operation VB.

    Inputs:
    HT- vector containing thermodynamic states in each cell
    M - class containing model parameters
    G - class containing geometry parameters
    F - class containing fluid boundary conditions
    S - class containing solver data
    C_hot - class containting correlations for hot channel
    C_cold - class containing correlatuons for cold channel
    flag - allows operation of function to be altered
            0 - default for operation
            1 - output temperature and heat fluxes
            2 - returns pressure traces
    Outputs:
    error - vector containing misbalance in energy equations in each cell
    """
    # Switching logic between adaptive and fixed grid
    if M.flag_constant_delta_h:
        print('Using adaptive grid.')
        hH, hC, TWH_ratio, TWC_ratio, CellLengths = \
            OpenHtl(HT, F.hH_in, F.hC_in, F.TH_in, F.TC_in, F.PH_in, F.PC_in, M, F)
        # Write cell lengths to solver manager object
        S.CellLengthsPrev = S.CellLengths
        S.CellLengths = CellLengths
        # Compute change in cell length vector, used to switch modes
        DeltaCellLengths = S.CellLengths-S.CellLengthsPrev

        # Logic to switch between preprocessing and solution modes
        if S.SolverStage.lower() == 'preprocess' and sum(DeltaCellLengths) > 1e-16:
            S.SolverStage = 'pending'

        if S.SolverStage.lower() == 'pending' and sum(DeltaCellLengths) == 0.0:
            S.SolverStage = 'solve'
            M.flag_constant_delta_h = 0
            print('\nPreprocessing successfully terminated.\n')
            raise TheTerminator()

    else:
        hH, hC, TWH_ratio, TWC_ratio = open_HT(HT, F.hH_in, F.hC_in, F.TH_in, F.TC_in, F.PH_in, F.PC_in, M, F)

    if M.verbosity > 1:
        print("Start of equation loop")
        print("Unpacking HT vector:")
        print("    hH:", hH)
        print("    hC:", hC)
        print("    TWH_ratio:", TWH_ratio)
        print("    TWC_ratio:", TWC_ratio)

    # TODO: Add some code that ensures temperatures are decreasing in each channel, otherwise adjust H

    if flag is 1 or M.verbosity > 1:
        Q1 = []
        Q2 = []
        Q3 = []
        Q4 = []
        Q5 = []
        Q6 = []
        Q7 = []
        Q8 = []

    error = np.zeros(M.n*M.N_cell)

    # Calculate pressure distribution in both pipes based on current enthalpy and construct
    # temperature profiles. Pressure is calculated in flow direction.
    PH = np.zeros(M.N_cell+1)
    PH[0] = F.PH_in
    PC = np.zeros(M.N_cell+1)
    PC[0] = F.PC_in
    TH = np.zeros(M.N_cell+1)
    TH[0] = F.TH_in
    TC = np.zeros(M.N_cell+1)
    TC[0] = F.TC_in
    QH = np.zeros(M.N_cell+1)
    QH[0] = F.QH_in
    QC = np.zeros(M.N_cell+1)
    QC[0] = F.QC_in

    for i in range(M.N_cell):
        # apply linear pressure drop if no correlation is specified
        if C_hot.frictionPolynominal is True: # apply linear pressure drop if no correlation is specified
            dP = (F.PH_in - F.PH_out) / (M.N_cell-1.)
        else:
            # check if boiling or condensing
            if i == 0:
                if (hH[i+1] - hH[i])/F.mdotH > 0:
                    cond_boil_flag = 1  # enthalpy increased -> boiling
                else:
                    cond_boil_flag = -1  # enthalpy decreased -> condensing
            if i == M.N_cell-1:
                if (hH[i] - hH[i-1])/F.mdotH > 0:
                    cond_boil_flag = 1
                else:
                    cond_boil_flag = -1
            else:
                if (hH[i+1] - hH[i-1])/F.mdotH > 0:
                    cond_boil_flag = 1
                else:
                    cond_boil_flag = -1
            # outside two-phase dome
            if QH[i] == -1.:
                # calculate pressure drop due to friction
                f = C_hot.calc_frictionFactor(hH[i], PH[i], QH[i], 0,
                                              F.mdotH, G.AH[i], EosH, G.Dh_H[i], 
                                              cond_boil_flag, G.epsilonH)
                # f = calc_friction_singlePhase(PH[i], TH[i], TH[i], F.mdotH,
                #                              G.AH[i], G.Dh_H[i], EosH,
                #                              M.f_CorrelationH, G.epsilonH)
                state = [CP.HmassP_INPUTS, hH[i], PH[i]]
                props = [CP.iDmass]
                rhoH = GetFluidPropLow(EosH, state, props)
                V = F.mdotH / rhoH / G.AH[i]  # calculate flow velocity
                h_f = f * G.HX_L/(M.N_cell+1.) / G.Dh_H[i] * V*V / (2.*9.81)  # calculate friction head loss
                dP = h_f * rhoH * 9.80665
            else:  # insider two-phase dome
                f = C_hot.calc_frictionFactor(hH[i], PH[i], QH[i], 0,
                                              F.mdotH, G.AH[i], EosH, G.Dh_H[i], 
                                              cond_boil_flag, G.epsilonH)
                # f = calc_friction_twoPhase(PH[i], TH[i], TH[i], F.mdotH,
                #                           G.AH[i], G.Dh_H[i], EosH,
                #                           M.f_CorrelationH, G.epsilonH)
                dP = 0.
        PH[i+1] = (PH[i] - dP)
        # use equation of state to calculate temperature based on hH[i] and local pressure
        state = [CP.HmassP_INPUTS, hH[i+1], PH[i+1]]
        props = [CP.iT]
        TH[i+1] = GetFluidPropLow(EosH, state, props)
        props = [CP.iQ]
        QH[i+1]  = GetFluidPropLow(EosH, state, props)

        # PC, TC, and QC vectors are built up in the reverse direction for counter flow.
        # They are reversed subsequently
        if M.co_flow:
            j = i
        else:
            j = M.N_cell - i - 1

        if C_cold.frictionPolynominal is True:  # apply linear pressure drop if no correlation is specified
            dP = (F.PC_in - F.PC_out) / (M.N_cell-1.)
        else:
            # check if boiling or condensing
            if i == 0:
                if (hC[i+1] - hC[i])/F.mdotC > 0:
                    cond_boil_flag = 1  # enthalpy increased -> boiling
                else:
                    cond_boil_flag = -1  # enthalpy decreased -> condensing
            if i == M.N_cell-1:
                if (hC[i] - hC[i-1])/F.mdotC > 0:
                    cond_boil_flag = 1
                else:
                    cond_boil_flag = -1
            else:
                if (hC[i+1] - hC[i-1])/F.mdotC > 0:
                    cond_boil_flag = 1
                else:
                    cond_boil_flag = -1
            # outside two-phase dome
            if QC[i] == -1.:
                f = C_cold.calc_frictionFactor(hC[i], PC[i], QC[i], 0,
                                               F.mdotC, G.AC[j], EosC, G.Dh_C[j],
                                               cond_boil_flag, G.epsilonC)
                # f = calc_friction_singlePhase(PC[i], TC[i], TC[i], F.mdotC,
                #                              G.AC[j], G.Dh_C[j], EosC,
                #                              M.f_CorrelationC, G.epsilonC)
                state = [CP.HmassP_INPUTS, hC[i], PC[i]]
                props = [CP.iDmass]
                rhoC = GetFluidPropLow(EosC, state, props)
                V = F.mdotC / rhoC / G.AC[i]  # calculate flow velocity
                h_f = f * G.HX_L/(M.N_cell+1.) / G.Dh_C[i] * V*V / (2.*9.81)  # calculate friction head loss
                dP = h_f * rhoC * 9.80665
            else:
                f = C_cold.calc_frictionFactor(hC[i], PC[i], QC[i], 0,
                                               F.mdotC, G.AC[j], EosC, G.Dh_C[j],
                                               cond_boil_flag, G.epsilonC)
                # f= calc_friction_twoPhase(PC[i], TC[i], TC[i], F.mdotC,
                #                          G.AC[j], G.Dh_C[j], EosC, 
                #                          M.f_CorrelationC, G.epsilonC)
                dP = 0.
        PC[i+1] = (PC[i] - dP)
        # use equation of state to calculate temperature based on hH[i] and local pressure
        state = [CP.HmassP_INPUTS, hC[j], PC[i+1]]
        props = [CP.iT]
        TC[i+1] = GetFluidPropLow(EosC, state, props)
        props = [CP.iQ]
        QC[i+1] = GetFluidPropLow(EosC, state, props)

    if not M.co_flow:
        # reverse direction of PC as flow is from i = -1 to i = 0
        PC = np.array(list(reversed(PC)))
        TC = np.array(list(reversed(TC)))
        QC = np.array(list(reversed(QC)))

    # reconstruct wall temperatures
    if M.n == 4:
        TWH = 0.5*(TH[0:-1] + TH[1:]) - TWH_ratio * (0.5*(TH[0:-1] + TH[1:]) - 0.5*(TC[0:-1] + TC[1:]))
        TWC = TWH - TWC_ratio * (TWH - 0.5*(TC[0:-1] + TC[1:]))
    else:
        TWH = []
        TWC = []

    if M.verbosity > 1:
        print("Reconstruction of P, T Q")
        print("    PH:", PH)
        print("    PC:", PC)
        print("    TH:", TH)
        print("   TWH:", TWH)
        print("   TWC:", TWC)
        print("    TC:", TC)
        print("    QH:", QH)
        print("    QC:", QC)

    # Initialize property vectors for CoolProp low level interface
    kHMidArray = np.zeros(M.N_cell)
    kHArray = np.zeros(M.N_cell+1)
    kCMidArray = np.zeros(M.N_cell)
    kCArray = np.zeros(M.N_cell+1)
    hHArray = hH
    hCArray = hC

    # Compute midpoint property values - (conductivity)
    for i in range(M.N_cell):
        if QH[i] == -1.:
            state = [CP.PT_INPUTS, (0.5*(PH[i]+PH[i+1])), (0.5*(TH[i]+TH[i+1]))]
            props = [CP.iconductivity]
        else:
            if i < M.N_cell-2:
                if QH[i] > 0 and QH[i+1] > 0:
                    Qtemp = 0.5*(QH[i]+QH[i+1])
                else:
                    Qtemp = QH[i]  # TODO should take average between current and 0 and 1
            else:
                Qtemp = QC[i]
            state = [CP.PQ_INPUTS, 0.5*(PH[i]+PH[i+1]), Qtemp]
            props = [CP.iconductivity]
        kHMidArray[i] = GetFluidPropLow(EosH, state, props)

        if QC[i] == -1.:
            state = [CP.PT_INPUTS, (0.5*(PC[i]+PC[i+1])), (0.5*(TC[i]+TC[i+1]))]
            props = [CP.iconductivity]
        else:
            if i < M.N_cell-2:
                if QC[i] > 0 and QC[i+1] > 0:
                    Qtemp = 0.5*(QC[i]+QC[i+1])
                else:
                    Qtemp = QC[i]  # TODO should take average between current and 0 and 1
            else:
                Qtemp = QC[i]
            state = [CP.PQ_INPUTS, 0.5*(PC[i]+PC[i+1]), Qtemp]
            props = [CP.iconductivity]
        kCMidArray[i] = GetFluidPropLow(EosC, state, props)

    # Compute startpoint property values - (conductivity, h)
    for i in range(M.N_cell+1):
        if QH[i] == -1.:
            # EosH.update(CP.PT_INPUTS,PH[i],TH[i])
            state = [CP.PT_INPUTS, PH[i], TH[i]]
            props = [CP.iconductivity]
        else:
            state = [CP.PQ_INPUTS, PH[i], QH[i]]
            props = [CP.iconductivity]
        kHArray[i] = GetFluidPropLow(EosH, state, props)

        if QC[i] == -1.:
            # EosC.update(CP.PT_INPUTS,PC[i],TC[i])
            state = [CP.PT_INPUTS, PC[i], TC[i]]
            props = [CP.iconductivity]
        else:
            state = [CP.PQ_INPUTS, PC[i], QC[i]]
            props = [CP.iconductivity]
        kCArray[i] = GetFluidPropLow(EosC, state, props)


    if M.n == 4:  # case where wall temperature is modelled
        # calculate energy balance for high pressure stream (H); low pressure stream (C) and dividing wall
        for i in range(M.N_cell):
            # Read hot stream props
            kH = kHMidArray[i]
            kHm = kHArray[i]
            kHp = kHArray[i+1]
            hHm = hHArray[i]
            hHp = hHArray[i+1]
            # New Read cold stream props
            kC = kCMidArray[i]
            kCm = kCArray[i]
            kCp = kCArray[i+1]
            hCm = hCArray[i]
            hCp = hCArray[i+1]

            # check if boiling or condensing
            if i == 0:
                if (hH[i+1] - hH[i])/F.mdotH > 0:
                    cond_boil_flag = 1  # enthalpy increased -> boiling
                else:
                    cond_boil_flag = -1  # enthalpy decreased -> condensing
            if i == M.N_cell-1:
                if (hH[i] - hH[i-1])/F.mdotH > 0:
                    cond_boil_flag = 1
                else:
                    cond_boil_flag = -1
            else:
                if (hH[i+1] - hH[i-1])/F.mdotH > 0:
                    cond_boil_flag = 1
                else:
                    cond_boil_flag = -1

            # get Nusselt Number
            [htcH, htcH_flag] = C_hot.calc_HTC([hH[i], hH[i+1]], [PH[i], PH[i+1]],
                                           QH[i], TWH[i], F.mdotH, G.AH[i],
                                           G.Lc_H, EosH, cond_boil_flag, N)
            # if QH[i] == -1.:
            #    NuH = calc_Nu_singlePhase(PH[i], PH[i+1], TH[i], TH[i+1],
            #                              TWH[i], F.mdotH, G.AH[i], G.Lc_H,
            #                              EosH, M.Nu_CorrelationH, N, M)
            # else:
            #    NuH = calc_Nu_twoPhase(PH[i], PH[i+1], QH[i], TH[i], TH[i+1],
            #                           TWH[i], F.mdotH, G.AH[i], G.Lc_H,
            #                           EosH, M.Nu_CorrelationH, N, M)

            # check if boiling or condensing
            if i == 0:
                if (hC[i+1] - hC[i])/F.mdotC > 0:
                    cond_boil_flag = 1  # enthalpy increased -> boiling
                else:
                    cond_boil_flag = -1  # enthalpy decreased -> condensing
            if i == M.N_cell-1:
                if (hC[i] - hC[i-1])/F.mdotC > 0:
                    cond_boil_flag = 1
                else:
                    cond_boil_flag = -1
            else:
                if (hC[i+1] - hC[i-1])/F.mdotC > 0:
                    cond_boil_flag = 1
                else:
                    cond_boil_flag = -1

            # get Nusselt Number
            [htcC, htcC_flag] = C_cold.calc_HTC([hC[i], hC[i+1]], [PC[i], PC[i+1]],
                                             QC[i], TWC[i], F.mdotC, G.AC[i],
                                             G.Lc_C, EosC, cond_boil_flag, N)

            # if QC[i] == -1.:
            #    NuC = calc_Nu_singlePhase(PC[i], PC[i+1], TC[i], TC[i+1],
            #                              TWC[i], F.mdotC, G.AC[i], G.Lc_C,
            #                              EosC, M.Nu_CorrelationC, N, M)
            # else:
            #    NuC = calc_Nu_twoPhase(PC[i],PC[i+1],QC[i],TC[i],TC[i+1],
            #                           TWC[i], F.mdotC, G.AC[i], G.Lc_C,
            #                           EosC, M.Nu_CorrelationC, N, M)

            # htc now calculated with correlations.
            # htcH = NuH * kH / G.Lc_H
            # htcC = NuC * kC / G.Lc_C

            # heat transfer in H channel
            q1_conv = hHm * F.mdotH
            if i == 0:
                q1_cond = - kHm * G.AH[i] / (G.L_wall*S.CellLengths[i]/2.) * (0.5*(TH[i] + TH[i+1]) - TH[i])
            else:
                q1_cond = - kHm * G.AH[i] / (G.L_wall*S.CellLengths[i]) * (0.5*(TH[i] + TH[i+1]) - 0.5*(TH[i] + TH[i-1]))
            q1 = q1_conv + q1_cond

            q2_conv = hHp * F.mdotH
            if i == M.N_cell-1:
                q2_cond = - kHp * G.AH[i] / (G.L_wall*S.CellLengths[i]/2.) * (TH[i+1] - 0.5*(TH[i] + TH[i+1]))
            else:
                q2_cond = - kHp * G.AH[i] / (G.L_wall*S.CellLengths[i]) * (0.5*(TH[i+1] + TH[i+2]) - 0.5*(TH[i] + TH[i+1]))
            q2 = q2_conv + q2_cond

            if htcH_flag == 'htc':
                q3 = htcH * G.Area*S.CellLengths[i] * (0.5*(TH[i]+TH[i+1]) - TWH[i])
            elif htcH_flag == 'q_critical':
                q3 = htcH * G.Area*S.CellLengths[i] * np.sign(0.5*(TH[i]+TH[i+1]) - TWH[i])
            else:
                raise MyError('An error occured.')

            # Heat transfer in wall
            q7_temp = (G.k_wall * G.AreaXWall * S.CellLengths[i])/G.t_wall * (TWH[i] - TWC[i])
            if M.flag_axial is 1:
                if i == 0:
                    q7_p = - G.k_wall * G.A_wall[i]/(G.L_wall*(S.CellLengths[i]+S.CellLengths[i+1])/2) * (0.5*(TWH[i+1]+TWC[i+1]) - 0.5*(TWH[i] + TWC[i]))
                    q7_m = 0.
                elif i == M.N_cell-1:
                    q7_p = 0.
                    q7_m = - G.k_wall * G.A_wall[i] / (G.L_wall*(S.CellLengths[i-1]+S.CellLengths[i])/2) * (0.5*(TWH[i] + TWC[i]) - 0.5*(TWH[i-1]+TWC[i-1]))
                else:
                    q7_p = - G.k_wall * G.A_wall[i] / (G.L_wall*(S.CellLengths[i]+S.CellLengths[i+1])/2) * (0.5*(TWH[i+1]+TWC[i+1]) - 0.5*(TWH[i]+TWC[i]))
                    q7_m = - G.k_wall * G.A_wall[i] / (G.L_wall*(S.CellLengths[i-1]+S.CellLengths[i])/2) * (0.5*(TWH[i] + TWC[i]) - 0.5*(TWH[i-1]+TWC[i-1]))
            else:
                q7_p = 0.
                q7_m = 0.
            q7h = q7_temp - 0.5 * (-q7_p + q7_m)
            q7c = q7_temp + 0.5 * (-q7_p + q7_m)

            if htcC_flag == 'htc':
                q4 = htcC * G.Area*S.CellLengths[i] * (TWC[i] - 0.5*(TC[i]+TC[i+1]))
            elif htcC_flag == 'q_critical':
                q4 = htcC * G.Area*S.CellLengths[i] * np.sign(TWC[i] - 0.5*(TC[i]+TC[i+1]))
            else:
                raise MyError('An error occured.')

            # Heat Transfer in C Channel
            q5_conv = hCm * -F.mdotC
            if i == 0:
                q5_cond = - kCm * G.AC[i] / (G.L_wall*S.CellLengths[i]/2.) * (0.5*(TC[i] + TC[i+1]) - TC[i])
            else:
                q5_cond = - kCm * G.AC[i] / (G.L_wall*S.CellLengths[i]) * (0.5*(TC[i] + TC[i+1]) - 0.5*(TC[i] + TC[i-1]))
            q5 = q5_conv + q5_cond
            q6_conv = hCp * -F.mdotC
            if i == M.N_cell-1:
                q6_cond = - kCp * G.AC[i] / (G.L_wall*S.CellLengths[i]/2.) * (TC[i+1] - 0.5*(TC[i] + TC[i+1]))
            else:
                q6_cond = - kCp * G.AC[i] / (G.L_wall*S.CellLengths[i]) * (0.5*(TC[i+1] + TC[i+2]) - 0.5*(TC[i] + TC[i+1]))
            q6 = q6_conv + q6_cond

            # calculate energy flux mismatch
            error[i] = q1-q2-q3
            error[M.N_cell+i] = q3-q7h
            error[2*M.N_cell+i] = q4-q7c
            error[3*M.N_cell+i] = q4+q5-q6

            if flag is 1 or M.verbosity > 1:
                Q1.append(q1)
                Q2.append(q2)
                Q3.append(q3)
                Q4.append(q4)
                Q5.append(q5)
                Q6.append(q6)
                Q7.append(q7h)
                Q8.append(q7c)

    # FIX: add in cell lengths for this case
    # Case where fluid temperature only is modelled
    elif M.n == 2:
        # calculate energy balance for high pressure stream (H); low pressure stream (C)
        for i in range(M.N_cell):
            # Read hot stream props
            kH = kHMidArray[i]
            kHm = kHArray[i]
            kHp = kHArray[i+1]
            hHm = hHArray[i]
            hHp = hHArray[i+1]
            # New Read cold stream props
            kC = kCMidArray[i]
            kCm = kCArray[i]
            kCp = kCArray[i+1]
            hCm = hCArray[i]
            hCp = hCArray[i+1]

            # heat transfer in H channel
            q1_conv = hHm * F.mdotH
            if i == 0:
                q1_cond = - kHm * G.AH[i]/ (G.L_wall/M.N_cell/2.) * ( 0.5*(TH[i] + TH[i+1]) - TH[i] )
            else:
                q1_cond = - kHm * G.AH[i]/ (G.L_wall/M.N_cell)    * ( 0.5*(TH[i] + TH[i+1]) - 0.5*(TH[i] + TH[i-1]) )
            q1 = q1_conv + q1_cond
            q2_conv = hHp * F.mdotH
            if i == M.N_cell-1:
                q2_cond = - kHp * G.AH[i]/ (G.L_wall/M.N_cell/2.) * (      TH[i+1]            - 0.5*(TH[i] + TH[i+1]) )
            else:
                q2_cond = - kHp * G.AH[i]/ (G.L_wall/M.N_cell)    * ( 0.5*(TH[i+1] + TH[i+2]) - 0.5*(TH[i] + TH[i+1]) )
            q2 = q2_conv + q2_cond

            # switch between mean and log mean temperature difference for heat flux calculation.
            if M.LM == 1:
                # calculate q3 based on U*A*dTlm
                if M.co_flow:
                    q3 = G.UA / M.N_cell * (((TH[i+1]-TC[i+1])-(TH[i]-TC[i]))/np.log((TH[i+1]-TC[i+1])/(TH[i]-TC[i])))
                else:
                    q3 = G.UA / M.N_cell * (((TH[i+1]-TC[i])-(TH[i]-TC[i+1]))/np.log((TH[i+1]-TC[i])/(TH[i]-TC[i+1]))) 
            else:
                # calculate q3 based on mean temperature difference
                q3 = G.UA / M.N_cell * (0.5*(TH[i]+TH[i+1]) - 0.5*(TC[i]+TC[i+1])) 

            # Heat Transfer in C Channel
            q5_conv = hCm * -F.mdotC
            if i == 0:
                q5_cond = - kCm * G.AC[i]/ (G.L_wall/M.N_cell/2.) * ( 0.5*(TC[i] + TC[i+1]) - TC[i] )
            else:
                q5_cond = - kCm * G.AC[i]/ (G.L_wall/M.N_cell)    * ( 0.5*(TC[i] + TC[i+1]) - 0.5*(TC[i] + TC[i-1]) )
            q5 = q5_conv + q5_cond
            q6_conv = hCp * -F.mdotC
            if i == M.N_cell-1:
                q6_cond = - kCp * G.AC[i]/ (G.L_wall/M.N_cell/2.) * (      TC[i+1]            - 0.5*(TC[i] + TC[i+1]) )
            else:
                q6_cond = - kCp * G.AC[i]/ (G.L_wall/M.N_cell)    * ( 0.5*(TC[i+1] + TC[i+2]) - 0.5*(TC[i] + TC[i+1]) )
            q6 = q6_conv + q6_cond

            # calculate mis-match in energy fluxes
            error[i] = q1-q2-q3
            error[M.N_cell+i] = q3+q5-q6

            if (flag is 1) or (M.verbosity > 1):
                Q1.append(q1)
                Q2.append(q2)
                Q3.append(q3)
                Q4.append(0)
                Q5.append(q5)
                Q6.append(q6)
                Q7.append(0)
                Q8.append(0)

    if M.verbosity > 1:
        print('Error', error)
        print('Q1:', Q1)
        print('Q2:', Q2)
        print('Q3:', Q3)
        print('Q4:', Q4)
        print('Q5:', Q5)
        print('Q6:', Q6)
        print('Q7:', Q7)
        print('Q8:', Q8)

    if flag is 0:
        return error
    elif flag is 1:
        return error, HT, Q1, Q2, Q3, Q4, Q5, Q6, Q7, Q8
    elif flag is 2:
        return TH, TC, PH, PC, QH, QC, hH, hC, TWH, TWC
    else:
        raise MyError('flag option not defined.')


def equations_min(HT, M, G, F, N, S, C_hot, C_cold, flag=0):
    """
    variation of equations to suit minimisation
    """
    error = equations(HT, M, G, F, N, S, C_hot, C_cold, flag=0)
    return sum(error**2)


def open_T(T, TH_in, TC_in, PH_in, PC_in, M, F):
    """
    function to unpack the Temperature vector T into 2/4 vectors
    T contains the data in the following order:
    TH, TC, TWH, TWC
    """
    N_cell = M.N_cell

    TH = np.zeros(N_cell+1)
    TC = np.zeros(N_cell+1)
    TH[0] = TH_in
    TH[1:N_cell+2] = T[0:N_cell]
    if M.co_flow:
        TC[0] = TC_in
        TC[1:N_cell+2] = T[1*N_cell:2*N_cell]
    else:
        TC[0:N_cell] = T[1*N_cell:2*N_cell]
        TC[N_cell] = TC_in

    if len(T) == 4*N_cell:
        TWH = np.zeros(N_cell)
        TWC = np.zeros(N_cell)
        TWH = T[2*N_cell:3*N_cell]
        TWC = T[3*N_cell:4*N_cell]
    else:
        TWH = []
        TWC = []
    return TH, TC, TWH, TWC  # ,PH,PC


def open_T2(T, TH_in, TC_in, PH_in, PC_in, M, F):
    """
    function to unpack the Temperature vector T into 2/4 vectors
    T contains the data in the following order:
    TH, TC, TWH, TWC
    """
    N_cell = M.N_cell

    if len(T) == 4*N_cell:
        TH = np.zeros(N_cell+1)
        TC = np.zeros(N_cell+1)
        TH[0] = TH_in
        TH[1:N_cell+2] = T[0:N_cell]
        T_delta0 = abs(T[1*N_cell:2*N_cell])
        T_delta1 = T[2*N_cell:3*N_cell]
        T_delta2 = T[3*N_cell:4*N_cell]
        TWH = TH[1:N_cell+2]-T_delta0
        TWC = TWH - T_delta1
        if M.co_flow:
            TC[0] = TC_in
            TC[1:N_cell+2] = TWC - T_delta2
        else:
            TC[0:N_cell] = TWC - T_delta2
            TC[N_cell] = TC_in
    else:   # look at case with only two temperatures
        TH = np.zeros(N_cell+1)
        T_delta = abs(T[1*N_cell:2*N_cell])
        TC = np.zeros(N_cell+1)
        TH[0] = TH_in
        TH[1:N_cell+2] = T[0:N_cell]
        if M.co_flow:
            TC[0] = TC_in
            TC[1:N_cell+2] = TH[1:N_cell+2] - T_delta
        else:
            TC[0:N_cell] = TH[0:N_cell] - T_delta
            TC[N_cell] = TC_in
        TWH = []
        TWC = [] 

    return TH, TC, TWH, TWC  # ,PH,PC
###
###
def open_HT_old(HT, hH_in, hC_in, TH_in, TC_in, PH_in, PC_in, M, F):
    """
    function to unpack the Enthlapy/Temperature vector to obtain 2 * 2/4 vectors
    HY contains the data in the following order:
    hH, hC, TWH, TWC
    """
    N_cell = M.N_cell

    hH = np.zeros(N_cell+1)
    hC = np.zeros(N_cell+1)
    hH[0] = hH_in
    hH[1:N_cell+2] = abs(HT[0:N_cell])
    if M.co_flow:
        hC[0] = hC_in
        hC[1:N_cell+2] = abs(HT[1*N_cell:2*N_cell])
    else:
        hC[0:N_cell] = abs(HT[1*N_cell:2*N_cell])
        hC[N_cell] = hC_in

    if len(HT) == 4*N_cell:
        TWH = np.zeros(N_cell)
        TWC = np.zeros(N_cell)
        TWH = HT[2*N_cell:3*N_cell]+HT[3*N_cell:4*N_cell]
        TWC = HT[2*N_cell:3*N_cell]-HT[3*N_cell:4*N_cell]
    else:
        TWH = []
        TWC = []

    return hH, hC, TWH, TWC
###
###
def open_HT(HT, hH_in, hC_in, TH_in, TC_in, PH_in, PC_in, M, F):
    """
    function to unpack the Enthlapy/Temperature vector to obtain 2 * 2/4 vectors
    HY contains the data in the following order:
    hH, hC, TWH, TWC
    """
    N_cell = M.N_cell

    hH = np.zeros(N_cell+1)
    hC = np.zeros(N_cell+1)
    hH[0] = hH_in
    hH[1:N_cell+2] = abs(HT[0:N_cell])*1e6
    if M.co_flow:
        hC[0] = hC_in
        hC[1:N_cell+2] = abs(HT[1*N_cell:2*N_cell])*1e6
    else:
        # ## Make change here ### (add +1 to both indices)
        hC[0:N_cell] = abs(HT[1*N_cell:2*N_cell])*1e6
        hC[N_cell] = hC_in

    if len(HT) == 4*N_cell:
        TWH_ratio = np.zeros(N_cell)
        TWC_ratio = np.zeros(N_cell)
        TWH_ratio = HT[2*N_cell:3*N_cell]
        TWC_ratio = HT[3*N_cell:4*N_cell]
        TWH_ratio[TWH_ratio > 1.] = 1.
        TWH_ratio[TWH_ratio < 0.] = 0.
        TWC_ratio[TWC_ratio > 1.] = 1.
        TWC_ratio[TWC_ratio < 0.] = 0.
    else:
        TWH_ratio = []
        TWC_ratio = []

    return hH, hC, TWH_ratio, TWC_ratio


def OpenHtl(HTX, hH_in, hC_in, TH_in, TC_in, PH_in, PC_in, M, F):
    """
    Unpacks input guess to equations for constant enthalpy
    change formulation.

    Hot stream total enthalpy change is Q, cold stream enthalpy change
    per cell is (Q/N_cell)+DeltaQ_i (FIX description).

    Inputs:
    HTX - contains guess in the following format:
            Q, DeltaQ, TempWallHotRatio, TempWallColdRatio, CellLengths
    M   - Class containing model parameters
    F   - Class containing fluid parameters
    hH_in, hC_in - Hot and cold stream inlet enthalpies
    TH_in, TC_in - Hot and cold stream inlet temperatures
    PH_in, PC_in - Hot and cold stream inlet pressures
    """
    # Unpack optimisation guess
    Q = HTX[0]
    DeltaQ = HTX[1:M.N_cell+1]
    CellLengths = abs(np.hstack((HTX[M.N_cell+1:2*M.N_cell], 1.0)))  # Last cell always length 1, all values +ve
    CellLengths = CellLengths/sum(CellLengths)  # Normalise to make TOTAL length 1
    TWH_ratio = HTX[2*M.N_cell:3*M.N_cell]
    TWC_ratio = HTX[3*M.N_cell:4*M.N_cell]

    # Reverse DeltaQ vector for cold stream
    DeltaQRev = np.fliplr([DeltaQ])[0]

    # ## FIX: split DeltaQ between hot and cold streams ###

    # ## Fixed cold deltaH ###
    # Creat hot and cold stream enthalpy vectors
    hC = hC_in + np.linspace(1, 0, M.N_cell+1)*Q/F.mdotC
    hH = hH_in - np.linspace(0, 1, M.N_cell+1)*Q/F.mdotH + np.hstack([0, DeltaQ])

    # ## Fixed hot deltaH ###
    # # Create hot stream enthalpy vector
    # hH = hH_in - np.linspace(0,1,M.N_cell+1)*Q/F.mdotH
    # # hH = hH_in - np.linspace(0,1,M.N_cell+1)*Q/F.mdotH - 0.5*np.hstack([0,DeltaQ]) # Don't really need 0.5

    # # Create cold stream enthalpy vector
    # if M.co_flow:
    #   hC = hC_in + np.linspace(0,1,M.N_cell+1)*Q/F.mdotC + np.hstack([0,DeltaQ])
    #   # hC = hC_in + np.linspace(0,1,M.N_cell+1)*Q/F.mdotC + 0.5*np.hstack([0,DeltaQ])
    # else:
    #   hC = hC_in + np.linspace(1,0,M.N_cell+1)*Q/F.mdotC + np.hstack([DeltaQ,0])
    #   ### FIX: Check position of 0 ###
    #   # hC = hC_in + np.linspace(1,0,M.N_cell+1)*Q/F.mdotC + 0.5*np.hstack([DeltaQRev,0])

    if len(HTX) == 4*M.N_cell:
        TWH_ratio[TWH_ratio > 1.] = 1.
        TWH_ratio[TWH_ratio < 0.] = 0.
        TWC_ratio[TWC_ratio > 1.] = 1.
        TWC_ratio[TWC_ratio < 0.] = 0.
    else:
        TWH_ratio = []
        TWC_ratio = []

    return hH, hC, TWH_ratio, TWC_ratio, CellLengths
###
###
def HX_solver(M, G, F, N, S, C_hot, C_cold, flag, HT0):
    # set up tuple of optional inputs for use by fsolve
    args = (M, G, F, N, S, C_hot, C_cold, flag)
    if M.optim == 'fsolve':
        HT, infodict, status, mesg = optimize.fsolve(equations, HT0, args=args, full_output=1)
    elif M.optim == 'root:hybr':
        sol = optimize.root(equations, HT0, args=args, method='hybr', options={'xtol': 1.e-12})
        status = sol.status
        HT = sol.x
        mesg = sol.message
    elif M.optim == 'root:lm':
        sol = optimize.root(equations, HT0, args=args, method='lm', options={'eps': 1.e-3, 'xtol': 1.e-12, 'ftol': 1e-12})
        status = sol.status
        HT = sol.x
        mesg = sol.message
    elif M.optim == 'root:Newton-CG':
        sol = optimize.root(equations, HT0, args=args, method='lm', options={'eps': 1.e-3, 'xtol': 1.e-12})
        status = sol.status
        HT = sol.x
        mesg = sol.message
    elif M.optim == 'root:df-sane':
        sol = optimize.root(equations, HT0, args=args, method='df-sane', options={'ftol': 1.e-12})
        status = sol.status
        HT = sol.x
        mesg = sol.message
    elif M.optim == 'L-BFGS-B':
        Tmin = min(T0)
        Tmax = max(T0)
        bounds = []
        for i in range(len(T0)):
            bounds.append((Tmin,Tmax))
        sol = optimize.minimize(equations_min,HT0,args=args,method='L-BFGS-B',bounds=bounds,options={'disp':True})
        status = sol.status      
        HT=sol.x
        mesg = sol.message
    elif M.optim == 'TNC':
        Tmin = min(T0)
        Tmax = max(T0)
        bounds = []
        for i in range(len(T0)):
            bounds.append((Tmin,Tmax))
        sol = optimize.minimize(equations_min,HT0,args=args,method='TNC',bounds=bounds,options={'disp':True})
        status = sol.status      
        HT =sol.x
        mesg = sol.message
    else:
        raise MyError("gdata.optim = '' not set properly.")

    return HT, status, mesg


def H_solver(M, G, F, N, flag, C_hot, C_cold, Q0):
    """Run H_solver."""
    # set up tuple of optional inputs for use by fsolve
    args = (M, G, F, N, flag, C_hot, C_cold)
    if M.optim == 'fsolve':
        HT, infodict, status, mesg = optimize.fsolve(Hequations, Q0, args=args, full_output=1)
    elif M.optim == 'root:hybr':
        sol = optimize.root(Hequations,Q0,args=args,method='hybr',options={'xtol':1.e-12})
        status = sol.status
        HT = sol.x
        mesg = sol.message
    elif M.optim == 'root:lm':
        sol = optimize.root(Hequations,Q0,args=args,method='lm',options={'eps':1.e-3, 'xtol':1.e-12, 'ftol':1e-12})
        status = sol.status
        HT = sol.x
        mesg = sol.message
    elif M.optim == 'root:Newton-CG':
        sol = optimize.root(Hequations,Q0,args=args,method='lm',options={'eps':1.e-3, 'xtol':1.e-12})
        status = sol.status
        HT = sol.x
        mesg = sol.message
    elif M.optim == 'root:df-sane':
        sol = optimize.root(Hequations,Q0,args=args,method='df-sane',options={'ftol':1.e-12})
        status = sol.status
        HT = sol.x
        mesg = sol.message
    elif M.optim == 'L-BFGS-B':
        Tmin = min(T0)
        Tmax = max(T0)
        bounds = []
        for i in range(len(T0)):
            bounds.append( (Tmin,Tmax))
        sol = optimize.minimize(Hequations_min,Q0,args=args,method='L-BFGS-B',bounds=bounds,options={'disp':True})
        status = sol.status      
        HT=sol.x
        mesg = sol.message
    elif M.optim == 'TNC':
        Tmin = min(T0)
        Tmax = max(T0)
        bounds = []
        for i in range(len(T0)):
            bounds.append( (Tmin,Tmax))
        status = sol.status      
        HT=sol.x
        mesg = sol.message
    else:
        raise MyError("gdata.optim = '' not set preoperly.")

    #if status is not 1:
    #    print(mesg)
    #    raise MyError('HX_solver.py: '+ M.optim + ' unable to converge. In HX label: ' + M.label)

    return HT,status,mesg
###
###
def calc_pinch_error(UA,M,G,F,N,HT0):
    # function to calculate the 
    G.UA = abs(UA)
    HT,status,mesg = HX_solver(M,G,F,N,S,0,HT0)
    #TH, TC, TWH, TWC= open_T2(T,F.TH_in,F.TC_in,F.PH_in,F.PC_in,M,F)
    TH, TC, PH, PC, QH, QC, hH, hC, TWH, TWC = equations(HT, M, G, F, N, S, 2)
    T_pinch = min(abs(TH-TC))
    print("T_pinch:", T_pinch, "UA", G.UA)
    return (T_pinch - G.dT)**2
###
###
def main(uoDict):
    """
    main function
    """
    # create string to collect warning messages
    warn_str = "\n"

    # main file to be executed
    jobFileName = uoDict.get("--job", "test")

    # strip .py extension from jobName
    jobName = jobFileName.split('.')
    jobName = jobName[0]

    # create classes to store input data
    M = Model()
    F = Fluid()
    G = Geometry()
    N = NuCorrelation()
    S = SolverManager()

    # set verbosity (can be overwritten from jobfile)
    M.verbosity = 1
    if "--verbosity" in uoDict:
        M.verbosity = int(uoDict.get("--verbosity", 1))

    C_hot = Correlation(verbosity=M.verbosity)
    C_cold = Correlation(verbosity=M.verbosity)

    # Execute jobFile, this creates all the variables
    exec(open(jobFileName).read(), globals(), locals())

    if M.verbosity > 0:
        print("Input data file read")

    # Check required input data is provided
    M.check()
    F.check(M)
    C_hot.check(G)
    C_cold.check(G)
    M.set_poly(F)
    G.check_initialise(M)
    S.CheckAndInitialise(M)

    if C_hot.includeCustom_check() or C_cold.includeCustom_check():
        N.check()  # Custom Nu correlation check

    # Create fluid classes
    global EosC
    global EosH

    # Create fluid property objects
    if F.fluidH.split(':')[0].upper() == 'CUSTOM':
        EosH = CustomFluid(F.fluidPropFileH)
    else:
        EosH = CP.AbstractState('HEOS',F.fluidH)  
    if F.fluidC.split(':')[0].upper() == 'CUSTOM':
        EosC = CustomFluid(F.fluidPropFileC)
    else:
        EosC = CP.AbstractState('HEOS',F.fluidC)

    # Update Boundaries include missing terms.
    F.set_BC()

    # Create initial guess vector
    HT0 = F.get_HT0(M)

    # calculate outlet pressure if pressure drop is prescribed by a poynominal
    if C_hot.frictionPolynominal is True:
        if isinstance(C_hot.frictionPolynominal_Coeff, float):
            dP = C_hot.frictionPolynominal_Coeff
        else:
            dP = 0.
            # evaluate polynominal dP = a0 + a1*mdot + a2*mdot**2 + ...
            for i in range(len(C_hot.frictionPolynominal_Coeff)):
                dP += C_hot.frictionPolynominal_Coeff[i] * F.mdotH**i
        F.PH_out = F.PH_in - dP
    if C_cold.frictionPolynominal is True:
        if isinstance(C_cold.frictionPolynominal_Coeff, float):
            dP = C_cold.frictionPolynominal_Coeff
        else:
            dP = 0.
            # evaluate polynominal dP = a0 + a1*mdot + a2*mdot**2 + ...
            for i in range(len(C_cold.frictionPolynominal_Coeff)):
                dP += C_cold.frictionPolynominal_Coeff[i] * F.mdotC**i
        F.PC_out = F.PC_in - dP

    # Change flow direction of cold channel if co_flow
    if M.co_flow: F.mdotC = -F.mdotC

    # Simulate geometry based and fixed UA heat exchangers
    if G.HXtype != 'pinch_dT' and G.HXtype != 'dT_map':
        # Preprocessing
        if M.flag_adaptive_grid:
            # Compute grid size by solving constant enthalpy change case for one set of
            # iterations with axial heat conduction switched off
            if M.optim != 'root:hybr': MyError('Adaptive grid currently only working for' \
                                               '"root:hybr" solution method.')
            S.SolverStage = 'preprocess'
            M.flag_axial = 0
            M.flag_constant_delta_h = 1
            X0 = F.get_HTL0(M)

            # Solve for one cycle of iterations to obtain cell lengths vector
            try: X,status,mesg = HX_solver(M,G,F,N,S,0,X0)
            except TheTerminator: pass

            # Switch on axial conduction then continue on to standard solution method
            M.flag_axial = 1
            # FIX: clean up flow control here (make recursive after N steps, 'else' not reqd)

        # Analyse heat exchanger using standard enthalpy-based method
        HT,status,mesg = HX_solver(M,G,F,N,S, C_hot, C_cold, 0, HT0)
        hH, hC, TWH_ratio, TWC_ratio = open_HT(HT,F.hH_in,F.hC_in,F.TH_in,F.TC_in,F.PH_in,F.PC_in,M,F)
        TH, TC, PH, PC, QH, QC, hH, hC, TWH, TWC = equations(HT, M, G, F, N, S, C_hot, C_cold, 2)

    # Perform nested optimisation to adjust UA to achieve a target pinch point temperature difference
    if G.HXtype == 'UA_dT': 
        Pinch_error = min(abs(TH-TC)) - G.dT
        if  abs(Pinch_error) > 0.1:
            maxiter = 100
            tol = 10. # tolerance to which UA is optimised
            print("Starting Loop to adjust UA in order to adjust pinch point temperature difference")
            args = (M,G,F,N,HT0)
            try:
                UA = optimize.newton(calc_pinch_error,G.UA,args=args,maxiter=maxiter,tol=tol)
                # A = optimize.minimize_scalar(calc_pinch_error,args=args,method='golden',
                #                              options={'maxiter': maxiter,'xtol':0.1})
                # A = optimize.fmin(calc_pinch_error,G.UA,args=args, maxiter=maxiter)
                #                   #, tol=0.1, ftol=0.1)
                # A = optimize.minimize(calc_pinch_error,G.UA,args=args,method='Nelder-Mead',
                #                       options={'maxiter': maxiter}) 
                G.update('UA',abs(UA),jobFileName,alpha=0.9)
                HT,status,mesg = HX_solver(M,G,F,N,S,C_hot,C_cold,0,HT0)  
                hH, hC, TWH_ratio, TWC_ratio= open_HT(HT,F.hH_in,F.hC_in,F.TH_in,F.TC_in,F.PH_in,F.PC_in,M,F)
                TH, TC, PH, PC, QH, QC, hH, hC, TWH, TWC = equations(HT, M, G, F, N, S, C_hot, C_cold, 2)
                #TH, TC, TWH_ratio, TWC_ratio= open_T3(T,F.TH_in,F.TC_in,F.PH_in,F.PC_in,M,F)
            except:
                print("Newton Iteration to find UA has not converged")

    # perform pinch point optimisation based on simple energy dicretisation
    if G.HXtype == 'pinch_dT':
        print("Running purely energy based Heat exchanger calculation to achieve target" \
              "pinch point. Only supports prescribed pressure drop calculations.")
        # calculate theoretical maximum heat fluxes
        #hH_out = GetFluidPropLow(EosH,[CP.PT_INPUTS,F.PH_out,F.TC_in],[CP.iHmass])
        #hC_out = GetFluidPropLow(EosC,[CP.PT_INPUTS,F.PC_out,F.TH_in],[CP.iHmass])
        #dH_H = abs((F.hH_in - hH_out)*F.mdotH); dH_C = abs((F.hC_in - hC_out)*F.mdotC)
        #Q0 = 0.5* min(dH_H, dH_C) # half heat flux of smaller value to get conservative stating point.
        # the above four lines can cause issues if temperature range of one fluid is exceeded, e.g due to frezing

        # use F.Q_start calculated in get_HT0
        Q0 = F.Q_start

        # Call solver that that varies Q till correct heat exchange is found
        # Q is iteratively solved until target pinch point is achieved
        Q, status, mesg = H_solver(M, G, F, N, 0, C_hot, C_cold, Q0)

    # perform calculation where Q is provided as a function of a map of dT    
    if G.HXtype == 'dT_map':
        print("Computing performance based on provided map.")
        Q_dT = G.Q_dT_interp(F.mdotH,F.mdotC)[0]
        dT = F.TH_in - F.TC_in
        Q = Q_dT * dT

        if M.co_flow == 0:
            hH_out = F.hH_in - Q / F.mdotH
            hH = np.linspace(F.hH_in, hH_out, num=M.N_cell+1)
            hC_out = F.hC_in + Q / F.mdotC
            hC = np.linspace(hC_out, F.hC_in, num=M.N_cell+1)

            PH = np.linspace(F.PH_in, F.PH_out, num=M.N_cell+1)
            PC = np.linspace(F.PC_out, F.PC_in, num=M.N_cell+1)
        else:
            raise MyEror("Heat-Exchanger of Type G.dT_map only support M.co_flow = 0. \n Bailing Out!")

        TH = []
        TC = []
        QH = []
        QC = []
        for h, P in zip(hH, PH):
            TH.append(GetFluidPropLow(EosH, [CP.HmassP_INPUTS, h, P], [CP.iT]))
            QH.append(GetFluidPropLow(EosH, [CP.HmassP_INPUTS, h, P], [CP.iQ]))
        for h, P in zip(hC, PC):
            TC.append(GetFluidPropLow(EosC, [CP.HmassP_INPUTS, h, P], [CP.iT]))
            QC.append(GetFluidPropLow(EosC, [CP.HmassP_INPUTS, h, P], [CP.iQ]))

        Q1 = np.zeros(M.N_cell)
        Q2 = np.zeros(M.N_cell)
        Q5 = np.zeros(M.N_cell)
        Q6 = np.zeros(M.N_cell)
        Q7 = np.zeros(M.N_cell)
        Q8 = np.zeros(M.N_cell)
        Q3 = np.ones(M.N_cell) * Q/M.N_cell
        Q4 = Q3

    # create property traces for output
    if G.HXtype == 'pinch_dT':
        TH, TC, PH, PC, QH, QC, hH, hC, TWH, TWC = Hequations(Q, M, G, F, N, S, C_hot, C_cold, 2)
        error, HT, Q1, Q2, Q3, Q4, Q5, Q6, Q7, Q8 = Hequations(Q, M, G, F, N, S, C_hot, C_cold, 1)
    elif G.HXtype == 'dT_map':
        TWH = [] 
        TWC = []
        error = 'n/a'
    else:
        TH, TC, PH, PC, QH, QC, hH, hC, TWH, TWC = equations(HT, M, G, F, N, S, C_hot, C_cold, 2)
        error, HT, Q1, Q2, Q3, Q4, Q5, Q6, Q7, Q8 = equations(HT, M, G, F, N, S, C_hot, C_cold, 1)

    TH = np.array(TH)
    TC = np.array(TC)
    PH = np.array(PH)
    PC = np.array(PC)
    QH = np.array(QH)
    QC = np.array(QC)
    hH = np.array(hH)
    hC = np.array(hC)

    # create enthlpies for output
    hH_in = hH[0]  # GetFluidPropLow(EosH,[CP.PT_INPUTS,F.PH_in,TH[0]],[CP.iHmass])
    hH_out = hH[-1]  # GetFluidPropLow(EosH,[CP.PT_INPUTS,F.PH_out,TH[-1]],[CP.iHmass])
    if M.co_flow:
        hC_in = hC[0]  # GetFluidPropLow(EosC,[CP.PT_INPUTS,F.PC_in,TC[0]],[CP.iHmass])
        hC_out = hC[-1]  # GetFluidPropLow(EosC,[CP.PT_INPUTS,F.PC_out,TC[-1]],[CP.iHmass])
    else:
        hC_in = hC[-1]
        hC_out = hC[0]

    # print results
    if M.verbosity > 0:
        print("\n")
        print("#################")
        print("#### RESULTS ####")
        print("#################")

        print("\n")
        print('Temperature Traces:')
        print('Hot-channel           [K]: ' + ', '.join('{0:.2f}'.format(F) for F in TH))
        print('Hot-channel wall temp [K]: ' + ', '.join('{0:.2f}'.format(F) for F in TWH))
        print('Cold-channel wall temp[K]: ' + ', '.join('{0:.2f}'.format(F) for F in TWC))
        print('Cold-channel          [K]: ' + ', '.join('{0:.2f}'.format(F) for F in TC))
        print('\n')
        print('Pressures Traces:')
        print('Hot-channel  [Pa]: ' + ', '.join('{0:.1f}'.format(F) for F in PH))
        print('Cold-channel [Pa]: ' + ', '.join('{0:.1f}'.format(F) for F in PC))
        print('\n')
        print('Quality Traces:')
        print('Hot-channel  [-]: ' + ', '.join('{0:.2f}'.format(F) for F in QH))
        print('Cold-channel [-]: ' + ', '.join('{0:.2f}'.format(F) for F in QC))
        print('\n')
        print('Enthalpy Traces:')
        print('Hot-channel  [J/kg]: ' + ', '.join('{0:.3f}'.format(F) for F in hH))
        print('Cold-channel [J/kg]: ' + ', '.join('{0:.3f}'.format(F) for F in hC))
        print('\n')
        print('Mass Flow Rate:')
        print('Hot-channel  [kg/s]: {0:.3f}'.format(F.mdotH))
        print('Cold-channel [kg/s]: {0:.3f}'.format(F.mdotC))

        if GetFluidPropLow(EosH, [CP.PQ_INPUTS, PH[0], 1], [CP.iT]) is None:
            S_in = 'supercritical'
        else:
            if TH[0] > GetFluidPropLow(EosH, [CP.PQ_INPUTS, PH[0], 1], [CP.iT]) and QH[0] < 0:
                S_in = 'gas'
            elif TH[0] < GetFluidPropLow(EosH, [CP.PQ_INPUTS, PH[0], 1], [CP.iT]) and QH[0] < 0:
                S_in = 'liquid'
            else:
                S_in = 'two-phase'
        if GetFluidPropLow(EosH, [CP.PQ_INPUTS, PH[-1], 1], [CP.iT]) is None:
            S_out = 'supercritical'
        else:
            if TH[-1] > GetFluidPropLow(EosH, [CP.PQ_INPUTS, PH[-1], 1], [CP.iT]) and QH[-1] < 0:
                S_out = 'gas'
            elif TH[-1] < GetFluidPropLow(EosH, [CP.PQ_INPUTS, PH[-1], 1], [CP.iT]) and QH[-1] < 0:
                S_out = 'liquid'
            else:
                S_out = 'two-phase'
        print("\n \n")
        print("Power Transferred - (H) channel")
        print("Fluid:", F.fluidH)
        print('T_in    (K): {0:.2f}     Q_in (-): {1:.2f}     ({2})'.format(TH[0], QH[0], S_in))
        print('T_out   (K): {0:.2f}     Q_in (-): {1:.2f}     ({2})'.format(TH[-1], QH[-1], S_out))
        print('Delta T (K): {0:.2f}'.format(abs(TH[0]-TH[-1])))
        # print(F.PH_out,TH[-1],GetFluidPropLow(EosH,[CP.PT_INPUTS,F.PH_out,TH[-1]],[CP.iHmass]))
        ###
        P_inH = hH_in*F.mdotH
        P_outH = hH_out*F.mdotH
        rho_in = GetFluidPropLow(EosH,[CP.PT_INPUTS,F.PH_in,TH[0]],[CP.iHmass])
        rho_out = GetFluidPropLow(EosH,[CP.PT_INPUTS,F.PH_out,TH[-1]],[CP.iHmass])
        mu_in = GetFluidPropLow(EosH,[CP.PT_INPUTS,F.PH_in,TH[0]],[CP.iviscosity])
        mu_out = GetFluidPropLow(EosH,[CP.PT_INPUTS,F.PH_out,TH[-1]],[CP.iviscosity])
        print('Power   (kW): {0:.3f}'.format((P_inH - P_outH)/1e3))
        if not G.HXtype=="UA" and not G.HXtype=="UA_dT" and not G.HXtype=="pinch_dT" and not G.HXtype=="dT_map" :
            print('Reynolds number (max): %.2f ' %(max(rho_in *F.mdotH/G.AH/rho_in * G.Lc_H / mu_in)))
            print('Reynolds number (min): %.2f ' %(min(rho_out *F.mdotH/G.AH/rho_out * G.Lc_H / mu_out )))
        print("\n")

        if GetFluidPropLow(EosC,[CP.PQ_INPUTS,PC[0],1],[CP.iT]) is None:
            S_in = 'supercritical'
        else:
            if TC[0] >  GetFluidPropLow(EosC,[CP.PQ_INPUTS,PC[0],1],[CP.iT]) and QC[0] < 0:
                S_in = 'gas'
            elif TC[0] < GetFluidPropLow(EosC,[CP.PQ_INPUTS,PC[0],1],[CP.iT]) and QC[0] < 0:
                S_in = 'liquid'
            else:
                S_in = 'two-phase'
        if GetFluidPropLow(EosC,[CP.PQ_INPUTS,PC[-1],1],[CP.iT]) is None:
            S_in = 'supercritical'
        else:
            if TC[-1] > GetFluidPropLow(EosC,[CP.PQ_INPUTS,PC[-1],1],[CP.iT]) and QC[-1] < 0:
                S_out = 'gas'
            elif TC[-1] < GetFluidPropLow(EosC,[CP.PQ_INPUTS,PC[-1],1],[CP.iT]) and QC[-1] < 0:
                S_out = 'liquid'
            else:
                S_out = 'two-phase'

        print("Power Transferred - (C) channel")
        print("Fluid:", F.fluidC)
        if M.co_flow:
            print('T_in    (K): %.2f     Q_in (-): %.2f     (%s)' %(TC[0] ,QC[0] ,S_in))
            print('T_out   (K): %.2f     Q_in (-): %.2f     (%s)' %(TC[-1],QC[-1],S_out))
            print('Delta T (K): {0:.3f}'.format(abs(TC[0]-TC[-1])))
        else:
            print('T_in    (K): %.2f     Q_in (-): %.2f     (%s)' %(TC[-1],QC[-1],S_out))
            print('T_out   (K): %.2f     Q_in (-): %.2f     (%s)' %(TC[0] ,QC[0] ,S_in))
            print('Delta T (K): {0:.3f}'.format(abs(TC[0]-TC[-1])))

        P_inC = hC_in*F.mdotC
        P_outC = hC_out*F.mdotC
        rho_in = GetFluidPropLow(EosC,[CP.HmassP_INPUTS,hH_in,F.PC_in],[CP.iDmass])
        rho_out = GetFluidPropLow(EosC,[CP.HmassP_INPUTS,hH_out,F.PC_out],[CP.iDmass])
        mu_in = GetFluidPropLow(EosC,[CP.HmassP_INPUTS,hH_in,F.PC_in],[CP.iviscosity])
        mu_out = GetFluidPropLow(EosC,[CP.HmassP_INPUTS,hH_out,F.PC_out],[CP.iviscosity])
        print('Power   (kW): {0:.3f}'.format((P_inC - P_outC)/1e3))
        if not G.HXtype=="UA" and not G.HXtype=="UA_dT" and not G.HXtype=="pinch_dT" and not G.HXtype=="dT_map" :
            print('Reynolds number (max): %.2f ' %(max(rho_in *F.mdotC/G.AC/rho_in * G.Lc_C / mu_in)))
            print('Reynolds number (min): %.2f ' %(min(rho_out *F.mdotC/G.AC/rho_out * G.Lc_C / mu_out )))
        print("\n \n")

        print('Heat Transfer Info:')
        print('Pinch point delta T (K): %.2f' %( min(abs(TH-TC))))
        DT_A = TH[0] - TC[0]; DT_B = TH[-1] - TC[-1]
        T_LM = (DT_B - DT_A) / np.log(DT_B/DT_A)
        #T_LM = abs(((TH[-1]-TC[0]) - (TH[0]-TC[-1])) / (np.log( (TH[-1]-TC[0])/(TH[0]-TC[-1]) ) ))
        print('Delta T Log Mean (K): %.2f' %( T_LM ))
        if not G.HXtype=="UA" and not G.HXtype=="UA_dT" and not G.HXtype=="pinch_dT" and not G.HXtype=="dT_map" :
            print('HTC  (W /(m K): %.2f' %( abs(P_inC - P_outC) / G.Area / abs(T_LM) ))
        if G.HXtype=="UA" or G.HXtype=="UA_dT":
            print('U (W/(m^2 K): %2.f' %G.U ) 
            print('Area   (m^2): %2.f' %G.Area ) 

        print("\n \n")
        print("Power Transferred - (H) channel [kW]: {0:.6f}".format((hH_in - hH_out)*F.mdotH/1e3))
        print("Power Transferred - (C) channel [kW]: {0:.6f}".format((hC_in - hC_out)*F.mdotC/1e3))


    if not '--noplot' in uoDict:
        # plot graphs
        plot_HX(TH,TWH,TWC,TC,M.N_cell)

        plot_HXq(Q1, Q2, Q3, Q4, Q5, Q6, Q7, Q8, M.N_cell, G.HX_L)
        plot_Hp(PH,PC,M.N_cell)
        
        print("\n \n")
        plt.draw()

        plt.pause(1)
        print('\n \n')
        input("<Hit Enter To Close Figures>")
        plt.close()

    # prepare data for output
    if M.co_flow:
        PH_out = PH[-1]
        PC_out = PC[-1]
        TH_out = TH[-1]
        TC_out = TC[-1]

    elif not M.co_flow:
        PH_out = PH[-1]
        PC_out = PC[0]
        TH_out = TH[-1]
        TC_out = TC[0]

    else:
        MyError('Flow direction not defined.')

    # print(PH_out, TH_out, PC_out, TC_out, PH, TH, PC, TC, HT0, hH_in, hH_out, hC_in, hC_out)
    return PH_out, TH_out, PC_out, TC_out, PH, TH, PC, TC, HT0, hH_in, hH_out, hC_in, hC_out
###
###
def plot_HX(TH, TWH, TWC, TC, N_cell):
    fig = plt.figure()
    plt.plot(np.linspace(0, 1., num=N_cell+1), TH, '--', label="(H) Channel")
    if len(TWH) > 0:
        plt.plot(np.linspace(0.5/N_cell,1.-0.5/N_cell,num=N_cell),TWH,'o--',label="Wall")
        plt.plot(np.linspace(0.5/N_cell,1.-0.5/N_cell,num=N_cell),TWC,'o-',label="Wall")
    plt.plot(np.linspace(0,1.,num=N_cell+1),TC,label = "(C) Channel")
    plt.ylabel('Temperature (K)')
    plt.xlabel('Position along Heat Exchanger (normalised)')
    plt.title('Temperature Distributions in Heat Exchanger')
    plt.legend(loc=1)
###
###
def plot_Hp(PH, PC, N_cell):
    fig, ax1  = plt.subplots()
    l1 = ax1.plot(np.linspace(0,1.,num=N_cell+1),PH,'--',label="Pressure (H) Channel")
    ax2 = ax1.twinx()
    l2 = ax2.plot(np.linspace(0,1.,num=N_cell+1),PC, label = "Pressure (C) Channel")
    ax1.set_ylabel('Pressure H channel (Pa)')
    ax2.set_ylabel('Pressure C channel (Pa)')
    plt.xlabel('Position along Heat Exchanger (normalised)')
    plt.title('Pressure Distributions in Heat Exchanger')
    lines = l1+l2
    labels = [l.get_label() for l in lines]
    plt.legend(lines,labels,loc=2)

###
###
def plot_HXq(Q1, Q2, Q3, Q4, Q5, Q6, Q7, Q8, N_cell,Length):
    fig = plt.figure()
    plt.plot(np.linspace(0,1.,num=N_cell+1)[0:-1]            ,np.array(Q1)/1.e3,label="q1")
    plt.plot(np.linspace(0,1.,num=N_cell+1)[1:]              ,np.array(Q2)/1.e3,label="q2")
    plt.plot(np.linspace(0.5/N_cell,1.-0.5/N_cell,num=N_cell),np.array(Q3)/1.e3,label="q3")
    plt.plot(np.linspace(0.5/N_cell,1.-0.5/N_cell,num=N_cell),np.array(Q4)/1.e3,label="q4")
    plt.plot(np.linspace(0,1.,num=N_cell+1)[0:-1]            ,np.array(Q5)/1.e3,label="q5")
    plt.plot(np.linspace(0,1.,num=N_cell+1)[1:]               ,np.array(Q6)/1.e3,label="q6")
    plt.plot(np.linspace(0.5/N_cell,1.-0.5/N_cell,num=N_cell),np.array(Q7)/1.e3,label="q7")
    plt.plot(np.linspace(0.5/N_cell,1.-0.5/N_cell,num=N_cell),np.array(Q8)/1.e3,label="q8")
    plt.ylabel('Heat Flow (kW)')
    plt.xlabel('Position along Heat Exchanger (normalised)')
    plt.title('Energy Fluxes in Heat Exchanger')
    plt.legend(loc=2)
    ###
    ###
    fig = plt.figure()
    plt.plot(np.linspace(0.5/N_cell,1.-0.5/N_cell,num=N_cell),np.array(Q3)/1.e3/(Length/N_cell),label="q3")
    plt.plot(np.linspace(0.5/N_cell,1.-0.5/N_cell,num=N_cell),np.array(Q4)/1.e3/(Length/N_cell),label="q4")
    plt.plot(np.linspace(0.5/N_cell,1.-0.5/N_cell,num=N_cell),np.array(Q7)/1.e3/(Length/N_cell),label="q7")
    plt.plot(np.linspace(0.5/N_cell,1.-0.5/N_cell,num=N_cell),np.array(Q8)/1.e3/(Length/N_cell),label="q8")
    plt.ylabel('Flow per unit length (kW/m)')
    plt.xlabel('Position along Heat Exchanger (normalised)')
    plt.title('Energy Fluxes in Heat Exchanger')
    plt.legend(loc=4)


shortOptions = ""
longOptions = ["help", "job=", "verbosity=", "noplot"]


def printUsage():
    """Print usage instructions for HX_solver."""
    print("")
    print("Usage: HX_solver.py [--help] [--job=<jobFileName>] [--verbosity=<0,1,2>] [--noplot]")
    print("\n")
    print(" --help      Display help.")
    print("\n")
    print(" --job=      Use this to specify the job file.")
    print("\n")
    print(" --verbosity   Set level of screen output 0-none; 1-some; 2-all.")
    print("\n")
    print(" --noplot    This suppresses the plotting of graphs.")
    return


class MyError(Exception):
    def __init__(self, value):
        self.value = value
    def __str__(self):
        return repr(self.value)


class TheTerminator(Exception):
    """
    Custom class used to prematurely terminate Scipy optimization and root
    finding routines.
    """
    def __init__(self):
        """Initialise class."""
        Exception.__init__(self)


if __name__ == "__main__":
    """Execute file as a program."""
    userOptions = getopt(sys.argv[1:], shortOptions, longOptions)
    uoDict = dict(userOptions[0])

    if len(userOptions[0]) == 0 or "--help" in uoDict:
        printUsage()
        sys.exit(1)

    # execute the code
    try:
        main(uoDict)
        print("\n \n")
        print("SUCCESS.")
        print("\n \n")

    except MyError as e:
        print("\nThis run has gone bad.")
        print(e.value)
        sys.exit(1)
