#! /usr/bin/env python3
"""
Collection of heat transfer correlations that are used by HX_solver.py

This module contains the following functions

DITTUS_BOELTER
Heat transfer is calculated using the Dittus Boelter correlation.
Validity range: n/a
Valid for ['GAS', 'LIQUID']

NATURAL_CONVECTION
Heat transfer is calculared using the Natural Convection relations.
Validity range: Re < 20000
Valid for ['GAS', 'LIQUID']

YOON_HORIZONTAL
Correlation for heat transfer of sCO2

Updated to suit better management of correlations.

Author: Ingo Jahn
Last Modified: 5/5/19
"""

import CoolProp as CP
import CoolProp.CoolProp as CPCP
from HX_fluids import *
import numpy as np

import numpy as np
import CoolProp as CP
import CoolProp.CoolProp as CPCP
from HX_fluids import *
#from HX_solver import MyError

'''
Each heat transfer / friction facto correlation should be defined as an outright 
class. The class should then be initialised and added to the respective list of
heat transfer and friction factor correlations.
'''

# define available correlation
heatTransferCorrelationList = []

frictionFactorCorrelationList = []


class HT_CORRELATION_TEMPLATE:
    """Class template for heat transfer."""

    def __init__(self):
        """Initialise class."""
        self.type = []  # set what type of flow is supported. Options: GAS, LIQUID, 2PHASE_CONDENSING, 2PHASE_BOILING
        self.name = 'TEST'  # name by which correlation will be called
        self.description = ''  # provide a description of the correlation
        self.reference = ''  # provide a reference for the correlation

    def calculate_HTC(self, Enthalpy, Pressure, Quality, Twall, mdot, Area, L_characteristic, EOS, CUSTOM=[]):
        """Calculate heat transfer."""
        # add formula to calculate  Nusselt number.
        HTC = None
        HTC_flag = None  # set indication how htc sgould be used.
        return [HTC, HTC_flag]

    def check_validity(self, Enthalpy, Pressure, Quality, Twall, mdot, Area, L_characteristic, EOS, CUSTOM=[]):
        """Check validaity of using ."""
        # add code to calculate if function is within validaity range
        self.validity = 'not checked'

    def write_correlation_details(self):
        """Write correlation details into output file."""
        string = 'Correlation Name={0}; Types of flow supported: {1}; Validity check:'.format(self.name, self.type, self.validity)
        if long is False:
            return string
        else:
            [string + '\nDescription: {0} \nReference: {1}'.format(self.description, self.reference)]
            return string


heatTransferCorrelationList.append(HT_CORRELATION_TEMPLATE())


class DITTUS_BOELTER:
    """Class template for heat transfer."""

    def __init__(self):
        """Initialise class."""
        self.type = ['GAS', 'LIQUID', 'SUPERCRITICAL']  # set what type of flow is supported. Options: GAS, LIQUID, 2PHASE_CONDENSING, 2PHASE_BOILING
        self.name = 'DITTUS_BOELTER'  # name by which correlation will be called
        self.description = 'Dittus Boelter Correlation for Heat Transfer'  # provide a description of the correlation
        self.reference = 'see HLT'  # provide a reference for the correlation

    def calculate_HTC(self, Enthalpy, Pressure, Quality, Twall, mdot, Area, L_characteristic, EOS, CUSTOM=[]):
        """Calculate heat transfer."""
        # Compute Bulk properties
        # Calculate bulk temperature
        enthalpyBulk = 0.5*(Enthalpy[0]+Enthalpy[1])
        pressureBulk = 0.5*(Pressure[0]+Pressure[1])
        stateBulk = [CP.HmassP_INPUTS, enthalpyBulk, pressureBulk]
        propsBulk = [CP.iPrandtl, CP.iDmass, CP.iviscosity, CP.iT, CP.iconductivity]
        [Pr, rhoBulk, muBulk, temperatureBulk, conductivityBulk] = GetFluidPropLow(EOS, stateBulk, propsBulk)

        # Calculate Reynolds number
        U = abs(mdot/(rhoBulk*Area))
        Re = rhoBulk*U*L_characteristic/muBulk

        # Dittus Boelter Equation
        if Twall > temperatureBulk:  # heating of fluid
            n = 0.3
        else:       # cooling of fluid
            n = 0.4
        Nu = 0.023 * Re**0.8 * Pr**n

        HTC = Nu * conductivityBulk / L_characteristic
        return [HTC, 'htc']

    def check_validity(self, Enthalpy, Pressure, Quality, Twall, mdot, Area, L_characteristic, EOS, CUSTOM=[]):
        """Check validaity of using ."""
        # add code to calculate if function is within validaity range
        self.validity = 'not checked'

    def write_correlation_details(self, long=False):
        """Write correlation details into output file."""
        string = 'Correlation Name={0}; Types of flow supported: {1}; Validity check:'.format(self.name, self.type, self.validity)
        if long is False:
            return string
        else:
            [string + '\nDescription: {0} \nReference: {1}'.format(self.description, self.reference)]
            return string


heatTransferCorrelationList.append(DITTUS_BOELTER())


class NATURAL_CONVECTION:
    """Class template for heat transfer."""

    def __init__(self):
        """Initialise class."""
        self.type = ['GAS', 'LIQUID', 'SUPERCRITICAL']  # set what type of flow is supported. Options: GAS, LIQUID, 2PHASE_CONDENSING, 2PHASE_BOILING
        self.name = 'NATURAL_CONVECTION'  # name by which correlation will be called
        self.description = 'Natural Convection Correlation for Heat Transfer'  # provide a description of the correlation
        self.reference = 'see HLT'  # provide a reference for the correlation

    def calculate_HTC(self, Enthalpy, Pressure, Quality, Twall, mdot, Area, L_characteristic, EOS, CUSTOM=[]):
        """Calculate heat transfer."""
        # Compute Bulk properties
        enthalpyBulk = 0.5*(Enthalpy[0]+Enthalpy[1])
        pressureBulk = 0.5*(Pressure[0]+Pressure[1])
        stateBulk = [CP.HmassP_INPUTS, enthalpyBulk, pressureBulk]
        propsBulk = [CP.iPrandtl, CP.iDmass, CP.iviscosity, CP.iT, CP.iconductivity]
        [Pr, rhoBulk, muBulk, temperatureBulk, conductivityBulk] = GetFluidPropLow(EOS, stateBulk, propsBulk)

        # Get wall properties
        rhoWall = GetFluidPropLow(EOS, [CP.PT_INPUTS, pressureBulk, Tw], [CP.iDmass])

        # Calculate Reynolds number
        U = abs(mdot/(rhoBulk*Area))
        Re = rhoBulk*U*L_characteristic/muBulk

        if Re == 0. or Re < 0.0001:
            # use Natural Convection relationship
            beta = 1./temperatureBulk
            Gr = abs(9.80665 * rhoWall**2 * L_characteristic**3 * (Twall - temperatureBulk) * beta / muBulk**2)
            GrPr = Gr*Pr
            if GrPr <= 1e9:
                C = 0.53
                n = 1./4.
            else:
                C = 0.126
                n = 1./3.
            Nu = C * GrPr**n
        else:
            # forced convection case
            if Re > 0.0001 and Re <= 0.004:
                C = 0.437
                n = 0.0985
            elif Re > 0.004 and Re <= 0.09:
                C = 0.565
                n = 0.136
            elif Re > 0.09 and Re <= 1.:
                C = 0.8
                n = 0.280
            elif Re > 1.0 and Re <= 35.:
                C = 0.795
                n = 0.384
            elif Re > 35. and Re <= 5000.:
                C = 0.583
                n = 0.471
            elif Re > 5000. and Re <= 50000.:
                C = 0.148
                n = 0.633
            elif Re > 50000. and Re <= 200000:
                C = 0.0208
                n = 0.814
            else:
                C = 0.0208
                n = 0.814
                print('Warning Correlation outside validity range')

            Nu = C * Re**n
        HTC = Nu * conductivityBulk / L_characteristic
        return [HTC, 'htc']

    def check_validity(self, Enthalpy, Pressure, Quality, Twall, mdot, Area, L_characteristic, EOS, CUSTOM=[]):
        """Check validaity of using ."""
        # Compute Bulk properties
        enthalpyBulk = 0.5*(Enthalpy[0]+Enthalpy[1])
        pressureBulk = 0.5*(Pressure[0]+Pressure[1])
        stateBulk = [CP.HmassP_INPUTS, enthalpyBulk, pressureBulk]
        propsBulk = [CP.iDmass, CP.iviscosity, CP.iT]
        [rhoBulk, muBulk, temperatureBulk] = GetFluidPropLow(EOS, stateBulk, propsBulk)

        # Calculate Reynolds number
        U = abs(mdot/(rhoBulk*Area))
        Re = rhoBulk*U*L_characteristic/muBulk

        if Re > 20000:
            self.validity = 'BAD. Upper limit of Reynolds number exceeded.'
        else:
            self.validity = 'OK'

    def write_correlation_details(self, long=False):
        """Write correlation details into output file."""
        string = 'Correlation Name={0}; Types of flow supported: {1}; Validity check:'.format(self.name, self.type, self.validity)
        if long is False:
            return string
        else:
            [string + '\nDescription: {0} \nReference: {1}'.format(self.description, self.reference)]
            return string


heatTransferCorrelationList.append(NATURAL_CONVECTION())


class YOON_HORIZONTAL:
    """Class template for heat transfer."""

    def __init__(self):
        """Initialise class."""
        self.type = ['SUPERCRITICAL']  # set what type of flow is supported. Options: GAS, LIQUID, 2PHASE_CONDENSING, 2PHASE_BOILING
        self.name = 'YOON_HORIZONTAL'  # name by which correlation will be called
        self.description = 'Yoon correlation for sCO2 in pipes for Tb > Tpc'  # provide a description of the correlation
        self.reference = 'see HLT'  # provide a reference for the correlation

    def calculate_HTC(self, Enthalpy, Pressure, Quality, Twall, mdot, Area, L_characteristic, EOS, CUSTOM=[]):
        """Calculate heat transfer."""
        # Compute Bulk properties
        enthalpyBulk = 0.5*(Enthalpy[0]+Enthalpy[1])
        pressureBulk = 0.5*(Pressure[0]+Pressure[1])
        stateBulk = [CP.HmassP_INPUTS, enthalpyBulk, pressureBulk]
        propsBulk = [CP.iPrandtl, CP.iDmass, CP.iviscosity, CP.iT, CP.iconductivity]
        [Pr, rhoBulk, muBulk, temperatureBulk, conductivityBulk] = GetFluidPropLow(EOS, stateBulk, propsBulk)

        # Calculate Reynolds number
        U = abs(mdot/(rhoBulk*Area))
        Re = rhoBulk*U*L_characteristic/muBulk

        # Yoon correlation for horizontal pipes
        # Compute pseudocritical temp and density according to Liao paper (in K)
        T_pc = -122.6 + 6.124*(pressureBulk*1e-5)-0.1657*(pressureBulk*1e-5)**2 \
            + 0.01773*(pressureBulk*1e-5)**2.5 - 0.0005608*(pressureBulk*1e-5)**3 + 273.15
        if temperatureBulk > T_pc:
            # Compute supercritical Nusselt number
            Nu = 0.14 * Re**0.69 * Pr**0.66
        else:
            # Compute subcritical Nusselt number
            state_pc = [CP.PT_INPUTS, pressureBulk, T_pc]
            props_pc = [CP.iDmass]
            rho_pc = GetFluidPropLow(EOS, state_pc, props_pc)
            # Compute Nusselt number correlation
            Nu = 0.013 * Re**1.0 * Pr**-0.05 * (rho_pc / rhoBulk)**1.6

        HTC = Nu * conductivityBulk / L_characteristic
        return [HTC, 'htc']

    def check_validity(self, Enthalpy, Pressure, Quality, Twall, mdot, Area, L_characteristic, EOS, CUSTOM=[]):
        """Check validaity of using ."""
        self.validity = 'not checked'

    def write_correlation_details(self, long=False):
        """Write correlation details into output file."""
        string = 'Correlation Name={0}; Types of flow supported: {1}; Validity check:'.format(self.name, self.type, self.validity)
        if long is False:
            return string
        else:
            [string + '\nDescription: {0} \nReference: {1}'.format(self.description, self.reference)]
            return string


heatTransferCorrelationList.append(YOON_HORIZONTAL())


class LIAO_HORIZONTAL:
    """Class template for heat transfer."""

    def __init__(self):
        """Initialise class."""
        self.type = ['SUPERCRITICAL']  # set what type of flow is supported. Options: GAS, LIQUID, 2PHASE_CONDENSING, 2PHASE_BOILING
        self.name = 'LIAO_HORIZONTAL'  # name by which correlation will be called
        self.description = 'Liao correlation for sCO2 in pipes'  # provide a description of the correlation
        self.reference = '???'  # provide a reference for the correlation

    def calculate_HTC(self, Enthalpy, Pressure, Quality, temperatureWall, mdot, Area, L_characteristic, EOS, CUSTOM=[]):
        """Calculate heat transfer."""
        # Compute Bulk properties
        enthalpyBulk = 0.5*(Enthalpy[0]+Enthalpy[1])
        pressureBulk = 0.5*(Pressure[0]+Pressure[1])
        stateBulk = [CP.HmassP_INPUTS, enthalpyBulk, pressureBulk]
        propsBulk = [CP.iPrandtl, CP.iDmass, CP.iviscosity, CP.iT, CP.iconductivity, CP.iUmass, CP.iCpmass]
        [Pr, rhoBulk, muBulk, temperatureBulk, conductivityBulk, internalEnergyBulk, CpBulk] = GetFluidPropLow(EOS, stateBulk, propsBulk)

        # Get wall properties
        [rhoWall, internalEnergyWall] = GetFluidPropLow(EOS, [CP.PT_INPUTS, pressureBulk, temperatureWall], [CP.iDmass, CP.iUmass])

        # Calculate Reynolds number
        U = abs(mdot/(rhoBulk*Area))
        Re = rhoBulk*U*L_characteristic/muBulk

        # Liao correlation for horizontal pipes
        Gr = abs( 9.80665 * (rhoBulk-rhoWall)*rhoBulk*L_characteristic**3 / muBulk**2)
        if temperatureWall == temperatureBulk:
            Nu = 0
        else:
            Cp_bar = (internalEnergyWall-internalEnergyBulk) / (temperatureWall-temperatureBulk)
            #print('Gr: ', Gr, 'Cp_bar: ', Cp_bar)
            Nu = 0.124 * Re**0.8 * Pr**0.4 * (Gr/Re**2)**0.203 * (rhoWall/rhoBulk)**0.842 * (Cp_bar / CpBulk)**0.384

        HTC = Nu * conductivityBulk / L_characteristic
        return [HTC, 'htc']

    def check_validity(self, Enthalpy, Pressure, Quality, Twall, mdot, Area, L_characteristic, EOS, CUSTOM=[]):
        """Check validaity of using ."""
        self.validity = 'not checked'

    def write_correlation_details(self, long=False):
        """Write correlation details into output file."""
        string = 'Correlation Name={0}; Types of flow supported: {1}; Validity check:'.format(self.name, self.type, self.validity)
        if long is False:
            return string
        else:
            [string + '\nDescription: {0} \nReference: {1}'.format(self.description, self.reference)]
            return string


heatTransferCorrelationList.append(LIAO_HORIZONTAL())


class SHAH_VERTICAL:
    """Class template for heat transfer."""

    def __init__(self):
        """Initialise class."""
        self.type = ['2PHASE_CONDENSING']  # set what type of flow is supported. Options: GAS, LIQUID, 2PHASE_CONDENSING, 2PHASE_BOILING
        self.name = 'SHAH_VERTICAL'  # name by which correlation will be called
        self.description = 'Correlation by Shah'  # provide a description of the correlation
        self.reference = 'Shah, M. M.: A general correlation for heat transfer during film condensation inside pipes. Int. J. Heat Mass Transfer 22(1979) 547-556.'

    def calculate_HTC(self, Enthalpy, Pressure, Quality, Twall, mdot, Area, L_characteristic, EOS, CUSTOM=[]):
        """Calculate heat transfer."""
        # Compute Bulk properties
        enthalpyBulk = 0.5*(Enthalpy[0]+Enthalpy[1])
        pressureBulk = 0.5*(Pressure[0]+Pressure[1])
        stateBulk = [CP.HmassP_INPUTS, enthalpyBulk, pressureBulk]
        propsBulk = [CP.iPrandtl, CP.iDmass, CP.iviscosity, CP.iT, CP.iP_critical]
        [PrBulk, rhoBulk, muBulk, temperatureBulk, pressureCritical] = GetFluidPropLow(EOS, stateBulk, propsBulk)

        # get liquid properties
        stateLiquid = [CP.PQ_INPUTS, pressureBulk, 0.]
        propsLiquid = [CP.iPrandtl, CP.iDmass, CP.iviscosity, CP.iconductivity]
        [PrLiquid, rhoLiquid, muLiquid, conductivityLiquid] = GetFluidPropLow(EOS, stateLiquid, propsLiquid)

        # Calculate Reynolds number
        w_bar = abs(mdot/(rhoLiquid*Area))
        Re_L = rhoLiquid*w_bar*L_characteristic/muLiquid

        p_star = pressureBulk / pressureCritical
        x_star = Quality

        # Shah correlation for condensation in verical pipes
        Nu = 0.023 * (Re_L**0.8) * (PrLiquid**0.4) * \
            ((1-x_star)**0.8 + (3.8 * ((1-x_star)**0.04) * (x_star**0.76)) / (p_star**0.38))

        HTC = Nu * conductivityLiquid / L_characteristic
        return [HTC, 'htc']

    def check_validity(self, Enthalpy, Pressure, Quality, Twall, mdot, Area, L_characteristic, EOS, CUSTOM=[]):
        """Check validaity of using ."""
        self.validity = 'not checked'

    def write_correlation_details(self, long=False):
        """Write correlation details into output file."""
        string = 'Correlation Name={0}; Types of flow supported: {1}; Validity check:'.format(self.name, self.type, self.validity)
        if long is False:
            return string
        else:
            [string + '\nDescription: {0} \nReference: {1}'.format(self.description, self.reference)]
            return string


heatTransferCorrelationList.append(SHAH_VERTICAL())

'''
class BLANGETTI_VERTICAL:
    """Class template for heat transfer."""

    def __init__(self):
        """Initialise class."""
        self.type = ['2PHASE_CONDENSING', '2PHASE_BOILING']  # set what type of flow is supported. Options: GAS, LIQUID, 2PHASE_CONDENSING, 2PHASE_BOILING
        self.name = 'BLANGETTI_VERTICAL'  # name by which correlation will be called
        self.description = 'Correlation by Blangetti'  # provide a description of the correlation
        self.reference = 'Blangetti, F.: Lokaler Wiirmeiibergang bei der Kondensation mit iiberlagerter Konvektion im vertikalen Rohr. Diss. Univ. Karlsruhe 1979'

    def calculate_Nu(self, Enthalpy, Pressure, Quality, Twall, mdot, Area, L_characteristic, EOS, CUSTOM=[]):
        """Calculate heat transfer."""
        # Compute Bulk properties
        enthalpyBulk = 0.5*(Enthalpy[0]+Enthalpy[1])
        pressureBulk = 0.5*(Pressure[0]+Pressure[1])
        stateBulk = [CP.HmassP_INPUTS, enthalpyBulk, pressureBulk]
        propsBulk = [CP.iPrandtl, CP.iDmass, CP.iviscosity, CP.iT, CP.iP_critical]
        [Pr, rhoBulk, muBulk, temperatureBulk, pressureCritical] = GetFluidPropLow(EOS, stateBulk, propsBulk)

        # Calculate Reynolds number
        U = abs(mdot/(rhoBulk*Area))
        Re = rhoBulk*U*L_characteristic/muBulk

        p_star = pressureBulk / pressureCritical
        x_star = Quality
        print(pressureCritical)
        print(p_star)

        # Shah correlation for condensation in verical pipes
        Nu = 0.023 * Re**0.8 * Pr**0.4 * \
            ((1-x_star)**0.8 + (3.8 * (1-x_star)**0.04 * x_star**0.76) / (p_star*0.38))

        Nu_dash = A * Re**n1 * Pr**n2 * (1. + tau_delta_star**n3)
        # Note Nu_dash is defined as:
        # Nu_dash = htc * (liquid_specific_volume/gravity)^(1/3) / thermal_conductivity_liquid

        return Nu

    def check_validity(self, Enthalpy, Pressure, Quality, Twall, mdot, Area, L_characteristic, EOS, CUSTOM=[]):
        """Check validaity of using ."""
        self.validity = 'not checked'

    def write_correlation_details(self, long=False):
        """Write correlation details into output file."""
        string = 'Correlation Name={0}; Types of flow supported: {1}; Validity check:'.format(self.name, self.type, self.validity)
        if long is False:
            return string
        else:
            [string + '\nDescription: {0} \nReference: {1}'.format(self.description, self.reference)]
            return string


heatTransferCorrelationList.append(BLANGETTI_VERTICAL())
'''


class BOIL_WATER:
    """Class template for heat transfer."""

    def __init__(self):
        """Initialise class."""
        self.type = ['2PHASE_BOILING']  # set what type of flow is supported. Options: GAS, LIQUID, 2PHASE_CONDENSING, 2PHASE_BOILING
        self.name = 'BOIL_WATER'  # name by which correlation will be called
        self.description = 'A hybrid boiling relationship based on implementation in RELAP5 MOD3_3'  # provide a description of the correlation
        self.reference = ''

    def calculate_HTC(self, Enthalpy, Pressure, quality, Twall, mdot, Area, L_characteristic, EOS, CUSTOM=[]):
        """Calculate heat transfer."""
        # Compute Bulk properties
        enthalpyBulk = 0.5*(Enthalpy[0]+Enthalpy[1])
        pressureBulk = 0.5*(Pressure[0]+Pressure[1])
        stateBulk = [CP.HmassP_INPUTS, enthalpyBulk, pressureBulk]
        propsBulk = [CP.iPrandtl, CP.iDmass, CP.iviscosity, CP.iT, CP.iP_critical, CP.iconductivity, CP.iCpmass, CP.iT]
        [PrBulk, rhoBulk, muBulk, temperatureBulk, pressureCritical, conductivityBulk, CpBulk, temperatureBulk] = GetFluidPropLow(EOS, stateBulk, propsBulk)
        print('TODO: There is an error here, by which CoolProp returns a negative Prandlt number.')

        p_star = pressureBulk / pressureCritical

        stateLiquid = [CP.PQ_INPUTS, pressureBulk, 0.]
        stateGas = [CP.PQ_INPUTS, pressureBulk, 1.]
        propsLiquid = [CP.iPrandtl, CP.iDmass, CP.iviscosity, CP.iHmass, CP.isurface_tension, CP.iconductivity, CP.iCpmass]
        propsGas = [CP.iPrandtl, CP.iDmass, CP.iviscosity, CP.iHmass, CP.iT, CP.iconductivity]
        [PrLiquid, rhoLiquid, muLiquid, hLiquid, surface_tensionLiquid, conductivityLiquid, CpLiquid] = GetFluidPropLow(EOS, stateLiquid, propsLiquid)
        [PrGas, rhoGas, muGas, hGas, TGas, conductivityGas] = GetFluidPropLow(EOS, stateGas, propsGas)
        h_fg = hGas - hLiquid  # enthalpy of phase change
        Tspt = TGas  # steam saturation temperature

        Psat_based_on_Twall = GetFluidPropLow(EOS, [CP.QT_INPUTS, 1., Twall], [CP.iP])

        '''
        if PrBulk < 0:
            print('Pr:', PrBulk, conductivityBulk, muBulk, CpBulk, quality)
            print('Pr_liquid={}, Pr_gas={}', PrLiquid, PrGas)
            print(Pressure, Enthalpy, temperatureBulk)
        '''


        # Calculate Reynolds number
        UBulk = abs(mdot/(rhoBulk*Area))
        ULiquid = abs(mdot/(rhoLiquid*Area))
        UGas = abs(mdot/(rhoGas*Area))
        ReBulk = rhoBulk*UBulk*L_characteristic/muBulk
        ReLiquid = rhoLiquid*ULiquid*L_characteristic/muLiquid
        ReGas = rhoGas*UGas*L_characteristic/muGas

        if quality < 0.05:  # treat as liquid
            print('quality < 0.1 --> treat as liquid')
            # caculate Dittus Boeller contribution
            n = 0.3  # only need to consider heatinbg case
            Nu_DB = 0.023 * ReLiquid**0.8 * PrLiquid**n
            HTC_DB = Nu_DB * conductivityLiquid / L_characteristic
            HTC_flag = 'htc'
            return [HTC_DB, HTC_flag]
        elif quality > 0.95:  # reat as gas
            print('quality > 0.9 --> treat as gas')
            # caculate Dittus Boeller contribution
            n = 0.3  # only need to consider heatinbg case
            Nu_DB = 0.023 * ReGas**0.8 * PrGas**n
            HTC_DB = Nu_DB * conductivityGas / L_characteristic
            HTC_flag = 'htc'
            return [HTC_DB, HTC_flag]
        else:
            # calculate heat transfer using saturated boiling.
            HTC_satBoil = self.saturatedBoilingHTC(mdot, quality, rhoGas, rhoLiquid, muLiquid, muGas,
                                                   conductivityLiquid, ReBulk, PrBulk, L_characteristic,
                                                   CpLiquid, surface_tensionLiquid, h_fg, Twall, Tspt, Psat_based_on_Twall-pressureBulk)
            q_area = HTC_satBoil * (temperatureBulk - Twall)

            # calculate critical Heat Transfer
            q_cr = self.criticalBoiling(mdot, Area, L_characteristic, p_star, quality)

            print('Q check: Q_satBoil={0}; Q_critical={1}'.format(q_area, q_cr))
            # decide on heat transfer mode.
            if False:  # abs(q_area) < abs(q_cr):  # heat transfer less than critical value
                HTC_flag = 'htc'
                return [HTC_satBoil, HTC_flag]
            else:
                print('Have exceed q_critical and using DB instead')
                # caculate Dittus Boeller contribution
                n = 0.3  # only need to consider heatinbg case
                PrBulk = quality*PrGas + (1-quality)*PrLiquid
                ReBulk = quality*ReGas + (1-quality)*ReLiquid
                Nu_DB = 0.023 * ReBulk**0.8 * PrBulk**n
                HTC_DB = Nu_DB * conductivityBulk / L_characteristic
                HTC_flag = 'htc'
                return [HTC_DB, HTC_flag]

    def criticalBoiling(self, mdot, area, L_characteristic, p_star, quality):
        """Calculate Critical Boiling."""
        # use equation developed by Doroshchuk at al
        mdot_area = mdot/area
        x_star = quality
        q_cr = (10.3 - 17.5*p_star + 8 * p_star*p_star) \
            * ((8e-3/L_characteristic)**0.5) \
            * ((mdot_area/1000)**(0.68*p_star - 1.2*x_star - 0.3)) \
            * np.exp(-1.5 * x_star)
        return q_cr * 1e6  # convert from MW to W

    def saturatedBoilingHTC(self, mdot, Quality, rhoGas, rhoLiquid, muLiquid, muGas,
                            conductivityLiquid, ReBulk, PrBulk, L_characteristic,
                            CpLiquid, surface_tensionLiquid, h_fg, Twall, Tspt, delta_P):
        """Calculate Satureated boiling by Chen."""
        mdotGas = mdot*Quality
        mdotLiquid = mdot-mdotGas

        chi_inv = ((mdotGas/mdotLiquid)**0.9) * ((rhoLiquid/rhoGas)**0.5) * ((muGas/muLiquid)**0.1)
        if chi_inv > 100:
            chi_inv = 100
        if chi_inv < 0.1:
            F = 1.0
        else:
            F = 2.35 * ((chi_inv + 0.213)**0.736)

        Re_f = mdotLiquid * L_characteristic / muLiquid
        Re_tp = min(70, 1e-4 * Re_f * (F**1.25))

        if Re_tp < 32.5:
            S = (1. + 0.12 * Re_tp)**-1.14
        elif Re_tp >= 70:
            S = 0.0797
        else:
            S = (1. + 0.42 * (Re_tp**0.78))**-1.

        # caculate Dittus Boeller contribution
        n = 0.3  # only need to consider heatinbg case
        Nu_DB = 0.023 * ReBulk**0.8 * PrBulk**n
        HTC_DB = Nu_DB * conductivityLiquid / L_characteristic

        # calculate Forster-Zuber contribution
        delta_Tw = Twall - Tspt
        # delta_P = None
        sigma = surface_tensionLiquid  # surfaceTension
        HTC_FZ = 0.00122 * ((conductivityLiquid**0.79) * (CpLiquid**0.45) * (rhoLiquid**0.49) * (9.80665**0.25)) \
            / ((sigma**0.5) * (muLiquid**0.29) * (h_fg**0.24) * (rhoGas**0.24)) \
            * (delta_Tw**0.24) * (delta_P**0.75)

        return HTC_DB * F + HTC_FZ * S

    def check_validity(self, Enthalpy, Pressure, Quality, Twall, mdot, Area, L_characteristic, EOS, CUSTOM=[]):
        """Check validaity of using ."""
        self.validity = 'not checked'

    def write_correlation_details(self, long=False):
        """Write correlation details into output file."""
        string = 'Correlation Name={0}; Types of flow supported: {1}; Validity check:'.format(self.name, self.type, self.validity)
        if long is False:
            return string
        else:
            [string + '\nDescription: {0} \nReference: {1}'.format(self.description, self.reference)]
            return string


heatTransferCorrelationList.append(BOIL_WATER())


class CUSTOM_1:
    """Class template for heat transfer."""

    def __init__(self):
        """Initialise class."""
        self.type = ['SUPERCRITICAL']  # set what type of flow is supported. Options: GAS, LIQUID, 2PHASE_CONDENSING, 2PHASE_BOILING
        self.name = 'CUSTOM_1'  # name by which correlation will be called
        self.description = 'Custom correlation of form \nNu = N.A*(Re**N.B)*(Pr**N.C)*((Gr/Re**2)**N.D)*((rho_w/rho_b)**N.E)*((Cp_bar/Cp_b)**N.F)\n N.A to N.F are ad justable'  # provide a description of the correlation
        self.reference = 'ask Viv Bone'  # provide a reference for the correlation

    def calculate_Nu(self, Enthalpy, Pressure, Quality, Twall, mdot, Area, L_characteristic, EOS, CUSTOM=[]):
        """Calculate heat transfer."""
        # Compute Bulk properties
        enthalpyBulk = 0.5*(Enthalpy[0]+Enthalpy[1])
        pressureBulk = 0.5*(Pressure[0]+Pressure[1])
        stateBulk = [CP.HmassP_INPUTS, enthalpyBulk, pressureBulk]
        propsBulk = [CP.iPrandtl, CP.iDmass, CP.iviscosity, CP.iT, CP.iUmass, CP.iCpmass, CP.iconductivity]
        [Pr, rhoBulk, muBulk, temperatureBulk, iBulk, CpBulk, conductivity] = GetFluidPropLow(EOS, stateBulk, propsBulk)

        # Get wall properties
        [rhoWall, iWall] = GetFluidPropLow(EOS, [CP.PT_INPUTS, pressureBulk, Tw], [CP.iDmass, CP.iUmass])

        # Calculate Reynolds number
        U = abs(mdot/(rhoBulk*Area))
        Re = rhoBulk*U*L_characteristic/muBulk

        N = CUSTOM

        # User input Nusselt number correlation
        # Nu = A *(Re**alpha)*(Pr**beta)
        # Follows same form as Liao correlation for horizontal pipes (bouyancy
        # effects are not important for this height scale)
        Gr = abs(9.80665 * (rhoBulk-rhoWall) * rhoBulk * L_characteristic**3 / muBulk**2)
        if Twall == temperatureBulk:
            Nu = 0
        else:
            Cp_bar = (iWall-iBulk) / (Twall-temperatureBulk)
            Nu = N.A * (Re**N.B) * (Pr**N.C) * ((Gr/Re**2)**N.D) * ((rhoWall/rhoBulk)**N.E) * ((Cp_bar/CpBulk)**N.F)

        HTC = Nu * conductivity / L_characteristic
        return HTC

    def check_validity(self, Enthalpy, Pressure, Quality, Twall, mdot, Area, L_characteristic, EOS, CUSTOM=[]):
        """Check validaity of using ."""
        self.validity = 'not checked'

    def write_correlation_details(self, long=False):
        """Write correlation details into output file."""
        string = 'Correlation Name={0}; Types of flow supported: {1}; Validity check:'.format(self.name, self.type, self.validity)
        if long is False:
            return string
        else:
            [string + '\nDescription: {0} \nReference: {1}'.format(self.description, self.reference)]
            return string


heatTransferCorrelationList.append(CUSTOM_1())


class CUSTOM_2:
    """Class template for heat transfer."""

    def __init__(self):
        """Initialise class."""
        self.type = ['SUPERCRITICAL']  # set what type of flow is supported. Options: GAS, LIQUID, 2PHASE_CONDENSING, 2PHASE_BOILING
        self.name = 'CUSTOM_2'  # name by which correlation will be called
        self.description = 'Custom correlation of form \nNu = N.A*(Re**N.B)*(Pr**N.C)*((Gr/Re**2)**N.D)*((rho_w/rho_b)**N.E)*((Cp_bar/Cp_b)**N.F)\n N.A to N.F are ad justable'  # provide a description of the correlation
        self.reference = 'ask Viv Bone'  # provide a reference for the correlation

    def calculate_Nu(self, Enthalpy, Pressure, Quality, Twall, mdot, Area, L_characteristic, EOS, CUSTOM=[]):
        """Calculate heat transfer."""
        # Compute Bulk properties
        enthalpyBulk = 0.5*(Enthalpy[0]+Enthalpy[1])
        pressureBulk = 0.5*(Pressure[0]+Pressure[1])
        stateBulk = [CP.HmassP_INPUTS, enthalpyBulk, pressureBulk]
        propsBulk = [CP.iPrandtl, CP.iDmass, CP.iviscosity, CP.iT, CP.iUmass, CP.iCpmass, CP.iconductivity]
        [Pr, rhoBulk, muBulk, temperatureBulk, iBulk, CpBulk, conductivity] = GetFluidPropLow(EOS, stateBulk, propsBulk)

        # Get wall properties
        [rhoWall, iWall] = GetFluidPropLow(EOS, [CP.PT_INPUTS, pressureBulk, Tw], [CP.iDmass, CP.iUmass])

        # Calculate Reynolds number
        U = abs(mdot/(rhoBulk*Area))
        Re = rhoBulk*U*L_characteristic/muBulk

        N = CUSTOM

        print('T_pc is not defined. Need to add missing code.')
        # Compute pseudocritical density
        state_pc = [CP.PT_INPUTS, pressureBulk, T_pc]
        props_pc = [CP.iDmass]
        rho_pc = GetFluidPropLow(EOS, state_pc, props_pc)

        # Compute supercritical Nu parameters
        Gr = abs(9.80665 * (rhoBulk-rhoWall) * rhoBulk * L_characteristic**3 / muBulk**2)
        Cp_bar = (iWall-iBulk) / (Twall-temperatureBulk)

        # Evaluate correlation
        if Twall == temperatureBulk:
            # No heat transfer condition
            Nu = 0
        elif Twall <= T_pc-N.BETA:
            # Compute subcritical (liquid) Nusselt number
            Nu = N.LIQ_A * Re**N.LIQ_B * Pr**N.LIQ_C * (rho_pc/rhoBulk)**N.LIQ_D
        elif T_pc-N.BETA < temperatureBulk < T_pc+N.ALPHA:
            # Compute blended Nusselt number (should create lambda functions for NuLiq and NuSuper)
            psi = (temperatureBulk-(T_pc-N.BETA)) / (N.ALPHA+N.BETA)
            NuLiq = N.LIQ_A * Re**N.LIQ_B * Pr**N.LIQ_C * (rho_pc/rhoBulk)**N.LIQ_D
            NuSuper = N.SC_A * (Re**N.SC_B) * (Pr**N.SC_C) * ((Gr/Re**2)**N.SC_D) * ((rhoWall/rhoBulk)**N.SC_E) * ((Cp_bar/CpBulk)**N.SC_F)
            # Compute blended Nusselt number
            Nu = (1-psi) * NuLiq + psi * NuSuper
        elif T_pc+N.ALPHA <= temperatureBulk:
            # Compute supercritical (liquid) Nusselt number
            Nu = N.SC_A * (Re**N.SC_B) * (Pr**N.SC_C) * ((Gr/Re**2)**N.SC_D) * ((rhoWall/rhoBulk)**N.SC_E) * ((Cp_bar/CpBulk)**N.SC_F)

        HTC = Nu * conductivity / L_characteristic
        return HTC

    def check_validity(self, Enthalpy, Pressure, Quality, Twall, mdot, Area, L_characteristic, EOS, CUSTOM=[]):
        """Check validaity of using ."""
        self.validity = 'not checked'

    def write_correlation_details(self, long=False):
        """Write correlation details into output file."""
        string = 'Correlation Name={0}; Types of flow supported: {1}; Validity check:'.format(self.name, self.type, self.validity)
        if long is False:
            return string
        else:
            [string + '\nDescription: {0} \nReference: {1}'.format(self.description, self.reference)]
            return string


heatTransferCorrelationList.append(CUSTOM_2())


def calc_Nu_singlePhase(Pm,Pp,Tm,Tp,Tw,mdot,A_total,L_c,Eos,Correlation,N,M):
    """
    function to calculate local Nusselt number based on bulk flow properties
    set up to work with single phase flow
    Inputs:
    P - bulk pressure (Pa)
    Tm - bulk temperature to left (K)
    Tp - bulk temperature to right (K)
    Tw - temperature of wall (K)
    mdot - total mass flow rate (kg/s)
    A_total - total flow area (m**2)
    L_c - characteristicjlength (m)
    Eos - Equation of State type
    Correlation - select correlation to be used:
        1 - Yoon correlation for sCO2 in pipes for Tb > Tpc
        2 - S.M. Liao and T.S Zhaou correlation for microchannels
            http://www.me.ust.hk/~mezhao/pdf/33.PDF
            - horizontal pipes
        3 - S.M. Liao and T.S Zhaou correlation for microchannels
            http://www.me.ust.hk/~mezhao/pdf/33.PDF
            - vertical pipes, upwards flow
        4 - - S.M. Liao and T.S Zhaou correlation for microchannels
            http://www.me.ust.hk/~mezhao/pdf/33.PDF
            - vertical pipes, downwards flow
        5 - Dittus Boelter Equation
            HLT
        6 - as per paper by Xi
        7 - Natural Convection
            HLT
        8 - J.D. Jackson and W.B. Hall ideal gas correlation
            Forced convection heat transfer to fluids at supercritical pressure,
            in Turbulent forced convection in channels and bundles.
    Outputs:
    Nu - Nusselt Number
    """
    # Calculate bulk temperature
    Tb = 0.5*(Tm+Tp)
    Pb = 0.5*(Pm+Pp)

    # check if two-phase
    stateBulk = [CP.PQ_INPUTS,Pb,1.]
    propsBulk = [CP.iT]    
    T_sat = GetFluidPropLow(Eos,stateBulk,propsBulk)

    # Compute Bulk properties      
    stateBulk = [CP.PT_INPUTS,Pb,Tb]
    propsBulk = [CP.iPrandtl,CP.iDmass,CP.iviscosity,CP.iUmass,CP.iCpmass]
    [Pr,rho_b,mu_b,i_b,Cp_b] = GetFluidPropLow(Eos,stateBulk,propsBulk)

    # Compute properties at wall
    stateWall = [CP.PT_INPUTS,Pb,Tw]
    propsWall = [CP.iDmass,CP.iUmass]
    [rho_w,i_w] = GetFluidPropLow(Eos,stateWall,propsWall)

    # Compute properties at midpoint
    stateMid = [CP.PT_INPUTS,Pb,0.5*(Tw+Tb)]
    propsMid = [CP.iDmass]
    rho_mid = GetFluidPropLow(Eos,stateMid,propsMid)

    # Calculate Reynolds number
    U = abs(mdot/(rho_b*A_total))
    Re = rho_b*U*L_c/mu_b

    if M.verbosity > 1:
        print('In single phase')        
        print('Tm,Tp,Pm,Pp:',Tm,Tp,Pm,Pp)
        print('Tsat:',T_sat)
        print('stateBulk:',stateBulk)
        print('Pr,rho_b,mu_b,i_b,Cp_b:',GetFluidPropLow(Eos,stateBulk,propsBulk))
        print('stateWall:',stateWall)
        print('rho_w,i_w:',GetFluidPropLow(Eos,stateWall,propsWall))


    if Correlation is 1:
        # Yoon correlation for horizontal pipes
        # Compute pseudocritical temp and density according to Liao paper (in K)
        T_pc = -122.6 + 6.124*(Pb*1e-5)-0.1657*(Pb*1e-5)**2 + 0.01773*(Pb*1e-5)**2.5  \
                - 0.0005608*(Pb*1e-5)**3 + 273.15
        if Tb > T_pc:
            # Compute supercritical Nusselt number
            Nu = 0.14 * Re**0.69 * Pr**0.66
        else:
            # Compute subcritical Nusselt number
            state_pc = [CP.PT_INPUTS,Pb,Tpc]
            props_pc = [CP.iDmass]
            rho_pc = GetFluidPropLow(Eos,state_pc,props_pc)
            # Compute Nusselt number correlation
            Nu = 0.013 * Re**1.0 * Pr**-0.05 * (rho_pc/rho_b)**1.6

    elif Correlation is 2:
        # Liao correlation for horizontal pipes
        Gr = abs( 9.80665 * (rho_b-rho_w)*rho_b*L_c**3 / mu_b**2)
        if Tw == Tb:
            Nu = 0
        else:
            Cp_bar = (i_w-i_b) / (Tw-Tb)
            #print('Gr: ', Gr, 'Cp_bar: ', Cp_bar)
            Nu = 0.124 * Re**0.8 * Pr**0.4 * (Gr/Re**2)**0.203 * (rho_w/rho_b)**0.842 * (Cp_bar / Cp_b)**0.384
 
    elif Correlation is 3:
        # Liao correlation for vertical pipes - upwards flow
        rho_m = 1/(Tw-Tb) * (Tw-Tb)/6. * (rho_b + 4*rho_mid + rho_w)  # integration using Simpsons rule
        Gr_m = abs( 9.80665 * (rho_b-rho_m)*rho_b*L_c**3 / mu_b**2)
        if Tw == Tb:
            Nu = 0
        else:
            Cp_bar = (i_w-i_b) / (Tw-Tb)
            #print('Gr: ', Gr, 'Cp_bar: ', Cp_bar)
            Nu = 0.354 * Re**0.8 * Pr**0.4 * (Gr_m/Re**2.7)**0.157 * (rho_w/rho_b)**1.297 * (Cp_bar / Cp_b)**0.296 

    elif Correlation is 4:
        # Liao correlation for vertical pipes - downwards flow
        rho_m = 1/(Tw-Tb) * (Tw-Tb)/6. * (rho_b + 4*rho_mid + rho_w)  # integration using Simpson's rule
        Gr_m = abs( 9.80665 * (rho_b-rho_m)*rho_b*L_c**3 / mu_b**2)
        if Tw == Tb:
            Nu = 0
        else:
            Cp_bar = (i_w-i_b) / (Tw-Tb)
            # print('Gr: ', Gr, 'Cp_bar: ', Cp_bar)
            Nu = 0.643 * Re**0.8 * Pr**0.4 * (Gr_m/Re**2.7)**0.186 * (rho_w/rho_b)**2.154 * (Cp_bar / Cp_b)**0.751 

    elif Correlation is 5:
        # For flow in circular pipes (from HLT) use for incompressible fluids (water/oil)
        # Dittus Boelter Equation
        if Tw > Tb: # heating of fluid
            n = 0.3
        else:       # cooling of fluid
            n = 0.4
        Nu = 0.023 * Re**0.8 * Pr**n

    elif Correlation is 6:
        # Correlation for shell side as per paper by Xie et al.
        C = 0.16442; m=0.65582
        e = np.exp( C + m * np.log(Re) )
        Nu = e * Pr**(1./3.)

    elif Correlation is 7:
        if Re == 0. or Re < 0.0001:
            # use Natural Convection relationship
            rho_mid = GetFluidPropLow(Eos,[CP.PT_INPUTS,Pb,Tw],[CP.iDmass])
            beta = 1./Tb
            Gr = abs( 9.80665 * rho_w**2 * L_c**3 * (Tw - Tb) * beta / mu_b**2)
            GrPr = Gr*Pr
            if GrPr <= 1e9:
                C = 0.53; n=1/4
            else:
                C = 0.126; n=1/3
            Nu = C * GrPr**n
        else:
            # forced convection case
            if Re > 0.0001 and Re <= 0.004:
                C = 0.437; n = 0.0985
            elif Re >0.004 and Re <= 0.09:
                C = 0.565; n = 0.136
            elif Re > 0.09 and Re <= 1.:
                C = 0.8; n = 0.280
            elif Re > 1.0 and Re <= 35.:
                C = 0.795; n= 0.384
            elif Re > 35. and Re <= 5000.:
                C = 0.583; n = 0.471
            elif Re > 5000. and Re <= 50000.:
                C = 0.148; n = 0.633
            elif Re > 50000. and Re <= 200000:
                C = 0.0208; n = 0.814
            else:
                raise MyError('Correlation outside range of valid Nusselt numbers.')
            Nu = C* Re**n


    elif Correlation is 8:
        # Jackson (1979) - ideal gas region correlation
        Nu = 0.0183*Re**0.82*Pr**0.5

    elif Correlation is 25:
        # User input Nusselt number correlation
        # Nu = N.A*(Re**N.alpha)*(Pr**N.beta)
        # Follows same form as Liao correlation for horizontal pipes (bouyancy
        # effects are not important for this height scale)
        Gr = abs(9.80665*(rho_b-rho_w)*rho_b*L_c**3/mu_b**2)
        if Tw == Tb:
            Nu = 0
        else:
            Cp_bar = (i_w-i_b) / (Tw-Tb)
            Nu = N.A*(Re**N.B)*(Pr**N.C)*((Gr/Re**2)**N.D)*((rho_w/rho_b)**N.E)*((Cp_bar/Cp_b)**N.F)

    elif Correlation is 26:
        # User input Nusselt number correlations with switching between
        # supercritical and liquid conditions

        # Compute pseudocritical density
        state_pc = [CP.PT_INPUTS, Pb, T_pc]
        props_pc = [CP.iDmass]
        rho_pc = GetFluidPropLow(Eos, state_pc, props_pc)

        # Compute supercritical Nu parameters
        Gr = abs(9.80665*(rho_b-rho_w)*rho_b*L_c**3/mu_b**2)
        Cp_bar = (i_w-i_b) / (Tw-Tb)

        # Evaluate correlation
        if Tw == Tb:
            # No heat transfer condition
            return 0
        elif Tb <= T_pc-N.BETA:
            # Compute subcritical (liquid) Nusselt number
            Nu = N.LIQ_A*Re**N.LIQ_B*Pr**N.LIQ_C*(rho_pc/rho_b)**N.LIQ_D
        elif T_pc-N.BETA < Tb < T_pc+N.ALPHA:
            # Compute blended Nusselt number (should create lambda functions for NuLiq and NuSuper)
            psi = (Tb-(T_pc-N.BETA))/(N.ALPHA+N.BETA)
            NuLiq = N.LIQ_A*Re**N.LIQ_B*Pr**N.LIQ_C*(rho_pc/rho_b)**N.LIQ_D
            NuSuper = N.SC_A*(Re**N.SC_B)*(Pr**N.SC_C)*((Gr/Re**2)**N.SC_D)*((rho_w/rho_b)**N.SC_E)*((Cp_bar/Cp_b)**N.SC_F)
            # Compute blended Nusselt number
            Nu = (1-psi)*NuLiq + psi*NuSuper
        elif T_pc+N.ALPHA <= Tb:
            # Compute supercritical (liquid) Nusselt number
            Nu = N.SC_A*(Re**N.SC_B)*(Pr**N.SC_C)*((Gr/Re**2)**N.SC_D)*((rho_w/rho_b)**N.SC_E)*((Cp_bar/Cp_b)**N.SC_F)

    elif Correlation is 27:
        # Liao (2002, Horizontal Pipes) and Jackson (1979) correlations with switching at N.ALPHA (~110 C)

        # Liao correlation
        Gr = abs(9.80665*(rho_b-rho_w)*rho_b*L_c**3/mu_b**2)
        Cp_bar = (i_w-i_b)/(Tw-Tb)
        NuLiao = 0.124*Re**0.8*Pr**0.4*(Gr/Re**2)**0.203*(rho_w/rho_b)**0.842*(Cp_bar/Cp_b)**0.384

        # Jackson correlation
        NuJackson = 0.0183*Re**0.82*Pr**0.5

        # Linear blending parameters: N.ALPHA - transition temp, N.BETA - HALF blending region
        if Tw == Tb:
            Nu = 0
        elif Tb <= N.ALPHA-N.BETA:
            # Use Liao Nusselt number
            Nu = NuLiao
        elif N.ALPHA-N.BETA < Tb < N.ALPHA+N.BETA:
            # Compute blended Nusselt number
            psi = (Tb-(N.ALPHA-N.BETA))/(2*N.BETA)
            # Compute blended Nusselt number
            Nu = (1-psi)*NuLiao + psi*NuJackson
        elif N.ALPHA+N.BETA <= Tb:
            # Use Jackson Nusselt number
            Nu = NuJackson

    else:
        raise MyError('Correlation option for Nusselt number calculation not implemented.')

    return Nu


def get_Correlation_List(type):
    """Return list of available fucntions."""
    NameList = []
    if type == 'ALL':
        for n in heatTransferCorrelationList:
            NameList.append(n.name)
        return NameList, heatTransferCorrelationList
    else:
        for n in heatTransferCorrelationList:
            if type in n.type:
                NameList.append(n.name)
        return NameList


class MyError(Exception):
    """Error catching exception."""

    def __init__(self, value):
        """Initilaise class."""
        self.value = value

    def __str__(self):
        """Create string."""
        return repr(self.value)
