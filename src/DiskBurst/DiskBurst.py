#! /usr/bin/env python3
""" 
Script to calculate the containment ability of cylindrical shells

Based on following paper:
Hagg A. C., Sankey G. O. (1974), The Containment of Disk Burst Fragments by 
Cylindrical Shells, Transactions of ASME, 1974. 

Author: Ingo Jahn
Last modified: 05/03/2018 
"""

import numpy as np
import sys as sys 
import os as os 
import shutil as shutil
from getopt import getopt


###
###
def main(uoDict):
    """
    main function
    """
    # create string to collect warning messages
    warn_str = "\n"

    # main file to be executed 
    jobFileName = uoDict.get("--job", "test")

    # ass .py extension is forgotten
    if not ".py" in jobFileName:
    	jobName = jobFileName
    	jobFileName = ''.join([jobFileName, ".py"])
    else:
    	# strip .py extension from jobName
    	jobName = jobFileName.replace('.py','')

    # Make sure that the desired job file actually exists
    if not jobFileName in os.listdir('.'):
    	local_directory = os.path.dirname(os.path.realpath(__file__))
    	raise MyError("No job file {0} found in directory: {1}"
    		.format(jobFileName, local_directory))

    print_flag = 0
    # set print_flag (can be overwritten from jobfile)
    print_flag = float(uoDict.get("--verbosity",0))

    # Execute jobFile, this creates all the variables
    exec(open(jobFileName).read(),globals(),locals())


    ####################
    # start calculations

    print("Dimensions")
    print("R_outer [m]:", R_outer)
    print("R_inner [m]:", R_inner)
    print("r_centroid [m]:",r_centroid)
    print("h_r [m]:",h_r)
    print("L_s [m]:",L_s)
    print("t_s [m]:",t_s)



    print("Starting Calculation:")
    # calculate rotor and fragment properties:
    theta = 2 * np.pi / N_frag      # angle of segment arc
    V1 = omega1 * r_centroid # translational speed at centroid of fragment
    M1 = (R_outer**2 - R_inner**2) * np.pi * h_r * rho_r / N_frag      # fragmnet mass
    I1 = h_r * M1 * (0.5*R_outer+0.5*R_inner)**2 * ( 1 - np.sin(theta)**2 / (theta**2) )  # fragment moment of inertia, based on shell segment (Based on standard solution for hoop element)
    E1 = 0.5*M1*V1**2 + 0.5*I1*omega1**2 # fragment energy as per Eqn (1)

    print("Fragment Properties")
    print("theta [rad]:",theta)
    print("omega [rad/s]:",omega1)
    print("V1 [m/s] / [in/s]:",V1, V1/25.4e-3)
    print("M1 [kg] / [something weird...]:",M1,M1/(14.593*12))
    print("I1 [kg.m2]:",I1)
    print("E1 [J] / [in lbs /1e6]:",E1,E1*8.8507/1e6)
    print("\n")

    ############################
    # Stage 1 calculation

    # Effective mass for Stage 1
    m21 = 1./N_frag * 2.*np.pi*R_s * t_s * rho_s * h_r # calculated using 
    a = (L_s - h_r) / 2.
    if a > 3. * t_s: # long shell case
        print("Long shell case")
        m22 = 1./N_frag * 2.*np.pi*R_s * t_s * rho_s * 3. * t_s
        #m22_eff = m22 * k**2 / R_s**2
        M2 = m21 + 0.34 * m22 # Eqn (6) Assumes a=3*T
    else: 
        print("Short shell case")
        b = (L_s - h_r) / 4.
        A_m22 = 2. * b * t_s
        I_m22 = A_m22 * ((2.*b)**2 + t_s**2) / 12.        # radfius of gyration assuming rectangular shape
        k0 = np.sqrt(I_m22/A_m22)
        m22 = 1./N_frag * 2.*np.pi*R_s * t_s * rho_s * (L_s-h_r)/2.
        temp = ( k0**2 / b**2) / ( k0**2/b**2 + 1)
        print(k0,b,temp)
        M2 = m21 + m22 * temp  # Eqn (7)

    delta_E1 = 0.5 * M1 * V1**2 * (1 - M1 / (M1+M2))

    print("delta_E1 [J] / [in lbs /1e6]:", delta_E1, delta_E1*8.8507 /1e6 )

    if h_r < 2*L_s:
        Peri = 2.*h_r + 2.* 2.*np.pi/N_frag*R_outer
    else:
        Peri = 2.* L_s  # for very short shells, assume shearing only occurs at both ends. 
        print("Perimeter reduced to end sections only")

    Acont = h_r * 2.*np.pi/N_frag*R_outer
    K = 0.3 # experimental constant in range 0.3 to 0.5

    Ec = Acont * t_s * epsilon * sigma_d
    #Es = K * tau_d * Peri * t_s**2
    Es = 0.27 * sigma_d * Peri * t_s**2

    print("m21 [kg] / [lbs]:",m21, m21 / (14.593*12))
    print("m22 [kg] / [lbs]:",m22, m22 / (14.593*12))
    print("M2 [kg] / [lbs]:",M2, M2 / (14.593*12))
    print("Ec [J] / [in lbs /1e6]:",Ec,Ec*8.8507/1e6)
    print("Es [J] / [in lbs /1e6]:",Es,Es*8.8507/1e6)
    print("Ec+Es [J] / [in lbs /1e6]:",Ec+Es,(Ec+Es)*8.8507/1e6)


    if Es + Ec > delta_E1: # criterion based on Eqn (10)
        print("no piercing")
    else:
        print("Criteria 1 FAILED")
        print("Disk will fail due to piercing")



    return 

###
###
shortOptions = ""
longOptions = ["help", "job=", "verbosity="]
###
###
def printUsage():
    print("")
    print("Usage: DiskBurst.py [--help] [--job=<jobFileName>] [--verbosity=0 (1 or 2)]")
    return


class MyError(Exception):
    def __init__(self, value):
        self.value = value
    def __str__(self):
        return repr(self.value)
###
###
if __name__ == "__main__":
    userOptions = getopt(sys.argv[1:], shortOptions, longOptions)
    uoDict = dict(userOptions[0])
    
    if len(userOptions[0]) == 0 or "--help" in uoDict:
        printUsage()
        sys.exit(1)

    # execute the code
    try:
        main(uoDict)
        print("\n \n")
        print("SUCCESS.")
        print("\n \n")

    except MyError as e:
        print("This run has gone bad.")
        print(e.value)
        sys.exit(1)

