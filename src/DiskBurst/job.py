# Define Input Parameters:

# Number of fragments
N_frag = 4

# Rotor Geometry
R_inner = 15e-3 # [m] Rotor inner radius 
R_outer = 22.5e-3 # [m] Rotor outer radius
h_r = 36e-3     # [m] Rotor Axial length
rho_r = 7800    # [kg/m3] density of rotor
theta = 2 * np.pi / N_frag 
r_centroid = (0.5*R_outer+0.5*R_inner) * np.sin(theta) / theta

# Shell Geometry
L_s = 0.08       # [m] Shell axial length
t_s = 0.1e-3      # [m] Shell wall thickness
R_s = 45e-3     # [m] shell mean radius
rho_s = 7800    # [kg/m3] density of rotor
uts_s = 400e6   # [Pa] ultimate tensile strength shell 
#yield_s = 250e6 # [Pa] yield stress
epsilon = 0.07      # as per discussion below Fig 18
#sigma_d = 0.5*uts_s + 0.5*yield_s # [Pa] average dynmaic plastic flow stress, taken as mean of uts and yield 
sigma_d = 1.25 * uts_s # set per figure 17
#tau_d = 0.5 * sigma_d   # [Pa] dynamic shear strength taken as 0.5* dynamic strength

# Operating Conditions
N_rotor = 32000   # [RPM] rotational speed

# convert to S.I units
omega1 = N_rotor / 60. * 2. * np.pi
