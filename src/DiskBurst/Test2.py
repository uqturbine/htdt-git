# Define Input Parameters:

# Rotor Geometry
R_inner = 5.8*25.4e-3/2 # [m] Rotor inner radius 
R_outer = 20*25.4e-3/2 # [m] Rotor outer radius
h_r = 1.75*25.4e-3     # [m] Rotor Axial length
rho_r = 7800    #* [kg/m3] density of rotor
r_centroid = 6.4*25.4e-3 

# Shell Geometry
L_s = 7*25.4e-3       # [m] Shell axial length
t_s = 2.5*25.4e-3      # [m] Shell wall thickness
R_s = 30*25.4e-3/2     # [m] shell mean radius
rho_s = 7800    #* [kg/m3] density of rotor

uts_s = 62e3*6894   # [Pa] ultimate tensile strength shell 
#yield_s = 250e6 # [Pa] yield stress
epsilon = 0.07     # as per discussion below Fig 18
#sigma_d = 0.5*uts_s + 0.5*yield_s # [Pa] average dynmaic plastic flow stress, taken as mean of uts and yield 
sigma_d = 1.5 * uts_s # set per figure 17
tau_d = 0.5 * sigma_d   # [Pa] dynamic shear strength taken as 0.5* dynamic strength

# Operating Conditions
V1 = 9800 *25.4e-3 
omega1 = V1 / r_centroid
N_rotor = omega1 / (2*np.pi) * 60

# Number of fragments
N_frag = 4

# convert to S.I units
omega1 = N_rotor / 60. * 2. * np.pi
