# -*- coding: utf-8 -*-
"""
Python scripts used to generate aerofoil profile for axial turbine stator &
rotor blades

Author: Hugh Russell
Created: 22/06/2018

Revision
Date
By
Note

A
22/06/2018
HR
Preliminary.

B
22/06/2018
HR
Changed ...cangle(Be,Bn1d) to cangle(xilist[i],Bn1d)
Added translate()

C
25/06/2018
HR
Fixed error in cangle() whereby slope was calculated as dy/dt and not dy/dx.
See https://math.stackexchange.com/questions/1383102
Required updates to cangle(), simposebp()

D
27/06/2018
HR
Updated variable names to suit conventions established by Ingo Jahn UQ.

E
28/06/2018
HR
Updated variable names to suit conventions established by Ingo Jahn UQ.
"""

def stag(L,x,psi1,psi2,mode='rad'):
    
    # Determine stagger angle per Schobeiri (2018) Eq. 9.37, Fig. 9.16
    # Angle naming convention has been changed
    # L=a,cax;x=c;c=cb;psi1=psi0;psi2=psi1;psis=psis 
    
    import numpy as np
    
    if mode=='deg':        
        psi1 = np.deg2rad(psi1)
        psi2 = np.deg2rad(psi2)
    
    psis = np.arctan( L / ( ( (L-x) / np.tan(psi2) ) - ( x / np.tan(np.pi-psi1) ) ) )
    
    if mode=='deg':

        psis = np.rad2deg(psis)
    
    return psis 

def aux(psis,psi1,psi2,mode='rad'):
    
    # Determine auxilliary angles per Schobeiri (2018) Fig. 9.17
    
    import numpy as np
    
    if mode=='deg':        
        psi1 = np.deg2rad(psi1)
        psi2 = np.deg2rad(psi2)
        psis   = np.deg2rad(psis)
        
    psi1aux = 0.5*np.pi+psis-psi1
    psi2aux = 0.5*np.pi-psis+psi2
    
    if mode=='deg':

        psi1aux = np.rad2deg(psi1aux)
        psi2aux = np.rad2deg(psi2aux)
    
    return psi1aux,psi2aux

def cb(psis,L, mode='rad'):
    
    # Determine chord length cb for staggered blade per Schobeiri (2018) Fig. 9.16
    
    import numpy as np
    
    if mode=='deg':        
        psis = np.deg2rad(psis)
        
    return L / np.cos(psis)

def cambez(xi,psi1aux,psi2aux,mode='rad'):
    
    # Determine outputs Bxi(xi),Beta(eta) per Schobeiri (2018) Eq. 9.41
    
    import numpy as np
 
    if mode=='deg':
        
        psi1aux = np.deg2rad(psi1aux)
        psi2aux = np.deg2rad(psi2aux)
        
    def cot(angle):
    
        return np.cos(angle)/np.sin(angle)
        
    P0e = 0.
    P0n = 0.
    
    P1e = 1. / (1. + (cot(psi1aux)/cot(psi2aux)))
    P1n = cot(psi1aux) / (1. + (cot(psi1aux)/cot(psi2aux)))
    
    P2e = 1.
    P2n = 0
    
    Be = P0e*(1.-xi)**2 + P1e*2.*xi*(1.-xi) + P2e*xi**2
    Bn = P0n*(1.-xi)**2 + P1n*2.*xi*(1.-xi) + P2n*xi**2
    
    return Be,Bn

def cambez1d(xi,psi1aux,psi2aux,mode='rad'):
    
    # Determine the first derivative (slope) outputs Bxi(xi),Beta(eta) per Schobeiri (2018) Eq. 9.41
    
    import numpy as np
 
    if mode=='deg':
        
        psi1aux = np.deg2rad(psi1aux)
        psi2aux = np.deg2rad(psi2aux)
        
    def cot(angle):
    
        return np.cos(angle)/np.sin(angle)
        
    P0e = 0.
    P0n = 0.
    
    P1e = 1. / (1. + (cot(psi1aux)/cot(psi2aux)))
    P1n = cot(psi1aux) / (1. + (cot(psi1aux)/cot(psi2aux)))
    
    P2e = 1.
    P2n = 0
    
    Be1d = 2*(1.-xi)*(P1e-P0e) + 2.*xi*(P2e-P1e)
    Bn1d = 2*(1.-xi)*(P1n-P0n) + 2.*xi*(P2n-P1n)
    
    return Be1d,Bn1d

def cangle(x,xdash,ydash,mode='rad'):
    
    """    
    Function ingests 3 values where x=f(t),xdash=f'(t)=dx/dt and ydash=f'(t)=dy/dt 
    to obtain angle of line relative to positive x axis. Note that for a Bezier curve 
    dy/dx = (dy/dt)/(dx/dt).
    """
    
    from numpy import arctan,rad2deg
    
    if mode=='deg':
        
        return x,rad2deg(arctan(ydash/xdash))
    else:
        
        return x,arctan(ydash/xdash)
    
def simposebp(x,y,xdash,ydash,d):
    
    #For a given camberline point (x,y) with local slope (y') and local thickness d, return the points
    #ppx,ppy,psx,psy that define the pressure and suction side points associated with the camberline 
    #location by superimposing a thickness.
    
    import numpy as np
    
    [x,lang] = cangle(x,xdash,ydash)
    
    ppx = x + 0.5*d*np.sin(lang)
    ppy = y - 0.5*d*np.cos(lang)
    psx = x - 0.5*d*np.sin(lang)
    psy = y + 0.5*d*np.cos(lang)    
    
    return ppx,ppy,psx,psy

def naca4(x,d):
    
    #Calculate the profile of a NACA symmetrical aerofoil per Payne, G. (1994), "NACA 6, 7, and 8 series".
    
    from numpy import sqrt
    
    y = 5*d * ( 0.2969*sqrt(x) - 0.1260*x -0.3516*x**2 + 0.2843*x**3 - 0.1015*x**4)
    
    return x,y

def scale(x,y,ox,oy,scale):
    
    #function to perform a scaling of a blade profile about a point
    
    if len(x)==len(y):
        
        xstar = [((z-ox)*scale)+ox for z in x]
        ystar = [((z-oy)*scale)+oy for z in y]
        
    return xstar,ystar

def rotate(x,y,ox,oy,theta,mode='rad'):
    
    #function to rotate coordinates around a point
    
    import numpy as np
    
    if mode=='deg':
        
        theta = np.deg2rad(theta)
    
    if len(x)==len(y):
        
        xstar = [(z-ox) for z in x]
        ystar = [(z-oy) for z in y]
        
        for i in range(0,len(x)):
            
            xp = xstar[i]
            yp = ystar[i]
            
            xstar[i] = xp*np.cos(theta)+yp*np.sin(theta)
            ystar[i] = -xp*np.sin(theta)+yp*np.cos(theta)
         
        xstar = [(z+ox) for z in xstar]
        ystar = [(z+oy) for z in ystar]
        
    return xstar,ystar

def vflip(x,y):
    
    #function to flip coordinates top / bottom
  
    if len(x)==len(y):
        
        xstar = x
        ystar = [-z for z in y]        
        
    return xstar,ystar

def translate(x,y,xshift,yshift):
    
    #function to perform a translation of points
    
    if len(x)==len(y):
        
        xstar = [z+xshift for z in x]
        ystar = [z+yshift for z in y]
        
    return xstar,ystar

if __name__ == "__main__":
    
    # Example per Schobeiri (2018) Fig. 9.20. Some parameters estimated.
    
    # Import modules
    
    from matplotlib import pyplot as plt
    import numpy as np

    # Initialize lists
    
    xilist = np.arange(0.,1.+0.001,0.001)
    clist = np.arange(0.,1.+0.001,0.001)    
    Belist = []
    Bnlist = []
    Bn1dlist = []
    ppxlist = []
    ppylist = []
    psxlist = []
    psylist = []
    dlist = []
    normlist=[]
    
    # Set parameters
    
    psi1 = 120.
    psi2 = 19.
    
    L = 40.
    x = 0.55*L
    
    # 1) Calculate stagger angle & associated chord length based on axial claim
    
    psis = stag(L,x,psi1,psi2,mode='deg')    
    chordlen = cb(psis,L,mode='deg')
    
    # 2) Calculate auxilliary angles
    
    [psi1aux,psi2aux] = aux(psis,psi1,psi2,mode='deg')
    
    for i in range(0,len(xilist)):
    
        # 3) Calculate chordline coordinates
    
        [Be,Bn] = cambez(xilist[i],psi1aux,psi2aux,mode='deg')
    
        Belist.append(Be)
        Bnlist.append(Bn)
    
        # 4) Calculate thickness distribution 
    
        [z,d] = naca4(clist[i],0.2)
    
        dlist.append(d)
    
        # 5) Calculate derivative & local angle of camber line
    
        [Be1d,Bn1d] = cambez1d(xilist[i],psi1aux,psi2aux,'deg')   
    
        Bn1dlist.append(Bn1d)
    
        [z,norm] = cangle(Be,Be1d,Bn1d,'deg')
        
        normlist.append(norm)
    
        # 6) Calculate location of pressure / suction side curve points
    
        [ppx,ppy,psx,psy] = simposebp(Be,Bn,Be1d,Bn1d,d)
    
        ppxlist.append(ppx)
        ppylist.append(ppy)
        psxlist.append(psx)
        psylist.append(psy)
    
    # 7) Scale, rotate & flip curves as required
    
    [Belist,Bnlist] = scale(Belist,Bnlist,0.,0.,L)
    [Belist,Bnlist] = rotate(Belist,Bnlist,0.,0.,90.-psis,mode='deg')
    
    [ppxlist,ppylist] = scale(ppxlist,ppylist,0.,0.,L)
    [ppxlist,ppylist] = rotate(ppxlist,ppylist,0.,0.,90.-psis,mode='deg')

    [psxlist,psylist] = scale(psxlist,psylist,0.,0.,L)
    [psxlist,psylist] = rotate(psxlist,psylist,0.,0.,90.-psis,mode='deg')
    
    #Check angles
    
    print('psi1=',psi1)
    print('psi2=',psi2)
    print('psis=',psis)
    print('chordlen=',chordlen)
    print('psi1aux=',psi1aux)
    print('psi2aux=',psi2aux)
    print('Inlet angle (raw)=',normlist[0])
    print('Outlet angle (raw)=',normlist[-1])
    print('Inlet angle (with stagger)=',psis+normlist[0])
    print('Outlet angle (with stagger)=',psis+normlist[-1])
    
    # Plotting
    
    plt.figure(figsize=(10,10))
    
    plt.plot(Belist,Bnlist,'k-')
    plt.plot(ppxlist,ppylist,'b-')    
    plt.plot(psxlist,psylist,'b-')
    
    plt.xlim([-10,50]);
    plt.ylim([-50,10]);