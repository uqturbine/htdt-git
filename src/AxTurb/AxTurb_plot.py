#! /usr/bin/env python3
"""
Module containing plotting functions for AxTurb.py

Author: Ingo Jahn
Last Modified: 21/05/2018
"""

import numpy as np
import scipy as sci
import matplotlib.pyplot as plt 

import pdb

"""
The following functions are included in this module:

plot_geometry(BLADEROW)
    Fucntion to plot cross-sectional view of axial turbine. to show evolution of 
    passge with axial stage.
    In addition a figure showing the respective velocity triangles is created.

plot_conditions(BLADEROW,fluid,cond,gdata)
    Function to plot pressure and temperature evolution along passage 
    
plot_Ts(BLADEROW,fluid,cond,gdata)
    Function to plot data on a T-s diagram

"""


###
###
def plot_geometry(BLADEROW):
    """
    function to plot passage that has been set up.
    """
    # create plot of pressure along different locations
    f, (ax1,ax2) = plt.subplots( 2, figsize=(11,8))

    # find maximum velocity
    Vmax = 0.
    for row in BLADEROW.instances:
        for Vx,Vt in zip(row.Vx1,row.Vt1):
            V1 = np.sqrt(Vx**2+Vt**2)
        for Vx,Vt in zip(row.Vx2,row.Vt2):
            V2 = np.sqrt(Vx**2+Vt**2)
        Vmax = max(V1,V2,Vmax)

    dy_max = 0.
    L_max = 0.
    L_inter = 0.01 # space applied between different stages. 
    if Vmax == 0:
        Vscale = 0.
    else:
        Vscale = L_inter / Vmax *2
    pas = 0 # index used to select appropriate passage
    row = BLADEROW.instances
    for i in range(len(row)):
        ## plot side view
        if i !=0:
            # plot link from previous row
            ax1.plot([L_max-L_inter, L_max+L_inter], # casing-connection
                     [row[i-1].r_tip2, row[i].r_tip1], 'k-',linewidth=2)
        ax1.plot([L_max+L_inter, L_max+L_inter+row[i].L[pas]], # casing
                 [row[i].r_casing1, row[i].r_casing2], 'k-',linewidth=2)

        if row[i].type == "stator":
            ax1.plot([L_max+L_inter, L_max+L_inter], [row[i].r_tip1, row[i].r_hub1], 'k:')
            ax1.plot([L_max+L_inter+row[i].L[pas], L_max+L_inter+row[i].L[pas]], 
                     [row[i].r_hub2, row[i].r_tip2], 'k:')
            ax1.plot([L_max+L_inter, L_max+L_inter+row[i].L[pas]], 
                     [row[i].r_hub1, row[i].r_hub2], 'k-')

            ax1.plot([L_max+L_inter, L_max+L_inter+row[i].L[pas]], 
                     [row[i].r_tip1, row[i].r_hub2], 'k:')
            ax1.plot([L_max+L_inter, L_max+L_inter+row[i].L[pas]], 
                     [row[i].r_hub1, row[i].r_tip2], 'k:')
        if row[i].type == "rotor":
            ax1.plot([L_max, L_max+L_inter, L_max+L_inter+row[i].L[pas], L_max+2*L_inter+row[i].L[pas]], 
                     [0., row[i].r_hub1, row[i].r_hub2, 0.], 'b-')
            ax1.plot([L_max+L_inter, L_max+L_inter, L_max+L_inter+row[i].L[pas], L_max+L_inter+row[i].L[pas]], 
                     [row[i].r_hub1, row[i].r_tip1, row[i].r_tip2, row[i].r_hub2], 'b:')
            ax1.plot([L_max+L_inter, L_max+L_inter+row[i].L[pas]], 
                     [row[i].r_tip1, row[i].r_hub2], 'b:')
            ax1.plot([L_max+L_inter, L_max+L_inter+row[i].L[pas]], 
                     [row[i].r_hub1, row[i].r_tip2], 'b:')
        ## add label
        if row[i].label == "empty":
            string = row[i].type
        else:
            if row[i].type == "rotor":
                string = "Rotor \n " + row[i].label
            if row[i].type == "stator":
                string = "Stator \n " + row[i].label
        ax1.text(0.5*(L_max+L_inter + L_max+L_inter+row[i].L[pas]), 0.5*(row[i].r_hub1) , string,horizontalalignment='center',verticalalignment='center')

        # plot blade shapes aad velocity triangles
        if row[i].type == "stator":
            # blade shape
            p0 = [L_max+L_inter, 0.]
            p1 = [L_max+L_inter + row[i].x[pas], 0. + np.tan( (row[i].phi1[pas]-90.)/180.*np.pi )*row[i].x[pas]]
            p2 = [L_max+L_inter + row[i].L[pas], 0. - row[i].c[pas] * np.cos(row[i].phiS[pas]/180.*np.pi)]
            xx,yy = plot_3P_Bezier(p0, p1, p2) 
            # camber line
            ax2.plot(xx, yy, 'k',linewidth=2)
            # tangents
            ax2.plot([p0[0], p1[0], p2[0]], [p0[1], p1[1], p2[1]], 'k:',linewidth=2) 
            # maximum thickness circle
            x_c, y_c = eval_Bezier_norm([p0, p1, p2], 0.4) # value of maximum thickness is set to 40% along camberline
            xx,yy = plot_Circle(x_c,y_c,row[i].d[pas])
            ax2.plot(xx, yy, 'k',linewidth=2)
            # thickness Trailing Edge
            ax2.plot([p2[0]+row[i].t_TE[pas]*np.cos(row[i].phi2[pas]/180.*np.pi) , p2[0]-row[i].t_TE[pas]*np.cos(row[i].phi2[pas]/180.*np.pi) ],[p2[1]+row[i].t_TE[pas]*np.sin(row[i].phi2[pas]/180.*np.pi) , p2[1]-row[i].t_TE[pas]*np.sin(row[i].phi2[pas]/180.*np.pi) ], 'k',linewidth=2)          


            # inlet velocity vector absolute
            dx=Vscale*row[i].Vx1[0]; dy=Vscale*row[i].Vt1[0]; dy_max = max(abs(dy), dy_max)
            ax2.annotate("", xytext=(p0[0]-dx, +dy), xy=(p0[0], 0.), 
                            arrowprops=dict(edgecolor='g',arrowstyle="->",shrinkA=0,shrinkB=0,linewidth=2)) 
            # outlet velocity vector absolute
            dx=Vscale*row[i].Vx2[0]; dy=Vscale*row[i].Vt2[0]; dy_max = max(abs(dy), dy_max)
            ax2.annotate("", xytext=(p2[0]+dx, p2[1]-dy), xy=(p2[0], p2[1]), 
                            arrowprops=dict(edgecolor='g',arrowstyle="<-",shrinkA=0,shrinkB=0,linewidth=2)) 

        # plot blade shapes aad velocity triangles
        if row[i].type == "rotor":
            # blade shape
            p0 = [L_max+L_inter, 0.]
            p1 = [L_max+L_inter + row[i].x[pas], 0. + np.tan( (row[i].phi1[pas]-90.)/180.*np.pi )*row[i].x[pas]]
            p2 = [L_max+L_inter + row[i].L[pas], 0. - row[i].c[pas] * np.cos(row[i].phiS[pas]/180.*np.pi)]
            xx,yy = plot_3P_Bezier(p0, p1, p2) 
            # camber line
            ax2.plot(xx, yy, 'b',linewidth=2)
            # tangents
            ax2.plot([p0[0], p1[0], p2[0]], [p0[1], p1[1], p2[1]], 'b:',linewidth=2) 
            ax2.plot([p0[0], p1[0], p2[0]], [p0[1], p1[1], p2[1]], 'k:',linewidth=2) 
            # maximum thickness circle
            x_c, y_c = eval_Bezier_norm([p0, p1, p2], 0.4)   # value of maximum thickness is set to 40% along camberline
            xx,yy = plot_Circle(x_c,y_c,row[i].d[pas])
            ax2.plot(xx, yy, 'b',linewidth=2)

            # thickness Trailing Edge
            ax2.plot([p2[0]+row[i].t_TE[pas]*np.cos(row[i].phi2[pas]/180.*np.pi) , p2[0]-row[i].t_TE[pas]*np.cos(row[i].phi2[pas]/180.*np.pi) ],[p2[1]+row[i].t_TE[pas]*np.sin(row[i].phi2[pas]/180.*np.pi) , p2[1]-row[i].t_TE[pas]*np.sin(row[i].phi2[pas]/180.*np.pi) ], 'b',linewidth=2)   

            # inlet velocity vector absolute
            dx=Vscale*row[i].Vx1[0]; dy=Vscale*row[i].Vt1[0]; dy_max = max(abs(dy), dy_max)
            ax2.annotate("", xytext=(p0[0]-dx, +dy), xy=(p0[0], 0.), 
                            arrowprops=dict(edgecolor='g',arrowstyle="->",shrinkA=0,shrinkB=0,linewidth=2)) 
            # outlet velocity vector absolute
            dx=Vscale*row[i].Vx2[0]; dy=Vscale*row[i].Vt2[0]; dy_max = max(abs(dy), dy_max)
            ax2.annotate("", xytext=(p2[0], p2[1]), xy=(p2[0]+dx, p2[1]-dy), 
                            arrowprops=dict(edgecolor='g',arrowstyle="->",shrinkA=0,shrinkB=0,linewidth=2)) 
            # inlet velocity vector relative
            dx=Vscale*row[i].Vx1[0]; dy=Vscale*( row[i].Vt1[0] - row[i].r1[pas]*row[i].Nrad ); dy_max = max(abs(dy), dy_max) 
            ax2.annotate("", xytext=(p0[0]-dx, +dy), xy=(p0[0], 0.), 
                            arrowprops=dict(edgecolor='r',arrowstyle="->",shrinkA=0,shrinkB=0,linewidth=2)) 
            # outlet velocity vector relative
            dx=Vscale*row[i].Vx2[0]; dy=Vscale*( row[i].Vt2[0] - row[i].r1[pas]*row[i].Nrad ); dy_max = max(abs(dy), dy_max) 
            ax2.annotate("", xytext=(p2[0]+dx, p2[1]-dy), xy=(p2[0], p2[1]), 
                            arrowprops=dict(edgecolor='r',arrowstyle="<-",shrinkA=0,shrinkB=0,linewidth=2)) 
            # rotational speed
            dy=Vscale*( row[i].r1[pas]*row[i].Nrad ); dy_max = max(abs(dy), dy_max) 
            ax2.annotate("", xytext=(0.5*(p0[0]+p2[0]), 0.5*(p0[1]+p2[1])-dy), xy=(0.5*(p0[0]+p2[0]), 0.5*(p0[1]+p2[1])), 
                            arrowprops=dict(edgecolor='b',arrowstyle="<-",shrinkA=0,shrinkB=0,linewidth=2))            


        # update L_max for next row
        L_max = L_max + 2*L_inter + row[i].L[pas]

    # plot centreline
    ax1.plot([0.-0.1*L_max, 1.1*L_max], [0., 0.], 'k-.',linewidth=2)  
    #ax2.plot([0.-0.1*L_max, 1.1*L_max], [0., 0.], 'k-.') 
    
    # ensure same x-axis for all
    ax1.set_xlim(-0.1*L_max,1.1*L_max)
    ax2.set_xlim(-0.1*L_max,1.1*L_max)
    ymin,ymax = ax2.get_ylim() # adjust ylim to ensure all vectors are inside.
    ax2.set_ylim(ymin-2*dy_max,ymax+2*dy_max) 
    ax2.set_aspect('equal',adjustable='datalim')

    # plot legend box for velocity arrows
    ax2.annotate(r"$u = \omega \, r$", xytext=(0.02,0.9),xy=(0.1,0.9), xycoords='axes fraction', arrowprops=dict(edgecolor='b',arrowstyle="->",shrinkA=0,shrinkB=0,linewidth=2))
    ax2.annotate(r"$V_{rel}$", xytext=(0.02,0.83),xy=(0.1,0.83), xycoords='axes fraction', arrowprops=dict(edgecolor='r',arrowstyle="->",shrinkA=0,shrinkB=0,linewidth=2))
    ax2.annotate(r"$V_{abs}$", xytext=(0.02,0.76),xy=(0.1,0.76), xycoords='axes fraction', arrowprops=dict(edgecolor='g',arrowstyle="->",shrinkA=0,shrinkB=0,linewidth=2))

###
###
def plot_conditions(BLADEROW,fluid,cond,gdata):
    """
    function to plot change in properties along the passage
    """
    # create plot of pressure along different locations
    f, (ax1,ax2,ax3,ax4) = plt.subplots( 4, figsize=(11,8))

    L_inter = 0.01
    Rows = BLADEROW.instances  
    xx = [0.]
    PT = np.zeros([gdata.passages,2*len(Rows)+1]); PT[:,0]=cond.PT_in
    TT = np.zeros([gdata.passages,2*len(Rows)+1]); TT[:,0]=cond.TT_in
    P = np.zeros([gdata.passages,2*len(Rows)+1]); P[:,0]=None
    T = np.zeros([gdata.passages,2*len(Rows)+1]); T[:,0]=None
    h = np.zeros([gdata.passages,2*len(Rows)+1]); h[:,0]=fluid.h_from_PT(cond.PT_in,cond.TT_in)
    hT = np.zeros([gdata.passages,2*len(Rows)+1]); hT[:,0]=fluid.h_from_PT(cond.PT_in,cond.TT_in)
    h_bar = np.zeros(2*len(Rows)+1); h_bar[0]=fluid.h_from_PT(cond.PT_in,cond.TT_in)
    #hT_bar = np.zeros(2*len(Rows)+1); hT_bar[0]=fluid.h_from_PT(cond.PT_in,cond.TT_in)
    M = np.zeros([gdata.passages,2*len(Rows)+1])
    M_bar = np.zeros(2*len(Rows)+1)

    pas = 0 # index used to select appropriate passage      
    i = 1
    for row in Rows:
        # Create conditions at (1) --> inlet to row
        if i == 1:
            xx.append(xx[-1]+L_inter)
        else:
            xx.append(xx[-1]+2*L_inter)
            
        for r in range(gdata.passages):
            PT[r,i] = row.PT1[r]; TT[r,i] = row.TT1[r]
            P[r,i] = row.P1[r]; T[r,i] = row.T1[r]
            h[r,i] = fluid.h_from_PT(row.P1[r],row.T1[r])
            hT[r,i] = fluid.h_from_PT(row.PT1[r],row.TT1[r])
            M[r,i] = row.M1[r]
        h_bar[i] = row.h1_bar
        M_bar[i] = row.M1_bar
        i = i+1
        
        # Create conditions at (2) --> outlet to row
        xx.append(xx[-1]+row.L[pas])

        for r in range(gdata.passages):
            PT[r,i] = row.PT2[r]; TT[r,i] = row.TT2[r]
            P[r,i] = row.P2[r]; T[r,i] = row.T2[r]
            h[r,i] = fluid.h_from_PT(row.P2[r],row.T2[r])
            hT[r,i] = fluid.h_from_PT(row.PT2[r],row.TT2[r])
            M[r,i] = row.M2[r]
        h_bar[i] = row.h2_bar
        M_bar[i] = row.M2_bar
        i = i+1

    # plot lines
    ax1.plot(xx,np.transpose(PT),"-",label="total")
    ax1.plot(xx,np.transpose(P),"--",label="static")
    ax2.plot(xx,np.transpose(TT),"-",label="total")
    ax2.plot(xx,np.transpose(T),"--",label="static")
    ax3.plot(xx,np.transpose(hT),"-",label="total")
    ax3.plot(xx,np.transpose(h),"--",label="static")
    ax3.plot(xx,h_bar,"-",linewidth=2,label="average")
    ax4.plot(xx,np.transpose(M),"-")
    ax4.plot(xx,M_bar,"-",linewidth=2,label="average")

    # find min/max for dividing lines
    P_min = min(np.amin(PT),np.amin(P[:,1:])); P_max = max(np.amax(PT),np.amax(P[:,1:]));
    T_min = min(np.amin(TT),np.amin(T[:,1:])); T_max = max(np.amax(TT),np.amax(T[:,1:]));
    h_min = np.amin(h); h_max = np.amax(h)
    M_min = np.amin(M); M_max = np.amax(M)    
     
    i = 0
    for ax,Min,Max in zip([ax1,ax2,ax3,ax4],[P_min,T_min,h_min,M_min],[P_max,T_max,h_max,M_max]):
        ax.plot( [xx[i], xx[i]], [Min,Max], 'k:')
        string = "Inlet \n (total)"
        ax.text(0.5*(xx[i]), 0.5*(Min+Max), string, horizontalalignment='right',verticalalignment='center')
    i = i+1
 
    # Add separation lines and labels  
    for row in Rows:
        for ax,Min,Max in zip([ax1,ax2,ax3,ax4],[P_min,T_min,h_min,M_min],[P_max,T_max,h_max,M_max]):
            ax.plot( [xx[i], xx[i]], [Min,Max], 'k:')
            ax.text( xx[i], 0.5*(Min+Max), "(0)", horizontalalignment='center',verticalalignment='center')
            ax.plot( [xx[i+1], xx[i+1]], [Min,Max], 'k:')
            ax.text( xx[i+1], 0.5*(Min+Max), "(1)", horizontalalignment='center',verticalalignment='center')

            ## add label
            if row.label == "empty":
                string = row[i].type
            else:
                if row.type == "rotor":
                    string = "Rotor \n " + row.label
                if row.type == "stator":
                    string = "Stator \n " + row.label
            ax.text(0.5*(xx[i]+xx[i+1]), 0.5*(Min+Max), string, horizontalalignment='center',verticalalignment='bottom')
        i = i+2

    # set axis limits
    for ax,Min,Max in zip([ax1,ax2,ax3,ax4],[P_min,T_min,h_min,M_min],[P_max,T_max,h_max,M_max]):
        ax.set_xlim(min(xx)-0.1*(max(xx)-min(xx)),max(xx)+0.1*(max(xx)-min(xx)) )
        #ax.set_ylim(Min-0.1*(Max-Min), Max+0.4*(Max-Min)) 

    # Add axis labels
    ax1.set_ylabel("Pressure (Pa)")
    ax2.set_ylabel("Temperature (K)")
    ax3.set_ylabel("Enthalpy (J/kg)")
    ax4.set_ylabel("Mach Number (-)")


###
###
def plot_Ts(BLADEROW,fluid,cond,gdata):
    """
    function to plot process on T-s diagram
    """
    # create plot for Ts
    f, (ax1) = plt.subplots( 1, figsize=(11,8))

    delta_s = 1.
    N = 10
 
    # plot initial conditions
    P_current = cond.PT_in; T_current = cond.TT_in; s_current = fluid.s_from_PT(P_current,T_current)
    ax1.plot( s_current, T_current , 'o')
    T_line,s_line = const_P_line(P_current,T_current,fluid,delta_s=delta_s,N=N)
    ax1.plot( s_line, T_line, 'k-')    
    ax1.text( s_current, T_current, "Inlet \n (total)", horizontalalignment='right',verticalalignment='bottom')

    linestyle_isen = 'k-'
    linestyle_actual = 'b:'
    width = 2.
    
    for row in BLADEROW.instances:
        ## add label
        if row.label == "empty":
            string = row[i].type
        else:
            if row.type == "rotor":
                string = "Rotor \n " + row.label
            if row.type == "stator":
                string = "Stator \n " + row.label
    
        # connect to previous row
        s = fluid.s_from_PT(row.P1_bar,row.T1_bar)
        ax1.plot([s_current, s], [T_current, row.T1_bar], 'b:')
        P_current = row.P1_bar; T_current = row.T1_bar; s_current = s
        ax1.plot( s_current, T_current , 'o')
        T_line,s_line = const_P_line(P_current,T_current,fluid,delta_s=delta_s,N=N)
        ax1.plot( s_line, T_line, 'k-')    
        ax1.text( s_current, T_current, "(0)", horizontalalignment='left',verticalalignment='bottom')

        # plot isentropic process
        T_isen = fluid.T_from_Ps(row.P2_bar,s_current)
        ax1.plot( s_current, T_isen , '*')
        ax1.plot( [s_current, s_current], [T_current, T_isen], 'k:')    
        ax1.text( s_current, T_isen, "(1_isen)", horizontalalignment='right',verticalalignment='top')

        # plot actual process
        s = fluid.s_from_PT(row.P2_bar,row.T2_bar)
        ax1.plot([s_current, s], [T_current, row.T2_bar], 'b:')
        ax1.text( 0.5*(s_current+s), 0.5*(T_current+row.T1_bar), 
            string, horizontalalignment='center',verticalalignment='center')

        P_current = row.P2_bar; T_current = row.T2_bar; s_current = s
        ax1.plot( s_current, T_current , 'o')
        T_line,s_line = const_P_line(P_current,[T_isen,T_current],fluid,delta_s=delta_s,N=N)
        ax1.plot( s_line, T_line, 'k-')    
        ax1.text( s_current, T_current, "(1)", horizontalalignment='right',verticalalignment='bottom')

    # add axis labels
    ax1.set_ylabel("Temperature (K)")
    ax1.set_xlabel("Enthroy (J/kg-K)")
###
###
def const_P_line(P,T,fluid,delta_s=1.,N=10):
    """
    function to create N points along a line of constant pressure for a T-s diagram
    the line will extend by +/- delta_s beyond both ends of the line given by h 
    """
    if not isinstance(T,list):
        T = [T] # convert T into a list

    s = []
    for t in T:
        s.append(fluid.s_from_PT(P,t))

    S = np.linspace(min(s)-delta_s,max(s)+delta_s,num=N)

    S = np.append(S,s)
    S = np.sort(S)
    T = []
    for s in S:
        T.append(fluid.T_from_Ps(P,s))
    
    return T,S
###
###
def plot_3P_Bezier(p0, p1, p2, N=10):
    """
    function to create list arrays containg Bezier curve evaluated 
    p0 = [x0, y0]
    p1 = [x1, y1]
    p2 = [x2. y2]
    """
    x = np.zeros(N); y = np.zeros(N)
    for i in range(N):
        t = float(i)/(N-1.)
        x[i],y[i] = eval_Bezier([p0, p1, p2], t)
    return x, y
###
###
def eval_Bezier(p0, t):
    """
    function to evaluate Bezier Curve at location t
    p0 = [list of control point]
    p0 = [[x0,y0], [x1, y1], ... [xN, yN]]
    """
    order = len(p0)
    x = 0.; y = 0.
    for i in range(order):
        #binominal_coeff = np.factorial(order) / ( np.factorial(i) * np.factorial(order-i) ) 
        binominal_coeff = sci.special.binom(order-1,i)
        x = x + binominal_coeff * (1.-t)**(order-1.-i) * t**(i) * p0[i][0]
        y = y + binominal_coeff * (1.-t)**(order-1.-i) * t**(i) * p0[i][1]
        # y = (1.-t)**2 * p0[1] + 2. * (1.-t) * t * p1[1] + t**2 * p2[1]
    return x, y
###
###
def eval_Bezier_norm(p0, t_norm, N=50):
    """
    function to evaluate Bezier Curve at location t_norm, where t_norm is arc-wise position
    N sets number of intervals used for normalisation
    p0 = [list of control point]
    p0 = [[x0,y0], [x1, y1], ... [xN, yN]]
    """
    # create length vector
    p_left = p0[0]
    L = []; L.append(0.)
    for i in range(N):
        t = float(i+1) / (N)
        p_right = eval_Bezier(p0, t)
        L.append( L[i] + ((p_right[0]-p_left[0])**2 + (p_right[1]-p_left[1])**2 )**0.5)
        p_left = p_right
    # normalise length vector
    for i in range(len(L)):
        L[i] = L[i] / L[-1]
    # find corresponding t
    for i in range(len(L)-1):
        if L[i+1] >= t_norm and L[i] <= t_norm:
            frac = (t_norm - L[i]) / (L[i+1] - L[i])
            t_left = float(i) / (N-1.)
            t_right = float(i+1) / (N-1.)

    # evaluate Bezier curve at new point
    t = t_left + frac*(t_right - t_left)
    x,y = eval_Bezier(p0, t)            
    
    return x, y
###
###
def plot_Circle(x_c, y_c, D, N=50):
    """
    function to create list of x/y coordinates for drawing a circle
    x_c - X-coordinate for circle centre
    y_c - Y-coordinate for circle centre
    """
    x=[]; y=[]
    for i in range(N+1):
        x.append( x_c + D/2. * np.cos(i/N * 2*np.pi) )
        y.append( y_c + D/2. * np.sin(i/N * 2*np.pi) )

    return x, y

