#! /usr/bin/env python3
"""
Python Code to evaluate on and off-design performance of Axial Turbines.

The turbine simulations follow the approach of the AXOD/AXOD2 programm by 
NASA. See following references for further details.  

Author: Ingo Jahn
Last Modified: 15/05/2018
"""

import numpy as np
import datetime as datetime
import scipy as sci
from scipy import optimize
import matplotlib.pyplot as plt 
import sys as sys 
import os as os 
import shutil as shutil
from getopt import getopt
import pdb
from AxTurb_plot import * # module containing plotting functions for AxTurb.
from AxTurb_losses import * # module that containse the loss models used by AxTurb.

###
###
class GDATA:
    def __init__(self):
        self.iter = 0  # integer to set number of iterations for fsolve
        self.name = '' # set name of simualtion.
        self.verbosity = 0 # adjust how much data will be printed to display (for debugging)
        self.optim = 'root:hybr' # select optimiser that will be used. 
        self.passages = 1
        self.name = ''
        self.flowsolver_maxiter = 100 # sets maximum number of iterations used by flow solver
        self.flowsolver_tol = 1.5e-8 # set convergence tolerance for root finder
    ###
    def check(self):
        if not self.passages == 1:
            raise MyError("Code has only been checked with gdata.passage =1 \n"
                          "Bailing Out....")
###
###
class FLUID:
    def __init__(self):
        self.type = "Ideal Gas"     # Currently only "Ideal Gas" implemented
        self.name = 'Air'           # set name of fluid
        self.Cp = 1005                  # (J /kg /K) specific heat capacity at constant pressure
        self.gamma = 1.4                # (-) (default = 1.4) ratio of specific heats
        self.Cv = self.Cp / self.gamma  # (J /kg /K) (default = calculated) specific heat capacity at constant volume 
        self.R = self.Cp-self.Cv        # (J /kg /K) (default = calculated) fluid specific cas constant
        self.T_ref = 273.15             # (K) reference temperature used to calculate enthalpy and entropy
        self.P_ref = 101530.            # (Pa) reference pressure used to calculate enthalpy and entropy
        self.viscosity_type = "Sutherland"# Currently only "Sutherland" law implemented
        self.mu_ref = 1.716e-5          # (kg /m /s)  Reference viscosity for Sutherlands law
        self.mu_T_ref = 273.15          # (K) Reference temperature for Sutherlands law
        self.mu_S = 110.4               # (K) Sutherland Temperature
    ###
    def a_from_PT(self,P,T):
        return np.sqrt(self.gamma * self.R * T)
    ###
    def h_from_PT(self,P,T):
        return self.Cp*T
    ###
    def h_from_Ps(self,P,s):
        T = self.T_from_Ps(P,s)
        return self.h_from_PT(P,T)
    ###
    def P_from_hs(self,h,s):
        return np.exp( (self.Cp * np.log(self.T_from_hs(h,s)/self.T_ref) - s ) / self.R ) * self.P_ref
    ###
    def rho_from_Ph(self,P,h):
        return P / (self.R * self.T_from_Ph(P,h))
    ###
    def rho_from_PT(self,P,T):
        return P / (self.R * T)
    ###
    def s_from_Ph(self,P,h):
        return self.Cp * np.log(self.T_from_Ph(P,h)/self.T_ref) - self.R * np.log(P/self.P_ref)
    ###
    def s_from_PT(self,P,T):
        return self.Cp * np.log(T/self.T_ref) - self.R * np.log(P/self.P_ref)
    ###
    def T_from_hs(self,h,s):
        return h / self.Cp
    ###
    def T_from_Ph(self,P,h):
        return h / self.Cp
    ###
    def T_from_Ps(self,P,s):
        return np.exp( (s + self.R * np.log(P/self.P_ref)) / self.Cp) * self.T_ref
    ###
    def mu_from_T(self,T):
        if self.viscosity_type == "Sutherland":
            return self.mu_ref * (T / self.mu_T_ref)**1.5 * (self.mu_T_ref + self.mu_S)/(T + self.mu_S)
        else:
            raise MyError("Option specified for fluid.viscosity_type not supported.")
        
    ###
    def write(self,fp):
        fp.write("Fluid name = {0} \n".format(self.name))
        fp.write("Gas Model Type = {0} \n".format(self.type))
        fp.write("Cp         (J/kg-K) = {0} \n".format(self.Cp))
        fp.write("gamma           (-) = {0} \n".format(self.gamma))
        fp.write("Cv         (J/kg-K) = {0} \n".format(self.Cv))
        fp.write("R-specific (J/kg-K) = {0} \n".format(self.R))
        fp.write("T               (K) = {0} \n".format(self.T_ref))
        fp.write("P_ref          (Pa) = {0} \n".format(self.P_ref))
###
###
class BLADEROW:
    instances = []
    def __init__(self, type, r_hub1, r_hub2, r_tip1, r_tip2, r_casing1=None, r_casing2=None, Nrpm=0., label='empty'):
        self.__class__.instances.append(self)    
        self.label = label
        self.type = type
        self.r_hub1 = r_hub1
        self.r_hub2 = r_hub2
        self.r_tip1 = r_tip1
        self.r_tip2 = r_tip2
        self.r_casing1 = r_casing1
        self.r_casing2 = r_casing2
        self.L = []
        self.r1 = []
        self.r2 = []
        self.Area1 = []
        self.Area2 = []
        self.Nrpm = Nrpm          # rotational Speed in RPM
        self.Nrad = self.Nrpm / 60. * np.pi * 2  # rotational speed in rad/s
        self.pitch = None
        self.phi1 = None
        self.phi2 = None
        self.phiS = None
        self.t_max = None
    ###
    ###
    def setup(self,gdata,cond,loss,fluid):
        self.gdata = gdata
        self.cond = cond
        self.loss = loss
        self.fluid = fluid
    ###
    ###
    def check(self):
        if (self.type != "rotor" and self.type != "stator"):
            raise MyError("blade.type for row: {0} needs to be set as rotor or stator".format(self.label))
    ###
    ###
    def calc_geom(self):
        self.c      = np.zeros(self.gdata.passages)
        self.delta  = np.zeros(self.gdata.passages) 
        self.x      = np.zeros(self.gdata.passages)
        self.phiS   = np.zeros(self.gdata.passages)

        # calculate profile specific parameters
        for i in range(self.gdata.passages):
            self.x[i] = self.xFraction[i] * self.L[i]
            y0 = np.tan( (self.phi1[i] - 90.)/180.*np.pi ) * self.x[i]
            y1 = np.tan( (self.phi2[i] - 90.)/180.*np.pi ) * (self.L[i] - self.x[i])
            self.c[i] = np.sqrt( self.L[i]**2 + (y1+y0)**2 )
            self.phiS[i] = np.arctan2( self.L[i], -(y0+y1)) /np.pi*180.             
            self.delta[i] = self.phi1[i] - self.phi2[i]

        # set empty parameters 
        if self.r_casing1 == None:
            self.r_casing1 = self.r_tip1
        if self.r_casing2 == None:
            self.r_casing2 = self.r_tip2
        # calculate passage specific information
        self.Area1Total = (self.r_tip1**2 - self.r_hub1**2) * np.pi
        self.Area2Total = (self.r_tip2**2 - self.r_hub2**2) * np.pi
        self.r1.append(self.r_hub1)
        self.r2.append(self.r_hub2)
        dr1 = (self.r_tip1 - self.r_hub1)/self.gdata.passages
        dr2 = (self.r_tip2 - self.r_hub2)/self.gdata.passages
        for i in range(self.gdata.passages):
            self.r1.append(self.r1[i] + dr1)
            self.r2.append(self.r2[i] + dr2)
            self.Area1.append((self.r1[i+1]**2 - self.r1[i]**2) * np.pi)
            self.Area2.append((self.r2[i+1]**2 - self.r2[i]**2) * np.pi)

        # passage height
        self.height = 0.5*(self.r_tip1+self.r_tip2) - 0.5*(self.r_hub1+self.r_hub2)
        self.gamma = [] # Cascade parameter as describved by Zehner_1980
        for i in range(self.gdata.passages):      
            self.gamma.append( self.eta_max[i] / self.pitch 
                             * np.sqrt( abs(self.phiS[i]) / 180.*np.pi * abs(self.delta[i]) /180.*np.pi) )

    ###
    ###
    def initialise(self):
        self.P1 = np.zeros(self.gdata.passages);        self.P2 = np.zeros(self.gdata.passages)
        self.T1 = np.zeros(self.gdata.passages);        self.T2 = np.zeros(self.gdata.passages)
        self.h1 = np.zeros(self.gdata.passages);       self.h2 = np.zeros(self.gdata.passages)
        self.PT1 = np.zeros(self.gdata.passages);       self.PT2 = np.zeros(self.gdata.passages)
        self.TT1 = np.zeros(self.gdata.passages);       self.TT2 = np.zeros(self.gdata.passages)
        self.hT1 = np.zeros(self.gdata.passages);       self.hT2 = np.zeros(self.gdata.passages)
        self.rho1 = np.zeros(self.gdata.passages);      self.rho2 = np.zeros(self.gdata.passages)
        self.Vt1 = np.zeros(self.gdata.passages);       self.Vt2 = np.zeros(self.gdata.passages)
        self.Vr1 = np.zeros(self.gdata.passages);       self.Vr2 = np.zeros(self.gdata.passages)
        self.Vx1 = np.zeros(self.gdata.passages);       self.Vx2 = np.zeros(self.gdata.passages) 
        self.M1 = np.zeros(self.gdata.passages);        self.M2 = np.zeros(self.gdata.passages)  
        self.mdot = np.zeros(self.gdata.passages) 
        self.mdot_star = np.zeros(self.gdata.passages)  
        self.Torque = np.zeros(self.gdata.passages);    self.Power = np.zeros(self.gdata.passages)
    ###
    ###
    def calc_average(self):
        self.P1_bar = 0.;   self.P2_bar = 0.
        self.T1_bar = 0.;   self.T2_bar = 0.
        self.h1_bar = 0.;   self.h2_bar = 0.
        self.M1_bar = 0.;   self.M2_bar = 0.
        for r in range(self.gdata.passages):
            self.P1_bar += self.P1[r] * self.mdot[r]
            self.P2_bar += self.P2[r] * self.mdot[r]
            self.T1_bar += self.T1[r] * self.mdot[r]
            self.T2_bar += self.T2[r] * self.mdot[r]
            self.h1_bar += self.fluid.h_from_PT(self.P1[r],self.T1[r]) * self.mdot[r]
            self.h2_bar += self.fluid.h_from_PT(self.P2[r],self.T2[r]) * self.mdot[r]
            self.M1_bar += self.M1[r] * self.mdot[r]
            self.M2_bar += self.M2[r] * self.mdot[r]
        self.P1_bar = self.P1_bar / sum(self.mdot)
        self.P2_bar = self.P2_bar / sum(self.mdot)
        self.T1_bar = self.T1_bar / sum(self.mdot)
        self.T2_bar = self.T2_bar / sum(self.mdot)
        self.h1_bar = self.h1_bar / sum(self.mdot)
        self.h2_bar = self.h2_bar / sum(self.mdot)
        self.M1_bar = self.M1_bar / sum(self.mdot)
        self.M2_bar = self.M2_bar / sum(self.mdot)
    ###
    ###
    def choked_massflow(self,Pt,Tt,fluid):
        for i in range(self.gdata.passages):
            self.mdot_star[i] = (min(self.Area1[i]*np.sin(self.phi1[i]/180.*np.pi), self.Area2[i]*np.sin(self.phi2[i]/180.*np.pi)) * Pt / np.sqrt(Tt) 
                                * np.sqrt(fluid.gamma/fluid.R)
                                * ( (fluid.gamma+1.)/2.)**(-(fluid.gamma+1.)/(2.*(fluid.gamma-1.))) )
            # TODO: This should be updated to be calculated on alpha
    ###
    ###
    def update_inlet_from_inflow(self,PT_in,TT_in,fluid):

        if self.gdata.verbosity > 1:
            print("\n")
            print("++++++++++++++++++++++++++++++++++++++")
            print("Inlet Conditions from Inflow:")

        # assume isentropic acceleration of flow from stagnation conditions
        #mdot_area = self.massTotal / self.Area0Total # this doesn't work as it overwrites the chocking check. 
        mdot_area = 0.
        for i in range(self.gdata.passages):
            mdot_area = mdot_area + self.mdot[i]
        mdot_area = mdot_area / self.Area1Total

        P_temp,T_temp,h_temp,rho_temp,M_temp,PT_temp,TT_temp,hT_temp = isentropic_expansion_with_loss_and_power(PT_in,TT_in,fluid,mdot_area, gdata=self.gdata)

        for i in range(self.gdata.passages):
            self.PT1[i] = PT_in
            self.TT1[i] = TT_in
            self.hT1[i] = hT_temp
            self.P1[i] = P_temp
            self.T1[i] = T_temp #fluid.T_from_Ps(self.P0[i],s_in)
            self.h1[i] = h_temp
            self.rho1[i] = rho_temp #fluid.rho_from_PT(self.P0[i],self.T0[i])
            self.Vx1[i] = self.mdot[i] / (self.Area1[i] * self.rho1[i])
            self.Vr1[i] = 0.  # assume the incoming flow has no radial component
            self.Vt1[i] = 0.  # assume the incoming flow has no tangential component
            #self.Vt1[i] = self.Vx1[i] / np.tan(self.alpha1[i]/180.*np.pi)  # assume the incoming flow has no tangential component
            self.M1[i] = M_temp #np.sqrt(self.Vx0[i]**2 + self.Vr0[i]**2 + self.Vt0[i]**2) / np.sqrt(fluid.gamma * self.P0[i] / self.rho0[i])
            self.mdot[i] = self.Vx1[i] * self.rho1[i] * self.Area1[i]

            if self.gdata.verbosity > 1:
                print("Comparing properties across gap:")
                print("In (PT, TT, P, T, rho):", self.PT1[i], self.TT1[i], self.P1[i], self.T1[i],  self.rho1[i])
                print("Out (Vx, Vt, Vr, rho):", self.Vx1[i], self.Vt1[i], self.Vr1[i], self.rho1[i])
    ###
    ###
    def update_inlet_conditions(self,previous,fluid, choked=False, pressure=None):
        # assume mass flow and angular momentum is conserved 
        # also assume that process is isentropic 

        if self.gdata.verbosity > 1:
            print("\n")
            print("++++++++++++++++++++++++++++++++++++++")
            print("Updating Inlet from Previous Row:")
        #print("Massflow:",self.mdot)

        for i in range(self.gdata.passages):
            self.mdot[i] = previous.mdot[i] # same mass flow throughout
            Vt_in = previous.Vt2[i]
            Vx_in = previous.Vx2[i]
            Vr_in = previous.Vr2[i]
            Ma_in = previous.M2[i]
            
            # use conservation of angular momentum to calculate Vt
            mt_in = previous.r2[i] * Vt_in * self.mdot[i]
            self.Vt1[i] = mt_in / (self.mdot[i] * self.r1[i] )
            self.Vr1[i] = 0.  # currently dont consider radial component

            mdot_area1 = self.mdot[i] / self.Area1[i]

            P_temp,T_temp,h_temp,rho_temp,M_temp,PT_temp,TT_temp,hT_temp = isentropic_expansion_with_loss_and_power(previous.PT2[i],previous.TT2[i],fluid,mdot_area1,loss=0., Vt2=self.Vt1[i],Vt2Flag="vector", gdata=self.gdata, choked=choked, Ma_in=Ma_in, pressure=pressure)

            self.PT1[i] = PT_temp
            self.TT1[i] = TT_temp
            self.hT1[i] = hT_temp
            self.P1[i] = P_temp
            self.T1[i] = T_temp #fluid.T_from_Ps(self.P0[i],s_in)
            self.h1[i] = h_temp
            self.rho1[i] = rho_temp # fluid.rho_from_PT(self.P0[i],self.T0[i])
            self.Vx1[i] = self.mdot[i] / (self.Area1[i] * self.rho1[i])
            self.M1[i] = M_temp #np.sqrt(self.Vx0[i]**2 + self.Vr0[i]**2 + self.Vt0[i]**2) / np.sqrt(fluid.gamma * self.P0[i] / self.rho0[i])

            if self.gdata.verbosity > 1:
                print("Comparing properties across gap: Vx, Vt, Vr, rho")
                print("In (Vx, Vt, Vr, rho):", Vx_in, Vt_in, Vr_in, previous.rho2[i] )
                print("Out (Vx, Vt, Vr, rho):", self.Vx1[i], self.Vt1[i], self.Vr1[i], self.rho1[i])
    ###
    ###
    def update_outlet_conditions(self,fluid):
        # calculate enthalpy change in stator or rotor stage
        if self.gdata.verbosity > 0:
            print("\n")
            print("++++++++++++++++++++++++++++++++++++++")

        if self.type == 'rotor':
            print("ROTOR calculation, updating Conditions from 1 to 2 in {0}:".format(self.label))
            # Model flow as an isentropic acceleration/decceleration from 0 to 1
            # but all in relative frame. (Rothalpy)

            for i in range(self.gdata.passages):
                mdot_area1 = self.mdot[i] / self.Area1[i]       
                mdot_area2 = self.mdot[i] / self.Area2[i] 
                
                u1 = self.r1[i] * self.Nrad               
                u2 = self.r2[i] * self.Nrad   

                # conditions upstream of leading edge.
                alpha1 = np.arctan2(self.Vx1[i], self.Vt1[i])      /np.pi*180.
                beta1  = np.arctan2(self.Vx1[i], (self.Vt1[i]-u1)) /np.pi*180.

                if self.gdata.verbosity > 0:
                    print("Inlet Velocities (Vx, Vt, Vr, u=omega*r, rho): {0:.3f}, {1:.3f}, {2:.3f}, {3:.3f}".format(self.Vx1[i],self.Vt1[i],self.Vr1[i],u1,self.rho1[i]))
                    print("Inlet Mach Number: {0:.3f}".format(self.M1[i]))
                    print("inlet flow angle (abs): {0:.3f}".format(alpha1))
                    print("inlet flow angle (rel): {0:.3f}".format(beta1))
                    print("inlet blade angle (rel): {0:.3f}".format(self.phi1[i]))

                if self.gdata.verbosity > 1:
                    print("START: Calculate Incident Losses")

                # calculate incident losses
                angle_error = [alpha1, self.phi1[i]]  
                loss_incident, lossFlag_incident = self.loss.rotorIncidentLoss(angle_error,self,i,fluid)

                P_temp,T_temp,h_temp,rho_temp,M_temp,PT_temp,TT_temp, hT_temp = isentropic_expansion_with_loss_and_power(self.PT1[i],self.TT1[i],fluid,mdot_area1,loss=loss_incident, lossFlag=lossFlag_incident, Vt1=self.Vt1[i],Vt1Flag="vector",u1=u1, Vt2=self.phi1[i],Vt2Flag="beta",u2=u2, gdata=self.gdata, Ma_in=self.M1[i])
                  
                Vx_temp = self.mdot[i] / (self.Area1[i] * rho_temp)
                Vt_temp = np.tan(self.phi1[i]/180.*np.pi) * Vx_temp +u1
                Wt_temp = Vt_temp - u1
                Vr_temp = 0.
                if self.gdata.verbosity > 1:
                    print("    After Vel1:",Vx_temp, Vt_temp)
                    print("    power1 =", (u1 * self.Vt1[i] - u1 * Vt_temp))
                    #print(P_temp,T_temp,h_temp,rho_temp,M_temp,PT_temp,TT_temp,hT_temp)
                    print("  COMPLETE: Calculate Incident Losses")

                    print("START: Calculate Passage Losses")
                # calculate passage losses
                loss_passage, lossFlag_passage = self.loss.rotorPassageLoss(P_temp,T_temp,Vx_temp,Wt_temp,Vr_temp,self,i,fluid)
                P_temp,T_temp,h_temp,rho_temp,M_temp,PT_temp,TT_temp,hT_temp = isentropic_expansion_with_loss_and_power(PT_temp,TT_temp,fluid,mdot_area1,loss=loss_passage, lossFlag=lossFlag_passage, Vt1=Vt_temp,Vt1Flag="vector",u1=u1, Vt2=self.phi2[i],Vt2Flag="beta",u2=u2, gdata=self.gdata) #, Ma_in=M_temp)

                Vt_temp_old = Vt_temp *1
                Vx_temp = self.mdot[i] / (self.Area2[i] * rho_temp)
                Vt_temp = np.tan(self.phi2[i]/180.*np.pi) * Vx_temp +u2
                Wt_temp = Vt_temp - u2
                Vr_temp = 0.
                if self.gdata.verbosity > 1:
                    print("    After Vel2:",Vx_temp, Vt_temp, Vt_temp_old)
                    print("    power2 =", (u1 * Vt_temp_old - u2 * Vt_temp))
                    #print(P_temp,T_temp,h_temp,rho_temp,M_temp,PT_temp,TT_temp,hT_temp)
                    print("  COMPLETE: Calculate Passage Losses")

                    print("START: Trailing Edge Losses")
                # calculate trailing edge losses
                loss_trailing, lossFlag_trailing = self.loss.statorTrailingLoss(P_temp,T_temp,Vx_temp,Wt_temp,Vr_temp,self,i,fluid)
                P_temp,T_temp,h_temp,rho_temp,M_temp,PT_temp,TT_temp,hT_temp = isentropic_expansion_with_loss_and_power(PT_temp,TT_temp,fluid,mdot_area2,loss=loss_trailing, lossFlag=lossFlag_trailing, Vt2=Vt_temp,Vt2Flag="vector", gdata=self.gdata, Ma_in=M_temp)
                # Note there is no power extraction in trailing edge section
                # Note Vt is set using vector to enforce conservation of angular momentum

                Vx_temp = self.mdot[i] / (self.Area2[i] * rho_temp)
                Vt_temp = np.tan(self.phi2[i]/180.*np.pi) * Vx_temp +u2
                Vr_temp = 0.

                if self.gdata.verbosity > 1:
                    print("    After Vel3:",Vx_temp, Vt_temp, Vr_temp)
                    print("  COMPLETE: Calculate Trailing Edge Losses")

                # update outlet conditions
                self.PT2[i] = PT_temp
                self.TT2[i] = TT_temp
                self.hT2[i] = hT_temp
                self.P2[i] = P_temp
                self.T2[i] = T_temp
                self.h2[i] = h_temp
                self.rho2[i] = rho_temp
                self.Vx2[i] = self.mdot[i] / (self.Area2[i] * self.rho2[i])
                self.Vt2[i] = np.tan(self.phi2[i]/180.*np.pi) * self.Vx2[i] + u2
                self.Vr2[i] = 0.
                self.M2[i] = M_temp
                self.Torque[i] = self.mdot[i] * (self.r1[i]*self.Vt1[i] - self.r2[i]*self.Vt2[i]) 
                self.Power[i] = self.Torque[i] * self.Nrad 

                if self.gdata.verbosity > 0:
                    print("Torque (Nm): {0:.3f}".format(self.Torque[i]))
                    print("Power (W): {0:.3f}".format(self.Power[i]))
                    print("power (J/kg): {0:.3f}".format(self.Power[i]/self.mdot[i]))
                    print("Exit Velocities (Vx, Vt, Vr, u=omega*r, rho): {0:.3f}, {1:.3f}, {2:.3f}, {3:.3f}".format(self.Vx2[i], self.Vt2[i], self.Vr2[i], u1, self.rho2[i]))

                    print("outlet flow angle (abs): {0:.3f}".format(np.arctan2(self.Vx2[i], self.Vt2[i])/np.pi*180.))
                    print("outlet flow angle (rel): {0:.3f}".format(np.arctan2(self.Vx2[i], (self.Vt2[i]-u2))/np.pi*180.))
                    print("outlet blade angle (rel): {0:.3f}".format(self.phi2[i]))

        if self.type == 'stator':
            print("STATOR calculation, updating Conditions from 1 to 2 in {0}:".format(self.label))
            # Model flow as an isentropic acceleration/decceleration from 0 to 1

            for i in range(self.gdata.passages):
                mdot_area1 = self.mdot[i] / self.Area1[i]      
                mdot_area2 = self.mdot[i] / self.Area2[i]  

                # conditions upstream of leading edge.
                alpha1 = np.arctan2(self.Vx1[i],self.Vt1[i])/np.pi*180.

                if self.gdata.verbosity > 0:
                    print("Inlet Velocities (Vx, Vt, Vr, rho): {0:.3f}, {1:.3f}, {2:.3f}, {3:.3f}".format(self.Vx1[i],self.Vt1[i],self.Vr1[i],self.rho1[i]))
                    print("Inlet Mach Number (M): {0:.3f}".format(self.M1[i])) 
                    print("inlet flow angle (abs): {0:.3f}".format(alpha1))
                    print("inlet blade angle (abs): {0:.3f}".format(self.phi1[i]))
                    #print(self.PT0[i],self.TT0[i],self.hT0[i])
                    print("Mass flow per area: {0:.3f}, {1:.3f}".format(mdot_area1, mdot_area2))

                if self.gdata.verbosity > 1:
                    print("START: Calculate Incident Losses")


                # calculate incident losses
                angle_error = [alpha1, self.phi1[i]]  
                loss_incident, lossFlag_incident = self.loss.statorIncidentLoss(angle_error,self,i,fluid)
                P_temp,T_temp,h_temp,rho_temp,M_temp,PT_temp,TT_temp,hT_temp = isentropic_expansion_with_loss_and_power(self.PT1[i],self.TT1[i],fluid,mdot_area1,loss=loss_incident,lossFlag=lossFlag_incident,Vt2=self.phi1[i],Vt2Flag="alpha", gdata=self.gdata, Ma_in=self.M1[i])

                Vx_temp = self.mdot[i] / (self.Area1[i] * rho_temp)
                Vt_temp = 1. / np.tan(self.phi1[i]/180.*np.pi) * Vx_temp
                Vr_temp = 0.
                if self.gdata.verbosity > 1:
                    print("    After Vel1:",Vx_temp, Vt_temp)
                    #print(PT_temp,TT_temp,hT_temp)
                    print("  COMPLETE: Calculate Incident Losses")

                    print("START: Calculate Passage Losses")
                # calculate passage losses
                loss_passage, lossFlag_passage = self.loss.statorPassageLoss(P_temp,T_temp,Vx_temp,Vt_temp,Vr_temp,self,i,fluid)
                P_temp,T_temp,h_temp,rho_temp,M_temp,PT_temp,TT_temp,hT_temp = isentropic_expansion_with_loss_and_power(PT_temp,TT_temp,fluid,mdot_area1,loss=loss_passage,lossFlag=lossFlag_passage,Vt2=self.phi2[i],Vt2Flag="alpha", gdata=self.gdata, Ma_in=M_temp)

                Vx_temp = self.mdot[i] / (self.Area2[i] * rho_temp)
                Vt_temp = 1. / np.tan(self.phi2[i]/180.*np.pi) * Vx_temp
                Vr_temp = 0.
                if self.gdata.verbosity > 1:
                    print("    After Vel2:",Vx_temp, Vt_temp, rho_temp)
                    #print(PT_temp,TT_temp,hT_temp)
                    print("  COMPLETE: Calculate Passage Losses")
                    
                    print("START: Calculate Trailing Egde Losses")

                # calculate trailing edge losses
                loss_trailing, lossFlag_trailing = self.loss.statorTrailingLoss(P_temp,T_temp,Vx_temp,Vt_temp,Vr_temp,self,i,fluid)
                P_temp,T_temp,h_temp,rho_temp,M_temp,PT_temp,TT_temp,hT_temp = isentropic_expansion_with_loss_and_power(PT_temp,TT_temp,fluid,mdot_area1,loss=loss_trailing, lossFlag=lossFlag_trailing, Vt2=Vt_temp,Vt2Flag="vector", gdata=self.gdata, Ma_in=M_temp)
                # Note Vt is set using a vector to enforce conservation of angular momentum.  
                if self.gdata.verbosity > 1:
                    print("  COMPLETE: Calculate Trailing Egde Losses")

                # update outlet conditions
                self.PT2[i] = PT_temp
                self.TT2[i] = TT_temp
                self.hT2[i] = hT_temp
                self.P2[i] = P_temp
                self.T2[i] = T_temp
                self.h2[i] = hT_temp
                self.rho2[i] = rho_temp
                self.Vx2[i] = self.mdot[i] / (self.Area2[i] * self.rho2[i])
                self.Vt2[i] = 1. / np.tan(self.phi2[i]/180.*np.pi) * self.Vx2[i]
                self.Vr2[i] = 0.
                self.M2[i] = M_temp
                self.Torque[i] = self.mdot[i] * (self.r2[i]*self.Vt2[i] - self.r1[i]*self.Vt1[i]) 
                self.Power[i] = self.Torque[i] * self.Nrad 
                if self.gdata.verbosity > 0:
                    print("Exit Velocities (Vx, Vt, Vr, rho): {0:.3f}, {1:.3f}, {2:.3f}, {3:.3f}".format(self.Vx2[i], self.Vt2[i], self.Vr2[i],self.rho2[i]))
                    print("outlet flow angle (abs): {0:.3f}".format(np.arctan2((self.Vx2[i]),self.Vt2[i])/np.pi*180.))
                    print("outlet blade angle (abs): {0:.3f}".format(self.phi2[i]))
                    #print(self.PT1[i],self.TT1[i],self.hT1[i])
    ###
    ###
    def write(self,fp):
        '''
        function to write data to output file
        '''
        fp.write("+BLADEROW+")
        if self.type == "stator":
            fp.write("Stator: \n")
        if self.type == "rotor":
            fp.write("Rotor: \n")
        fp.write("Label = {0} \n".format(self.label)) 
        fp.write("Speed (RPM) = {0}".format(self.Nrpm))
        fp.write("Torque (Nm) = {0}".format(self.Torque))
        fp.write("Power (W) = {0}".format(self.Power))
        fp.write("mdot (kg/s) = {0}".format(self.mdot))
        fp.write("              Positon 1;      Postion 2 \n")
        fp.write("Ptotal (Pa) = {0}; {1}".format(self.PT1,self.PT2))
        fp.write("Ttotal  (K) = {0}; {1}".format(self.TT1,self.TT2))
        fp.write("P      (Pa) = {0}; {1}".format(self.P1,self.P2))
        fp.write("T       (K) = {0}; {1}".format(self.T1,self.T2))
        fp.write("rho (kg/m3) = {0}; {1}".format(self.rho1,self.rho2))
        fp.write("Mach    (-) = {0}; {1}".format(self.M1,self.M2))
        fp.write("Vx    (m/s) = {0}; {1}".format(self.Vx1,self.Vx2))
        fp.write("Vt    (m/s) = {0}; {1}".format(self.Vt1,self.Vt2))
        fp.write("Vr    (m/s) = {0}; {1}".format(self.Vr1,self.Vr2))
###
###
class CONDITIONS:
    def __init__(self):
        self.PT_in = np.nan      # (Pa) Total Pressure at inlet  
        self.TT_in = np.nan      # (K) Total Temperature at inlet
        self.P_out_target = np.nan      # (Pa) Target outlet pressure (static)
        self.P_out = np.nan      # (Pa) Achieved outlet pressure (static)
        self.T_out = np.nan      # (K) Achieved outlet temperature
        self.mdot = np.nan       # (kg/s) mass flow rate 
    ###
    def check(self):
        if np.isnan(self.PT_in):
            raise MyError("PT_in, total pressure at Inlet needs to be specified")
        if np.isnan(self.TT_in):
            raise MyError("TT_in, total temperature at Inlet needs to be specified")
        if np.isnan(self.P_out_target):
            raise MyError("P_out, static pressure at Outlet needs to be specified")
    ###
    def calc_total_outlet(self,fluid):
        s_out = fluid.s_from_PT(self.P_out, self.T_out)
        h_out = fluid.h_from_PT(self.P_out, self.T_out)
        rho_out = fluid.rho_from_PT(self.P_out, self.T_out)
        self.hT_out = h_out + 0.5*( self.Vx_out**2 + self.Vt_out**2 + self.Vr_out**2)
        self.PT_out = fluid.P_from_hs(self.hT_out,s_out)
        self.TT_out = fluid.T_from_hs(self.hT_out,s_out) 
    ###
    def calc_eta(self,mdot,fluid):
        # get inlet conditions (total)
        s_in = fluid.s_from_PT(self.PT_in, self.TT_in)
        hT_in = fluid.h_from_PT(self.PT_in, self.TT_in)
        # get outlet conditions for an isentropic process
        h_isen = np.average(fluid.h_from_Ps(self.P_out, s_in))
        hT_isen = np.average(fluid.h_from_Ps(self.PT_out, s_in))
        # prepare lists for actual outlet conditions
        self.h_out = np.zeros(len(self.T_out))
        self.eta = np.zeros(len(self.T_out))
        self.eta_tt = np.zeros(len(self.T_out))
        self.T_bar = 0.; self.TT_bar = 0.
        for i in range(len(self.T_out)):
            self.h_out[i] = fluid.h_from_PT(self.P_out, self.T_out[i])
            self.eta[i] = (hT_in - self.hT_out[i]) / (hT_in - h_isen)
            self.eta_tt[i] = (hT_in - self.hT_out[i]) / (hT_in - hT_isen)
            self.T_bar += self.T_out[i] * mdot[i]
            self.TT_bar += self.TT_out[i] * mdot[i]
        self.T_bar = self.T_bar / sum(mdot)     # calculate mass weigthed avergae 
        self.TT_bar = self.TT_bar / sum(mdot)   # calculate mass weigthed avergae 
        # cacluate other actual outlet conditions. 
        self.h_out_bar = fluid.h_from_PT(self.P_out, self.T_bar)
        self.hT_out_bar = fluid.h_from_PT(self.PT_out, self.TT_bar)
        self.eta_bar = (hT_in - self.hT_out_bar) / (hT_in - h_isen)
        self.eta_tt_bar = (hT_in - self.hT_out_bar) / (hT_in - hT_isen)
        self.power = hT_in - self.hT_out_bar
        self.Power = self.power * sum(self.mdot)
    ###
    def write(self,fp):
        '''
        function to write data to output file
        '''
        fp.write("stuff =  \n")     
        fp.write("Total Pressure at Inlet PT_in   (Pa) = {0} \n".format(self.PT_in))
        fp.write("Total Temperature at Inlet TT_in (K) = {0} \n".format(self.TT_in))
        fp.write("Static Pressure at Outlet PT_in (Pa) = {0} \n".format(self.P_out))
        fp.write("Isentropic Efficiency by passage Total-Static (-) = {0} \n".format(self.eta))
        fp.write("Mass-weighted Isen Efficiency Total-Static    (-) = {0} \n".format(self.eta_bar))
###
###
def isentropic_expansion_with_loss_and_power(PT,TT,fluid,mdot_area, loss=0., lossFlag="absolute",Vt1=0.,Vt1Flag="vector",u1=0.,Vt2=0.,Vt2Flag="vector",u2=0.,Vr2=0.,Vr2Flag="vector", gdata=None, Ma_in=None, choked=False, pressure=None):
    """
    Function to evaluate the throat pressure for a fluid to accelerate from 
    total conditions hT and sT to achieve a given mass flow per unit area
    Inputs:
     TT -       Total temperature (K) 
     PT -       Total pressure (Pa)
     fluid -    fluid as used for equation of state call
     mdot_area -mass flux (kg/s-m2) that is to be achieved in the axial direction, (i.e. Vx * rho)
     u        - rotational speed in m/s (omega*r)
     loss -   Enthlapy loss (J/kg) that takes place in the process. 
     lossFlag - Define type of enthalpy loss, either proprotional to k.e. or absolute.
    Outputs:
     P_throat - Pressure (Pa) at throat
     T_throat - Temperature (K) at throat
     M_throat - Mach number (-) at throat 
    """

    # set conditions additonal inputs for root finder from gdata
    if gdata == None:
        maxiter = 200.
        tol = 1.48e-8
    else:
        maxiter = gdata.flowsolver_maxiter
        tol = gdata.flowsolver_tol

    #args = (PT, TT, fluid, mdot_area, h_loss, Vt, 1)
    #delta_h = sci.optimize.newton(massflow_loss,h0,args=args) # iterate enthalpy change to get correct mass flow


    # sarting point for iterative solver.
    if choked or (Ma_in != None and Ma_in > 1): # for supersonic outflow
        delta_h_start = 40.e3       # TODO: replace this by function to set appropriate delta_h to ensure MAch > 1 solution is obtained.
    else:
        delta_h_start = 0.  # for subsonic outflow


    fun = lambda delta_h:  massflow_loss_power(delta_h,PT,TT,fluid, loss=loss, lossFlag=lossFlag,Vt1=Vt1,Vt1Flag=Vt1Flag,u1=u1,Vt2=Vt2,Vt2Flag=Vt2Flag,u2=u2,Vr2=Vr2,Vr2Flag=Vr2Flag,flag=1, pressure=pressure)-mdot_area

    # use newtons method
    #delta_h = sci.optimize.newton(fun, delta_h_start, maxiter=maxiter, tol=tol) # iterate enthalpy change to get correct mass flow

    # use scipy.root with hybr method.
    sol = sci.optimize.root(fun, delta_h_start, method='hybr',options={'xtol':1.e-8, 'maxfev':maxiter})
    status = sol.status
    delta_h = sol.x
    mesg = sol.message

    # get remainig properties at throat
    P_throat,T_throat,h_throat,rho_throat,M_throat,PT_throat,TT_throat,hT_throat = massflow_loss_power(delta_h,PT,TT,fluid, loss=loss, lossFlag=lossFlag,Vt1=Vt1,Vt1Flag=Vt1Flag,u1=u1,Vt2=Vt2,Vt2Flag=Vt2Flag,u2=u2,Vr2=Vr2,Vr2Flag=Vr2Flag,flag=0, pressure=pressure)
    #print(P_throat,T_throat,h_throat,rho_throat,M_throat,PT_throat,TT_throat)

    return P_throat,T_throat,h_throat,rho_throat,M_throat,PT_throat,TT_throat,hT_throat
###
###
def massflow_loss_power(delta_h,PT,TT,fluid, loss=0., lossFlag="absolute",Vt1=0.,Vt1Flag="vector",u1=0.,Vt2=0.,Vt2Flag="vector",u2=0.,Vr2=0.,Vr2Flag="vector",flag=0, pressure=None):
    """
    function to calculate the per unit area axial mass flow for a fluid, which
    stagnation conditions PT and TT. The in the process of the accelerating the 
    fluid can exprience lossed with magnitude h_loss and when crossing the exit 
    plane the fluid can have velocity components in the r and t direction 
    (perpendicular to axial)

    Inputs:
     delta_h -  change in enthalpy (j/kg) required to give fluid change in Vx.
     PT -       Total pressure (Pa)
     TT -       Total temperature (K)
     fluid -    Fluid class
     loss -   Loss in enthalpy. Can be specified as absolute loss or 
                proportional loss by setting h_lossFlag
     lossFlag - "absolute" an absolute value of enthalpy (J/kg) will be 
                substracted from delta_h
                "proportional_to_ke_abs" loss is a fraction of kinetic energy = 0.5 * (Vx**2 + Vr**2 + Vt**2)
                "proportional_to_ke_rel" loss is a fraction of kinetic energy = 0.5 * (Vx**2 + Vr**2 + Vt**2)
                "zeta_rel" loss defined as zeta = 1 - (w/w_isen)**2, where w is the relative velocity
     Vt -       Tangential velocity. Can be specified as vector component or 
                angle as defined by VtFlag
     VtFlag -   "vector" Vt is specified as a velocity component (m/s)
                "alpha" absolute angle (deg) between axial direction and velocity 
                vector in x-t plane 
                "beta" relarive angle (deg) between axial direction and velocity 
                vector in x-t plane 
     Vr -       Radial velocity. Can be specified as vector component or 
                angle as defined by VrFlag
     VtFlag -   "vector" Vr is specified as a velocity component (m/s)
                "angle" angle (deg) between axial direction and velcoity vector 
                in x-r plane 
    flag -      0/1 switches between output options. 
                0 --> parameters at outlet
                1 --> axial mass flow
    Outputs:

    """
    # velocity vector [Vx, Vt, Vr]
    # if angle is specified: 
    #   Vt/Vx = tan(Vt_angle)
    #   Vr/Vx = tan(Vr_angle)

    #print("in mass flow calc") 

    delta_h_Vx2 = abs(delta_h)
    VX2 = np.sqrt(2*delta_h_Vx2)
    hT = fluid.h_from_PT(PT,TT)
    sT = fluid.s_from_PT(PT,TT)

    if Vt1Flag == "vector":
        VT1 = Vt1
    else:
        raise MyError("Setting for Vt1Flag = {0} not supported".format(Vt1Flag))
    delta_h_Vt1 = 0.5 * VT1**2 
    
    if Vt2Flag == "vector":
        VT2 = Vt2
    elif Vt2Flag == "alpha":
        VT2 = 1./ np.tan(Vt2/180.*np.pi) * VX2
    elif Vt2Flag == "beta":
        VT2 = 1./ np.tan(Vt2/180.*np.pi) * VX2 + u2
    else:
        raise MyError("Setting for Vt2Flag = {0} not supported".format(Vt2Flag))
    delta_h_Vt2 = 0.5 * VT2**2 

    if Vr2Flag == "vector":
        VR2 = Vr2
    elif Vr2Flag == "angle":
        VR2 = 1./ np.tan(Vr2/180.*np.pi) * VX2
    else:
        raise MyError("Setting for Vr2Flag = {0} not supported".format(VrFlag))
    delta_h_Vr2 = 0.5 * VR2**2 


    if lossFlag == "zeta_rel":
        lossFlag = "proportional_to_ke_rel"  # zeta is equivalent to fraction loss compared to attainable isentropic velocity

    if lossFlag == "absolute":
        h_loss = loss
    elif lossFlag == "proportional_to_ke_abs":
        h_loss = loss * (delta_h_Vx2 + delta_h_Vr2 + delta_h_Vt2)
    elif lossFlag == "proportional_to_ke_rel":
        h_loss = loss * (delta_h_Vx2 + delta_h_Vr2 + 0.5*(VT2-u2)**2)
    elif lossFlag == "Y_pressure":
        pass
    else: 
        raise MyError("Setting for lossFlag = {0} not supported".format(lossFlag))

    #power removed, based on Euler equations
    power = u1 * VT1 - u2 * VT2 


    #print("u1,u2:",u1,u2,"VT1,VT2:",VT1,VT2)
    #print("enthalpy:", delta_h_Vx2 , delta_h_Vt2, delta_h_Vr2, "h_loss:", h_loss, "power:", power)   

    if lossFlag != "Y_pressure":
        if pressure == None:
            # calculate downstream conditions for cases where a loss (in units of energy) is defined.
            hisen = hT - delta_h_Vx2 - delta_h_Vt2 - delta_h_Vr2 - power - h_loss # Vt is contribution from tangential velocity 
            Pisen = fluid.P_from_hs(hisen,sT)
            Tisen = fluid.T_from_Ph(Pisen,hisen)
            rhoisen = fluid.rho_from_PT(Pisen,Tisen)
            aisen = fluid.a_from_PT(Pisen,Tisen)

            h_actual = hisen + h_loss
            P2 = Pisen
            T2 = fluid.T_from_Ph(P2,h_actual)
        else:
            # for case where downstream pressure is prescribed. Here an additional loss (loss_slowing) exists, which corresponds to oblique shocks reducing the flow velocity
            Pisen = pressure
            #hisen = hT - delta_h_Vx2 - delta_h_Vt2 - delta_h_Vr2 - power - h_loss - loss_slowing  
            hisen = fluid.h_from_Ps(Pisen,sT)
            h_actual = hT - delta_h_Vx2 - delta_h_Vt2 - delta_h_Vr2 - power
            
            P2 = Pisen
            T2 = fluid.T_from_Ph(P2,h_actual)                        
            
    else: 
        if pressure != None:
            raise MyError("setting pressure for isentropic expansion calc together with a loss model is not supported.")

        # calculate downstream conditions for case where a reduction in total pressure is defined. 
        Y = loss
        h_actual = hT - delta_h_Vx2 - delta_h_Vt2 - delta_h_Vr2 - power 
        if fluid.type == "Ideal Gas":
            if power != 0.:
                PT = fluid.P_from_hs(hT - power,sT)  # calculate total pressure loss due to isentropic work extraction
            T2 = fluid.T_from_Ph(np.nan,h_actual)  #h_actual / fluid.Cp
            P2 = PT * 1./ ( 1. + (1. + Y) * 1./(fluid.R * T2) * 1./2. *(VX2**2 + VR2**2 + VT2**2) )
            PT_temp = P2 + P2 / (fluid.R * T2) * (VX2**2 + VR2**2 + VT2**2) / 2.
        else: 
            # non-ideal gas requires iterative solution.
            # Current solution is based on simplification that rho2 = rho_isen
            # TODO: apply iterative solution to above to calculate dynamic pressure, q based on actual pressure
            print("Approximating pressure loss factor Y calculation by using rho_2 = rho_isen")
            Pisen = fluid.P_from_hs(h_actual,sT)
            Tisen = fluid.T_from_Ph(Pisen,h_actual)
            rhoisen = fluid.rho_from_PT(Pisen,Tisen)

            q_isen = PT - Pisen  # calculate dynamic pressure (ideally this would be based on P_actual)
            print('q_isen1',q_isen)
            q_isen = rhoisen / 2. * (VX2**2 + VR2**2 + VT2**2)
            print('q_isen2',q_isen)
            delta_P = Y * q_isen  # apply pressure loss
            print('delta_P, Y',delta_P, Y)

            PT_temp = PT - delta_P
            s_actual = fluid.s_from_Ph(PT_temp,hT)
            P2 = fluid.P_from_hs(h_actual,s_actual)
            T2 = fluid.T_from_Ph(P2,h_actual)
        #print("hT,h_actual,TT,T2",hT,h_actual,hT/fluid.Cp,T2) 
        #print("Total Pressure Loss - PT_in, PT_out, delta_P, power:",PT,PT_temp,PT-PT_temp,power)
        #print("P2",P2)


    P_actual = P2
    T_actual = T2
    rho_actual = fluid.rho_from_PT(P_actual,T_actual)
    a_actual = fluid.a_from_PT(P_actual,T_actual)    
 
    M_actual = np.sqrt(VX2**2+VT2**2+VR2**2)/a_actual

    #if M_actual > 1.:
    #    print("Flow is chocked, Mach = {0}".format(M_actual))
    #    delta_h_Vx2 = 0.9 * delta_h_Vx2

    #print("Velocities:",M_actual, VX2,VT2,VR2)

    if flag == 0:
        # also calculate updated total conditions.
        s_actual = fluid.s_from_PT(P_actual,T_actual)
        if lossFlag == "Y_pressure":
            hT = h_actual+0.5*(VX2**2+VT2**2+VR2**2)
        else:
            hT = h_actual+0.5*(VX2**2+VT2**2+VR2**2)            
        PT = fluid.P_from_hs(hT,s_actual)
        TT = fluid.T_from_Ph(P_actual,hT)
        return P_actual,T_actual,h_actual,rho_actual,M_actual,PT,TT,hT
    elif flag == 1:
        mdot = VX2 * rho_actual
        return mdot
    else:
        raise MyError("Wrong setting for flag.")
###
###
def equations(A, BLADEROW, cond, fluid, gdata):
    """
    function to be solved by optimiser
    Inputs:

    Outputs:
    error - vector containing miss-balance in mass flow, pressure and temperature.
    """

    Rows = BLADEROW.instances
    if gdata.verbosity > 0:
        print("\n") 
        print("######################################")
        print("#### Starting new Evaluation Loop - Find sub-sonic Solution ####")
        print("######################################")
        print("Input variables to loop: A=",A)
    
    massFlow = abs(A[0])

    cond.massFlow.append(float(massFlow))
    #print("Massflow:",cond.massFlow)

    # update mass flow for each Blade row
    for row in Rows:
        row.massTotal = massFlow
        row.mdot[0] = massFlow


    # calculate chocked mass flow for each row and check choking
    for i in range(gdata.passages):
        mdot_star_min = np.inf # find minimum massflow for all rows for each passage
        for row in Rows:
            row.choked_massflow(cond.PT_in,cond.TT_in,fluid)
            mdot_star_min = min(row.mdot_star[i], mdot_star_min)
        # now adjust mass flow is required. 
        if mdot_star_min < row.mdot[i]:
            if gdata.verbosity > 0:          
                print("Chocking check, mdot={0}, mdot_star={1}".format(row.mdot[i],mdot_star_min)) 
                print("Chocking check, mdot/area={0}, mdot_star/area={1}".format(row.mdot[i]/row.Area2[i],mdot_star_min/row.Area2[i])) 
                print("fluid chocked and mass flow reduced in {0}, passage {1}".format(row.label,i) )
            for row in Rows:
                row.mdot[i] = 1.0 * mdot_star_min # set 5% margin 
            A[i] = mdot_star_min# reduce mass flow rate in this passage

    for i in range(len(Rows)):
        #if gdata.verbosity > 1:
        #    print("Evaluating properties for row= {0}".format(Rows[i].label))

        if i == 0: # differentiate between first and subsequent rows
            Rows[i].update_inlet_from_inflow(cond.PT_in,cond.TT_in,fluid)           
        else:
            Rows[i].update_inlet_conditions(Rows[i-1],fluid)
        # calculate property change between 0 and 1
        Rows[i].update_outlet_conditions(fluid)

    # report progress
    if gdata.verbosity > 1:
        print("\n")
        print("++++++++++++++++++++++++++")
        print("+++ Iteration Progress +++")
        print("Row , Location, Pressure, Temperature , Mach, Vx, Vt, Vr, rho ")
        write_row_data(Rows)

    # calculate error function for optimiser
    error = np.mean(Rows[-1].P2) - cond.P_out_target

    # write results into condition class
    cond.T_out = BLADEROW.instances[-1].T2
    cond.P_out = BLADEROW.instances[-1].P2
    cond.mdot = BLADEROW.instances[-1].mdot 
    cond.Vx_out = BLADEROW.instances[-1].Vx2
    cond.Vt_out = BLADEROW.instances[-1].Vt2
    cond.Vr_out = BLADEROW.instances[-1].Vr2

    if gdata.verbosity > 1:
        print("+++++++++++++ \n Iteration Error:")
        print("Error {0}".format(error))

    return error
###
###
def equations_1(A, BLADEROW, cond, fluid, gdata):
    """
    function to be solved by optimiser. This 
    Inputs:

    Outputs:
    error - vector containing miss-balance in mass flow, pressure and temperature.
    """

    Rows = BLADEROW.instances
    if gdata.verbosity > 1:
        print("\n") 
        print("######################################")
        print("#### Starting new Evaluation Loop - Find chocked massflow rate ####")
        print("######################################")
        print("Input variables to loop: A=",A)
    
    massFlow = abs(A[0])

    cond.massFlow.append(float(-massFlow))
    print("Massflow:",cond.massFlow)

    # update mass flow for each Blade row
    for row in Rows:
        row.massTotal = massFlow
        row.mdot[0] = massFlow

    for i in range(len(Rows)):
        if i == 0: # differentiate between first and subsequent rows
            Rows[i].update_inlet_from_inflow(cond.PT_in,cond.TT_in,fluid)           
        else:
            Rows[i].update_inlet_conditions(Rows[i-1],fluid)
        # calculate property change between 0 and 1
        Rows[i].update_outlet_conditions(fluid)

    # report progress
    if gdata.verbosity > 1:
        print("\n")
        print("++++++++++++++++++++++++++")
        print("+++ Iteration Progress +++")
        print("Row , Location, Pressure, Temperature , Mach, Vx, Vt, Vr, rho ")
        write_row_data(Rows)

    # calculate error function for optimiser. This tries to get exit flow condition for first row to match to Mach = 1 
    error = (np.mean(Rows[0].M2) - 1.)**2

    # write results into condition class
    cond.T_out = BLADEROW.instances[-1].T2
    cond.P_out = BLADEROW.instances[-1].P2
    cond.mdot = BLADEROW.instances[-1].mdot 
    cond.Vx_out = BLADEROW.instances[-1].Vx2
    cond.Vt_out = BLADEROW.instances[-1].Vt2
    cond.Vr_out = BLADEROW.instances[-1].Vr2

    if gdata.verbosity > 1:
        print("+++++++++++++ \n Iteration Error:")
        print("Error {0}".format(error))

    return error
###
###
def equations_2(P, massFlow, BLADEROW, cond, fluid, gdata):
    """
    function to be solved by optimiser. This 
    Inputs:

    Outputs:
    error - vector containing miss-balance in mass flow, pressure and temperature.
    """

    Rows = BLADEROW.instances
    if gdata.verbosity > 1:
        print("\n") 
        print("######################################")
        print("#### Starting new Evaluation Loop - Find intermediary Pressure for trans-sonic ####")
        print("######################################")
        print("Input variables to loop: P=",P)
    
    #massFlow = abs(A[0])

    """
    # update mass flow for each Blade row
    for row in Rows:
        row.massTotal = massFlow
        row.mdot[0] = massFlow
    """
    for i in range(len(Rows)):
        if i == 0: # differentiate between first and subsequent rows
            pass
        elif i == 1:
            Rows[i].update_inlet_conditions(Rows[i-1],fluid,pressure=P)
        else:
            Rows[i].update_inlet_conditions(Rows[i-1],fluid)

        if i != 0:
            # calculate property change between 0 and 1
            Rows[i].update_outlet_conditions(fluid)

    # report progress
    if gdata.verbosity > 1:
        print("\n")
        print("++++++++++++++++++++++++++")
        print("+++ Iteration Progress +++")
        print("Row , Location, Pressure, Temperature , Mach, Vx, Vt, Vr, rho ")
        write_row_data(Rows)

    # calculate error function for optimiser
    error = np.mean(Rows[-1].P2) - cond.P_out_target

    # write results into condition class
    cond.T_out = BLADEROW.instances[-1].T2
    cond.P_out = BLADEROW.instances[-1].P2
    cond.mdot = BLADEROW.instances[-1].mdot 
    cond.Vx_out = BLADEROW.instances[-1].Vx2
    cond.Vt_out = BLADEROW.instances[-1].Vt2
    cond.Vr_out = BLADEROW.instances[-1].Vr2

    if gdata.verbosity > 1:
        print("+++++++++++++ \n Iteration Error:")
        print("Error {0}".format(error))

    return error
###
###
def write_row_data(Rows):
    """
    function to plot properties in every row and passage
    """
    for r in range(len(Rows)):
        #for p in range(gdata.passages):
        print("{0}, A1=[".format(r) + 
            ', '.join('{0:.2f}'.format(F) for F in Rows[r].Area1) + "] (m2), A2=["+ 
            ', '.join('{0:.2f}'.format(F) for F in Rows[r].Area2) + "] (m2), mdot=[" + 
            ', '.join('{0:.3f}'.format(F) for F in Rows[r].mdot) + "] (kg/s)" )  
        print("{0}, 1t:, PT=[".format(r) +
            ', '.join('{0:.1f}'.format(F) for F in Rows[r].PT1) + "] (Pa), TT=["+
            ', '.join('{0:.2f}'.format(F) for F in Rows[r].TT1) + "] (K), hT=["+
            ', '.join('{0:.2f}'.format(F) for F in Rows[r].hT1) + "] (J/kg)")
        print("{0}, 1s:, P=[".format(r) +
            ', '.join('{0:.1f}'.format(F) for F in Rows[r].P1) + "] (Pa), T=["+
            ', '.join('{0:.2f}'.format(F) for F in Rows[r].T1) + "] (K), Ma=["+
            ', '.join('{0:.3f}'.format(F) for F in Rows[r].M1) + "] (-), Vx=["+
            ', '.join('{0:.2f}'.format(F) for F in Rows[r].Vx1) + "] (m/s), Vt=["+
            ', '.join('{0:.2f}'.format(F) for F in Rows[r].Vt1) + "] (m/s), Vr=["+
            ', '.join('{0:.2f}'.format(F) for F in Rows[r].Vr1) + "] (m/s), rho=["+
            ', '.join('{0:.3f}'.format(F) for F in Rows[r].rho1) + "] (kg/m3) ")
        print("{0}, 2s:, P=[".format(r) +
            ', '.join('{0:.1f}'.format(F) for F in Rows[r].P2) + "] (Pa), T=["+
            ', '.join('{0:.2f}'.format(F) for F in Rows[r].T2) + "] (K), Ma=["+
            ', '.join('{0:.3f}'.format(F) for F in Rows[r].M2) + "] (-), Vx=["+
            ', '.join('{0:.2f}'.format(F) for F in Rows[r].Vx2) + "] (m/s), Vt=["+
            ', '.join('{0:.2f}'.format(F) for F in Rows[r].Vt2) + "] (m/s), Vr=["+
            ', '.join('{0:.2f}'.format(F) for F in Rows[r].Vr2) + "] (m/s), rho=["+
            ', '.join('{0:.3f}'.format(F) for F in Rows[r].rho2) + "] (kg/m3) ")
        print("{0}, 2t:, PT=[".format(r) +
            ', '.join('{0:.1f}'.format(F) for F in Rows[r].PT2) + "] (Pa), TT=["+
            ', '.join('{0:.2f}'.format(F) for F in Rows[r].TT2) + "] (K), hT=["+
            ', '.join('{0:.2f}'.format(F) for F in Rows[r].hT2) + "] (J/kg)")
    return 0
###
###
def main(uoDict):
    """
    main function
    """
    # create string to collect warning messages
    warn_str = "\n"

    # main file to be executed 
    jobFileName = uoDict.get("--job", "test")

    # Make AxTurb run even if .py extension is forgotten
    if not ".py" in jobFileName:
    	jobName = jobFileName
    	jobFileName = ''.join([jobFileName, ".py"])
    else:
    	# strip .py extension from jobName
    	jobName = jobFileName.replace('.py','')

    # Trying to execute from a relative directory, not supported
    if os.sep in jobFileName:
    	relative_dir = jobFileName.split(os.sep)[:-1]
    	abs_dir = os.path.abspath(os.path.join(*relative_dir))
    	jobName = jobFileName.split(os.sep)[-1]
    	raise MyError(("AxTurb does not currently support relative imports. "
    		"Please change directory to {0} and retry with command: \n\n "
    		"$ AxTurb.py --job={1} \n"
    		.format(abs_dir, jobName)))

    # Make sure that the desired job file actually exists
    if not jobFileName in os.listdir('.'):
    	local_directory = os.path.dirname(os.path.realpath(__file__))
    	raise MyError("No job file {0} found in directory: {1}"
    		.format(jobFileName, local_directory))

    # get in-file and out-file name 
    out_file = uoDict.get("--out-file", "none")
    in_file = uoDict.get("--in-file", "none")

    # initialise GDATA, CONDITIONS, FLUID classes so that they can be adjusted by job-file
    gdata = GDATA()
    cond = CONDITIONS()
    fluid = FLUID()
   
    gdata.verbosity = 0
    # set verbosity (can be overwritten from jobfile)
    gdata.verbosity = float(uoDict.get("--verbosity",0))

    # initiaise LOSS model
    loss = LOSS(verbosity=gdata.verbosity)


    # clear component instances. This is required to allow batch processing. Otherwise repeated calls of the cycle. E.g. by calling Cycle.main(uodict) will see the previously defined instances.
    BLADEROW.instances=[]  # Blades
 
    # Execute jobFile, this creates all the variables
    exec(open(jobFileName).read(),globals(),locals())
    #execfile(jobFileName,globals(),locals())

    if gdata.verbosity >= 1:
        print("AxTurb Input data file read")
       
    if gdata.verbosity >= 1:
        print("Running checks on Input data")
    
    loss.check()
    cond.check()
    gdata.check()

    # set up general conditions in rows
    for row in BLADEROW.instances:
        row.setup(gdata,cond,loss,fluid)
        row.calc_geom()
        row.initialise()
        row.check()
    
    if gdata.verbosity >= 1:
        print("    Complete: Running checks on Input data")

    # Display Initia Conditions.
    if gdata.verbosity > 1:
        print('INITIAL CONDITIONS:')
        print('------------------------------')

    # create empty mass flow list to monitor mass flow rate progression
    cond.massFlow=[]

    # set maximum number of function evaluations
    max_fev = gdata.iter  # caculate maximum number of function evaluations. 0 --> till convergence

    args = (BLADEROW,cond,fluid,gdata) # set list of optional inputs used by optimiser.

    if not '--justplot' in uoDict:
        # set starting condition for optimiser
        A0 = cond.mdot0
    
        # employ non linear equations solver.
        if gdata.optim == 'fsolve':
            # use fsolve to solve equations
            A , infodict, status, mesg = sci.optimize.fsolve(equations,A0,args=args, full_output=1, factor=100., maxfev = max_fev , epsfcn=0.0001 )
        elif gdata.optim == 'root:lm':
            sol = sci.optimize.root(equations,A0,args=args,method='lm',options={'eps':1.e-3, 'xtol':1.e-12, 'ftol':1e-12,  'maxfev':max_fev})
            status = sol.status
            A = sol.x
            mesg = sol.message
        elif gdata.optim == 'root:Newton-CG':
            sol = sci.optimize.root(equations,A0,args=args,method='lm',options={'eps':1.e-3, 'xtol':1.e-12, 'maxfev':max_fev})
            status = sol.status
            A = sol.x
            mesg = sol.message
        elif gdata.optim == 'root:df-sane':
            sol = sci.optimize.root(equations,A0,args=args,method='df-sane',options={'ftol':1.e-12,  'maxfev':max_fev})
            status = sol.status
            A = sol.x
            mesg = sol.message
        elif gdata.optim == 'root:hybr':
            sol = sci.optimize.root(equations,A0,args=args,method='hybr',options={'xtol':1.e-8, 'maxfev':max_fev})
            status = sol.status
            A = sol.x
            mesg = sol.message
        else:
            raise MyError("gdata.optim = '' not set properly.")

        print("\n")
        print("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
        print("++++ Found matching outlet pressures for sub-sonic ++++")
        print("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")


    # for case that solution hasn't converged try to chocked flow case
    if (not '--justplot' in uoDict) and (status != 1):
        # set starting condition for optimiser from previous attempt
        A0 = A
    
        # employ non linear equations solver.
        if gdata.optim == 'fsolve':
            # use fsolve to solve equations
            A , infodict, status, mesg = sci.optimize.fsolve(equations_1,A0,args=args, full_output=1, factor=100., maxfev = max_fev , epsfcn=0.0001 )
        elif gdata.optim == 'root:Newton-CG':
            sol = sci.optimize.root(equations_1,A0,args=args,method='lm',options={'eps':1.e-3, 'xtol':1.e-12, 'maxfev':max_fev})
            status = sol.status
            A = sol.x
            mesg = sol.message
        elif gdata.optim == 'root:hybr':
            sol = sci.optimize.root(equations_1,A0,args=args,method='hybr',options={'xtol':1.e-8, 'maxfev':max_fev})
            status = sol.status
            A = sol.x
            mesg = sol.message
        else:
            raise MyError("gdata.optim = '' not set properly.")    

        print("\n")
        print("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
        print("++++ Found chocked mass flow ++++")
        print("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")


        # set mass flow rate
        massFlow = A

        # Now proceed to solve rows downstream of choked point
        # For this we adjust the pressure at inlet to rotor to get correct outflow condition. 
        # physically this repesents an ablique shock train

        Rows = BLADEROW.instances 
        P0 = Rows[1].P1[0]
        args2 = (massFlow, BLADEROW,cond,fluid,gdata) # set list of optional inputs used by optimiser.

        # employ non linear equations solver.
        if gdata.optim == 'fsolve':
            # use fsolve to solve equations
            P, infodict, status, mesg = sci.optimize.fsolve(equations_2,P0,args=args2, full_output=1, factor=100., maxfev = max_fev , epsfcn=0.0001 )
        elif gdata.optim == 'root:Newton-CG':
            sol = sci.optimize.root(equations_2,P0,args=args2,method='lm',options={'eps':1.e-3, 'xtol':1.e-12, 'maxfev':max_fev})
            status = sol.status
            P = sol.x
            mesg = sol.message
        elif gdata.optim == 'root:hybr':
            sol = sci.optimize.root(equations_2,P0,args=args2,method='hybr',options={'xtol':1.e-8, 'maxfev':max_fev})
            status = sol.status
            P = sol.x
            mesg = sol.message
        else:
            raise MyError("gdata.optim = '' not set properly.")    

        print("\n")
        print("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
        print("++++ Found matching outlet pressures for trans-sonic ++++")
        print("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")


    # Prepare output data
    if (gdata.verbosity > 0 or (not out_file is "none") or (not '--noplot' in uoDict) ) and (not '--justplot' in uoDict):
        if gdata.verbosity > 1:
            print("\n")
            print("Preparing data for output")
        
        T_out = BLADEROW.instances[-1].T1
        mdot_out = BLADEROW.instances[-1].mdot
        cond.calc_total_outlet(fluid)
        cond.calc_eta(mdot_out,fluid)
        
        for row in BLADEROW.instances:
            row.calc_average()
 
        if gdata.verbosity > 1:
            print("    Complete: Preparing data for output")
 
 
    # print on screen output summary
    if gdata.verbosity > 0:
        print('++++++++++++++++++++++++++++++++++++++++')
        print('SOLVER - OUTPUT:')
        print("Status: {0}".format(status))
        print("Message: {0}".format(mesg))
        print("\n")
        ###
        print('++++++++++++++++++++++++++++++++++++++++')
        print('RESULTS:')
        print("Name: {0}".format(gdata.name))
        print("Number of Stages: {0:.1f}".format(len(BLADEROW.instances)/2))
        string = "Inlet"
        for row in BLADEROW.instances:
            string = string + " | " + row.type
        print("Configuration: {}".format(string))
        print("Total Mass flow rate     (kg/s): {0:.3f}".format(sum(mdot_out)))
        #print("Mass flow rate (kg/s): {0:f}".format(mdot_out))
        print("Inlet Pressure (total)     (Pa): {0:.1f}".format(cond.PT_in))
        print("Inlet Temperature (total)   (K): {0:.2f}".format(cond.TT_in))
        print('Outlet Pressure (total)    (Pa): '+ ', '.join('{0:.1f}'.format(F) for F in cond.PT_out) )
        print('Outlet Temperature (total)  (K): '+ ', '.join('{0:.2f}'.format(F) for F in cond.TT_out) )
        print("Outlet Pressure (static)   (Pa): " + ', '.join('{0:.1f}'.format(F) for F in cond.P_out) )
        print("Outlet Temperature (static) (K): " + ', '.join('{0:.2f}'.format(F) for F in cond.T_out) )
        print("Power (W): {0:.2f}".format(cond.Power))
        print("Specific Power (J/kg): {0:.2f}".format(cond.power))
        print("Isentropic Efficiency (t-s): {0:.2%}".format(cond.eta_bar))
        print("Isentropic Efficiency (t-t): {0:.2%}".format(cond.eta_tt_bar))

        
    # create output data file with results
    if not out_file is "none":
 
        if gdata.verbosity >= 1:
            print("\n")
            print("Starting to writing OUTPUT FILE to {0}".format(out_file))
        
        if os.path.isfile(out_file):
            print("Output file already exists.")
            fntemp = out_file+".old"
            shutil.copyfile(out_file, fntemp)
            print("Existing file has been copied to:",fntemp)

        fp = open(out_file,'w')
        fp.write("++++++++++++++++++++++++++++++\n")
        fp.write("Output File from AxTurb.\n")
        fp.write("++++++++++++++++++++++++++++++\n")
        fp.write("Date / Time = "+str(datetime.datetime.now())+"\n")
        fp.write("Jobfile = "+jobFileName+"\n")
        fp.write("Iterative Solver Message ="+mesg+"\n")
        fp.write("++++++++++++++++++++++++++++++\n")

        # write loss Model data
        loss.write(fp)
        fp.write("++++++++++++++++++++++++++++++\n")
        
        # write fluid Model data
        fluid.write(fp)
        fp.write("++++++++++++++++++++++++++++++\n")
        
        # write Operating Conditions
        cond.write(fp)
        fp.write("++++++++++++++++++++++++++++++\n")
        
        # write blade row data
        fp.write("BLADE GEOMETRIES: \n")
        for b in BLADEROW.instances:
            b.write(fp)
        fp.write("++++++++++++++++++++++++++++++\n")
        
        # write loss Model data
        loss.write(fp)
        fp.write("++++++++++++++++++++++++++++++\n")

        if gdata.verbosity >= 1:
            print("    Complete: Writing Outfile")
        
    if not '--noplot' in uoDict:
        # plot useful outputs
        plot_geometry(BLADEROW)
        if not '--justplot' in uoDict:
            plot_conditions(BLADEROW,fluid,cond,gdata)
            plot_Ts(BLADEROW,fluid,cond,gdata)
        plt.draw()

        plt.pause(1) # <-------
        print('\n \n')

        input('<Hit Enter To Close Figures>')

        plt.close()

    return 0
###
###
shortOptions = ""
longOptions = ["help", "job=", "verbosity=", "out-file=", "in-file=", "noplot", "justplot"]
###
###
def printUsage():
    print("")
    print("Usage: AxTurb.py [--help] [--job=<jobFileName>] [--verbosity=0 (1 or 2)]")
    print("                [--in-file=<FileName>] [--out-file=<FileName>]")
    print("                [--noplot] [--justplot]")
    return
###
###
class MyError(Exception):
    def __init__(self, value):
        self.value = value
    def __str__(self):
        return repr(self.value)
###
###
if __name__ == "__main__":
    userOptions = getopt(sys.argv[1:], shortOptions, longOptions)
    uoDict = dict(userOptions[0])
    
    if len(userOptions[0]) == 0 or "--help" in uoDict:
        printUsage()
        sys.exit(1)

    # execute the code
    try:
        main(uoDict)
        print("\n \n")
        print("SUCESS.")
        print("\n \n")

    except MyError as e:
        print("This run has gone bad.")
        print(e.value)
        sys.exit(1)
