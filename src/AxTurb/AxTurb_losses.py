#! /usr/bin/env python3
"""
Python Code to evaluate on and off-design performance of Axial Turbines.

Collection of loss models for use by AxTurb

Author: Ingo Jahn
Last Modified: 2018/07/03
"""


import numpy as np


###
###
class MyError(Exception):
    def __init__(self, value):
        self.value = value
    def __str__(self):
        return repr(self.value)
###
###
class LOSS:
    def __init__(self,verbosity = 0 ):
        self.statorIncidentType = "None"
        self.statorPassageType = "None"
        self.statorTrailingType = "None"
        self.rotorIncidentType = "None"
        self.rotorPassageType = "None"
        self.rotorTrailingType = "None"
        self.rotorTipClearanceType = "None"
        self.verbosity = verbosity
    ###
    def check(self):
        # function to check if Loss Model selection is supported. 
        statorIncidentModelTypeList = ["None", "Point_one", "Test", "Zehner_1980", "Kacker_Okapuu_1982"]
        if self.statorIncidentType not in statorIncidentModelTypeList:
            raise MyError("Selected Stator Incident Loss Model {0} is not supported. \n"
                "Possible loss models {1}".format(self.statorIncidentType,statorIncidentModelTypeList)) 
        ###
        statorPassageModelTypeList = ["None", "Point_one", "Zehner_1980", "Kacker_Okapuu_1982"]
        if self.statorPassageType not in statorPassageModelTypeList:
            raise MyError("Selected Stator Passage Loss Model {0} is not supported. \n"
                "Possible loss models {1}".format(self.statorPassageType,statorPassageModelTypeList)) 
        ###
        statorTrailingModelTypeList = ["None", "Point_one"]
        if self.statorTrailingType not in statorTrailingModelTypeList:
            raise MyError("Selected Stator Trailing Edge Loss Model {0} is not supported. \n"
                "Possible loss models {1}".format(self.statorTrailingType,statorTrailingModelTypeList)) 
        ###
        rotorIncidentModelTypeList = ["None", "Point_one", "Test", "Zehner_1980", "Kacker_Okapuu_1982"]
        if self.rotorIncidentType not in rotorIncidentModelTypeList:
            raise MyError("Selected Rotor Incident Loss Model {0} is not supported. \n"
                "Possible loss models {1}".format(self.rotorIncidentType,rotorIncidentModelTypeList)) 
        ###
        rotorPassageModelTypeList = ["None", "Point_one", "Zehner_1980", "Kacker_Okapuu_1982"]
        if self.rotorPassageType not in rotorPassageModelTypeList:
            raise MyError("Selected Rotor Passage Loss Model {0} is not supported. \n"
                "Possible loss models {1}".format(self.rotorPassageType,rotorPassageModelTypeList)) 
        ###
        rotorTrailingModelTypeList = ["None", "Point_one"]
        if self.rotorTrailingType not in rotorTrailingModelTypeList:
            raise MyError("Selected Rotor Trailing Edge Loss Model {0} is not supported. \n"
                "Possible loss models {1}".format(self.rotorTrailingType,rotorTrailingModelTypeList)) 
        ###               
        rotorTipClearanceModelTypeList = ["None", "Point_one"]
        if self.rotorTipClearanceType not in rotorTipClearanceModelTypeList:
            raise MyError("Selected Rotor Tip Clearance Loss Model {0} is not supported. \n"
                "Possible loss models {1}".format(self.rotorTipClearanceType,rotorTipClearanceTypeList)) 

        # check that appropriate combinations are selected
        if (self.statorIncidentType == "Zehner_1980" or self.statorIncidentType == "Kacker_Okapuu_1982") and self.statorIncidentType != self.statorPassageType:
            raise MyError("Stator Incident and Stator Passage loss model type should be the same.")
        if (self.statorPassageType == "Zehner_1980" or self.statorPassageType == "Kacker_Okapuu_1982") and self.statorPassageType != self.statorIncidentType:
            raise MyError("Stator Incident and Stator Passage loss model type should be the same.")
        ###
        if (self.rotorIncidentType == "Zehner_1980" or self.rotorIncidentType == "Kacker_Okapuu_1982") and self.rotorIncidentType != self.rotorPassageType:
            raise MyError("Rotor Incident and Rotor Passage loss model type should be the same.")
        if (self.rotorPassageType == "Zehner_1980" or self.rotorPassageType == "Kacker_Okapuu_1982") and self.rotorPassageType != self.rotorIncidentType:
            raise MyError("Rotor Incident and Rotor Passage loss model type should be the same.")
        ###

    ###
    """
    Literature References on various loss models

    Zehner_1980
    Zehner P. (1980), Calculation of Four-Quadrant Characteristics of Turbines,
    ASME, 80-GT-2


    Kacker_Okapuu_1982
    Kacker S. C., Okapuu U. (1982), A Mean Line Prediction Method for Axial Floe Turbine Efficiency,
    ASME, Journal of Engineering for Power, january 1982, Vol. 104/111

    Tournier_2010
    Tournier J-M, El-Genk, M. S. (2010), Axial flow, multi-stage turbine and compressor models,
    Energy Conversion and Management, 51 (2010) 16-29

    """
    ###
    def statorIncidentLoss(self,angle_error,row,i,fluid):
        if self.statorIncidentType == "None":
            loss = 0
            lossFlag = "absolute"

        elif self.statorIncidentType == "Point_one":
            loss = 0.1
            lossFlag = "proportional_to_ke_abs"

        elif self.statorPassageType == "Kacker_Okapuu_1982":
            self.alpha1 = angle_error[0]
            self.phi1 = angle_error[1]
            self.angle_error = self.alpha1 - self.phi1
    
            # loss is calculated as part of main passage loss. 
            loss = 0
            lossFlag = "absolute"

        elif self.statorIncidentType == "Zehner_1980":
            self.alpha1 = angle_error[0]
            self.phi1 = angle_error[1]

            self.beta = self.alpha1 - self.phi1
            if self.beta >= 0:
                self.beta_star = self.beta / (180. - self.phi1)
            else: # self.beta <0
                self.beta_star = self.beta / (- self.phi1)


            #beta_star = angle_error
            #gamma = row.gamma  # TODO: need to calculate this from blade
            #zeta_N = 1. - eta_passage  #TODO: 
            #if beta_star >= 0:
            #    Ca0 = 2.587; Ca1 = -0.426; Ca2 = -1.216
            #    Cb0 = 4.175; Cb1 = 10.802; Cb2 = -13.881
            #else: # beta_start < 0
            #    Ca0 = 0.446; Ca1 =  3.820; Ca2 = -2.899
            #    Cb0 = 2.413; Cb1 = 10.380; Cb2 = -10.166
            #a = Ca0 * 1+ Ca1 * gamma + Ca2 * gamma**2
            #b = Cb0 * 1+ Cb1 * gamma + Cb2 * gamma**2
            #zeta = 1 - (1 - zeta_N ) * np.exp(-a * abs(beta_star)**b )
            #loss = zeta
            #lossFlag = "zeta_rel"

            # loss is calculated as part of main passage loss. 
            loss = 0
            lossFlag = "absolute"


        elif self.statorIncidentType == "Test":        
            if angle_error < 20:
                loss = angle_error * 0.1/20
            else:
                loss = 0.1 + (angle_error-20.) * 0.01           
            lossFlag = "proportional_to_ke_abs"

        else:
            raise MyError("Shouldn't be here")
        return loss, lossFlag
    ###
    def statorPassageLoss(self,P,T,Vx,Vt,Vr,row,i,fluid):
        if self.statorPassageType == "None":
            loss = 0
            lossFlag = "absolute"

        elif self.statorPassageType == "Point_one":
            loss = 0.1
            lossFlag = "proportional_to_ke_abs"

        elif self.statorPassageType == "Kacker_Okapuu_1982":
            """
            Caclulation as pressure loss as per paper by Kacker Okapuu.
            Coeffiecients for curve fits are taken from Tournier et al 2010
            """
            h_blade = row.height# blade height
            L = row.L[i]        # axial chord
            c = row.c[i]        # true chord
            pitch = row.pitch   # pitch
            R_hub = 0.5 * (row.r_hub1 + row.r_hub2) # hub radiua
            R_tip = 0.5 * (row.r_tip1 + row.r_tip2) # tip radius
            t_max = row.d[i]    # maximum blade thickness
            P1 = row.P1[i]      # pressure at inlet
            P2 = row.P2[i]      # pressure at outlet 
            if P2 == 0.: # work around for first iteration
                P2 = P1 
            T1 = row.T1[i]      # temperature at inlet 
            T2 = row.T2[i]      # temperature at outlet
            if T2 == 0.: # work around for first iteration
                T2 = T1
            rho_m = 0.5 * (row.rho1[i] + row.rho2[i])          # average density
            V1 = np.sqrt(row.Vt1[i]**2 + row.Vr1[i]**2 + row.Vx1[i]**2)
            V2 = np.sqrt(row.Vt2[i]**2 + row.Vr2[i]**2 + row.Vx2[i]**2)
            if V2 == 0.: # work around for first iteration
                V2 = V1
            V_m = 0.5 * (V1 + V2 )  # average velocity
            mu_m = fluid.mu_from_T( 0.5*(row.T1[i] + row.T2[i]) )            # average viscosity
            M1 = V1 / fluid.a_from_PT(P1,T1)
            M2 = V2 / fluid.a_from_PT(P2,T2)
            M1_hub = M1  # TODO: add correction for fact that Mach number at hub is always largest  
            gamma = fluid.gamma  # ratio of specific heats
            sigma = c / pitch 
            
            # sign convention and labeling of angles follows convention from Kacker and Okapuu paper
            # beta = metal angle defined relative to axial direction
            beta1 = row.phi1[i] - 90.     
            beta2 = 90. - row.phi2[i] 
            # alpha = gas angle defined relative to axial direction
            alpha1 = beta1 + self.angle_error
            alpha2 = beta2    # TODO: add correction for deviation at exit


            # do some error checking if geometry fits withing validity range of loss model
            if alpha2 == 0.:
                raise MyError("Kacker_Okapuu_1982 loss model does not support stages with metal angle = 90deg (axial flow)")
            if pitch/c < 0.3:
                raise MyError("Kacker_Okapuu_1982 loss model has no verifiction data for pitch/chord < 0.3. Bailing Out")
            if L / h_blade > 1.4:
                raise MyError("Kacker_Okapuu_1982 loss model has no verifiction data for axial_chord / blade_height >1.4. Bailing Out")
            if beta1 < 0. or beta2 < 0.:
                raise MyError("Kacker_Okapuu_1982 loss model has no verifiction data for beta < 0. Bailing Out")

            # profile Loss 
            # calculate straight inflow case
            if alpha1 < 63.2:   # coefficients taken from Tournier et al
                A = -5.58e-5 * alpha2**2 + 1.03e-2 * alpha2 - 0.275
                B = 1.553e-5 * alpha2**2 - 2.32e-3 * alpha2 + 8.02e-2
                C = -8.54e-3 * alpha2 + 0.238
                D =  4.83e-5 * alpha2**2 + 2.83e-4 * alpha2 - 2.93e-2
            else:
                A =  2.44e-4 * alpha2**2 - 4.33e-2 * alpha2 + 1.92
                B = -4.02e-5 * alpha2**2 + 6.94e-3 * alpha2 - 0.282
                C = -2.23e-4 * alpha2**2 + 4.96e-2 * alpha2 - 2.548
                D =  5.39e-5 * alpha2**2 - 1.57e-2 * alpha2 + 0.958   
            Yp_beta1_0 = A + B * sigma + 1./sigma * ( C + D/sigma )

            if alpha1 > 60.:
                one_over_sigma_min = -5.14e-4 * alpha2**2 + 5.48e-2 * alpha2 - 0.798
            else:
                one_over_sigma_min = -8.63e-6 * alpha2**3 + 9.68e-4 * alpha2**2 - 3.76e-2 * alpha2 + 1.272

            Yp_min = 0.280 * ( 1. - one_over_sigma_min )

            if 1./sigma >= one_over_sigma_min:
                n = 1.524e-4 * alpha2**2 - 0.031 * alpha2 + 2.992
                if alpha2 > 60:
                    A = 5.407e-3 * alpha2 - 0.19642
                else:
                    A = -2.91e-3 * alpha2 + 0.30260
            else:
                if alpha2 > 60:
                    n = 1.174e-2 * alpha2**2 - 1.5731 * alpha2 + 54.85
                    A = 9.240e-3 * alpha2**2 - 1.2067 * alpha2 + 40.04
                else:
                    n = 3.271e-3 * alpha2**2 - 0.3010 * alpha2 + 9.023
                    A = 2.701e-3 * alpha2**2 - 0.2456 * alpha2 + 5.909

            Yp_beta1_alpha2 = Yp_min + A * ( abs( 1./sigma - one_over_sigma_min) )**n

            #print('beta1,alpha2, s/c',beta1,alpha2,pitch/c)
            #print('Yp_beta1_0,Yp_beta1_alpha2',Yp_beta1_0,Yp_beta1_alpha2)

            Yp_AMDC = ( Yp_beta1_0 + abs(beta1/alpha2) * (beta1/alpha2) * ( Yp_beta1_alpha2 - Yp_beta1_0) ) \
                        * ((t_max/c) / 0.2 )**(beta1/alpha2) 

            # subsonic Mach number correction
            if M1 > 0.4: # from Eqn 4
                dP_q1 = 0.75 * (M1_hub - 0.4)**1.75  
            else:
                dP_q1 = 0

            dP_q1 = R_hub/R_tip * dP_q1     # Eqn 5

            Y_shock = dP_q1 * (P1/P2) \
                    * (1. - (1. + (gamma-1.)/2. * M1**2)**( gamma/(gamma-1.)) ) \
                    / (1. - (1. + (gamma-1.)/2. * M2**2)**( gamma/(gamma-1.)) )   # Eqn 6

            if abs(M2) < 0.2:   # from Fig 8
                K1 = 1.
            else:
                K1 = 1. - 1.25 * abs(M2 - 0.2)  
            K2 = (M1/M2)**2     # from Fig 9
            Kp = 1 - K2 * (1. -K1)  # Eqn 7

            Yp = 0.914 * (2./3. * Yp_AMDC * Kp + Y_shock)    # Eqn 8

            # supersonic Drag Rise
            if M2 > 1: # from Eqn 9
                CFM = 1. + 60. * (M2 - 1.)**2 
            else:
                CFM = 1.

            # Reynolds Number Correction
            Re_c = rho_m * c * V_m / mu_m
            if Re_c < 2.e5:  # from Eqn 10
                f_Re = (Re_c/2.e5)**(-0.4)
            elif Re_c > 1.e6:
                f_Re = (Re_c/1.e6)**(-0.2)
            else:
                f_Re = 1.0

            # secondary loss coefficient Ys
            alpha_m = np.arctan( 0.5 * np.tan(alpha1/180.*np.pi) + np.tan(alpha2/180.*np.pi))  # Eqn 13
            h_c = h_blade / c
            if h_c <= 2: # Eqn 14
                f_AR = ( 1. - 0.25 * np.sqrt(2. - h_c) ) / h_c
            else:
                f_AR = 1. / h_c

            CL_over_sc = 2 * (np.tan(alpha1/180.*np.pi) + np.tan(alpha2/180.*np.pi)) * np.cos(alpha_m)  # Eqn 12
            
            Ys_AMDC = 0.0334 * f_AR * ( np.cos(alpha2/180.*np.pi) / np.cos(beta2/180.*np.pi) ) * CL_over_sc**2 * np.cos(alpha2/180.*np.pi)**2 / ( np.cos(alpha_m)**3) # Eqn 11
            
            K3 = (1. / (h_blade/L))**2   # as per Fig 13        
            Ks = 1. - K3 * (1. - Kp)  # Eqn 15
            Ys = 1.2 * Ys_AMDC * Ks   # Eqn 16
            #print('K3,Ks',K3,Ks)

            # Tip clearance loss;
            Y_TC = 0. # TODO: implement tip clearance loss

            # Trailing Edge losses:
            # calculate these separately at later station.
            Y_TET = 0.

            # combine losses
            Y = Yp*f_Re + Ys + Y_TET + Y_TC

            if self.verbosity > 1:
                print("From Kacker_Okapuu_1982 Loss model")
                print('Yp_AMDC, Yp', Yp_AMDC, Yp )
                print('Re_c, f_Re',Yp, Re_c, f_Re)
                print('Ys_AMDC, Ys',Ys_AMDC, Ys) 
                print('Y_TET, Y_TC', Y_TET, Y_TC)
                print('Y=',Y)

            loss = Y
            lossFlag = "Y_pressure"

        elif self.statorPassageType == "Zehner_1980":
            """
            Calcuation of Wall and Secondary Losses as per Zehner paper
            """
            B00 = 4.442e-3; B01 = 7.4842e-2; B02 = 1.6170e-3
            B10 = 7.637e-3; B11 = 2.0060e-3; B12 = 3.7016e-2
            h_blade = row.height 
            pitchratio = row.pitch
            w1 = np.sqrt(row.Vx1[i]**2 + row.Vt1[i]**2 + row.Vr1[i]**2)
            w2 = np.sqrt(row.Vx2[i]**2 + row.Vt2[i]**2 + row.Vr2[i]**2)
            if w2 == 0: 
                w2 = w1
            vel_ratio = w1/w2 
            def_angle = row.delta[i]/180.*np.pi

            F = (  B00             + B01 * vel_ratio             + B02 * vel_ratio**2
                 + B10 * def_angle + B11 * vel_ratio * def_angle + B12 * vel_ratio**2 * def_angle)        
            k = 5.9151 - 8.866 * vel_ratio + 4.1402 * (vel_ratio)**2
            zeta_WS = k * F * pitchratio/h_blade

            # Now do correction for incident angle using Eqn 5. 
            gamma = row.gamma[i] 
            zeta_N = zeta_WS
            if self.beta >= 0:
                Ca0 = 2.587; Ca1 = -0.426; Ca2 = -1.216
                Cb0 = 4.175; Cb1 = 10.802; Cb2 = -13.881
            else: # self.beta < 0
                Ca0 = 0.446; Ca1 =  3.820; Ca2 = -2.899
                Cb0 = 2.413; Cb1 = 10.380; Cb2 = -10.166

            a = Ca0 * 1+ Ca1 * gamma + Ca2 * gamma**2
            b = Cb0 * 1+ Cb1 * gamma + Cb2 * gamma**2

            if self.beta_star == 0.:
                zeta = zeta_N
            else:
                zeta = 1. - (1. - zeta_N ) * np.exp(-a * abs(self.beta_star)**b )           

            if self.verbosity > 1:
                print("From Zehner_1980 Loss model")
                print('w1, w2, vel_ratio', w1, w2, vel_ratio)
                print('k, F, delta', k, F, def_angle)
                print('zeta_WS, zeta_N',zeta_WS,zeta_N)
                print('beta_star', self.beta_star)
                print('gamma, a, b', gamma, a, b)
                print('zeta',zeta)

            loss = zeta
            lossFlag = "zeta_rel"

        else:
            raise MyError("Shouldn't be here")
        return loss, lossFlag
    ###
    def statorTrailingLoss(self,P,T,Vx,Vt,Vr,row,i,fluid):
        if self.statorTrailingType == "None":
            loss = 0
            lossFlag = "absolute"

        elif self.statorTrailingType == "Point_one":
            loss = 0.1
            lossFlag = "proportional_to_ke_abs"


        elif self.statorTrailingType == "Kacker_Okapuu_1982":
            t_TE = 0.02 #TODO
            O = 0.1 #TODO

            d_Gamma_TE_b1_0      = 0.59563 * (t_TE/O)**2 + 0.12264 * t_TE/O  - 2.2796e-3 # loss for axial entry
            d_Gamma_TE_b1_alpha2 = 0.31066 * (t_TE/O)**2 + 0.065617 * t_TE/O - 1.4318e-3 # loss for impulse blading
            d_Gamma_TE = d_Gamma_TE_b1_0 + abs(beta1 / alpha2) * (beta1 / alpha2) * (d_Gamma_TE_b1_alpha2 - d_Gamma_TE_b1_0)

            loss = d_Gamma_TE #TODO: check that this is corrrect
            lossFlag = "proportional_to_ke_abs"

        else:
            raise MyError("Shouldn't be here")
        return loss, lossFlag
    ###
    ###
    def rotorIncidentLoss(self,angle_error,row,i,fluid):
        if self.rotorIncidentType == "None":
            loss = 0
            lossFlag = "absolute"

        elif self.rotorIncidentType == "Zehner_1980":
            self.alpha1 = angle_error[0]
            self.phi1 = angle_error[1]

            # calculate delta_beta = beta and delta_beta_star = beta_star
            self.beta = self.alpha1 - self.phi1
            if self.beta >= 0:
                self.beta_star = self.beta / (180. - self.phi1)
            else: # self.beta < 0
                self.beta_star = self.beta / (- self.phi1)


            loss = 0
            lossFlag = "absolute"

        elif self.rotorIncidentType == "Kacker_Okapuu_1982":
            self.alpha1 = angle_error[0]
            self.phi1 = angle_error[1]
            self.angle_error = self.alpha1 - self.phi1
            loss = 0
            lossFlag = "absolute"

        elif self.rotorIncidentType == "Point_one":
            loss = 0.1
            lossFlag = "proportional_to_ke_rel"

        elif self.rotorIncidentType == "Test":        
            if angle_error < 20:
                loss = angle_error * 0.1/20
            else:
                loss = 0.1 + (angle_error-20.) * 0.01           
            lossFlag = "proportional_to_ke_rel"

        else:
            raise MyError("Shouldn't be here")
        return loss, lossFlag
    ###
    def rotorPassageLoss(self,P,T,Vx,Vt,Vr,row,i,fluid):
        if self.rotorPassageType == "None":
            loss = 0
            lossFlag = "absolute"

        elif self.rotorPassageType == "Kacker_Okapuu_1982":
            """
            Caclulation as pressure loss as per paper by Kacker Okapuu.
            Coeffiecients for curve fits are taken from Tournier et al 2010
            """
            h_blade = row.height# blade height
            L = row.L[i]        # axial chord
            c = row.c[i]        # true chord
            pitch = row.pitch   # pitch
            R_hub = 0.5 * (row.r_hub1 + row.r_hub2) # hub radiua
            R_tip = 0.5 * (row.r_tip1 + row.r_tip2) # tip radius
            t_max = row.d[i]    # maximum blade thickness
            P1 = row.P1[i]      # pressure at inlet
            P2 = row.P2[i]      # pressure at outlet 
            if P2 == 0.: # work around for first iteration
                P2 = P1 
            T1 = row.T1[i]      # temperature at inlet 
            T2 = row.T2[i]      # temperature at outlet
            if T2 == 0.: # work around for first iteration
                T2 = T1
            rho_m = 0.5 * (row.rho1[i] + row.rho2[i])          # average density
            u1 = row.r1[i] * row.Nrad 
            u2 = row.r2[i] * row.Nrad 
            w1 = np.sqrt(row.Vx1[i]**2 + (row.Vt1[i] - u1)**2 + row.Vr1[i]**2) # correct for relative velocity
            w2 = np.sqrt(row.Vx2[i]**2 + (row.Vt2[i] - u2)**2 + row.Vr2[i]**2) # correct for relative velocity
            if w2 == 0.:
                w2 = w1
            w_m = 0.5 * (w1 + w2 )  # average velocity
            V1 = np.sqrt(row.Vt1[i]**2 + row.Vr1[i]**2 + row.Vx1[i]**2)
            V2 = np.sqrt(row.Vt2[i]**2 + row.Vr2[i]**2 + row.Vx2[i]**2)
            if V2 == 0.: # work around for first iteration
                V2 = V1
            V_m = 0.5 * (V1 + V2 )  # average velocity
            mu_m = fluid.mu_from_T( 0.5*(row.T1[i] + row.T2[i]) )            # average viscosity
            M1 = w1 / fluid.a_from_PT(P1,T1)
            M2 = w2 / fluid.a_from_PT(P2,T2)
            M1_hub = M1  # TODO: add correction for fact that Mach number at hub is always largest  
            gamma = fluid.gamma  # ratio of specific heats
            sigma = c / pitch 
            
            # sign convention and labeling of angles follows convention from Kacker and Okapuu paper
            # beta = metal angle defined relative to axial direction
            beta1 = 90. - row.phi1[i]     
            beta2 = row.phi2[i] - 90. 
            # alpha = gas angle defined relative to axial direction
            alpha1 = beta1 + self.angle_error
            alpha2 = beta2    # TODO: add correction for deviation at exit


            # do some error checking if geometry fits withing validity range of loss model
            if alpha2 == 0.:
                raise MyError("Kacker_Okapuu_1982 loss model does not support stages with metal angle = 90deg (axial flow)")
            if pitch/c < 0.3:
                raise MyError("Kacker_Okapuu_1982 loss model has no verifiction data for pitch/chord < 0.3. Bailing Out")
            if L / h_blade > 1.4:
                raise MyError("Kacker_Okapuu_1982 loss model has no verifiction data for axial_chord / blade_height >1.4. Bailing Out")
            if beta1 < 0. or beta2 < 0.:
                raise MyError("Kacker_Okapuu_1982 loss model has no verifiction data for beta < 0. Bailing Out")

            # profile Loss 
            # calculate straight inflow case
            if alpha1 < 63.2:   # coefficients taken from Tournier et al
                A = -5.58e-5 * alpha2**2 + 1.03e-2 * alpha2 - 0.275
                B = 1.553e-5 * alpha2**2 - 2.32e-3 * alpha2 + 8.02e-2
                C = -8.54e-3 * alpha2 + 0.238
                D =  4.83e-5 * alpha2**2 + 2.83e-4 * alpha2 - 2.93e-2
            else:
                A =  2.44e-4 * alpha2**2 - 4.33e-2 * alpha2 + 1.92
                B = -4.02e-5 * alpha2**2 + 6.94e-3 * alpha2 - 0.282
                C = -2.23e-4 * alpha2**2 + 4.96e-2 * alpha2 - 2.548
                D =  5.39e-5 * alpha2**2 - 1.57e-2 * alpha2 + 0.958   
            Yp_beta1_0 = A + B * sigma + 1./sigma * ( C + D/sigma )

            if alpha1 > 60.:
                one_over_sigma_min = -5.14e-4 * alpha2**2 + 5.48e-2 * alpha2 - 0.798
            else:
                one_over_sigma_min = -8.63e-6 * alpha2**3 + 9.68e-4 * alpha2**2 - 3.76e-2 * alpha2 + 1.272

            Yp_min = 0.280 * ( 1. - one_over_sigma_min )

            if 1./sigma >= one_over_sigma_min:
                n = 1.524e-4 * alpha2**2 - 0.031 * alpha2 + 2.992
                if alpha2 > 60:
                    A = 5.407e-3 * alpha2 - 0.19642
                else:
                    A = -2.91e-3 * alpha2 + 0.30260
            else:
                if alpha2 > 60:
                    n = 1.174e-2 * alpha2**2 - 1.5731 * alpha2 + 54.85
                    A = 9.240e-3 * alpha2**2 - 1.2067 * alpha2 + 40.04
                else:
                    n = 3.271e-3 * alpha2**2 - 0.3010 * alpha2 + 9.023
                    A = 2.701e-3 * alpha2**2 - 0.2456 * alpha2 + 5.909
            Yp_beta1_alpha2 = Yp_min + A * ( abs( 1./sigma - one_over_sigma_min) )**n

            #print('beta1,alpha1,alpha2, s/c',beta1,alpha1,alpha2,pitch/c)
            #print('Yp_min,one_over_sigma_min',Yp_min,one_over_sigma_min)
            # print('Yp_beta1_0,Yp_beta1_alpha2',Yp_beta1_0,Yp_beta1_alpha2)

            Yp_AMDC = ( Yp_beta1_0 + abs(beta1/alpha2) * (beta1/alpha2) * ( Yp_beta1_alpha2 - Yp_beta1_0) ) \
                        * ((t_max/c) / 0.2 )**(beta1/alpha2) 

            # subsonic Mach number correction
            if M1 > 0.4: # from Eqn 4
                dP_q1 = 0.75 * (M1_hub - 0.4)**1.75  
            else:
                dP_q1 = 0

            dP_q1 = R_hub/R_tip * dP_q1     # Eqn 5

            Y_shock = dP_q1 * (P1/P2) \
                    * (1. - (1. + (gamma-1.)/2. * M1**2)**( gamma/(gamma-1.)) ) \
                    / (1. - (1. + (gamma-1.)/2. * M2**2)**( gamma/(gamma-1.)) )   # Eqn 6

            if abs(M2) < 0.2:   # from Fig 8
                K1 = 1.
            else:
                K1 = 1. - 1.25 * abs(M2 - 0.2)  
            K2 = (M1/M2)**2     # from Fig 9
            Kp = 1 - K2 * (1. -K1)  # Eqn 7

            Yp = 0.914 * (2./3. * Yp_AMDC * Kp + Y_shock)    # Eqn 8

            # supersonic Drag Rise
            if M2 > 1: # from Eqn 9
                CFM = 1. + 60. * (M2 - 1.)**2 
            else:
                CFM = 1.

            # Reynolds Number Correction
            Re_c = rho_m * c * w_m / mu_m
            if Re_c < 2.e5:  # from Eqn 10
                f_Re = (Re_c/2.e5)**(-0.4)
            elif Re_c > 1.e6:
                f_Re = (Re_c/1.e6)**(-0.2)
            else:
                f_Re = 1.0

            # secondary loss coefficient Ys
            alpha_m = np.arctan( 0.5 * np.tan(alpha1/180.*np.pi) + np.tan(alpha2/180.*np.pi))  # Eqn 13
            h_c = h_blade / c
            if h_c <= 2: # Eqn 14
                f_AR = ( 1. - 0.25 * np.sqrt(2. - h_c) ) / h_c
            else:
                f_AR = 1. / h_c

            CL_over_sc = 2 * (np.tan(alpha1/180.*np.pi) + np.tan(alpha2/180.*np.pi)) * np.cos(alpha_m)  # Eqn 12
            
            Ys_AMDC = 0.0334 * f_AR * ( np.cos(alpha2/180.*np.pi) / np.cos(beta2/180.*np.pi) ) * CL_over_sc**2 * np.cos(alpha2/180.*np.pi)**2 / ( np.cos(alpha_m)**3) # Eqn 11
            
            K3 = (1. / (h_blade/L))**2   # as per Fig 13        
            Ks = 1. - K3 * (1. - Kp)  # Eqn 15
            Ys = 1.2 * Ys_AMDC * Ks   # Eqn 16
            #print('K3,Ks',K3,Ks)

            # Tip clearance loss;
            Y_TC = 0. # TODO: implement tip clearance loss

            # Trailing Edge losses:
            # calculate these separately at later station.
            Y_TET = 0.

            # combine losses
            Y = Yp*f_Re + Ys + Y_TET + Y_TC

            if self.verbosity > 1:
                print("From Kacker_Okapuu_1982 Loss model")
                print('Yp_AMDC, Yp', Yp_AMDC, Yp )
                print('Re_c, f_Re',Yp, Re_c, f_Re)
                print('Ys_AMDC, Ys',Ys_AMDC, Ys) 
                print('Y_TET, Y_TC', Y_TET, Y_TC)
                print('Y=',Y)

            loss = Y
            lossFlag = "Y_pressure"

        elif self.rotorPassageType == "Zehner_1980":
            """
            Calcuation of Wall and Secondary Losses as per Zehner paper
            """
            B00 = 4.442e-3; B01 = 7.4842e-2; B02 = 1.6170e-3
            B10 = 7.637e-3; B11 = 2.0060e-3; B12 = 3.7016e-2
            h_blade = row.height 
            pitchratio = row.pitch
            u1 = row.r1[i] * row.Nrad 
            u2 = row.r2[i] * row.Nrad 
            w1 = np.sqrt(row.Vx1[i]**2 + (row.Vt1[i] - u1)**2 + row.Vr1[i]**2) # correct for relative velocity
            w2 = np.sqrt(row.Vx2[i]**2 + (row.Vt2[i] - u2)**2 + row.Vr2[i]**2) # correct for relative velocity
            if w2 == 0:
                w2 = w1
            vel_ratio = w1/w2 
            def_angle = row.delta[i]/180.*np.pi

            F = (  B00             + B01 * vel_ratio             + B02 * vel_ratio**2
                 + B10 * def_angle + B11 * vel_ratio * def_angle + B12 * vel_ratio**2 * def_angle)        
            k = 5.9151 - 8.866 * vel_ratio + 4.1402 * (vel_ratio)**2
            zeta_WS = k * F * pitchratio/h_blade

            # Now do correction for incident angle using Eqn 5. 
            gamma = row.gamma[i] 
            zeta_N = zeta_WS
            if self.beta >= 0:
                Ca0 = 2.587; Ca1 = -0.426; Ca2 = -1.216
                Cb0 = 4.175; Cb1 = 10.802; Cb2 = -13.881
            else: # self.beta < 0
                Ca0 = 0.446; Ca1 =  3.820; Ca2 = -2.899
                Cb0 = 2.413; Cb1 = 10.380; Cb2 = -10.166

            a = Ca0 * 1+ Ca1 * gamma + Ca2 * gamma**2
            b = Cb0 * 1+ Cb1 * gamma + Cb2 * gamma**2

            if self.beta_star == 0.:
                zeta = zeta_N
            else:
                zeta = 1 - (1 - zeta_N ) * np.exp(-a * abs(self.beta_star)**b )             

            if self.verbosity > 1:
                print("From Zehner_1980 Loss model")
                print('w1, w2, vel_ratio', w1, w2, vel_ratio)
                print('k, F, delta', k, F, def_angle)
                print('zeta_WS',zeta_WS)
                print('beta_star', self.beta_star)
                print('gamma, a, b', gamma, a, b)
                print(zeta)

            loss = zeta
            lossFlag = "zeta_rel"

        elif self.rotorPassageType == "Point_one":
            loss = 0.1
            lossFlag = "proportional_to_ke_rel"

        else:
            raise MyError("Shouldn't be here")
        return loss, lossFlag
    ###
    def rotorTrailingLoss(self,P,T,Vx,Vt,Vr,row,i,fluid):
        if self.rotorTrailingType == "None":
            loss = 0
            lossFlag = "absolute"

        elif self.rotorTrailingType == "Point_one":
            loss = 0.1
            lossFlag = "proportional_to_ke_rel"

        else:
            raise MyError("Shouldn't be here")
        return loss, lossFlag
    ###
    def rotorTipClearanceLoss(self,P,T,Vx,Vt,Vr,row,i,fluid):
        if self.rotorTipClearanceType == "None":
            loss = 0
            lossFlag = "absolute"

        elif self.rotorTipClearanceType == "Point_one":
            loss = 0.1
            lossFlag = "proportional_to_ke_rel"

        else:
            raise MyError("Shouldn't be here")
        return loss, lossFlag
    ###
    def write(self,fp):
        '''
        function to write data to output file
        '''
        fp.write("+LOSS+")
        fp.write("statorIncidentType = {0} \n".format(self.statorIncidentType))
        fp.write("statorPassageType = {0} \n".format(self.statorPassageType))
        fp.write("statorTrailingType = {0} \n".format(self.statorTrailingType))
        fp.write("rotorIncidentType = {0} \n".format(self.rotorIncidentType))
        fp.write("rotorPassageType = {0} \n".format(self.rotorPassageType))
        fp.write("rotorTrailingType = {0} \n".format(self.rotorTrailingType))       
        fp.write("rotorTipClearanceType = {0} \n".format(self.rotorTipClearanceType))
