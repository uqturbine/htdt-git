#! /usr/bin/env python3
"""
A script for the analysis of heat fluxes in cylinders based on surface temperature history.

Last modified: 12th of March 2019
Author: Phillip Swann, Ingo Jahn

"""
import numpy as np
import math as ma
from scipy import special, integrate, sparse, interpolate, signal
import CoolProp.CoolProp as cp
import scipy.fftpack as fft
from mpl_toolkits.mplot3d.art3d import Poly3DCollection
import shutil
import matplotlib.pyplot as plt
import matplotlib.lines as mlines
from getopt import getopt
import sys as sys
import os as os
import datetime as dt
import csv
import pandas as pd

s_t_f = '%H:%M:%S.%f'
plt.rcParams.update({'font.size': 14})
plt.ticklabel_format(useOffset=False, style='plain')

# Class Definitions
class GDATA:
    """Class defining Simulation Settings."""

    def __init__(self):
        """Initialise Class."""
        self.verbosity = 0              # Verbosity flag
        self.n_bessel = None            # number of coefficients used
        self.time_solutions = None
        self.space_solutions = None 
        self.validation_flag = False  # turns on verification part of codes
        self.validation_function = None
        self.validation_heat_in = None
        self.unit_step = False
        self.final_time = None
        self.time_step = None
        self.data_files = None
        self.cam_start = None
        self.calibration = None
        self.FEA_data = None
        self.FEA_flux_flag = False
        self.exp_indiv_plots = False
        self.spectral_analysis = False
        self.spectral_analysis_data = None
        self.HTC_monte_error = None
        self.fluid = None
        self.q_monte_error = None
        self.speeds = None
        self.mass_flow = None
        self.crio_srate = None
        self.crio_lps = None
        self.comparison_data_file = None
        self.comparison_with_lit = False
        self.start_time = None
        self.end_time = None
        self.average_Ts = False
        
class GEOM:
    """Class defining geometry and physical properties."""

    def __init__(self):
        """Initialise Class."""
        self.r_cylinder = None          # Outer radial bound (m)
        self.kappa = None               # Thermal conductivity of the rod (W/m/K)
        self.rho = None                 # Density (kg/m3)
        self.Cp = None                  # Thermal capacity (J / kg / K)
        self.d_h = None                 # Hydraulic diameter (m)
        self.length = None              # Length of test region (m)
        
    def Calc_Alpha(self):
        """Calculate Alpha."""
        self.alpha = self.kappa / (self.rho * self.Cp)
    
    def Calc_AnnulusA(self):
        """Calculate Annulus Area."""
        self.annulus_area = ma.pi*((self.r_cylinder+(self.d_h/2))**2-self.r_cylinder**2)


class INITIAL:
    """Class defining initial conditions."""

    def __init__(self):
        """Initialise Class."""
        self.T_init = None                 # Initial temperature (K)

class DATA:
    """A class to handle data"""
    
    def __init__(self, gdata, geom):
        """ """
        self.gdata = gdata
        self.geom = geom

    def Import_Data(self):
        self.imported_data = Load_Data(self.gdata.data_files, self.gdata)
        for key in self.imported_data.keys():
            self.imported_data[key]['P in - filtered'] = Filter(self.imported_data[key]['P in'], self.gdata.crio_srate, self.gdata.crio_lps)
            self.imported_data[key]['P out - filtered'] = Filter(self.imported_data[key]['P out'], self.gdata.crio_srate, self.gdata.crio_lps)
    
    def Counts_To_Temps(self):
        for key in self.imported_data.keys():
            grad, intercept = self.gdata.calibration[key]
            self.imported_data[key]['cam temps'] = Conv_Counts(self.imported_data[key]['cam counts'], grad, intercept)
            self.imported_data[key]['cam temps - smooth'] = Smooth(self.imported_data[key]['cam temps'])

    def Mod_Data(self):
        self.mod_data = {}
        for test in self.imported_data.keys():
            self.mod_data[test] = {}
        for test in self.imported_data.keys():
            self.mod_data[test]['cam temps - smooth'] = self.imported_data[test]['cam temps - smooth']
            self.mod_data[test]['cam temps'] = self.imported_data[test]['cam temps']
            self.mod_data[test]['times'] = self.imported_data[test]['cam times']
            
            #store average values
            T_fs = []
            p_fs = []
            if self.gdata.average_Ts:
                for i in range(len(self.imported_data[test]['T in'])):
                    T_fs.append((self.imported_data[test]['T in'][i]+self.imported_data[test]['T out'][i])/2)
                    p_fs.append((self.imported_data[test]['P in'][i]+self.imported_data[test]['P out'][i])/2)
            
            else:
                for i in range(len(self.imported_data[test]['P in'])):
                    p_fs.append((self.imported_data[test]['P in'][i]+self.imported_data[test]['P out'][i])/2)
                    T_fs.append(self.imported_data[test]['T fluid'][i])
            #create an array which containts teh same number of values as the surface temperature values used. Calculated based on the temperature at or around the same time as the surface temperature time.
            T_fs_u = []
            T_in_u = []
            T_out_u = []
            p_fs_u = []
            p_in_u = []
            p_out_u = []
            p_inf_u = []
            p_outf_u = []
            for i in range(len(self.mod_data[test]['times'])):
                index = np.argwhere(self.imported_data[test]['crio times'] >= self.mod_data[test]['times'][i])[0][0]
                T_fs_u.append(T_fs[index])
                T_in_u.append(self.imported_data[test]['T in'][index])
                T_out_u.append(self.imported_data[test]['T out'][index])
                p_fs_u.append(p_fs[index])
                p_in_u.append(self.imported_data[test]['P in'][index])
                p_out_u.append(self.imported_data[test]['P out'][index])
                p_inf_u.append(self.imported_data[test]['P in - filtered'][index])
                p_outf_u.append(self.imported_data[test]['P out - filtered'][index])
            T_fs_u = np.array(T_fs_u)
            p_fs_u = np.array(p_fs_u)
            T_in_u = np.array(T_in_u)
            T_out_u = np.array(T_out_u)
            p_in_u = np.array(p_in_u)
            p_out_u = np.array(p_out_u)
            p_inf_u = np.array(p_inf_u)
            p_outf_u = np.array(p_outf_u)
            
            self.mod_data[test]['T fluid'] = T_fs_u
            self.mod_data[test]['P fluid'] = p_fs_u
            self.mod_data[test]['T in'] = T_in_u
            self.mod_data[test]['T out'] = T_out_u
            self.mod_data[test]['P in - filtered'] = p_inf_u
            self.mod_data[test]['P out - filtered'] = p_outf_u
            self.mod_data[test]['P in'] = p_in_u
            self.mod_data[test]['P out'] = p_out_u
    
    

class RESPONSE:
    """Class containing response functions."""

    def __init__(self, gdata, geom, initial):
        """Initialise Class."""
        self.gdata = gdata
        self.geom = geom
        self.initial = initial
        self.Calc_eig_values()
        self.Calc_coeffs()
        self.surface_r_solns = np.linspace(0.0, self.geom.r_cylinder, 20)
        if self.gdata.final_time:
            self.surface_t_solns = np.linspace(0.0, self.gdata.final_time, 20)
    
    def Temperature_Func(self, r, t):
        #solution of 1-D axi-symmetric conduction equation in cylindrical coords.
        #b.c.'s = no conduction at r=0, conduction = heat = conduction at r=R.
        #i.c.'s = T = T_init at t = 0.
        #reference = Transient heat conduction: Analytical methods
        summation = 0.0
        for i in range(self.gdata.n_bessel):
            exponent = ma.exp(-self.eig_values[i]**2*self.geom.alpha*t)
            bessel = special.jv(0, self.eig_values[i]*r)
            summation += exponent * bessel * self.coeffs[i]
        v = (r**2/(2*self.geom.kappa*self.geom.r_cylinder)) + (2*self.geom.alpha*t/(self.geom.kappa*self.geom.r_cylinder))
        T = v + summation
        return T
        
    def Impulse_Res_Func(self, r, t):
        """ Evaluates the impulse response at a time and radius. """
        imp_sum = 0.0
        for i in range(self.gdata.n_bessel):
            exponent = ma.exp(-self.eig_values[i]**2*self.geom.alpha*t)
            bessel = special.jv(0, self.eig_values[i]*r)
            imp_sum += -self.eig_values[i]**2 * self.geom.alpha * exponent * bessel * self.coeffs[i]
        v = 2.*self.geom.alpha/(self.geom.kappa*self.geom.r_cylinder)
        T_imp = v + imp_sum
        return T_imp
        
    def Create_Impulse_Matrix(self, r_imp_m, t_imp_m):
        n_elem = len(t_imp_m)
        lil_matrix = sparse.lil_matrix((n_elem, n_elem))
        impulses = []
        matrix_row = np.zeros(n_elem)
        t_diff_calc = t_imp_m[1] - t_imp_m[0]
        for n in range(n_elem):
            impulse = self.Impulse_Res_Func(r_imp_m, n*t_diff_calc)
            impulses.append(impulse)
            matrix_row[:n+1] = impulses[::-1]
            lil_matrix[n] = matrix_row[::-1]
        return lil_matrix
    
    def Calc_eig_values(self):
        """ Evaluates the eigenfunction of the analytical equations """
        def eig_func(x):
            return special.jv(1, x*self.geom.r_cylinder)
        ev_count = 0
        b_count = 0
        b_eps = 10 #distance to move the bounds
        self.eig_values = np.zeros(self.gdata.n_bessel)
        while ev_count < self.gdata.n_bessel:
            eig_val = roots(eig_func, b_count, b_count+b_eps)
            b_count += b_eps
            if len(eig_val) != 0:
                self.eig_values[ev_count] = eig_val[0]
                ev_count += 1
                
    def Calc_coeffs(self):
        """ This function calculates the coefficients for the impulse response equation. """
        self.coeffs = np.zeros(self.gdata.n_bessel)
        for i in range(self.gdata.n_bessel):
            def integ_func_1(x):
                f_x = 0.0 - ((x**2) / (2.*self.geom.kappa*self.geom.r_cylinder))
                return x * f_x * special.jv(0, self.eig_values[i]*x)
            def integ_func_2(y):
                f_y = 0.0 - ((y**2) / (2.*self.geom.kappa*self.geom.r_cylinder))
                return y*f_y
            self.coeffs[i] = (2.*integrate.quad(integ_func_1, 0.0, self.geom.r_cylinder)[0]) / (self.geom.r_cylinder**2*(special.jv(0, self.eig_values[i]*self.geom.r_cylinder))**2)
        self.coeffs[0] = (2.*integrate.quad(integ_func_2, 0.0, self.geom.r_cylinder)[0])/self.geom.r_cylinder**2
    
    def Unit_Step(self, us_rs, us_times):
        """ Calculates the temperature surface for a unit step heat flux from the analytical equation T(r,t)."""
        T_step_analytic = []     
        for rad in us_rs:
            temp_arr = np.zeros(len(us_times))
            for n in range(len(us_times)):
                temp_arr[n] = self.Temperature_Func(rad, us_times[n])
            T_step_analytic.append(temp_arr+self.initial.T_init)
        return T_step_analytic
               
    def Impulse(self, i_rs, i_times, q_vector = None, T_vector = None):
        """ """
        t_diff = i_times[1] - i_times[0]
        impulse_matrices = []
        for r in i_rs:
            impulse_matrix = self.Create_Impulse_Matrix(r, i_times)*t_diff
            sparse_csr_mat = sparse.csr_matrix(impulse_matrix)
            impulse_matrices.append(sparse_csr_mat)
        if q_vector is None:
            Ts = T_vector
        else:
            Ts = []
            T_initial = self.initial.T_init
            for i in range(len(impulse_matrices)):
                Ts.append(impulse_matrices[i].dot(q_vector)+T_initial)
        self.qs = []
        for i in range(len(impulse_matrices)):
            if q_vector is None:
                T_initial = Ts[i][0]
            self.qs.append(sparse.linalg.gmres(impulse_matrices[i], Ts[i] - T_initial)[0])
        
        self.q_avg = moving_average(self.qs[0], 50)
        return Ts, self.qs
    
    def Nu_Jak_1998(self, kin_visc, dens, test):
        """
        Using the values for periodic flow
        """
        a_o_n = 0.8
        a_u_n = 0.625
        t_n = 0.27
        b_n = 15.0    
        a_o_c = 0.04
        a_u_c = 0.136
        t_c = 0.3
        b_c = 12.9
        L_on_s = self.geom.length/(self.geom.d_h/2.)
        n = (a_o_n*np.exp(t_n*(L_on_s-b_n)) + a_u_n*np.exp(t_n*(-L_on_s+b_n))) / (np.exp(t_n*(L_on_s-b_n)) + np.exp(t_n*(-L_on_s+b_n)))
        c = (a_o_c*np.exp(t_c*(L_on_s-b_c)) + a_u_c*np.exp(t_c*(-L_on_s+b_c))) / (np.exp(t_c*(L_on_s-b_c)) + np.exp(t_c*(-L_on_s+b_c)))
        Q = self.gdata.mass_flow[test] / dens
        v_x = Q / self.geom.annulus_area
        omega = self.gdata.speeds[test]*2*ma.pi/60.
        v_rot = omega * self.geom.r_cylinder
        v_eff = np.sqrt(v_x**2 + v_rot**2)
        Re_eff = (v_eff*self.geom.length)/kin_visc
        Nu_TCP = c*(Re_eff**n)*self.geom.d_h/self.geom.length
        return Nu_TCP

    def Nu_CandT_1994(self, kin_visc, dens, Pr, test):
        Q = self.gdata.mass_flow[test] / dens
        v_x = Q / self.geom.annulus_area
        omega = self.gdata.speeds[test]*2*ma.pi/60.
        v_rot = omega * self.geom.r_cylinder
        Re_a = (v_x*self.geom.d_h)/kin_visc
        Nu_z = 0.023 * Re_a**0.8 * np.sqrt(Pr)
        Nu = Nu_z * 0.068 * (v_rot/v_x)**2 + Nu_z
        return Nu

    def Calc_Nusselt(self, modified_data, test):
        """ """
        #calculate fluid properties at centre point, inlet and outlet
        Pr_fluid = []
        k_fluid = []
        viscosity_fluid = []
        density_fluid = []
        for i in range(len(modified_data[test]['T fluid'])):
            dens_val = cp.PropsSI('D',  'P', modified_data[test]['P fluid'][i]*1e6, 'T', modified_data[test]['T fluid'][i]+273.15, self.gdata.fluid)
            k_val = cp.PropsSI('CONDUCTIVITY',  'D', dens_val,'P', modified_data[test]['P fluid'][i]*1e6, self.gdata.fluid)
            Pr_val = cp.PropsSI('Prandtl',  'D', dens_val,'P', modified_data[test]['P fluid'][i]*1e6, self.gdata.fluid)
            visc_val = cp.PropsSI('V',  'D', dens_val, 'P', modified_data[test]['P fluid'][i]*1e6, self.gdata.fluid)
            Pr_fluid.append(Pr_val)
            k_fluid.append(k_val)
            viscosity_fluid.append(visc_val)
            density_fluid.append(dens_val)
        Pr_fluid = np.array(Pr_fluid)
        k_fluid = np.array(k_fluid)
        viscosity_fluid = np.array(viscosity_fluid)
        density_fluid = np.array(density_fluid)
        kin_visc_fluid = viscosity_fluid/density_fluid
        
        self.Nu_jak = self.Nu_Jak_1998(kin_visc_fluid, density_fluid, test)
        self.Nu_CandT = self.Nu_CandT_1994(kin_visc_fluid, density_fluid, Pr_fluid, test)
            
        #calculate h value.
        self.h = -self.qs[0]/(modified_data[test]['cam temps'] - modified_data[test]['T fluid'])
        self.h_mavg = moving_average(self.h, 100)
        self.h_jak = self.Nu_jak*k_fluid/self.geom.d_h
        self.h_candt = self.Nu_CandT*k_fluid/self.geom.d_h
  
        #calculate dimensionless values
        omega = self.gdata.speeds[test]*2*ma.pi/60.
        v_axial = self.gdata.mass_flow[test]/(density_fluid*self.geom.annulus_area)
        v_rot = omega*self.geom.r_cylinder
        self.Ta_vals = (omega**2*self.geom.r_cylinder*(self.geom.d_h/2.)**3)/kin_visc_fluid**2
        self.Re_t_vals = (omega*self.geom.r_cylinder**2)/kin_visc_fluid
        self.Re_a_vals = v_axial*self.geom.d_h/kin_visc_fluid
        self.Nu_vals = self.h*self.geom.d_h/k_fluid
        self.Re_equiv_vals = np.sqrt(v_axial**2+v_rot**2)*self.geom.d_h/kin_visc_fluid
        

def Reynolds_Eff_conv(Re_eff, L, s):
    #conversion from effective reynolds to equivalent reynolds
    return Re_eff*2*s/L

def Nusselt_L_conv(Nu_l, L, s):
    #conversion from a length based nusselt number to hydraulic diameter based nusselt number
    return Nu_l*2*s/L

def moving_average(values, window):
    weights = np.repeat(1.0, window)/window
    values_1 = np.append(np.append(values[(window-1)-len(values)::-1], values), values[:-(window+1):-1])
    smas = np.convolve(values_1, weights, 'same')
    return smas[window:-window]

#define functions for finding roots
def rootsearch(f,a,b,dx):
    #searches for where a root is (or not) within the bounds a and b
    x1 = a; f1 = f(a)
    x2 = a + dx; f2 = f(x2)
    while f1*f2 > 0.0:
        if x1 >= b:
            return None,None
        x1 = x2; f1 = f2
        x2 = x1 + dx; f2 = f(x2)
    return x1,x2

def bisect(f,x1,x2,switch=0,epsilon=1.0e-9):
    #general bisection method
    f1 = f(x1)
    if f1 == 0.0:
        return x1
    f2 = f(x2)
    if f2 == 0.0:
        return x2
    if f1*f2 > 0.0:
        print('Root is not bracketed')
        return None
    n = int(ma.ceil(ma.log(abs(x2 - x1)/epsilon)/ma.log(2.0)))
    for i in range(n):
        x3 = 0.5*(x1 + x2); f3 = f(x3)
        if (switch == 1) and (abs(f3) >abs(f1)) and (abs(f3) > abs(f2)):
            return None
        if f3 == 0.0:
            return x3
        if f2*f3 < 0.0:
            x1 = x3
            f1 = f3
        else:
            x2 =x3
            f2 = f3
    return (x1 + x2)/2.0

def roots(f, a, b, eps=1.0):
    #Returns a list of roots for the given function f
    #It uses the bisection method to find the root, however the bounds used
    #are shifted over the function by eps. That is it begins with bounds
    #x1 = x1 and x2 = x1 + eps
    root_list = []
    while 1:
        x1,x2 = rootsearch(f,a,b,eps)
        if x1 != None:
            a = x2
            root = bisect(f,x1,x2,1)
            if root != None:
                pass
                root_list.append(root)
        else:
            break
    return root_list

def Conv_Counts(counts, m, c):
    temps = (counts - c)/m
    return temps

def Conv_Temps(temps, m, c):
    counts = m*temps + c
    return counts

def Create_q_Vector(q_in, q_ts, q_ts_max, function):
    q_vector = np.zeros(len(q_ts))
    if function == "step":
        for i in range(len(q_vector)):
            q_vector[i] = q_in  # square input
    elif function == "experiment approximation":
        for i in range(len(q_ts)):
            if i == 0:
                q_vector[i] = 0.0
            else: 
                q_vector[i] = q_in - q_ts[i]/q_ts_max  # experiment approximation           
    elif function == "sine wave":
        for i in range(len(q_ts)):
            if i == 0:
                q_vector[i] = 0.0
            else: 
                q_vector[i] = (q_in/2.)*np.sin(q_ts[i])  # sine wave input
    elif function == "ramp":
        for i in range(len(q_ts)):
            if i == 0:
                q_vector[i] = 0.0
            else: 
                q_vector[i] = q_in*q_ts[i]/q_ts_max #ramp input
    elif function == "ramp + sine wave":
        for i in range(len(q_ts)):
            if i == 0:
                q_vector[i] = 0.0
            else: 
                q_vector[i] = q_in*q_ts[i]/q_ts_max + (q_in/2.)*np.sin(q_ts[i]) #ramp + sine wave input
    else:
        raise MyError("Value supplied for gdata.validation_test, {0} not supported.".format(function))
    return q_vector    

def Convert_Times(times):
    # Outputs a list of times in seconds or "useful_times" 
    # converted from the string format (s_t_f)
    useful_times = []
    total_time = 0.0
    for i in range(len(times)):
        if i == 0:
            useful_times.append(total_time)
        else:
            time_prev = dt.datetime.strptime(times[i-1], s_t_f)
            time_now = dt.datetime.strptime(times[i], s_t_f)
            time_diff = (time_now - time_prev).total_seconds()
            total_time += time_diff
            useful_times.append(total_time)
    return useful_times

def Load_Data(data_files, gdata, load_FEA_data = False):
    if load_FEA_data:
        FEA_data = []
        for data_file in data_files:
            FEA_data.append(pd.read_csv(data_file))
        return FEA_data
    else:
        tests = []
        test_dicts = {}
        for data_file in data_files:
            with open(data_file, "rt") as csv_file:
                _reader = csv.DictReader(csv_file)
                count = 0
                for row in _reader:
                    if count == 0:
                        test_val = int(float(row['Test Number']))
                        if test_val in tests:
                            pass
                        else:
                            tests.append(test_val)
                            test_dicts[test_val] = {}
                    else:
                        pass
                    count += 1       
        for test_val in tests:
            test_dicts[test_val]['cam times'] = []
            test_dicts[test_val]['cam counts'] = []
            test_dicts[test_val]['crio times'] = []
            test_dicts[test_val]['T in'] = []
            test_dicts[test_val]['T out'] = []
            test_dicts[test_val]['P in'] = []
            test_dicts[test_val]['P out'] = []
            test_dicts[test_val]['mass flow'] = []
            test_dicts[test_val]['test times'] = []
            test_dicts[test_val]['cam temps'] = []
            test_dicts[test_val]['T fluid'] = []
        for data_file in data_files:
            indicator = 0
            with open(data_file, "rt") as csv_file:
                _reader = csv.reader(csv_file)
                line_count = 0
                for row in _reader:
                    if len(row) == 0:
                        continue
                    else:
                        if line_count == 0:
                            if 'Test Increment' in row:
                                print("Loading CRIO file...")
                                indicator = 1
                            else:
                                print("Loading Camera file...")
                        elif line_count == 1:
                            if indicator == 0:
                                test_val = int(float(row[2]))
                            else:
                                test_val = int(float(row[3]))
                        elif line_count != 0:
                            if indicator == 0:
                                test_dicts[test_val]['cam times'].append(row[0])
                                test_dicts[test_val]['cam counts'].append(float(row[1]))
                            else:
                                test_dicts[test_val]['crio times'].append(row[2])
                                test_dicts[test_val]['T in'].append(float(row[10]))
                                test_dicts[test_val]['T out'].append(float(row[11]))
                                test_dicts[test_val]['P in'].append(float(row[12]))
                                test_dicts[test_val]['P out'].append(float(row[13]))
                                test_dicts[test_val]['mass flow'].append(float(row[9]))
                                test_dicts[test_val]['T fluid'].append(float(row[8]))
                                if float(row[6]) == 1.0:
                                    test_dicts[test_val]['test times'].append(row[2])
                        line_count += 1
                
                print("Loaded ", line_count, " rows of data.")
            if indicator == 0:
                test_dicts[test_val]['cam times'] = np.array(test_dicts[test_val]['cam times'])
                test_dicts[test_val]['cam counts'] = np.array(test_dicts[test_val]['cam counts'])
            else:
                test_dicts[test_val]['crio times'] = np.array(test_dicts[test_val]['crio times'])
                test_dicts[test_val]['T in'] = np.array(test_dicts[test_val]['T in'])
                test_dicts[test_val]['T out'] = np.array(test_dicts[test_val]['T out'])
                test_dicts[test_val]['P in'] = np.array(test_dicts[test_val]['P in'])
                test_dicts[test_val]['P out'] = np.array(test_dicts[test_val]['P out'])
                test_dicts[test_val]['mass flow'] = np.array(test_dicts[test_val]['mass flow'])
                test_dicts[test_val]['test times'] = np.array(test_dicts[test_val]['test times'])
                test_dicts[test_val]['cam temps'] = np.array(test_dicts[test_val]['cam temps'])
                test_dicts[test_val]['T fluid'] = np.array(test_dicts[test_val]['T fluid'])
                
        for test in tests:
            cam_start = dt.datetime.strptime(gdata.cam_start[test], s_t_f) # time cam starts rec (according to cam)
            crio_start = dt.datetime.strptime(test_dicts[test]['crio times'][0], s_t_f) # time the tc starts recording.
            test_start = dt.datetime.strptime(test_dicts[test]['test times'][0], s_t_f)
            test_finish = dt.datetime.strptime(test_dicts[test]['test times'][-1], s_t_f)
            cor_cam_times = []
            cor_crio_times = Convert_Times(test_dicts[test]['crio times'])
            current_time = 0.0
            for i in range(len(test_dicts[test]['cam times'])):
                if i == 0:
                    start_diff = (cam_start - crio_start).total_seconds()
                    current_time += start_diff
                    cor_cam_times.append(current_time)
                else:
                    uncorr_prev = dt.datetime.strptime(test_dicts[test]['cam times'][i-1], s_t_f)
                    uncorr_now = dt.datetime.strptime(test_dicts[test]['cam times'][i], s_t_f)
                    diff = (uncorr_now - uncorr_prev).total_seconds()
                    current_time += diff
                    cor_cam_times.append(current_time)
            cor_cam_times = np.array(cor_cam_times)
            test_start_rel = (test_start - crio_start).total_seconds()
            test_finish_rel = (test_finish - crio_start).total_seconds()
            test_dicts[test]['cam times'] = cor_cam_times
            test_dicts[test]['crio times'] = cor_crio_times
            test_dicts[test]['test times'] = [test_start_rel, test_finish_rel]
        return test_dicts

def PSD(F):
    return F.imag*F.imag + F.real*F.real #power spectral density

def Spectal_Analysis(analysis_data, s_rate, flp, test_num):
    """
    flp = <low pass filter cutoff>
    s_rate = <sampling rate>
    analysis_data = <data to perform spectral analysis on>
    """
    N = len(analysis_data)
    time = np.linspace(0,1/s_rate*N,N)
    freq = fft.rfftfreq(N, 1/s_rate)
    accelerance = fft.rfft(analysis_data)*(1./s_rate)
    b,a = signal.butter(3, flp/s_rate, 'low', analog=False)
    filtered = signal.filtfilt(b, a, analysis_data, padtype='even')
    filt_accel = fft.rfft(filtered)*(1/s_rate)

    data_fig, ax1 = plt.subplots()
    ax1.plot(time, analysis_data, color = 'black', marker = 'x', linestyle = None, label = 'Raw data')
    ax1.plot(time, filtered, color = 'green', linestyle = '--', marker = '*', markersize = 3, label = 'Filtered data')
    ax1.set_title('Data for Spectral Analysis (Test '+str(test_num)+')')
    ax1.set_ylabel('Data')
    ax1.set_xlabel('Time, s')
    
    freq_fig, ax1 = plt.subplots()
    ax1.plot(freq, accelerance)
    ax1.set_title('accelerance (Test '+str(test_num)+')')
    ax1.set_xlabel('Frequency, Hz')
    
    filt_fig, ax1 = plt.subplots()
    ax1.plot(freq, filt_accel)
    ax1.set_title('accelerance (Filtered) (Test '+str(test_num)+')')
    ax1.set_xlabel('Frequency, Hz')

def Filter(analysis_data, s_rate, flp):
    """
    flp = <low pass filter cutoff>
    s_rate = <sampling rate>
    analysis_data = <data to perform spectral analysis on>
    """
    b,a = signal.butter(3, flp/(0.5*s_rate), 'low', analog=False)
    filtered = signal.filtfilt(b, a, analysis_data, padtype='even')
    return filtered

def Smooth(raw_data):
    smooth_data = signal.savgol_filter(raw_data, 301, 2)
    return smooth_data

def main(uoDict):
    """
    Evaluate main function.

    Loads data, does computation, plots outputs, etc...
    """
    print("")
    print("Setting up job file...")
    print("")
    # main file to be executed
    jobFileName = uoDict.get("--job", "test")

    # ass .py extension is forgotten
#    if ".py" not in jobFileName:
#        jobName = jobFileName
#        jobFileName = ''.join([jobFileName, ".py"])
#    else:
#        # strip .py extension from jobName
#        jobName = jobFileName.replace('.py', '')

    # Make sure that the desired job file actually exists
#    if jobFileName not in os.listdir('.'):
#        local_directory = os.path.dirname(os.path.realpath(__file__))
#        raise MyError("No job file {0} found in directory: {1}"
#                      .format(jobFileName, local_directory))
        
    out_file = uoDict.get("--out_file", "none")
        
    print("")
    print("Initializing classes...")
    print("")
    geom = GEOM()
    gdata = GDATA()
    initial = INITIAL()

    # Set verbosity (can be overwritten from jobfile)
    gdata.verbosity = float(uoDict.get("--verbosity", 0))

    # Execute jobFile, this creates all the variables
    exec(open(jobFileName).read(), globals(), locals())

    # optional code to check that correct setting in classes
    # geom.check()
    
    if gdata.data_files:
        
        geom.Calc_Alpha()
        geom.Calc_AnnulusA()
        
        #1. load data
        print("")
        print("Loading data...")
        print("")
        data = DATA(gdata, geom)
        data.Import_Data()
        data.Counts_To_Temps()
        print("")
        print("Successfully loaded the data.")
        print("")
        
        print("")
        print("Modifying data for convenience...")
        print("")
        data.Mod_Data()
        
        
        if out_file is not "none":
            print("")
            print("CREATING OUTPUT FILE")
            
            if os.path.isfile(out_file):
                print("Output file already exists.")
                fntemp = out_file+".old"
                shutil.copyfile(out_file, fntemp)
                print("Existing file has been copied to:", fntemp)

            with open(out_file, 'w') as output_csv:
                output_writer = csv.writer(output_csv)
                output_writer.writerow([" "])
                output_writer.writerow(["NISI_Shaft outfile HEADER."])
                output_writer.writerow([" "])

        print("")
        print("Calculating heat flux information based on data imported...")
        print("")
        Nus = []
        Re_equivs = []
        for test in data.imported_data.keys():
#            si = np.argwhere(data.mod_data[test]['times']>=gdata.start_time)[0][0]
#            fi = np.argwhere(data.mod_data[test]['times']>=gdata.end_time)[0][0]
            result_1 = RESPONSE(gdata, geom, initial)
            T_s, q_calculated = result_1.Impulse([geom.r_cylinder], data.mod_data[test]['times'], T_vector=[data.mod_data[test]['cam temps']])
            print("")
            print("Starting HTC calculation for Test ", test)
            print("")
            result_1.Calc_Nusselt(data.mod_data, test)
            
            if out_file is not "none":
                #create flat lists for ease of putting into output file
                print("")
                print("Populating output file...")
                print("")
                titles = ["Time", "T_in", "T_out", "T_fluid", "T_shaft", "Heat Flux", "HTC", "Taylor", "Axial Reynolds", "Nusselt", "Equivalent Reynolds"]
                outputs = zip(data.mod_data[test]['times'], data.mod_data[test]['T in'], data.mod_data[test]['T out'], data.mod_data[test]['T fluid'], 
                              data.mod_data[test]['cam temps'], q_calculated[0], result_1.h, result_1.Ta_vals, result_1.Re_a_vals, result_1.Nu_vals, result_1.Re_equiv_vals)
                with open(out_file, 'a') as output_csv:
                    output_writer = csv.writer(output_csv)
                    output_writer.writerow([" "])
                    output_writer.writerow(["Test", test])
                    output_writer.writerow([" "])
                    output_writer.writerow(["Static Variables"])
                    output_writer.writerow(["geom.r_cylinder", geom.r_cylinder])
                    output_writer.writerow(["geom.length", geom.length])
                    output_writer.writerow(["geom.d_h", geom.d_h])
                    output_writer.writerow(["geom.rho", geom.rho])
                    output_writer.writerow(["geom.Cp" , geom.Cp])
                    output_writer.writerow(["geom.kappa", geom.kappa])
                    output_writer.writerow(["gdata.speed", gdata.speeds[test]])
                    output_writer.writerow(["gdata.mass_flow", gdata.mass_flow[test]])
                    output_writer.writerow(["gdata.calibration", gdata.calibration[test][0], gdata.calibration[test][1]])
                    output_writer.writerow(titles)
                    output_writer.writerows(outputs)
            
            if gdata.exp_indiv_plots:
                si = np.argwhere(data.mod_data[test]['times']>=gdata.start_time)[0][0]
                fi = np.argwhere(data.mod_data[test]['times']>=gdata.end_time)[0][0]
                
                #plot temperature and counts for tests
                fig_temps, ax1 = plt.subplots()
                ax2 = ax1.twinx()
                ax1.set_xlim(data.imported_data[test]['crio times'][0]-5.0, data.imported_data[test]['crio times'][-1]+5.0)
                counts_pl, = ax1.plot(data.imported_data[test]['cam times'][si:fi], data.imported_data[test]['cam counts'][si:fi], linestyle = 'None', color = 'black', marker = 'x', markersize = 5, label = 'Shaft Surface - Counts')
                temp_raw_pl, = ax2.plot(data.imported_data[test]['cam times'][si:fi], data.imported_data[test]['cam temps'][si:fi], linestyle = 'None', color = 'green', marker = '*', markersize = 3, label = 'Shaft Surface - Temperature')
#                temp_filt_pl, = ax1.plot(data.imported_data[test]['cam times'], data.imported_data[test]['cam temps - smooth'], color = 'green', linestyle = '-', label = 'Shaft Surface - Temperature (Smoothed)')
#                Tfluid_pl, = ax2.plot(data.mod_data[test]['times'], data.mod_data[test]['T fluid'], color = 'blue', linestyle = '-.', label = 'Fluid - Temperature Middle')
#                Tin_pl, = ax2.plot(data.imported_data[test]['crio times'], data.imported_data[test]['T in'], color = 'blue', linestyle = '-', label = 'Fluid - Temperature In')
#                Tout_pl, = ax2.plot(data.imported_data[test]['crio times'], data.imported_data[test]['T out'], color = 'blue', linestyle = '--', label = 'Fluid - Temperature Out')
#                ax1.axvline(x=gdata.start_time, color = 'orange', linestyle = '--')
#                ax1.axvline(x=gdata.end_time, color = 'orange', linestyle = '--')
#                ax2.set_ylim(np.min(data.imported_data[test]['T in'])-5, np.max(data.imported_data[test]['T in'])+5)
#                ax1.set_ylim(Conv_Temps(np.min(data.imported_data[test]['T in'])-5, gdata.calibration[test][0], gdata.calibration[test][1]), Conv_Temps(np.max(data.imported_data[test]['T in'])+5, gdata.calibration[test][0], gdata.calibration[test][1]))
#                ax1.set_title('Test = '+str(test))
                ax1.set_xlabel('Time, s')
                ax1.set_ylabel('Camera Counts')
                ax2.set_ylabel('Temperature, $^\circ$C')
#                Tfluid_pl,
#                params = [counts_pl, temp_raw_pl,  temp_filt_pl, Tin_pl, Tout_pl]
                params = [counts_pl, temp_raw_pl]
                legend = ax1.legend(params, [p.get_label() for p in params])
                
                temp_diff = data.imported_data[test]['cam temps - smooth'] - data.imported_data[test]['cam temps']
                print("The 95% confidence interval from the smoothed temperature to the raw is +/-", 1.96*np.std(temp_diff))
                
                #plot plenum conditions
                fig_plenum, ax1 = plt.subplots()
#                ax2 = ax1.twinx()
                ax1.set_xlim(data.imported_data[test]['crio times'][0]-5.0, data.imported_data[test]['crio times'][-1]+5.0)
#                pin_pl, = ax1.plot(data.imported_data[test]['crio times'], data.imported_data[test]['P in - filtered'], color = 'black', linestyle = '-', label = 'Pressure - In')
#                pout_pl, = ax1.plot(data.imported_data[test]['crio times'], data.imported_data[test]['P out - filtered'], color = 'black', linestyle = '--', label = 'Pressure - Out')
                temp_diff_pl, = ax1.plot(data.mod_data[test]['times'], data.mod_data[test]['cam temps'] - data.mod_data[test]['T fluid'], color = 'green', linestyle = '--', label = 'Temperature difference')
                ax1.axvline(x=gdata.start_time, color = 'orange', linestyle = '--')
                ax1.axvline(x=gdata.end_time, color = 'orange', linestyle = '--')
                ax1.set_title("Test = "+str(test))
                ax1.set_xlabel('Time, s')
#                ax1.set_ylabel('Pressure, MPa')
                ax1.set_ylabel('Temperature Difference, K')
#                params = [pin_pl, pout_pl, temp_diff_pl]
                params = [temp_diff_pl]
                legend = ax1.legend(params, [p.get_label() for p in params])
                
                #plot combined temperatures
                fig_combT, ax1 = plt.subplots()
                ax1.set_xlim(data.imported_data[test]['crio times'][0]-5.0, data.imported_data[test]['crio times'][-1]+5.0)
                Tavg_pl, = ax1.plot(data.mod_data[test]['times'], data.mod_data[test]['T fluid'], color = 'green', linestyle = '-.', label = 'Temperature - Avg')
                Tshaft_pl, = ax1.plot(data.imported_data[test]['cam times'], data.imported_data[test]['cam temps - smooth'], color = 'black', linestyle = '--', label = 'Shaft Surface - Temperature (smoothed)')
                Tin_pl, = ax1.plot(data.imported_data[test]['crio times'], data.imported_data[test]['T in'], color = 'green', linestyle = '-', label = 'Temperature - In')
                Tout_pl, = ax1.plot(data.imported_data[test]['crio times'], data.imported_data[test]['T out'], color = 'green', linestyle = '--', label = 'Temperature - Out')
                ax1.axvline(x=gdata.start_time, color = 'orange', linestyle = '--')
                ax1.axvline(x=gdata.end_time, color = 'orange', linestyle = '--')
                ax1.set_title("Test = "+str(test))
                ax1.set_xlabel('Time, s')
                ax1.set_ylabel('Temperature, $^\circ$C')
                params = [Tavg_pl, Tshaft_pl, Tin_pl, Tout_pl]
                legend = ax1.legend(params, [p.get_label() for p in params])
                
                #plot heat flux and HTC for each test
                t_seg = data.mod_data[test]['times'][si:fi]
                h_seg_avg = [np.average(result_1.h[si:fi])]*len(t_seg)
                fig_HTC, ax1 = plt.subplots()
#                ax2 = ax1.twinx()
                ax1.set_xlim(data.imported_data[test]['crio times'][0]-5.0, data.imported_data[test]['crio times'][-1]+5.0)
#                q_pl, = ax1.plot(data.mod_data[test]['times'], q_calculated[0], color = 'black', linestyle = '--', alpha = 0.5, label = 'Surface Heat Flux')
#                q_pl2, = ax1.plot(data.mod_data[test]['times'], result_1.q_avg, color = 'black', linestyle = '-.', label = 'Surface Heat Flux - moving average')
                #plot HTC only between start and end times specified
                HTC_pl, = ax1.plot(data.mod_data[test]['times'], result_1.h, color = 'green', alpha = 0.8, linestyle = '--', label = 'HTC')
#                HTC_pl2, = ax1.plot(t_seg, h_seg_avg, color='blue', linestyle='--', alpha = 0.8, label = 'HTC - average')
#                HTC_pl, = ax1.plot(data.mod_data[test]['times'], result_1.h, color = 'lime', alpha = 0.8, linestyle = '--', label = 'HTC')
#                HTC_pl2, = ax1.plot(t_seg, h_seg_avg, color='blue', linestyle='--', alpha = 0.8, label = 'HTC - average')
#                HTC_pl3, = ax2.plot(data.mod_data[test]['times'], result_1.h_mavg, color = 'darkgreen', linestyle = '-.', label = 'HTC - Avg')
                ax1.axvline(x=gdata.start_time, color = 'orange', linestyle = '--')
                ax1.axvline(x=gdata.end_time, color = 'orange', linestyle = '--')
                ax1.set_title("Test = "+str(test))
                ax1.set_xlabel('Time, s')
#                ax1.set_ylabel('Heat Flux, W/m$^2$')
#                ax2.set_ylabel('HTC, W/m$^2$K')
                ax1.set_ylabel('HTC, W/m$^2$K')
#                params = [HTC_pl, HTC_pl2, q_pl, q_pl2]
                params = [HTC_pl]
                legend = ax1.legend(params, [p.get_label() for p in params])
                
                q_flux_diff = result_1.q_avg - q_calculated[0]
                print("The 95% confidence interval from the moving average heat flux to the raw calculated is +/-", 1.96*np.std(q_flux_diff))
                print("The average value of Nu over the stabilised region is ", np.average(result_1.Nu_vals[si:fi]))
                print("The average value of Ta over the stabilised region is ", np.average(result_1.Ta_vals[si:fi]))
                print("The average value of Rea over the stabilised region is ", np.average(result_1.Re_a_vals[si:fi]))
                print("The average value of Ree over the stabilised region is ", np.average(result_1.Re_equiv_vals[si:fi]))
            if gdata.spectral_analysis:
                Spectal_Analysis(data.mod_data[test][gdata.spectral_analysis_data], gdata.crio_srate, gdata.crio_lps, test)
                
        if gdata.comparison_with_lit:
            
            
            
            #Compare with literature - Jakoby
            jakoby_L = 0.144 #Jakoby channel length for modified Nusselt number
            jakoby_s1 = 0.01
            jakoby_s2 = 0.015
            jakoby_re_eff_10 = []
            jakoby_nu_l_10 = []
            jakoby_re_eff_15 = []
            jakoby_nu_l_15 = []
            with open(gdata.comparison_data_file, "rt") as csv_file:
                _reader = csv.reader(csv_file)
                count = 0
                for row in _reader:
                    if count <= 5:
                        jakoby_re_eff_10.append(float(row[0]))
                        jakoby_nu_l_10.append(float(row[1]))
                    else:
                        jakoby_re_eff_15.append(float(row[0]))
                        jakoby_nu_l_15.append(float(row[1]))
                    count += 1
            jakoby_nu_10 = []
            jakoby_re_equiv_10 = []
            jakoby_nu_15 = []
            jakoby_re_equiv_15 = []
            for i in range(len(jakoby_nu_l_10)):
                jakoby_nu_10.append(Nusselt_L_conv(jakoby_nu_l_10[i], jakoby_L, jakoby_s1))
                jakoby_re_equiv_10.append(Reynolds_Eff_conv(jakoby_re_eff_10[i], jakoby_L, jakoby_s1))
            for i in range(len(jakoby_nu_l_15)):
                jakoby_nu_15.append(Nusselt_L_conv(jakoby_nu_l_15[i], jakoby_L, jakoby_s2))
                jakoby_re_equiv_15.append(Reynolds_Eff_conv(jakoby_re_eff_15[i], jakoby_L, jakoby_s2))
            fig_Nu, ax1 = plt.subplots()
            jakob_pl, = ax1.plot(jakoby_re_equiv_10, jakoby_nu_10, 'g+')
            jakob_pl2, = ax1.plot(jakoby_re_equiv_15, jakoby_nu_15, 'go')
            ax1.ticklabel_format(style='sci', axis = 'x', scilimits=(0,0))
            ax1.set_xlabel("Equivalent Reynolds Number")
            ax1.set_ylabel("Nusselt Number")
            jak_data_label2 = mlines.Line2D([], [], color='green', marker='o', linestyle='None', label='Ta$_{ref}$ - Jakoby 1998')
            jak_data_label1 = mlines.Line2D([], [], color='green', marker='+', linestyle='None', label='Ta$_{ref}$ x(2/3)$^3$ - Jakoby 1998')
            ax1.legend(handles=[jak_data_label2, jak_data_label1])
  
                
    if gdata.validation_flag is True:
        
        geom.Calc_Alpha()
        
        # 1. Create input for q.
        print("")
        print("Producing verification data...")
        print("")
        if gdata.time_step <= 0.001:
            print("")
            print("Generating time step verification data for a time step < 0.001 seconds will consume significant memory and time.")
            print("Consider using a slightly larger time step.")
            print("")
        # create time array
#        time = 0.0
#        t_vals = []
#        while time < gdata.final_time:
#            t_vals.append(time)
#            time += gdata.time_step
#        t_vals.append(time)
#        time_array = np.array(t_vals)  # time array
        time_array = np.linspace(0.0, gdata.final_time, int(gdata.final_time/gdata.time_step))
        
        # create heat array
        q_original = Create_q_Vector(gdata.validation_heat_in, time_array, gdata.final_time, gdata.validation_function)
        # 2. Get T input and q calculated
        result_1 = RESPONSE(gdata, geom, initial)
        T_calculated, q_calculated = result_1.Impulse(gdata.space_solutions, time_array, q_vector = q_original)
        
        #3. plots
        print("")
        print("Plotting...")
        print("")
        #q original and q calced
        fig_qs, ax1 = plt.subplots()
        ax1.plot(time_array, q_original, linestyle = '-', color = 'black', label = 'Original q input')
        ax1.plot(time_array, q_calculated[-1], linestyle = '--', color = 'green', label = 'Calculated q')
        ax1.set_ylabel('Heat flux, W/m$^2$')
        ax1.set_xlabel('Time, s')
        ax1.yaxis.grid(which='major', linestyle='--')
        ax1.set_title('Applied and calculated '+gdata.validation_function+' surface heat flux - Comparison')
        ax1.legend(loc=4)
        
        #difference between q original and q calced
        q_diff = q_original - q_calculated[-1]
        fig_diffq, ax1 = plt.subplots()
        ax1.plot(time_array, q_diff, linestyle = '-', color = 'green', label = 'Difference in q' )
        ax1.yaxis.grid(which='major', linestyle='--')
        ax1.set_ylabel('Heat flux difference, W/m$^2$')
        ax1.set_xlabel('Time, s')
        ax1.set_title('Applied and calculated '+gdata.validation_function+' surface heat flux - Difference')
        ax1.legend(loc=4)
        
        #Temperature plots
        #Temperatures at constant radii
        fig_Ts1, ax1 = plt.subplots()
        for i in range(len(gdata.space_solutions)):
            ax1.plot(time_array, T_calculated[i], label = 'Radius = '+str(gdata.space_solutions[i])+'m' )
        ax1.set_title('Temperature profiles over time')
        ax1.set_ylabel('Temprature, K')
        ax1.set_xlabel('Time, s')
        ax1.legend()
        
        #Surface temperatures calculated for varying time_steps
        if gdata.time_solutions:
            print("")
            if gdata.final_time > 5.1:
                print("Generating time step verification data for gdata.final_time > 5 seconds will consume significant memory and time.")
                print("Consider changing gdata.final_time to a lower value for better efficiency.")
            if all(ts >= 0.001 for ts in gdata.time_solutions) is False:
                print("Generating time step verification data for a time step < 0.001 seconds will consume significant memory and time.")
                print("Consider changing the gdata.time_solutions to contain larger values for better efficiency.")
            print("Creating time step verification data...This may take a minute")
            check_time_steps = gdata.time_solutions
            time_lengths = [gdata.final_time]*len(check_time_steps)
            
            result_2 = RESPONSE(gdata, geom, initial)
            fig_Ts2, ax1 = plt.subplots()
            for i in range(len(check_time_steps)):
                print("Creating time step verification data for $\Delta$t = ", check_time_steps[i], "s.")
                time = 0.0
                t_vals = []
                while time < time_lengths[i]:
                    t_vals.append(time)
                    time += check_time_steps[i]
                t_vals.append(time)
                
                time_array_v = np.array(t_vals)  # time array
                q_Ts2_plot = Create_q_Vector(gdata.validation_heat_in, time_array_v, gdata.final_time, gdata.validation_function)

                T_calculated, q_calculated = result_2.Impulse([geom.r_cylinder], time_array_v, q_vector = q_Ts2_plot)
                for j in range(len(T_calculated)):
                    ax1.plot(time_array_v, T_calculated[j], label = '$\Delta$t = '+str(check_time_steps[i])+'s.')
            ax1.set_title('Time step comparison using surface temperature')
            ax1.yaxis.grid(which='major', linestyle='--')
            ax1.set_ylabel('Temperature, K')
            ax1.set_xlabel('Time, s')
            ax1.legend()
        
        if gdata.FEA_data:
            print("")
            print("Preparing to display FEA and NISI_Shaft.py data for comparion...")
            print("Note: The FEA data must match the gdata.validation_function and gdata.heat_in to get meaningful results.")
            print("")
            #get data
            data = Load_Data(gdata.FEA_data, gdata, load_FEA_data=True)
#            T_calculated, q_calculated = result_1.Impulse([geom.r_cylinder], time_array, q_vector = q_original)
            
           
            
            #plot FEA and NISI_Shaft results
            fig_FEA_Ts, ax1 = plt.subplots()
            for index, data_group in enumerate(data):
                if 'Temperature' in list(data_group):
                    time_s = data_group['Time'].values
                    FEA_T = data_group['Temperature'].values
                    
#                    func = interpolate.InterpolatedUnivariateSpline(data_group['Time'].values, data_group['Temperature'].values)
                    
#                    time_FEA_new = time_array
#                    T_FEA_new = func(time_FEA_new)
            ax1.plot(time_s, FEA_T, 'k-', label = 'FEA Result')
#                    ax1.plot(data_group['Time'].values, data_group['Temperature'].values, 'k-', alpha = 1.0/(index+1.), label = 'FEA Result_'+str(index))
            q_original = Create_q_Vector(gdata.validation_heat_in, time_array, gdata.final_time, gdata.validation_function)
#            ax1.plot(time_s, q_original)
            T_calculated, q_calculated = result_1.Impulse([geom.r_cylinder], time_array, q_vector = q_original)
            ax1.plot(time_array, T_calculated[0], color = 'green', linestyle = '--', label = 'Method Result')
#            ax1.set_title('FEA result and NISI_Shaft.py Result - Comparison')
            ax1.yaxis.grid(which='major', linestyle='-')
            ax1.set_ylabel('Temperature, K')
            ax1.set_xlabel('Time, s')
            ax1.legend()
            
            #plot difference
            fig_FEA_diff_Ts, ax1 = plt.subplots()
            for index, data_group in enumerate(data):
                if 'Temperature' in list(data_group):
                    func = interpolate.InterpolatedUnivariateSpline(time_array, T_calculated[0])
                    time_Imp_new = time_s
                    T_Imp_new = func(time_Imp_new)
                    func = interpolate.InterpolatedUnivariateSpline(time_s, FEA_T)
                    T_FEA_new = func(time_array)
            T_diff = FEA_T - T_Imp_new
            ax1.plot(time_s, T_diff, color = 'black', linestyle = '-', label = 'Difference between FEA and Method Result')
#            ax1.set_title('FEA result and NISI_Shaft.py Result - Difference')
            ax1.set_ylabel('Temperature difference, K')
            ax1.yaxis.grid(which='major', linestyle='-')
            ax1.set_xlabel('Time, s')
            ax1.legend()
            
            
            if gdata.FEA_flux_flag:
                #plot FEA and NISI_Shaft results
                fig_FEA_q, ax1 = plt.subplots()
                for index, data_group in enumerate(data):
                    if 'Heat Flux' in list(data_group):
                        FEA_q = data_group['Heat Flux'].values
#                        time_FEA_new = time_array
#                        Q_FEA_new = func(time_FEA_new) 
                        T_s, q_calculated = result_1.Impulse([geom.r_cylinder], time_array, T_vector=[T_FEA_new])
#                        
                ax1.plot(time_s, FEA_q, 'k-', label = 'FEA Result')
#                        ax1.plot(data_group['Time'].values, data_group['Heat Flux'].values, 'k-', alpha = 1.0/(index+1.), label = 'FEA Result_'+str(index))
                ax1.plot(time_array, q_calculated[0], color = 'green', linestyle = '--', label = 'Impulse Result')
                ax1.set_ylabel('Heat Flux, W/m$^2$')
                ax1.yaxis.grid(which='major', linestyle='-')
                ax1.set_xlabel('Time, s')
                ax1.legend()
                
#                print('95% confidence interval of impulse method = ', 1.96*np.std(q_calculated[0] - q_original))
#                print('95% confidence interval of FEA method = ', 1.96*np.std(Q_FEA_new - q_original))
#                fig_FEA_q1, ax1 = plt.subplots()
#                ax1.plot(time_array, q_calculated[0], color = 'green', linestyle = '--', label = 'Impulse Method Result')
#                ax1.plot(time_array, q_original, color = 'green', linestyle = '-', label = 'Original Input')
#                ax1.ticklabel_format(useOffset=False, style='plain')
#                ax1.set_ylabel('Heat Flux, W/m$^2$')
#                ax1.set_xlabel('Time, s')
#                ax1.legend()
                
                
#                
#                fig_FEA_q2, ax1 = plt.subplots()
#                ax1.plot(time_FEA_new, Q_FEA_new, 'k-', alpha = 1.0/(index+1.), label = 'FEA Result')
#                ax1.plot(time_array, q_original, color = 'green', linestyle = '-', label = 'Original Input')
#                ax1.set_ylabel('Heat Flux, W/m$^2$')
#                ax1.set_xlabel('Time, s')
#                ax1.legend()
                
                #plot difference
                fig_FEA_diff, ax1 = plt.subplots()
                for index, data_group in enumerate(data):
                    if 'Heat Flux' in list(data_group):
                        func = interpolate.InterpolatedUnivariateSpline(time_array, q_calculated[0])
                        time_Imp_new = time_s
                        Q_Imp_new = func(time_Imp_new) 
                Q_diff = FEA_q - Q_Imp_new
                ax1.plot(time_s, Q_diff, color = 'black', linestyle = '-', label = 'Difference between FEA and Impulse')
                ax1.yaxis.grid(which='major', linestyle='-')
                ax1.set_ylabel('Heat Flux Difference, W/m$^2$')
                ax1.set_xlabel('Time, s')
                ax1.legend()
            
    if gdata.unit_step is True:
        geom.Calc_Alpha()
        
        #unit step analysis (analytical and impulse response method)
        if initial.T_init is None:
            print("The initial temperature 'initial.T_init' must be set to a value in the job file.")
        # 1. create unit step q input
#        print("")
        print("Setting up unit step calculations... This may take a minute.")
        print("")
        # create time array
        time = 0.0
        t_vals = []
        while time <= gdata.final_time:
            t_vals.append(time)
            time += gdata.time_step
        time_array = np.array(t_vals)  # time array
        
        # create q vector
        q_US = np.zeros(len(t_vals))
        for i in range(len(t_vals)):
            q_US[i] = 1.0 #q vector
                
        # 2. get T input and q calculated for impulse and T analytical response
        res_US_a = RESPONSE(gdata, geom, initial)
        res_US_i = RESPONSE(gdata, geom, initial)
        T_a = res_US_a.Unit_Step(gdata.space_solutions, time_array)
        T_impulse, q_impulse = res_US_i.Impulse(gdata.space_solutions, time_array, q_vector = q_US)
        
        # 3D surface plots
        #surface calculations
        print("")
        print("Doing surface calculations...")
        print("")
        T_a_surf_r = []
        for rad in res_US_a.surface_r_solns:
            temp_arr = np.zeros(len(time_array))
            for n in range(len(time_array)):
                temp_arr[n] = res_US_a.Temperature_Func(rad, time_array[n])
            T_a_surf_r.append(temp_arr + initial.T_init)
        imp_surf_matrices = []
        for r in range(len(res_US_i.surface_r_solns)):
            imp_surf_matrix = res_US_i.Create_Impulse_Matrix(res_US_i.surface_r_solns[r], time_array)*gdata.time_step
            sparse_csr_mat = sparse.csr_matrix(imp_surf_matrix)
            imp_surf_matrices.append(sparse_csr_mat)
        T_imp_surf_r = []
        for i in range(len(imp_surf_matrices)):
            T_imp_surf_r.append(imp_surf_matrices[i].dot(q_US)+initial.T_init)
        
        print("")
        print("Finalising surface calculations...")
        print("")
        T_imp_surf_t = []
        T_a_surf_t = []
        for time in res_US_i.surface_t_solns:
            temp_arr_i = np.zeros(len(res_US_i.surface_r_solns))
            temp_arr_a = np.zeros(len(res_US_i.surface_r_solns))
            if time == res_US_i.gdata.final_time:
                time_index = -1
            else:
                time_index = np.argwhere(time_array>=time)[0][0]
            for n in range(len(res_US_i.surface_r_solns)):
                temp_arr_i[n] = T_imp_surf_r[n][time_index]
                temp_arr_a[n] = res_US_a.Temperature_Func(res_US_i.surface_r_solns[n], time_array[time_index])
            T_imp_surf_t.append(temp_arr_i)
            T_a_surf_t.append(temp_arr_a+initial.T_init)
        
        print("")
        print("Plotting surfaces and other figures for Unit Step...")
        print("")
        #impulse method plot
        fig_surf_i = plt.figure()
        ax1 = fig_surf_i.gca(projection='3d')
        r_lines_leg = mlines.Line2D([], [],
                                 color = 'green',
                                 linestyle = '-',
                                 label = 'constant radius')
        t_lines_leg = mlines.Line2D([], [],
                             marker = 'o',
                             color = 'black',
                             linestyle = '--',
                             label = 'constant time')
        ax1.legend(handles = [r_lines_leg, t_lines_leg], loc = 2)
        for i in range(len(T_imp_surf_r)):
            ax1.plot(time_array,[res_US_i.surface_r_solns[i]]*len(time_array), T_imp_surf_r[i], 'g-')
        for i in range(len(T_imp_surf_t)):
            ax1.plot([res_US_i.surface_t_solns[i]]*len(res_US_i.surface_r_solns), res_US_i.surface_r_solns, T_imp_surf_t[i], 'ko--', markersize = 3.)
        ax1.set_xlabel('Time, s')
        ax1.set_ylabel('Radius, m')
        ax1.set_zlabel('Temperature, K')
        ax1.set_title('UNIT STEP response surface - Impulse method')
        ax1.set_xlim3d(0.0, res_US_i.gdata.final_time)
        ax1.set_ylim3d(0, res_US_i.geom.r_cylinder)
        
        #analytical surface plot
        fig_surf_a = plt.figure()
        ax1 = fig_surf_a.gca(projection='3d')
        r_lines_leg = mlines.Line2D([], [],
                                 color = 'green',
                                 linestyle = '-',
                                 label = 'constant radius')
        t_lines_leg = mlines.Line2D([], [],
                             marker = 'o',
                             color = 'black',
                             linestyle = '--',
                             label = 'constant time')
        ax1.legend(handles = [r_lines_leg, t_lines_leg], loc = 2)
        for i in range(len(T_a_surf_r)):
            ax1.plot(time_array,[res_US_a.surface_r_solns[i]]*len(time_array), T_a_surf_r[i], 'g-')
        for i in range(len(T_a_surf_t)):
            ax1.plot([res_US_a.surface_t_solns[i]]*len(res_US_a.surface_r_solns), res_US_a.surface_r_solns, T_a_surf_t[i], 'ko--', markersize = 3.)
        ax1.set_xlabel('Time, s')
        ax1.set_ylabel('Radius, m')
        ax1.set_zlabel('Temperature, K')
        ax1.set_title('UNIT STEP response surface - Analytical')
        ax1.set_xlim3d(0.0, res_US_a.gdata.final_time)
        ax1.set_ylim3d(0, res_US_a.geom.r_cylinder)
            
        #difference in 3D surfaces (plot)
        T_surf_t_diff = []
        for i in range(len(T_a_surf_t)):
            T_diff = T_a_surf_t[i] - T_imp_surf_t[i]
            T_surf_t_diff.append(T_diff)
        T_surf_r_diff = []
        for i in range(len(T_a_surf_r)):
            T_diff = T_a_surf_r[i] - T_imp_surf_r[i]
            T_surf_r_diff.append(T_diff)
        
        #difference in surfaces plot
        fig_diff_surf = plt.figure()
        ax1 = fig_diff_surf.gca(projection='3d')
        r_lines_leg = mlines.Line2D([], [],
                                 color = 'green',
                                 linestyle = '-',
                                 label = 'constant radius')
        t_lines_leg = mlines.Line2D([], [],
                             marker = 'o',
                             color = 'black',
                             linestyle = '--',
                             label = 'constant time')
        ax1.legend(handles = [r_lines_leg, t_lines_leg], loc = 2)
        for i in range(len(T_a_surf_r)):
            ax1.plot(time_array,[res_US_a.surface_r_solns[i]]*len(time_array), T_surf_r_diff[i], 'g-')
        for i in range(len(T_a_surf_t)):
            ax1.plot([res_US_a.surface_t_solns[i]]*len(res_US_a.surface_r_solns), res_US_a.surface_r_solns, T_surf_t_diff[i], 'ko--', markersize = 3.)
        ax1.set_xlabel('Time, s')
        ax1.set_ylabel('Radius, m')
        ax1.set_zlabel('Temperature, K')
        ax1.set_title('UNIT STEP response surfaces - Difference')
        ax1.set_xlim3d(0.0, res_US_a.gdata.final_time)
        ax1.set_ylim3d(0, res_US_a.geom.r_cylinder)
        
        #unit step comparison plots
        fig_qs, ax1 = plt.subplots()
        ax1.plot(time_array, q_US, linestyle = '-', color = 'black', label = 'Original q input')
        ax1.plot(time_array, q_impulse[-1], linestyle = '--', color = 'green', label = 'Calculated q')
        ax1.set_ylabel('Heat flux, W/m$^2$')
        ax1.set_xlabel('Time, s')
        ax1.set_title('Applied and calculated UNIT STEP heat flux - Comparsion')
        ax1.legend(loc=4)
        q_diff = q_US - q_impulse[-1]
        fig_diffq, ax1 = plt.subplots()
        ax1.plot(time_array, q_diff, linestyle = '-', color = 'green', label = 'Difference in q' )
        ax1.set_ylabel('Heat flux difference, W/m$^2$')
        ax1.set_xlabel('Time, s')
        ax1.set_title('Applied and calculated UNIT STEP heat flux - Difference')
        ax1.legend(loc=4)
    plt.show()
    return 0


shortOptions = ""
longOptions = ["help", "job=", "verbosity=", "out_file="]


def printUsage():
    """Print Usage Instructions."""
    print("")
    print("Usage: NISI_Shaft.py [--help] [--job=<jobFileName>] [--out_file=<outputFileName>] [--verbosity=0 (1 or 2)]")
    return


class MyError(Exception):
    """Raise Error Message."""

    def __init__(self, value):
        """Initialise class."""
        self.value = value

    def __str__(self):
        """Set string."""
        return repr(self.value)


if __name__ == "__main__":
    userOptions = getopt(sys.argv[1:], shortOptions, longOptions)
    uoDict = dict(userOptions[0])

    if len(userOptions[0]) == 0 or "--help" in uoDict:
        printUsage()
        sys.exit(1)

    # execute the code
    try:
        main(uoDict)
        print("\n \n")
        print("SUCCESS.")
        print("\n \n")

    except MyError as e:
        print("This run has gone bad.")
        print(e.value)
        sys.exit(1)
