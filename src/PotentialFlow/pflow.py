#! /usr/bin/env python3
""" 
Script to create Potential Flow Flow-Fields

Author: Ingo Jahn
Last modified: 11/08/2015 
"""

import numpy as np
import matplotlib.pyplot as plt
import sys as sys
import os as os
from getopt import getopt
import datetime as datetime
import shutil as shutil
#import pdb


shortOptions = ""
longOptions = ["help", "job=", "noprint", "noplot", "out-file=", "version"]
###
###
def printUsage():
    print("")
    print("Usage: pflow.py [--help] [--job=<jobFileName>] [--noprint] [--out-file=<fileName>] [--version]")
    print("\n")
    print(" --help      Display help")
    print("\n")
    print(" --job=      Use this to specify the job file.")
    print("\n")
    print(" --noprint   This suppresses on screen outputs.")
    print("\n")
    print(" --noplot    This suppresses on graph outputs.")
    print("\n")
    print(" --out-file= Use this to specify a file for the output data.")
    print("\n")
    print(" --version   Shows current version of the code.")
    return 0
###
###
class PotentialFlow:
    """
    class for PotentiaFlow-fields
    """
    def __init__(self):
        self.size()
    ##
    def size(self, xmin=0.0, xmax=1.0, ymin=0.0, ymax = 1.0):
        self.x0 = xmin
        self.x1 = xmax
        self.y0 = ymin
        self.y1 = ymax
    ##
    def calc(self,A,nx=100,ny=100):
        self.A = A
        # create mesh
        xx = self.x0 + np.arange(nx) * (self.x1-self.x0) / float(nx-1)
        yy = self.y0 + np.arange(ny) * (self.y1-self.y0) / float(ny-1)
        self.X,self.Y = np.meshgrid(xx,yy)
        self.PSI = np.zeros([nx,ny])
        self.UU = np.zeros([nx,ny])
        self.VV = np.zeros([nx,ny])      
        # calculate stream functions and velocities
        for i in range(nx):
            x = xx[i]
            for j in range(ny):
                y = yy[j]
                psi = 0.
                U = 0.
                V = 0
                for it in range(len(A)):
                    psi = psi + A[it].evalP(x,y)
                    u,v = A[it].eval(x,y)
                    U = U + u
                    V = V + v
                self.PSI[j,i] = psi
                self.UU[j,i] = U
                self.VV[j,i] = V
    def evalPsi(self,x,y):
	# calculate Psi at a point
        PSI = 0.
        for it in range(len(self.A)):
            PSI = PSI + self.A[it].evalP(x,y)
        return PSI
    ##
    def evalUV(self,x,y):
        # calculate U and V at a point
        U = 0.
        V = 0
        for it in range(len(self.A)):
            u,v = self.A[it].eval(x,y)
            U = U + u
            V = V + v
        return U, V
    ##
    def evalPressure(self,x,y,rho):
        # calculate pressure reduction
        u,v = self.eval(x,y)
        Umag2 = u**2 + v**2
        dP = - 0.5 * rho * Umag2
        return dP
    ##
    def LinevalU(self,x0,y0,x1,y1,n=100,plot_flag=0):
        # calculate u-velocity at N points linearly spaced between point 0 and 1
        xx = x0 + np.arange(n) * (x1-x0) / float(n-1)
        yy = y0 + np.arange(n) * (y1-y0) / float(n-1)
        UU = np.zeros(n)
        for i in range(n):
            u,v = self.eval(xx[i],yy[i])
            UU[i] = u
        if plot_flag == 1:
            plt.figure()
            plt.plot(UU)
            plt.title('U-velocity')
            plt.xlabel('Position along line')
            plt.ylabel('U-velocity')
        return UU
    ##
    def LinevalV(self,x0,y0,x1,y1,n=100,plot_flag=0):
        # calculate u-velocity at N points linearly spaced between point 0 and 1
        xx = x0 + np.arange(n) * (x1-x0) / float(n-1)
        yy = y0 + np.arange(n) * (y1-y0) / float(n-1)
        VV = np.zeros(n)
        for i in range(n):
            u,v = self.eval(xx[i],yy[i])
            VV[i] = v
        if plot_flag == 1:
            plt.figure()
            plt.plot(VV)
            plt.title('V-velocity')
            plt.xlabel('Position along line')
            plt.ylabel('V-velocity')
        return VV			
    ##
    def LinevalPressure(self,x0,y0,x1,y1,rho,n=100,plot_flag=0):
        # calculate u-velocity at N points linearly spaced between point 0 and 1
        xx = x0 + np.arange(n) * (x1-x0) / float(n-1)
        yy = y0 + np.arange(n) * (y1-y0) / float(n-1)
        PP = np.zeros(n)
        for i in range(n):
            u,v = self.eval(xx[i],yy[i])
            PP[i] = - 0.5 * rho * (v**2 + u**2)
        if plot_flag == 1:
            plt.figure()
            plt.plot(PP)
            plt.title('Pressure')
            plt.xlabel('Position along line')
            plt.ylabel('Pressure')
        return PP		
###
###
class Model():
    def __init__(self):
        self.name = ""
        self.dimensions = 2
        self.Pinf = 0.      # reference pressure P-infinity [Pa]
        self.rho = 1.225    # density [kg/m3]
        self.Uinf = np.nan   # free stream velocity [m/s]
    ###
    def check(self):
        """
        function to check that minimum number of model inputs have been defined.
        """
        return 0
    def set_Uinf(self,blocklist):
        if np.isnan(self.Uinf):
            # set Uinf based on Uniform Flows
            U = 0.; V=0.
            for b in blocklist:
                if b.__class__.__name__ is 'UniformFlow':
                    U = U+b.u
                    V = V+b.v
            self.Uinf = np.sqrt(U**2 + V**2)
            print('Calculated U-inf = {: 10.2f} (m/s)'.format(self.Uinf))
        else:
            print('Using User-defined U-inf = {: 10.2f} (m/s)'.format(self.Uinf))
        return 0 
###
###
class Visual():
    def __init__(self):
        self.xmin = -1.
        self.xmax = 1.
        self.ymin = -1.
        self.ymax = 1.
        self.Nx = 50
        self.Ny = 50 
        self.subplot = 0
    ###
    def check(self):
        """
        function to check that minimum number of model inputs have been defined
        do some house keeping to ensure integers are defined were required
        """
        self.Nx = int(self.Nx)
        self.Ny = int(self.Ny)
        self.subplot = int(self.subplot)
        if self.xmin >= self.xmax:
            print("xmin must be less then xmax --> aborting")
            return 1
        if self.ymin >= self.ymax:
            print("ymin must be less then ymax --> aborting")
            return 1
        if self.Nx <= 1:
            print("Nx must be greater than 1")
            return 1
        if self.Ny <= 1:
            print("Ny must be greater than 1")
            return 1
        if not (self.subplot == 1 or self.subplot ==0):
            print("suplot must be [0] or [1]")
        return 0
    ###
    def show(self):
        print("No visualisation routines defined yet")
        return 0
###
###
class PLOT():
    def __init__(self,mdata):
        self.rho = mdata.rho    # density from [kg/m3]
        self.Uinf = mdata.Uinf  # free stream velocity [m/s]
        self.Pinf = mdata.Pinf  # free steram pressure [Pa]
        self.N = 0
        self.vectors_v=[0]
        self.psi_v=[0]
        self.psi_magU_v=[0]
        self.magU_v=[0]
        self.vectors_magU_v=[0]
        self.U_v=[0]
        self.V_v=[0]
        self.P_v=[0]
        self.Cp_v=[0]
    ###
    def update_Uinf(self,mdata):
        self.rho = mdata.rho    # density from [kg/m3]
        self.Uinf = mdata.Uinf  # free stream velocity [m/s]
        self.Pinf = mdata.Pinf  # free steram pressure [Pa]
    ###
    def vectors(self):
        """
        plots nice looking streamlines. Note these are not euqipotentials of psi.
        """
        self.N = self.N+1
        self.vectors_v = [1]
    ###
    def plot_vectors(self,T):
        plt.streamplot(T.X,T.Y,T.UU,T.VV, density = 2, linewidth = 1, arrowsize=2, arrowstyle='->')
        plt.title('Velocity Vectors')
        plt.xlabel('X-Position')
        plt.ylabel('Y-Position')
        plt.gca().set_aspect('equal')
        plt.gca().set_xlim([self.xmin,self.xmax])
        plt.gca().set_ylim([self.ymin,self.ymax])
    ###
    def psi(self, levels=20):
        """
        plots 'real' streamlines, contours of psi. Use levels to set number of contours.
        """
        self.N = self.N+1
        self.psi_v = [1, levels]
    ###
    def plot_psi(self,T):
        CS=plt.contour(T.X, T.Y,T.PSI,self.psi_v[1],cmap='hsv')
        plt.clabel(CS, inline=1, fontsize=10) 
        plt.title('Streamfunction (PSI)')  
        plt.xlabel('X-Position')
        plt.ylabel('Y-Position')
        plt.gca().set_aspect('equal')
        plt.gca().set_xlim([self.xmin,self.xmax])
        plt.gca().set_ylim([self.ymin,self.ymax])

        #psi_norm = np.mod(T.PSI,1.)
        #CS = plt.contour(T.X, T.Y,psi_norm,20,cmap='hsv')
        #plt.clabel(CS, inline=1, fontsize=10)
        #plt.colorbar(CS)
        #A = np.linspace(-0.5,0.5,51)
        #A = A*50/51
        #A = np.sin(A*np.pi) 
        #print A
        #CS = plt.contour(T.X, T.Y, 0.5*np.sin(T.PSI*2.*np.pi),0.5*np.array([-np.sin(np.pi*2/5),-np.sin(np.pi*1/5), 0., np.sin(np.pi*1/5), np.sin(np.pi*2/5)]))
        #CS = plt.contour(T.X, T.Y, 0.5*np.sin(T.PSI*2.*np.pi),0.5*A)
        #plt.clabel(CS, inline=1, fontsize=10)
        #self.plot_contour(np.cos(T.PSI),T.X,T.Y,self.psi_v[1],"Streamfunction (PSI) Contours")
        #CS2 = plt.contourf(T.X, T.Y, 0.5*np.sin(T.PSI*2.*np.pi),cmap='hsv',vmin=-0.5, vmax=0.5)
        #plt.colorbar(CS2,vmin=-0.5, vmax=0.5)
        #self.plot_contourf(T.PSI,T.X,T.Y,self.psi_v[1],"Streamfunction (PSI) Contours",cmap='flag')
        #self.plot_contourf(T.PSI,T.X,T.Y,self.psi_v[1],"Streamfunction (PSI) Contours",cmap='hsv')
        #CS = plt.contourf(T.X,T.Y,T.PSI,self.psi_v[1]*10,cmap="prism_r")
    ###
    def psi_magU(self, min=[], max=[], levels=20):
        """
        plots 'real' streamlines, contours of psi. Use levels to set number of contours.
        """
        self.N = self.N+1
        self.psi_magU_v = [1, min, max, levels]
    ###
    def plot_psi_magU(self,T):
        plt.contour(T.X,T.Y,T.PSI,self.psi_magU_v[3],colors='k')
        magU = (T.VV**2 + T.UU**2)**0.5
        if self.psi_magU_v[1]:
            magU[magU<self.psi_magU_v[1]] = self.psi_magU_v[1]
        if self.psi_magU_v[2]:
            magU[magU>self.psi_magU_v[2]] = self.psi_magU_v[2]
        CS = plt.contourf(T.X,T.Y,magU,self.psi_magU_v[3]) 
        plt.colorbar(CS)
        plt.title('Stream Functions (PSI) + Velocity Magnitude') 
        plt.xlabel('X-Position')
        plt.ylabel('Y-Position')
        plt.gca().set_aspect('equal')
        plt.gca().set_xlim([self.xmin,self.xmax])
        plt.gca().set_ylim([self.ymin,self.ymax])    
    ###
    def magU(self, min=[], max=[], levels=20):
        """
        create contour plot of velocity magnitude. Use min and max to specify range and levels sets numbers of contours.
        """
        self.N = self.N+1
        self.magU_v=[1, min, max, levels]
    ###
    def plot_magU(self,T):
        magU = (T.VV**2 + T.UU**2)**0.5
        if self.magU_v[1]:
            magU[magU<self.magU_v[1]] = self.magU_v[1]
        if self.magU_v[2]:
            magU[magU>self.magU_v[2]] = self.magU_v[2]
        CS = plt.contourf(T.X,T.Y,magU,self.magU_v[3]) 
        plt.colorbar(CS)
        plt.title('Velocity Magnitude') 
        plt.xlabel('X-Position')
        plt.ylabel('Y-Position')
        plt.gca().set_aspect('equal')
        plt.gca().set_xlim([self.xmin,self.xmax])
        plt.gca().set_ylim([self.ymin,self.ymax])         
    ###
    def vectors_magU(self, min=[], max=[], levels=20):
        """
        create contour plot of velocity magnitude. Use min and max to specify range and levels sets numbers of contours.
        """
        self.N = self.N+1
        self.vectors_magU_v=[1, min, max, levels]
    ###
    def plot_vectors_magU(self,T):
        plt.streamplot(T.X,T.Y,T.UU,T.VV, density = 2, linewidth = 1, arrowsize=2, arrowstyle='->')
        plt.xlabel('X-Position')
        plt.ylabel('Y-Position')
        plt.gca().set_aspect('equal')
        plt.gca().set_xlim([self.xmin,self.xmax])
        plt.gca().set_ylim([self.ymin,self.ymax])
        magU = (T.VV**2 + T.UU**2)**0.5
        if self.vectors_magU_v[1]:
            magU[magU<self.vectors_magU_v[1]] = self.vectors_magU_v[1]
        if self.vectors_magU_v[2]:
            magU[magU>self.vectors_magU_v[2]] = self.vectors_magU_v[2]
        CS = plt.contourf(T.X,T.Y,magU,self.vectors_magU_v[3]) 
        plt.colorbar(CS)
        plt.title('Vectors + Velocity Magnitude') 
        plt.xlabel('X-Position')
        plt.ylabel('Y-Position')
        plt.gca().set_aspect('equal')
        plt.gca().set_xlim([self.xmin,self.xmax])
        plt.gca().set_ylim([self.ymin,self.ymax])
    ###
    def U(self,min=[], max=[], levels=20):
        """
        create contour plot of U velocity. Use min and max to specify range and levels sets numbers of contours.
        """
        self.N = self.N+1
        self.U_v = [1, min, max, levels]
    ###
    def plot_U(self,T):
        U = T.UU
        if self.U_v[1]:
            U[U<self.U_v[1]] = self.U_v[1]
        if self.U_v[2]:
            U[U>self.U_v[2]] = self.U_v[2]
        self.plot_contourf(U,T.X,T.Y,levels=self.U_v[3],label="U-Velocity")  
    ###
    def V(self,min=[], max=[], levels=20):
        """
        create contour plot of V velocity. Use min and max to specify range and levels sets numbers of contours.
        """
        self.N = self.N+1
        self.V_v = [1, min, max, levels]
    ###
    def plot_V(self,T):
        V = T.VV
        if self.V_v[1]:
            V[V<self.V_v[1]] = self.V_v[1]
        if self.V_v[2]:
            V[V>self.V_v[2]] = self.V_v[2]
        self.plot_contourf(V,T.X,T.Y,levels=self.V_v[3],label="V-Velocity") 
    ###
    def P(self,min=[], max=[], levels=20):
        """
        create contour plot of pressure, using P_inf and rho to perform the calculation. 
        P = P_inf - 1/2 * rho * magU**2. Use min and max to specify range and levels sets numbers of contours.
        """
        self.N = self.N+1
        self.P_v = [1, min, max, levels]
    ###
    def plot_P(self,T):
        P = self.Pinf - 1./2. * self.rho * (T.UU**2 + T.VV**2)
        if self.P_v[1]:
            P[P<self.P_v[1]] = self.P_v[1]
        if self.P_v[2]:
            P[P>self.P_v[2]] = self.P_v[2]
        self.plot_contourf(P,T.X,T.Y,levels=self.P_v[3],label="Pressure (rho="+str(self.rho)+"kg/s, P_inf="+str(self.Pinf)+"Pa)") 
    ###
    def Cp(self,U_inf=0., rho=1.225, min=[], max=[], levels = 20):
        """
        create contorus of pressure coefficient Cp, using U_inf and rho to perform the calculation.
        Cp = P / ( 1/2 * rho * U_inf**2 )
        """
        self.N = self.N+1
        self.Cp_v = [1, min, max, levels]
    ###
    def plot_Cp(self,T):
        Uinf = float(self.Uinf)
        if Uinf == 0. or np.isnan(Uinf):
            print("Cannot create Cp graph for U_inf = 0. Division by zero creates inf.")
            print("Skipping Cp graph.")
        else:
            Cp = (0.5* self.rho * self.Uinf**2 - 0.5* self.rho * (T.VV**2 + T.UU**2)) / (0.5 * self.rho * self.Uinf**2)
            if self.Cp_v[1]:
                Cp[Cp<self.Cp_v[1]] = self.Cp_v[1]
            if self.Cp_v[2]:
                Cp[Cp>self.Cp_v[2]] = self.Cp_v[2]  
            self.plot_contourf(Cp,T.X,T.Y,levels=self.Cp_v[3],label="Cp (rho="+str(self.rho)+"kg/s, U_inf="+str(self.Uinf)+"m/s)")     
    ### 
    def plot_contourf(self,Z,X,Y,levels,label="",cmap=[]):
        if cmap:
            CS = plt.contourf(X, Y, Z,levels,cmap=cmap)
        else:
            CS = plt.contourf(X, Y, Z,levels)
        # set graph details       
        plt.colorbar(CS)
        plt.title(label)
        plt.xlabel('X-Position')
        plt.ylabel('Y-Position')
        plt.legend
        plt.gca().set_aspect('equal')
        plt.gca().set_xlim([self.xmin,self.xmax])
        plt.gca().set_ylim([self.ymin,self.ymax])
    ###
    def plot(self,T,visual):
        """
        actually plot the graphs
        """
        self.xmin = visual.xmin; self.ymin = visual.ymin
        self.xmax = visual.xmax; self.ymax = visual.ymax
        if visual.subplot == 0:
            if self.vectors_v[0]:
                plt.figure()
                self.plot_vectors(T)
                plt.axis('equal')
            if self.psi_v[0]:
                plt.figure()
                self.plot_psi(T) 
                plt.axis('equal')
            if self.magU_v[0]:
                plt.figure()
                self.plot_magU(T)
                plt.axis('equal')
            if self.vectors_magU_v[0]:
                plt.figure()
                self.plot_vectors_magU(T)
                plt.axis('equal')
            if self.psi_magU_v[0]:
                plt.figure()
                self.plot_psi_magU(T)
                plt.axis('equal')
            if self.U_v[0]:
                plt.figure()
                self.plot_U(T)
                plt.axis('equal')
            if self.V_v[0]:
                plt.figure()
                self.plot_V(T)
                plt.axis('equal')
            if self.P_v[0]:
                plt.figure()
                self.plot_P(T)
                plt.axis('equal')
            if self.Cp_v[0]:
                plt.figure()
                self.plot_Cp(T)
                plt.axis('equal')
        else:
            if self.N == 1:
                si=1; sj=1
            elif self.N == 2:
                si=2; sj=1
            elif self.N <= 3:
                si=3; sj=1
            elif self.N <= 4:
                si=2; sj=2
            elif self.N <= 6:
                si=3; sj=2
            elif self.N <= 9:
                si=3; sj=3
            else:
                print("Only supporting up to 9 subplots. Skipping plots")
            n = 1
            if self.vectors_v[0]:
                plt.subplot(sj,si,n); n=n+1
                self.plot_vectors(T)
                plt.axis('equal')
            if self.psi_v[0]:
                plt.subplot(sj,si,n); n=n+1
                self.plot_psi(T) 
                plt.axis('equal')
            if self.magU_v[0]:
                plt.subplot(sj,si,n); n=n+1
                self.plot_magU(T)
                plt.axis('equal')
            if self.vectors_magU_v[0]:
                plt.subplot(sj,si,n); n=n+1
                self.plot_vectors_magU(T)
                plt.axis('equal')
            if self.psi_magU_v[0]:
                plt.subplot(sj,si,n); n=n+1
                self.plot_psi_magU(T)
                plt.axis('equal')
            if self.U_v[0]:
                plt.subplot(sj,si,n); n=n+1
                self.plot_U(T)
                plt.axis('equal')
            if self.V_v[0]:
                plt.subplot(sj,si,n); n=n+1
                self.plot_V(T)
                plt.axis('equal')
            if self.P_v[0]:
                plt.subplot(sj,si,n); n=n+1
                self.plot_P(T)
                plt.axis('equal')
            if self.Cp_v[0]:
                plt.subplot(sj,si,n); n=n+1
                self.plot_Cp(T)  
                plt.axis('equal')          
            return 0
###
###
class SCREEN:
    def __init__(self,mdata):
        self.rho = mdata.rho    # density from [kg/m3]
        self.Uinf = mdata.Uinf  # free stream velocity [m/s]
        self.Pinf = mdata.Pinf  # free steram pressure [Pa]
        self.var_options = ['Psi','magU','U','V','P','Cp']
        self.var = []
        self.points = []
        self.lines = []
        self.variables() # initialise variables
    ###
    def update_Uinf(self,mdata):
        self.rho = mdata.rho    # density from [kg/m3]
        self.Uinf = mdata.Uinf  # free stream velocity [m/s]
        self.Pinf = mdata.Pinf  # free steram pressure [Pa]
    ###
    def locations(self,loc):
        self.points.append(loc)
        return 0
    ###
    def variables(self,var=['Psi','magU','P']):
        self.var = var
        return 0
    ###
    def check_variables(self):
        var = self.var
        self.var=[]
        for a in var:
            if a in self.var_options:
                self.var.append(a)
            else:
                print("Variable {0} not recognised. Select from the following: [{1}]".format(a,self.var_options))
                print("Skipping: {0}".format(a))
        return 0
    ###
    def Lineval(self,loc0,loc1,N=5):
        self.lines.append([loc0,loc1,N])
        return 0        
    ###
    def display(self,T):
        self.check_variables()
        if len(self.points) > 0 or len(self.lines) > 0:
            print("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
            print("Output Data:")
            print("Density = {0} kg/s".format(self.rho))
            print("Uinf = {0} m/s".format(self.Uinf))
            print("Pinf = {0} Pa".format(self.Pinf))
            print("\n")
            if len(self.points) > 0:
                print("Points:")
                string = "".join('{:10s}'.format(k) for k in ['X-loc']+['Y-loc']+self.var)
                print("     "+string)
                for loc_list in self.points:
                    for loc in loc_list:
                        data=[]
                        x = loc[0];y=loc[1]
                        U,V = T.evalUV(x,y)
                        magU = np.sqrt(U**2+V**2)
                        for v in self.var:
                            if v == 'Psi':
                                data.append(T.evalPsi(x,y))
                            if v == 'magU':
                                data.append(magU)
                            if v == 'P':
                                data.append(self.Pinf - 0.5*self.rho*magU**2)
                            if v == 'U':
                                data.append(U)
                            if v == 'V':
                                data.append(V)
                            if v == 'Cp':
                                data.append(1. - magU**2 / self.Uinf**2)  
                        string = "".join('{: 10.3f}'.format(k) for k in ([x] + [y] + data))                      
                        print(string)
                    print("\n")
            ###
            if len(self.lines) > 0:
                print("Lines:")
                string = "".join('{:10s}'.format(k) for k in ['X-loc']+['Y-loc']+self.var)
                print("     "+string)
                for ll in self.lines:
                    # create lists of X/Y
                    X = np.linspace(ll[0][0],ll[1][0],num=ll[2])
                    Y = np.linspace(ll[0][1],ll[1][1],num=ll[2])
                    for i in range(ll[2]):
                        data=[]
                        x=X[i];y=Y[i]
                        U,V = T.evalUV(x,y)
                        magU = np.sqrt(U**2+V**2)
                        for v in self.var:
                            if v == 'Psi':
                                data.append(T.evalPsi(x,y))
                            if v == 'magU':
                                data.append(magU)
                            if v == 'P':
                                data.append(self.Pinf - 0.5*self.rho*magU**2)
                            if v == 'U':
                                data.append(U)
                            if v == 'V':
                                data.append(V)
                            if v == 'Cp':
                                data.append(1. - magU**2 / self.Uinf**2) 
                        string = "".join('{: 10.3f}'.format(k) for k in ([x] + [y] + data))
                        print(string)
                    print("\n")
        return 0
    ##
    def write(self,T,fp):
        self.check_variables()
        if len(self.points) > 0 or len(self.lines) > 0:
            fp.write("Output Data: \n")
            fp.write("Density = {0} kg/s \n".format(self.rho))
            fp.write("Uinf = {0} m/s \n".format(self.Uinf))
            fp.write("Pinf = {0} Pa \n".format(self.Pinf))
            fp.write("\n")
            if len(self.points) > 0:
                fp.write("Points:")
                string = "".join('{:10s}'.format(k) for k in ['X-loc']+['Y-loc']+self.var)
                fp.write("     "+string+" \n")
                for loc_list in self.points:
                    for loc in loc_list:
                        data=[]
                        x = loc[0];y=loc[1]
                        U,V = T.evalUV(x,y)
                        magU = np.sqrt(U**2+V**2)
                        for v in self.var:
                            if v == 'Psi':
                                data.append(T.evalPsi(x,y))
                            if v == 'magU':
                                data.append(magU)
                            if v == 'P':
                                data.append(self.Pinf - 0.5*self.rho*magU**2)
                            if v == 'U':
                                data.append(U)
                            if v == 'V':
                                data.append(V)
                            if v == 'Cp':
                                data.append(1. - magU**2 / self.Uinf**2)  
                        string = "".join('{: 10.6f}'.format(k) for k in ([x] + [y] + data))                      
                        fp.write(string+" \n")
                    fp.write("\n")
            ###
            if len(self.lines) > 0:
                fp.write("Lines: \n")
                string = "".join('{:10s}'.format(k) for k in ['X-loc']+['Y-loc']+self.var)
                fp.write("     "+string+" \n")
                for ll in self.lines:
                    # create lists of X/Y
                    X = np.linspace(ll[0][0],ll[1][0],num=ll[2])
                    Y = np.linspace(ll[0][1],ll[1][1],num=ll[2])
                    for i in range(ll[2]):
                        data=[]
                        x=X[i];y=Y[i]
                        U,V = T.evalUV(x,y)
                        magU = np.sqrt(U**2+V**2)
                        for v in self.var:
                            if v == 'Psi':
                                data.append(T.evalPsi(x,y))
                            if v == 'magU':
                                data.append(magU)
                            if v == 'P':
                                data.append(self.Pinf - 0.5*self.rho*magU**2)
                            if v == 'U':
                                data.append(U)
                            if v == 'V':
                                data.append(V)
                            if v == 'Cp':
                                data.append(1. - magU**2 / self.Uinf**2) 
                        string = "".join('{: 10.6f}'.format(k) for k in ([x] + [y] + data))
                        fp.write(string+" \n")
                    fp.write("\n")
        return 0
    ##
    def evalPsi(self,x,y):
	# calculate Psi at a point
        PSI = 0.
        for it in range(len(self.A)):
            PSI = PSI + self.A[it].evalP(x,y)
        return PSI
    ##
    def evalUV(self,x,y):
        # calculate U and V at a point
        U = 0.
        V = 0
        for it in range(len(self.A)):
            u,v = self.A[it].eval(x,y)
            U = U + u
            V = V + v
        return U, V
    ##
    def evalPressure(self,x,y,rho):
        # calculate pressure reduction
        u,v = self.eval(x,y)
        Umag2 = u**2 + v**2
        dP = - 0.5 * rho * Umag2
        return dP
###
###
## Definition of classes used as Building Blocks 
class UniformFlow:
    """
    class that creates a uniform flow field for potential flow
    UniformFlow(u,v)
    u  -  x-component of velocity
    v  -  y-component of velocity
    label - optional label
    """
    instances = []
    def __init__(self,u,v,label=''):
        self.__class__.instances.append(self) # add instance to list 
        self.type = 'UniformFlow'
        self.u = u
        self.v = v
        self.label = label
    ##
    def evalP(self,x,y):
        P = self.u*y - self.v*x
        return P        
    ##
    def eval(self,x,y):
        u = self.u
        v = self.v
        return u,v        
###
###
class Source:
    """
    class that creates a source for potential flow.
    Source(x0,y0,m)
    x0  -  x-position of Source
    y0  -  y-position of Source
    m   -  total flux generated by source (for sink set -ve)
    label - optional label
    """
    instances = []
    def __init__(self,x0,y0,m,label=''):
        self.__class__.instances.append(self) # add instance to list 
        self.type = 'Source'
        self.x0 = x0
        self.y0 = y0
        self.m = m
        self.label = label
    ##
    def evalP(self,x,y):
        theta = np.arctan2(y-self.y0,x-self.x0)
        PSI = theta * self.m /(2*np.pi)
        return PSI    
    ##
    def eval(self,x,y):
        r = ( (x-self.x0)**2 + (y-self.y0)**2 )**0.5
        if r == 0.:
            u=2*self.m; v=2*self.m
        else:
            u = self.m / (2*np.pi) * (x - self.x0) / (r**2)
            v = self.m / (2*np.pi) * (y - self.y0) / (r**2)
        return u,v  
###
###
class Vortex:
    """
    class that creates an irrotational vortex for potential flow.
    Vortex(x0,y0,K)
    x0  -  x-position of Vortex core
    y0  -  y-position of Vortex core
    K   -  Strength of Vortex K = Gamma / (2 * pi)
    Gamma -  Strength of Vortex
    label - optional label
    """
    instances = []
    def __init__(self,x0,y0,K=None,Gamma=None,label=''):
        self.__class__.instances.append(self) # add instance to list 
        self.type = 'Vortex'
        self.x0 = x0
        self.y0 = y0
        if Gamma == None and K == None:
            raise MyError("Error in definition of Vortex, either K or Gamma have to be defined.")
        if (Gamma != None and K != None):
            raise MyError("Error in definition of Vortex, both K and Gamma have been defined. Set only one.")
        if Gamma == None:
            self.Gamma = 2. * np.pi * K
        else:
            self.Gamma = Gamma 
        self.label = label
    ##
    def evalP(self,x,y):
        r = ( (x-self.x0)**2 + (y-self.y0)**2 )**0.5
        if r == 0.:
            P = float('Inf')
        else:
            P = self.Gamma / (2*np.pi) * np.log(r)
        return P  
    ##
    def eval(self,x,y):
        r = ( (x-self.x0)**2 + (y-self.y0)**2 )**0.5
        if r == 0.:
            u=float('Inf'); v=float('Inf')
        else:
            u = self.Gamma / (2*np.pi) * (y - self.y0) / (r**2)
            v = - self.Gamma / (2*np.pi) * (x - self.x0) / (r**2)
        return u,v  
###
###
class Doublet:
    """
    class that creates a doublet. 
    If combined with uniform flow of veloicty U_inf in the +x direction, this creates the flow around a cylidner.
    Doublet(x0,y0,a,U_inf)
    x0  -  x-position of Vortex core
    y0  -  y-position of Vortex core
    a   -  radius of cylinder generated if superimposed to Uniform Flow
    U-inf  -  Strength of uniform flow 
    label - optional label
    """
    instances = []		
    def __init__(self,x0,y0,a,U_inf,label=''):
        self.__class__.instances.append(self) # add instance to list 
        self.type = 'Doublet'
        self.x0 = x0
        self.y0 = y0
        self.a = a
        self.U_inf = U_inf
        self.label = label
    def evalP(self,x,y):
        if ((y-self.y0)**2 + (x-self.x0)**2) == 0.:
            P = float('Inf')
        else:
            P = self.U_inf * (y-self.y0) * ( - self.a**2 / ((y-self.y0)**2 + (x-self.x0)**2) ) 
        # set to zero inside circle
        #if ((y-self.y0)**2 + (x-self.x0)**2) < self.a**2:
        #    P = np.nan
        return P
    ##
    def eval(self,x,y):
        if ((y-self.y0)**2 + (x-self.x0)**2) == 0.:        
            u=float('Inf');v=float('Inf')
        else:
            u = self.U_inf * self.a**2 * - ((x-self.x0)**2 - (y-self.y0)**2) / ( ((x-self.x0)**2 + (y-self.y0)**2)**2 ) 
            v = self.U_inf * self.a**2 * -2. * (x-self.x0) * (y-self.y0) / ( ((x-self.x0)**2 + (y-self.y0)**2)**2 )
        # set to zero inside circle
        #if ((y-self.y0)**2 + (x-self.x0)**2) < self.a**2:
        #    u = np.nan
        #    v = np.nan
        return u,v
###
###	
class User_Defined:
    """
    Special Userdefined building block (flow through 90 degree corner)
    User_Defined(x0,y0,A)
    x0  -  x-position of corner
    y0  -  y-position of corner
    A   -  Strength of Flow 
    label - optional label
    """
    instances = []
    def __init__(self,x0,y0,A,label=''):
        self.__class__.instances.append(self) # add instance to list 
        self.type = 'User_Defined'
        self.x0 = x0
        self.y0 = y0
        self.A = A
        self.label = label
    ##
    def evalP(self,x,y):
        P = self.A * (x-self.x0) * (y-self.y0)
        return P 
    ##
    def eval(self,x,y):
        u = self.A * (x-self.x0) 
        v = - self.A * (y-self.y0)
        return u,v 
###
###
class Name:
    """
    Template for user generated building blocks
    Name(x0,y0,Var1,Var2,Var3)
    x0  -  x-position
    y0  -  y-position
    Var1  -  Variable1
    Var2  -  Variable2
    Var3  -  Variable3
    label - optional label

    When creating a new class, you need to also add this to the BLOCK_list.
    E.g. If you want to add a new class with name Name1, find where the BLOCK_list is created. 
    this should look like
    BLOCK_list = UniformFlow.instances + Vortex.instances + Source.instances + Doublet.instances + User_Defined.instances 
    to 
    BLOCK_list = UniformFlow.instances + Vortex.instances + Source.instances + Doublet.instances + User_Defined.instances + Name1.instances

    """
    instances = []
    def __init__(self,x0,y0,Var1,Var2,Var3,label=''):
        self.__class__.instances.append(self) # add instance to list 
        self.type = 'Name'
        self.x0 = x0
        self.y0 = y0
        self.Var1 = Var3
        self.Var2 = Var2
        self.Var3 = Var1 
        self.label = label
    ##
    def evalP(self,x,y):
        ## Function for streamfunction goes here
        P = 0
        return P 
    ##
    def eval(self,x,y):
        ## Functions for u and v velocity go here. I.e. differentiate stream function with respect to x and y and then provide the functions.
        u = 0 
        v = 0
        return u,v 	
###
###
def main(uoDict):
    """
    Main function that is executed when running the Potential Flow solver.
    """
    # create string to collect warning messages
    warn_str = "\n"

    # main file to be executed
    jobFileName = uoDict.get("--job", "test")

    # strip .py extension form jobName
    jobName = jobFileName.split('.')
    jobName = jobName[0]

    # create classes to store input data
    mdata = Model()
    visual = Visual()
    plot = PLOT(mdata)
    screen = SCREEN(mdata)

    # set print_flag (can be overwritten from jobfile)
    mdata.vrbLvl = 1
    if "--noprint" in uoDict:
        mdata.vrbLvl = 0
    
    # set plotflag
    mdata.plotflag = 1
    if "--noplot" in uoDict:
        mdata.plotflag = 0

    if mdata.vrbLvl >= 1:
        print("Loading jobfile= {0}".format(jobFileName))
    # Execute jobFile, this creates all the variables
    exec(open(jobFileName).read(),globals(),locals())
    if mdata.vrbLvl >= 1:
        print("    DONE: Loading jobfile.")
    mdata.jobFileName = jobFileName

    if not mdata.check():
        MyError("Failed check for pdata. Check that sufficient model parameters have been set")
    if not visual.check():
        MyError("Failed check for visual. Check that sufficient visualisation parameters have been set")

    # check that more than one building block has been defined. 
    BLOCK_list = UniformFlow.instances + Vortex.instances + Source.instances + Doublet.instances + User_Defined.instances


    if len(BLOCK_list) == 0:
        MyError("No building blocks have been defined. Aborting.")

    # calculate Uinf based on define flows.
    mdata.set_Uinf(BLOCK_list)
    plot.update_Uinf(mdata)
    screen.update_Uinf(mdata)

    if mdata.vrbLvl >= 1:
        print("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
        print("Constructing a Potential Flow Solution from:")
        print("  type      Label")
        for a in BLOCK_list:
            print("+ {0}, {1}".format(a.type, a.label))
        print("\n")    
    
    # set up and solve potential flow field
    T = PotentialFlow()
    T.size(visual.xmin, visual.xmax, visual.ymin, visual.ymax)
    T.calc(BLOCK_list, visual.Nx, visual.Ny)

    # display the output data (plots)
    plot.plot(T, visual)

    # display the on screen data (point and line data)
    screen.display(T)

    # write data to file, uses same as on-screen data
    if "--out-file" in uoDict:
        if mdata.vrbLvl >= 1:
            print("Writing out-file.")

        outFileName = uoDict.get("--out-file", "test")
        if os.path.isfile(outFileName):
            print("Output file already exists.")
            fntemp = outFileName+".old"
            shutil.copyfile(outFileName, fntemp)
            print("Existing file has been copied to:"+fntemp)

        fp = open(outFileName,'w') 
        fp.write("Output file for pflow.py \n")
        fp.write("\n++++++++++++++++++++++++++++++\n")
        fp.write("Date / Time = "+str(datetime.datetime.now())+"\n")
        fp.write("Jobfile = "+mdata.jobFileName+"\n")        
        fp.write("Name = "+mdata.name+"\n")
        fp.write("\n++++++++++++++++++++++++++++++\n")
        fp.write("Model Data: \n")
        attrs=vars(mdata)
        fp.write('\n'.join("%s: %s" % item for item in attrs.items()))
        fp.write("\n++++++++++++++++++++++++++++++\n")
        fp.write("Building Blocks: \n")
        for b in BLOCK_list:
            attrs=vars(b)
            fp.write('\n'.join("%s: %s" % item for item in attrs.items()))
            fp.write('\n\n')

        fp.write("++++++++++++++++++++++++++++++\n")
        screen.write(T,fp)
        fp.close()
        if mdata.vrbLvl >= 1:
            print("    DONE: Writing out-file.")

    # display plots
    if mdata.plotflag > 0:
            plt.draw()
            plt.pause(1) # <-------
            print('\n')
            a = input("<Hit Enter To Close Figures>")
            plt.close()

    return 0

###
###
class MyError(Exception):
    def __init__(self, value):
        self.value = value
    def __str__(self):
        return repr(self.value)
###
### Main section of the code, executed if running the file
if __name__ == "__main__":
    userOptions = getopt(sys.argv[1:], shortOptions, longOptions)
    uoDict = dict(userOptions[0])

    if len(userOptions[0]) == 0 or ("--help" in uoDict):
        printUsage()
        sys.exit(1)

    if "--version" in uoDict:
        print("pflow.py: Version 1.0")

    # execute the code
    try:
        main(uoDict)
        print("\n \n")
        print("SUCCESS.")
        print("\n \n")

    except MyError as e:
        print("This run has gone bad.")
        print(e.value)
        sys.exit(1)



