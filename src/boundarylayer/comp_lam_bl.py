#! /usr/bin/env python3
"""
Compressible Laminar Boundary Layer.

2D compressible air boundary layer computation
1st order, implicit, lagged coefficinet,
laminar with sutherland viscosity law.

From Appendix B Computer Codes for Students
of the text:
    Joseph A. Schetz
    Boundary Layer Analysis
    Prentice-Hall 1993 ISBN 0-13-086885

Python implementation developed from earlier fortran code written by Wilson and PJ.

Euqations are dimensionless using:
    FREE STREAM VELOCITY, UINF,
    VISCOSITY, CMUINF,
    DENSITY, RHOINF,
    LENGTH, L,
    GAMMA,
    MACH NUMBER, XMIF,
    TEMPERATURE, TINF:

X/L, Y/L, U/UINF, V/UINF, P/RHOINF/UINF**2=P/GAMMA/XMIF**2/PINF
T/TINF.
RE=RHOINF*UINF*L/CMUINF. TAKE L=1.0.

Example:
Flat Plate with Constant Tw (Tw/Te=1) as a sample.
Free-stream Mach number, XMIF=4.0, TINF=300K, UINF=1390m/s
Start at Leading Edge with uniform profiles.


Author: Ingo Jahn
Last Modified: 12/3/2020
"""

from getopt import getopt
import matplotlib.pyplot as plt
import numpy as np
import os
from scipy.interpolate import interp1d
import sys
import selfsim_comp_bl as ssbl


class BL_Profile:
    """Boundary Layer Profile data."""

    def __init__(self, free_stream, gas_properties, delta_y, label=""):
        """Initialise profile."""
        self.free_stream = free_stream
        self.gas = gas_properties
        self.label = label
        self.delta_y = delta_y
        return None

    def set_properties(self, U, V, rho, H, XM1):
        """Add properties across profile. All normalised."""
        self.U = np.array(U)
        self.V = np.array(V)
        self.rho = np.array(rho)
        self.H = np.array(H)
        self.XM1 = np.array(XM1)
        return None

    def print_to_screen(self, interval=1, norm=True):
        """Print velocity Profile to Screen."""
        print(self.label)
        delta_index = next(i for i, u in enumerate(self.U) if u > 0.99)
        delta = delta_index * self.delta_y
        if norm is True:
            print(' {:^6}  {:^18}  {:^18}  {:^18}  {:^18}  {:^18}  {:^18}'
                  .format("Index", "y[m]", "y/delta[-]", "U/U_inf[-]", "V/U_inf[-]",
                          "RHO/RHO_inf[-]", "T/T_inf[-]"))
            for m in range(len(self.U)):
                if m % interval == 0 or m == len(self.U)-1:
                    print(' {:>6d}  {:>18.3e}  {:>18.3e}  {:>18.6e}  {:>18.6e}  {:>18.6e}'
                          .format(m, m*self.delta_y, m*self.delta_y/delta, self.U[m],
                                  self.V[m], self.rho[m], self.H[m]*self.XM1))
        else:
            print(' {:^6}  {:^18}  {:^18}  {:^18}  {:^18}  {:^18}  {:^18}'
                  .format("Index", "y[m]", "y/delta[-]",  "U[m/s]", "V[m/s]",
                          "RHO[kg/m3]", "T[K]"))
            for m in range(len(self.U)):
                if m % interval == 0 or m == len(self.U)-1:
                    print(' {:>6d}  {:>12.3e}  {:>12.3e}  {:>18.6e}  {:>18.6e}  {:>18.6e}'
                          .format(m, m*self.delta_y, m*self.delta_y/delta,
                                  self.U[m]*self.free_stream['u'],
                                  self.V[m]*self.free_stream['u'],
                                  self.rho[m]*self.free_stream['rho'],
                                  self.H[m]*self.XM1*self.free_stream['T']))
        return 0

    def plot(self, norm=True, ax=None):
        """Plot profiles to axis."""
        if ax is None:
            f, ax0 = plt.subplots(1, figsize=(11, 7), sharex=True)
        else:
            ax0 = ax

        y = np.arange(len(self.U))*self.delta_y
        delta_index = next(i for i, u in enumerate(self.U) if u > 0.99)
        delta = y[delta_index]
        if norm is True:
            ax0.plot(self.U, y, label='U/U_inf')
            ax0.plot(self.H*self.XM1, y, label='T/T_inf')
            ax0.plot(self.rho, y, label='rho/rho_inf')
            ax0.plot(self.V*10., y, label='10x V/U_inf')
            ax0.plot(ax0.get_xlim(), [delta, delta], 'k--', label="BL edge")
            ax0.set_ylim([0., ax0.get_ylim()[1]])
            ax0.set_xlim([0., ax0.get_xlim()[1]])
            ax0.set_ylabel('y [m]')
            ax0.set_xlabel(r'$\frac{U}{U_\infty}$, $\frac{T}{T_\infty}$, $\frac{\rho}{\rho_\infty}$, $10 \times \frac{V}{U_\infty}$')
            ax0.set_title(self.label)
            ax0.legend()

        else:
            print("Currently can only plot normalised velocity Profile")
            pass

        # add y/delta axis to right
        def forward(x):
            return x/delta

        def reverse(x):
            return x*delta
        secax = ax0.secondary_yaxis('right', functions=(forward, reverse))
        secax.set_ylabel(r'$\frac{y}{\delta}$')
        return ax0

class Fluid:
    """Fluid viscosity calulcated by Sutherland's law."""

    def __init__(self, C1=1.458e-6, S=110.39, gamma_const=True):
        """Initialise class."""
        self.C1 = C1  # [kg/m/s/K^.5] slope for sutherland's law
        self.S = S  # [K] offset for sutherland's law
        self.gamma_const = gamma_const
        return None

    def set_referenceConditions(self, L_ref, T_inf, U_inf, rho_inf, p_inf, M_inf):
        """Set reference conditions."""
        self.L_ref = L_ref
        self.T_inf = T_inf
        self.U_inf = U_inf
        self.rho_inf = rho_inf
        self.p_inf = p_inf
        self.M_inf = M_inf
        self.mu0 = self.calc_mu(T_inf)
        return None

    def get_gamma_temp_table(self):
        """Get gamma."""
        TEMP = [273., 373., 473., 573., 673., 773., 873., 973.]
        if self.gamma_const is True:
            GAMMA = 1.4*np.ones(8)
        else:
            GAMMA = [1.401, 1.397, 1.390, 1.378, 1.368, 1.357, 1.346, 1.338]
        return GAMMA, TEMP
        # TODO: Current tables need to be extended to negative temperatures!.

    def calc_mu(self, T):
        """Calculate viscosity."""
        mu = self.C1 * T**1.5 / (T + self.S)
        return mu

    def calc_mu_norm(self, T_norm):
        """Calculate viscosity (normalised)."""
        mu = self.calc_mu(T_norm * self.T_inf)
        # print(T_norm, T_norm * self.T_inf, mu, self.mu0)
        # print(mu/self.mu0)
        return mu/self.mu0


def trid(M_max, A, B, C, R, S):
    """Compute tridiagonal matrix inversion."""
    GAM = np.zeros(M_max)
    RP = np.zeros(M_max)
    GAM[0] = C[0]/B[0]
    RP[0] = R[0]/B[0]
    for m in np.arange(1, M_max):
        DENO = B[m] - A[m]*GAM[m-1]
        GAM[m] = C[m]/DENO
        RP[m] = (R[m] - A[m]*RP[m-1])/DENO

    MM = M_max-1  # need to reduce by 1 due to python indexing
    S[MM] = RP[MM]
    for m in np.arange(0, M_max-1):
        S[MM-m-1] = RP[MM-m-1] - GAM[MM-m-1]*S[MM-m+1-1]
    return S


def lagrange_interp(M_max, MM, GAMMA, TEMP, H, gas, XM1, verbosity=0):
    """Lagrangian inperolator for GAMMA and calculation of temperature."""
    YGAM, XT = gas.get_gamma_temp_table()

    # print('Here1', YGAM, XT, MM, NNX)
    # print('XM1', XM1, 'gas.T_inf', gas.T_inf)
    # print('H', H)
    for m in np.arange(1, MM+10):
        if m > M_max-1:
            print('Range exceeded for lagrange_interp.')
            break
        j = 0
        TEMP[m] = XM1*H[m]*gas.T_inf
        if TEMP[m] < min(XT) or TEMP[m] > max(XT):
            if verbosity > 0:  # check that within range
                print('Value for TEMP[{0}]={1} is outside of range for XT=[{2}; {3}]'.format(m, TEMP[m], min(XT), max(XT)))
        while True:
            j = j+1
            XINT = TEMP[m]
            # print('j, XINT', j, XINT)
            if j > 161:
                break
            else:
                YOUT = 0.
                for k in range(8):
                    TERM = YGAM[k]
                    for l in range(8):
                        if k != l:
                            TERM = TERM*(XINT-XT[l])/(XT[k]-XT[l])
                    YOUT = YOUT + TERM
                # print('YOUT', YOUT)
                # print(XINT, XT)
                if np.isnan(YOUT):
                    a = a+5
                TEMP[m] = (1.4/YOUT)*(YOUT-1.0) * gas.M_inf**2 * H[m] * gas.T_inf
                # print('TEMP[m]', m, TEMP[m])
                if abs(TEMP[m]-XINT) <= 0.005:
                    break
        GAMMA[m] = YOUT
        TEMP[m] = TEMP[m]/gas.T_inf
    # print("GAMMA in lagrange", GAMMA, TEMP)

    return GAMMA, TEMP


'''
# superseed by BL_profile class
def velocityProfile(title, M_max, delta_y, U, V, rho, H, XM1, gas, interval=1, norm=True):
    """Display Velocity Profile."""
    print(title)
    if norm is True:
        print(' {:^6}  {:^18}  {:^18}  {:^18}  {:^18}  {:^18}'
              .format("Index", "y[m]",  "U/U_inf[-]", "V/U_inf[-]",
                      "RHO/RHO_inf[-]", "T/T_inf[-]"))
        for m in range(M_max):
            if m % interval == 0 or m == M_max-1:
                print(' {:>6d}  {:>18.3e}  {:>18.3e}  {:>18.6e}  {:>18.6e}  {:>18.6e}'
                      .format(m, m*delta_y, U[m], V[m], rho[m], H[m]*XM1))
    else:
        print(' {:^6}  {:^18}  {:^18}  {:^18}  {:^18}  {:^18}'
              .format("Index", "y[m]",  "U[m/s]", "V[m/s]",
                      "RHO[kg/m3]", "T[K]"))
        for m in range(M_max) or m == M_max-1:
            if m % interval == 0:
                print(' {:>6d}  {:>12.3e}  {:>12.3e}  {:>18.6e}  {:>18.6e}  {:>18.6e}'
                      .format(m, m*delta_y, U[m]*gas.U_inf, V[m]*gas.U_inf,
                              rho[m]*gas.rho_inf, H[m]*XM1*gas.T_inf))
    return 0
'''

def axialProfiles(title, N_max, delta_x, ST, CF, DELTA, interval=5):
    """Display Axial profiles."""
    print(title)
    print(' {:^6}  {:^18}  {:^18}  {:^18}  {:^18}'
          .format("Index", "X[m]",  "ST", "CF[-]",
                  "delta[m]"))
    for m in range(N_max):
        if m % interval == 0 or m == N_max-1:
            print(' {:>6d}  {:>18.3e}  {:>18.3e}  {:>18.6e}  {:>18.6e}'
                  .format(m, m*delta_x, ST[m], CF[m], DELTA[m]))
    return 0


# ------------------------------------------------------------------------------
# MAIN SIMULATION CODE


def main(uo_dict):
    """Main code section."""
    # Read the jobfile then do some input checking
    exec(open(uo_dict["--job"]).read(), globals())

    # DEFINE VARIABLES
    prandtlNumber = gas_properties['Pr']  # [-] Prandtl Number
    gas = Fluid(C1=gas_properties['SL_slope'], S=gas_properties['SL_offset'], gamma_const=True)

    U_inf = free_stream['u'] # [m/s] free-stream velocity
    M_inf = free_stream['M']  # [-] free-stream Mach number
    p_inf = free_stream['p']  # [Pa] free-stream pressure
    L_ref = bc_space_march['L_ref']  # [m] Reference length    

    # Simulation parameters
    N_max = config_space_march['N_max']  # number of steps in x-direction
    M_max = config_space_march['M_max']  # number of steps in y-direction
    delta_y = config_space_march['delta_y'] # [m] step size in y_direction
    normaliseProfileFlag = config_space_march['normaliseProfileFlag']  # select if profiles are plotted as U or U/U_inf, etc...

    # Create interpolants for axially varying conditions
    F_Ue = interp1d(bc_space_march["x_L-vector"], bc_space_march["Ue/U_inf"])
    F_Te = interp1d(bc_space_march["x_L-vector"], bc_space_march["Te/T_inf"])
    F_Tw = interp1d(bc_space_march["x_L-vector"], bc_space_march["Tw/T_inf"])

    # consider flat plate case
    UE = bc_space_march['UE']
    DUEDX = bc_space_march['DUEDX']
    print('FLAT PLATE FLOWS:')
    print('UE={0:4e}[-] and DUEDX={1:4e}[-]'.format(UE, DUEDX))
    print('\n')

    # calculate remaining reference properties
    T_inf = (U_inf/M_inf)**2 / (gas_properties['gamma']*gas_properties['R'])
    free_stream['T'] = T_inf
    rho_inf = p_inf / (gas_properties['R']*T_inf)
    free_stream['rho'] = rho_inf
    mu_inf = gas.calc_mu(T_inf)
    free_stream['mu'] = mu_inf
    nu_inf = mu_inf/rho_inf

    RE_L = rho_inf * L_ref * U_inf / mu_inf
    RE_1 = rho_inf * 1.0 * U_inf / mu_inf
    delta_x = L_ref/(N_max-1)
    gas.set_referenceConditions(L_ref, T_inf, U_inf, rho_inf, p_inf, M_inf)

    print('FREE-STREAM CONDITIONS')
    print('Defined conditions:')
    print('U_inf={0:4e}[m/s]; M_inf={1:4e}[-]; p_inf={2:4e}[Pa]; L_ref={3:4e}[m]'
          .format(U_inf, M_inf, p_inf, L_ref))
    print('Infered conditions:')
    print('T_inf={0:4e}[K]; rho_inf={1:4e}[kg/m3]; mu_inf={2:4e}[Pa s]; nu_inf={3:4e}[m2/s]; RE_1={4:4e}[-]'
          .format(T_inf, rho_inf, mu_inf, nu_inf, RE_1))
    print('Grid dimensions: delta_x={0:4e}[m] (axial), delta_y={1:4e}[m] (wall normal)'
          .format(delta_x, delta_y))
    print('\n')

    GAMMA = np.zeros(M_max)
    GAMMA[0] = gas_properties['gamma']
    XM1 = (GAMMA[0]-1.0) * M_inf**2
    HE = F_Te(0.)/XM1  # evaluate interpolant at x/L = 0
    RHOE = 1.0
    PE = 1.0/(GAMMA[0]*M_inf**2)
    # Develop initial velocity profile
    U = np.zeros(M_max)
    U0 = np.zeros(M_max)
    V = np.zeros(M_max)
    V0 = np.zeros(M_max)
    H = np.zeros(M_max)
    H0 = np.zeros(M_max)
    TREF = np.zeros(M_max)
    TEMP = np.zeros(M_max)
    CMU = np.zeros(M_max)
    RHO = np.zeros(M_max)
    RHN = np.zeros(M_max)
    # set free-stream conditions into var and var0

    ST = np.zeros(N_max)
    CF = np.zeros(N_max)
    DELTA = np.zeros(N_max)
    A = np.zeros(M_max)
    B = np.zeros(M_max)
    C = np.zeros(M_max)
    R = np.zeros(M_max)

    DELTA[0] = bc_space_march["bl_thickness_start"]

    if bc_space_march["bl_profile_start"] == 'selfsimilar':
        # only need to read call self-similar profile and create option if these are selected. 
        SS_DATA = ssbl.main(uo_dict)
        F_UUe = interp1d(SS_DATA['y/delta'], SS_DATA['U/U_e'])
        F_HHe = interp1d(SS_DATA['y/delta'], SS_DATA['h/h_e'])
    elif bc_space_march["bl_profile_start"] == 'linear':
        pass
    else:
        raise CustomError('Option set for:\n'
                          'config_space_march["bl_profile_start"]={}\n'
                          'is not supported.'.format(config_space_march["bl_profile_start"]))


    for m in range(M_max):
        if m*delta_y < bc_space_march["bl_thickness_start"]:
            if bc_space_march["bl_profile_start"] == 'linear':
                U[m] = m*delta_y/bc_space_march["bl_thickness_start"] * F_Ue(0.)  # evaluate interpolant at x/L = 0
                H[m] = (F_Tw(0.) * (1. - m*delta_y/bc_space_march["bl_thickness_start"])
                       + F_Te(0.) * m*delta_y/bc_space_march["bl_thickness_start"])/XM1
            elif bc_space_march["bl_profile_start"] == 'selfsimilar':
                U[m] = F_UUe(m*delta_y/bc_space_march["bl_thickness_start"])
                H[m] = F_HHe(m*delta_y/bc_space_march["bl_thickness_start"])/XM1
        else:
            U[m] = F_Ue(0.)  # evaluate interpolant at x/L = 0
            H[m] = HE

        H0[m] = H[m]
        U0[m] = U[m]
        V[m] = 0.
        V0[m] = 0.
        TREF = XM1*H0[m]
        TEMP[m] = TREF
        GAMMA[m] = GAMMA[0]
        CMU[m] = gas.calc_mu_norm(TREF)
        RHO[m] = 1./(XM1*H[m])
        RHN[m] = RHO[m]
    # set wall conditions
    U[0] = 0.
    U0[0] = 0.
    H[0] = F_Tw(0.)/XM1   # evaluate interpolant at x/L = 0
    H0[0] = H[0]
    TREF = XM1*H0[0]
    TEMP[0] = TREF
    CMU[0] = gas.calc_mu_norm(TREF)
    RHO[0] = 1./(XM1*H[0])
    RHN[0] = RHO[0]

    # not sure what the following section is doing???
    # Set properties in first 5 cells to match free-stream.
    # I think this will only do something if user defined inflow profile.
    MEST = 2
    MMM = MEST+3
    """
    for m in np.arange(1, MEST):
        U0[m] = UE
        V0[m] = 0.0
        H0[m] = HE
        RHO[m] = 1.0/(XM1*H0[m])
    """
    profile_start = BL_Profile(free_stream, gas_properties,
                               config_space_march['delta_y'],
                               label='Velocity Profile Start, x={:4e}[m]'.format(0.))
    profile_start.set_properties(U0, V0, RHO, H0, XM1)
    profile_start.print_to_screen(interval=5, norm=True)

    # print('U0[1], U0[2], CMU[0], UE, delta_y, RE_1, RHOE')
    # print(U0[1], U0[2], CMU[0], UE, delta_y, RE_1, RHOE)

    CF[0] = (4.0*U0[1]-U0[2])*CMU[0]/(UE*UE*delta_y*RE_1*RHOE)
    RR = prandtlNumber**0.5
    HAW = HE * (1.0 + 0.195 * RR * M_inf**2)  # In Java code the recovery factor seems to be changed to 0.2 from 0.195 ???
    ST[0] = (3.0*H0[0] - 4.0*H0[1] + H0[2])*CMU[0] \
        / (2.0*prandtlNumber*UE*delta_y*RE_1*RHOE*(H[0]-HAW))

    # print('CF[0], ST[0]')
    # print(CF[0], ST[0], ST_temp)
    # print(H0[0:5], HAW, RR, M_inf, CMU[0], U[0:3])

    """
    # Not required as set from inputs
    # set DELTA for [0]
    for m in range(M_max):
        if U[m] > 0.99*UE:  # detect edge of boundary layer
            DELTA[0] = m*delta_y
            break
    """

    # space march along wall to solve for boundary layer
    for n in np.arange(1, N_max):
        if config_space_march["verbosity"] > 0:
            print('STARTING NEW SPACE MARCH STEP - n={}'.format(n))

        NNX = n
        U0[M_max-1] = UE
        H0[M_max-1] = HE
        RHO[M_max-1] = 1.0/(XM1*H0[M_max-1])

        B[0] = 1.0
        C[0] = 0.0
        R[0] = 0.0
        A[M_max-1] = 0.0
        B[M_max-1] = 1.0
        R[M_max-1] = UE
        DENO = RE_1*delta_y*delta_y

        # print('DENO, RHO[1], V0[1]')
        # print(DENO, RHO[0:3], V0[0:3], CMU[0:3], U0[0:3], delta_x)

        for m in np.arange(1, M_max-1):
            A[m] = -0.5*RHO[m]*V0[m]/delta_y - CMU[m-1]/DENO
            B[m] = RHO[m]*U0[m]/delta_x + 0.5*(CMU[m-1]+CMU[m])*2.0/DENO
            C[m] = 0.5*RHO[m]*V0[m]/delta_y - CMU[m]/DENO
            R[m] = RHOE*UE*DUEDX + RHO[m]*U0[m]*U0[m]/delta_x

        U = trid(M_max, A, B, C, R, U)
        # print('A, B, C, R', A, B, C, R)
        # print('U after trid', U)

        B[0] = 1.0
        C[0] = 0.0
        R[0] = H0[0]
        A[M_max-1] = 0.0
        B[M_max-1] = 1.0
        R[M_max-1] = HE
        DENO = prandtlNumber*RE_1*delta_y*delta_y

        for m in np.arange(1, M_max-1):
            A[m] = -0.5*RHO[m]*V0[m]/delta_y - CMU[m-1]/DENO
            B[m] = RHO[m]*U0[m]/delta_x + 0.5*(CMU[m-1]+CMU[m])*2.0/DENO
            C[m] = 0.5*RHO[m]*V0[m]/delta_y - CMU[m]/DENO
            R[m] = -RHOE*UE*DUEDX*U0[m] + RHO[m]*U0[m]*H0[m]/delta_x \
                + CMU[m]*(U[m+1]-U[m-1])*(U[m+1]-U[m-1])/(4.0*RE_1*delta_y*delta_y)

        H = trid(M_max, A, B, C, R, H)

        # check for edge of boundary layer.
        # print('HERE2', MEST, U)
        MT = MEST-8
        if MT < 0:
            MT = 0
        for m in np.arange(MT, M_max):
            if U[m] > 0.99*UE:  # detect edge of boundary layer
                MEST = m
                DELTA[n] = m*delta_y
                MMM = MEST + 3
                break

        # GAMMA, TEMP = lagrange_interp(M_max, MMM, GAMMA, H, gas)
        # print('First', GAMMA, TEMP)
        GAMMA, TEMP = lagrange_interp(M_max, MMM, GAMMA, TEMP, H, gas, XM1)
        # print('Second', GAMMA, TEMP)

        for m in np.arange(1, M_max-1):
            # temp0 = (1.0+T1)*(0.5*(TEMP[m]+TEMP[m-1]))**1.5 \
            #    / (0.5*(TEMP[m]+TEMP[m-1])+T1)
            temp1 = gas.calc_mu_norm(0.5*(TEMP[m]+TEMP[m-1]))
            # print('Compare temp0, temp1', m, temp0, temp1)
            CMU[m] = temp1
            RHN[m] = PE*GAMMA[m]/(0.5*(H[m-1]+H[m])*(GAMMA[m]-1.0))
        # print('GAMMA', GAMMA)

        for m in np.arange(1, M_max-1):
            RHOUA = RHO[m]*U0[m]
            RHOUB = RHO[m-1]*U0[m-1]
            # print('RHO', RHO)
            # print('U0', U0)
            # print('RHN', RHN)

            R1 = RHN[m-1]/RHN[m]
            V[m] = R1*V[m-1] - 0.5*delta_y/delta_x * ( U[m] + R1*U[m-1] - (RHOUA+RHOUB)/RHN[m] )
            if U[m] == U[m-1] and U0[m] == U0[m-1] and V0[m] == V0[m-1]:
                V[m] = 0.0

        # print('U')
        # print(U)

        # update old properties in var0
        U0[1:] = U[1:]
        V0[1:] = V[1:]
        H0[1:] = H[1:]
        RHO[1:] = RHN[1:]

        CF[n] = (4.0*U0[1]-U0[2])*CMU[0]/(UE*UE*delta_y*RE_1*RHOE)
        ST[n] = (3.0*H0[0]-4.0*H0[1]+H0[2])*CMU[0] \
            / (2.0*prandtlNumber*UE*delta_y*RE_1*RHOE*(H[0]-HAW))

        if config_space_march['verbosity'] > 0:
            print("Calc")
            print(n)
            # print(U0[1], U0[2], CMU[0], UE, RE_1, RHOE)
            print('CF[n], ST[n]')
            print(CF[n], ST[n])
            print(U[0:3], V[0:3], H[0:3]*XM1)

    # Write final velocity profile
    # velocityProfile('Velocity Porfile End, x={:4e}[m]'.format(n*delta_x), M_max, delta_y, U, V, RHO, H, XM1, gas, interval=1, norm=normaliseProfileFlag)

    profile_end = BL_Profile(free_stream, gas_properties,
                               config_space_march['delta_y'],
                               label='Velocity Porfile End, x={:4e}[m]'.format(n*delta_x))
    profile_end.set_properties(U, V, RHO, H, XM1)
    profile_end.print_to_screen(interval=5, norm=True)

    axialProfiles('Wall profiles', N_max, delta_x, ST, CF, DELTA, interval=1)

    if config_space_march['plot_results'] is True:
        # plot velocity profiles
        profile_start.plot()
        profile_end.plot()

        # plot axial variations
        f, (ax1, ax2, ax3) = plt.subplots(3, figsize=(11, 7), sharex=True)
        x_graph = np.arange(N_max)*delta_x
        ax1.plot(x_graph, DELTA, label=r'$\delta$')
        ax1.set_ylabel(r'$\delta$ [m]')
        ax1.set_xlim([0., ax1.get_xlim()[1]])
        ax1.set_ylim([0., ax1.get_ylim()[1]])
        ax2.plot(x_graph, CF, label='CF')
        ax2.set_ylabel('Skin Friction, CF')
        ax2.set_ylim([0., ax2.get_ylim()[1]])
        ax3.plot(x_graph, ST, label='ST')
        ax3.set_ylabel('Stanton Number, St')
        ax3.set_xlabel('x [m]')
        ax3.set_ylim([0., ax3.get_ylim()[1]])

        def forward(x):
            return x/bc_space_march['L_ref']
        def reverse(x):
            return x*bc_space_march['L_ref']
        secax = ax1.secondary_xaxis('top', functions=(forward, reverse))
        secax.set_xlabel(r'$\frac{x}{L_{ref}}$')

        # plot boundary conditions


        plt.draw()
        plt.pause(1)  # <-------
        print('\n \n')
        input('<Hit Enter To Close Figures>')
        plt.close()

    print('DONE.')



class CustomError(Exception):
    def __init__(self, value):
        self.value = value
    def __str__(self):
        return repr(self.value)


def check_inputs(uo_dict):
    """Check all mandatory options have been provided."""
    reqd_options = ["--job"]
    for op in reqd_options:
        if op not in uo_dict:
            raise CustomError("".join(("hypersonic_selfsimilar_bl requires argument '",
                              op, "', but this argument was not provided.\n")))

    # Check that jobfile exists
    if not os.path.isfile(uo_dict["--job"]):
        raise CustomError("".join(("Jobfile '", uo_dict["--job"], "' not found,",
                          " check that file path is correct.\n")))


def print_usage():
    print("")
    print("Code for numerical solution of compressible laminar boundary layer.")
    print("From a starting profile, the BL equations (momentum, energy) are solved")
    print("by implicitly space marching along a flat plate.")
    print("Options to specify a linear or self-similar (Crocco) inlet profile are")
    print("possible. For Crocco case selfsim_comp_bl.py is called.")
    print("")
    print("WARNING: currently only flat plates with constant external properties")
    print("are coded.")
    print("")
    print("Usage:")
    print("python3 comp_lam_bl.py --job=<jobfile>")
    print("")


short_options = ""
long_options = ["help", "job="]


if __name__ == '__main__':
    user_options = getopt(sys.argv[1:], short_options, long_options)
    uo_dict = dict(user_options[0])

    if len(user_options[0]) == 0 or "--help" in uo_dict:
        print_usage()
        sys.exit(1)

    else:
        check_inputs(uo_dict)
        main(uo_dict)
        print("\n")
        print("SUCCESS.")
        print("\n")