#! /usr/bin/env python3
"""
Self-similar boundray layer calculator for compressible laminar boundary layers.

The following code computes the boundary layer profiles for compressible laminar
cases. It follows the theory from Chapter 6 of:
Title: Boundary Layer Analysis
Author:	Joseph A Schetz; Rodney D W Bowersox
Publisher:	Reston, VA : American Institute of Aeronautics and Astronautics, 2011.

The boundary layer profile is calculated using a shooting method, that solves
the inital value problem for the simultaneous Momentum and Energy Equation:
(C * f_dd)_dash + f * f_dd = 0
(C/Pr * g_d)_dash + f * g_d + C * u_e^2/h_e * f_dd^2 = 0

The code support both adiabatic wall boundary conditions, and fixed T_wall/T_inf
boundary condition.

Author: Ingo Jahn
Last modified: 23-March-2020
"""

from getopt import getopt
import numpy as np
import matplotlib.pyplot as plt
import os
from scipy.optimize import root
from scipy.interpolate import interp1d
import sys


def eta_func(eta, X, DATA, gas_properties, free_stream, config_self_similar):
    """
    d_eta / d_y = U_e * rho(eta) / sqrt(2 * xi)
    or
    d_y / d_eta = sqrt(2 * xi) / U_e * 1 / rho(eta)
                = sqrt(2 * U_e * rho_e * mu_e * x / (U_e**2 * rho_2**2))  * rho_e / rho(eta)
                = rho_e/rho(eta) * sqrt(2) * x * sqrt(mu_e / (U_e * rho_e * x)
                = rho_e/rho(eta) * sqrt(2) * x / sqrt(Re_x)
    X = [eta_d]
    RHS[0] = rho_e/rho(eta) * sqrt(2) * 1  -->  [x / sqrt(Re_x)] taken to other side
    """
    F = interp1d(DATA['eta'], DATA['rho'], kind='linear', fill_value="extrapolate")
    rho = F(eta)
    RHS = np.zeros(1)
    RHS[0] = free_stream['rho']/rho * np.sqrt(2)
    return RHS


def selfsimilar_clbl(eta, X, gas_properties, free_stream, bc_self_similar, config_self_similar):
    """
    Momentum Equation
    (C * f_dd)_dash + f * f_dd = 0
    C_dash * f_dd + C * f_ddd + f * f_dd = 0  ----equation (1)

    Energy Equation
    (C/Pr * g_d)_dash + f * g_d + C * u_e^2/h_e * f_dd^2 = 0
    (C/Pr)_dash * g_d + C/Pr * g_dd + f * g_d + C * u_e^2/h_e * f_dd^2 = 0  ----equation (2)

         0   1    2    3   4
    X = [f, f_d, f_dd, g, g_d]
    X = RHS
    RHS[0] = f_d
    RHS[1] = f_dd
    RHS[2] = - 1/C * X[0] * X[2] - 1/C * C_dash * X[2]
    RHS[3] = g_d
    RHS[4] = - Pr/C * X[0] * X[4] - Pr * u_e^2/h_e * X[2]^2 - Pr/C * (C/Pr)_dash * X[4]
    """
    T_e = free_stream["T"]  # [K] temperature at outside of wall.
    u_e = free_stream["u"]  # [m/s]
    h_e = gas_properties["Cp"] * T_e  # [J/kg] - h = Cp * T

    RHS = np.zeros(5)
    RHS[0] = X[1]
    RHS[1] = X[2]
    RHS[3] = X[4]

    # evaluate constants
    T = free_stream['T']*X[3]
    if T < 0:
        T = abs(T)  # to avoid -1**1.5 we take the absolute value of X[3]
        if config_self_similar["verbosity"] > 1:
            print('Warning')
    mu_e = gas_properties['SL_slope'] * free_stream['T']**1.5 / (free_stream['T'] + gas_properties['SL_offset'])
    mu = gas_properties['SL_slope'] * T**1.5 / (T + gas_properties['SL_offset'])

    # C = rho * mu / (rho_e * mu_e)
    # as rho = P/RT --> rho/rho_e = T_e/T = 1/g
    C = 1./X[3] * mu/mu_e
    Pr = gas_properties["Pr"]  # mu * Cp * k  --> in accordiance with Van Driest we keep Pr constant

    # TODO: C and Pr should be evaluated as function of eta and also d_C/d_eta and d_Pr/d_eta

    if config_self_similar["evaluate_C_and_Pr_gradients"] is False:
        C_dash = 0
        Pr_dash = 0
        C_Pr_dash = 0
    else:
        raise CustomError("The simulation option for:\n"
                          "config_self_similar['evaluate_C_and_Pr_gradients']= True\n"
                          "has not been implemented yet.")

    RHS[2] = - 1/C * X[0] * X[2] - 1/C * C_dash * X[2]
    RHS[4] = - Pr/C * X[0] * X[4] - Pr * u_e**2 / h_e * X[2]**2 - Pr/C * C_Pr_dash * X[4]

    return RHS


def RK4_solve(func, x_0, args, t_max, dt=0.001, write_interval=10, t_start=0., verbosity=0):
    """
    RK4 solver.

    Runge-Kutta (RK4) applied differential to ODEs.
    Inputs:
    func - function returns function eqnavulations.
    x_0 - initial guess
    t_max - max time step
    dt=0.001 - forward marching interval
    write_interval=10 - after how many timesteps to write results
    t_start=0 - starting time
    verbosity=0 - define output
    """
    t = t_start
    X = x_0
    i = 0
    T_out = []
    X_out = []
    T_out.append(t)
    X_out.append(X)
    while t < t_max and t+dt < t_max:
        k1 = func(t, X, *args)
        k2 = func(t + dt / 2, X + dt / 2 * k1, *args)
        k3 = func(t + dt / 2, X + dt / 2 * k2, *args)
        k4 = func(t + dt, X + dt * k3, *args)
        X = X + dt / 6 * (k1 + 2 * k2 + 2 * k3 + k4)
        t = t + dt

        if verbosity > 0:
            if i % 20 == 0:
                print("{:^6}  {:^12}  {:^12}".format("Step", "t", "X"))
            print("{:>6d}  {:>12.3e}  {}".format(i, t, X))
        i = i+1
        if write_interval is None:
            pass
        else:
            if i % write_interval == 0:
                T_out.append(t)
                X_out.append(X)
    T_out.append(t)  # make sure final value is added.
    X_out.append(X)

    return T_out, X_out  # return final state vector


def plot_results(eta, X, DATA, delta_index):
    """Plot Results."""
    f, ax0 = plt.subplots(1, figsize=(11, 7), sharex=True)
    ax0.plot(X[0, :], eta, label="f")
    ax0.plot(X[1, :], eta, label="f' = u/u_e", linewidth=4)
    ax0.plot(X[2, :], eta, label="f''")
    ax0.plot(X[3, :], eta, label="g = h/h_e", linewidth=4)
    ax0.plot(X[4, :], eta, label="g'")
    ax0.plot(ax0.get_xlim(), [eta[delta_index], eta[delta_index]], 'k--', label="BL edge")
    ax0.set_ylabel(r'$\eta$')
    ax0.set_title('Self-similar compressible boundary layer profiles')
    ax0.legend()

    f, ax1 = plt.subplots(1, figsize=(11, 7), sharex=True)
    # ax11 = ax1.twinx()
    ax1.plot(X[1, :], eta, label="f' = u/u_e", linewidth=4)
    ax1.plot(X[3, :]/np.max(X[3, :]), eta, label="g = h/h_e", linewidth=4)
    ax1.plot(DATA['rho']/free_stream['rho'], eta, label="rho/rho_e")
    ax1.plot(ax1.get_xlim(), [eta[delta_index], eta[delta_index]], 'k--', label="BL edge")
    ax1.legend()
    ax1.set_xlabel('U/U_e, T/T_max, rho/rho_e')
    ax1.set_ylabel(r'$\eta$')
    print("Maximum T/T_e={}[-]".format(np.max(X[3, :])))

    f, ax2 = plt.subplots(1, figsize=(11, 7), sharex=True)
    # ax11 = ax1.twinx()
    ax2.plot(DATA['eta_2'], DATA['y/x*sqrt(Re)_2'], label="", linewidth=4)
    ax2.set_xlabel(r'$\eta$')
    ax2.set_ylabel(r'$\frac{y}{x} \, \sqrt{Re_\infty}$')
    ax2.set_title(r'Relationship between $\eta$ and $\frac{y}{x} \, \sqrt{Re_\infty}$')

    f, ax3 = plt.subplots(1, figsize=(11, 7), sharex=True)
    # ax11 = ax1.twinx()
    ax3.plot(DATA['U/U_e'], DATA['y/x*sqrt(Re)'], label="U/U_e", linewidth=4)
    ax3.plot(DATA['h/h_e'], DATA['y/x*sqrt(Re)'], label="h/h_e / T/T_e", linewidth=4)
    ax3.plot(DATA['rho/rho_e'], DATA['y/x*sqrt(Re)'], label="rho/rho_e", linewidth=4)
    ax3.plot(ax3.get_xlim(), [DATA['y/x*sqrt(Re)'][delta_index], DATA['y/x*sqrt(Re)'][delta_index]], 'k--', label="BL edge")
    ax3.legend()
    ax3.set_xlabel('U/U_e, T/T_e, rho/rho_e')
    ax3.set_ylabel(r'$\frac{y}{x} \, \sqrt{Re_\infty}$')
    ax3.set_title('Boundary Layer Properties')

    # add y/delta axis to right
    def forward(x):
        return x/DATA['y/x*sqrt(Re)'][delta_index]

    def reverse(x):
        return x*DATA['y/x*sqrt(Re)'][delta_index]
    secax = ax3.secondary_yaxis('right', functions=(forward, reverse))
    secax.set_ylabel(r'$\frac{y}{\delta}$')

    return 0


# ------------------------------------------------------------------------------
# MAIN SIMULATION CODE


def main(uo_dict):
    """Main code section."""
    # Read the jobfile then do some input checking
    exec(open(uo_dict["--job"]).read(), globals())

    # compute inferred parameters:
    T_inf = (free_stream["u"]/free_stream["M"])**2 / (gas_properties["gamma"]*gas_properties["R"])
    free_stream['T'] = T_inf
    rho_inf = free_stream['p'] / (gas_properties['R'] * free_stream['T'])
    free_stream['rho'] = rho_inf
    mu_inf = gas_properties['SL_slope'] * free_stream['T']**1.5 / (free_stream['T'] + gas_properties['SL_offset'])
    free_stream['mu'] = mu_inf

    print('FREE-STREAM CONDITIONS')
    print('Defined conditions:')
    print('U_inf={0:4e}[m/s]; M_inf={1:4e}[-]; p_inf={2:4e}[Pa]'
          .format(free_stream["u"], free_stream["M"], free_stream["p"]))
    print('Inferred conditions:')
    print('T_inf={0:4e}[K]'
          .format(T_inf))
    if bc_self_similar["adiabatic"] is True:
        print('Boundary Condition - Adiabatic:')
        print('d_T/d_eta|_wall = 0')
    else:
        print('Boundary Condition - Fixed Temperature:')
        print('T_w/T_e={0:4e}[-]; T_wall={1:4e}[K]'
              .format(bc_self_similar["g_wall"], bc_self_similar["g_wall"]*T_inf))
    print('\n')

    # Boundary Conditions from boundary layer theory.
    f_0 = 0.
    f_d_0 = 0.

    # define solver function that will be used in shooting method
    def solve_fun(X, gas_properties, free_stream, bc_self_similar, config_self_similar):
        """Root finding function."""
        args = (gas_properties, free_stream, bc_self_similar, config_self_similar)
        if bc_self_similar["adiabatic"] is True:
            x_iter = [f_0, f_d_0, X[0], X[1], 0.]  # X = [f, f_d, f_dd, g, g_d]
        else:
            x_iter = [f_0, f_d_0, X[0], bc_self_similar["g_wall"], X[1]]  # X = [f, f_d, f_dd, g, g_d]
        if config_self_similar["verbosity"] > 0:
            print('Starting Function Evaluation with X[0]={:18e}, X[1]={:18e}'.format(X[0], X[1]))
            print(x_iter)
        T_out, X_out = RK4_solve(selfsimilar_clbl, x_iter, args, config_self_similar["eta_max"], dt=config_self_similar["d_eta"], write_interval=None)
        OUT = [X_out[-1][1]-1., X_out[-1][3]-1.]
        if config_self_similar["verbosity"] > 0:
            print('    OUT = {}'.format(OUT))
        return OUT

    result = root(solve_fun, [config_self_similar["init-X[0]"], config_self_similar["init-X[1]"]],
                  args=(gas_properties, free_stream, bc_self_similar, config_self_similar),
                  method=config_self_similar["root_finder"])
    print("RESULTS from Shooting Method:\n", result, "\n")
    X = result.x

    print('Evaluating final profiles.')
    if bc_self_similar["adiabatic"] is True:
        x_final = [f_0, f_d_0, X[0], X[1], 0.]  # X = [f, f_d, f_dd, g, g_d]
    else:
        x_final = [f_0, f_d_0, X[0], bc_self_similar["g_wall"], X[1]]  # X = [f, f_d, f_dd, g, g_d]
    args = (gas_properties, free_stream, bc_self_similar, config_self_similar)
    T_out, X_out = RK4_solve(selfsimilar_clbl, x_final, args,
                             config_self_similar["eta_max"],
                             dt=config_self_similar["d_eta"],
                              write_interval=config_self_similar["write_interval"])
    eta = np.array(T_out)
    X = np.array(X_out).T

    rho = free_stream['rho'] * 1./X[3, :]
    DATA = {}
    DATA['rho'] = rho
    DATA['eta'] = eta
    DATA['U/U_e'] = X[1, :]
    DATA['h/h_e'] = X[3, :]
    DATA['rho/rho_e'] = 1./X[3, :]

    # find BL/thickness -> DELTA
    if max(X[1, :]) < 0.99:
        print("eta_max is at less than delta ==> Extend evaluation range.")
        print("At eta_max={}, U/U_e={}[-]".format(eta[-1], X[1, -1]))
        delta_index = 0
    else:
        delta_index = next(i for i, x in enumerate(X[1, :]) if x > 0.99)
        print("Boundary Layer Edge: eta={}.".format(eta[delta_index]))


    # compute y and y/x sqrt(Re_x)
    args = (DATA, gas_properties, free_stream, config_self_similar)
    temp0, temp1 = RK4_solve(eta_func, [0], args,
                             config_self_similar["eta_max"],
                             dt=config_self_similar["d_eta"],
                             write_interval=1)

    DATA['eta_2'] = temp0
    DATA['y/x*sqrt(Re)_2'] = np.array(temp1).flatten()
    # interpolate so thay we have y/x*sqrt(Re) for other data too.
    F = interp1d(DATA['eta_2'], DATA['y/x*sqrt(Re)_2'], kind='linear')
    DATA['y/x*sqrt(Re)'] = F(DATA['eta'])
    DATA['y/delta'] = DATA['y/x*sqrt(Re)']/DATA['y/x*sqrt(Re)'][delta_index]

    if config_self_similar['plot_results'] is True:
        # plot results
        plot_results(eta, X, DATA, delta_index)

        plt.draw()
        plt.pause(1)  # <-------
        print('\n \n')
        input('<Hit Enter To Close Figures>')
        plt.close()

    return DATA


class CustomError(Exception):
    def __init__(self, value):
        self.value = value
    def __str__(self):
        return repr(self.value)


def check_inputs(uo_dict):
    """Check all mandatory options have been provided."""
    reqd_options = ["--job"]
    for op in reqd_options:
        if op not in uo_dict:
            raise CustomError("".join(("hypersonic_selfsimilar_bl requires argument '",
                              op, "', but this argument was not provided.\n")))

    # Check that jobfile exists
    if not os.path.isfile(uo_dict["--job"]):
        raise CustomError("".join(("Jobfile '", uo_dict["--job"], "' not found,",
                          " check that file path is correct.\n")))


def print_usage():
    print("")
    print("Code for the calculation of self-similar compressible boundary layers.")
    print("BL equations (Momentum and Energy) are solved using a shooting method.")
    print("")
    print("WARNING: Currently the BL equations are simplified and terms C' and Pr' are set to zero.")
    print("")
    print("Usage:")
    print("python3 self_comp_bl.py --job=<jobfile>")
    print("")


short_options = ""
long_options = ["help", "job="]


if __name__ == '__main__':
    user_options = getopt(sys.argv[1:], short_options, long_options)
    uo_dict = dict(user_options[0])

    if len(user_options[0]) == 0 or "--help" in uo_dict:
        print_usage()
        sys.exit(1)

    else:
        check_inputs(uo_dict)
        main(uo_dict)
        print("\n")
        print("SUCCESS.")
        print("\n")
