#!/usr/bin/env python
""" 
Simple script to convert files from VTK to STL format

Author: Ingo Jahn
Last modified: 07/11/2016
"""

import vtk
import shutil as sh
from getopt import getopt
import sys as sys 
import os as os
import numpy as np


shortOptions = ""
longOptions = ["help", "input=", "output=", 'overwrite=']

###
###
def printUsage():
    print ""
    print "Usage: rotor_profile.py [--help] [--input=<Input FileName>] [--output=<output FileName>] [--overwrite=Y/N]"
    print " input: Must be specified"
    print " output: if blank, write to input.STL"
    print " overwrite: default is No. --> will check before overwriting"
    return
###
###
class MyError(Exception):
    def __init__(self, value):
        self.value = value
    def __str__(self):
        return repr(self.value)
###
###
def main(uoDict):

    """
    main function
    """
    # create string to collect warning messages
    warn_str = "\n"

    # get input data  
    inputFileName = uoDict.get("--input", "n/a")
    outputFileName = uoDict.get("--output", "n/a")    
    overwrite = uoDict.get("--overwrite", "n/a")  

    # check if input file was specified
    if inputFileName is "n/a":
        raise MyError("Input file name was not specified. Bailing out.")

    # check that input file name is appropriate
    file_parts = inputFileName.split('.')
    if len(file_parts) is 1:
        inputFileName = (file_parts[0] + '.vtk')
        print ".vtk extension has been added to input filename"
    elif len(file_parts) is 2:
        if file_parts[1] != 'vtk':
            raise MyError("Input file has wrong extension. Extension should be blank or .vtk. Bailing out.")
    else:
        raise MyError("Inappropriate input filename. Bailing out.")

    # check that input filename exists 
    if not os.path.isfile(inputFileName):
        raise MyError("Input File: " + inputFileName + " doesn't exist. Bailing out.")        

    # replace and/or add extension to 
    if outputFileName is "n/a":
        # check that input file name is appropriate
        file_parts = inputFileName.split('.')
        outputFileName = (file_parts[0] + '.stl')

    # check extension on output filename    
    file_parts = outputFileName.split('.')
    if len(file_parts) is 1:
        outputFileName = (file_parts[0] + '.stl')
        print ".stl extension has been added to output filename"
    elif len(file_parts) is 2:
        if file_parts[1] != 'stl':
            raise MyError("Output file has wrong extension. Extension should be blank or .stl. Bailing out.")
    else:
        raise MyError("Inappropriate output filename. Bailing out.")

    # check if output exists and can be overwritten
    if os.path.isfile(outputFileName):
        print "\n"
        print "Output file with name: ", outputFileName, " already exists."
        if overwrite is "n/a":
            print "Do you want to overwrite the file?"
            while True:
                answer = raw_input("Y/N")
                if answer is 'Y' or answer is 'y':
                    break
                elif answer is 'N' or answer is 'n':
                    raise MyError("Cannot write output as file already exits. Bailing out.") 
                else:
                    print "Input not recognised. Answer Y/N"
        if overwrite == 'N' or overwrite == 'n':
            raise MyError("Output file already exists. Bailing out.") 

    # do actual conversion
    reader = vtk.vtkUnstructuredGridReader()
    reader.SetFileName(inputFileName)

    filter = vtk.vtkDataSetSurfaceFilter()
    filter.SetInput(reader.GetOutput())

    clean = vtk.vtkCleanPolyData()
    clean.SetInputConnection(filter.GetOutputPort())

    triangles = vtk.vtkTriangleFilter()
    triangles.SetInputConnection(clean.GetOutputPort() )

    write = vtk.vtkSTLWriter()
    write.SetInputConnection(triangles.GetOutputPort() )
    #write.SetFileTypeToASCII()
    write.SetFileTypeToBinary()
    write.SetFileName(outputFileName)
    write.Write() 
    
    print "Output data has been written to ", outputFileName

    return 0
###
###
if __name__ == "__main__":
    userOptions = getopt(sys.argv[1:], shortOptions, longOptions)
    uoDict = dict(userOptions[0])
    
    if len(userOptions[0]) == 0 or uoDict.has_key("--help"):
        printUsage()
        sys.exit(1)
    
    try:
        main(uoDict)
        print "\n \n"
        print "SUCESS: Conversion has been completed."
        print "\n \n"
    except MyError as e:
        print "\n \n"
        print "BAD: This run of vtk2stl.py has gone bad."
        print e.value
        print "\n \n"

        sys.exit(1)
