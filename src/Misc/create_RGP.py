#! /usr/bin/env python3
"""
Create_RGP.py
Python script to generate Real Gas Property (.rpg) filesm as used by ANSYS CFX.
The tabulated fluid properties are created using CoolProp. 

To create generate file, execute:
>>> create_RPG.py

Author: Ingo Jahn - University of Queensland
Last Updated: 2020-3-9
"""

# import modules
import numpy as np
import CoolProp.CoolProp as CP

"""
# Default selection for comparison to example in CFX Guide
# https://www.sharcnet.ca/Software/Ansys/16.0/en-us/help/cfx_mod/cfxRealRealOrga.html
# Define Properties and Names
fluid = "R134a"
Filename = "R134a.rgp"
Description = "Refrigerant R134fa frm CoolProp Database"

# Define Ranges and Levels
P_min = 5.5e4
P_max = 4e5
P_levels = 50
T_min = 240
T_max = 400
T_levels = 30
Tsat_min = 240
Tsat_max = 350
Tsat_levels = 5
P_crit = CP.PropsSI('PCRIT', fluid)
P_trip = CP.PropsSI('PTRIPLE', fluid)
T_crit = CP.PropsSI('TCRIT', fluid)
T_trip = CP.PropsSI('TTRIPLE', fluid)

# Note When comparing to data on CFX webpage some differnces exist for liquid properties.
"""

# Define Properties and Names
fluid = "R245fa"
Filename = "R245fa.rgp"
Description = "Refrigerant R245fa frm CoolProp Database"

# Define Ranges and Levels
P_min = 1e5
P_max = 1e6
P_levels = 50
T_min = 200
T_max = 500
T_levels = 30
Tsat_min = 200
Tsat_max = 247
Tsat_levels = 10
P_crit = CP.PropsSI('PCRIT', fluid)
P_trip = CP.PropsSI('PTRIPLE', fluid)
T_crit = CP.PropsSI('TCRIT', fluid)
T_trip = CP.PropsSI('TTRIPLE', fluid)

P_inc = (P_max-P_min) / float(P_levels-1.)
T_inc = (T_max-T_min) / float(T_levels-1.)
# Tsat_inc = (Tsat_max-Tsat_min) / float(Tsat_levels-1.)

print("\n")
print("##########################################################")

print("Builidng Real Gas Property File for ", fluid, ".\n")
print("Pressure Range   : ", P_min, " to ", P_max, " Pa in ", P_levels, " steps of ", P_inc, " Pa.")
print("Temperature Range: ", T_min, " to ", T_max, " K in  ", T_levels, " steps of ", T_inc, " K.")
print("Saturation Lines : ", Tsat_min, " to ", Tsat_max, " K in ", Tsat_levels, " steps of equal pressurs.")
print("Critical Point: ", P_crit, " Pa; ", T_crit, " K.")
print("Tripple Point : ", P_trip, " Pa; ", T_trip, " K.")
print("Maximum separation line temperature should be near critical point temperature. Otherwise errors may occur. ")
print("##########################################################")
print("\n")

# start working with file
f = open(Filename, 'w')

# Write Header
f.write('$$$$HEADER  \n')
f.write('$$$' + fluid + '\n')
f.write('1 \n')
f.write('$$PARAM  \n')
f.write('26  \n')
f.write('DESCRIPTION  \n')
f.write(Description + '\n')
f.write('NAME \n')
f.write(fluid + '\n')
f.write('INDEX \n')
f.write(fluid + '\n')
f.write('MODEL \n')
f.write('2  \n')
f.write('UNITS \n')
f.write('1  \n')
f.write('PMIN_SUPERHEAT \n')
f.write('%7.7e \n' % P_min)
f.write('PMAX_SUPERHEAT  \n')
f.write('%7.7e \n' % P_max)
f.write('TMIN_SUPERHEAT  \n')
f.write('%7.7e \n' % T_min)
f.write('TMAX_SUPERHEAT  \n')
f.write('%7.7e \n' % T_max)
f.write('TMIN_SATURATION  \n')
f.write('%7.7e \n' % Tsat_min)
f.write('TMAX_SATURATION  \n')
f.write('%7.7e \n' % Tsat_max)
f.write('SUPERCOOLING  \n')
f.write('%7.7e \n' % 0.)
f.write('P_CRITICAL  \n')
# f.write('%7.7e \n' % P_crit)
f.write('P_TRIPLE  \n')
# f.write('%7.7e \n' % P_trip)
f.write('T_CRITICAL  \n')
# f.write('%7.7e \n' % T_crit)
f.write('T_TRIPLE  \n')
# f.write('%7.7e \n' % T_trip)
f.write('TABLE_1  \n')
f.write('%i   %i \n' % (T_levels, P_levels))
f.write('TABLE_2  \n')
f.write('%i   %i \n' % (T_levels, P_levels))
f.write('TABLE_3  \n')
f.write('%i   %i \n' % (T_levels, P_levels))
f.write('TABLE_4  \n')
f.write('%i   %i \n' % (T_levels, P_levels))
f.write('TABLE_5  \n')
f.write('%i   %i \n' % (T_levels, P_levels))
f.write('TABLE_6  \n')
f.write('%i   %i \n' % (T_levels, P_levels))
f.write('TABLE_7  \n')
f.write('%i   %i \n' % (T_levels, P_levels))
f.write('TABLE_8  \n')
f.write('%i   %i \n' % (T_levels, P_levels))
f.write('TABLE_9  \n')
f.write('%i   %i \n' % (T_levels, P_levels))
f.write('SAT_TABLE  \n')
f.write('%i   4   9  \n' % (Tsat_levels))

# Write DATA
f.write('$$$$DATA  \n')
f.write('$$$' + fluid + '\n')
f.write('1 \n')
f.write('$$PARAM  \n')
f.write('26  \n')
f.write('DESCRIPTION  \n')
f.write(Description + '\n')
f.write('NAME \n')
f.write(fluid + '\n')
f.write('INDEX \n')
f.write(fluid + '\n')
f.write('MODEL \n')
f.write('2  \n')
f.write('UNITS \n')
f.write('1  \n')
f.write('PMIN_SUPERHEAT \n')
f.write('%7.7e \n' % P_min)
f.write('PMAX_SUPERHEAT  \n')
f.write('%7.7e \n' % P_max)
f.write('TMIN_SUPERHEAT  \n')
f.write('%7.7e \n' % T_min)
f.write('TMAX_SUPERHEAT  \n')
f.write('%7.7e \n' % T_max)
f.write('TMIN_SATURATION  \n')
f.write('%7.7e \n' % Tsat_min)
f.write('TMAX_SATURATION  \n')
f.write('%7.7e \n' % Tsat_max)
f.write('SUPERCOOLING  \n')
f.write('%7.7e \n' % 0.)
f.write('P_CRITICAL  \n')
# f.write('%7.7e \n' % P_crit)
f.write('P_TRIPLE  \n')
# f.write('%7.7e \n' % P_trip)
f.write('T_CRITICAL  \n')
# f.write('%7.7e \n' % T_crit)
f.write('T_TRIPLE  \n')
# f.write('%7.7e \n' % T_trip)
f.write('TABLE_1  \n')
f.write('%i   %i \n' % (T_levels, P_levels))
f.write('TABLE_2  \n')
f.write('%i   %i \n' % (T_levels, P_levels))
f.write('TABLE_3  \n')
f.write('%i   %i \n' % (T_levels, P_levels))
f.write('TABLE_4  \n')
f.write('%i   %i \n' % (T_levels, P_levels))
f.write('TABLE_5  \n')
f.write('%i   %i \n' % (T_levels, P_levels))
f.write('TABLE_6  \n')
f.write('%i   %i \n' % (T_levels, P_levels))
f.write('TABLE_7  \n')
f.write('%i   %i \n' % (T_levels, P_levels))
f.write('TABLE_8  \n')
f.write('%i   %i \n' % (T_levels, P_levels))
f.write('TABLE_9  \n')
f.write('%i   %i \n' % (T_levels, P_levels))
f.write('SAT_TABLE  \n')
f.write('%i   4   9  \n' % (Tsat_levels))

# Super Tables
f.write('$$$$SUPER_TABLE  \n')
f.write('9 \n')


# define helper functions used to generate the different superheated tables.
def write_TandP_levels():
    """Write T and P levels."""
    f.write('%i   %i \n' % (T_levels, P_levels))
    String = ''
    for t in range(T_levels):
        T = T_min + t*T_inc
        String = String + ('%7.7e ' % T)
    f.write(String+'\n')
    String = ''
    for p in range(P_levels):
        P = P_min + p*P_inc
        String = String + ('%7.7e ' % P)
    f.write(String+'\n')
    return 0


def write_2D_table(phi_string):
    """Write 2-D table for phi."""
    String = ''
    for p in range(P_levels):
        P = P_min + p*P_inc
        for t in range(T_levels):
            T = T_min + t*T_inc
            if P > P_crit:
                phi = CP.PropsSI(phi_string, 'P', P, 'T', T, fluid)
            else:
                Tsat = CP.PropsSI('T', 'P', P, 'Q', 1., fluid)
                if T > Tsat:
                    phi = CP.PropsSI(phi_string, 'P', P, 'T', T, fluid)
                else:
                    phi = CP.PropsSI(phi_string, 'P', P, 'Q', 1., fluid)
            String = String + ('%7.7e ' % phi)
    f.write(String+'\n')
    return 0


def write_1D_saturationLine(phi_string):
    """Write 1-D saturation line for phi."""
    String = ''
    for p in range(P_levels):
        P = P_min + p*P_inc
        if P > P_crit:
            Tsat = T_crit
        else:
            Tsat = CP.PropsSI('T', 'P', P, 'Q', 1., fluid)
        String = String + ('%7.7e ' % Tsat)
    f.write(String+'\n')
    String = ''
    for p in range(P_levels):
        P = P_min + p*P_inc
        if P > P_crit:
            phi_sat = CP.PropsSI(phi_string, 'P', P_crit, 'Q', 1., fluid)
        else:
            phi_sat = CP.PropsSI(phi_string, 'P', P, 'Q', 1., fluid)
        String = String + ('%7.7e ' % phi_sat)
    f.write(String+'\n')
    return 0


# create superheated tables
# Table_1 - Specific Enthalpy
print('Writing - TABLE_1 - Specific Enthalpy')
f.write('TABLE_1  \n')
write_TandP_levels()
write_2D_table('H')
write_1D_saturationLine('H')


# Table_2 - Speed of Sound
print('Writing - TABLE_2 - Speed of Sound')
f.write('TABLE_2  \n')
write_TandP_levels()
write_2D_table('A')
write_1D_saturationLine('A')


# Table_3 - Specific Volume
print('Writing - TABLE_3 - Specific Volume')
f.write('TABLE_3  \n')
write_TandP_levels()
String = ''
for p in range(P_levels):
    P = P_min + p*P_inc
    for t in range(T_levels):
        T = T_min + t*T_inc
        if P > P_crit:
            D = CP.PropsSI('D', 'P', P, 'T', T, fluid)
        else:
            Tsat = CP.PropsSI('T', 'P', P, 'Q', 1., fluid)
            if T > Tsat:
                D = CP.PropsSI('D', 'P', P, 'T', T, fluid)
            else:
                D = CP.PropsSI('D', 'P', P, 'Q', 1., fluid)
        String = String + ('%7.7e ' % (1./D))
f.write(String+'\n')
String = ''
for p in range(P_levels):
    if P > P_crit:
        Tsat = T_crit
    else:
        Tsat = CP.PropsSI('T', 'P', P, 'Q', 1., fluid)
    String = String + ('%7.7e ' % Tsat)
f.write(String+'\n')
String = ''
for p in range(P_levels):
    P = P_min + p*P_inc
    phi_string = 'D'
    if P > P_crit:
        phi_sat = CP.PropsSI(phi_string, 'P', P_crit, 'Q', 1., fluid)
    else:
        phi_sat = CP.PropsSI(phi_string, 'P', P, 'Q', 1., fluid)
    String = String + ('%7.7e ' % (1./phi_sat))
f.write(String+'\n')


# Table_4 - Specific Heat at Constant Volume
print('Writing - TABLE_4 - Specific Heat at constant Volume')
f.write('TABLE_4  \n')
write_TandP_levels()
write_2D_table('CVMASS')
write_1D_saturationLine('CVMASS')


# Table_5 - Specific Heat at Constant Pressure
print('Writing - TABLE_5 - Specific Heat at constant Pressure')
f.write('TABLE_5  \n')
write_TandP_levels()
write_2D_table('CP0MASS')
write_1D_saturationLine('CP0MASS')


# Table_6 - dP/ dv at constant temperature
print('Writing - TABLE_6 - dP/ dv at constant Temperature')
f.write('TABLE_6  \n')
write_TandP_levels()
String = ''
for p in range(P_levels):
    P = P_min + p*P_inc
    for t in range(T_levels):
        T = T_min + t*T_inc
        if P > P_crit:
            dP_dD = CP.PropsSI('d(P)/d(D)|T', 'P', P, 'T', T, fluid)
            D = CP.PropsSI('D', 'P', P, 'T', T, fluid)
        else:
            Tsat = CP.PropsSI('T', 'P', P, 'Q', 1., fluid)
            if T > Tsat:
                dP_dD = CP.PropsSI('d(P)/d(D)|T', 'P', P, 'T', T, fluid)
                D = CP.PropsSI('D', 'P', P, 'T', T, fluid)
            else:
                dP_dD = CP.PropsSI('d(P)/d(D)|T', 'P', P, 'Q', 1., fluid)
                D = CP.PropsSI('D', 'P', P, 'Q', 1., fluid)
        dat = -1. * dP_dD * D*D
        String = String + ('%7.7e ' % dat)
f.write(String+'\n')
String = ''
for p in range(P_levels):
    if P > P_crit:
        Tsat = T_crit
    else:
        Tsat = CP.PropsSI('T', 'P', P, 'Q', 1., fluid)
    String = String + ('%7.7e ' % Tsat)
f.write(String+'\n')
String = ''
for p in range(P_levels):
    P = P_min + p*P_inc
    if P > P_crit:
        dP_dD = CP.PropsSI('d(P)/d(D)|T', 'P', P_crit, 'Q', 1., fluid)
        D = CP.PropsSI('D', 'P', P_crit, 'Q', 1., fluid)
    else:
        dP_dD = CP.PropsSI('d(P)/d(D)|T', 'P', P, 'Q', 1., fluid)
        D = CP.PropsSI('D', 'P', P, 'Q', 1., fluid)
    dat = -1. * dP_dD * D*D
    String = String + ('%7.7e ' % dat)
f.write(String+'\n')


# Table_7 - Specific Entropy
print('Writing - TABLE_7 - Specific Entropy')
f.write('TABLE_7  \n')
write_TandP_levels()
write_2D_table('S')
write_1D_saturationLine('S')


# Table_8 - Dynamic Viscosity
print('Writing - TABLE_8 - Dynamic Viscosity')
f.write('TABLE_8  \n')
write_TandP_levels()
String = ''
for p in range(P_levels):
    P = P_min + p*P_inc
    for t in range(T_levels):
        T = T_min + t*T_inc
        if P > P_crit:
            DV = CP.PropsSI('VISCOSITY', 'P', P, 'T', T, fluid)
        else:
            Tsat = CP.PropsSI('T', 'P', P, 'Q', 1., fluid)
            if T > Tsat:
                for i in range(100):
                    try:
                        DV = CP.PropsSI('VISCOSITY', 'P', P, 'T', T, fluid)
                    except:
                        T = T + np.random.rand()*1.
                        continue
                    else:
                        break
            else:
                DV = CP.PropsSI('VISCOSITY', 'P', P, 'Q', 1., fluid)
        String = String + ('%7.7e ' % DV)
f.write(String+'\n')
write_1D_saturationLine('VISCOSITY')


# Table_9 - Thermal Conductivity
print('Writing - TABLE_9 - Therml Conductivty')
f.write('TABLE_9  \n')
write_TandP_levels()
String = ''
for p in range(P_levels):
    P = P_min + p*P_inc
    for t in range(T_levels):
        T = T_min + t*T_inc
        if P > P_crit:
            TC = CP.PropsSI('CONDUCTIVITY', 'P', P, 'T', T, fluid)
        else:
            Tsat = CP.PropsSI('T', 'P', P, 'Q', 1., fluid)
            if T > Tsat:
                for i in range(100):
                    try:
                        TC = CP.PropsSI('CONDUCTIVITY', 'P', P, 'T', T, fluid)
                    except:
                        T = T + np.random.rand()*1.
                        continue
                    else:
                        break
            else:
                TC = CP.PropsSI('CONDUCTIVITY', 'P', P, 'Q', 1., fluid)
        String = String + ('%7.7e ' % TC)
f.write(String+'\n')
write_1D_saturationLine('CONDUCTIVITY')


# Create Saturation Tables
print('Writing - Saturation Tables')
f.write('$$SAT_TABLE  \n')
f.write('%i   4   9  \n' % (Tsat_levels))
Psat_min = CP.PropsSI('P', 'T', Tsat_min, 'Q', 1., fluid)
Psat_max = CP.PropsSI('P', 'T', Tsat_max, 'Q', 1., fluid)
Psat_levels = Tsat_levels
Psat_inc = (Psat_max-Psat_min) / (Tsat_levels - 1.)
String = ''
for p in range(Psat_levels):
    P = Psat_min + p*Psat_inc
    String = String + ('%7.7e ' % P)
f.write(String+'\n')
String = ''
for p in range(Psat_levels):
    P = Psat_min + p*Psat_inc
    T = CP.PropsSI('T', 'P', P, 'Q', 1., fluid)
    String = String + ('%7.7e ' % T)
f.write(String+'\n')
String = ''
for p in range(Psat_levels):
    String = String + ('%7.7e ' % 0.)
f.write(String+'\n')
String = ''
for p in range(Psat_levels):
    String = String + ('%7.7e ' % 0.)
f.write(String+'\n')

# Properties
print('Writing - 2-phase region properties')
for q in range(2):
    String = ''
    for p in range(Psat_levels):  # Specific Enthalpy
        P = Psat_min + p*Psat_inc
        H = CP.PropsSI('H', 'P', P, 'Q', q, fluid)
        String = String + ('%7.7e ' % H)
    f.write(String+'\n')
    String = ''
    for p in range(Psat_levels):  # Specific Heat at Constant Pressure
        P = Psat_min + p*Psat_inc
        CPm = CP.PropsSI('CPMOLAR', 'P', P, 'Q', q, fluid)
        M = CP.PropsSI('M', fluid)
        String = String + ('%7.7e ' % (CPm/M))
    f.write(String+'\n')
    String = ''
    for p in range(Psat_levels):  # Density
        P = Psat_min + p*Psat_inc
        D = CP.PropsSI('D', 'P', P, 'Q', q, fluid)
        String = String + ('%7.7e ' % D)
    f.write(String+'\n')
    String = ''
    for p in range(Psat_levels):    # Change in Density with Pressure
        P = Psat_min + p*Psat_inc
        dD_dP = CP.PropsSI('d(D)/d(P)|T', 'P', P, 'Q', q, fluid)
        String = String + ('%7.7e ' % dD_dP)
    f.write(String+'\n')
    String = ''
    for p in range(Psat_levels):    # Entropy
        P = Psat_min + p*Psat_inc
        S = CP.PropsSI('S', 'P', P, 'Q', q, fluid)
        String = String + ('%7.7e ' % S)
    f.write(String+'\n')
    String = ''
    for p in range(Psat_levels):  # Specific Heat at Constant Volume
        P = Psat_min + p*Psat_inc
        CV = CP.PropsSI('CVMASS', 'P', P, 'Q', q, fluid)
        String = String + ('%7.7e ' % CV)
    f.write(String+'\n')
    String = ''
    for p in range(Psat_levels):  # Speed of Sound
        P = Psat_min + p*Psat_inc
        A = CP.PropsSI('A', 'P', P, 'Q', q, fluid)
        String = String + ('%7.7e ' % A)
    f.write(String+'\n')
    String = ''
    for p in range(Psat_levels):  # Vicosity
        P = Psat_min + p*Psat_inc
        DV = CP.PropsSI('VISCOSITY', 'P', P, 'Q', q, fluid)
        String = String + ('%7.7e ' % DV)
    f.write(String+'\n')
    String = ''
    for p in range(Psat_levels):  # Thermal Conductivity
        P = Psat_min + p*Psat_inc
        TC = CP.PropsSI('CONDUCTIVITY', 'P', P, 'Q', q, fluid)
        String = String + ('%7.7e ' % TC)
    f.write(String+'\n')

f.close()  # close the file

print('\n \n')
print('.RGP File has been Generated')
print('\n \n')
