#! /usr/bin/env python3
"""
script to install the different python codes into $REPO/bin, so that they can be executed from the command line.

This has been developed specifically for use with spyder. 
To execute the 'installed' codes, move into the newly created sub-directory /bin.

Call using %run spyder_install.py

Author: Ingo JH Jahn 
Last modified: 2018/10/25
"""

from getopt import getopt
import os as os
import shutil as shutil
import sys as sys

shortOptions = ""
longOptions = ["help", "overwrite", "src-only", "verbosity="]


def main(uoDict):
    """Run main code to copy files to install directory."""
    
    # set verbosity flag
    verbosity = float(uoDict.get("--verbosity", 0))
    # get gernal parameters
    cwd = os.getcwd()
    
    if verbosity >= 1:
        print("START: Checking directory structure")
    if '--overwrite' in uoDict:
        # check that /bin directory exists
        if os.path.exists(os.path.join(cwd,'bin')):
            # delete bin and any sub-directories
            shutil.rmtree(os.path.join(cwd,'bin'))
        if verbosity >= 1:
            print("    overwrite --> Removing existing /bin directory")
    else:
        if '--src-only' not in uoDict:
            # check if /bin exists in current working directory.
            if os.path.exists(os.path.join(cwd,'bin')):
                raise MyError("The directory bin or a file of the same name already exists. Remove the directory/file or invoke the installation with the --overwrite option to replace all files or the --src_only option to only replace the files from src")
    if verbosity >= 1:
        print("  DONE: Checking directory structure \n")
        
    if verbosity >= 1:
        print("START: Checking bin directory")        
    dest = os.path.join(cwd,'bin')
    # create the /bin directory if required
    if not os.path.isdir(dest):   
        if verbosity >= 1:
            print("    Creating new /bin directory") 
        try:  
            os.mkdir(dest)
        except OSError:  
            raise MyError("Creation of the directory {0} failed".format())
    if verbosity >= 1:
        print("  DONE: Checking bin directory  \n") 
        
    if verbosity >= 1:
        print("START: Copying files from src")      
    # recursively copy files form /src
    src_directory = os.path.join(cwd,'src')
    src_files = getListOfFiles(src_directory)
    for file_name in src_files:
        #full_file_name = os.path.join(src_directory, file_name)
        if (os.path.isfile(file_name)):
            if verbosity >= 2:
                print("    Copy {0} to {1}.".format(file_name,dest))
            shutil.copy(file_name, dest)
    if verbosity >= 1:
        print("  DONE: Copying files from src \n")              

    if '--src-only' not in uoDict:
        if verbosity >= 1:
            print("START: Copying files from examples")      
        # recursively copy files form /examples
        src_directory = os.path.join(cwd,'examples')
        src_files = getListOfFiles(src_directory)
        for file_name in src_files:
            #full_file_name = os.path.join(src_directory, file_name)
            if (os.path.isfile(file_name)):
                if verbosity >= 2:
                    print("    Copy {0} to {1}.".format(file_name,dest))
                shutil.copy(file_name, dest)
        if verbosity >= 1:
            print("  DONE: Copying files from examples \n")              

    return 0

    
def getListOfFiles(dirName):
    """Create a list of file and sub-directory names."""

    listOfFile = os.listdir(dirName)
    allFiles = list()
    # Iterate over all the entries
    for entry in listOfFile:
        # Create full path
        fullPath = os.path.join(dirName, entry)
        # If entry is a directory then get the list of files in this directory 
        if os.path.isdir(fullPath):
            allFiles = allFiles + getListOfFiles(fullPath)
        else:
            allFiles.append(fullPath)
                
    return allFiles
    

def printUsage():
    """Print usage instructions."""
    print("")
    print("Usage: spyder_install.py [--help] [--overwrite] [--src_only] [--verbosity=0 (1 or 2)]")
    return 0


class MyError(Exception):
    """Class for exceptions."""

    def __init__(self, value):
        """Initialse class."""
        self.value = value

    def __str__(self):
        """Set String class."""
        return repr(self.value)
        
        


if __name__ == "__main__":
    userOptions = getopt(sys.argv[1:], shortOptions, longOptions)
    uoDict = dict(userOptions[0])

    if "--help" in uoDict:
        printUsage()
        sys.exit(1)

    # execute the code
    try:
        main(uoDict)
        print("\n \n")
        print("SUCESS.")
        print("\n \n")

    except MyError as e:
        print("This run has gone bad.")
        print(e.value)
        sys.exit(1)