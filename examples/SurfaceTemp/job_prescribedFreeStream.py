"""
Input files with capability to read Sholto's heat flux files.
"""
import numpy as np

# load time and value from file
timeList = []
heatFluxList = []
rhoInfList = []
VInfList = []
TInfList = []

filename = "ExampleFlighData.txt"
with open(filename, 'r') as infile:
    for line in infile:
        lineList = line.split(" ")
        if lineList[0] is "#":
            pass
        else:
            timeList.append(float(lineList[0]))
            heatFluxList.append(-1.*float(lineList[6]))
            rhoInfList.append(float(lineList[5]))
            VInfList.append(float(lineList[2]) * np.sqrt(1.4*287*float(lineList[3])))
            TInfList.append(float(lineList[3]))

# settings for run
gdata.qUpdatedMode = 'firstOrderHold'  # approach taken to update
gdata.method = 'UNITSTEP'  # set method for how response is calculated. Options 'IMPULSE', 'UNITSTEP'
gdata.scheme = 'IMPLICIT'  # Set scheme used foor solutuion. Options: 'IMPLICIT', 'EXPLICIT'
gdata.subcycle = None  # Set number of subcycles. Set to None or 1 to switch off.
gdata.geometry = 'SPHERICAL'  # set of equations used Options: 'flat', 'cylindrical', 'spherical'
gdata.N_coeff = 20  # Number of coefficients used
gdata.tStart = timeList[0]  # [s] set starting time
gdata.tEnd = timeList[-1]  # [s] set end time
gdata.timeSteps = 20   # [-] number of steps used for temporal solution
gdata.spaceSteps = 5  # [-] number of steps used for spatial solution
gdata.dtplot = 5  # [s] time interval between plots
gdata.graph = 'SINGLE'  # Set if output plotted in single or multiple figures. Options: 'SINGLE', 'MULTIPLE'

# set how heat transfer is set at end0 and end1
end0_HTC.type = 'adiabatic'

end1_HTC.type = 'EQUATION_STAGNATION'  # Options: 'EQUATION_STAGNATION', 'EQUATION_LAMINAR_FP', 'EQUATION_TURBULENT_FP', 'EQUATION_LEADING_EDGE'
end1_HTC.time = timeList  # [s]
end1_HTC.rhoInf = rhoInfList  # [kg/m3] free-stream density
end1_HTC.VInf = VInfList  # [m/s] free-stream velocity
end1_HTC.TInf = TInfList  # [K] free-stream temperature
end1_HTC.radius = 50e-3  # [m] stagnation point radius
end1_HTC.Cp = 1000.  # [J/kg.K] free-stream specific heat at constant pressure
# end1_HTC.Qref = heatFluxList  # [W/m^2] optional reference heat flux for comparison
end1_HTC.radiationFlag = False  # True/False switch if radiation modelling is includes
end1_HTC.radiationEmissivity = 0.8  # [-] emissivity



# define the wall that will be modelled
wall.x0 = 10e-3  # [m] thickness of wall to be analysed (x0 should be smaller for cylindrical and spherical)
wall.x1 = 50e-3  # [m] thickness of wall to be analysed
wall.density = 19.3e3  # [kg / m3] density of wall material
wall.thermalConductivity = 237  # [W /(m K)] thermal conductivity
wall.specificHeat = 134  # [J / (kg K)]
wall.temperatureStart = 300  # [K] temperature at start


# some material data
# Tungsten
# density = 19.3e3  # [kg/m3] density
# thermalConductivity = 237  # [W /(m K)] thermal conductivity
# specificHeat = 134  # [J / (kg K)]

# Cork
# density = 2700.  # [kg/m3] density
# thermalConductivity = 0.039  # [W /(m K)] thermal conductivity
# specificHeat = 1900  # [J / (kg K)]

# Titanium
# density = 4506.  # [kg/m3] density
# thermalConductivity = 17  # [W /(m K)] thermal conductivity NOTE: varies with temperature.
# specificHeat = 470  # [J / (kg K)]

# Carbon-Carbon
# density = 1.547e3  # [kg/m3] density
# thermalConductivity = 6.603  # Based on conditions at 1077K # [W /(m K)] thermal conductivity NOTE: varies with temperature.
# specificHeat = 1784.  # Based on conditions at 1077K # [J / (kg K)]
