"""
Example input file for SurfTemp.
In current case this models heat transfer to a thick cylindrical wall with inner
and outer radii r=10e-3 m and R=50e-3m.

Depending on the settings either the inner or outer wall is adiabatic and the a
heat fluw of -1000 W/m^2 is applied to the other wall. As heat flux is defined
positive in the end0 to end1 direction, this corresponds to energy loss from the
wall at end0 and a heat conduction into the cylinder wall at end1.


Author: Ingo Jahn
Last Modified: 2020.03.09
"""

# settings for run
gdata.qUpdatedMode = 'firstOrderHold'  # approach taken to update
gdata.scheme = 'EXPLICIT'  # Set scheme used foor solutuion. Options: 'IMPLICIT', 'EXPLICIT'
gdata.method = 'UNITSTEP'  # set method for how response is calculated. Options 'IMPULSE', 'UNITSTEP'
gdata.subcycle = None  # Set number of subcycles. Set to None or 1 to switch off.
gdata.geometry = 'CYLINDRICAL'  # set of equations used Options: 'flat', 'cylindrical', 'spherical'
gdata.N_coeff = 20  # Number of coefficients used
gdata.tStart = 0.  # [s] set starting time
gdata.tEnd = 5.  # [s] set end time
gdata.timeSteps = 40  # [-] number of steps used for temporal solution
gdata.spaceSteps = 20  # [-] number of steps used for spatial solution
gdata.dtplot = 0.1  # [s] time interval between plots
gdata.graph = 'SINGLE'  # Set if output plotted in single or multiple figures. Options: 'SINGLE', 'MULTIPLE'


# set how heat transfer is set at end0 and end1
if False:  # thermal conduction out of wall at end0, the inner surface. 
    end0_HTC.type = 'set_Q'
    end0_HTC.time = [0., 1., 2., 3., 4., 5., 6., 7., 8., 9., 10.]
    end0_HTC.value= [-1000., -1000., -1000., -1000., -1000., -1000., -1000., -1000., -1000., -1000., -1000., ]
    end0_HTC.radiationFlag = False  # True/False switch if radiation modelling is includes
    end0_HTC.radiationEmissivity = 0.8  # [-] emissivity
else:
    end0_HTC.type = 'adiabatic'

if True:  # thermal conduction into the wall at end1, the outer surfac.
    end1_HTC.type = 'set_Q'
    end1_HTC.time = [0., 1., 2., 3., 4., 5., 6., 7., 8., 9., 10.]
    end1_HTC.value= [-1000., -1000., -1000., -1000., -1000., -1000., -1000., -1000., -1000., -1000., -1000., ]
    end1_HTC.radiationFlag = False  # True/False switch if radiation modelling is includes
    end1_HTC.radiationEmissivity = 0.8  # [-] emissivity
else:
    end1_HTC.type = 'adiabatic'   


# define the wall that will be modelled
wall.x0 = 10.e-3  # [m] thickness of wall to be analysed
wall.x1 = 50.e-3  # [m] thickness of wall to be analysed
wall.density = 2700.  # [kg / m3] density of wall material
wall.thermalConductivity = 237.  # [W /(m K)] thermal conductivity
wall.specificHeat = 900.  # [J / (kg K)]
wall.temperatureStart = 0.  # [K] temperature at start
