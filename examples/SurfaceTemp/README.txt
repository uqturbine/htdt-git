README.txt

SurfTemp.py is a simple solver to calculate the effects of cumulative heat 
transfer applied to a wall modelled using 1-D. This tools can be used to explore
the evolution of the wall temperature profile or to explore the effects of 
changing surface temperature on heat-transfer.

The underlying assumption of the approach is that thermal conduction in the 1-D
wall system can be approximated as linear, which allows the thermal response to
modelled as linear combination of impulse (or unit step) responses.


    | T(x, t=0) |     | I(x, t=0)      0      ...      0     |   | q(t=0) |
    | T(x, t=1) |  =  | I(x, t=1)  I(x, t=0)  ...      0     | * | q(t=1) |
          ...   |     |    ...        ...     ...      0     |   |  ...   |
    | T(x, t=N) |     | I(x, t=N)     ...     ...  I(x, t=0) |   | q(t=N) |


Currently SurfaceTemp includes 1-D wall models for planar, cylindrical, and
spherical geometries and support a range of models to prescribe heat flux at
each end of the wall. Schematically the wall is defined as having end0 and end1,
corresponding to the left and right surface of the wall respectively. (Note, for
cylindrical and spherical, the end1 must be at the larger radius)

Wall model schematic:

          end0                                                    end1
           |----X----X----X----X----X----X----X----X----X----X----|
        Tsurf0  T1   T2   T3   T4   T5   T6   T7   T8   T9  T10 Tsurf1
          
        'end0_HTC'              'wall'                          'end1_HTC'
        class to set            class to set                    class to set 
        HTC boundary            wall properties                 HTC boundary
        conditions                                              conditions

Simulations are set up an run by four classes (gdata, end0_HTC, end1_HTC, wall)
that define the simulation parameters, boundary conditions, and wall properties
respectively. 

Users are recommended to inspect the examples cases job_prescribedHeatFlux.py
and job_prescribedFreeStream.py respectively for example simulations for further
guidance on how to use the tool.

The following gives a brief summary of how to run the tool and description of 
the classes that need to be defined.

Running
-------
Usage: SurfTemp.py [--help] [--job=<jobFileName>] [--verbosity=0 (1 or 2)]
                [--out-file=<FileName>] [--plot]

     --job=          Name of python file, e.g. job.py that defines how the simulation parameters.
     --verbosity=    Sets the level of on-screen output being provided.
     --out-file=     Provide filename for writing output data.
     --plot          flag to plot output data


gdata - Sets simulation control parameters
------------------------------------------
gdata.qUpdatedMode = 'firstOrderHold'  # approach taken to update heat flux.
        Currently only 'firtOrderHold' is supported.
gdata.method = 'UNITSTEP'  # set method for how response is calculated. Options 'IMPULSE', 'UNITSTEP'
        The thermal response can be calculated using two approaches. 
        IMPULSE -> discrete heat flux impulses are applied at each time.
        UNITSTEP -> A square pulse with net heat flux matching applied heat flux
                    over same time period is applied. The square pulse is
                    constructed by a pair of equal and opposite magnitude unit
                    step responses with appropriate delays.
gdata.subcycle = 1 # Set number of subcycles. Set to None or 1 to switch off.
gdata.scheme = 'IMPLICIT'  # Set scheme used foor solutuion. Options: 'IMPLICIT', 'EXPLICIT'
        Both implict and explicit solution approaches are implemented. 
gdata.geometry = 'flat'  # set of equations used Options: 'flat', 'cylindrical', 'spherical'
        Allows selection of solution approach for the 1-D thermal conduction model.
        This can be solved in cartesian (flat), axi-symmetric (cyclindrical), or
        spherical (spherical) coordinate systems, corresponding to corresponding
        geometries. 
gdata.N_coeff = 20  # Number of coefficients used
        The solution of the impulse/unit step response uses a Fourier series
        solution. This sets the number of coefficients used. 
gdata.tStart = 0  # [s] set starting time
gdata.tEnd = 100  # [s] set end time
gdata.timeSteps = int(gdata.tEnd / 10.)   # [-] number of steps used for temporal solution
gdata.spaceSteps = 10  # [-] number of steps used for spatial solution
        Changing this number does not affect the simulation accuracy, only the
        spatial resolution of the displayes/written data. 
gdata.dtplot = 50  # [s] time interval between plots
        As part of the output discrete temperature profile solutions will be
        shown. This sets the time interval between these profiles.
gdata.graph = 'SINGLE'  # Set if output plotted in single or multiple figures. Options: 'SINGLE', 'MULTIPLE'


end0_HTC & end1_HTC - Set heat transfer boundary conditions for end0 and end1
-----------------------------------------------------------------------------
end0_HTC and end1_HTC refer to the two ends of the 1-D wall respectively. The
following describes the settings for different heat transfer modelling
approaches. All examples are described with respect to end0, but can equally be
applied to end1. Note heat flux is left -> right, hence signs need to be set
accordingly.

    ADIABATIC
    end0_HTC.type = 'adiabatic'

    PRESCRIBED HEAT FLUX
    end0_HTC.type = 'set_Q'
    end0_HTC.time = [0., 1., 2., 3., 4., 5., 6., 7., 8., 9., 10.]  # [s]
    end0_HTC.value= [-1000., -1000., -1000., -1000., -1000., -1000., -1000., -1000., -1000., -1000., -1000., ]  # [W/m^2]

    'time' and 'value' must be two equal length vectors that prescribe heat flux at
    discrete points in time. The actual heat flux is evaluated using linear
    interpolation.

    Optionally, emission via radiation can be added. 
    end0_HTC.radiationFlag = False  # True/False switch if radiation modelling is includes
    end0_HTC.radiationEmissivity = 0.8  # [-] emissivity

    PRESCRIBED SURFACE TEMPERATURE
    end0_HTC.type = 'set_T'
    end0_HTC.time = [0., 1., 2., 3., 4., 5., 6., 7., 8., 9., 10.]  # [s]
    end0_HTC.value= [300., 400., 400., 400., 400., 500., 500., 500., 700., 700., 700., ]  # [K]

    'time' and 'value' must be two equal length vectors that prescribe temperature at
    discrete points in time. The actual temperature is evaluated using linear
    interpolation.

    MODELLED HEAT TRANSFER
    Here the heat transfer is calculated based on the prescribed free-stream flow conditions
    and current wall temperature using approximate empirical correlations. Currently 
    equations/correlations are available for 
        'EQUATION_TURBULENT_FP' - Turbulent flow parallel to flat plate 
        'EQUATION_LAMINAR_FP'   - Laminar flow parallel to flat plate
        'EQUATION_STAGNATION'   - Stagnation point heat transfer
        'EQUATION_LEADING_EDGE' - Heat transfer to inclined leading edge.
    In the following examples timeList, rhoInfList, VInfList, and TInfList, are equal length arrays containing
    respective free-stream conditions. 
    end0_HTC.type = 'EQUATION_TURBULENT_FP'  # Options: 'EQUATION_STAGNATION', 'EQUATION_LAMINAR_FP', 'EQUATION_TURBULENT_FP', 'EQUATION_LEADING_EDGE'
    end0_HTC.time = timeList  # [s] time
    end0_HTC.rhoInf = rhoInfList  #  [kg/m^3] free-stream density
    end0_HTC.VInf = VInfList  #  [m/s] free-stream velocity
    end0_HTC.TInf = TInfList  #  [K] free-stream temperature
    end0_HTC.radius = 50e-3  #  [m] stagnation point radius
    end0_HTC.phi = np.radians(34.8)  # [rad] leading edge inclination angle
    end0_HTC.delta = np.radians(15.)  # [rad] leading edge sweep
    end0_HTC.x = 10.  # [m] 
    end0_HTC.Cp = 1000.  # [J/(kg K)] Specific heat at constant pressure for free-stream gas
    # end0_HTC.Qref = heatFluxList  # [W/m^2] optional reference heat transfer for comparison

    Optionally, emission via radiation can be added as shown previously.


wall - defines geometry and thermophysical properties of wall
-------------------------------------------------------------
wall.x0 = 10.e-3  # [m] x-position (radius) for end0 of wall (x0 should be smaller for cylindrical and spherical)
wall.x1 = 20.e-3  # [m] x-position (radius) for end1 of wall to be analysed
wall.density = 2700.  # [kg / m3] density of wall material
wall.thermalConductivity = 0.039  # [W /(m K)] thermal conductivity
wall.specificHeat = 1900.  # [J / (kg K)]
wall.temperatureStart = 300.  # [K] Initial temperature distribution for wall.






Author: Ingo Jahn
Last modified: 9/3/2020