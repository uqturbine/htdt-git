"""
Example input file for tracking a rotating wheel with speckle patern.
"""


# set image/video details
source.path = 'liam_wheel/MotorTune1k_006'
source.filenameBase = 'Cam_2409050113_'
source.firstFrame = '000000'
source.lastFrame = '000999'
source.suffix = 'tif'


# set blob identification details
detector0 = DETECTOR()
# Change thresholds
detector0.params.minThreshold = 50
detector0.params.maxThreshold = 200
# set parameter
detector0.minDistBetweenBlobs = 0.1
# Filter by Area.
detector0.params.filterByArea = True
detector0.params.minArea = 50
detector0.params.maxArea = 200
# Filter by Circularity
detector0.params.filterByCircularity = True
detector0.params.minCircularity = 0.1
detector0.params.maxCircularity = 1
# Filter by Convexity
detector0.params.filterByConvexity = False
detector0.params.minConvexity = None
detector0.params.maxConvexity = None
# Filter by Inertia
detector0.params.filterByInertia = False
detector0.params.minInertiaRatio = None
detector0.params.maxInertiaRatio = None

# set blob identification details
detector1 = DETECTOR()
# Change thresholds
detector1.params.minThreshold = 50
detector1.params.maxThreshold = 200
# set parameter
detector1.minDistBetweenBlobs = 0.1
# Filter by Area.
detector1.params.filterByArea = True
detector1.params.minArea = 200
detector1.params.maxArea = 2000

# specify regions that will be analysed for blobs.
region0 = REGION(type='ALL', detector=detector0, name='wheel', threshold=50)  
region1 = REGION(type='ALL', detector=detector1, name='centre', threshold=50)

# settings for image processing
process.scale = 2.1

# setting data for displaying
gdata.wait = 0.1  # time interval between image updates
