"""
Example input file for tracking a rotating wheel with speckle patern.
"""

# tracking settings
track.N_max = [300, 40] # maximum number of blobs that will be tracked
track.deltaT = 1./2e3  # time interval between observations
track.AssociationType = 'nearest'  # define how association is permformed
track.filterType = 'HYACE_1'  # selct type of filter used

track.distNearest = 10.  # distance used for nearest association cut-off


if track.filterType == 'HYACE_0':
    """Set up a first order Kalman Filter."""
    # Kalman filter settigns for TypeA_1
    track.x_start = np.array([0., 0., 0.])  # starting point for state-space
    track.covariance_P = np.diag([1., 1., 1.])*0.1  # set covarinace (P)
    track.processNoise_Q = Q_discrete_white_noise(3, dt=track.deltaT, var=1.)  # set proces noise (Q)
    track.measurementNoise_R_scale = 1.  # set measurement noise

elif track.filterType == 'HYACE_1':
    """Set up a first order Kalman Filter."""
    # Kalman filter settigns for TypeA_1
    track.x_start = np.array([0., 0., 0., 0., 0., 0.])  # starting point for state-space
    track.covariance_P = np.diag([1., 1., 1., 1., 1., 1.])*0.1  # set covarinace (P)
    Q = np.zeros([6, 6])
    Q[0:3, 0:3] = Q_discrete_white_noise(3, dt=track.deltaT, var=1.)  # set proces noise (Q)
    Q[3:6, 3:6] = Q_discrete_white_noise(3, dt=track.deltaT, var=1.)  # set proces noise (Q)
    track.processNoise_Q = Q

    track.measurementNoise_R_scale = 5.  # set measurement noise


else:
    print('Error')

print("Display matrices set for Kalman Filter")
print('track.processNoise_Q \n:', track.processNoise_Q)
