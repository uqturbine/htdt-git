"""
Example input file for tracking a rotating wheel with speckle patern.
"""


# set image/video details
source.path = 'HyACE/RUN882_TIFF'
source.filenameBase = 'run882_C001H001S0001'
source.firstFrame = '000001'  # '002190'
source.lastFrame = '000801' # '002540'
source.suffix = 'tif'


# detect blobs on WING
detector0 = DETECTOR()
# set blob identification details
# Change thresholds
detector0.params.minThreshold = 50
detector0.params.maxThreshold = 200
# set parameter
detector0.minDistBetweenBlobs = 0.1
# Filter by Area.
detector0.params.filterByArea = True
detector0.params.minArea = 20
detector0.params.maxArea = 200
# Filter by Circularity
detector0.params.filterByCircularity = True
detector0.params.minCircularity = 0.01
detector0.params.maxCircularity = 1
# Filter by Convexity
detector0.params.filterByConvexity = False
detector0.params.minConvexity = None
detector0.params.maxConvexity = None
# Filter by Inertia
detector0.params.filterByInertia = False
detector0.params.minInertiaRatio = None
detector0.params.maxInertiaRatio = None

# detect blobs on FLAP
detector1 = DETECTOR()
# set blob identification details
# Change thresholds
detector0.params.minThreshold = 50
detector0.params.maxThreshold = 200
# set parameter
detector0.minDistBetweenBlobs = 0.01
# Filter by Area.
detector0.params.filterByArea = True
detector0.params.minArea = 20
detector0.params.maxArea = 200
# Filter by Circularity
detector0.params.filterByCircularity = True
detector0.params.minCircularity = 0.1
detector0.params.maxCircularity = 1
# Filter by Convexity
detector0.params.filterByConvexity = False
detector0.params.minConvexity = None
detector0.params.maxConvexity = None
# Filter by Inertia
detector0.params.filterByInertia = False
detector0.params.minInertiaRatio = None
detector0.params.maxInertiaRatio = None


# settings for image processing
process.scale = 2

# specify regions that will be analysed for blobs.
region0 = REGION(type='RECTANGLE', detector=detector0, name='wing', threshold=50, x0=375, y0=265, width=300, height=125)  
region1 = REGION(type='RECTANGLE', detector=detector1, name='flap', threshold=50, x0=155, y0=225, width=135, height=150)

# show regions to confirm correct selection
gdata.showRegions = True

# setting data for displaying
gdata.wait = 0.05  # time interval between image updates
