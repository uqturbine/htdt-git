"""
Example input file for tracking a rotating wheel with speckle patern.
"""

# settings for run
gdata.imageJobFile = 'wheel_image2.py'

# tracking settings
track.N_max = 200  # maximum number of blobs that will be tracked
track.delta_T = 1e-3  # time interval between observations
track.AssociationType = 'nearest'  # define how association is permformed
track.filterType = 'TypeA_2'  # selct type of filter used

track.distNearest = 10.  # distance used for nearest association cut-off


if track.filterType == 'TypeA_1':
    """Set up a first order Kalman Filter."""
    # Kalman filter settigns for TypeA_1
    track.x_start = np.array([0., 0.])  # starting point for state-space
    track.covariance_P = np.diag([1., 1.])*0.1  # set covarinace (P)
    track.processNoise_Q = Q_discrete_white_noise(2, dt=track.delta_T, var=10.)  # set proces noise (Q)
    track.measurementNoise_R_scale = 2  # set measurement noise

elif track.filterType == 'TypeA_2':
    """Set up a second order Kalman Filter."""
    # Kalman filter settigns for TypeA_2
    track.x_start = np.array([0., 0., 0.])  # starting point for state-space
    track.covariance_P = np.diag([1., 1., 1.])*0.1  # set covarinace (P)
    track.processNoise_Q = Q_discrete_white_noise(3, dt=track.delta_T, var=1000.)  # set proces noise (Q)
    track.measurementNoise_R_scale = 2  # set measurement noise

else:
    print('Error')

print("Display matrices set for Kalman Filter")
print('track.processNoise_Q \n:', track.processNoise_Q)
