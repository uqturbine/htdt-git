"""Jobfile for MotionTracker.py."""


# GConf fixes overall parameters.
global_config = {
    "VERBOSITY"         : 2,
    "PX2M"              : 1.,  # conversion factor from pixels (unit in input file) to m
    "IMAGE_DIR"         : "./",  # path to image directory
    "RUN_NAME"          : "run",  #

    "IMAGE_DATA_FILE"   : "hyace-run882-image.txt",
#    "FILTER_DATA_FILE"  : "run-filter-data.txt",
    "FILTER_DATA_FILE"  : "hyace-run882-data_HYACE_1.txt",
#    "FILTER_DATA_FILE_LIST" : ["hyace-run884-data_HYACE_1.txt", "hyace-run884-data_HYACE_1.txt"],

    "ASSOCIATION_TYPE"  : "nearest",  # how predictions and measurements are associated

    "FILTER_TYPE"       : "HYACE_1",
    "DT"                : 1./2e3,  # time steps between images
    }


# image_process_config is a dictionary that defines settings for image processing.
image_process_config = {
    "start_frame"       : 0,
    "end_frame"         : 100,
    }


associate_config = {
    "distNearest"       : 10.,  # distance used for nearest association cut-off
    }


# tracking settings
filter_settings = {
    "N_max"     : [350, 38], # maximum number of blobs that will be tracked/filtered
    "DT"        : 1./2e3  # time interval between observations
    }

measurementNoise = 2.  # Measurement Noise in Pixels

# filter_settings is a disctionary that defines settings for filtering of the data
if global_config["FILTER_TYPE"] == 'HYACE_0':
    """Set up a first order Kalman Filter."""
    # Kalman filter settigns for TypeA_1
    filter_settings["x_start"] = np.array([0., 0., 0.])  # starting point for state-space
    filter_settings["covariance_P"] = np.diag([1., 1., 1.])*0.1  # set covarinace (P)
    filter_settings["processNoise_Q"] = Q_discrete_white_noise(3, dt=global_config["DT"], var=1.)  # set proces noise (Q)
    filter_settings["measurementNoise_R_scale"] = measurementNoise  # set measurement noise

elif global_config["FILTER_TYPE"] == 'HYACE_1':
    """Set up a first order Kalman Filter."""
    # Kalman filter settigns for TypeA_1
    Q0 = np.zeros([6, 6])
    Q0[0:3, 0:3] = Q_discrete_white_noise(3, dt=global_config["DT"], var=1.)  # set proces noise (Q)
    Q0[3:6, 3:6] = Q_discrete_white_noise(3, dt=global_config["DT"], var=1.)  # set proces noise (Q)

    print('Q', Q0)
    Q = np.zeros([6, 6])
    filter_settings["x_start"] = np.array([0., 0., 0., 0., 0., 0.])  # starting point for state-space
    filter_settings["covariance_P"] = np.diag([1., 1., 1., 1., 1., 1.])  # set covarinace (P)
    filter_settings["processNoise_Q"] = Q   # set proces noise (Q)
    filter_settings["measurementNoise_R_scale"] = measurementNoise  # set measurement noise

elif global_config["FILTER_TYPE"] == 'HYACE_2':
    """Set up a first order Kalman Filter."""
    # Kalman filter settigns for TypeA_1
    Q = np.zeros([10, 10])
    Q[0:3, 0:3] = Q_discrete_white_noise(3, dt=global_config["DT"], var=1.)  # set proces noise (Q) for Wing
    Q[3:6, 3:6] = Q_discrete_white_noise(3, dt=global_config["DT"], var=1.)  # set proces noise (Q) for Wing
    Q[6:8, 6:8] = Q_discrete_white_noise(2, dt=global_config["DT"], var=1.)  # set proces noise (Q) for Wing
    Q[8:10, 8:10] = Q_discrete_white_noise(2, dt=global_config["DT"], var=1.)  # set proces noise (Q) for Flap

    filter_settings["x_start"] = np.array([0., 0., 0., 0., 0., 0., 0., 0., 0., 0.])  # starting point for state-space
    filter_settings["covariance_P"] = np.diag([1., 1., 1., 1., 1., 1., 1., 1., 1., 1.])*0.1  # set covarinace (P)
    filter_settings["processNoise_Q"] = Q
    filter_settings["measurementNoise_R_scale"] = measurementNoise  # set measurement noise

else:
    print('Error')


# plot_config is a dictionary that defines how output data is plotted.
plot_config = {
#    "figure_list"        : None,  # list that defines
    "figure_list"        : [0, 0, 0, 0, 0, 0, 1, 1, 1, 1, ],  # list that defines
                                    # which figure state variable is associated with.
    "plot_covariance"    : True,  # adds lines for co-variance to plots
    }
