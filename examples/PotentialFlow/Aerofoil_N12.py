"""
Template input file for HX_solver.py
""" 

# set model parameters
"""
Set parameters that define model conditions (optional)
mdata.Pinf     - (defualt = 0 Pa)        sets P-infinity used in calculations
mdata.rho      - (default = 1.225 kg/s)  sets density used for calculations
mdata.Uinf     - (default = np.nan m/s)      sets U-infinity used in calculations. 
                        If NaN this will be calculated automatically. 
"""
mdata.name = 'Vortex + Uniform Flow adjacent to a wall'
mdata.dimensions = 2

# Define the Building Blocks 
"""
The following is a short summary of the supported components. See the User-Guide 
for more detailed instructions and detailed definition.

A = UniformFlow(Vx,Vy,label='UFlow1')
--> creates a uniform with velocity magnitude and direction defined by the 
        x and y components Vx and Vy

B = Vortex(Cx,Cy,K=K,label='Vortex1')
--> creates an irrotational vortex located at (Cx, Cy) with a strength 
        K = Gamma / (2 * pi). A positive Gamma results in a vortex rotating in 
        the anticlockwise direction.

B = Vortex(Cx,Cy,Gamma=Gamma,label='Vortex1')
--> creates an irrotational vortex located at (Cx, Cy) with a strength Gamma. A 
        positive K results in a vortex rotating in the anticlockwise direction.

C = Source(Cx,Cy,m,label='Source1')
--> creates a source/sink located at (Cx,Cy). m is the mass flow rate (per unit 
        depth) coming out of the source. Use +ve m for source and -ve m for sinks

D = Doublet(Cx,Cy,R,Uinf,label='Doublet1')
--> creates a doublet (co-located source and sink) located at (Cx, Cy). R sets 
        the radius of the enclosing streamline that is generated. When used in 
        conjunction with a uniform flow to show the flow around a cylinder, set 
        Ux and Uy to match the x and y components of the uniform flow.

E = User_Defined(Cx,Cy,n,label='user1')
--> Secret. Try it out and see if you can work out what it is.  

"""

# Uniform Flows  
A1 = UniformFlow(10.,0.)

# Solution for N = 12 and AoA = 10 deg
C1 = Vortex(0.089665341913212238, 0.028619883180236935 , Gamma = 6.48963064)
C2 = Vortex(0.074469283588822432,-0.057561246124725324 , Gamma = -3.59793301)
C3 = Vortex(0.25651794236780018 , 0.015092922170340167 , Gamma = 4.58931595)
C4 = Vortex(0.23588593413830383 ,-0.10191701100380533  , Gamma = -3.38555318)
C5 = Vortex(0.42029176724829553 ,-0.015894642779590726 , Gamma = 4.34411552)
C6 = Vortex(0.40038136026187776 ,-0.12881217194285122  , Gamma = -3.57229846)
C7 = Vortex(0.58260375343353232 ,-0.055172706945155317 , Gamma = 4.64736517)
C8 = Vortex(0.5663386250807102  ,-0.1474168336662634   , Gamma = -4.12114305)
C9 = Vortex(0.74402440029256067 ,-0.099505807626556336 , Gamma = 6.04977393)
C10= Vortex(0.7331872292257513  ,-0.16096645887383917  , Gamma = -5.70488093)
C11= Vortex(0.90473600353902528 ,-0.1478600944572053   , Gamma = 17.02879953)
C12= Vortex(0.90074487698335592 ,-0.17049489793216696  , Gamma = -16.83754893)

# Define how the solution will be visualised. 
"""
By use the following settings to adjust the visaulisation.
---- Define plotting Window -----
visual.xmin     - (default = -1.) sets x_min for plots
visual.xmax     - (default =  1.) sets x_max for plots
visual.ymin     - (default = -1.) sets y_min for plots
visual.ymax     - (default =  1.) sets y_max for plots
visual.Nx       - (default = 50) number of points used for discretisation 
                                in x-direction
visual.Ny       - (default = 50) number of points used for discretisation 
                                in x-direction
visual.subplot  - (default = 0)  0 - all individual graphs; 1 - subplots in 
                                single figure 

---- Define what is plotted ----
plot.psi(levels=20) - plots 'real' streamlines, contours of psi. Use levels to 
                        set number of contours.
plot.psi_magU(min=[], max=[], levels=20) - create contour plot of velocity 
                        magnitude with overlaid stream functions. Use min and 
                        max to specify range and levels sets numbers of contours.
plot.vectors()  - plots nice looking streamlines. Note these are not 
                        euqipotentials of psi.
plot.vectors_magU(min=[], max=[], levels=20) - create contour plot of velocity 
                        magnitude with overlaid velocity vectors. Use min and 
                        max to specify range and levels sets numbers of contours.
plot.magU(min=[], max=[], levels=20) - create contour plot of velocity magnitude. 
                        Use min and max to specify range and levels sets numbers 
                        of contours.
plot.U(min=[], max=[], levels=20) - create contour plot of U velocity. Use min 
                        and max to specify range and levels sets numbers of contours.
plot.V(min=[], max=[], levels=20) - create contour plot of V velocity. Use min 
                        and max to specify range and levels sets numbers of contours.
plot.P(P_inf=0., rho=1.225, min=[], max=[], levels=20) - create contour plot of 
                        pressure, using P_inf and rho to perform the calculation. 
                        P = P_inf - 1/2 * rho * magU**2. Use min and max to 
                        specify range and levels sets numbers of contours.
plot.Cp(U_inf=1., rho=1.225, min=[], max=[]) - create contorus of pressure 
                        coefficient Cp, using U_inf and rho to perform the 
                        calculation.
                        Cp = P / ( 1/2 * rho * U_inf**2 )
"""

visual.xmin=-1.
visual.xmax =2.
visual.subplot = 0

plot.psi(levels = 50)
plot.vectors_magU(min=0., max=20.)
plot.P(min=-200, max=0)


# Define what is printed to screen
"""
---- Define what is displayed ----
screen.variables(['Psi','magU','U','V','P','Cp'])  - provide list of parameters that 
                        will be evaluated. (default ['Psi','P', 'magU'])
screen.locations([ [x0,y0], [x1,y1], [x2,y2], ...] ) - provide list of points 
                        where to evaluate data
screen.Lineval([x0,y0], [x1,y1], N=5)     - evaluates conditions at N equally 
                        spaced points between (x0,y0) and (x1,y1)
"""

screen.variables(['Psi', 'U', 'V', 'P']) 
screen.Lineval([-2.0,0.0], [2.0,0.0], N=19) 

