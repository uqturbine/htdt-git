"""
Template input file for HX_solver.py
""" 

# set fluid conditions at heat exchanger inlet and outlet
F.fluidH = 'CO2' 
F.TH_in = 273.15+400
F.mdotH = 10.
F.PH_in = 7.68e6
F.PH_out = 7.68e6

F.fluidC = 'H2O' 
F.TC_in = 273.15+80
F.mdotC = 1.
F.PC_in = 1.e5
F.PC_out = 1.e5

F.T_ext = 295 # External Temperature (K) optional

F.T0 = [ ] 

# set geometry for heat exhanger - required settings depend on type
G.HXtype = 'shell-tube'
G.N_T = 176    # number of tubes
G.d = 8.e-3    # tube inner diameter
G.D = 10.e-3    # tube outer diameter
G.DD = 207.e-3 # shell inner diameter 
G.t_casing = 1.e-6 #
G.HX_L = 10.    # length of HX (m)
G.k_wall = 30 # thermal conductivity (W / m /K)
G.epsilonH = 0. # roughness height for H channel
G.epsilonC = 0. # roughness height for C cahannel

# Set modelling parameters
M.N_cell = 20 # number of cells
M.flag_axial = 0
M.external_loss = 0
M.co_flow = 0

# set correlations for hot channel
C_hot.frictionCorrelation_Gas = None  # set friction factor correlation of gas phase.
C_hot.frictionCorrelation_Liquid = None  # set friction factor correlation of liquid phase.
C_hot.frictionCorrelation_Supercritical = None  # set friction factor correlation of supercritical phase.
C_hot.frictionCorrelation_2phase_condensing = None  # set friction factor correlation for condensing.
C_hot.frictionCorrelation_2phase_boiling = None  # set friction factor correlation for boiling.
C_hot.frictionPolynominal = False  # set pressure drop using a polynominal
C_hot.heatTransferCorrelation_Liquid = 'DITTUS_BOELTER'  # set heat transfer correlation of liquid phase.
C_hot.heatTransferCorrelation_Gas = 'DITTUS_BOELTER'  # set heat transfer correlation of gas phase.
C_hot.heatTransferCorrelation_Supercritical = 'DITTUS_BOELTER'  # set heat transfer correlation of supercritical phase.
C_hot.heatTransferCorrelation_2phase_condensing = None  # set heat transfer correlation for condensing.
C_hot.heatTransferCorrelation_2phase_boiling = None  # set heat transfer correlation for boiling.
C_hot.frictionPolynominal_Coeff = None  # provide List of coefficients for p

# set correlations for cold channel
C_cold.frictionCorrelation_Gas = None  # set friction factor correlation of gas phase.
C_cold.frictionCorrelation_Liquid = None  # set friction factor correlation of liquid phase.
C_cold.frictionCorrelation_Supercritical = None  # set friction factor correlation of supercritical phase.
C_cold.frictionCorrelation_2phase_condensing = None  # set friction factor correlation for condensing.
C_cold.frictionCorrelation_2phase_boiling = None  # set friction factor correlation for boiling.
C_cold.frictionPolynominal = False  # set pressure drop using a polynominal
C_cold.heatTransferCorrelation_Liquid = 'DITTUS_BOELTER'  # set heat transfer correlation of liquid phase.
C_cold.heatTransferCorrelation_Gas = 'DITTUS_BOELTER'  # set heat transfer correlation of gas phase.
C_cold.heatTransferCorrelation_Supercritical = 'YOON_HORIZONTAL'  # set heat transfer correlation of supercritical phase.
C_cold.heatTransferCorrelation_2phase_condensing = 'SHAH_VERTICAL'  # set heat transfer correlation for condensing.
C_cold.heatTransferCorrelation_2phase_boiling = 'BOIL_WATER'  # set heat transfer correlation for boiling.
C_cold.frictionPolynominal_Coeff = None  # provide List of coefficients for p
