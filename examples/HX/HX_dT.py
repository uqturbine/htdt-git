"""
Template input file for HX_solver.py
""" 

# set fluid conditions at heat exchanger inlet and outlet
F.fluidH = 'Air' 
F.TH_in = 580.+273.15  # Inlet temperature of H stream (K)
F.mdotH = 1.    # mass flow rate of H stream (kg/s)
F.PH_in = 1e5   # Inlet pressure of H stream (Pa)
F.PH_out = 1e5  # Inlet pressure of H stream (Pa)
F.fluidC = 'CO2' 
F.TC_in = 25.+273.15  # Inlet temperature of C stream (K)
F.mdotC = 1.    # mass flow rate of C stream (kg/s)
F.PC_in = 20.e6 # Inlet pressure of C stream (Pa)
F.PC_out = 20.e6# Inlet pressure of C stream (Pa)
F.T_ext = 295 # External Temperature (K) optional

F.T0 = [ ] 

# set geometry for heat exhanger - required settings depend on type
G.HXtype = 'pinch_dT'
G.dT = 5.

# Set modelling parameters
M.N_cell = 10 # number of cells
M.flag_axial = 0
M.external_loss = 0
M.co_flow = 0

