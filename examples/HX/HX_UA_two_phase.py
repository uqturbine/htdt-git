"""
Template input file for HX_solver.py
""" 

# set fluid conditions at heat exchanger inlet and outlet
F.fluidH = 'H2O' 
F.TH_in = 580.+273.15  # Inlet temperature of H stream (K)
F.mdotH = 4.    # mass flow rate of H stream (kg/s)
F.PH_in = 1e5   # Inlet pressure of H stream (Pa)
F.PH_out = 1e5  # Inlet pressure of H stream (Pa)

F.fluidC = 'H2O' 
F.TC_in = 25.+273.15  # Inlet temperature of C stream (K)
F.mdotC = 1.    # mass flow rate of C stream (kg/s)
F.PC_in = 1e5 # Inlet pressure of C stream (Pa)
F.PC_out = 1e5# Inlet pressure of C stream (Pa)

F.T_ext = 295 # External Temperature (K) optional

F.T0 = [ ] 

# set geometry for heat exhanger - required settings depend on type
G.HXtype = 'UA'
G.U=20000.          # Overall Heat Transfer Coefficient (W/m2/K)
G.Area=1.           # Effective Heat Transfer Area (m2)
G.HX_L = 2*0.620    # length of HX (m)
G.AH = 0.    # Total Cross-section of H channel (m2)
G.AC = 0.     # Total Cross-section of C channel (m2)


# Set modelling parameters
M.N_cell = 40 # number of cells
M.flag_axial = 0
M.external_loss = 0
M.co_flow = 0

