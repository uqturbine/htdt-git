"""
Template input file for HX_solver.py

Simulation is for a steam-water heat exchanger. 
The hot channel contains high temperature steam, 
which is cooled down and heat, evaporates, and 
superheats water in the cold channel. 
This demonstrates the ability to correctly simulate 
heat transfer to a medium undergoing phase change.

The modelling approach for the current application 
is to prescribe a fixed minimum approach temperature 
difference between the hot and cold stream. 

Author: Ingo Jahn
Last Modified: 2018/06/29
""" 

# set fluid conditions at heat exchanger inlet and outlet
F.fluidH = 'H2O' 
F.TH_in = 580.+273.15  # Inlet temperature of H stream (K)
F.mdotH = 4.    # mass flow rate of H stream (kg/s)
F.PH_in = 1e5   # Inlet pressure of H stream (Pa)
F.PH_out = 1e5  # Inlet pressure of H stream (Pa)

F.fluidC = 'H2O' 
F.TC_in = 25.+273.15  # Inlet temperature of C stream (K)
F.mdotC = 1.    # mass flow rate of C stream (kg/s)
F.PC_in = 1e5   # Inlet pressure of C stream (Pa)
F.PC_out = 1e5  # Inlet pressure of C stream (Pa)

F.T_ext = 295 # External Temperature (K) optional

F.T0 = [ ] 

# set geometry for heat exchanger - required settings depend on type
G.HXtype = 'pinch_dT'
G.dT = 10.

# Set modelling parameters
M.N_cell = 20 # number of cells
M.flag_axial = 0
M.external_loss = 0
M.co_flow = 0

