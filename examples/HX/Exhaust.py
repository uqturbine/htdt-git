"""
Teplate file containg properties of a custom gas defined by interploatable tables
Below data corresponds to an air like exhaust gas. 
"""
# Optionaly use the below to set a custom table based gas model. 
# This will be called by setting F.FluidH or F.FLuidC = "CUSTOM"
# And also setting the variable F.fluidPropFileH = "Exhaust.py"

self.temperature   = np.array([274.15, 283.15, 293.15, 303.15, 313.15, 
            323.15, 328.15, 331.15, 331.65, 332.15, 333.15, 343.15, 353.15, 363.15, 373.15,
            423.15, 473.15, 523.15, 573.15, 623.15, 673.15, 723.15, 773.15, 823.15, 873.15]) #[K]
self.quality       = np.array([ -1., -1., -1., -1., -1, -1., -1., -1., -1., -1, 
            -1., -1., -1., -1., -1, -1., -1., -1., -1., -1, -1., -1., -1., -1., -1 ]) # [-]
self.density       = np.array([1.500784911, 1.443916132, 1.377733078, 1.304505501, 1.218972577,
            1.114381201, 1.052372363, 1.025839881, 1.018978474, 1.016735466, 1.013658028, 0.98388398, 0.955840184, 0.929359393, 0.904314484,
            0.797054088, 0.712616156, 0.644392177, 0.588110753, 0.540882612, 0.500680926, 0.466048376, 0.435898889, 0.409414905, 0.385967756]) #[kg/m3]
self.specific_heat = np.array([0.0, 0.070881158, 0.166297118, 0.291967673, 0.468689127, 
            0.731796379, 0.914734115, 1.048044693, 1.071641791, 1.075598374, 1.075761669, 1.077079994, 1.078380292, 1.079608977, 1.080798606, 
            1.086635945, 1.092444257, 1.098461244, 1.104684638, 1.111144989, 1.117804353, 1.124607619, 1.131539805, 1.138565268, 1.145645078])*1e3 #[J/kg/K]
self.enthalpy = np.array([ 0.00, 20.07, 48.75, 88.51, 146.77,
            236.48, 300.17, 347.06, 355.41, 357.26, 358.39, 369.6, 380.83, 392.06, 403.3,
            459.81, 516.89, 574.66, 633.15, 692.41, 752.45, 813.26, 874.85, 937.21, 1000.32])*1e3 #[J/kg]
self.conductivity  = np.array([ 0.024, 0.024, 0.024, 0.024, 0.024, 
            0.024, 0.024, 0.024, 0.024, 0.024, 0.024, 0.024, 0.024, 0.024, 0.024, 
            0.024, 0.024, 0.024, 0.024, 0.024, 0.024, 0.024, 0.024, 0.024, 0.024, ]) #[W/m/K]
self.viscosity     = np.array([ 1e-4, 1e-4, 1e-4, 1e-4, 1e-4, 
            1e-4, 1e-4, 1e-4, 1e-4, 1e-4, 1e-4, 1e-4, 1e-4, 1e-4, 1e-4, 
            1e-4, 1e-4, 1e-4, 1e-4, 1e-4, 1e-4, 1e-4, 1e-4, 1e-4, 1e-4,])
self.prandtl_number = np.array([ 1., 1., 1., 1., 1., 
            1., 1., 1., 1., 1., 1., 1., 1., 1., 1., 
            1., 1., 1., 1., 1., 1., 1., 1., 1., 1.,]) 
self.saturation_pressure   = np.array([ 2.e6, 2.e6, 2.e6, 2.e6, 2.e6, 
             2.e6, 2.e6, 2.e6, 2.e6, 2.e6, 2.e6, 2.e6, 2.e6, 2.e6, 2.e6,
             2.e6, 2.e6, 2.e6, 2.e6, 2.e6, 2.e6, 2.e6, 2.e6, 2.e6, 2.e6]) #[Pa]
self.Tmin        = np.min(self.temperature)
self.Tmax        = np.max(self.temperature)
self.TminPsat    = np.min(self.temperature[~np.isnan(self.saturation_pressure)])
self.name        = "Exhaust"
self.description = "Test"
self.reference   = "Construction Ingo"


