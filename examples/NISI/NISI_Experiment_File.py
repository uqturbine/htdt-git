"""Input settings for NISI_Shaft a program to calculate thermal response."""

# Geometric and Physical settings
geom.r_cylinder = 0.0125        # Lower radial bound (m)
geom.rho = 7990.           # Desnity (kg/m3)
geom.Cp = 500.         # Thermal capacity (J / kg / K)
geom.kappa = 16.0          # Thermal conductivity of the rod (W /m /K)

# Model Settings
gdata.final_time = 5.             # Final time (s)
gdata.time_step = 0.01 #0.015          # Change in time (s)
gdata.n_bessel = 10                   # number of Bessel coefficients used.
gdata.time_solutions = [0., 0.001, 0.01, 0.02, 1., 2., 3., 4., 5.]    # plots spatial variation in temperature at discrete times specified in list (s)
gdata.space_solutions = [0.0, 0.005, 0.0075, 0.01, 0.0125] # plots temporal variation in temperature at discrete points in space (m)
#gdata.space_solutions = np.linspace(0.0, geom.r_cylinder, 10) # code to generate 
gdata.validation_heat_in = 1.0  # maximum heat input value for verification tests
gdata.validation_flag = True  # turns on verification part of codes
gdata.validation_function = "unit step"  # selects different validation cases. Follwowing option 1:"unit step", 2:"sin wave"; 3:"ramp", 4:"ramp + sine wave"
gdata.unit_step = False # perform a step response verification of impulse method with the analytical equation T(r,t) 
gdata.surface_plots = False


# Intial Condtions
initial.T_init = 0 #300           # Initial temperature (K)

#experiment information
exp.cam_file = "C:\working\QGECE36_HX_Tests\data_csvs\\QGECE36_TCP_23012019-8_Cam"
exp.crio_file = "C:\working\QGECE36_HX_Tests\data_csvs\\QGECE36_TCP_23012019-8_CRIO"
