#!/usr/bin/tclsh
# eilmer3-test.tcl
#
# Smoke test the Eilmer3 code.
#
# PJ, 11-Jan-2011, 12-Jul-2011
#
# Presently, we work through the specified directories and explicitly invoke 
# test scripts.  Of course, there must be a better way to do this using the 
# tcltest module.
# Short tests are those which may take up to 20 minutes on my workstation.
# I don't want to wait more than an hour, or so, for the set of short tests
# to run.
#
# Usage:
# 1. ./all-test.tcl
# 2. tclsh all-test.tcl
# 3. tclsh all-test.tcl --long-tests
# 4. ./all-test.tcl --dummy-run

set long_tests 0
set dummy_run 0
for {set i 0} {$i < $argc} {incr i} {
    set arg [lindex $argv $i]
    if {[string first "long" $arg] >= 0} {
        set long_tests 1
    }
    if {[string first "dummy" $arg] >= 0} {
        set dummy_run 1
    }
}
set test_scripts [list "HX/HX.test"]
lappend test_scripts "PotentialFlow/PotentialFlow.test"
lappend test_scripts "SSCAR/fixed_component_efficiencies/fixed_eta.test"
lappend test_scripts "SSCAR/CSP/CSP.test"
lappend test_scripts "SSCAR/ACM/ACM.test"
lappend test_scripts "SSCAR/ICE/ICE.test"
lappend test_scripts "SSCAR/map_components/map.test"


if {$long_tests} {
    puts "Do long tests as well as short tests..."
    puts "Currently no long tests implemented."
    #lappend test_scripts "dir/dir/Name.test"

} else {
    puts "Do short tests only..."
}
set original_dir [pwd]
foreach test_script $test_scripts {
    cd [file dir $test_script]
    puts "[exec date] [pwd]"
    if { !$dummy_run } {
        source [file tail $test_script]
    }
    cd $original_dir
}
puts "[exec date] Finished tests."
