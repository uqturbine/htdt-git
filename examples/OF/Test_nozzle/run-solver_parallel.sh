#!/bin/bash

# split file into multiple subdomains
decomposePar -force

# run parallel openfoam case
mpirun -np 4 rhoPimpleFoam -parallel > log_mpi

# reconstruct Mesh
reconstructPar

#delete parallel folder
rm -r processor*

