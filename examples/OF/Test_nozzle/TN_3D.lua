-- 3D 90 deg Mesh for test section Nozzle 
-- Author: Joshua Keep
-- last modified: 11/2019
--#########################################
--# Define Sectional Geometry                
--#########################################

--              --+-----+
-- +------+----/  |     |
-- |  B   |  C    | E   |
-- b------c-------d-----e

OP = {0.9, 0.9,0.5, 0.05, 0.5, 0.45, 0.35, 0.6, 1.687}
optim = 1 -- set to 1 for running as part of optimiser, if not running with optimiser, above values will be used.

-- ############################
-- Set geometric parameters
-- ############################

-- Merge section
aspect=OP[1]  
contraction=OP[2]
l1r=OP[3] -- length ratio

-- diverging section control points
d0_xf = OP[4]
d1_xf = OP[5]
d1_rf = OP[6]
d2_xf = OP[7]
l2r= OP[8] --length ratio


-- Merge section geom calcs
Ri = 25.e-3  -- inlet radius (circular cross section)
Area1=math.pi*Ri^2
Area2=Area1*contraction
Rt=math.sqrt(Area2/(4*aspect)) -- half throat height (rectangular cross section)
w=Rt*aspect -- half test section depth
Ro=Rt*OP[9] -- half test section exit height


-- section lenghts
l_in_max=200.e-3
l_in=l1r*l_in_max

l_div_max=800.e-3
l_div=l2r*l_div_max

l_out=200.e-3 --fixed, check solution quality

-- Axial coordinates - zero set as exit
z4=0.0
z3 = z4-l_out
z2=z3-l_div
z1=z2-l_in

-- ############################
-- Define faces to be extruded
-- ############################

--     7
--     |
--     4 -
--     |   \       +8
--     |      \
--     |      5
--     |     /  \
--     1----2    \   
--     |    |     \
--     |    |      \
--     0----3------6--9
--       n_a  n_c   n_b
-- ############################
-- define end face spacing
-- ############################
bl=0.8 -- boundary layer fraction from origin
cl=0.4 --core layer fraction from origin
-- ############################
-- Define block nodes
-- ############################

b0 = Vector3:new{x=0.0, y=0.0, z=z1}
b1 = Vector3:new{x=0.0, y=cl*Ri, z=z1}
b2 = Vector3:new{x=cl*Ri, y=cl*Ri, z=z1}
b3 = Vector3:new{x=cl*Ri, y=0.0, z=z1}
b4 = Vector3:new{x=0.0, y=bl*Ri, z=z1}
b5 = Vector3:new{x=bl*Ri/math.sqrt(2), y=bl*Ri/math.sqrt(2), z=z1}
b6 = Vector3:new{x=bl*Ri, y=0.0, z=z1}
b7 = Vector3:new{x=0.0, y=Ri, z=z1}
b8 = Vector3:new{x=Ri/math.sqrt(2), y=Ri/math.sqrt(2), z=z1}
b9 = Vector3:new{x=Ri, y=0.0, z=z1}

c0 = Vector3:new{x=0.0, y=0.0, z=z2}
c1 = Vector3:new{x=0.0, y=cl*w, z=z2}
c2 = Vector3:new{x=cl*Rt, y=cl*w, z=z2}
c3 = Vector3:new{x=cl*Rt, y=0.0, z=z2}
c4 = Vector3:new{x=0.0, y=bl*w, z=z2}
c5 = Vector3:new{x=bl*Rt, y=bl*w, z=z2}
c6 = Vector3:new{x=bl*Rt, y=0.0, z=z2}
c7 = Vector3:new{x=0.0, y=w, z=z2}
c8 = Vector3:new{x=Rt, y=w, z=z2}
c9 = Vector3:new{x=Rt, y=0.0, z=z2}

d0 = Vector3:new{x=0.0, y=0.0, z=z3}
d1 = Vector3:new{x=0.0, y=cl*w, z=z3}
d2 = Vector3:new{x=cl*Ro, y=cl*w, z=z3}
d3 = Vector3:new{x=cl*Ro, y=0.0, z=z3}
d4 = Vector3:new{x=0.0, y=bl*w, z=z3}
d5 = Vector3:new{x=bl*Ro, y=bl*w, z=z3}
d6 = Vector3:new{x=bl*Ro, y=0.0, z=z3}
d7 = Vector3:new{x=0.0, y=w, z=z3}
d8 = Vector3:new{x=Ro, y=w, z=z3}
d9 = Vector3:new{x=Ro, y=0.0, z=z3}

e0 = Vector3:new{x=0.0, y=0.0, z=z4}
e1 = Vector3:new{x=0.0, y=cl*w, z=z4}
e2 = Vector3:new{x=cl*Ro, y=cl*w, z=z4}
e3 = Vector3:new{x=cl*Ro, y=0.0, z=z4}
e4 = Vector3:new{x=0.0, y=bl*w, z=z4}
e5 = Vector3:new{x=bl*Ro, y=bl*w, z=z4}
e6 = Vector3:new{x=bl*Ro, y=0.0, z=z4}
e7 = Vector3:new{x=0.0, y=w, z=z4}
e8 = Vector3:new{x=Ro, y=w, z=z4}
e9 = Vector3:new{x=Ro, y=0.0, z=z4}

-- ############################
-- Define lines linking nodes
-- ############################

b45=Arc:new{p0=b4, p1=b5, centre=b0}
b56=Arc:new{p0=b6, p1=b5, centre=b0}
b78=Arc:new{p0=b7, p1=b8, centre=b0}
b89=Arc:new{p0=b9, p1=b8, centre=b0}

c45=Line:new{p0=c4, p1=c5}
c56=Line:new{p0=c6, p1=c5}
c78=Line:new{p0=c7, p1=c8}
c89=Line:new{p0=c9, p1=c8}

d45=Line:new{p0=d4, p1=d5}
d56=Line:new{p0=d6, p1=d5}
d78=Line:new{p0=d7, p1=d8}
d89=Line:new{p0=d9, p1=d8}

e45=Line:new{p0=e4, p1=e5}
e56=Line:new{p0=e6, p1=e5}
e78=Line:new{p0=e7, p1=e8}
e89=Line:new{p0=e9, p1=e8}

-- External section curve
cp3 = Vector3:new{z=((1-d0_xf)*z2+d0_xf*z3), x=Rt, y=0.0}
cp4 = Vector3:new{z=((1-d1_xf)*z2+d1_xf*z3), x=((1-d1_rf)*Rt+d1_rf*Ro), y=0.0}
cp5 = Vector3:new{z=((1-d2_xf)*z2+d2_xf*z3), x=Ro, y=0.0}
div = Bezier:new{points={c9,cp3,cp4,cp5,d9}}


-- ############################
-- Define surfaces
-- ############################
surf = {}
surf[10] = CoonsPatch:new{p00=b0, p10=b3, p11=b2, p01=b1}
surf[11] = CoonsPatch:new{north=b45, south=Line:new{p0=b1, p1=b2}, west=Line:new{p0=b1, p1=b4}, east=Line:new{p0=b2, p1=b5}}
surf[12] = CoonsPatch:new{north=Line:new{p0=b2, p1=b5}, south=Line:new{p0=b3, p1=b6}, west=Line:new{p0=b3, p1=b2}, east=b56}
surf[13] = CoonsPatch:new{north=b78, south=b45, west=Line:new{p0=b4, p1=b7}, east=Line:new{p0=b5, p1=b8}}
surf[14] = CoonsPatch:new{north=Line:new{p0=b5, p1=b8}, south=Line:new{p0=b6, p1=b9}, west=b56, east=b89}

surf[20] = CoonsPatch:new{p00=c0, p10=c3, p11=c2, p01=c1}
surf[21] = CoonsPatch:new{north=c45, south=Line:new{p0=c1, p1=c2}, west=Line:new{p0=c1, p1=c4}, east=Line:new{p0=c2, p1=c5}}
surf[22] = CoonsPatch:new{north=Line:new{p0=c2, p1=c5}, south=Line:new{p0=c3, p1=c6}, west=Line:new{p0=c3, p1=c2}, east=c56}
surf[23] = CoonsPatch:new{north=c78, south=c45, west=Line:new{p0=c4, p1=c7}, east=Line:new{p0=c5, p1=c8}}
surf[24] = CoonsPatch:new{north=Line:new{p0=c5, p1=c8}, south=Line:new{p0=c6, p1=c9}, west=c56, east=c89}

surf[30] = CoonsPatch:new{p00=d0, p10=d3, p11=d2, p01=d1}
surf[31] = CoonsPatch:new{north=d45, south=Line:new{p0=d1, p1=d2}, west=Line:new{p0=d1, p1=d4}, east=Line:new{p0=d2, p1=d5}}
surf[32] = CoonsPatch:new{north=Line:new{p0=d2, p1=d5}, south=Line:new{p0=d3, p1=d6}, west=Line:new{p0=d3, p1=d2}, east=d56}
surf[33] = CoonsPatch:new{north=d78, south=d45, west=Line:new{p0=d4, p1=d7}, east=Line:new{p0=d5, p1=d8}}
surf[34] = CoonsPatch:new{north=Line:new{p0=d5, p1=d8}, south=Line:new{p0=d6, p1=d9}, west=d56, east=d89}

surf[40] = CoonsPatch:new{p00=e0, p10=e3, p11=e2, p01=e1}
surf[41] = CoonsPatch:new{north=e45, south=Line:new{p0=e1, p1=e2}, west=Line:new{p0=e1, p1=e4}, east=Line:new{p0=e2, p1=e5}}
surf[42] = CoonsPatch:new{north=Line:new{p0=e2, p1=e5}, south=Line:new{p0=e3, p1=e6}, west=Line:new{p0=e3, p1=e2}, east=e56}
surf[43] = CoonsPatch:new{north=e78, south=e45, west=Line:new{p0=e4, p1=e7}, east=Line:new{p0=e5, p1=e8}}
surf[44] = CoonsPatch:new{north=Line:new{p0=e5, p1=e8}, south=Line:new{p0=e6, p1=e9}, west=e56, east=e89}

function funB0(r, s, t)
    low = surf[10]:eval(r, s)
    high = surf[20]:eval(r, s)
    return {x=(1-t)*low.x + t*high.x, y=(1-t)*low.y + t*high.y , z=(1-t)*low.z + t*high.z}
end

function funB1(r, s, t)
    low = surf[11]:eval(r, s)
    high = surf[21]:eval(r, s)
    return {x=(1-t)*low.x + t*high.x, y=(1-t)*low.y + t*high.y , z=(1-t)*low.z + t*high.z}
end

function funB2(r, s, t)
    low = surf[12]:eval(r, s)
    high = surf[22]:eval(r, s)
    return {x=(1-t)*low.x + t*high.x, y=(1-t)*low.y + t*high.y , z=(1-t)*low.z + t*high.z}
end

function funB3(r, s, t)
    low = surf[13]:eval(r, s)
    high = surf[23]:eval(r, s)
    return {x=(1-t)*low.x + t*high.x, y=(1-t)*low.y + t*high.y , z=(1-t)*low.z + t*high.z}
end

function funB4(r, s, t)
    low = surf[14]:eval(r, s)
    high = surf[24]:eval(r, s)
    return {x=(1-t)*low.x + t*high.x, y=(1-t)*low.y + t*high.y , z=(1-t)*low.z + t*high.z}
end

function funC0(r, s, t)
    low = surf[20]:eval(r, s)
    high = surf[30]:eval(r, s)
    temp=div:eval(t)
    R=(Ro-temp.x)/(Ro-Rt)-- normalised x component of bezier
    return {x=(R)*low.x + (1-R)*high.x, y=(R)*low.y + (1-R)*high.y , z=(1-t)*low.z + t*high.z}
end

function funC1(r, s, t)
    low = surf[21]:eval(r, s)
    high = surf[31]:eval(r, s)
    temp=div:eval(t)
    R=(Ro-temp.x)/(Ro-Rt)-- normalised x component of bezier
    return {x=(R)*low.x + (1-R)*high.x, y=(R)*low.y + (1-R)*high.y , z=(1-t)*low.z + t*high.z}
end

function funC2(r, s, t)
    low = surf[22]:eval(r, s)
    high = surf[32]:eval(r, s)
    temp=div:eval(t)
    R=(Ro-temp.x)/(Ro-Rt)-- normalised x component of bezier
    return {x=(R)*low.x + (1-R)*high.x, y=(R)*low.y + (1-R)*high.y , z=(1-t)*low.z + t*high.z}
end

function funC3(r, s, t)
    low = surf[23]:eval(r, s)
    high = surf[33]:eval(r, s)
    temp=div:eval(t)
    R=(Ro-temp.x)/(Ro-Rt)-- normalised x component of bezier
    return {x=(R)*low.x + (1-R)*high.x, y=(R)*low.y + (1-R)*high.y , z=(1-t)*low.z + t*high.z}
end

function funC4(r, s, t)
    low = surf[24]:eval(r, s)
    high = surf[34]:eval(r, s)
    temp=div:eval(t)
    R=(Ro-temp.x)/(Ro-Rt)-- normalised x component of bezier
    return {x=(R)*low.x + (1-R)*high.x, y=(R)*low.y + (1-R)*high.y , z=(1-t)*low.z + t*high.z}
end


function funD0(r, s, t)
    low = surf[30]:eval(r, s)
    high = surf[40]:eval(r, s)
    return {x=(1-t)*low.x + t*high.x, y=(1-t)*low.y + t*high.y , z=(1-t)*low.z + t*high.z}
end

function funD1(r, s, t)
    low = surf[31]:eval(r, s)
    high = surf[41]:eval(r, s)
    return {x=(1-t)*low.x + t*high.x, y=(1-t)*low.y + t*high.y , z=(1-t)*low.z + t*high.z}
end

function funD2(r, s, t)
    low = surf[32]:eval(r, s)
    high = surf[42]:eval(r, s)
    return {x=(1-t)*low.x + t*high.x, y=(1-t)*low.y + t*high.y , z=(1-t)*low.z + t*high.z}
end

function funD3(r, s, t)
    low = surf[33]:eval(r, s)
    high = surf[43]:eval(r, s)
    return {x=(1-t)*low.x + t*high.x, y=(1-t)*low.y + t*high.y , z=(1-t)*low.z + t*high.z}
end

function funD4(r, s, t)
    low = surf[34]:eval(r, s)
    high = surf[44]:eval(r, s)
    return {x=(1-t)*low.x + t*high.x, y=(1-t)*low.y + t*high.y , z=(1-t)*low.z + t*high.z}
end


-- ############################
-- Define volumes
-- ############################
volume = {}

volume[10] = LuaFnVolume:new{luaFnName="funB0"}
volume[11] = LuaFnVolume:new{luaFnName="funB1"}
volume[12] = LuaFnVolume:new{luaFnName="funB2"}
volume[13] = LuaFnVolume:new{luaFnName="funB3"}
volume[14] = LuaFnVolume:new{luaFnName="funB4"}

volume[20] = LuaFnVolume:new{luaFnName="funC0"}
volume[21] = LuaFnVolume:new{luaFnName="funC1"}
volume[22] = LuaFnVolume:new{luaFnName="funC2"}
volume[23] = LuaFnVolume:new{luaFnName="funC3"}
volume[24] = LuaFnVolume:new{luaFnName="funC4"}

volume[40] = LuaFnVolume:new{luaFnName="funD0"}
volume[41] = LuaFnVolume:new{luaFnName="funD1"}
volume[42] = LuaFnVolume:new{luaFnName="funD2"}
volume[43] = LuaFnVolume:new{luaFnName="funD3"}
volume[44] = LuaFnVolume:new{luaFnName="funD4"}

-- ############################
-- Define clustering and numbers
-- ############################

N_refine = 0.2

n_a=15; n_c=15; n_b=20; nB=80; nC=250; nE=100

n_a = math.ceil(n_a*N_refine)
n_b = math.ceil(n_b*N_refine)
n_c = math.ceil(n_c*N_refine)
nB = math.ceil(nB*N_refine)
nC = math.ceil(nC*N_refine)
nE = math.ceil(nE*N_refine)

-- b = bl, a = horiz distance
c_bl_b = RobertsFunction:new{end0=false, end1=true, beta=1.03}
c_bl_c = RobertsFunction:new{end0=false, end1=true, beta=1.03}
c_bl_d = RobertsFunction:new{end0=false, end1=true, beta=1.02}
c_bl_f = RobertsFunction:new{end0=false, end1=true, beta=1.1}

-- horizontal spacing
chB = RobertsFunction:new{end0=false, end1=true, beta=10.0}
chE = nil --RobertsFunction:new{end0=true, end1=false, beta=1.4}

-- ############################
-- Define grids
-- ############################
grid = {}
grid[10] = StructuredGrid:new{pvolume=volume[10], niv=n_a, njv=n_a, nkv = nB,cfList={edge04=chB,edge15=chB,edge26=chB,edge37=chB} }
grid[11] = StructuredGrid:new{pvolume=volume[11], niv=n_a, njv=n_c, nkv = nB,cfList={edge04=chB,edge15=chB,edge26=chB,edge37=chB} }
grid[12] = StructuredGrid:new{pvolume=volume[12], niv=n_c, njv=n_a, nkv = nB,cfList={edge04=chB,edge15=chB,edge26=chB,edge37=chB} }
grid[13] = StructuredGrid:new{pvolume=volume[13], niv=n_a, njv=n_b, nkv = nB,cfList={edge03=c_bl_b,edge12=c_bl_b, edge56=c_bl_c,edge47=c_bl_c,edge04=chB,edge15=chB,edge26=chB,edge37=chB} }
grid[14] = StructuredGrid:new{pvolume=volume[14], niv=n_b, njv=n_a, nkv = nB,cfList={edge01=c_bl_b,edge32=c_bl_b, edge45=c_bl_c, edge76=c_bl_c,edge04=chB,edge15=chB,edge26=chB,edge37=chB} }

grid[20] = StructuredGrid:new{pvolume=volume[20], niv=n_a, njv=n_a, nkv = nC,cfList={edge04=chC,edge15=chC,edge26=chC,edge37=chC} }
grid[21] = StructuredGrid:new{pvolume=volume[21], niv=n_a, njv=n_c, nkv = nC,cfList={edge04=chC,edge15=chC,edge26=chC,edge37=chC} }
grid[22] = StructuredGrid:new{pvolume=volume[22], niv=n_c, njv=n_a, nkv = nC,cfList={edge04=chC,edge15=chC,edge26=chC,edge37=chC} }
grid[23] = StructuredGrid:new{pvolume=volume[23], niv=n_a, njv=n_b, nkv = nC,cfList={edge03=c_bl_c,edge12=c_bl_c, edge56=c_bl_d,edge47=c_bl_d,edge04=chC,edge15=chC,edge26=chC,edge37=chC} }
grid[24] = StructuredGrid:new{pvolume=volume[24], niv=n_b, njv=n_a, nkv = nC,cfList={edge01=c_bl_c,edge32=c_bl_c, edge45=c_bl_d, edge76=c_bl_d,edge04=chC,edge15=chC,edge26=chC,edge37=chC} }

grid[40] = StructuredGrid:new{pvolume=volume[40], niv=n_a, njv=n_a, nkv = nE,cfList={edge04=chE,edge15=chE,edge26=chE,edge37=chE} }
grid[41] = StructuredGrid:new{pvolume=volume[41], niv=n_a, njv=n_c, nkv = nE,cfList={edge04=chE,edge15=chE,edge26=chE,edge37=chE} }
grid[42] = StructuredGrid:new{pvolume=volume[42], niv=n_c, njv=n_a, nkv = nE,cfList={edge04=chE,edge15=chE,edge26=chE,edge37=chE} }
grid[43] = StructuredGrid:new{pvolume=volume[43], niv=n_a, njv=n_b, nkv = nE,cfList={edge03=c_bl_d,edge12=c_bl_d, edge56=c_bl_f,edge47=c_bl_f,edge04=chE,edge15=chE,edge26=chE,edge37=chE} }
grid[44] = StructuredGrid:new{pvolume=volume[44], niv=n_b, njv=n_a, nkv = nE,cfList={edge01=c_bl_d,edge32=c_bl_d, edge45=c_bl_f, edge76=c_bl_f,edge04=chE,edge15=chE,edge26=chE,edge37=chE} }

-- ############################
-- Define blocks and boundaries
-- ############################
block = {}
block[10] = FoamBlock:new{grid=grid[10],bndry_labels={bottom="i-00", south="w-01",west="w-01"}}
block[11] = FoamBlock:new{grid=grid[11],bndry_labels={bottom="i-00",west="w-01"}}
block[12] = FoamBlock:new{grid=grid[12],bndry_labels={bottom="i-00",south="w-01"}}		     
block[13] = FoamBlock:new{grid=grid[13],bndry_labels={bottom="i-00",west="w-01",north="w-00"}}
block[14] = FoamBlock:new{grid=grid[14],bndry_labels={bottom="i-00",south="w-01", east="w-00"}}

block[20] = FoamBlock:new{grid=grid[20],bndry_labels={ south="w-01",west="w-01"}}
block[21] = FoamBlock:new{grid=grid[21],bndry_labels={west="w-01"}}
block[22] = FoamBlock:new{grid=grid[22],bndry_labels={south="w-01"}}		     
block[23] = FoamBlock:new{grid=grid[23],bndry_labels={ west="w-01",north="w-00"}}
block[24] = FoamBlock:new{grid=grid[24],bndry_labels={south="w-01", east="w-00"}}

block[40] = FoamBlock:new{grid=grid[40],bndry_labels={top="o-00",south="w-01",west="w-01"}}
block[41] = FoamBlock:new{grid=grid[41],bndry_labels={top="o-00",west="w-01"}}
block[42] = FoamBlock:new{grid=grid[42],bndry_labels={top="o-00",south="w-01"}}		     
block[43] = FoamBlock:new{grid=grid[43],bndry_labels={top="o-00",west="w-01",north="w-00"}}
block[44] = FoamBlock:new{grid=grid[44],bndry_labels={top="o-00",south="w-01", east="w-00"}}

