# Test job file for AxTurb
#
# Author: Ingo Jahn
# Last Modified: 16/05/2018


# +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# adjust the global settings for the solver. Uncomment and adjust the following lines as appropriate.
#gdata.passages = 1 # (defualt = 1) discretisations of the passage in radial tracks
gdata.name = 'One_and_half_stage'
#gdata.iter = 0  # (default = 1) integer to set number of iterations for fsolve
#gdata.verbosity = 0 # (defualt = 1) adjust how much data will be printed to display (for debugging). Will be overwritten from call
#gdata.optim = 'root:hybr' # (default = 'root:hybr') select optimiser that will be used. 
#gdata.flowsolver_maxiter = 100. # (default = 100) sets maximum number of iterations used by flow solver
#gdata.flowsolver_tol = 1.5e-8 # (default = 1.5e-8) set convergence tolerance for root finder


# +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# adjust gas model - default is ideal gas model for air with gamma = 1.4
# the following lines can be un-commented and edited to over-write the default settings for the ideal gas model 
#fluid.type = "Ideal Gas"            # Currently only "Ideal Gas" implemented
#fluid.name = 'Air'      # set name of fluid
#fluid.Cp = 1005         # (J /kg /K) (default = 1005) specific heat capacity at constant pressure
#fluid.gamma = 1.3       # (-) (default = 1.4) ratio of specific heats
#fluid.Cv = self.Cp / self.gamma  # (J /kg /K) (default = calculated) specific heat capacity at constant volume 
#fluid.R = self.Cp-self.Cv        # (J /kg /K) (default = calculated) fluid specific cas constant
#fluid.T_ref = 273.15    # (K) reference temperature used to calculate enthalpy and entropy
#fluid.P_ref = 101530.   # (Pa) reference pressure used to calculate enthalpy and entropy
#fluid.viscosity_type = "Sutherland"  # Currently only "Sutherland" law implemented
#fluid.mu_ref = 1.716e-5  # (kg /m /s)  Reference viscosity for Sutherlands law
#fluid.mu_T_ref = 273.15  # (K) Reference temperature for Sutherlands law
#fluid.mu_S = 110.4       # (K) Sutherland Temperature

# set conditions that will be investigated
cond.PT_in = 1e5     # (Pa) Total Pressure upstream of cascade inlet
cond.TT_in = 320.    # (K) Total Temperature upstream of cascade inlet
cond.P_out_target = 0.95e5   # (Pa) Target outlet pressure
cond.mdot0 = 0.5       # (kg/s) intial estimate of mass flow through machine. 

# set loss models
loss.statorIncidentType = "None" # None Zehner_1980 Kacker_Okapuu_1982
loss.statorPassageType = "None" # None Zehner_1980 Kacker_Okapuu_1982
loss.statorTrailingType = "None"
loss.rotorIncidentType = "None" # None Zehner_1980 Kacker_Okapuu_198
loss.rotorPassageType = "None"  # None Zehner_1980 Kacker_Okapuu_1982
loss.rotorTrailingType = "None"
loss.rotorTipClearanceType = "None"


Speed = 4000. # (RPM) rotational Speed in RPM)

# define geometries of Blade Rows. Note these need to be defined in sequence as they are installed
row1 = BLADEROW('stator', r_hub1=0.09, r_hub2=0.09, r_tip1=0.11, r_tip2=0.11, label='row1')
row1.pitch = 0.05         # pitch 
row1.phi1 = [90.]         # (deg) relative blade angle at inlet
row1.phi2 = [30.]         # (deg) relative blade angle at outlet
row1.xFraction = [0.4]    # (-) fraction of axial chord where inlet and outlet tangents meet
row1.L = [0.02]           # (m) Axial Chord
row1.eta_max = [0.01]     # (m) height of the camber line
row1.d = [0.005]          # (m) largest thickness of the profile
row1.t_TE = [0.001]       # (m) thickness of trailing edge


row2 = BLADEROW('rotor', r_hub1=0.09, r_hub2=0.09, r_tip1=0.11, r_tip2=0.11, r_casing1=0.11, r_casing2=0.11, Nrpm = Speed, label='row2')
row2.pitch = 0.05         # pitch 
row2.phi1 = [45.]         # (deg) relative blade angle at inlet
row2.phi2 = [135.]        # (deg) relative blade angle at outlet
row2.xFraction = [0.5]    # (-) fraction of axial chord where inlet and outlet tangents meet
row2.L = [0.02]           # (m) Axial Chord
row2.eta_max = [0.01]     # (m) height of the camber line
row2.d = [0.005]          # (m) largest thickness of the profile
row2.t_TE = [0.001]       # (m) thickness of trailing edge

"""
row3 = BLADEROW('stator', r_hub1=0.09, r_hub2=0.09, r_tip1=0.11, r_tip2=0.11, label='row3')
row3.pitch = 0.05          # pitch 
row3.phi1 = [100.]         # (deg) relative blade angle at inlet
row3.phi2 = [80.]          # (deg) relative blade angle at outlet
row3.xFraction = [0.3]     # (-) fraction of axial chord where inlet and outlet tangents meet
row3.L = [0.02]            # (m) Axial Chord
row3.eta_max = [0.01]      # (m) height of the camber line
row3.d = [0.005]           # (m) largest thickness of the profile
row3.t_TE = [0.001]        # (m) thickness of trailing edge
"""

