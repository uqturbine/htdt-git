"""
Example input file for setting up Optimsiation run. 
Based on OF simulation for a convergent-divergent Nozzle.

The corresponding default OF case is provided in directory Case-Master/

Author: Ingo Jahn & Jianhui Qi
Last Modified: 08/04/2018
"""


# Optimiser settings
M.method = 'Nelder-Mead'
M.maxiter = 100
M.Nvar = 10   # number of optimisation variables
M.NPost = 2
M.NCost = 2
M.cutoff_0 = 1e-3
M.cutoff_1 = 10
M.allow_interpolation = True
M.plot_results = True
M.Post_properties = ['Ma', 'P']
M.Post_customSolverCommand = "sonicFoam -postProcess -func MachNo -latestTime > MachNoLatestTime"  # custom command provided to calculate MachNo
#M.Post_properties = ['mdot', 'mdot']
M.Post_boundaries = ['o-00', 'o-00']
M.Post_sumMode = ['FA', 'AA']
M.Post_targets = [1.2, 'max']
M.Post_customCostFlag = False
M.Post_CustomCostFunction = 'X[0] * 0.001 + X[1] * -1.'  # define custom equation to calculate cost. x=[Flux[0], Flux[1], ...], where flux is returned from OF_flux_calc.py
M.Cost_weights = [1., 1e-14]
M.Plot_progress = True
M.Save_solver_log = False
M.V_Eilmer = 4
M.interpolationType = 'interpolate'  # options are krigin and linear interpolation
#M.interpolationType = 'kriging'  # options are krigin and linear interpolation

# CFD case (or other piece of software) that is used for function evaluation. 
C.MasterCase_folder = 'Case_Master'
C.ChildCase_folder_base = 'Case_'
C.Input_filename = 'con-div-nozzle.lua'
C.Keep_ChildCase = True
C.setup_command = 'run-mesh.sh'
C.simulate_command = 'run-solver.sh'
C.postprocess_command =  'run-post.sh'


# Data that used by optimiser and results
D.Use_initial_simplex = True   # if false, the simulation will restart based on data from D.filename
D.initial_simplex_filename = 'Inputs.txt'
D.write_data = True
D.filename = 'Results.txt'
 
