Example for OF optimsier.

Optimiser is used to adjust the shape of an axi-symmetric convergen-divergent 
nozzle. The aim of the optimiser is to achieve a target Mach number while 
minimising the total pressure loss. 

The CFD simulation is conducted using sonicFoam from OpenFoam (of40).


Author: Ingo Jahn & Jianhui Qi
Last Modified: 08/04/2018


