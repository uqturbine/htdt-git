#! /usr/bin/env python
"""
Python Code to scale a given compressor map, and evaulate performance


Author: Joshua Keep
Last Modified: 08/02/2017
"""

import csv
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.mlab as ml
import CoolProp.CoolProp as CP



###########################################################################################
"""Import raw sandia data"""

#import efficiency data
with open('Eff.csv') as f:
    reader = csv.reader(f)
    props_list = list(reader)

#data_shape=np.shape(props_list)
#data_length=data_shape[0]

mdot_rows=[]
dh_rows=[]
eta_rows=[]
for line in props_list:
    #data filtering - (extract list to string, convert to float, split string, convert to array)
    mdot_rows.append( float(line[0]) )
    dh_rows.append( float(line[1]) )
    eta_rows.append( float(line[2]) )

#mdot_cols=zip(mdot_rows); mdot_eta=np.array(mdot_cols[0])
#dh_cols=zip(*dh_rows); dh_eta=np.array(dh_cols[0])
#eta_cols=zip(*eta_rows); eta=np.array(eta_cols[0])
#dh_isen_eta = dh_eta * eta

mdot_eta=np.array(mdot_rows)
dh_eta=np.array(dh_rows)
eta=np.array(eta_rows)

#import speedline data
with open('Speed.csv') as f:
    reader_1 = csv.reader(f)
    props_list_1 = list(reader_1)

data_shape_1=np.shape(props_list_1)
data_length_1=data_shape_1[0]

mdot_rows_1=[]
dh_rows_1=[]
N_rows=[]
for line in props_list_1:
    #data filtering - (extract list to string, convert to float, split string, convert to array)
    mdot_rows_1.append( float(line[0]) )
    dh_rows_1.append( float(line[1]) )
    N_rows.append( float(line[2]) )
    #mdot_rows_1.append(map(float, props_list_1[i][0].split()))
    #dh_rows_1.append(map(float, props_list_1[i][1].split()))
    #N_rows.append(map(float, props_list_1[i][2].split()))
#mdot_cols_1=zip(*mdot_rows_1); mdot_speed=np.array(mdot_cols_1[0])
#dh_cols_1=zip(*dh_rows_1); dh_speed=np.array(dh_cols_1[0])
#N_cols=zip(*N_rows); speed=np.array(N_cols[0])

mdot_speed=np.array(mdot_rows_1)
dh_speed=np.array(dh_rows_1)
speed=np.array(N_rows)


# create surge line 
speeds = set(speed)
speeds = sorted(speeds)
mdot_surge = []
dh_surge = []
speed_surge = []
for s in speeds:
    index = np.where(speed==s)[0][0]
    mdot_surge.append(mdot_speed[index])
    dh_surge.append(dh_speed[index])
    speed_surge.append(s)
mdot_surge = np.array(mdot_surge)
dh_surge = np.array(dh_surge)
speed_surge = np.array(speed_surge)

###########################################################################################
"""convert values to metric"""
#constants
lbs_2_kg=1/2.20462234   # [kg/lbs]
btu_2_j=1055.06         # [J/btu]

#clear that standard conditions have not been used. suspect normalisation using critical conditions
T0_std=304.7 # [K]
P0_std=7.377*1.e6 # [Pa]

#change map to metric units
mdot_speed=mdot_speed*lbs_2_kg
mdot_eta=mdot_eta*lbs_2_kg
mdot_surge=mdot_surge*lbs_2_kg 

dh_speed=dh_speed*btu_2_j / lbs_2_kg
dh_eta=dh_eta*btu_2_j / lbs_2_kg
dh_isen_eta=dh_eta*btu_2_j / lbs_2_kg
dh_surge=dh_surge*btu_2_j / lbs_2_kg

# Design Point for map
mdot_d=3.53    # [kg/s]
speed_d=75000. # [RPM]
T_d=305.3  # [K]
P_d=7.687*1e6  # [Pa]
dh_d = 10107.9 # [J/kg]
eta_d=0.67     # efficiency

#convert design values to equivalent conditions for normalisation
speed_d_eq = get_N_eq(speed_d,T0_std,P0_std,T_d,P_d)
mdot_d_eq  = get_mdot_eq(mdot_d,T0_std,P0_std,T_d,P_d)
dh_d_eq  = get_h_eq(dh_d,T0_std,P0_std,T_d,P_d)

###########################################################################################
# sandia metric map for reference / verificiation
pf=0
if pf is 1:
    N=100
    MDOT=np.linspace(min(mdot_eta),max(mdot_eta),N)
    DH=np.linspace(min(dh_eta),max(dh_eta),N)
    ETA=ml.griddata(mdot_eta, dh_eta, eta, MDOT, DH, interp='nn')
    SPEED=ml.griddata(mdot_speed, dh_speed, speed, MDOT, DH, interp='nn')

    plt.figure()
    plt.pcolormesh(MDOT,DH,ETA, cmap = plt.get_cmap('rainbow'))
    plt.colorbar()
    CS=plt.contour(MDOT,DH,SPEED,10,colors='k')
    #plt.clabel(CS, inline=1, fmt='%1.2f', fontsize=10) #fmt adjusts rounding
    plt.xlabel('corrected massflow [kg/sec]')
    plt.ylabel('Corrected specific enthalpy rise [J/Kg]')
    plt.title('Sandia compressor map (metric, corrected)')

    # sandia uncorrected metric map for reference / verificiation
    ###########################################################################################
    MDOT_1=get_mdot(MDOT,T0_std,P0_std,T_d,P_d)
    DH_1=get_h(DH,T0_std,P0_std,T_d,P_d)
    SPEED_1=get_N(SPEED,T0_std,P0_std,T_d,P_d)
    ###########################################################################################

    plt.figure()
    plt.pcolormesh(MDOT_1,DH_1,ETA, cmap = plt.get_cmap('rainbow'))
    plt.colorbar()
    CS=plt.contour(MDOT_1,DH_1,SPEED_1,10,colors='k')
    #plt.clabel(CS, inline=1, fmt='%1.2f', fontsize=10) #fmt adjusts rounding
    plt.xlabel(' massflow [kg/sec]')
    plt.ylabel('specific enthalpy rise [J/Kg]')
    plt.title('Sandia compressor map (metric, uncorrected at design conds)')

    # normalised map - all values aside from dh
    ###########################################################################################
    MDOT_2=MDOT/mdot_d_eq
    SPEED_2=SPEED/speed_d_eq

    plt.figure()
    plt.pcolormesh(MDOT_2,DH,ETA, cmap = plt.get_cmap('rainbow'))
    plt.colorbar()
    CS=plt.contour(MDOT_2,DH,SPEED_2,10,colors='k')
    #plt.clabel(CS, inline=1, fmt='%1.2f', fontsize=10) #fmt adjusts rounding
    plt.xlabel('nomralised massflow [-]')
    plt.ylabel('Corrected specific enthalpy rise [J/Kg]')
    plt.title('Sandia compressor map (normalised, corrected)')
    plt.show()


###########################################################################################
#scale raw variables as above map for export. h & eta do not scale.


# normalise all the maps to equivalent conditions
mdot_eta_nd=mdot_eta/mdot_d_eq
dh_eta_nd = dh_eta/dh_d_eq
dh_isen_eta_nd = dh_isen_eta/dh_d_eq
eta_nd = eta/eta_d

mdot_speed_nd=mdot_speed/mdot_d_eq
dh_speed_nd = dh_speed/dh_d_eq
speed_nd=speed/speed_d_eq

# create surge line
mdot_surge_nd = mdot_surge/mdot_d_eq
dh_surge_nd = dh_surge/dh_d_eq
speed_surge_nd = speed_surge/speed_d_eq

#print mdot_surge_nd
#print dh_surge_nd

"""
Include the following for export:
C.mdot_eta_nd = ...  List of mass flow rates corresponding to eta list (normalised to 1.0 by sandia design value)
C.dh_eta = ...       List of dh corresponding to eta list 
C.eta = ...          List of eta
C.mdot_speed_nd = ...List of mass flow rates corresponding to speed list (normalised to 1.0 by sandia design value)
C.dh_speed = ...     List of dh corresponding to speed list 
C.speed_nd = ...     List of speed (normalised to 1.0 by sandia design value)

C.Tcorr =...        Temperature for correction of maps
C.Pcorr =...        Temperature for correction of maps
"""

N_eq = get_N_eq(1.0,T0_std,P0_std,305.,9.e6)
mdot_eq = get_mdot_eq(10.,T0_std,P0_std,305.,9.e6)
dh_eq = get_h_eq(22.e3,T0_std,P0_std,305.,9.e6)
eta_design=0.67

print('Compressor data adjustments:')
print('Adjusted speed (1.0) to equivalent conditions      : {0:.4f}'.format(N_eq))
print('Adjusted mass flow (11.0) to equivalent conditions : {0:.4f}'.format(mdot_eq))
print('Adjusted dh (21.954e3) to equivalent conditions    : {0:.4f}'.format(dh_eq))


C.mdot_eta = mdot_eta_nd
C.DH_eta = dh_eta_nd
C.DH_isen_eta = dh_isen_eta_nd
C.eta = eta_nd  # should not be scaled
C.mdot_speed = mdot_speed_nd
C.DH_speed = dh_speed_nd
C.speed = speed_nd
C.mdot_design = mdot_eq     # [kg/s]
C.DH_design = dh_eq   # not used for scaling
C.eta_design = eta_design
C.Volume = 1.e-3        # [m3]
C.speed_design = N_eq # 
C.mdot_surge = mdot_surge_nd
C.dh_surge = dh_surge_nd
C.speed_surge = speed_surge_nd
C.Tcorr=T0_std
C.Pcorr=P0_std
#C.OP_speed = 1.0


#"""
#Include the following:
#C.mdot_eta = ...     List of mass flow rates corresponding to eta list (normalised to 1.0)
#C.DH_eta = ...       List of DH corresponding to eta list (normalised to 1.0)
#C.eta = ...          List of eta
#C.mdot_speed = ...   List of mass flow rates corresponding to speed list (normalised to 1.0)
#C.DH_speed = ...     List of DH corresponding to speed list (normalised to 1.0)
#C.speed = ...        List of speed (normalised to 1.0)
#C.mdot_design = ...  Design mass flow rate
#C.DH_design = ...    Design DH
#C.speed_design = ... Design speed
#C.Tcorr = ...        Temperature at reference point used for the maps
#C.Pcorr = ...        Pressure at reference point used for the maps 
#"""


