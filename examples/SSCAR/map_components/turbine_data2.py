""" 
file containing data that defines turbine operating conditions

u/c curve from Ricardo A70 turbine

"""

import  numpy as np

#U_C data
U_C=np.array([0.0991358369433,0.194416671403,0.293620712236,0.365608435536,
    0.394820458202,0.441560880621,0.494125322177,0.548679538092,0.599349097658,
    0.653915175118,0.698769611707,0.753403893058,0.802220086046,0.849090985472,
    0.907689988608,0.952648213725,0.995693764533,1.02310579758,1.06028581384,
    1.09164477635,1.12104954914,1.13481487339,1.16031719751,1.18581952163])
eta_raw=np.array([0.194393442542,0.362965781911,0.520894854807,0.627186663668,
    0.656025047632,0.701558557735,0.762282260316,0.803246604015,0.835097326457,
    0.869982627777,0.882083875958,0.882014683606,0.863715777586,0.842379821533,
    0.810390714386,0.769300341761,0.708455552563,0.659788617364,0.60503028401,
    0.533561997583,0.463615942906,0.408887263418,0.338946151052,0.269005038686])
eta=eta_raw*100.0

#ER running line data
#60k
ER_60=[2.4950161963,2.35682671807,2.2253712657,2.08379774075,
    1.97600790368,1.86491351092,1.77066553865,1.67307326518,
    1.58228883066,1.52855857211,1.4883020432,1.41778214348]
MFP_60=[7.27694958849,7.00875253023,6.77416882191,6.53967028256,
    6.13653720038,5.53141173231,4.97664950985,4.33774035391,
    3.51358870773,2.8238052924,2.18441351234,1.2759162051]
speed_60=60000.*np.ones(len(ER_60))

ER=np.array(ER_60)
MFP=np.array(MFP_60)
speed=np.array(speed_60[0])

# non-dimensionalise ER running line data from design values of ricardo turbine 
ER_ref=2.08379774075
MFP_ref=6.53967028256
speed_ref=60000.
eta_ref = 0.89

ER_norm= (np.array(ER) - 1.) / (ER_ref-1.) #scaling should be OK if new turbine is ~2.0 ER
MFP_norm=MFP/MFP_ref
speed_norm=speed/speed_ref
eta_norm = eta/eta_ref

# assign normalised data to T class object
"""
Include the following:
T.UC = ...
T.eta = ...
T.ER = ...   Expansion Ratio normalised to 1.0 by Ricardo design
T.MFP = ...  Mass flow parameter normalised to 1.0 by Ricardo design
T.speed = ... Shaft speed normalised to 1.0 by Ricardo design

T.T0_design = ... Design inlet pressure [K]
T.ER_design = ... Design expansion ratio for present design
T.MFP_design = ... Design mass flow parameter for present design [rpm.sqrt(K)/Bar]
T.Speed_design = ... Design shaft speed for present design [rpm]

"""

# calculate mass flow parameter
# calculate mass flow parameter
print('Turbine data adjustments:')
MFP= 10.*np.sqrt(800.)/(19.8e6/10**5)
print('MFP at design point: {0:.4f} \n'.format(MFP) )

#N_corr=N*np.sqrt(T0_d)/np.sqrt(T0_in)



T.uc = U_C
T.eta = eta_norm
T.er = ER_norm
T.mfp = MFP_norm
T.speed = speed_norm

T.T0_design=800.
T.P0_design=19.8e6
T.er_design = 2.2
T.U_C_design = 0.7
T.mfp_design = MFP  
T.speed_design = 1.0
T.eta_design = 0.89
T.Volume = 2.e-3  #[m3]
