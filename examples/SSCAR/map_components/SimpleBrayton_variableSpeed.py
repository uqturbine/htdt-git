"""
Example input file for cycle modeller

Brayton cycle running from compressor and turbine map.
Variable Speed Turbine 

Author: Ingo Jahn
Last Modified: 10/02/2017
"""


# set fluid
gdata.iter = 10
gdata.print_flag = 2.
gdata.optim = 'root:hybr'

# set mass flows
m1 = MASS(9.,'CO2',label='m1')

# Define control points and intial conditions
P0=9.00e6; P1 = 19.8e6; t0=305.; t1 = 800.; t2 = 708.
p0 = POINT(P0,t0,label='p0',ptype='P_fixed')
p1 = POINT(P1,t0,label='p1')
p2 = POINT(P1,t1,label='p2')
p3 = POINT(P0,t2,label='p3')

# Define Heat Exchangers
hx_source = HX('source',800.,p1,p2,[m1],label='HX_hot')
hx_sink = HX('sink',305.,p3,p0,[m1],label='HX_cold')

# Define Turbines
N_turb = 0.95
t1 = TURB('turbine_data.py',p2,p3,[m1],N_turb) # turbine map that supports variable speed operation

# Define Compressors
N_comp = 1.0
c1 = COMP('comp_data.py',p0,p1,[m1],N_comp)

# Provide Info for Efficiency Calculation
eta.Qin([hx_source])
eta.Qout([hx_sink])
