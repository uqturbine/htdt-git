"""
Example input file for cycle modeller

Simple internal combustion engine 

Author: Ingo Jahn
Last Modified: 10/02/2017
"""


# set fluid
gdata.iter = 10
gdata.print_flag = 0
gdata.optim = 'root:hybr'

# set mass flows
m1 = MASS(4.,'Air',label='m1',mtype='free')

# Define control points and intial conditions
P0=1.e5; t0=300.
p0 = POINT(P0,t0,label='p0',ptype='PT_fixed')
p1 = POINT(P0,t0,label='p1',ptype='P_fixed')

# Define ICE
Speed = 1000./60. # speed in Rev/s
Vswept = 2e-3   # swept volume in m3
Qfuel = 100e3   # Power added to fluid (Q = Q_fuel - W_shaft)
eff = 0.3       # efficiency fraction of Q_fuel that becomes shaft power
coolant_split = 0.5 # fraction of losses that are conducted into coolant
i0 = ICE_SIMPLE(p0,p1,[m1],Speed,Vswept,Qfuel,eff,coolant_split,label='ICE_simple')


# Provide Info for Efficiency Calculation
eta.Qin([i0])
eta.Qout([])
