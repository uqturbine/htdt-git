"""
Example input file for cycle modeller

Internal Combustion engine operating with a turbocharged set-up.
The engine operation can be altered by adjusting Rev/s (adjusting volume flow 
through cylinder) and Q_fuel (adjusting cylinder exit temperature). 

Author: Ingo Jahn
Last Modified: 2018/10/05
"""


# set fluid
gdata.iter = 10
gdata.print_flag = 0
gdata.optim = 'root:hybr'


# Inout Variables used to manipulate the engine operation
Speed = 4000./60.   # speed in [Rev/s]
Vswept = 2e-3       # swept volume in [m3]
AFR = 14            # air to fuel ratio [-] 
eff = 0.3           # ICE efficiency [-]
coolant_split = 0.3 # split of losses between Gas and Coolant 
mdot_fuel = 1.25 * Speed/2.*Vswept / AFR # mass flow rate of fuel [kg/s]
hcomb = 46e6        # specific heat of combustion of fuel [J/kg]
Qfuel = mdot_fuel * hcomb  # Fuel energy released [Watt] 
# Power Power added to fluid passing trough engine is calcuated as:
# W_shaft = eff * Q_fuel
# W_loss = (1-eff) * Q_fuel
# Q_coolant = W_loss * coolant_split
# Q_fluid = W_loss * (1-coolant_split)

# set mass flows
mAir = MASS(Speed / 2. * Vswept,'Air',label='m1',mtype='free')
#shaft_p = POWER(1.e3,label='shaft-power') 
#shaft_s = SPEED(10000.,label='shaft-speed') 


# Define control points and intial conditions
P_amb=1.e5; T_amb = 300.
P1=1.5e5; T1=350.
P2=1.3e5; T2=900.
T3=400.

p0 = POINT(P_amb,T_amb,label='p0',ptype='PT_fixed')
p1 = POINT(P1,T1,label='p1')
p2 = POINT(P2,T2,label='p2')
p3 = POINT(P_amb,T3,label='p3',ptype='P_fixed')

power_comp = 10e3
power_turb = 10e3
# Set Compressor
c1 = COMP_POWER(p0,p1,[mAir],power_comp,0.7,S=None,P=None,label='Compressor')

# Define ICE
i0 = ICE_SIMPLE(p1,p2,[mAir],Speed,Vswept,Qfuel,eff,coolant_split,label='ICE_simple')

# Set Turbine
t1 = TURB_POWER(p2,p3,[mAir],power_turb,0.8,S=None,P=None,label='Turbine')

# Provide Info for Efficiency Calculation
eta.Qin([i0])
eta.Qout([i0])
