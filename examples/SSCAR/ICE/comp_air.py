#! /usr/bin/env python3
""" 
File containing data that defines compressor operating conditions.

Map is base on BorgWarner EFR7163 Compressor.
Data is provided in file BW_EFR7163.dta

Author: Ingo Jahn
Created: 2018/10/05
"""

import  numpy as np

#------------------------------------------------------------------------------

# Define Reference Conditions
T01 = 298.15    # [K]
P01 = 1.013e5   # [Pa]

# Define Data file 
datafile = 'BW_EFR7163.dat'

# Define Base Conditions for non-dimensionalising [units to match source]
mdot_base = 0.27
PR_base = 2.3
eta_base = 74.
speed_base = 131000.


# Define Design Conditions for re-dimensionalising
mdot_design = 0.196      # [kg/s]
PR_design = 1.5         # [-]
eta_design = 0.7       # [-]
speed_design = 10000.   # [rad/s]


#------------------------------------------------------------------------------
# Load data file 

speed_raw=[]
mdot_raw=[]
PR_raw=[]
eta_raw=[]

speed_surge_raw=[]
mdot_surge_raw=[]
PR_surge_raw=[]

with open(datafile,'r') as infile:
    for line in infile:
        if line.startswith('#'):
            pass
        else:
            line = line.rstrip('\n')
            data = line.split(',')
            speed_raw.append(float(data[0]))  
            mdot_raw.append(float(data[1]))  
            PR_raw.append(float(data[2]))
            eta_raw.append(float(data[3]))
            if len(data) > 4 and (data[4] == 'surge' or data[4] == 'end'):
                speed_surge_raw.append(float(data[0]))
                mdot_surge_raw.append(float(data[1]))
                PR_surge_raw.append(float(data[2]))

# convert lists to numpy arrays
speed_raw = np.array(speed_raw)
mdot_raw  = np.array(mdot_raw)
PR_raw    = np.array(PR_raw)
eta_raw   = np.array(eta_raw)

speed_surge_raw = np.array(speed_surge_raw)
mdot_surge_raw  = np.array(mdot_surge_raw)
PR_surge_raw   = np.array(PR_surge_raw)

#------------------------------------------------------------------------------
# Non-dimensionalize arrays by base point
mdot_eta_nd = mdot_raw / mdot_base
PR_eta_nd = PR_raw / PR_base
eta_nd = eta_raw / eta_base

mdot_speed_nd = mdot_raw / mdot_base
PR_speed_nd = PR_raw / PR_base
speed_nd = speed_raw / speed_base

#------------------------------------------------------------------------------
# Create Class variables to suit  Comp_funct.py
C.type = 'MDOT_PR'
C.mdot_eta = mdot_eta_nd
C.PR_eta = PR_eta_nd
C.eta = eta_nd  # should not be scaled
C.mdot_speed = mdot_speed_nd
C.PR_speed = PR_speed_nd
C.speed = speed_nd
C.mdot_design = mdot_design    # [kg/s]
C.PR_design = PR_design
C.eta_design = eta_design
C.speed_design = speed_design 
C.Volume = 1.e-3        # [m3]
#C.mdot_surge = mdot_surge_nd
#C.dh_surge = dh_surge_nd
#C.speed_surge = speed_surge_nd
C.Tcorr=T01
C.Pcorr=P01
