"""
Example input file for SSCAR

Design for an Air Cycle Cooling Loop.
Uses the TURB_POWER and COMP_POWER components. These allow the respective power 
extracted from turbine and supplied to compressor to be specified.

Author: Ingo Jahn
Last Modified: 10/02/2017
"""


# set fluid
gdata.iter = 10
gdata.print_flag = 0
gdata.optim = 'root:hybr'

# set mass flows
mAir = MASS(0.04,'Air',label='mAir')
mCoolant = MASS(0.1,'CUSTOM:Glycol.py',label='mCoolant',mtype='fixed')

# Define control points and intial conditions for air
P_in=1.0e5; P_out=1e5
Pint = 1.05e5
T_in = 273.15+25

p0 = POINT(P_in,T_in,label='p0',ptype='PT_fixed')
p1 = POINT(Pint,T_in,label='p1')
p2 = POINT(Pint,T_in,label='p2')
p3 = POINT(Pint,T_in,label='p3')
p4 = POINT(P_out,T_in,label='p4',ptype='P_fixed')


# Define control points and intial conditions for coolant
PC=1.e5;
TC=273.15+70
c0 = POINT(PC,TC,label='c1',ptype='PT_fixed')
c1 = POINT(PC,TC-5,label='c2')

# Define Ram air intake
r1 = RAM_AIR_INTAKE(p0,p1,[mAir],0.40,0.8,label='Ram-Intake')


# Define Turbines
t1 = TURB_POWER(p1,p2,[mAir],480.,0.89,label='Turbine')

# Define Heat Exchangers
#hx1 = HX('source',273.15+40,p2,p3,[mAir],label='Cooler')
hx1 = RECUP('HX_Cooler.py',p2,p3,c0,c1,[mAir],[mCoolant],label='Cooler')

# Define Compressors
c1 = COMP_POWER(p3,p4,[mAir],480.,0.8,label='Compressor')

# Provide Info for Efficiency Calculation
eta.Qin([hx1])
eta.Qout([])
