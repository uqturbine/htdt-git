"""
Template file containg properties of a custom gas defined by interploatable tables
"""
# Optionaly use the below to set a custom table based gas model. 
# This will be called by setting F.FluidH = "CUSTOM:Glycol.py"


self.temperature   = np.array([200., 250., 300., 350., 400.]) #[K]
self.quality   = np.array([-1., -1., -1, -1., -1]) #[K]
self.density       = np.array([1000., 1000., 1000, 1000., 1000]) #[kg/m3]
self.specific_heat = np.array([4.18228, 4.18228, 4.18228, 4.18228, 4.18228])*1e3 #[J/kg/K]
self.enthalpy      = np.array([100., 309.114, 518.228, 727.342, 936.456 ])*1e3 #[J/kg]
self.conductivity  = np.array([100., 100., 100., 100., 100.]) #[W/m/K]
self.viscosity     = np.array([0.1, 0.1, 0.1, 0.1, 0.1]) # [N - s/m2]]
self.prandtl_number= np.array([ 1., 1., 1., 1., 1.]) # [-]
self.saturation_pressure = np.array([np.nan, np.nan, np.nan]) #[Pa]
self.Tmin        = np.min(self.temperature)
self.Tmax        = np.max(self.temperature)
self.TminPsat    = np.min(self.temperature[~np.isnan(self.saturation_pressure)])
self.name        = "Glycol"
self.description = "Glycol"
self.reference   = "n/a"
