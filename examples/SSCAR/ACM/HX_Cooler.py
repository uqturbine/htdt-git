"""
Template input file for HX_solver.py
""" 

# set fluid conditions at heat exchanger inlet and outlet
F.fluidH = 'CUSTOM:Glycol.py' 
F.fluidPropFileH = 'Glycol.py'
#F.TH_in = 313.15
F.hH_in = 698693.3819999999
F.mdotH = 0.1
F.PH_in = 100000.0
F.PH_out = 100000.0

F.fluidC = 'Air'
#F.TC_in = 296.2
F.hC_in = 415380.444587
F.mdotC = 0.0236815145699
F.PC_in = 84942.1988031
F.PC_out = 84942.1988031

F.T_ext = 295 # External Temperature (K) optional

# set geometry for heat exhanger - required settings depend on type
G.HXtype = 'pinch_dT'
G.dT = 5.


# Set modelling parameters
M.N_cell = 10 # number of cells


