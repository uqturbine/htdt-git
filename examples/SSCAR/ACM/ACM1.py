"""
Example input file for SSCAR

Design for an Air Cycle Cooling Loop.
Uses the TURB_POWER and COMP_POWER components and POWER class to set power level.
This allows power transferred to be turned into a free variable and in exchange 
another variable, e.g mAir can be fixed.

Author: Ingo Jahn
Last Modified: 01/06/2018
"""


# set fluid
gdata.iter = 10
gdata.print_flag = 0
gdata.optim = 'root:hybr'

# set mass flows
mAir = MASS(0.03,'Air',label='mAir',mtype='fixed')
mCoolant = MASS(0.05,'CUSTOM:Glycol.py',label='mCoolant', mtype='fixed')
shaft0 = POWER(490.,label='shaft1') 

# Define control points and intial conditions for air
P_in=1.0e5; P_out=1e5
Pint = 1.05e5
T_in = 273.15+25

p0 = POINT(P_in,T_in,label='p0',ptype='PT_fixed')
p1 = POINT(Pint,T_in,label='p1')
p2 = POINT(Pint,T_in,label='p2')
p3 = POINT(Pint,T_in,label='p3')
p4 = POINT(P_out,T_in,label='p4',ptype='P_fixed')

# Define control points and intial conditions for coolant
PC=1.e5;
TC=273.15+70
c0 = POINT(PC,TC,label='c1',ptype='PT_fixed')
c1 = POINT(PC,TC-5,label='c2')

# Define Ram air intake
r1 = RAM_AIR_INTAKE(p0,p1,[mAir],0.70,0.8,label='Ram-Intake')

# Define Turbines
t1 = TURB_POWER(p1,p2,[mAir],0.,0.89,P=[shaft0],label='Turbine')
# note: by providing P=[shaft0] to TURB_POWER, the power will be set by a POWER instance, which can be included in the optimisation

# Define Heat Exchangers
hx1 = RECUP('HX_Cooler.py',p2,p3,c0,c1,[mAir],[mCoolant],label='Cooler')

# Define Compressors
c1 = COMP_POWER(p3,p4,[mAir],0.,0.8,P=[shaft0],label='Compressor')
# note: by providing P=[shaft0] to COMP_POWER, the power will be set by a POWER instance, which can be included in the optimisation

# Provide Info for Efficiency Calculation
eta.Qin([hx1])
eta.Qout([])
