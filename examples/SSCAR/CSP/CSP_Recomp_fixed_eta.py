"""
Example input file for cycle modeller

Recompression cycle running with fixed efficiency turbine and compressors

Author: Ingo Jahn
Last Modified: 10/02/2017
"""

# set fluid
gdata.iter = 10
gdata.print_flag = 2
gdata.optim = 'root:hybr'


# set Sodium Stream
Ts_in = 610.+273.15  # [K]
mdot_source = 30. # [kg/s]
m_source = MASS(mdot_source,'CUSTOM:Sodium.py',label='mdot-sodium',mtype='fixed')
ps0 = POINT(1e5,Ts_in,label='ps0',ptype='PT_fixed')
ps1 = POINT(1e5,Ts_in-60.,label='ps1')


# set cooling water
Tw_in = 273.15 + 35. # [K]
mdot_water = 10.   #[kg/s]
m_water = MASS(mdot_water,'H2O',label='mdot-water',mtype='fixed')
pw0 = POINT(1e5,Tw_in,label='pw0',ptype='PT_fixed')
pw1 = POINT(1e5,Tw_in+30.,label='pw1')


# set mass flows
m1 = MASS(11.,'CO2',label='m1')
m2 = MASS(2.8,'CO2',label='m2')


# Define control points and intial conditions
P0=9e6; P1 = 19.8e6; 
t0=320.; t1=360.; t2=450.; t3=450.; t4=750.; t5=870.; t6=780.; t7=450.; t8=370.; t9 = 450.

p0 = POINT(P0,t0,label='p0',ptype='P_fixed')
p1 = POINT(P1,t1,label='p1')
p2 = POINT(P1,t2,label='p2')
p3 = POINT(P1,t3,label='p3')
p4 = POINT(P1,t4,label='p4')
p5 = POINT(P1,t5,label='p5')
p6 = POINT(P0,t6,label='p6')
p7 = POINT(P0,t7,label='p7')
p8 = POINT(P0,t8,label='p8')
p9 = POINT(P1,t9,label='p9')


# Define Heat Exchangers
hx_source = RECUP('HX-source.py',p4,p5,ps0,ps1,[m1,m2],[m_source],label='HX_source')
hx_sink = RECUP('HX-sink.py',pw0,pw1,p8,p0,[m_water],[m1],label='HX_sink')

hx_recup_cold = RECUP('HX-Recup_cold.py',p1,p2,p7,p8,[m1],[m1,m2],label='Low Temp. Recup.')
hx_recup_hot = RECUP('HX-Recup_hot.py',p3,p4,p6,p7,[m1,m2],[m1,m2],label='High Temp. Recup.')


# Define Turbines
t1 = TURB_ER(p5,p6,[m1,m2],2.22,0.89,label='Turbine')

# Define Compressors
c1 = COMP_MASSF(p0,p1,[m1],10.,0.78,label='MC')
c2 = COMP_MASSF(p8,p9,[m2],3.8,0.80,label='RC')

# MERGE
m1 = MERGE(p2,p9,p3,[m1],[m2],[m1,m2],label='MERGE')


# Provide Info for Efficiency Calculation
eta.Qin([hx_source])
eta.Qout([hx_sink])

