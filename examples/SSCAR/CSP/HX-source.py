"""
Template input file for HX_solver.py
""" 

# set fluid conditions at heat exchanger inlet and outlet
F.fluidH = 'CUSTOM:Sodium.py' 
F.fluidPropFileH = 'Sodium.py'
#F.TH_in = 883.15
F.hH_in = 873863.5364466098
F.mdotH = 30.0
F.PH_in = 100000.0
F.PH_out = 100000.0

F.fluidC = 'CO2'
#F.TC_in = 740.11981963
F.hC_in = 932723.139886
F.mdotC = 13.8
F.PC_in = 19980000.0
F.PC_out = 19980000.0
F.T_ext = 295 # External Temperature (K) optional

F.T0 = [ ] 

# set geometry for heat exhanger - required settings depend on type
G.HXtype = 'pinch_dT'
G.dT = 5.

# Set modelling parameters
# M.optim='fsolve'
M.N_cell = 20 # number of cells

