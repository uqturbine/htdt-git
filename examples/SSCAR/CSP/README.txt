Examples for Cycle Ananlys of heat engine integrated with Concentrated Solar 
Thermal. 


Case 1: CSP_Recomp_fixed_eta.py
+++++++++++++++++++++++++++++++

This example can be executed using the command:
$$ SSCAR.py --job=CSP_Recomp_fixed_eta.py

The is an example set-up for a sCO2 recompression cycle operating between a 
sodium <--> CO2 heat exchnager at the hot side and a CO2 <--> water heat 
exchanger at the cold side. 

In this cycle all heat exchangers are modelled using a fixed approach 
temperature, which prescribes the minimum temperature differnce in the heat
exchanger. 

The turbine, main compressor and re-compressor are modelled using a fixed
thermodynamic efficiency, which is set in the jobfile. 
The split ratio, defining the mass flow through the main and recompressor 
respectovely can be adjusted by modifying the prescribed mass-flow rate of the 
two compressors in the job file. 

Using the prescribed conditions from the jobfile, a cycle with a thermodynamic 
efficiency of XX% is obtained. This cycle can be further optimised by adjusting 
the split ratio, which will increase the amount of recuperation possible, 
and a resulting efficiency increase. 


