"""
Template input file for HX_solver.py
""" 

# set fluid conditions at heat exchanger inlet and outlet
F.fluidH = 'CO2' 
#F.TH_in = 368.031558281
F.hH_in = 504762.491319
F.mdotH = 10.0
F.PH_in = 9000000.0
F.PH_out = 9000000.0

F.fluidC = 'H2O'
#F.TC_in = 308.15
F.hC_in = 146718.67689228375
F.mdotC = 10.0
F.PC_in = 100000.0
F.PC_out = 100000.0
F.T_ext = 295 # External Temperature (K) optional

F.T0 = [ ] 

# set geometry for heat exhanger - required settings depend on type
G.HXtype = 'pinch_dT'
G.dT = 5.

# Set modelling parameters
# M.optim='fsolve'
M.N_cell = 10 # number of cells

