"""
Template input file for HX_solver.py
""" 

# set fluid conditions at heat exchanger inlet and outlet
F.fluidH = 'CO2' 
#F.TH_in = 779.264559647
F.hH_in = 991072.056822
F.mdotH = 13.8
F.PH_in = 9000000.00001
F.PH_out = 9000000.00001

F.fluidC = 'CO2'
#F.TC_in = 426.517456018
F.hC_in = 528638.011995
F.mdotC = 13.8
F.PC_in = 19980000.0
F.PC_out = 19980000.0
F.T_ext = 295 # External Temperature (K) optional


# set geometry for heat exhanger - required settings depend on type
G.HXtype = 'pinch_dT'
G.dT = 5.

# Set modelling parameters
# M.optim='fsolve'
M.N_cell = 10 # number of cells


"""
# set geometry for heat exhanger - required settings depend on type
G.HXtype = 'micro-channel'
G.N_R = 100    # number of rows in HX
G.N_C = 400    # number of columns in HX matrix
G.t_1 = 2e-3  #
G.t_2 = 0.5e-3 # 
G.t_casing = 5e-3 #
G.HX_L = 1.    # length of HX (m)
G.d_tube = 1.5e-3 # tube diameter
G.k_wall = 16 # thermal conductivity (W / mk)


# Set modelling parameters
M.N_cell = 5 # number of cells
M.flag_axial = 1
M.external_loss = 0
M.Nu_CorrelationH = 2 
M.Nu_CorrelationC = 2 
M.f_CorrelationH = 0 
"""

