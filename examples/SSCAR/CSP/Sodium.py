"""
Template file containg properties of a custom gas defined by interploatable tables
"""
# Optionaly use the below to set a custom table based gas model. 
# This will be called by setting F.FluidH = "CUSTOM"
# and setting the new variable   F.fluidPropFileH = "Sodium.py"


self.temperature   = np.array([371., 400., 500, 600., 700.,  
                                800.,  900., 1000., 1100.]) #[K]
self.quality   = np.array([-1., -1., -1., -1., -1.,  
                                -1.,  -1., -1., -1.]) #[K]
self.density       = np.array([919., 919., 897., 874., 852., 
                                828., 805., 781., 756. ]) #[kg/m3]
self.specific_heat = np.array([1.383, 1.372, 1.334, 1.301, 1.277, 
                                1.260, 1.252, 1.252, 1.261 ])*1e3 #[J/kg/K]
self.enthalpy      = np.array([207., 247., 382., 514., 642.,
                                769., 895., 1020., 1146. ])*1e3 #[J/kg]
self.conductivity  = np.array([89.44, 87.22, 80.09, 73.70, 68.00, 
                                62.90, 58.34, 54.24, 50.54]) #[W/m/K]
self.viscosity     = np.array([6.88-4, 5.99e-4, 4.15e-4, 3.21e-4, 2.64e-4,
                                2.27e-4, 2.01e-4, 1.81e-4, 1.66e-4]) # [N - s/m2]]
self.prandtl_number= np.array([ 1., 1., 1., 1., 1., 1., 1., 1., 1.]) # [-]
self.saturation_pressure = np.array([np.nan, 1.8e-4, 8.99e-2, 5.57, 1.05e2, 9.41e2, 
                                5.147e3, 1.995e4, 6.016e4, 0.1504e6]) #[Pa]
self.Tmin        = np.min(self.temperature)
self.Tmax        = np.max(self.temperature)
self.TminPsat    = np.min(self.temperature[~np.isnan(self.saturation_pressure)])
self.name        = "Sodium"
self.description = "Sodium"
self.reference   = "http://www.ne.anl.gov/eda/ANL-RE-95-2.pdf"
