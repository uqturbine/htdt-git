"""
Template input file for HX_solver.py
""" 

# set fluid conditions at heat exchanger inlet and outlet
F.fluidH = 'Air' 
#F.TH_in = 973.0
F.hH_in = 1029215.903034337
F.mdotH = 20.0
F.PH_in = 100000.0

F.fluidC = 'H2O'
#F.TC_in = 423.0
F.hC_in = 85893.2805434
F.mdotC = 3.0
F.PC_in = 2560000.0
F.T_ext = 295 # External Temperature (K) optional
F.T0 = [ ] 

# set geometry for heat exhanger - required settings depend on type
G.HXtype = 'pinch_dT'
G.dT= 5

# Set modelling parameters
M.N_cell = 20 # number of cells
M.flag_axial = 0
M.external_loss = 0
M.co_flow = 0
M.LM = 0

