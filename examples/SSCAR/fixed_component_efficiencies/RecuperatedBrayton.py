"""
Example input file for cycle modeller

Recuperated Brayton cycle with fixed compressor and turbine efficiencies

Author: Ingo Jahn
Last Modified: 10/02/2017
"""


# set fluid
gdata.iter = 10
gdata.print_flag = 2.
gdata.optim = 'root:hybr'

# set mass flows
m1 = MASS(10.,'CO2',label='m1')

# Define control points and intial conditions
P0=9.0e6; P1 = 19.8e6; t0=305.; t1 = 800.; t2 = 700.
p0 = POINT(P0,t0,label='p0',ptype='P_fixed')
p1 = POINT(P1,t0,label='p1')
p2 = POINT(P1,t0,label='p2')
p3 = POINT(P1,t1,label='p3')
p4 = POINT(P0,t2,label='p4')
p5 = POINT(P0,t2,label='p5')

# Define Heat Exchangers
hx1 = HX('source',800.,p2,p3,[m1],label='HX_hot')
hx2 = HX('sink',305.,p5,p0,[m1],label='HX_cold')

hx3 = RECUP('HX_recuperator.py',p1,p2,p4,p5,[m1],[m1])

# Define Turbines
t1 = TURB_ER(p3,p4,[m1],2.2,0.89,label='Turbine')


# Define Compressors
c1 = COMP_MASSF(p0,p1,[m1],11.,0.65,label='Compressor')

# Provide Info for Efficiency Calculation
eta.Qin([hx1])
eta.Qout([hx2])

