"""
Example input file for cycle modeller

Rankine Cycle using water (typical Steam).

Author: Ingo Jahn
Last Modified: 14/10/2017
"""


# set fluid
gdata.iter = 10
gdata.print_flag = 0
gdata.optim = 'root:hybr'


# set exhaust gas
Te_in = 600.+273  # [K]
mdot_exhaust = 20. # [kg/s]
m_exhaust = MASS(mdot_exhaust,'Air',label='mdot-air',mtype='fixed')
pe0 = POINT(1e5,Te_in,label='pe0',ptype='PT_fixed')
pe1 = POINT(1e5,Te_in-300.,label='pe1')


# set mass flows
mdot_cycle = 3.0
m1 = MASS(mdot_cycle,'H2O',label='m1')

# Define control points and intial conditions
P0=0.8e5; P1 = 25e5; t0=(273.+20); t1 = Te_in-10.; t2 = 273.+300.
p0 = POINT(P0,t0,label='p0',ptype='P_fixed')
p1 = POINT(P1,t0,label='p1')
p2 = POINT(P1,t1,label='p2')
p3 = POINT(P0,t2,label='p3')
p4 = POINT(P0,t2,label='p4')
p5 = POINT(P0,t2,label='p5')

# Define Heat Exchangers
hx1 = RECUP('HX_source.py',p1,p2,pe0,pe1,[m1],[m_exhaust],label='SOURCE')
#hx1 = HX('source',(273.+600),p1,p2,[m1],label='HX_hot')
hx2 = HX('sink',(273.+20),p5,p0,[m1],label='HX_cold')

# Define Turbines
N_turb = 1.0
t1 = TURB_ER(p2,p3,[m1],4.,0.85,label='Turbine-HP')
t2 = TURB_ER(p3,p4,[m1],4.,0.85,label='Turbine-IP')
t2 = TURB_ER(p4,p5,[m1],2.,0.85,label='Turbine-LP')

# Define Compressors
N_comp = 1.0
c1 = COMP_MASSF(p0,p1,[m1],mdot_cycle ,0.98,label='Pump')

# Provide Info for Efficiency Calculation
eta.Qin([hx1])
eta.Qout([hx2])
