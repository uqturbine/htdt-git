"""
Example input file for cycle modeller

Brayton cycle running with fixed compressor and turbine efficiencies.

Author: Ingo Jahn
Last Modified: 10/02/2017
"""


# set fluid
gdata.iter = 10
gdata.print_flag = 0
gdata.optim = 'root:hybr'

# set mass flows
m1 = MASS(9.,'CO2',label='m1')

# Define control points and intial conditions
P0=7.5e6; P1 = 25.e6; t0=305.; t1 = 800.; t2 = 708.
p0 = POINT(P0,t0,label='p0',ptype='P_fixed')
p1 = POINT(P1,t0,label='p1')
p2 = POINT(P1,t0,label='p2')
p3 = POINT(P1,t0,label='p3')
p4 = POINT(P1,t1,label='p4')
p5 = POINT(P0,t2,label='p5')

# Define Heat Exchangers
hx1 = HX('source',800.,p3,p4,[m1],label='HX_hot')
hx2 = HX('sink',305.,p5,p0,[m1],label='HX_cold')
hx3 = HX('sink',305.,p1,p2,[m1],label='HX_intercooler')

# Define Turbines
N_turb = 1.0
t1 = TURB_ER(p4,p5,[m1],3.33,0.89,label='Turbine')


# Define Compressors
N_comp = 1.0
c1 = COMP_MASSF(p0,p1,[m1],11.,0.8,label='Compressor-Stage1')
c2 = COMP_PR(p2,p3,[m1],1.66,0.8,label='Compressor-Stage2')

# Provide Info for Efficiency Calculation
eta.Qin([hx1])
eta.Qout([hx2,hx3])
