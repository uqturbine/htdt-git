"""
Example input file for cycle modeller

Brayton cycle running with fixed mass flow rate compressor and an expansion orifice instead of the turbine. 

Author: Ingo Jahn
Last Modified: 14/03/2018
"""


# set fluid
gdata.iter = 10
gdata.print_flag = 0
gdata.optim = 'root:hybr'

# set mass flows
m1 = MASS(3.,'CO2',label='m1')

# Define control points and intial conditions
P0=7.5e6; P1 = 25e6; t0=273.; t1 = 800.; t2 = 800.
p0 = POINT(P0,t0,label='p0',ptype='P_fixed')
p1 = POINT(P1,t0,label='p1')
p2 = POINT(P1,t1,label='p2')
p3 = POINT(P0,t2,label='p3')

# Define Heat Exchangers
hx1 = HX('source',800.,p1,p2,[m1],label='HX_hot')
hx2 = HX('sink',305.,p3,p0,[m1],label='HX_cold')

# Expansion Orifice
#e1 = EX_OR(p2,p3,[m1],4e-3,0.9,label='Expansion-Orifice',otype='isentropic')  # isentropic is not corrce as no heat is removed from system. 
e1 = EX_OR(p2,p3,[m1],90e-6,0.9,label='Expansion-Orifice',otype='isenthalpic')

# Define Compressors
c1 = COMP_MASSF(p0,p1,[m1],3.,0.8,label='Compressor')

# Provide Info for Efficiency Calculation
eta.Qin([hx1])
eta.Qout([hx2])
