"""
Example jobfile for setting up parametric evaluation


Author: Ingo Jahn
Last Modified: 26/07/2018
"""


II.varNames.append('Mdot')  # Variable Names cannot have spaces
II.varUnits.append('kg/s')
#i.varValues.append(np.mgrid(0.1,2.5,7j))
II.varValues.append(np.arange(8.,12.,2.))


II.varNames.append('PR')   # Variable Names cannot have spaces
II.varUnits.append('-')
II.varValues.append(np.arange(1.8,2.8,0.4)) 

II.varNames.append('T_source')   # Variable Names cannot have spaces
II.varUnits.append('K')
II.varValues.append(np.arange(300.,400.,100.)) 


