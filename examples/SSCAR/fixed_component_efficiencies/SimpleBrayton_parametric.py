"""
Example input file for cycle modeller

Brayton cycle running with fixed compressor and turbine efficiencies.

Author: Ingo Jahn
Last Modified: 26/07/2018
"""

# Parameters used for parametric evaluation
Mdot = 11.
PR = 2.2
T_source = 850.


# set fluid
gdata.iter = 10
gdata.print_flag = 0
gdata.optim = 'root:hybr'

# set mass flows
m1 = MASS(9.,'CO2',label='m1')

# Define control points and intial conditions
P0=9.00e6; P1 = 19.8e6; t0=305.; t1 = 800.; t2 = 708.
p0 = POINT(P0,t0,label='p0',ptype='P_fixed')
p1 = POINT(P1,t0,label='p1')
p2 = POINT(P1,t1,label='p2')
p3 = POINT(P0,t2,label='p3')

# Define Heat Exchangers
hx1 = HX('source',T_source,p1,p2,[m1],label='HX_hot')
hx2 = HX('sink',305.,p3,p0,[m1],label='HX_cold')

# Define Turbines
N_turb = 1.0
t1 = TURB_ER(p2,p3,[m1],PR,0.89,label='Turbine')


# Define Compressors
N_comp = 1.0
c1 = COMP_MASSF(p0,p1,[m1],Mdot,0.65,label='Compressor')

# Provide Info for Efficiency Calculation
eta.Qin([hx1])
eta.Qout([hx2])
