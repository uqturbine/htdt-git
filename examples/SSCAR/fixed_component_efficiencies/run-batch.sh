#! /bin/sh

# batch file to run parametric evaluation

# create data file that contains conditions
SSCAR_batch_1.py --jobfile=job-param --datafile=datafile.txt


# use the data file to create input files for SSCAR
SSCAR_batch_2.py --datafile=datafile.txt --MasterCase=SimpleBrayton_parametric --resultsfile=Results.txt


# use search the resultsfile and evaluate all cases with Status != 1
SSCAR_batch_3.py --resultsfile=Results.txt
