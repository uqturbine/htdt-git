"""Setting file for laminar boundary layer calculation."""
gas_properties = {
        "R": 287,  # [J/kg.K] gas constant
        "Pr": 0.75,  # [-] Prandtl Number
        "gamma": 1.4,  # [-] Ratio of specific heats
        "SL_slope": 1.458e-6,  # [kg/m.s.K^0.5] Sutherland's law slope
        "SL_offset": 110.39,  # [K] Sutherland's law offset
        "Cp": 1004.5,  # [J/kg.K] specific heat at constant pressure
}

free_stream = {
        "u": 295*12.,  # [m/s] free-stream velocity
        "M": 12,  # [m/s] free-stream Mach Number
        "p": 1000,  # [Pa]  free-stream Pressure
        }
"""
Free-stream conditions have been set to correspond to parameters from Van Driest.
theta = 0.505 = SL_offsewt / T_inf  --> T_inf = 217K
Hence Speed of Sound (a) = 295 m/s

Mach 12 - T_wall / T_e = 0.25 case
"""

bc_self_similar = {
        "g_wall": 0.25,  # []-] wall to free-stream enthalpy ratio
        "adiabatic": False,  # if set to True, value for g_wall is ignored.
        }

bc_space_march = {
        "L_ref": 1.0,  # [m] Reference length
        "bl_thickness_start": 2e-4,  # [m] Boundary layer thickness at star
        "bl_profile_start": 'linear',  # 'linear', 'selfsimilar' - sets how intitial BL profile is defined
        "x_L-vector": [0., 1.],  # [-] vector of x/L used to set spatially varying boundary conditions.
        "Ue/U_inf": [1., 1.],  # [-] velocity ratio at outer edge
        "Me/M_inf": [1., 1.],  # [-] Mach ratio at outer edge
        "pe/p_inf": [1., 1.],  # [-] pressure ratio at outer edge
        "Te/T_inf": [1., 1.],  # [-] temperature ratio at outer edge
        "Tw/T_inf": [1., 1.],  # [-] temperature ratio at wall
        "UE": 1.0,
        "DUEDX": 0.,
        }

config_self_similar = {
        "root_finder": 'hybr',  # root finder algorithm
        "verbosity": 0,  # set output level.
        "eta_max": 10,  # upper limit for solving BL equations
        "d_eta": 0.001,  # increment in eta used by RK4 solver
        "write_interval": 10,  # set how often output is written
        "evaluate_C_and_Pr_gradients": False,  # evaluate C' and Pr'
        "init-X[0]": 0.59,  # inital guess for f''(0)
        "init-X[1]": 13.8,  # inital gues for g(0) or g'(0)
        "plot_results": True,  # plot results
        }

config_space_march = {
        "verbosity": 0,  # set output level.
        "M_max": 200,  # number of steps in y-direction
        "N_max": 100,  # number of steps in x-direction
        "delta_y": 2e-4,  # [m] step size in y_direction
        "normaliseProfileFlag": True,  # select if profiles are plotted as U or U/U_inf, etc...
        "plot_results": True,  # plot results
        }
