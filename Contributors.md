## General
The code in this repository is the work of many. Through discussions, 
provision of code snippets, examples or feedback on codes the following 
have contributed to the development of the content of this repository. 
In no particular order:

Ingo H.J. Jahn
Viv Bone
Joshua Keep
Yuanshen Lu
Mosen Modirschanechi
Mostafa Odabaee
Hugh Russell
Kamel Hoomann
Halim Gurgenci
Peter Jacobs
Rowan Gollam
Anand Veeraragavan
Thomas Reddell
Jianhui Qi
Kan Qin
Luuk Meijboom
Aleks Atrens 
Michael Kearney
David Madsen
Samuel Roubin
Sam Duniman


## SSCAR
Main solver and solution Framework - Ingo Jahn
Development of turbine and compressor maps - Joshua Keep
Scripts for batch processing - Yuanshen Lu
Code housekeeping and file processing - Thomas Reddell

## HX_solver
Main solver and solution process - Ingo Jahn
Implementation of custom gas function and links lower level CoolProp Interface - Viv Bone
Implementation of UA HX model - Yuanshen Lu


## Radial Inflow Turbine Meshing
Core code - Ingo Jahn
Addition of O-mesh and set-up for Eilmer 4- Luuk Meijboom
Addition parametrised passages and splitter blades - David Madsen


## AxTurb
Core code - Ingo Jahn
