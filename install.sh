#! /bin/sh
# script to install the different python codes into /bin, so that they can be executed from the command line
# Call using ./install.sh
# Ingo Jahn 02/10/2017

# Set Repository and Installation directory
REPO=$(pwd)
INSTALL_DIR=${HOME}/htdt-inst  # should be on PATH and PYTHONPATH variable

# create /bin directory if it doesn't exist
mkdir -p ${INSTALL_DIR}
mkdir -p ${INSTALL_DIR}/bin
echo "Created install directory " ${INSTALL_DIR}

# copy respective codes into install directory
cp -f ${REPO}/src/HX_solver/* ${INSTALL_DIR}/bin/  # Copy files for HX_solver
echo "Copied Files for HX_solver."

cp -f ${REPO}/src/SSCAR/* ${INSTALL_DIR}/bin     # Copy files for SSCAR 
echo "Copied Files for SSCAR."

cp -f ${REPO}/src/AxTurb/* ${INSTALL_DIR}/bin     # Copy files for AxTurb 
echo "Copied Files for AxTurb."

cp -f ${REPO}/src/PotentialFlow/* ${INSTALL_DIR}/bin/       # Copy files from Potential Flow 
echo "Copied Files from PotentialFlow."

cp -f ${REPO}/src/NISI/* ${INSTALL_DIR}/bin/       # Copy files from NISI 
echo "Copied Files from NISI."

cp -f ${REPO}/src/MotionTracker/* ${INSTALL_DIR}/bin/       # Copy files from MotionTracker
echo "Copied Files from MotionTracker."

cp -f ${REPO}/src/SurfaceTemp/* ${INSTALL_DIR}/bin/       # Copy files from SurfaceTemp
echo "Copied Files from SurfTemp."

cp -f ${REPO}/src/OF-optim/* ${INSTALL_DIR}/bin/       # Copy files from OF-optim 
echo "Copied Files from OF-optim."

cp -f ${REPO}/src/boundarylayer/* ${INSTALL_DIR}/bin/       # Copy files from boundarylayer 
echo "Copied Files from boundarylayer."

cp -f ${REPO}/src/Misc/* ${INSTALL_DIR}/bin/       # Copy files from Misc 
echo "Copied Files from Misc."
