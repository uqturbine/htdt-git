# HTDT - Heat engine & Turbomachinery Design Tools
This is a collection of tools useful for the thermo and fluid dynamic analysis and design of heat engines and turbomachinery.

## Contents
* SSCAR - Steady State Cycle Analyser
    * Features
    * Getting started
* HX_solver - Heat Exchanger Analysis 
    * Features
    * Getting started
* Radial Inflow Turbine Meshing
    * Features
    * Dependencies
    * Getting started
* AxTurb 
* Miscellaneous Scripts
    * List of Scripts
* Computational Environment
* Documentation
* License
* Contributors
* Contributing and Collaborating
* Chief Gardeners

## SSCAR - Steady State Cycle Analyser
Steady-State Cycle AnalyseR (SCCAR), a solution engine 
for the steady state evaluation of thermodynamical cycles.

### SSCAR - Features
SCCAR can be used to parametrically design thermodynamic 
cycles using a library of predefined building blocks, such
as compressors, turbines, heat exchangers, and other user 
defined components. For any cycle the steady state performance 
is evaluated, so that energy and mass flow around the 
cycle are balanced. To allow accurate off-design performance 
analysis the component library in SCCAR includes components 
that incorporate accurate physics or map based performance 
models. Using these allow the accurate analysis of changes 
in the operating point and overall efficiency as external 
parameters are altered. 

### SSCAR - Getting started
A brief user-guide and theory manual for the code can 
be downloaded from: [https://espace.library.uq.edu.au/view/UQ:604824](https://espace.library.uq.edu.au/view/UQ:604824)

The find out how to use the code, execute the command `$$ SSCAR.py --help`

A range of examples is available from `htdt/examples/SSCAR/`

## HX_solver
HX_solver.py is stand-alone piece of software for the 
simulation and analysis of a range of Heat Exchangers.
It employs a one-dimensional approximation to predict 
heat exchanger performance and operation. 

### HX_solver - Features
HX_solver.py is a quasi one-dimensional code that can 
be used for the design and evaluation of heat exchangers 
operating with ideal fluid, ideal gases and real gases. 
For a given heat exchanger defined by heat exchanger 
type, a range of variables defining the geometry of the 
heat exchangers, selected heat transfer correlations and 
selected pressure drop correlation the exchanger performance 
is evaluated. Employing a fixed heat exchanger geometry 
allows the performance of a given heat exchanger 
to be evaluated as fluid boundary conditions (mass flow rate, 
inlet temperature, inlet pressure) change in both heat 
transfer channels. In the first instance this allows 
change in heat exchanger performance for a given heat 
exchanger to be evaluated across a range of operating 
conditions. Furthermore this can be used to appropriately 
size a heat exchanger that is required to operate at 
a number of conditions.

### HX_solver - Getting started
A brief user-guide and theory manual for the code can 
be downloaded from: [https://espace.library.uq.edu.au/view/UQ:524902](https://espace.library.uq.edu.au/view/UQ:524902)

The find out how to use the code, execute the command `$$ HX_solver.py --help`

A range of examples is available from `htdt/examples/HX/`

## Radial Inflow Turbine Meshing
This code allows the parameterised generation of meshes 
as used in the CFD simulation of Radial Inflow Turbines.  
The code support the following mesh types:
* meshes for subsonic nozzle guide vanes
* meshes for supersonic nozzle guide vanes
* meshes for radial inflow (or radial outflow) turbines. C meshes, O meshes and H meshes are supported. 

### Features
A key feature of the turbine rotor mesh and geometry generation 
tool is that it allows a parametric definition of the geometry 
based on properties of the aerodynamic passage. For example 
desired flow direction and evolution of flow area. This is in 
contrast to alternative methods, which start by defining the 
physical features of the rotor (e.g. hub and shroud shape), 
in which case the aerodynamic passage becomes and output. 
In addition to providing the description of the two tools, 
usage instructions and examples are provided.

Currently meshes are generated in the Eilmer 3 native format. 
Tools are provided to convert these meshes to the *foam* format 
used by OpenFOAM, which then allows conversion to a number 
of common formats.

### Dependencies
This meshing tool has been developed based on the meshing 
engine from Eilmer 3. Hence to create meshes a full working 
installation of Eilmer 3 is required. Please refer to instructions 
from [http://cfcfd.mechmining.uq.edu.au/eilmer3.html](http://cfcfd.mechmining.uq.edu.au/eilmer3.html) 
for installation and build instructions.

### Getting started
A brief user-guide and theory manual for the code can 
be downloaded from: [https://espace.library.uq.edu.au/view/UQ:362796](https://espace.library.uq.edu.au/view/UQ:362796)

A range of examples is available from `htdt/examples/TurbineMesh/`


## AxTurb
Preliminary design code for multi stage axial machines. 
A sequence of blades (rotors and stators) with corresponding 
blade angles is defined as well as appropriate loss model.
The code then automatically finds a solution that returns the 
correct performance for a given overall pressure ratio. 


## Miscellaneous Scripts
This directory contains a selection of small, useful scripts. See the 
respective scripts for a brief description and usage instructions.

### List of Scripts
* Python script to automatically generate Real-Gas-Property files, `.rgp` 
files, as used by CFX. The real gas property files are generated using 
CoolProp, which can use its build in properties calculator or tunnel through
to the NIST RefProp database.
* add new Scripts


## Computational Environment
Once the repository has been cloned, all you require is 
a python and command line environment. The exception is 
the Radial Turbine Meshing, which also requires a Linux 
environment and a working Eilmer installation. 

The following dependencies commonly not included in standard 
python distributions exist: `CoolProp`, 

To run the programmes, a number of environment variables must be set. 
For a bash shell these would be: 
```
    export HTDT=${HOME}/htdt-inst
    export PATH=${PATH}:${HTDT}/bin
    export PYTHONPATH=${PYTHONPATH}:${HTDT}/bin
```
In a Linux or OS-x based system this can achieved by adding the 
above lines to the `.bashrc` or `.profile` files.
For windows, follow the instructions from 
[https://superuser.com/questions/949560/how-do-i-set-system-environment-variables-in-windows-10](https://superuser.com/questions/949560/how-do-i-set-system-environment-variables-in-windows-10)
to edit or add the required environmental variables.

Then to install the code, execute the `install.sh` script. This 
will create the htdt-inst directory, which is referenced above.
To customise your installation, change both the install script 
and your environmental variables. 

## Documentation
Specific documentation in the form of user and theory 
guides, as pdf, for the individual codes are available 
via the hyperlinks provided above. These should be read 
in conjunction with looking at specific examples provided 
in `htdt/examples/`.

## License
For the source code, we use the GNU General Public License 3.
Please see the file `gpl.txt`.
For the documentation, we use the Creative Commons 
Attribution-ShareAlike 4.0 International License.

## Contributors 
Many people have contributed to this code. 
Please refer to `contributors.txt` for some details on 
individual contributions. The commit history is the 
place for further information.

## Contributing and Collaborating
We are always keen for new collaborators that want to contribute 
to this collection of codes. Please get in contact and we can set 
up an account with write access. 
Please read through `Developer-notes.md` before creating new commits.

## Publications 
A number of publications document the development of the 
above codes. Please refer to `publications.txt` for some 
details and corresponding references. 

## Support and Issues
We try to make the code as self explanatory as possible and to 
provide appropriate user guides. See the above links for the
appropriate user-guides for the different tools. 
If you require specific assistance or if you want to report 
issues, please contact the author on [i.jahn@uq.edu.au](i.jahn@uq.edu.au)
  

## Chief Gardeners
Ingo Jahn, 2018-06-29
